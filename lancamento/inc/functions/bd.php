<?php
### FUN��ES PARA MANIPULA��O DE BANCO DE DADOS #######################
#																	 #
#	Autor: Thiago Sim�es de Almeida									 #
#	e-mail/MSN: simonsbr@gmail.com									 #
#	Data de cria��o: 24/08/2005                             		 #
#																	 #
######################################################################

### 01 - Executa qualquer Query no Mysql #############################
function banco($sql,$evento){
	conecta();
	$resultado = mysql_query($sql);
	//or die(mostra_erro(mysql_error(),$sql,server_info()));
	$res['resultado'] = $resultado;
	$res['uid'] = mysql_insert_id();

	if($evento == "s"){
		$linhas = mysql_num_rows($resultado);
		$res["linhas"] = $linhas;

	}elseif($evento == "i" || $evento == "u" || $evento == "d"){
		$afetadas = mysql_affected_rows();
		$res["afetadas"] = $afetadas;
	}
	return $res;
}
### Fim 01 ############################################################
///////////////////////////////////////////////////////////////////////
### 02 - Cria pagina��o de qualquer sql onde os resultados > $res #####
function paginacao($query,$saida,$max_pages=10,$max_results=10,$pag_topo=1,$pag_rodape=0,$det_topo=1,$det_rodape=0,$pagina=_ATUAL_PAGE_,$id_css="id_css",$det_pag = "det_pag"){
//-- 0 - Verifica pagina atual para paginar -------------------------//	
	$pagina = explode("/",$pagina);
	$pagina = end($pagina);
	if($pagina == "index.php" || $pagina == "index2.php"){
		
		$pagina .="?p=$_GET[p]&";
		
	}else {
		
		$pagina = $pagina."?";
	}
// Termina 0 --------------------------------------------------------//
///////////////////////////////////////////////////////////////////////
//-- 1 - Verifica quantas linhas o banco retorna --------------------//
	
	(count($_GET) > 0) ? extract($_GET) : false;
	
		conecta();
		$resultado = banco($query,"s");
		extract($resultado);
		mysql_free_result($resultado);


// Termina 1 --------------------------------------------------------//
///////////////////////////////////////////////////////////////////////
//-- 2- Decide em quantas p�ginas ser�o divididos os resultados -----//

	(!isset($s))? $s = 1 : false;
	(!isset($t))? $t = ceil($linhas / $max_results):false;
	($s > $t)? exit("O n�mero $s n�o � v�lido nesta consulta"):false;
	($t > ceil($linhas / $max_results))? exit("O n�mero $t n�o � v�lido nesta consulta"):false;
	$linhas_total = $linhas;

// Termina 2 ---------------------------------------------------------//
////////////////////////////////////////////////////////////////////////
//-- 3 - Monta o Start  ----------------------------------------------//

	if($s>1){
		$start = ($max_results * ($s-1));
	}else{
		$start = 0;
	}

// Termina 2 ---------------------------------------------------------//
////////////////////////////////////////////////////////////////////////
//-- 3 - Executa a chamada utilizando LIMIT  -------------------------//

	$sql = $query . " LIMIT $start, $max_results";

	conecta();
	$resultado = banco($sql,"s");
	extract($resultado);

// Termina 3 --------------------------------------------------------//
///////////////////////////////////////////////////////////////////////
//-- 4 - Verifico Inicio e Fim da pagina atual ----------------------//

		$de = $start + 1;
		$para = $start + $max_results;
		($para > $linhas_total)? $para = $para - ($para-$linhas_total):false; 
		($linhas_total == 1)? $plural="" : $plural = "s";
		$detalhes= "<div id=\"$det_pag\"> Resultados <b>$de</b> - <b>$para</b> de <b>$linhas_total</b>.</div>";
		
		
// Termina 4 --------------------------------------------------------//
///////////////////////////////////////////////////////////////////////
//-- 4 - Detalhes topo  ---------------------------------------------//
	
	($det_topo == 1 && $linhas_total > $max_results)?print $detalhes:false;
	
// Termina 4 --------------------------------------------------------//
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
//-- 5 - Calculos necess�rios para paginar --------------------------//
		// Se o total de linhas do banco for maior que o numero que foi definifo em resultados executa a p�gina��o.
			if($linhas_total > $max_results){
				
				// Monta quantas p�ginas ser�o inseridas antes e depois da p�gina atual.
				$ant = ceil($max_pages / 2);
				$pos = ceil($max_pages / 2)-1;
		
			
				// Monta qual ser� o meu intervalo de p�ginas a ser montada exemplo 1 at� 10 ou 8 at� 19 e etc
				$decres=$s - $ant;
				$acres=$s + $pos;
				
				//Verifrica se meu primeiro item � negativo... pois n�o poderia ter pagina��o de -5 at� 8 tenho que come�ar do 1.
				if($decres < 1){
		
					//Retorna na vari�vel resto quantas p�ginas eu terei que adicionar ao final sendo que tenho n�meros negativos.
					$conta = $decres - 1;
					$resto = substr($conta, 1);
					// Acima retiro o sinal de negativo que sobrou para que possa somar na pr�xima.
			
					// Somo quantas p�gina ser�o adicionadas ao final e ao come�o para que fique positivo.
					$decres += $resto;
					$acres += $resto;
	
				}
				
				//Se meu indice final de pagina��o for maior que o numero de paginas que preciso reduzo...
				if($acres > $t){
			
					$acres0 = $acres-$t;
					$acres -=  $acres0;
			
				}
			
				// Finalmente tenho os indices da minha pagina��o EX: 8 - 12 , 80 - 92 e etc.
				
				$l=range($decres, $acres);
			}
			
				
////////////////////////////////////////////////////////////////////////
//-- 5 - Inicia Pagina��o do topo ------------------------------------//

	if($pag_topo == 1 && $linhas_total > $max_results){

		//Se n�o estiver na primeira p�gina coloco o bot�o anterior.
		if($s>1){
			$h = $s - 1;
			print "<a id=\"$id_css\" href=\"$pagina"."s=$h&t=$t\" title=\"P�gina anterior\"><<</a> ";
		}else{
			print "<span id=\"$id_css\"><<</span> ";
		}
	
		//Monto todos os links de p�ginas.
		foreach($l as $indice2 => $valor2){
			
			$atual = $indice2+1;
			
			$contar = $max_results - 1;
			$inicio= $h + 1;
			$final= $inicio + $contar;
	
			if($s != $valor2){
				print "<a id=\"$id_css\" href=\"$pagina"."s=$atual&t=$t\">$valor2</a> ";
			}else{
				print "<strong id=\"$id_css\">$valor2</strong>";
			}
		}
	
		if($s!=$t){
			$h = $s + 1;
			print "<a id=\"$id_css\" href=\"$pagina"."s=$h&t=$t\" title=\"Pr�xima p�gina\">>></a><br /><br />";
		}else{
			print "<span id=\"$id_css\">&nbsp;>></span><br /><br />";
		}
	}
// Termina 5 ---------------------------------------------------------//
///////////////////////////////////////////////////////////////////////
//-- 6 - Monta na tela os resultados  -------------------------------//
		$o = 1;
		while($res=mysql_fetch_array($resultado)){
	    	extract($res);
			eval($saida);
			$o++;
		}
// Termina 6 --------------------------------------------------------//
///////////////////////////////////////////////////////////////////////
//-- 7 - Detalhes topo  ---------------------------------------------//

	($det_rodape == 1 && $linhas_total > $max_results)?print $detalhes:false;

// Termina 7 --------------------------------------------------------//
///////////////////////////////////////////////////////////////////////
//-- 5 - Inicia Pagina��o do topo ------------------------------------//

	if($pag_rodape == 1 && $linhas_total > $max_results){

		//Se n�o estiver na primeira p�gina coloco o bot�o anterior.
		if($s>1){
			$h = $s - 1;
			print "<a id=\"$id_css\" href=\"$pagina"."s=$h&t=$t\" title=\"P�gina anterior\"><<</a> ";
		}else{
			print "<span id=\"$id_css\"><<</span> ";
		}
	
		//Monto todos os links de p�ginas.
		foreach($l as $indice2 => $valor2){
			
			$atual = $indice2+1;
			
			$contar = $max_results - 1;
			$inicio= $h + 1;
			$final= $inicio + $contar;
	
			if($s != $valor2){
				print "<a id=\"$id_css\" href=\"$pagina"."s=$atual&t=$t\" title=\"Mostrar resultados de $inicio - $final\">$valor2</a> ";
			}else{
				print "<strong id=\"$id_css\">$valor2</strong> ";
			}
		}
	
		if($s!=$t){
			$h = $s + 1;
			print "<a id=\"$id_css\" href=\"$pagina"."s=$h&t=$t\" title=\"Pr�xima p�gina\">>></a><br />";
		}else{
			print "<span id=\"$id_css\">>></span><br /><br />";
		}
	}
// Termina 5 ---------------------------------------------------------//
}
### Fim 02 ###########################################################
//------------------------------------------------------------------//
//------------------------------------------------------------------//
?>
