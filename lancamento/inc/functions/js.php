<?php
### FUN��ES GERADORAS DE JS INLINE ###################################
#																	 #
#	Autor: Thiago Sim�es de Almeida									 #
#	e-mail/MSN: simonsbr@gmail.com									 #
#	Data de cria��o: 24/08/2005                             		 #
#																	 #
######################################################################

### 01 - Desabilitar bot�o de form ###################################
// Argumento 1  - Qualquer evento javascript
// Argumento 2  - texto a ser exibido no bot�o desabilitado
function desabilita_bt($evento,$texto){

	$hd = "$evento=\"this.value='$texto'; this.disabled=true; this.form.submit();\"";
	print $hd;

}
### Fim 01 ############################################################
///////////////////////////////////////////////////////////////////////
### 02 - Desabilita sele��o de texto da p�gina #######################
function travar_selecao(){

	$tr = "onselectstart=\"return false\"";
	print $tr;

}
### Fim 02 ############################################################
///////////////////////////////////////////////////////////////////////
### 03 - Muda CSS de objeto ###########################################
function muda_obj($propiedade,$valor){

	$muda = "this.style.$propiedade='$valor';";
	print $muda;
}
### Fim 03 ############################################################
///////////////////////////////////////////////////////////////////////
### 04 - Atualiza o site ##############################################
function atualizar($evento){

	$at = "$evento=\"history.go();\"";
	print $at;

}
### Fim 04 ############################################################
///////////////////////////////////////////////////////////////////////
### 05 - Imprime o site ###############################################
function imprime($evento){

	$at = "$evento=\"print();\"";
	print $at;

}
### Fim 05 ############################################################
///////////////////////////////////////////////////////////////////////
### 05 - Arrasta objetos com class="dragme" ###########################
function arrastar(){
	$arr = "
	<style type=\"text/css\">
		.dragme{position:relative;}
	</style>
	<script language=\"javascript\">
		var ie=document.all;
		var nn6=document.getElementById&&!document.all;
		var isdrag=false;
		var x,y;
		var dobj;
				
			function movemouse(e){
			  if (isdrag){
		
			    dobj.style.left = nn6 ? tx + e.clientX - x : tx + event.clientX - x;
			    dobj.style.top  = nn6 ? ty + e.clientY - y : ty + event.clientY - y;
			    return false;
		  	  }
			}
		
		
			function selectmouse(e){
		
			  var fobj       = nn6 ? e.target : event.srcElement;
			  var topelement = nn6 ? \"HTML\" : \"BODY\";
	
			  	while (fobj.tagName != topelement && fobj.className != \"dragme\"){
				    fobj = nn6 ? fobj.parentNode : fobj.parentElement;
		        }
				
		        if (fobj.className==\"dragme\"){
		
				    isdrag = true;
				    dobj = fobj;
				    tx = parseInt(dobj.style.left+0);
				    ty = parseInt(dobj.style.top+0);
				    x = nn6 ? e.clientX : event.clientX;
				    y = nn6 ? e.clientY : event.clientY;
				    document.onmousemove=movemouse;
				    return false;
		       }
		   }
		
		document.onmousedown=selectmouse;
		document.onmouseup=new Function(\"isdrag=false\");
</script>
	 	  ";
	print $arr;
}
### Fim 05 ############################################################
///////////////////////////////////////////////////////////////////////
### 06 - Monta banner fixo ############################################
function banner_fixo($pos_esq,$pos_top,$link_ban,$end_img_ban,$alt_ban,$class_fechar,$class_ban){

	$mt = "<script language=javascript>
         //////////////////////////////////////    BANNER 120X60   /////////////////////////////////
			<!--
			// POSI��O DA JANELA
			document.write('<div style=\"position:absolute;left:".$pos_esq."px; top:".$pos_top."px\">')
			// POSI��O DA JANELA
			
			var isIE=document.all?true:false;var isNS4=document.layers?true:false;var isIE6=isIE&&document.getElementById?true:false;var isNS6=!isIE&&document.getElementById?true:false;var isNS=isNS4||isNS6;var AD_CLOSED=false;var SCROLL_TIMER=null;var VerticalPositionMode=\"top\";var VerticalPositionOffset=0;var HorizontalPositionMode=\"right\";var HorizontalPositionOffset=468+16;var DYN_AD=null;var cwidth;var cheight;var SHOW;var INVIS;
			
			// ENDERE�O AO CLICAR NO BANNER
			var cgi='$link_ban';
			// ENDERE�O AO CLICAR NO BANNER
			
			function ReturnXPos(){if (isIE)cwidth=document.body.clientWidth;else cwidth=window.innerWidth-35;if (HorizontalPositionMode == \"right\")return cwidth - HorizontalPositionOffset;else if (HorizontalPositionMode == \"left\")return HorizontalPositionOffset;}
			function ReturnYPos()
			{
			  var _ADD=(document.layers?6:0);
			  var _VPO = VerticalPositionOffset+_ADD;
			  if(!document || ! document.body) return _VPO;
			  if (VerticalPositionMode == \"bottom\")
			  {
			    if (isIE)
			      return document.body.scrollBottom - _VPO + 10;
			    else
			      return window.pageYOffset + window.height - _VPO + 10;
			  }
			  else if (VerticalPositionMode == \"top\")
			  {
			    if(document.body.scrollTop<_VPO)
			      _VPO=_VPO-document.body.scrollTop;
			    else if(window.pageYOffset<_VPO)
			      _VPO=_VPO-window.pageYOffset;
			    else
			      _VPO=0;
			    if (isIE)
			      return document.body.scrollTop - 0 + _VPO;
			    else
			      return window.pageYOffset - 0 + _VPO;
			  }
			  return 0;
			}
			
			function CreateFreezingTag()
			{
			  var _ot='';
			  if (isIE||isNS6)
			  {
			    var DivPositionString ='style=\"position:absolute;top:' + ReturnYPos () +';left:'+ReturnXPos()+';';
			    _ot ='<div class=\'$class_ban\' align=\"right\" id=DYN_AD_ID '+DivPositionString+'visibility:visible;\">';
			    _ot+='<a href=\"'+cgi+'\"target=_blank\"\">';
			
			// ENDERE�O DA IMAGEM
			    _ot+='<img src=\"$end_img_ban\" border=0 alt=\"$alt_ban\">';
			// ENDERE�O DA IMAGEM
			
			    _ot+='</a>';
			    _ot+='<table cellspacing=\"0\"><tr><td align=\"right\">';
			    _ot+='<a onMouseOut=\"window.status=\'\'\" href=\"#\" onMouseOver=\"window.status=\'Fechar\';return true\" onclick=\"javascript:goAway();\" class=\"$class_fechar\">';
			    _ot+='X';
			    _ot+='</a>';
			    _ot+='</td></tr></table>';
			    _ot+='</div>';
			    document.write(_ot);
			    if(isIE6||isNS6)
			      DYN_AD = (document.getElementById('DYN_AD_ID'))?document.getElementById('DYN_AD_ID').style:null;
			    else
			      DYN_AD = document.all.DYN_AD_ID.style;
			    INVIS=\"hidden\";
			    SHOW=\"visible\";
			  }
			  else
			  {
			    document.write(_ot);
			    DYN_AD = document.DYN_AD_ID;
			    INVIS=\"hide\";
			    SHOW=\"show\";
			  }
			}
			function ScrollEvent(e){if(AD_CLOSED)return;
			
			if(!DYN_AD&&isNS6)DYN_AD=document.getElementById(\"DYN_AD_ID\").style;
			ReturnXPos();if ((cwidth<468) ){DYN_AD.visibility=INVIS;}else{DYN_AD.left = ReturnXPos();DYN_AD.top = ReturnYPos();DYN_AD.visibility=SHOW;}}
			
			function initAD()
			{
			  AD_CLOSED=false;
			  if (isIE || isNS)
			  {
			    if (isNS && HorizontalPositionMode==\"right\")
			    {
			      HorizontalPositionOffset=HorizontalPositionOffset-16;
			    }
			    CreateFreezingTag();
			    window.onresize = ScrollEvent;
			    if (isIE)window.onscroll= ScrollEvent;
			    if (isNS)SCROLL_TIMER=window.setInterval(\"ScrollEvent();\", 1000);
			  }
			}
			initAD();
			function goAway(){DYN_AD.visibility=INVIS;AD_CLOSED=true;if(isNS){if(SCROLL_TIMER)clearInterval(SCROLL_TIMER);SCROLL_TIMER=null;}}
			-->
			</script>
			";

	print $mt;
}
### Fim 06 ############################################################
///////////////////////////////////////////////////////////////////////
### 06 - Cria Tooltip #################################################
function tooltip(){
	print " <script src=\""._INC_DIR_."prototype.js\" type=\"text/javascript\"></script>
			<script src=\""._INC_DIR_."scriptaculous.js\" type=\"text/javascript\"></script>
			<script src=\""._INC_DIR_."Tooltip.js\" type=\"text/javascript\"></script>
		  ";
}
### Fim 07 ############################################################
///////////////////////////////////////////////////////////////////////	
?>
