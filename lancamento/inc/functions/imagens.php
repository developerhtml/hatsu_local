<?php
### FUN��ES PARA TRATAMENTO DE IMAGEM ################################
#																	 #
#	Autor: Thiago Sim�es de Almeida									 #
#	e-mail/MSN: simonsbr@gmail.com									 #
#	Data de cria��o: 23/11/2005                             		 #
#																	 #
######################################################################

### 01 - Gera Miniatura ##############################################
function miniatura($imagem_a_converter,$dir_e_ou_nome_para_salvar="images/miniatura/mini.jpg",$apagar_original=1,$gravar_por_cima=1,$qualidade=65,$largura_fixa_ou_percentual="50%",$marca_agua="",$posicao_agua=4,$margem_agua=0,$alpha_agua=50,$width,$height){

	if(!file_exists($imagem_a_converter)){
//		die("$imagem_a_converter");
    	print "O arquivo de origem n�o existe!";
    	exit;
	}else{
		if($tamanho_fixo=="N" && ($percentual<1 || $percentual>100)){
    		print "O percentual deve ser entre 1 a 100!";
    		exit;
		}
		// monta o nome do arquivo resultante
		$arquivo_miniatura = explode('.', $imagem_a_converter);
		$extensao = strtolower(end($arquivo_miniatura));
		$tipo_imagem = strtoupper($extensao);
		$co = count($arquivo_miniatura);
		$loop = $co - 2;
		if($co > 2){
			for($i=0;$i<=$loop;$i++){
				$arquivo_miniatura .= $arquivo_miniatura[$i].".";
			}
		}else{
			$arquivo_miniatura = $arquivo_miniatura[0];
		}
		if(($tipo_imagem=="JPG") OR ($tipo_imagem=="JPEG")){ 
		$tipo = "JPEG"; 
		} 
			elseif($tipo_imagem=="GIF"){ 
		$tipo = "GIF"; 
		} 
			elseif($tipo_imagem=="PNG"){ 
		$tipo = "PNG"; 
		}
		else{ 
			print "O tipo de arquivo escolhido n�o � v�lido!";
    		exit;
		}
		$CriarImagemDe = "imagecreatefrom".$tipo;
		$SalvarImagemComo = "image".$tipo;
		
		// l� a imagem de origem e obt�m suas dimens�es
		$img_origem = $CriarImagemDe($imagem_a_converter);
			$origem_x = ImagesX($img_origem);
			$origem_y = ImagesY($img_origem);

//		 se n�o for tamanho fixo, calcula as dimens�es da miniatura
//		$t= substr($largura_fixa_ou_percentual,-1,1);
//		$medida = explode("%",$largura_fixa_ou_percentual);
//		$medida = current($medida);
//		
//		if($t != "%"){
//			$x = explode("%",$medida);
//			$x = current($x);
//			$y = $origem_y * $x / $origem_y;
//		}elseif($t == "%"){
//			
//			$x = intval ($origem_x * $medida/100);
//    		$y = intval ($origem_y * $medida/100);
//		}else{
//			print "Medida inv�lida para redimensionamento. Use conforme exemplo: 50 ou 50%";	
//			exit;
//		}

//		$x = $width;
//		$y = $height;

		if ($origem_y>$origem_x) {
				
			$y = $width;
			$x = $height;
		
		}else {
				
			$x = $width;
			$y = $height;
			
		} 

		// cria a imagem final, que ir� conter a miniatura
		$img_final = ImageCreateTrueColor($x,$y);

		// copia a imagem original redimensionada para dentro da imagem final
		ImageCopyResampled($img_final, $img_origem, 0, 0, 0, 0, $x+1, $y+1, $origem_x , $origem_y);
		if($marca_agua != ""){
			
			$arquivo_miniatura_agua = explode('.', $marca_agua);
			$extensao_agua = strtolower(end($arquivo_miniatura_agua));
			$tipo_imagem_agua = strtoupper($extensao_agua);
			$co_agua = count($arquivo_miniatura_agua);
			$loop_agua = $co_agua - 2;
		
				if($co_agua > 2){
					for($i=0;$i<=$loop_agua;$i++){
						$arquivo_miniatura_agua .= $arquivo_miniatura_agua[$i].".";
					}
				}else{
					$arquivo_miniatura_agua = $arquivo_miniatura_agua[0];
				}
				
				if(($tipo_imagem_agua=="JPG") OR ($tipo_imagem_agua=="JPEG")){ 
					$tipo_agua = "JPEG"; 
				}elseif($tipo_imagem_agua=="GIF"){ 
					$tipo_agua = "GIF"; 
				}elseif($tipo_imagem_agua=="PNG"){
					$tipo_agua = "PNG"; 
				}else{
					print "O tipo de arquivo para escolhido para marca d'agua n�o � v�lido!";
	    			exit;
				}
				
				$CriarImagemDe_agua = "imagecreatefrom".$tipo_agua;
				
				$imagem_marca = $CriarImagemDe_agua($marca_agua);
				$pontoX1 = ImagesX($imagem_marca);
				$pontoY1 = ImagesY($imagem_marca);
				
				
				if($posicao_agua == 1){
					$dest_x=0+$margem_agua;
					$dest_y=0+$margem_agua;
				}elseif ($posicao_agua == 2){
					$dest_x=($x-$pontoX1)-$margem_agua;
					$dest_y=0+$margem_agua;
				}elseif ($posicao_agua == 3){
					$dest_x= ($x-$pontoX1)-$margem_agua;
					$dest_y= ($y-$pontoY1)-$margem_agua;
				}elseif ($posicao_agua == 4){
					$dest_x=0+$margem_agua;
					$dest_y=($y-$pontoY1)-$margem_agua;
				}elseif ($posicao_agua == 5){
					$dest_x= ($x/2) - ($pontoX1/2);
					$dest_y= ($y/2) - ($pontoY1/2);
				}
				
				ImageCopyMerge($img_final, $imagem_marca, $dest_x, $dest_y, 0, 0, $pontoX1, $pontoY1, $alpha_agua);
				
		}

		// cria pasta se for necess�rio
		$dir = explode("/",$dir_e_ou_nome_para_salvar);
			if(count($dir) > 2){
   				$loop = count($dir) - 2; // thiago.jpg

   					for($i=0;$i<=$loop;$i++){
    					$pasta .= $dir[$i]."/";
    				}
    				cria_pasta($pasta);
			}
			if($gravar_por_cima == 1){
				
				//salva o arquivo na pasta criado ou na raiz do script
				if($SalvarImagemComo($img_final, $dir_e_ou_nome_para_salvar)){
					// libera a mem�ria alocada para as duas imagens
					ImageDestroy($img_origem);
					ImageDestroy($img_final);
					($apagar_original == 1)? unlink($imagem_a_converter):false;
					return true;
				}else{
					return false;	
				}
			}else{
				if(file_exists($dir_e_ou_nome_para_salvar)){
					print "A imagem $dir_e_ou_nome_para_salvar j� existe";
					exit;
				}
				return false;
			}
   	}
}
##################################################################################

### 01 - Gera Miniatura CAPA DE ALBUM DE PHOTOS ##############################################
function miniatura_album_photos($imagem_a_converter,$dir_e_ou_nome_para_salvar="images/miniatura/mini.jpg",$apagar_original=1,$gravar_por_cima=1,$qualidade=85){

	if(!file_exists($imagem_a_converter)){
    	print "O arquivo de origem n�o existe!";
    	exit;
	}else{

		// monta o nome do arquivo resultante
		$arquivo_miniatura = explode('.', $imagem_a_converter);
		$extensao = strtolower(end($arquivo_miniatura));
		$tipo_imagem = strtoupper($extensao);
		$co = count($arquivo_miniatura);
		$loop = $co - 2;
		if($co > 2){
			for($i=0;$i<=$loop;$i++){
				$arquivo_miniatura .= $arquivo_miniatura[$i].".";
			}
		}else{
			$arquivo_miniatura = $arquivo_miniatura[0];
		}
		if(($tipo_imagem=="JPG") OR ($tipo_imagem=="JPEG")){ 
		$tipo = "JPEG"; 
		} 
			elseif($tipo_imagem=="GIF"){ 
		$tipo = "GIF"; 
		} 
			elseif($tipo_imagem=="PNG"){ 
		$tipo = "PNG"; 
		}
		else{ 
			print "O tipo de arquivo escolhido n�o � v�lido!";
    		exit;
		}
		$CriarImagemDe = "imagecreatefrom".$tipo;
		$SalvarImagemComo = "image".$tipo;
		
		// l� a imagem de origem e obt�m suas dimens�es
		$img_origem = $CriarImagemDe($imagem_a_converter);
		$origem_x = ImagesX($img_origem);
		$origem_y = ImagesY($img_origem);

//		 se n�o for tamanho fixo, calcula as dimens�es da miniatura
//		$t= substr($largura_fixa_ou_percentual,-1,1);
//		$medida = explode("%",$largura_fixa_ou_percentual);
//		$medida = current($medida);
//		
//		if($t != "%"){
//			$x = explode("%",$medida);
//			$x = current($x);
//			$y = $origem_y * $x / $origem_y;
//		}elseif($t == "%"){
//			
//			$x = intval ($origem_x * $medida/100);
//    		$y = intval ($origem_y * $medida/100);
//		}else{
//			print "Medida inv�lida para redimensionamento. Use conforme exemplo: 50 ou 50%";	
//			exit;
//		}

		if ($origem_y>$origem_x) {
				
			$y = 160;
			$x = 120;
		
		}else {
				
			$x = 160;
			$y = 120;
			
		} 

		// cria a imagem final, que ir� conter a miniatura
		$img_final = ImageCreateTrueColor($x,$y);

		// copia a imagem original redimensionada para dentro da imagem final
		ImageCopyResampled($img_final, $img_origem, 0, 0, 0, 0, $x+1, $y+1, $origem_x , $origem_y);

		// cria pasta se for necess�rio
		$dir = explode("/",$dir_e_ou_nome_para_salvar);
			if(count($dir) > 2){
   				$loop = count($dir) - 2; // thiago.jpg

   					for($i=0;$i<=$loop;$i++){
    					$pasta .= $dir[$i]."/";
    				}
    				cria_pasta($pasta);
			}
			if($gravar_por_cima == 1){
				
				//salva o arquivo na pasta criado ou na raiz do script
				if($SalvarImagemComo($img_final, $dir_e_ou_nome_para_salvar)){
					// libera a mem�ria alocada para as duas imagens
					ImageDestroy($img_origem);
					ImageDestroy($img_final);
					($apagar_original == 1)? unlink($imagem_a_converter):false;
					return true;
				}else{
					return false;	
				}
			}else{
				if(file_exists($dir_e_ou_nome_para_salvar)){
					print "A imagem $dir_e_ou_nome_para_salvar j� existe";
					exit;
				}
				return false;
			}
   	}
}
###################################################################
?>
