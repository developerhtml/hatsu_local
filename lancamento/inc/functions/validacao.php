<?php
### 01 - Verifica se o e-mail � v�lido ###############################
//////////////////////////////////////////////////////////////////////
function checaEmail($email){
	$e=explode("@",$email);
	if(count($e)<=1){
		return FALSE;
	}elseif(count($e)==2){
		$ip=gethostbyname($e[1]);
		if($ip==$e[1]){
			return FALSE;
		}elseif($ip!=$e[1]){
			return TRUE;
		}
	}
}
### Fim 01 ###########################################################
//////////////////////////////////////////////////////////////////////
### 02 - Valida CPF ou CNPJ ##########################################
//////////////////////////////////////////////////////////////////////
	/*func��es auxiliares*/
	function cleaner($string){
		return $string = str_replace("/", "", str_replace("-", "", str_replace(".", "", $string)));
	}
	
	function check_fake($string, $length){
		for($i = 0; $i <= 9; $i++) {
			$fake = str_pad("", $length, $i);
			if($string === $fake) return(1);
		}
	}

	function cpf($cpf){
		$cpf = cleaner($cpf);
		$cpf = trim($cpf);
		if(empty($cpf) || strlen($cpf) != 11) return FALSE;
		else {
			if(check_fake($cpf, 11)) return FALSE;
			else {
				$sub_cpf = substr($cpf, 0, 9);
				for($i =0; $i <=9; $i++) {
					$dv += ($sub_cpf[$i] * (10-$i));
				}
				if ($dv == 0) return FALSE; 
				$dv = 11 - ($dv % 11); 
				if($dv > 9) $dv = 0;
				if($cpf[9] != $dv) return FALSE;

				$dv *= 2;
				for($i = 0; $i <=9; $i++) {
					$dv += ($sub_cpf[$i] * (11-$i));
				}
				$dv = 11 - ($dv % 11); 
				if($dv > 9) $dv = 0;
				if($cpf[10] != $dv) return FALSE;
				return TRUE;
			}
		}
	}

	function cnpj($cnpj){
		$cnpj = cleaner($cnpj);
		$cnpj = trim($cnpj);
		if(empty($cnpj) || strlen($cnpj) != 14) return FALSE;
		else {
			if(check_fake($cnpj, 14)) return FALSE;
			else {
				$rev_cnpj = strrev(substr($cnpj, 0, 12));
				for ($i = 0; $i <= 11; $i++) {
					$i == 0 ? $multiplier = 2 : $multiplier;
					$i == 8 ? $multiplier = 2 : $multiplier;
					$multiply = ($rev_cnpj[$i] * $multiplier);
					$sum = $sum + $multiply;
					$multiplier++;

				}
				$rest = $sum % 11;
				if ($rest == 0 || $rest == 1)  $dv1 = 0;
				else $dv1 = 11 - $rest;
				
				$sub_cnpj = substr($cnpj, 0, 12);
				$rev_cnpj = strrev($sub_cnpj.$dv1);
				unset($sum);
				for ($i = 0; $i <= 12; $i++) {
					$i == 0 ? $multiplier = 2 : $multiplier;
					$i == 8 ? $multiplier = 2 : $multiplier;
					$multiply = ($rev_cnpj[$i] * $multiplier);
					$sum = $sum + $multiply;
					$multiplier++;

				}
				$rest = $sum % 11;
				if ($rest == 0 || $rest == 1)  $dv2 = 0;
				else $dv2 = 11 - $rest;

				if ($dv1 == $cnpj[12] && $dv2 == $cnpj[13]) return TRUE;
				else return FALSE;
			}
		}
	}
	/*fim func��es auxiliares*/
	
	function valida_CPF_CNPJ($num){
		if(strlen($num)<=14){
			if(cpf($num)){
				return true;
			}else{
				return false;	
			}
		}else{
			if(cnpj($num)){
				return true;
			}else{
				return false;	
			}			
		}
	}
### Fim 02 ###########################################################
//////////////////////////////////////////////////////////////////////
?>
