<?php
function validaData($dt){ // se validar retorna true sen�o retorna false
	$conta_caracteres = strlen($dt);
	if ($conta_caracteres == 10) {
		$array_data = explode("/",$dt);
		$dia = $array_data[0];
		$mes = $array_data[1];
		$ano = $array_data[2];
		if (strlen($dia) == 2 && strlen($mes) == 2 && strlen($ano) == 4) {
			if ($ano >= 1970 && $ano <= 2038) {
				((($ano % 4) == 0 && ($ano % 100)!=0) || ($ano % 400)==0) ? $bissexto = 0 : $bissexto = 1;
				if (($mes != 02) && ($dia >= 1 && $dia <= 31) && ($mes >= 1 && $mes <= 12)) {
					return true;
				}else{
					if ($bissexto == 1) {
						if($dia >= 1 && $dia <= 28){
							return true;
						}
					}else{
						if ($dia >= 1 && $dia <= 29){
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}

function transformaData($dt,$NormaltoSql_um_SqltoNormal_dois=1) {
	if ($dt=="0000-00-00" || $dt=="00/00/0000") return '';
	if ($NormaltoSql_um_SqltoNormal_dois == 1) {
		return implode("-", array_reverse(explode("/", $dt)));
	}elseif ($NormaltoSql_um_SqltoNormal_dois == 2){
		return implode("/", array_reverse(explode("-", $dt)));
	}
}

/*

FUN��O DE OPERA��ES COM DATAS E HORAS

O que faz ?
Analisa qualquer descri��o em texto em ingl�s de data hora em timestamp Unix.
Exemplos de uso do tipo de c�lculos:

operations($data,"now");
operations($data,"10 September 2000");
operations($data,"+1 day");
operations($data,"+1 week");
operations($data,"+1 week 2 days 4 hours 2 seconds");
operations($data,"next Thursday");
operations($data,"last Monday");

*/

function operations($dt, $tipo_calculo = ""){

	$array_hora = explode(":",$dt);
	$array_data = explode("/",$dt);
	$dia = $array_data[0];
	$mes = $array_data[1];
	$ano = $array_data[2];
	$hora = $array_hora[0];
	$minuto = $array_hora[1];
	$segundo = $array_hora[2];

	$timestamp = mktime($hora, $minuto, $segundo, $mes, $dia, $ano);
	return date("d/m/Y",strtotime("$tipo_calculo", $timestamp));

}
?>
