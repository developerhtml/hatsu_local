<?php
### 01 - Retorna parte da string ou do array #########################
function ler_string($string,$de=0,$ate=50){
	$mesa = count($string);
	if($mesa == 1){	
		$saida = substr($string,$de,$ate);
	}else{
		$i = 0;
		while($i < $mesa){
		$saida[] = substr($string[$i],$de,$ate);	
		$i++;
		}
	}
	return $saida;		
}
### Fim 01 ###########################################################
//////////////////////////////////////////////////////////////////////
### 02 - Retira acentos e espa�os em branco ##########################
function tirar_acentos($string){
		
		$bad = "����������������������������&������������������������������������������������������ ";
		$good = "AaEeIiOoUuCcAaAaAaEeIiOoOoUueSZszYAAAAACEEEEIIIINOOOOOUUUYaaaaaceeeeiiiinooooouuuyy_";
		$arr = array('�' => 'ae', '�' => 'oe', '�' => 'ue', '�' => 'ss');
		$str = strtolower(strtr($string, $bad, $good));
		$str = strtr($str, $arr);
		$str = preg_replace('/[^a-zA-Z0-9._-]/', '', $str);
		return $str;
	}
### Fim 02 ###########################################################
//////////////////////////////////////////////////////////////////////
### 03 - Limpa tudo que vier de um formul�rio ########################
function tratar_dados($zero_post_um_get=0){
	if($zero_post_um_get == 0){
		if(count($_POST)>1){
			foreach($_POST as $indice => $valor){
				if(!is_array($valor)){
				if($indice <> "artigo" && $indice <> "descricao" && $indice <> "description" && $indice <> "embed"){
					$valor = trim(strip_tags($valor));
					}
					$_POST[$indice] = $valor;
					$_SESSION["valores"][$indice] = $valor;
				}else{
					$_SESSION["valores"][$indice] = $valor;
				}
			}
			extract($_SESSION['valores']);
			return true;
		}else{
			return false;	
		}
	}elseif($zero_post_um_get == 1){
		if(count($_GET)>1){
			foreach($_GET as $indice => $valor){
				if(!is_array($valor) && $indice != "area"){
				$valor = trim(strip_tags($valor));
					$_GET[$indice] = $valor;
					$_SESSION["valores"][$indice] = $valor;
				}else{
					$_SESSION["valores"][$indice] = $valor;
				}
			}
			extract($_SESSION['valores']);
			return true;
		}else{
			return false;	
		}
	}
}
### Fim 03 ###########################################################
//////////////////////////////////////////////////////////////////////
### 04 - Elimina dados criados pela fun��o acima #####################

function eliminar_dados($zero_sess_valores_um_sess_erros_dois_ambos=2){
	if($zero_sess_valores_um_sess_erros_dois_ambos == 0){
		unset($_SESSION['valores']);
	}elseif($zero_sess_valores_um_sess_erros_dois_ambos == 1){
		unset($_SESSION['erros']);
	}elseif($zero_sess_valores_um_sess_erros_dois_ambos == 2){
		unset($_SESSION['erros']);
		unset($_SESSION['valores']);
	}
}

### Fim 04 ###########################################################
//////////////////////////////////////////////////////////////////////
?>
