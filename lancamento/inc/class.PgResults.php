<?php
/**
* Classe de Paginacao
*
**/

/**
* Impede que o arquivo da classe seja
* acessado diretamente.
*/
$checkurl = $_SERVER["PHP_SELF"];
if (eregi("class.PgResults.php", $checkurl)) {
    die("Este arquivo n�o pode ser acessado diretamente.");
}

class PgResults{
    // Atributos
    var $porPagina = "10";
    var $verPorPagina;
    var $get;

    /**
     * Retorna quantos resultados devem aparecer
     * em cada pagina
     *    
     * @return int
     */
    function porPagina(){
        $this->get = $_GET["pp"];
        if ($this->get == "" OR $this->get == 0){
            return $this->porPagina;
        } else {
            return $this->get;
        }
    }
    
    /**
     * Retorna o numero da pagina atual.
     *
     * @return int
     */
    function pagina(){
        $this->get = $_GET["pg"];
        if ($this->get == ""){
            return (int)1;
        } else {
            return $this->get;
        }
    }
    
    /**
     * Retorna o primeiro registro de acordo
     * com a pagina atual.
     *
     * @return int
     */
    function primeiroRegistro(){
        return     ($this->pagina()*$this->porPagina()) - $this->porPagina();
    }
    
    /**
     * Retorna o numero total de paginas
     *
     * @param int $totalRegistros
     * @return int
     */
    function totalPaginas($totalRegistros){
        return ceil($totalRegistros/$this->porPagina());
    }
    
    /**
     * Retorna painel de navegacao
     *
     * @param int $totalRegistros
     * @return string
     */
    function painelNavegacao($totalRegistros){
        global $PHP_SELF;
        
        if ($this->pagina() > 1) {
            
        	$prev = $this->pagina() - 1;
            
        	$prev_link = "<span class=\"pagina_anterior\"><a href=\"$PHP_SELF?pg=$prev&pp=".$this->porPagina()."\" title=\"Go to previous page\"><<</a></span>";
        }
        
           
        if ($this->totalPaginas($totalRegistros) > $this->pagina()) {

        	$next = $this->pagina() + 1;

        	$next_link = "<span class=\"proxima_pagina\"><a href=\"$PHP_SELF?pg=$next&pp=".$this->porPagina()."\" title=\"Go to next page\">>></a></span>";
        }
        
        for ($x=1; $x <= $this->totalPaginas($totalRegistros); $x++) {

        	if ($x == $this->pagina()) {

        		$painel .= "<span class=\"pagina_atual\">$x</span>";
              
        	} else {

        		$painel .= "<span class=\"outras_paginas\"><a href=\"$PHP_SELF?pg=$x&pp=".$this->porPagina()."\">$x</a></span>";
              
        	}
        }
        
        return "<div id=\"paginacao\">$prev_link  $painel $next_link</div>";
    }
    
    /**
     * Retorna menu para selecao de quantos
     * resultados devem aparecer por pagina
     *
     * @return string
     */
    function verPorPagina(){
        global $PHP_SELF;
        
        if (isset($_GET["pp"])) {
        	
	        $this->verPorPagina = "
	        
	        <span class=\"n_results\">[ Resultados por p&aacute;gina: 
	        
	        <a href=\"$PHP_SELF?pg=".$this->pagina()."&pp=10\" ";
	        
	        if ($_GET["pp"] == "10") {
	        	
	        	$this->verPorPagina .= "class=\"selected\">";
	        	
	        }else {
	        	
	        	$this->verPorPagina .= "class=\"non_selected\">";
	        	
	        }
	
	        $this->verPorPagina .= "10</a>&nbsp;|&nbsp;";
	        
	        $this->verPorPagina .= "
	        
	        <a href=\"$PHP_SELF?pg=".$this->pagina()."&pp=50\" ";
	        
	        if ($_GET["pp"] == "50") {
	        	
	        	$this->verPorPagina .= "class=\"selected\">";
	        	
	        }else {
	        	
	        	$this->verPorPagina .= "class=\"non_selected\">";
	        	
	        }
	
	        $this->verPorPagina .= "50</a>&nbsp;|&nbsp;";
	
	        $this->verPorPagina .= "
	        
	        <a href=\"$PHP_SELF?pg=".$this->pagina()."&pp=100\" ";
	        
	        if ($_GET["pp"] == "100") {
	        	
	        	$this->verPorPagina .= "class=\"selected\">";
	        	
	        }else {
	        	
	        	$this->verPorPagina .= "class=\"non_selected\">";
	        	
	        }
	
	        $this->verPorPagina .= "100</a>&nbsp;]
	        
	        </span>";
	        
        }else {
        	
	        $this->verPorPagina = "
	        
	        <span class=\"n_results\">[ Resultados por p&aacute;gina: 
	        
	        <a href=\"$PHP_SELF?pg=".$this->pagina()."&pp=10\" ";
	        
        	$this->verPorPagina .= "class=\"selected\">";
	        	
	        $this->verPorPagina .= "10</a>&nbsp;|&nbsp;";
	        
	        $this->verPorPagina .= "
	        
	        <a href=\"$PHP_SELF?pg=".$this->pagina()."&pp=50\" ";
	        
        	$this->verPorPagina .= "class=\"non_selected\">";
	        	
	        $this->verPorPagina .= "50</a>&nbsp;|&nbsp;";
	
	        $this->verPorPagina .= "
	        
	        <a href=\"$PHP_SELF?pg=".$this->pagina()."&pp=100\" ";
	        
        	$this->verPorPagina .= "class=\"non_selected\">";
	        	
	        $this->verPorPagina .= "100</a>&nbsp;]
	        
	        </span>";        	
        	
        }
        
        return $this->verPorPagina;                        
    }
}
?>