<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Title here -->
    <title>Lançamento Oficial | Hatsu | Óculos de Alta Tecnologia</title>
    <!-- Description, Keywords and Author -->
    <meta name="description" content="Your description">
    <meta name="keywords" content="Your,Keywords">
    <meta name="author" content="ResponsiveWebInc">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Styles -->
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Font awesome CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet">   
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <script type="text/javascript" src="js/cidades-estados-1.2-utf8.js"></script>
    <script type="text/javascript">

    function validaCampos(){

        var nome = document.cadastro.nome;
        var email = document.cadastro.email;
        var estado = document.cadastro.uf;
        var cidade = document.cadastro.city;
        //validaÃ§Ã£o do nomes
        if(nome.value ==""){
          alert("Informe seu nome!");
          nome.focus();
          return false;
        }
        //validaÃ§Ã£o para que o e-mail
        if(!filter(email.value)){
          alert("Informe seu e-mail corretamente");
          email.focus();
          return false;
        }

        if(estado.value ==""){
          alert("Selecione seu estado!");
          estado.focus();
          return false;
        }
      if(cidade.value ==""){
        alert("Selecione sua cidade!");
        cidade.focus();
        return false;
      }

      }
      </script>

      <script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript">
          window.onload = function() {
            new dgCidadesEstados({
              estado: document.getElementById('uf'),
              cidade: document.getElementById('city')
            });
          }
    </script>

      <style type="text/css">

      label.control-label { font-weight: bold; color:#fff; }

      h5.title { font-family: 'grotesque-regular'}

      p {line-height: 18px; color:#000;}

      div.texto {
        background: #f1f1f1;
        font-family: 'grotesque-regular';
        font-weight: bold;
        padding-top: 10px;
        font-size: 20px;
      }

      .btn-default { min-width: 140px; 
        background: green; 
        color: #fff; 
        font-family: 'grotesque-regular'; 
        font-size: 30px;
      border: none;

    }

    p.sucesso {
        margin-top:20px;
        margin-bottom: 20px;
      font-family: 'grotesque-regular';
      font-size: 20px;
      color:#fff;
    }

      </style>

  </head>
  
  <body style="background-color:#FBFBFB">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=188415054684695&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  
<div class="col-md-12" style="min-height:800px;background:url('./img/background.jpg') no-repeat center top" class="responsive">

<div class="col-md-5" style="margin-top:100px;">

  <h4 class="title" style="border:none;"><img src="./img/logo-hatsu.png" class="responsive"></h4>

  <br />

  <h5 class="title" style="border:none;font-size:26px;">LANÇAMENTO OFICIAL</h5>

  <p style="line-spacing:0;">Criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil, a Hatsu proporciona óculos produzidos no Japão, a partir de projetos inovadores e avançados processos de fabricação. Queremos oferecer produtos que se justifiquem por seus materiais e tecnologias.

  <br /><br />
  Lançaremos nosso site no dia 23 de Junho de 2014. Caso queira receber por email um aviso sobre o lançamento, preencha os campos abaixo. Não enviamos spam.

  <br />

  <div style="background: #000;
    filter:alpha(opacity=60);
    -moz-opacity:0.6;
    opacity: 0.6;
    padding:10px;margin-top:10px;">

    <h5 class="title" style="color:#FFF;border:none;font-size:20px;">FIQUE SABENDO</h5>

   <p class="sucesso">Suas informações foram enviadas com sucesso!</p>
</div>

  <hr />        
      <div class="center" style="background:#f1f1f1;padding:5px;">

         <div class="col-md-6 texto">Conheça a Hatsu no Facebook</div>

         <div class="fb-like-box" data-href="https://www.facebook.com/HatsuBrasil" data-width="100" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false"></div>

         &nbsp;
      </div>
</div>

</div>

    <!-- Javascript files -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Isotope, Pretty Photo JS -->
    <script src="js/jquery.isotope.js"></script> 
    <!-- Respond JS for IE8 -->
    <script src="js/respond.min.js"></script>
    <!-- HTML5 Support for IE -->
    <script src="js/html5shiv.js"></script>
  </body> 
</html>