Hey!

Thank you for purchasing the 'Svelte' Wordpress Theme!

All documentation, credits, change log and support info can be found at:
http://themecobra.com/support

Cheers,
Rob & Derek
ThemeCobra

support@themecobra.com
themecobra.com
twitter.com/themecobra