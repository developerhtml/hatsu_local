<div id="sidebar">
 
  <?php dynamic_sidebar('sidebar_posts') ?>
  
  <div class="sidebar-widget">  
    <span class="sidebar-widget-title" id="sidebar-widget-title-social">Connect With Us</span>
    <div class="sidebar-widget-social">
        <?php include("incl/social-icons.php"); ?>   
    </div><!-- .sidebar-widget-social -->
  </div><!-- .sidebar-widget -->
  <div class="clear-widget"></div>   
  
</div><!-- #sidebar -->