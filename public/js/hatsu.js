 $(function() {

    $(".galeria").click(function() {
    var image = $(this).attr("rel");
    $('#galeria').hide();
    $('#galeria').fadeIn('slow');
    $('#galeria').html('<img src="' + image + '" class="imagem-grande" 

/>');
    return false;
      });
    });

    $(document).ready(function(){
      
      $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
      
      // ESCONDE TODOS DIVS INATIVOS

      // DIV DE EXIBIÇÃO
      $("#exibe-tipo").hide();
      $("#exibe-lente").hide();
      $("#exibe-tipo-semgrau").hide();
      $("#exibe-tipo-simples").hide();
      $("#exibe-comprar").hide();
      $("#exibe-selecionar").hide();
      $("#exibe-experimente").hide();
      $("#exibe-texto-multifocal").hide();

      // LINKS EDITAR
      $("#editar-lente").hide();
      $("#editar-tipo-semgrau").hide();
      $("#editar-tipo-simples").hide();

      // DIVS DE ESCOLHA LENTE
      $("#escolha-lente-semgrau").hide();
      $("#escolha-lente-simples").hide();
      $("#escolha-lente-multifocal").hide();

      // DIVS DE ESCOLHA TIPO
      $("#escolha-tipo-plana").hide();
      $("#escolha-tipo-demonstrativa").hide();
      $("#escolha-tipo-normais").hide();
      $("#escolha-tipo-finas").hide();
      $("#escolha-tipo-finissimas").hide();
      $("#escolha-tipo-naosei").hide();

        $('input[type="radio"]').click(function(){

            // BLOCO DE FUNÇÕES PARA LENTE

            // LENTE SEMGRAU
            if($(this).attr("value")=="semgrau"){
                
                $("#exibe-lente").hide(500);

                $("#editar-lente").show(500);

                $("#escolha-lente-semgrau").show(500);
                $("#escolha-lente-simples").hide(500);
                
                $("#exibe-tipo-semgrau").show(500);
                $("#exibe-tipo-simples").hide(500);

            }

            // LENTE SIMPLES
            if($(this).attr("value")=="simples"){

                $("#exibe-lente").hide(500);
                
                $("#editar-lente").show(500);
                
                $("#escolha-lente-semgrau").hide(500);
                $("#escolha-lente-simples").show(500);

                $("#exibe-tipo-simples").show(500);
                $("#exibe-tipo-semgrau").hide(500);
            }

            // LENTE MULTIFOCAL
            if($(this).attr("value")=="multifocal"){
                $("#exibe-lente").hide(500);
                $("#exibe-experimente").show(500);
                $("#exibe-selecionar").hide(500);
                $("#editar-lente").show(500);
                $("#escolha-lente-semgrau").hide(500);
                $("#escolha-lente-simples").hide(500);
                $("#escolha-lente-multifocal").show(500);
                $("#exibe-tipo-semgrau").hide(500);
                $("#exibe-tipo-simples").hide(500);
                $("#exibe-texto-multifocal").show(500);

            }

            // BLOCO DE FUNÇÕES PARA TIPOS DE LENTE SEM GRAU

            // LENTE SEM GRAU

              // PLANA
              if($(this).attr("value")=="plana"){
                  $("#exibe-comprar").show(500);
                  $("#editar-tipo-semgrau").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#escolha-tipo-plana").show(500);
                  $("#exibe-tipo-semgrau").hide(500);
              }

              // DEMONSTRATIVA
              if($(this).attr("value")=="demonstrativa"){
                  $("#exibe-comprar").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#editar-tipo-semgrau").show(500);
                  $("#escolha-tipo-demonstrativa").show(500);
                  $("#exibe-tipo-semgrau").hide(500);
              }

            // BLOCO DE FUNÇÕES PARA TIPOS DE LENTE SIMPLES

            // NORMAIS
            if($(this).attr("value")=="normais"){
                  $("#exibe-comprar").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#editar-tipo-simples").show(500);
                  $("#escolha-tipo-normais").show(500);
                  $("#exibe-tipo-simples").hide(500);
            }

            // FINAS
            if($(this).attr("value")=="finas"){
                  $("#exibe-comprar").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#editar-tipo-simples").show(500);
                  $("#escolha-tipo-finas").show(500);
                  $("#exibe-tipo-simples").hide(500);
            }

            // FINISSIMAS
            if($(this).attr("value")=="finissimas"){
                  $("#exibe-comprar").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#editar-tipo-simples").show(500);
                  $("#escolha-tipo-finissimas").show(500);
                  $("#exibe-tipo-simples").hide(500);
            }

            // NAOSEI
            if($(this).attr("value")=="naosei"){
                  $("#exibe-comprar").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#editar-tipo-simples").show(500);
                  $("#escolha-tipo-naosei").show(500);
                  $("#exibe-tipo-simples").hide(500);
            }                                    

        }); // fecha click function de cradio button
    
    }); // fecha document ready jquery

</script>

<script type="text/javascript">

    $(document).ready(function(){

       $("#forma_pagamento_cartao").click(function() {
          $("#checkout-cartao").show(500); // exibe novamente o 

selecionar
        });

       $("#forma_pagamento_boleto").click(function() {
          $("#checkout-cartao").hide(500); // exibe novamente o 

selecionar
        });
    }); // fecha document ready jquery


</script>

<script type="text/javascript">

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-semgrau").click(function() {

          $("#car-detalhe-semgrau").show(500); // exibe novamente a 

escolha de lentes
          $("#car-detalhe-simples").hide(500); // exibe novamente o 

selecionar

        });
    }); // fecha document ready jquery

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-simples").click(function() {

          $("#car-detalhe-semgrau").hide(500); // exibe novamente a 

escolha de lentes
          $("#car-detalhe-simples").show(500); // exibe novamente o 

selecionar

        });
    }); // fecha document ready jquery

  </script>

<script type="text/javascript">

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#exibe-tudo").click(function() {

          $("#exibe-lente").show(500); // exibe novamente a escolha de 

lentes
          $("#exibe-selecionar").show(500); // exibe novamente o 

selecionar
          $("#exibe-escolher").hide(); // exibe novamente o selecionar
          $("#exibe-experimente").show(500); // exibe novamente o 

selecionar
          $("#exibe-texto-multifocal").hide(500);

        });
    }); // fecha document ready jquery

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#editar-lente").click(function() {

          $("#exibe-lente").show(500); // exibe novamente a escolha de 

lentes
          $("#exibe-selecionar").show(500); // exibe novamente o 

selecionar
          $("#exibe-tipo").hide(500);
          $("#exibe-experimente").show(500);
          $("#exibe-comprar").hide(500);

          $("#editar-lente").hide(500);
          $("#editar-tipo-simples").hide(500);
          $("#editar-tipo-semgrau").hide(500);
          
          $("#escolha-lente-semgrau").hide(500);
          $("#escolha-lente-simples").hide(500);
          $("#escolha-lente-multifocal").hide(500);
          $("#exibe-texto-multifocal").hide(500);

          $("#escolha-tipo-plana").hide(500);
          $("#escolha-tipo-demonstrativa").hide(500);
          $("#exibe-tipo-semgrau").hide(500);
          $("#exibe-tipo-simples").hide(500);

          $("#escolha-tipo-normais").hide(500);
          $("#escolha-tipo-finas").hide(500);
          $("#escolha-tipo-finissimas").hide(500);
          $("#escolha-tipo-naosei").hide(500);

        });
    }); // fecha document ready jquery

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR TIPO 
       $("#editar-tipo-semgrau").click(function() {

          $("#exibe-lente").hide(500);
          $("#exibe-tipo-semgrau").show(500);
          $("#exibe-experimente").show(500);
          $("#exibe-comprar").hide(500);
          $("#exibe-selecionar").show(500);
          $("#editar-lente").show(500);
          
          $("#editar-tipo-semgrau").hide(500);
          $("#escolha-lente-semgrau").show(500);
          $("#escolha-lente-simples").hide(500);
          $("#escolha-lente-multifocal").hide(500);

          $("#escolha-tipo-plana").hide(500);
          $("#escolha-tipo-demonstrativa").hide(500);

          $("#escolha-tipo-normais").hide(500);
          $("#escolha-tipo-finas").hide(500);
          $("#escolha-tipo-finissimas").hide(500);
          $("#escolha-tipo-naosei").hide(500);

          $("#exibe-tipo-simples").hide(500);
          $("#exibe-texto-multifocal").hide(500);

        });
    }); // fecha document ready jquery

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR TIPO 
       $("#editar-tipo-simples").click(function() {
        
          $("#exibe-lente").hide(500);
          $("#exibe-tipo-semgrau").hide(500);
          $("#exibe-tipo-simples").show(500);
          $("#exibe-experimente").show(500);
          $("#exibe-comprar").hide(500);
          $("#exibe-selecionar").show(500);
          $("#editar-lente").show(500);
          
          $("#editar-tipo-simples").hide(500);
          $("#escolha-lente-semgrau").hide(500);
          $("#escolha-lente-simples").show(500);
          $("#escolha-lente-multifocal").hide(500);

          $("#escolha-tipo-normais").hide(500);
          $("#escolha-tipo-finas").hide(500);
          $("#escolha-tipo-finissimas").hide(500);
          $("#escolha-tipo-naosei").hide(500);
          $("#exibe-texto-multifocal").hide(500);
          

        });
    }); // fecha document ready jquery


