$(function () {
    $('#galeria img').each(function () {
        $(this).data('original', this.src)
        console.log(this.src);
    }).mouseenter(function () {
        $(this)
            .attr('src', $(this).data('hover'))
            .animate({
            marginTop: 0,
            marginRight: 0,
            marginLeft: 0,
            borderWidth: 0
        }, 'fast')
    }).mouseleave(function () {
        $(this)
            .attr('src', $(this).data('original'))
        .animate({
            marginTop: 0,
            marginRight: 0,
            marginLeft: 0,
            borderWidth: 0
        }, 'fast')
    })

})

// GUIA DE COMPRA //

$(document).ready(function(){

    $('#banner-guia').click(function(){

              // SCROLL DOWN //
              var catTopPosition = jQuery('#masculino-feminino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;
    });

});

$(document).ready(function(){

    $('#exibe-masculino').click(function(){

          if($(this).attr("class")=="show-souhomem"){
              
              $('#label-masculino').prop('checked', true);
              $(".brbr").css("display","block"); 
              $('.show-souhomem').removeClass('show-souhomem').addClass('hide-souhomem');
              $("#souhomem img.over").css("display","block"); 
              $("#souhomem img.normal").css("display","none"); 
              $("#rostos-masculino").show(100); 
              $("#perfil-masculino").show(100); 
              $("#guia-qualidades").show(100); 
              $("#rostos-feminino").hide(100); 
              $("#perfil-feminino").hide(100);

              // BLOQUEADORES WHITE //
              jQuery('#white-block-masculino').css({'display':'block','opacity': '0.5'});
              jQuery('#white-block-feminino').css({'display':'none','opacity': '0.5'});
              jQuery('#white-block-qualidades').css({'display':'block','opacity': '0.5'});

              // SCROLL DOWN //
              var catTopPosition = jQuery('#exibe-rosto-masculino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;
              
          }else if($(this).attr("class")=="hide-souhomem"){

              $('#label-masculino').prop('checked', false);
              $(".brbr").css("display","none"); 
              $('.hide-souhomem').removeClass('hide-souhomem').addClass('show-souhomem');
              $("#souhomem img.over").css("display","none"); 
              $("#souhomem img.normal").css("display","block");               
              $("#rostos-masculino").hide(100); 
              $("#perfil-masculino").hide(100); 
              $("#guia-qualidades").hide(100); 
              $("#rostos-feminino").hide(100); 
              $("#perfil-feminino").hide(100);

              // BLOQUEADORES WHITE //
              jQuery('#white-block-masculino').css({'display':'none','opacity': '0.5'});
              jQuery('#white-block-feminino').css({'display':'none','opacity': '0.5'});
              jQuery('#white-block-qualidades').css({'display':'none','opacity': '0.5'});

              // SCROLL UP //
              var catTopPosition = jQuery('#masculino-feminino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;
              
          }

    }); // fecha document ready jquery

    $('#exibe-feminino').click(function(){

          if($(this).attr("class")=="show-soumulher"){
              
              $('#label-feminino').prop('checked', true);
              $(".brbr").css("display","block"); 
              $('.show-soumulher').removeClass('show-soumulher').addClass('hide-soumulher');
              $("#soumulher img.over").css("display","block"); 
              $("#soumulher img.normal").css("display","none");               
              $("#rostos-feminino").show(100); 
              $("#perfil-feminino").show(100); 
              $("#guia-qualidades").show(100); 
              $("#rostos-masculino").hide(100); 
              $("#perfil-masculino").hide(100);

              // BLOQUEADORES WHITE //
              jQuery('#white-block-feminino').css({'display':'block','opacity': '0.5'});
              jQuery('#white-block-masculino').css({'display':'none','opacity': '0.5'});
              jQuery('#white-block-qualidades').css({'display':'block','opacity': '0.5'});

              // SCROLL DOWN //
              var catTopPosition = jQuery('#exibe-rosto-feminino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false; 

          }else if($(this).attr("class")=="hide-soumulher"){

              $('#label-feminino').prop('checked', false);
              $(".brbr").css("display","none"); 
              $('.hide-soumulher').removeClass('hide-soumulher').addClass('show-soumulher');
              $("#soumulher img.over").css("display","none"); 
              $("#soumulher img.normal").css("display","block");               
              $("#rostos-feminino").hide(100); 
              $("#perfil-feminino").hide(100); 
              $("#guia-qualidades").hide(100); 
              $("#rostos-masculino").hide(100); 
              $("#perfil-masculino").hide(100);

              // BLOQUEADORES WHITE //
              jQuery('#white-block-feminino').css({'display':'none','opacity': '0.5'});
              jQuery('#white-block-masculino').css({'display':'none','opacity': '0.5'});
              jQuery('#white-block-qualidades').css({'display':'none','opacity': '0.5'});

              // SCROLL UP //
              var catTopPosition = jQuery('#masculino-feminino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;
              
          }

    }); // fecha document ready jquery

    $('#exibe-perfil-masculino-1').click(function(){

          if($(this).attr("class")=="select-masculino-rosto-redondo"){

                $('#masculino-rosto-redondo-radio').prop('checked', true);
                $('.select-masculino-rosto-redondo').removeClass('select-masculino-rosto-redondo').addClass('hide-masculino-rosto-redondo');

                jQuery('#masculino-rosto-redondo p.descricao-rosto').css('display', 'none');
                jQuery('#masculino-rosto-redondo p.descricao-rosto-over').css('display', 'block');
                jQuery('#masculino-rosto-redondo').css({'background-color':'#000','color':'#fff'});
                jQuery('#masculino-rosto-redondo img.over').css('display', 'block');
                jQuery('#masculino-rosto-redondo img.normal').css('display', 'none');
                jQuery('#masculino-rosto-redondo p.selecione-rosto').css({'color':'#000','background-color':'#000'});
                jQuery('#white-block-masculino').css('opacity', '1');

                jQuery('#masculino-rosto-quadrado p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-quadrado img.over').css('display', 'none');
                jQuery('#masculino-rosto-quadrado img.normal').css('display', 'block');
                jQuery('#masculino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-triangular p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-triangular img.over').css('display', 'none');
                jQuery('#masculino-rosto-triangular img.normal').css('display', 'block');
                jQuery('#masculino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-oval p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-oval p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-oval img.over').css('display', 'none');
                jQuery('#masculino-rosto-oval img.normal').css('display', 'block');     
                jQuery('#masculino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});         
              
              var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false; 

          }else if($(this).attr("class")=="hide-masculino-rosto-redondo"){

              $('#masculino-rosto-redondo-radio').prop('checked', false);
              $('.hide-masculino-rosto-redondo').removeClass('hide-masculino-rosto-redondo').addClass('select-masculino-rosto-redondo');

                jQuery('#masculino-rosto-redondo p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-redondo img.over').css('display', 'none');
                jQuery('#masculino-rosto-redondo img.normal').css('display', 'block');
                jQuery('#masculino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});

                jQuery('#masculino-rosto-quadrado p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-quadrado img.over').css('display', 'none');
                jQuery('#masculino-rosto-quadrado img.normal').css('display', 'block');
                jQuery('#masculino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-triangular p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-triangular img.over').css('display', 'none');
                jQuery('#masculino-rosto-triangular img.normal').css('display', 'block');
                jQuery('#masculino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-oval p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-oval p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-oval img.over').css('display', 'none');
                jQuery('#masculino-rosto-oval img.normal').css('display', 'block');
                jQuery('#masculino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});

                jQuery('#white-block-masculino').css('opacity', '0.5');

              // SCROLL UP //
              var catTopPosition = jQuery('#rostos-masculino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;
              
          }

     }); // fecha document ready jquery


    $('#exibe-perfil-masculino-2').click(function(){

       if($(this).attr("class")=="select-masculino-rosto-quadrado"){

              $('#masculino-rosto-redondo-radio').prop('checked', true);
              $('.select-masculino-rosto-quadrado').removeClass('select-masculino-rosto-quadrado').addClass('hide-masculino-rosto-quadrado');

              jQuery('#masculino-rosto-quadrado p.descricao-rosto').css('display', 'none');
              jQuery('#masculino-rosto-quadrado p.descricao-rosto-over').css('display', 'block');
              jQuery('#masculino-rosto-quadrado').css({'background-color':'#000','color':'#fff'});
              jQuery('#masculino-rosto-quadrado img.over').css('display', 'block');
              jQuery('#masculino-rosto-quadrado img.normal').css('display', 'none');
              jQuery('#masculino-rosto-quadrado p.selecione-rosto').css({'color':'#000','background-color':'#000'});
              jQuery('#white-block-masculino').css('opacity', '1');
              
              jQuery('#masculino-rosto-redondo p.descricao-rosto').css('display', 'block');
              jQuery('#masculino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
              jQuery('#masculino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#masculino-rosto-redondo img.over').css('display', 'none');
              jQuery('#masculino-rosto-redondo img.normal').css('display', 'block');
              jQuery('#masculino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#masculino-rosto-triangular p.descricao-rosto').css('display', 'block');
              jQuery('#masculino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
              jQuery('#masculino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#masculino-rosto-triangular img.over').css('display', 'none');
              jQuery('#masculino-rosto-triangular img.normal').css('display', 'block');
              jQuery('#masculino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#masculino-rosto-oval p.descricao-rosto').css('display', 'block');
              jQuery('#masculino-rosto-oval p.descricao-rosto-over').css('display', 'none');
              jQuery('#masculino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#masculino-rosto-oval img.over').css('display', 'none');
              jQuery('#masculino-rosto-oval img.normal').css('display', 'block');
              jQuery('#masculino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false; 

          }else if($(this).attr("class")=="hide-masculino-rosto-quadrado"){

              $('#masculino-rosto-quadrado-radio').prop('checked', false);
              $('.hide-masculino-rosto-quadrado').removeClass('hide-masculino-rosto-quadrado').addClass('select-masculino-rosto-quadrado');

                jQuery('#masculino-rosto-quadrado p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-quadrado img.over').css('display', 'none');
                jQuery('#masculino-rosto-quadrado img.normal').css('display', 'block');
                jQuery('#masculino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
                jQuery('#masculino-rosto-redondo p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-redondo img.over').css('display', 'none');
                jQuery('#masculino-rosto-redondo img.normal').css('display', 'block');
                jQuery('#masculino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-triangular p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-triangular img.over').css('display', 'none');
                jQuery('#masculino-rosto-triangular img.normal').css('display', 'block');
                jQuery('#masculino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-oval p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-oval p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-oval img.over').css('display', 'none');
                jQuery('#masculino-rosto-oval img.normal').css('display', 'block');
                jQuery('#masculino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});

                jQuery('#white-block-masculino').css('opacity', '0.5');

              // SCROLL UP //
              var catTopPosition = jQuery('#rostos-masculino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;
              
          }

        });

      $('#exibe-perfil-masculino-3').click(function(){

          if($(this).attr("class")=="select-masculino-rosto-triangular"){

              $('#masculino-rosto-triangular-radio').prop('checked', true);
              $('.select-masculino-rosto-triangular').removeClass('select-masculino-rosto-triangular').addClass('hide-masculino-rosto-triangular');

              jQuery('#masculino-rosto-triangular p.descricao-rosto').css('display', 'none');
              jQuery('#masculino-rosto-triangular p.descricao-rosto-over').css('display', 'block');
              jQuery('#masculino-rosto-triangular').css({'background-color':'#000','color':'#fff'});
              jQuery('#masculino-rosto-triangular img.over').css('display', 'block');
              jQuery('#masculino-rosto-triangular img.normal').css('display', 'none');
              jQuery('#masculino-rosto-triangular p.selecione-rosto').css({'color':'#000','background-color':'#000'});
              jQuery('#white-block-masculino').css('opacity', '1');
              
              jQuery('#masculino-rosto-quadrado p.descricao-rosto').css('display', 'block');
              jQuery('#masculino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
              jQuery('#masculino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#masculino-rosto-quadrado img.over').css('display', 'none');
              jQuery('#masculino-rosto-quadrado img.normal').css('display', 'block');
              jQuery('#masculino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#masculino-rosto-redondo p.descricao-rosto').css('display', 'block');
              jQuery('#masculino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
              jQuery('#masculino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#masculino-rosto-redondo img.over').css('display', 'none');
              jQuery('#masculino-rosto-redondo img.normal').css('display', 'block');
              jQuery('#masculino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#masculino-rosto-oval p.descricao-rosto').css('display', 'block');
              jQuery('#masculino-rosto-oval p.descricao-rosto-over').css('display', 'none');
              jQuery('#masculino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#masculino-rosto-oval img.over').css('display', 'none');
              jQuery('#masculino-rosto-oval img.normal').css('display', 'block');
              jQuery('#masculino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false; 

          }else if($(this).attr("class")=="hide-masculino-rosto-triangular"){

              $('#masculino-rosto-triangular-radio').prop('checked', false);
              $('.hide-masculino-rosto-triangular').removeClass('hide-masculino-rosto-triangular').addClass('select-masculino-rosto-triangular');

                jQuery('#masculino-rosto-triangular p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-triangular img.over').css('display', 'none');
                jQuery('#masculino-rosto-triangular img.normal').css('display', 'block');
                jQuery('#masculino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
                jQuery('#masculino-rosto-quadrado p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-quadrado img.over').css('display', 'none');
                jQuery('#masculino-rosto-quadrado img.normal').css('display', 'block');
                jQuery('#masculino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-redondo p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-redondo img.over').css('display', 'none');
                jQuery('#masculino-rosto-redondo img.normal').css('display', 'block');
                jQuery('#masculino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-oval p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-oval p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-oval img.over').css('display', 'none');
                jQuery('#masculino-rosto-oval img.normal').css('display', 'block');
                jQuery('#masculino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});

                jQuery('#white-block-masculino').css('opacity', '0.5');

              // SCROLL UP //
              var catTopPosition = jQuery('#rostos-masculino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;

          }
      });
            
      $('#exibe-perfil-masculino-4').click(function(){

          if($(this).attr("class")=="select-masculino-rosto-oval"){

              $('#masculino-rosto-oval-radio').prop('checked', true);
              $('.select-masculino-rosto-oval').removeClass('select-masculino-rosto-oval').addClass('hide-masculino-rosto-oval');

              jQuery('#masculino-rosto-oval p.descricao-rosto').css('display', 'none');
              jQuery('#masculino-rosto-oval p.descricao-rosto-over').css('display', 'block');
              jQuery('#masculino-rosto-oval').css({'background-color':'#000','color':'#fff'});
              jQuery('#masculino-rosto-oval img.over').css('display', 'block');
              jQuery('#masculino-rosto-oval img.normal').css('display', 'none');
              jQuery('#masculino-rosto-oval p.selecione-rosto').css({'color':'#000','background-color':'#000'});
              jQuery('#white-block-masculino').css('opacity', '1');
              
              jQuery('#masculino-rosto-quadrado p.descricao-rosto').css('display', 'block');
              jQuery('#masculino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
              jQuery('#masculino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#masculino-rosto-quadrado img.over').css('display', 'none');
              jQuery('#masculino-rosto-quadrado img.normal').css('display', 'block');
              jQuery('#masculino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#masculino-rosto-triangular p.descricao-rosto').css('display', 'block');
              jQuery('#masculino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
              jQuery('#masculino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#masculino-rosto-triangular img.over').css('display', 'none');
              jQuery('#masculino-rosto-triangular img.normal').css('display', 'block');
              jQuery('#masculino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#masculino-rosto-redondo p.descricao-rosto').css('display', 'block');
              jQuery('#masculino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
              jQuery('#masculino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#masculino-rosto-redondo img.over').css('display', 'none');
              jQuery('#masculino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false; 

          }else if($(this).attr("class")=="hide-masculino-rosto-oval"){

              $('#masculino-rosto-oval-radio').prop('checked', false);
              $('.hide-masculino-rosto-oval').removeClass('hide-masculino-rosto-oval').addClass('select-masculino-rosto-oval');

                jQuery('#masculino-rosto-oval p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-oval p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-oval img.over').css('display', 'none');
                jQuery('#masculino-rosto-oval img.normal').css('display', 'block');
                jQuery('#masculino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-quadrado p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-quadrado img.over').css('display', 'none');
                jQuery('#masculino-rosto-quadrado img.normal').css('display', 'block');
                jQuery('#masculino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-triangular p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-triangular img.over').css('display', 'none');
                jQuery('#masculino-rosto-triangular img.normal').css('display', 'block');
                jQuery('#masculino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#masculino-rosto-redondo p.descricao-rosto').css('display', 'block');
                jQuery('#masculino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
                jQuery('#masculino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#masculino-rosto-redondo img.over').css('display', 'none');
                jQuery('#masculino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});

                jQuery('#white-block-masculino').css('opacity', '0.5');

              // SCROLL UP //
              var catTopPosition = jQuery('#rostos-masculino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;
              
          }

    }); // fecha document ready jquery

    $('#exibe-perfil-feminino-1').click(function(){

          if($(this).attr("class")=="select-feminino-rosto-redondo"){

              $('#feminino-rosto-redondo-radio').prop('checked', true);
              $('.select-feminino-rosto-redondo').removeClass('select-feminino-rosto-redondo').addClass('hide-feminino-rosto-redondo');

                jQuery('#feminino-rosto-redondo p.descricao-rosto').css('display', 'none');
                jQuery('#feminino-rosto-redondo p.descricao-rosto-over').css('display', 'block');
                jQuery('#feminino-rosto-redondo').css({'background-color':'#000','color':'#fff'});
                jQuery('#feminino-rosto-redondo img.over').css('display', 'block');
                jQuery('#feminino-rosto-redondo img.normal').css('display', 'none');
                jQuery('#feminino-rosto-redondo p.selecione-rosto').css({'color':'#000','background-color':'#000'});
                jQuery('#white-block-feminino').css('opacity', '1');

                jQuery('#feminino-rosto-quadrado p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-quadrado img.over').css('display', 'none');
                jQuery('#feminino-rosto-quadrado img.normal').css('display', 'block');
                jQuery('#feminino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-triangular p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-triangular img.over').css('display', 'none');
                jQuery('#feminino-rosto-triangular img.normal').css('display', 'block');
                jQuery('#feminino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-oval p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-oval p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-oval img.over').css('display', 'none');
                jQuery('#feminino-rosto-oval img.normal').css('display', 'block');     
                jQuery('#feminino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});         
              
              var catTopPosition = jQuery('#exibe-qualidades-2').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false; 

          }else if($(this).attr("class")=="hide-feminino-rosto-redondo"){

              $('#feminino-rosto-quadrado-radio').prop('checked', false);
              $('.hide-feminino-rosto-redondo').removeClass('hide-feminino-rosto-redondo').addClass('select-feminino-rosto-redondo');

                jQuery('#feminino-rosto-redondo p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-redondo img.over').css('display', 'none');
                jQuery('#feminino-rosto-redondo img.normal').css('display', 'block');
                jQuery('#feminino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});

                jQuery('#feminino-rosto-quadrado p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-quadrado img.over').css('display', 'none');
                jQuery('#feminino-rosto-quadrado img.normal').css('display', 'block');
                jQuery('#feminino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-triangular p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-triangular img.over').css('display', 'none');
                jQuery('#feminino-rosto-triangular img.normal').css('display', 'block');
                jQuery('#feminino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-oval p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-oval p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-oval img.over').css('display', 'none');
                jQuery('#feminino-rosto-oval img.normal').css('display', 'block');
                jQuery('#feminino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});

                jQuery('#white-block-feminino').css('opacity', '0.5');

              // SCROLL UP //
              var catTopPosition = jQuery('#rostos-feminino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;
              
          }

     }); // fecha document ready jquery


    $('#exibe-perfil-feminino-2').click(function(){

       if($(this).attr("class")=="select-feminino-rosto-quadrado"){

              $('#feminino-rosto-quadrado-radio').prop('checked', true);
              $('.select-feminino-rosto-quadrado').removeClass('select-feminino-rosto-quadrado').addClass('hide-feminino-rosto-quadrado');

              jQuery('#feminino-rosto-quadrado p.descricao-rosto').css('display', 'none');
              jQuery('#feminino-rosto-quadrado p.descricao-rosto-over').css('display', 'block');
              jQuery('#feminino-rosto-quadrado').css({'background-color':'#000','color':'#fff'});
              jQuery('#feminino-rosto-quadrado img.over').css('display', 'block');
              jQuery('#feminino-rosto-quadrado img.normal').css('display', 'none');
              jQuery('#feminino-rosto-quadrado p.selecione-rosto').css({'color':'#000','background-color':'#000'});
              jQuery('#white-block-feminino').css('opacity', '1');
              
              jQuery('#feminino-rosto-redondo p.descricao-rosto').css('display', 'block');
              jQuery('#feminino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
              jQuery('#feminino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#feminino-rosto-redondo img.over').css('display', 'none');
              jQuery('#feminino-rosto-redondo img.normal').css('display', 'block');
              jQuery('#feminino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#feminino-rosto-triangular p.descricao-rosto').css('display', 'block');
              jQuery('#feminino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
              jQuery('#feminino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#feminino-rosto-triangular img.over').css('display', 'none');
              jQuery('#feminino-rosto-triangular img.normal').css('display', 'block');
              jQuery('#feminino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#feminino-rosto-oval p.descricao-rosto').css('display', 'block');
              jQuery('#feminino-rosto-oval p.descricao-rosto-over').css('display', 'none');
              jQuery('#feminino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#feminino-rosto-oval img.over').css('display', 'none');
              jQuery('#feminino-rosto-oval img.normal').css('display', 'block');
              jQuery('#feminino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              var catTopPosition = jQuery('#exibe-qualidades-2').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false; 

          }else if($(this).attr("class")=="hide-feminino-rosto-quadrado"){

              $('#feminino-rosto-quadrado-radio').prop('checked', false);
              $('.hide-feminino-rosto-quadrado').removeClass('hide-feminino-rosto-quadrado').addClass('select-feminino-rosto-quadrado');

                jQuery('#feminino-rosto-quadrado p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-quadrado img.over').css('display', 'none');
                jQuery('#feminino-rosto-quadrado img.normal').css('display', 'block');
                jQuery('#feminino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
                jQuery('#feminino-rosto-redondo p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-redondo img.over').css('display', 'none');
                jQuery('#feminino-rosto-redondo img.normal').css('display', 'block');
                jQuery('#feminino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-triangular p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-triangular img.over').css('display', 'none');
                jQuery('#feminino-rosto-triangular img.normal').css('display', 'block');
                jQuery('#feminino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-oval p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-oval p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-oval img.over').css('display', 'none');
                jQuery('#feminino-rosto-oval img.normal').css('display', 'block');
                jQuery('#feminino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});

                jQuery('#white-block-feminino').css('opacity', '0.5');

              // SCROLL UP //
              var catTopPosition = jQuery('#rostos-feminino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;
              
          }

        });

      $('#exibe-perfil-feminino-3').click(function(){

          if($(this).attr("class")=="select-feminino-rosto-triangular"){

              $('#feminino-rosto-triangular-radio').prop('checked', true);
              $('.select-feminino-rosto-triangular').removeClass('select-feminino-rosto-triangular').addClass('hide-feminino-rosto-triangular');

              jQuery('#feminino-rosto-triangular p.descricao-rosto').css('display', 'none');
              jQuery('#feminino-rosto-triangular p.descricao-rosto-over').css('display', 'block');
              jQuery('#feminino-rosto-triangular').css({'background-color':'#000','color':'#fff'});
              jQuery('#feminino-rosto-triangular img.over').css('display', 'block');
              jQuery('#feminino-rosto-triangular img.normal').css('display', 'none');
              jQuery('#feminino-rosto-triangular p.selecione-rosto').css({'color':'#000','background-color':'#000'});
              jQuery('#white-block-feminino').css('opacity', '1');
              
              jQuery('#feminino-rosto-quadrado p.descricao-rosto').css('display', 'block');
              jQuery('#feminino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
              jQuery('#feminino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#feminino-rosto-quadrado img.over').css('display', 'none');
              jQuery('#feminino-rosto-quadrado img.normal').css('display', 'block');
              jQuery('#feminino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#feminino-rosto-redondo p.descricao-rosto').css('display', 'block');
              jQuery('#feminino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
              jQuery('#feminino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#feminino-rosto-redondo img.over').css('display', 'none');
              jQuery('#feminino-rosto-redondo img.normal').css('display', 'block');
              jQuery('#feminino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#feminino-rosto-oval p.descricao-rosto').css('display', 'block');
              jQuery('#feminino-rosto-oval p.descricao-rosto-over').css('display', 'none');
              jQuery('#feminino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#feminino-rosto-oval img.over').css('display', 'none');
              jQuery('#feminino-rosto-oval img.normal').css('display', 'block');
              jQuery('#feminino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              var catTopPosition = jQuery('#exibe-qualidades-2').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false; 

          }else if($(this).attr("class")=="hide-feminino-rosto-triangular"){

              $('#feminino-rosto-trinagular-radio').prop('checked', false);
              $('.hide-feminino-rosto-triangular').removeClass('hide-feminino-rosto-triangular').addClass('select-feminino-rosto-triangular');

                jQuery('#feminino-rosto-triangular p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-triangular img.over').css('display', 'none');
                jQuery('#feminino-rosto-triangular img.normal').css('display', 'block');
                jQuery('#feminino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
                jQuery('#feminino-rosto-quadrado p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-quadrado img.over').css('display', 'none');
                jQuery('#feminino-rosto-quadrado img.normal').css('display', 'block');
                jQuery('#feminino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-redondo p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-redondo img.over').css('display', 'none');
                jQuery('#feminino-rosto-redondo img.normal').css('display', 'block');
                jQuery('#feminino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-oval p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-oval p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-oval img.over').css('display', 'none');
                jQuery('#feminino-rosto-oval img.normal').css('display', 'block');
                jQuery('#feminino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});

                jQuery('#white-block-feminino').css('opacity', '0.5');

              // SCROLL UP //
              var catTopPosition = jQuery('#rostos-feminino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;

          }
      });
            
      $('#exibe-perfil-feminino-4').click(function(){

          if($(this).attr("class")=="select-feminino-rosto-oval"){

              $('#feminino-rosto-oval-radio').prop('checked', true);
              $('.select-feminino-rosto-oval').removeClass('select-feminino-rosto-oval').addClass('hide-feminino-rosto-oval');

              jQuery('#feminino-rosto-oval p.descricao-rosto').css('display', 'none');
              jQuery('#feminino-rosto-oval p.descricao-rosto-over').css('display', 'block');
              jQuery('#feminino-rosto-oval').css({'background-color':'#000','color':'#fff'});
              jQuery('#feminino-rosto-oval img.over').css('display', 'block');
              jQuery('#feminino-rosto-oval img.normal').css('display', 'none');
              jQuery('#feminino-rosto-oval p.selecione-rosto').css({'color':'#000','background-color':'#000'});
              jQuery('#white-block-feminino').css('opacity', '1');
              
              jQuery('#feminino-rosto-quadrado p.descricao-rosto').css('display', 'block');
              jQuery('#feminino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
              jQuery('#feminino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#feminino-rosto-quadrado img.over').css('display', 'none');
              jQuery('#feminino-rosto-quadrado img.normal').css('display', 'block');
              jQuery('#feminino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#feminino-rosto-triangular p.descricao-rosto').css('display', 'block');
              jQuery('#feminino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
              jQuery('#feminino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#feminino-rosto-triangular img.over').css('display', 'none');
              jQuery('#feminino-rosto-triangular img.normal').css('display', 'block');
              jQuery('#feminino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              jQuery('#feminino-rosto-redondo p.descricao-rosto').css('display', 'block');
              jQuery('#feminino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
              jQuery('#feminino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
              jQuery('#feminino-rosto-redondo img.over').css('display', 'none');
              jQuery('#feminino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
              
              var catTopPosition = jQuery('#exibe-qualidades-2').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false; 

          }else if($(this).attr("class")=="hide-feminino-rosto-oval"){


              $('#feminino-rosto-oval-radio').prop('checked', false);
              $('.hide-feminino-rosto-oval').removeClass('hide-feminino-rosto-oval').addClass('select-feminino-rosto-oval');

                jQuery('#feminino-rosto-oval p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-oval p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-oval').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-oval img.over').css('display', 'none');
                jQuery('#feminino-rosto-oval img.normal').css('display', 'block');
                jQuery('#feminino-rosto-oval p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-quadrado p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-quadrado p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-quadrado').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-quadrado img.over').css('display', 'none');
                jQuery('#feminino-rosto-quadrado img.normal').css('display', 'block');
                jQuery('#feminino-rosto-quadrado p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-triangular p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-triangular p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-triangular').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-triangular img.over').css('display', 'none');
                jQuery('#feminino-rosto-triangular img.normal').css('display', 'block');
                jQuery('#feminino-rosto-triangular p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});
                
                jQuery('#feminino-rosto-redondo p.descricao-rosto').css('display', 'block');
                jQuery('#feminino-rosto-redondo p.descricao-rosto-over').css('display', 'none');
                jQuery('#feminino-rosto-redondo').css({'background-color':'#F8F8F8','color':'#000'});
                jQuery('#feminino-rosto-redondo img.over').css('display', 'none');
                jQuery('#feminino-rosto-redondo p.selecione-rosto').css({'color':'#FFF','background-color':'#77A403'});

                jQuery('#white-block-feminino').css('opacity', '0.5');

              // SCROLL UP //
              var catTopPosition = jQuery('#rostos-feminino').offset().top-30;
              jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
              return false;
              
          }

    }); // fecha document ready jquery

    $('#perfil-masculino-1').click(function(){

        if($(this).attr("class")=="select-perfil-masculino-esportista"){
            
            $('#masculino-esportista').prop('checked', true);
            $("#botao-final").css("display","block"); 

            $('.select-perfil-masculino-esportista').removeClass('select-perfil-masculino-esportista').addClass('hide-perfil-masculino-esportista');
            $("#perfil-masculino-esportista img.over").css("display","block"); 
            $("#perfil-masculino-esportista img.normal").css("display","none"); 

            $("#perfil-masculino-sofisticado img.over").css("display","none"); 
            $("#perfil-masculino-sofisticado img.normal").css("display","block"); 

            $("#perfil-masculino-cosmopolita img.over").css("display","none"); 
            $("#perfil-masculino-cosmopolita img.normal").css("display","block"); 

            $("#perfil-masculino-kids img.over").css("display","none"); 
            $("#perfil-masculino-kids img.normal").css("display","block"); 

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '1'});

            // SCROLL DOWN //
            var catTopPosition = jQuery('#guia-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }else if($(this).attr("class")=="hide-perfil-masculino-esportista"){

            $('#masculino-esportista').prop('checked', false);
            $("#botao-final").css("display","none"); 

            $('.hide-perfil-masculino-esportista').removeClass('hide-perfil-masculino-esportista').addClass('select-perfil-masculino-esportista');
            $("#perfil-masculino-kids img.over").css("display","none"); 
            $("#perfil-masculino-kids img.normal").css("display","block");     

            $("#perfil-masculino-esportista img.over").css("display","none"); 
            $("#perfil-masculino-esportista img.normal").css("display","block");   

            $("#perfil-masculino-cosmopolita img.over").css("display","none"); 
            $("#perfil-masculino-cosmopolita img.normal").css("display","block");   

            $("#perfil-masculino-sofisticado img.over").css("display","none"); 
            $("#perfil-masculino-sofisticado img.normal").css("display","block");                 

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '0.5'});

            // SCROLL UP //
            var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }

    }); // fecha document ready jquery

    $('#perfil-masculino-2').click(function(){

        if($(this).attr("class")=="select-perfil-masculino-sofisticado"){
            
            $('#masculino-sofisticado').prop('checked', true);
            $("#botao-final").css("display","block"); 

            $('.select-perfil-masculino-sofisticado').removeClass('select-perfil-masculino-sofisticado').addClass('hide-perfil-masculino-sofisticado');
            $("#perfil-masculino-sofisticado img.over").css("display","block"); 
            $("#perfil-masculino-sofisticado img.normal").css("display","none"); 

            $("#perfil-masculino-cosmopolita img.over").css("display","none"); 
            $("#perfil-masculino-cosmopolita img.normal").css("display","block"); 

            $("#perfil-masculino-esportista img.over").css("display","none"); 
            $("#perfil-masculino-esportista img.normal").css("display","block"); 

            $("#perfil-masculino-kids img.over").css("display","none"); 
            $("#perfil-masculino-kids img.normal").css("display","block");            

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '1'});

            // SCROLL DOWN //
            var catTopPosition = jQuery('#guia-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }else if($(this).attr("class")=="hide-perfil-masculino-sofisticado"){

            $('#masculino-sofisticado').prop('checked', false);
            $("#botao-final").css("display","none"); 

            $('.hide-perfil-masculino-sofisticado').removeClass('hide-perfil-masculino-sofisticado').addClass('select-perfil-masculino-sofisticado');
            $("#perfil-masculino-kids img.over").css("display","none"); 
            $("#perfil-masculino-kids img.normal").css("display","block");     

            $("#perfil-masculino-esportista img.over").css("display","none"); 
            $("#perfil-masculino-esportista img.normal").css("display","block");   

            $("#perfil-masculino-cosmopolita img.over").css("display","none"); 
            $("#perfil-masculino-cosmopolita img.normal").css("display","block");   

            $("#perfil-masculino-sofisticado img.over").css("display","none"); 
            $("#perfil-masculino-sofisticado img.normal").css("display","block");                

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '0.5'});

            // SCROLL UP //
            var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }

    }); // fecha document ready jquery

    $('#perfil-masculino-3').click(function(){

        if($(this).attr("class")=="select-perfil-masculino-cosmopolita"){
            
            $('#masculino-cosmopolita').prop('checked', true);
            $("#botao-final").css("display","block"); 

            $('.select-perfil-masculino-cosmopolita').removeClass('select-perfil-masculino-cosmopolita').addClass('hide-perfil-masculino-cosmopolita');
            $("#perfil-masculino-cosmopolita img.over").css("display","block"); 
            $("#perfil-masculino-cosmopolita img.normal").css("display","none"); 

            $("#perfil-masculino-sofisticado img.over").css("display","none"); 
            $("#perfil-masculino-sofisticado img.normal").css("display","block"); 

            $("#perfil-masculino-esportista img.over").css("display","none"); 
            $("#perfil-masculino-esportista img.normal").css("display","block"); 

            $("#perfil-masculino-kids img.over").css("display","none"); 
            $("#perfil-masculino-kids img.normal").css("display","block");

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '1'});

            // SCROLL DOWN //
            var catTopPosition = jQuery('#guia-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }else if($(this).attr("class")=="hide-perfil-masculino-cosmopolita"){

            $('#masculino-cosmopolita').prop('checked', false);
            $("#botao-final").css("display","none"); 

            $('.hide-perfil-masculino-cosmopolita').removeClass('hide-perfil-masculino-cosmopolita').addClass('select-perfil-masculino-cosmopolita');
            $("#perfil-masculino-kids img.over").css("display","none"); 
            $("#perfil-masculino-kids img.normal").css("display","block");     

            $("#perfil-masculino-esportista img.over").css("display","none"); 
            $("#perfil-masculino-esportista img.normal").css("display","block");   

            $("#perfil-masculino-cosmopolita img.over").css("display","none"); 
            $("#perfil-masculino-cosmopolita img.normal").css("display","block");   

            $("#perfil-masculino-sofisticado img.over").css("display","none"); 
            $("#perfil-masculino-sofisticado img.normal").css("display","block");               

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '0.5'});

            // SCROLL UP //
            var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }

    }); // fecha document ready jquery

    $('#perfil-masculino-4').click(function(){

        if($(this).attr("class")=="select-perfil-masculino-kids"){

            $('#masculino-kids').prop('checked', true);
            $("#botao-final").css("display","block"); 

            $('.select-perfil-masculino-kids').removeClass('select-perfil-masculino-kids').addClass('hide-perfil-masculino-kids');
            $("#perfil-masculino-kids img.over").css("display","block"); 
            $("#perfil-masculino-kids img.normal").css("display","none"); 

            $("#perfil-masculino-cosmopolita img.over").css("display","none"); 
            $("#perfil-masculino-cosmopolita img.normal").css("display","block"); 

            $("#perfil-masculino-esportista img.over").css("display","none"); 
            $("#perfil-masculino-esportista img.normal").css("display","block"); 

            $("#perfil-masculino-sofisticado img.over").css("display","none"); 
            $("#perfil-masculino-sofisticado img.normal").css("display","block"); 

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '1'});

            // SCROLL DOWN //
            var catTopPosition = jQuery('#guia-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }else if($(this).attr("class")=="hide-perfil-masculino-kids"){

            $('#masculino-kids').prop('checked', false);
            $("#botao-final").css("display","none"); 

            $('.hide-perfil-masculino-kids').removeClass('hide-perfil-masculino-kids').addClass('select-perfil-masculino-kids');
            $("#perfil-masculino-kids img.over").css("display","none"); 
            $("#perfil-masculino-kids img.normal").css("display","block");     

            $("#perfil-masculino-esportista img.over").css("display","none"); 
            $("#perfil-masculino-esportista img.normal").css("display","block");   

            $("#perfil-masculino-cosmopolita img.over").css("display","none"); 
            $("#perfil-masculino-cosmopolita img.normal").css("display","block");   

            $("#perfil-masculino-sofisticado img.over").css("display","none"); 
            $("#perfil-masculino-sofisticado img.normal").css("display","block");                                                 

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '0.5'});

            // SCROLL UP //
            var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }

    }); // fecha document ready jquery

    $('#perfil-feminino-1').click(function(){

        if($(this).attr("class")=="select-perfil-feminino-esportista"){
            
            $('#feminino-esportista').prop('checked', true);
            $("#botao-final").css("display","block"); 

            $('.select-perfil-feminino-esportista').removeClass('select-perfil-feminino-esportista').addClass('hide-perfil-feminino-esportista');
            $("#perfil-feminino-esportista img.over").css("display","block"); 
            $("#perfil-feminino-esportista img.normal").css("display","none"); 

            $("#perfil-feminino-sofisticado img.over").css("display","none"); 
            $("#perfil-feminino-sofisticado img.normal").css("display","block"); 

            $("#perfil-feminino-cosmopolita img.over").css("display","none"); 
            $("#perfil-feminino-cosmopolita img.normal").css("display","block"); 

            $("#perfil-feminino-kids img.over").css("display","none"); 
            $("#perfil-feminino-kids img.normal").css("display","block"); 

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '1'});

            // SCROLL DOWN //
            var catTopPosition = jQuery('#guia-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }else if($(this).attr("class")=="hide-perfil-feminino-esportista"){

            $('#feminino-esportista').prop('checked', false);
            $("#botao-final").css("display","none"); 

            $('.hide-perfil-feminino-esportista').removeClass('hide-perfil-feminino-esportista').addClass('select-perfil-feminino-esportista');
            $("#perfil-feminino-kids img.over").css("display","none"); 
            $("#perfil-feminino-kids img.normal").css("display","block");     

            $("#perfil-feminino-esportista img.over").css("display","none"); 
            $("#perfil-feminino-esportista img.normal").css("display","block");   

            $("#perfil-feminino-cosmopolita img.over").css("display","none"); 
            $("#perfil-feminino-cosmopolita img.normal").css("display","block");   

            $("#perfil-feminino-sofisticado img.over").css("display","none"); 
            $("#perfil-feminino-sofisticado img.normal").css("display","block");                 

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '0.5'});

            // SCROLL UP //
            var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }

    }); // fecha document ready jquery

    $('#perfil-feminino-2').click(function(){

        if($(this).attr("class")=="select-perfil-feminino-sofisticado"){
            
            $('#feminino-sofisticado').prop('checked', true);
            $("#botao-final").css("display","block"); 

            $('.select-perfil-feminino-sofisticado').removeClass('select-perfil-feminino-sofisticado').addClass('hide-perfil-feminino-sofisticado');
            $("#perfil-feminino-sofisticado img.over").css("display","block"); 
            $("#perfil-feminino-sofisticado img.normal").css("display","none"); 

            $("#perfil-feminino-cosmopolita img.over").css("display","none"); 
            $("#perfil-feminino-cosmopolita img.normal").css("display","block"); 

            $("#perfil-feminino-esportista img.over").css("display","none"); 
            $("#perfil-feminino-esportista img.normal").css("display","block"); 

            $("#perfil-feminino-kids img.over").css("display","none"); 
            $("#perfil-feminino-kids img.normal").css("display","block");            

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '1'});

            // SCROLL DOWN //
            var catTopPosition = jQuery('#guia-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }else if($(this).attr("class")=="hide-perfil-feminino-sofisticado"){

            $('#feminino-sofisticado').prop('checked', false);
            $("#botao-final").css("display","none"); 

            $('.hide-perfil-feminino-sofisticado').removeClass('hide-perfil-feminino-sofisticado').addClass('select-perfil-feminino-sofisticado');
            $("#perfil-feminino-kids img.over").css("display","none"); 
            $("#perfil-feminino-kids img.normal").css("display","block");     

            $("#perfil-feminino-esportista img.over").css("display","none"); 
            $("#perfil-feminino-esportista img.normal").css("display","block");   

            $("#perfil-feminino-cosmopolita img.over").css("display","none"); 
            $("#perfil-feminino-cosmopolita img.normal").css("display","block");   

            $("#perfil-feminino-sofisticado img.over").css("display","none"); 
            $("#perfil-feminino-sofisticado img.normal").css("display","block");                

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '0.5'});

            // SCROLL UP //
            var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }

    }); // fecha document ready jquery

    $('#perfil-feminino-3').click(function(){

        if($(this).attr("class")=="select-perfil-feminino-cosmopolita"){
            
            $('#feminino-cosmopolita').prop('checked', true);
            $("#botao-final").css("display","block"); 

            $('.select-perfil-feminino-cosmopolita').removeClass('select-perfil-feminino-cosmopolita').addClass('hide-perfil-feminino-cosmopolita');
            $("#perfil-feminino-cosmopolita img.over").css("display","block"); 
            $("#perfil-feminino-cosmopolita img.normal").css("display","none"); 

            $("#perfil-feminino-sofisticado img.over").css("display","none"); 
            $("#perfil-feminino-sofisticado img.normal").css("display","block"); 

            $("#perfil-feminino-esportista img.over").css("display","none"); 
            $("#perfil-feminino-esportista img.normal").css("display","block"); 

            $("#perfil-feminino-kids img.over").css("display","none"); 
            $("#perfil-feminino-kids img.normal").css("display","block");

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '1'});

            // SCROLL DOWN //
            var catTopPosition = jQuery('#guia-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }else if($(this).attr("class")=="hide-perfil-feminino-cosmopolita"){

            $('#feminino-cosmopolita').prop('checked', false);
            $("#botao-final").css("display","none"); 

            $('.hide-perfil-feminino-cosmopolita').removeClass('hide-perfil-feminino-cosmopolita').addClass('select-perfil-feminino-cosmopolita');
            $("#perfil-feminino-kids img.over").css("display","none"); 
            $("#perfil-feminino-kids img.normal").css("display","block");     

            $("#perfil-feminino-esportista img.over").css("display","none"); 
            $("#perfil-feminino-esportista img.normal").css("display","block");   

            $("#perfil-feminino-cosmopolita img.over").css("display","none"); 
            $("#perfil-feminino-cosmopolita img.normal").css("display","block");   

            $("#perfil-feminino-sofisticado img.over").css("display","none"); 
            $("#perfil-feminino-sofisticado img.normal").css("display","block");               

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '0.5'});

            // SCROLL UP //
            var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }

    }); // fecha document ready jquery

    $('#perfil-feminino-4').click(function(){

        if($(this).attr("class")=="select-perfil-feminino-kids"){

            $('#feminino-kids').prop('checked', true);
            $("#botao-final").css("display","block"); 

            $('.select-perfil-feminino-kids').removeClass('select-perfil-feminino-kids').addClass('hide-perfil-feminino-kids');
            $("#perfil-feminino-kids img.over").css("display","block"); 
            $("#perfil-feminino-kids img.normal").css("display","none"); 

            $("#perfil-feminino-cosmopolita img.over").css("display","none"); 
            $("#perfil-feminino-cosmopolita img.normal").css("display","block"); 

            $("#perfil-feminino-esportista img.over").css("display","none"); 
            $("#perfil-feminino-esportista img.normal").css("display","block"); 

            $("#perfil-feminino-sofisticado img.over").css("display","none"); 
            $("#perfil-feminino-sofisticado img.normal").css("display","block"); 

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '1'});

            // SCROLL DOWN //
            var catTopPosition = jQuery('#guia-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }else if($(this).attr("class")=="hide-perfil-feminino-kids"){

            $('#feminino-kids').prop('checked', false);
            $("#botao-final").css("display","none"); 

            $('.hide-perfil-feminino-kids').removeClass('hide-perfil-feminino-kids').addClass('select-perfil-feminino-kids');
            $("#perfil-feminino-kids img.over").css("display","none"); 
            $("#perfil-feminino-kids img.normal").css("display","block");     

            $("#perfil-feminino-esportista img.over").css("display","none"); 
            $("#perfil-feminino-esportista img.normal").css("display","block");   

            $("#perfil-feminino-cosmopolita img.over").css("display","none"); 
            $("#perfil-feminino-cosmopolita img.normal").css("display","block");   

            $("#perfil-feminino-sofisticado img.over").css("display","none"); 
            $("#perfil-feminino-sofisticado img.normal").css("display","block");                                                 

            // BLOQUEADORES WHITE //
            jQuery('#white-block-qualidades').css({'display':'block','opacity': '0.5'});

            // SCROLL UP //
            var catTopPosition = jQuery('#exibe-qualidades').offset().top-30;
            jQuery('html, body').animate({scrollTop:catTopPosition}, 'slow');
            return false;
            
        }

    }); // fecha document ready jquery

    $('#item-1').click(function(){

          if($(this).attr("class")=="show-item-qualidade"){
              
              $('#item-1').removeClass('show-item-qualidade').addClass('hide-item-qualidade');
              $("#item-1 img.over").css("display","block"); 
              $("#item-1 img.normal").css("display","none");

              
          }else if($(this).attr("class")=="hide-item-qualidade"){

              $('#item-1').removeClass('hide-item-qualidade').addClass('show-item-qualidade');
              $("#item-1 img.over").css("display","none"); 
              $("#item-1 img.normal").css("display","block");               
              
          }

    }); // fecha document ready jquery

    $('#item-2').click(function(){

          if($(this).attr("class")=="show-item-qualidade"){
              
              $('#item-2').removeClass('show-item-qualidade').addClass('hide-item-qualidade');
              $("#item-2 img.over").css("display","block"); 
              $("#item-2 img.normal").css("display","none"); 
              
          }else if($(this).attr("class")=="hide-item-qualidade"){

              $('#item-2').removeClass('hide-item-qualidade').addClass('show-item-qualidade');
              $("#item-2 img.over").css("display","none"); 
              $("#item-2 img.normal").css("display","block");               
              
          }

    }); // fecha document ready jquery

       $('#item-3').click(function(){

          if($(this).attr("class")=="show-item-qualidade"){
              
              $('#item-3').removeClass('show-item-qualidade').addClass('hide-item-qualidade');
              $("#item-3 img.over").css("display","block"); 
              $("#item-3 img.normal").css("display","none"); 
              
          }else if($(this).attr("class")=="hide-item-qualidade"){

              $('#item-3').removeClass('hide-item-qualidade').addClass('show-item-qualidade');
              $("#item-3 img.over").css("display","none"); 
              $("#item-3 img.normal").css("display","block");               
              
          }

    }); // fecha document ready jquery

    $('#item-4').click(function(){

          if($(this).attr("class")=="show-item-qualidade"){
              
              $('#item-4').removeClass('show-item-qualidade').addClass('hide-item-qualidade');
              $("#item-4 img.over").css("display","block"); 
              $("#item-4 img.normal").css("display","none"); 
              
          }else if($(this).attr("class")=="hide-item-qualidade"){

              $('#item-4').removeClass('hide-item-qualidade').addClass('show-item-qualidade');
              $("#item-4 img.over").css("display","none"); 
              $("#item-4 img.normal").css("display","block");               
              
          }

    }); // fecha document ready jquery            

    $('#item-5').click(function(){

          if($(this).attr("class")=="show-item-qualidade"){
              
              $('#item-5').removeClass('show-item-qualidade').addClass('hide-item-qualidade');
              $("#item-5 img.over").css("display","block"); 
              $("#item-5 img.normal").css("display","none"); 
              
          }else if($(this).attr("class")=="hide-item-qualidade"){

              $('#item-5').removeClass('hide-item-qualidade').addClass('show-item-qualidade');
              $("#item-5 img.over").css("display","none"); 
              $("#item-5 img.normal").css("display","block");               
              
          }

    }); // fecha document ready jquery            

}); // fecha document ready jquery

$(document).ready(function(){

  $('#botaozin').click(function(){
     $(this).css("display","none");
  });

});

jQuery(document).ready(function($){
    $('#guia-de-compra').submit(function(){
      jQuery.ajax({
      type: "POST",
      url: "guia-resultado",
      async: false,
      data:$('#guia-de-compra').serialize(), 
            success: function(response) {
                $('#guia-de-compra').find('.form_result').html(response);
            }
        }); //jquery ajax

        $("#guia-de-compra").unbind('submit');
        return false;

    }); // estado on change
}); // jquery document ready

// $(document).ready(function(){
//         $("#guia-de-compra").submit(function() {
//         $.ajax(
//           {
//             type:'POST', 
//             url: '{{ URL::to("") }}', 
            

//             /* Add this line */
//             $("#guia-de-compra").unbind('submit');
//             return false;
//         });
//     });

// function submitForm() {
//     $.ajax({type:'POST', url: 'guia-resultado', data:$('#guia-de-compra').serialize(), success: function(response) {
//         $('#guia-de-compra').find('.form_result').html(response);
//     }});

//     $("#guia-de-compra").unbind('submit');

//     // $("#loading").css("display","none");
//     $("#botaozin").val("ENCONTRAR NOVAMENTE");
//     return false;
// }



jQuery(document).ready(function($){
   $(".the_faces-kizo-feminino").each(function(){
          $(this).on("mouseover", function(){
            $("#image-all-kizo-feminino").hide();
            $("#image-kizo-feminino-"+$(this).attr("data-number")).show();
          }).on("mouseout",function(){
            $("#image-kizo-feminino-"+$(this).attr("data-number")).hide();
          });
        });
        $("#face-area-kizo-feminino").on("mouseleave", function(){
          $("#image-all-kizo-feminino").show();
        });
        $("#face-area-kizo-feminino").on("mouseenter", function(){
          $("#image-all-kizo-feminino").hide();
        });
  });

jQuery(document).ready(function($){
   $(".the_faces-kizo-masculino").each(function(){
          $(this).on("mouseover", function(){
            $("#image-all-kizo-masculino").hide();
            $("#image-kizo-masculino-"+$(this).attr("data-number")).show();
          }).on("mouseout",function(){
            $("#image-kizo-masculino-"+$(this).attr("data-number")).hide();
          });
        });
        $("#face-area-kizo-masculino").on("mouseleave", function(){
          $("#image-all-kizo-masculino").show();
        });
        $("#face-area-kizo-masculino").on("mouseenter", function(){
          $("#image-all-kizo-masculino").hide();
        });
  });

jQuery(document).ready(function($){
   $(".the_faces").each(function(){
          $(this).on("mouseover", function(){
            $("#image-all").hide();
            $("#image-"+$(this).attr("data-number")).show();
          }).on("mouseout",function(){
            $("#image-"+$(this).attr("data-number")).hide();
          });
        });
        $("#face-area").on("mouseleave", function(){
          $("#image-all").show();
        });
        $("#face-area").on("mouseenter", function(){
          $("#image-all").hide();
        });
  });

 $(function() {

    $(".galeria").click(function() {

    var image = $(this).attr("rel");
    var classes = $(this).attr("class");
    
    /*if(classes == "galeria galeria-PR_01_01"){

    	var hover = "<?php echo URL::asset('img/produtos/detalhes/medidas_pr_01.png') ?>";

    	$('#galeria').hide();
    	$('#galeria').fadeIn(800);
    	$('#galeria').html('<img src="' + image + '" data-hover="' + hover + '" class="galeria imagem-grande "  />');

    }else { */

    	$('#galeria').hide();
    	$('#galeria').fadeIn(800);
    	$('#galeria').html('<img src="' + image + '" class="imagem-grande img-responsive " />');	
    
    /*} */
    
    return false;
      });
    });

    $(document).ready(function(){
      
      $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
      // ESCONDE TODOS DIVS INATIVOS

      // DIV DE EXIBIÇÃO
      $("#exibe-tipo").hide();
      $("#exibe-lente").hide();
      $("#exibe-tipo-semgrau").hide();
      $("#exibe-tipo-simples").hide();
      $("#exibe-comprar").hide();
      $("#exibe-selecionar").hide();
      $("#exibe-experimente").hide();
      $("#exibe-texto-multifocal").hide();

      // LINKS EDITAR
      $("#editar-lente").hide();
      $("#editar-tipo-semgrau").hide();
      $("#editar-tipo-simples").hide();

      // DIVS DE ESCOLHA LENTE
      $("#escolha-lente-semgrau").hide();
      $("#escolha-lente-simples").hide();
      $("#escolha-lente-multifocal").hide();

      // DIVS DE ESCOLHA TIPO
      $("#escolha-tipo-plana").hide();
      $("#escolha-tipo-demonstrativa").hide();
      $("#escolha-tipo-normais").hide();
      $("#escolha-tipo-finas").hide();
      $("#escolha-tipo-finissimas").hide();
      $("#escolha-tipo-naosei").hide();

        $('input[type="radio"]').click(function(){

            // BLOCO DE FUNÇÕES PARA LENTE

            // LENTE SEMGRAU
            if($(this).attr("value")=="semgrau"){
                
                $("#exibe-lente").hide(500);

                $("#editar-lente").show(500);

                $("#escolha-lente-semgrau").show(500);
                $("#escolha-lente-simples").hide(500);
                
                $("#exibe-tipo-semgrau").show(500);
                $("#exibe-tipo-simples").hide(500);

            }

            // LENTE SIMPLES
            if($(this).attr("value")=="simples"){

                $("#exibe-lente").hide(500);
                
                $("#editar-lente").show(500);
                
                $("#escolha-lente-semgrau").hide(500);
                $("#escolha-lente-simples").show(500);

                $("#exibe-tipo-simples").show(500);
                $("#exibe-tipo-semgrau").hide(500);
            }

            // LENTE MULTIFOCAL
            if($(this).attr("value")=="multifocal"){
                $("#exibe-lente").hide(500);
                $("#exibe-experimente").show(500);
                $("#exibe-selecionar").hide(500);
                $("#editar-lente").show(500);
                $("#escolha-lente-semgrau").hide(500);
                $("#escolha-lente-simples").hide(500);
                $("#escolha-lente-multifocal").show(500);
                $("#exibe-tipo-semgrau").hide(500);
                $("#exibe-tipo-simples").hide(500);
                $("#exibe-texto-multifocal").show(500);

            }

            // BLOCO DE FUNÇÕES PARA TIPOS DE LENTE SEM GRAU

            // LENTE SEM GRAU

              // PLANA
              if($(this).attr("value")=="plana"){
                  $("#exibe-comprar").show(500);
                  $("#editar-tipo-semgrau").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#escolha-tipo-plana").show(500);
                  $("#exibe-tipo-semgrau").hide(500);
              }

              // DEMONSTRATIVA
              if($(this).attr("value")=="demonstrativa"){
                  $("#exibe-comprar").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#editar-tipo-semgrau").show(500);
                  $("#escolha-tipo-demonstrativa").show(500);
                  $("#exibe-tipo-semgrau").hide(500);
              }

            // BLOCO DE FUNÇÕES PARA TIPOS DE LENTE SIMPLES

            // NORMAIS
            if($(this).attr("value")=="normais"){
                  $("#exibe-comprar").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#editar-tipo-simples").show(500);
                  $("#escolha-tipo-normais").show(500);
                  $("#exibe-tipo-simples").hide(500);
            }

            // FINAS
            if($(this).attr("value")=="finas"){
                  $("#exibe-comprar").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#editar-tipo-simples").show(500);
                  $("#escolha-tipo-finas").show(500);
                  $("#exibe-tipo-simples").hide(500);
            }

            // FINISSIMAS
            if($(this).attr("value")=="finissimas"){
                  $("#exibe-comprar").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#editar-tipo-simples").show(500);
                  $("#escolha-tipo-finissimas").show(500);
                  $("#exibe-tipo-simples").hide(500);
            }

            // NAOSEI
            if($(this).attr("value")=="naosei"){
                  $("#exibe-comprar").show(500);
                  $("#exibe-selecionar").hide(500);
                  $("#editar-tipo-simples").show(500);
                  $("#escolha-tipo-naosei").show(500);
                  $("#exibe-tipo-simples").hide(500);
            }                                    

        }); // fecha click function de cradio button
    
    }); // fecha document ready jquery

$(document).ready(function(){

    $('#exibe-sobre').click(function(){

          if($(this).attr("class")=="show-sobre"){
              
              $('.show-sobre').removeClass('show-sobre').addClass('hide-sobre');
              $(".arrow-down-sobre").hide(); 
              $(".arrow-up-sobre").show(); 
              $(".arrow-down-produtos").show(); 
              $(".arrow-up-produtos").hide(); 
              $("#menu-produtos").slideUp(100); 
              $("#menu-sobre").slideDown(100); 
              
          }else if($(this).attr("class")=="hide-sobre"){

              $(".arrow-down-sobre").show(); 
              $(".arrow-up-sobre").hide(); 
              $('.hide-sobre').removeClass('hide-sobre').addClass('show-sobre');
              $("#menu-sobre").slideUp(100); 
              
          }

    }); // fecha document ready jquery

}); // fecha document ready jquery

$(document).ready(function(){

    $('#exibe-menu').click(function(){

          if($(this).attr("class")=="show-menu"){
              
              $('.show-menu').removeClass('show-menu').addClass('hide-menu');
              $(".arrow-down-produtos").hide(); 
              $(".arrow-up-produtos").show(); 
              $(".arrow-down-sobre").show(); 
              $(".arrow-up-sobre").hide(); 
              $("#menu-sobre").slideUp(100); 
              $("#menu-produtos").slideDown(100); 

          }else if($(this).attr("class")=="hide-menu"){

            $('.hide-menu').removeClass('hide-menu').addClass('show-menu');
            $(".arrow-down-produtos").show(); 
              $(".arrow-up-produtos").hide(); 
              $("#menu-produtos").slideUp(100); 
              
          }

    }); // fecha document ready jquery

}); // fecha document ready jquery

    $(document).ready(function(){

       $("#forma_pagamento_cartao").click(function() {
          $("#checkout-cartao").show(500); 
          $("#checkout-boleto").hide(500); 

        });

       $("#forma_pagamento_boleto").click(function() {
          $("#checkout-boleto").show(500); 
          $("#checkout-cartao").hide(500); 
        });
    }); // fecha document ready jquery


    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-semgrau-1").click(function() {

          $("#car-detalhe-semgrau-1").show(0); // exibe novamente a escolha de lentes
          $("#car-detalhe-simples-1").hide(0); 

        });

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-semgrau-2").click(function() {

          $("#car-detalhe-semgrau-2").show(0); // exibe novamente a escolha de lentes
          $("#car-detalhe-simples-2").hide(0); 

        });

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-semgrau-3").click(function() {

          $("#car-detalhe-semgrau-3").show(0); // exibe novamente a escolha de lentes
          $("#car-detalhe-simples-3").hide(0); 

        });

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-semgrau-4").click(function() {

          $("#car-detalhe-semgrau-4").show(0); // exibe novamente a escolha de lentes
          $("#car-detalhe-simples-4").hide(0); 

        });

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-semgrau-5").click(function() {

          $("#car-detalhe-semgrau-5").show(0); // exibe novamente a escolha de lentes
          $("#car-detalhe-simples-5").hide(0); 

        });
    }); // fecha document ready jquery

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-simples-1").click(function() {

          $("#car-detalhe-simples-1").show(0); 
          $("#car-detalhe-semgrau-1").hide(0); // exibe novamente a escolha de lentes

        });

        // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-simples-2").click(function() {

          $("#car-detalhe-simples-2").show(0); 
          $("#car-detalhe-semgrau-2").hide(0); // exibe novamente a escolha de lentes

        });

       // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-simples-3").click(function() {

          $("#car-detalhe-simples-3").show(0); 
          $("#car-detalhe-semgrau-3").hide(0); // exibe novamente a escolha de lentes

        });

       // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-simples-4").click(function() {

          $("#car-detalhe-simples-4").show(0); 
          $("#car-detalhe-semgrau-4").hide(0); // exibe novamente a escolha de lentes

        });

       // CONTROLE DO LINK DE EDITAR LENTE 
       $("#car-exibe-simples-5").click(function() {

          $("#car-detalhe-simples-5").show(0); 
          $("#car-detalhe-semgrau-5").hide(0); // exibe novamente a escolha de lentes

        });
    }); // fecha document ready jquery

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#exibe-tudo").click(function() {

          $("#exibe-lente").show(500); // exibe novamente a escolha de lentes
          $("#exibe-selecionar").show(500); 
          $("#exibe-escolher").hide(); 
          $("#exibe-experimente").show(500); 
          $("#exibe-texto-multifocal").hide(500);

        });
    }); // fecha document ready jquery

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR LENTE 
       $("#editar-lente").click(function() {

          $("#exibe-lente").show(500); // exibe novamente a escolha de lentes
          $("#exibe-selecionar").show(500); 
          $("#exibe-tipo").hide(500);
          $("#exibe-experimente").show(500);
          $("#exibe-comprar").hide(500);

          $("#editar-lente").hide(500);
          $("#editar-tipo-simples").hide(500);
          $("#editar-tipo-semgrau").hide(500);
          
          $("#escolha-lente-semgrau").hide(500);
          $("#escolha-lente-simples").hide(500);
          $("#escolha-lente-multifocal").hide(500);
          $("#exibe-texto-multifocal").hide(500);

          $("#escolha-tipo-plana").hide(500);
          $("#escolha-tipo-demonstrativa").hide(500);
          $("#exibe-tipo-semgrau").hide(500);
          $("#exibe-tipo-simples").hide(500);

          $("#escolha-tipo-normais").hide(500);
          $("#escolha-tipo-finas").hide(500);
          $("#escolha-tipo-finissimas").hide(500);
          $("#escolha-tipo-naosei").hide(500);

        });
    }); // fecha document ready jquery

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR TIPO 
       $("#editar-tipo-semgrau").click(function() {

          $("#exibe-lente").hide(500);
          $("#exibe-tipo-semgrau").show(500);
          $("#exibe-experimente").show(500);
          $("#exibe-comprar").hide(500);
          $("#exibe-selecionar").show(500);
          $("#editar-lente").show(500);
          
          $("#editar-tipo-semgrau").hide(500);
          $("#escolha-lente-semgrau").show(500);
          $("#escolha-lente-simples").hide(500);
          $("#escolha-lente-multifocal").hide(500);

          $("#escolha-tipo-plana").hide(500);
          $("#escolha-tipo-demonstrativa").hide(500);

          $("#escolha-tipo-normais").hide(500);
          $("#escolha-tipo-finas").hide(500);
          $("#escolha-tipo-finissimas").hide(500);
          $("#escolha-tipo-naosei").hide(500);

          $("#exibe-tipo-simples").hide(500);
          $("#exibe-texto-multifocal").hide(500);

        });
    }); // fecha document ready jquery

    $(document).ready(function(){

      // CONTROLE DO LINK DE EDITAR TIPO 
       $("#editar-tipo-simples").click(function() {
        
          $("#exibe-lente").hide(500);
          $("#exibe-tipo-semgrau").hide(500);
          $("#exibe-tipo-simples").show(500);
          $("#exibe-experimente").show(500);
          $("#exibe-comprar").hide(500);
          $("#exibe-selecionar").show(500);
          $("#editar-lente").show(500);
          
          $("#editar-tipo-simples").hide(500);
          $("#escolha-lente-semgrau").hide(500);
          $("#escolha-lente-simples").show(500);
          $("#escolha-lente-multifocal").hide(500);

          $("#escolha-tipo-normais").hide(500);
          $("#escolha-tipo-finas").hide(500);
          $("#escolha-tipo-finissimas").hide(500);
          $("#escolha-tipo-naosei").hide(500);
          $("#exibe-texto-multifocal").hide(500);
          

        });
    }); // fecha document ready jquery


  function checkInput(ob) {
    var invalidChars = /[^0-9]/gi
    if(invalidChars.test(ob.value)) {
              ob.value = ob.value.replace(invalidChars,"");
        }
  }

  jQuery(function($){
     $("#telefone").mask("(99)99999-9999");
     $("#celular").mask("(99)99999-9999");
     $("#cpf").mask("999.999.999-99");
     $("#card_number").mask("9999999999999999");
     $("#card_expiration_month").mask("99");
     $("#card_expiration_year").mask("99");
     $("#card_cvv").mask("999");
  });

