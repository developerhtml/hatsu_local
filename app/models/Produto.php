<?php

class Produto extends Eloquent
{

	protected $table 			= 'produtos';
	protected $primaryKey	 	= 'id_produto';
	protected $softDelete 		= 'true';
	
}