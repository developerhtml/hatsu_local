<?php

class Newsletter extends Eloquent
{

	protected $table 			= 'newsletter';
	protected $primaryKey	 	= 'id';
	protected $softDelete 		= 'true';
	
}