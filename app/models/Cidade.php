<?php

class Cidade extends Eloquent
{

	protected $softDelete 	= 	true;
	
	public function estado()
	{
    	return $this->belongsTo('Estado');
    }

}