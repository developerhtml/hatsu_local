<?php

class Modelo extends Eloquent
{

	protected $table 			= 'modelos';
	protected $primaryKey	 	= 'id';
	protected $softDelete 		= 'true';
	
}