<?php

class Logger extends Eloquent
{

	protected $table = 'logs';
	protected $primaryKey = 'id_log';
	protected $softDelete = true;

}