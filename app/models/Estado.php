<?php

class Estado extends Eloquent
{

	protected $softDelete = true;
	protected $table = 'estados';

	public function cidades()
	{
         return $this->hasMany('Cidade','uf','uf');
    }

}