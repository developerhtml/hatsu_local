<?php

class Contato extends Eloquent
{

	protected $table 			= 'contatos';
	protected $primaryKey	 	= 'id';
	protected $softDelete 		= 'true';
	
}