<?php

class PedidoItem extends Eloquent
{

	protected $table 			= 'pedidos_itens';
	protected $primaryKey	 	= 'id';
	protected $softDelete 		= 'true';
	
}