<?php

class Helper {

	public static function formataData($date,$inverso=NULL) {

		if (!empty($date)) {
			if ($inverso == 1) {
				$db_date = explode('-', $date);
				$new_date = $db_date[2]."/".$db_date[1]."/".$db_date[0];
				return $new_date;
			}else {
				$db_date = explode('-', $date);
				$new_date = $db_date[2].$db_date[1].$db_date[0];
				return $new_date;
			}

		}else {
			return "";
		}

	} 

	public static function toDateTime($unixTimestamp){
		return date("Y-m-d", $unixTimestamp);
	}

	public static function Galeria($sku){

		$fotos = Fotos::where('sku','=',$sku)->get();

		$return = '';

		foreach ($fotos as $key => $value) {

			if ($key == 0) {

				$return = '<div id="galeria">';

				$return .= '

                 <img src="'.URL::asset("img/".$value->imagem).'" ';

                 if (!empty($value->medidas)) {

	                 $return .= 'data-hover="'.URL::asset("img/".$value->medidas).'" ';

	             }

                 $return  .= 'class="imagem-grande img-responsive" border="0"/> ';

				$return .= '</div>';

            }

			$return .= '<p style="float:left;border:1px solid #ccc;margin:2px;text-align:center;">

			<a href="#" rel="'.URL::asset("img/".$value->imagem).'"  ';

            if (!empty($value->medidas)) {

                $return .= ' class="galeria galeria-'.$sku.'" ';

            }else {
	
				$return .= ' class="galeria" ';

            }

			$return .= '><img src="'.URL::asset("img/".$value->imagem).'" class="thumb" border="0" /></a></p>';

		}

		$return .= "<br style='clear:both;' />";

		return $return;

	}


public static function novoMenu(){

		$linhas = Produto::where('ativo','=',1)->get();

		$return = '<div id="menu-produtos">';

		$j=1;

		foreach ($linhas as $linha) {
			
			$return .= '

				<div id="item-menu">

	        	<a href="'.URL::to($linha->menu).'">

	        		<p class="menu-linha">'.strtoupper($linha->menu).'</p>
	        
	                 <p class="menu-descricao">'.$linha->descricao_menu.'</p>

					<img src="'.URL::asset("img/".$linha->thumb).'" style="margin-top:5px;margin-bottom:5px;max-width:180px;height:62px;" />';

					$array_tipos = explode(";", $linha->tipos);
					$array_cores = explode(";", $linha->cores);

	                 $return .= '<p style="text-align:center">';


	                 foreach ($array_cores as $key => $value) {
	                 	
						$cor = URL::asset("img/menu/".$value.".png");
						$title = $value;

						if ($linha->menu == "raid" && $value == "azul") {
							$cor = URL::asset("img/menu/".$value."raid.png");
						}elseif ($linha->menu == "titanium" && $value == "pretoouro") {
							$title = "Preto e Ouro";
						}elseif ($linha->menu == "titanium" && $value == "pretoamarelo") {
							$title = "Preto e Amarelo";
						}elseif ($linha->menu == "titanium" && $value == "pretoverde") {
							$title = "Preto e Verde";
						}elseif ($linha->menu == "titanium" && $value == "cinzavermelho") {
							$title = "Cinza e Vermelho";
						}elseif ($linha->menu == "titanium" && $value == "pretolaranja") {
							$title = "Preto e Laranja";
						}elseif ($linha->menu == "projection" && $value == "gunmetal") {
							$title = "Gun Metal";
						}elseif ($linha->menu == "ultra" && $value == "roxo") {
							$cor = URL::asset("img/menu/".$value."ultra.png");
						}

	                 	$return .= '<img src='.$cor.' class="micro" data-toggle="tooltip" title="'.strtoupper($linha->menu." - ".$title).'" />';
	                 }

	                 $return .= '&nbsp;&nbsp;&nbsp;&nbsp;';


	                 foreach ($array_tipos as $key => $value) {

						$vetor = URL::asset("img/menu/".$value.".png");

						if ($value == "arofechado") {
							$title = "Aro Fechado";
						}elseif ($value == "meioaro") {
							$title = "Meio Aro";
						}elseif ($value == "trespecas") {
							$title = "Três Peças";
						}

	                 	
	                 	$return .= '<img src='.$vetor.' class="micro_vetor" data-toggle="tooltip" title="'.strtoupper($title).'" />';

	                 }

				$return .= '</span>';

	            $return .= '</p>';

	            $return .= '</div></a>';


		}

		$return .= '</div>';

		return $return;

      }    

	public static function menuLinha(){

		$linhas = Produto::where('ativo','=',1)->get();

		$return = '

		<div class="container" style="border-top: 1px solid #000;padding-top:5px;">

		<div class="row">

		<ul id="main-nav">';

		$j=1;

		foreach ($linhas as $linha) {
			
			$return .= '<li>

	        <a href="'.URL::to($linha->menu).'">'.strtoupper($linha->menu).'</a>
	        
	        <div class="sub-nav-'.$linha->menu.' uparrowdiv-'.strtolower(substr($linha->menu, 0, 1)).'">

	          <div class="row col-md-12" style="font-family:roboto-light;text-align:center;">

	            <div class="col-md-4" style="text-align:center;margin-top:10px;">
	            
	               <div style="padding-top:10px;padding-left:10px;">

	               ';

	            /*   if ($j==1) {

		               $return .= '<img src="'.URL::asset("img/produtos/submenu_".strtolower($linha->menu).".jpg").'" style="opacity: 0.25;filter: alpha(opacity=25);position:absolute;top:80px;left:-10px">';

		           } */

		           $return .= '
		           
	                 <p>'.$linha->descricao_menu.'</p>

	                 <br />

					 <p><a href="'.URL::to(strtolower($linha->nome)).'" title="'.$linha->nome.'" style="color:#77a302">VEJA TODOS MODELOS >></a></p>

	               </div>
	            </div>';

	            $modelos = Modelo::where('id_produto','=',$linha->id_produto)->where('ativo','=',1)->orderBy(DB::raw('RAND()'))->take(4)->get();

	            $i=1;

	            foreach ($modelos as $modelo) {

	            	if ($i==1 || $i == 3 || $i==5) { $return .= '<div class="col-md-4" style="text-align:center;">'; }
					
					$return .= '

		               <div style="margin-top:10px;padding:10px;">
		                 
		                 <p>

		                 <a href="'.URL::to($linha->menu).'/'.strtolower($modelo->url).'" title="'.$modelo->modelo.' - '.$modelo->cor.'">

		                 <img src="'.URL::asset("img/".$modelo->thumb).'" class="img-responsive" />

		                 </a>

		                 </p>
		                 
		                 <p>

		                 <a href="'.URL::to($linha->menu).'/'.strtolower($modelo->url).'" title="'.$modelo->modelo.' - '.$modelo->cor.'">

		                 '.$modelo->modelo.' - '.strtoupper($modelo->cor).'

		                 </a>

		                 </p>
		                 
		               </div>';  

		               if ($i==2 || $i == 4 || $i==6) {

		               		$return .= '</div>';

		               }

		               $i++;

	            }

            	$return .= '</div>';

			$return .= '</div></li>';

		}

		$return .= '</ul>';

		$return .= '</div>';

		$return .= '</div>';

		return $return;

      }    

	public static function LinhaProdutos ($id_produto){

		if ($id_produto == 1) {

			$linha = "projection";
			$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";

		}elseif ($id_produto == 2) {

			$linha = "raid";
			$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span><span class='tag_sport'>SPORT</span></p>";

		}elseif ($id_produto == 3) {

			$linha = "ultra";
			$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";

		}elseif ($id_produto == 4) {

			$linha = "kizo";
			$tag = "<p class='tags'><span class='tag_kids'>KIDS</span></p>";

		}elseif ($id_produto == 5) {

			$linha = "titanium";
			$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";
		}

         $modelos = Modelo::where('id_produto','=',$id_produto)->where('ativo','=',1)->get();

		$count = count($modelos);

        $return = '<div class="row">

        <div class="cold-md-12" style="text-align:center;">';

        $i=1;
        $j=1;
        $k=1;

        foreach ($modelos as $modelo) {

        	if($modelo->novo == 1){
				$novo = "<p class='novo'>NOVO</p>";
        	}else {
        		$novo = "";
        	}

			if ($linha == "kizo" && $modelo->modelo == "KI_03" && $i==1) {
        		$return .= "<h3 class='titulo' style='text-align:left;margin-left:20px;'>PARA CRIANÇAS DE 5 A 10 ANOS</h3><br />";
        		$i++;
        	}

        	if ($linha == "kizo" && $modelo->modelo == "KI_13" && $k==5) {
        		$return .= "<br style='clear:both;' />";
        		$return .= "<h3 class='titulo' style='text-align:left;margin-left:20px;'>PARA CRIANÇAS DE 8 A 13 ANOS</h3><br />";
        	}

        	$k++;

        	if (($j==4 || $j==8) && ($linha == "kizo" || $linha == "raid")) {

	        	$return .= '<div class="col-md-4" id="new-thumb" style="margin-left:313px;">';	 

	        }elseif ($j==4 && ($linha == "ultra" || $linha == "titanium")) {

				$return .= '<div class="col-md-4" id="new-thumb" style="margin-left:155px;">';	  

	        }else {

				$return .= '<div class="col-md-4" id="new-thumb">';	        	
	        }
			
				$return .= '

					'.$novo.'

					'.$tag.'

	                 <p class="thumb-img">

	                 <a href="'.URL::to($linha.'/'.strtolower($modelo->url)).'" title="'.$modelo->modelo.' - '.$modelo->cor.'">

 					';

					$img_normal = URL::asset("img/".$modelo->thumb);
 					$img_return = "this.src='".URL::asset("img/".$modelo->thumb)."'";
 					$img_over = "this.src='".URL::asset("img/".$modelo->over)."'";

					$return .= '<img src="'.$img_normal.'" 

					onmouseover="'.$img_over.'" 

					onmouseout="'.$img_return.'" 

					class="thumb-over" />


	                 </a>

	                 </p>
	                 
	                 <p class="thumb-texto">

	                 <a href="'.URL::to($linha.'/'.strtolower($modelo->url)).'" title="'.$modelo->modelo.' - '.$modelo->cor.'">

	                 <span class="thumb-titulo"><b>'.strtoupper($linha).'</b></span>

	                 <br /><span class="thumb-descricao">'.$modelo->modelo.' - '.strtoupper($modelo->cor).'</span><br />

	                 ';

	                 if ($modelo->esgotado == 1) {
	                 	$return .= '<span class="thumb-descricao">ESGOTADO</span>';
	                 }else {
						$return .= '<span class="thumb-descricao">R$ '.number_format($modelo->preco,0,",",".").'</span>';
	                 }

	                 $return .= '

	                 </a>

	                 </p>

	                 ';


				$return .= '</div>';

              $j++;

        }

		$return .= '</div>';

		$return .= '</div>';

		return $return;

      }          

 

	public static function LinhaTitanium ($id_produto){

		if ($id_produto == 1) {

			$linha = "projection";

		}elseif ($id_produto == 2) {

			$linha = "raid";

		}elseif ($id_produto == 3) {

			$linha = "ultra";

		}elseif ($id_produto == 4) {

			$linha = "kizo";

		}elseif ($id_produto == 5) {

			$linha = "titanium";
		}

         $modelos = Modelo::where('id_produto','=',$id_produto)->where('ativo','=',1)->take(3)->get();

        $return = '<div class="row"><div class="cold-md-12" style="text-align:center;">';

        foreach ($modelos as $modelo) {

        	$return .= '<div class="col-md-4">';
			
			$return .= '

                 <p>

                 <a href="'.URL::to($linha).'/'.strtolower($modelo->url).'" title="'.$modelo->modelo.' - '.$modelo->cor.'" style="color:#000;">

				<img src="'.URL::asset("img/".$modelo->thumb).'" />

                 </a>

                 </p>
                 
                 <p>

                 <a href="'.URL::to($linha).'/'.strtolower($modelo->url).'" title="'.$modelo->modelo.' - '.$modelo->cor.'" style="color:#000;">

                 '.$modelo->modelo.' - '.strtoupper($modelo->cor).'

                 </a>

                 <br />

                 <a href="'.URL::to($linha).'/'.strtolower($modelo->url).'" title="'.$modelo->modelo.' - '.$modelo->cor.'" style="color:#77A302;text-decoration:underline;font-weight:bold;">R$ '.number_format($modelo->preco,2,",",".").'</a>

                 </p>

                 <br />';

              $return .= "</div>";

        }

        $return .= '</div>';

		$modelos = Modelo::where('id_produto','=',$id_produto)->where('ativo','=',1)->skip(3)->take(2)->get();

        $return .= '<div class="row"><div class="cold-md-12" style="text-align:center;">';

        foreach ($modelos as $modelo) {        	

        	if ($modelo->id == 29) {

				$return .= '<div class="col-md-6" style="text-align:center;" id="modelos-titanium-2">';

        	}else {

        		$return .= '<div class="col-md-6" style="text-align:center;" id="modelos-titanium-1">';

        	}
			
			$return .= '

				<div id="smart-thumb">

                <p>

                 <a href="'.URL::to($linha).'/'.strtolower($modelo->url).'" title="'.$modelo->modelo.' - '.$modelo->cor.'" style="color:#000;">

				<img src="'.URL::asset("img/".$modelo->thumb).'" 

				/>

                 </a>

                 </p>
                 
                 <p>

                 <a href="'.URL::to($linha).'/'.strtolower($modelo->url).'" title="'.$modelo->modelo.' - '.$modelo->cor.'" style="color:#000;">

                 '.$modelo->modelo.' - '.strtoupper($modelo->cor).'

                 </a>

                 <br />

                 <a href="'.URL::to($linha).'/'.strtolower($modelo->url).'" title="'.$modelo->modelo.' - '.$modelo->cor.'" style="color:#77A302;text-decoration:underline;font-weight:bold;">R$ '.number_format($modelo->preco,2,",",".").'</a>

                 </p>

                 </div>


                 <br />';

              $return .= "</div>";

        }

		$return .= '</div>';

		$return .= '</div>';

		$return .= '</div>';

		return $return;

      }          


	public static function ExibeCores ($id_produto,$id,$model){

		if ($id_produto == 1) {

			$linha = "projection";

		}elseif ($id_produto == 2) {

			$linha = "raid";

		}elseif ($id_produto == 3) {

			$linha = "ultra";

		}elseif ($id_produto == 4) {

			$linha = "kizo";

		}elseif ($id_produto == 5) {

			$linha = "titanium";
		}

         $modelos = Modelo::where("modelo","=",$model)->where('id_produto','=',$id_produto)->where('ativo','=',1)->get();

         $return = '';

        foreach ($modelos as $modelo) {

        		$return .= '<div class="cold-md-3" style="text-align:center;font-size:14px;float:left;border:0px solid #ccc;margin:2px;">';

				$return .= '<a href="'.URL::to($linha.'/'.$modelo->url).'" title="'.$linha.' - '.$modelo->modelo.'" style="color:#000;"><img src="'.URL::asset('img/'.$modelo->thumb).'" style="width:150px;height:60px;border:0;" /></a>';

				$return .= '<p><a href="'.URL::to($linha.'/'.$modelo->url).'" title="'.$linha.' - '.$modelo->modelo.'" style="color:#000;">'.$modelo->modelo.'</a> - <a href="'.URL::to($linha.'/'.$modelo->url).'" title="'.$linha.' - '.$modelo->modelo.'" style="color:#000;">'.$modelo->cor.'</a></p>';

				$return .= '</div>';
        }

		return $return;

      }   


	public static function ExibeModelos ($id_produto,$model){

		if ($id_produto == 1) {

			$linha = "projection";

		}elseif ($id_produto == 2) {

			$linha = "raid";

		}elseif ($id_produto == 3) {

			$linha = "ultra";

		}elseif ($id_produto == 4) {

			$linha = "kizo";

		}elseif ($id_produto == 5) {

			$linha = "titanium";
		}

         $modelos = Modelo::where("modelo","<>",$model)->where('id_produto','=',$id_produto)->where('ativo','=',1)->get();

         $return = '';

        foreach ($modelos as $modelo) {

        		$return .= '<div class="cold-md-3" style="text-align:center;font-size:14px;float:left;border:0px solid #ccc;margin:2px;">';

				$return .= '<a href="'.URL::to($linha.'/'.$modelo->url).'" title="'.$linha.' - '.$modelo->modelo.'" style="color:#000;"><img src="'.URL::asset('img/'.$modelo->thumb).'" style="width:150px;height:60px;border:0;" /></a>';

				$return .= '<p><a href="'.URL::to($linha.'/'.$modelo->url).'" title="'.$linha.' - '.$modelo->modelo.'" style="color:#000;">'.$modelo->modelo.'</a> - <a href="'.URL::to($linha.'/'.$modelo->url).'" title="'.$linha.' - '.$modelo->modelo.'" style="color:#000;">'.$modelo->cor.'</a></p>';

				$return .= '</div>';
        }

		return $return;

      }         

	public static function Destaques ($numero){

		$modelos = Modelo::where('destaque','=','1')->take($numero)->get();

		$return = '<div class="container" style="padding:0px;margin-top:1px;"> ';

		$return .='<h1 class="titulo">PRODUTOS</h1>

		<p class="subtitulo">Armações inéditas no Brasil, com lentes <strong>grátis</strong> e com 1 ano de garantia!</p>';

		$return .= '';

		$i=1;

            foreach ($modelos as $modelo) {

            	if ($modelo->novo == 1) {
					$novo = "<p class='novo'>NOVO</p><p class='novo-over'>NOVO</p>";
            	}else {
            		$novo = "";
            	}

            	if (substr($modelo->sku,0,2) == "PR") {
            		$linha = "projection";
            		$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";
            	}elseif (substr($modelo->sku,0,2) == "RA") {
            		$linha = "raid";
            		$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span><span class='tag_sport'>SPORT</span></p>";
            	}elseif (substr($modelo->sku,0,2) == "TI") {
					$linha = "titanium";
					$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";
            	}elseif (substr($modelo->sku,0,2) == "KI") {
					$linha = "kizo";
					$tag = "<p class='tags'><span class='tag_kids'>KIDS</span></p>";
            	}elseif (substr($modelo->sku,0,2) == "UT") {
            		$linha = "ultra";
            		$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";
            	}

	           	$return .= '<div class="col-md-4" id="destaques">';

				$return .= '

					'.$novo.'

					'.$tag.'

	                 <p class="thumb-img">

	                 <a href="'.URL::to($linha.'/'.strtolower($modelo->url)).'" title="'.$modelo->modelo.' - '.$modelo->cor.'">

 					';

					$img_normal = URL::asset("img/".$modelo->thumb);
 					$img_return = "this.src='".URL::asset("img/".$modelo->thumb)."'";
 					$img_over = "this.src='".URL::asset("img/".$modelo->over)."'";

					$return .= '<img src="'.$img_normal.'" 

					onmouseover="'.$img_over.'" 

					onmouseout="'.$img_return.'" 

					class="thumb-over" />


	                 </a>

	                 </p>
	                 
	                 <p class="thumb-texto">

	                 <a href="'.URL::to($linha.'/'.strtolower($modelo->url)).'" title="'.$modelo->modelo.' - '.$modelo->cor.'">

	                 <span class="thumb-titulo"><b>'.strtoupper($linha).'</b></span>

	                 <br /><span class="thumb-descricao">'.$modelo->modelo.' - '.strtoupper($modelo->cor).'</span><br />

					';

	                 if ($modelo->esgotado == 1) {
	                 	$return .= '<span class="thumb-descricao">ESGOTADO</span>';
	                 }else {
						$return .= '<span class="thumb-descricao">R$ '.number_format($modelo->preco,0,",",".").'</span>';
	                 }

	                 $return .= '

	                 </a>

	                 </p>

	                 ';

	              $return .= "</div>";

				$i++;

            }

            // $return .= "</div>";

            $return .= "</div>";

		return $return;

      }  
	  
//yahan se mera function shuru ho raha hai 	  
	  
	  
	  public static function MeraFunc ($numero){

		$modelos = Modelo::where('destaque','=','1')->take($numero)->get();

		$return = '<div class="container"> ';

		$return .='<h1>produtos em destaque</h1>';

		$return .= '';

		$i=1;

            foreach ($modelos as $modelo) {

            	if ($modelo->novo == 1) {
					$novo = "<p class='novo'>NOVO</p><p class='novo-over'>NOVO</p>";
            	}else {
            		$novo = "";
            	}

            	if (substr($modelo->sku,0,2) == "PR") {
            		$linha = "projection";
            		$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";
            	}elseif (substr($modelo->sku,0,2) == "RA") {
            		$linha = "raid";
            		$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span><span class='tag_sport'>SPORT</span></p>";
            	}elseif (substr($modelo->sku,0,2) == "TI") {
					$linha = "titanium";
					$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";
            	}elseif (substr($modelo->sku,0,2) == "KI") {
					$linha = "kizo";
					$tag = "<p class='tags'><span class='tag_kids'>KIDS</span></p>";
            	}elseif (substr($modelo->sku,0,2) == "UT") {
            		$linha = "ultra";
            		$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";
            	}

	           	$return .= '<div class="col-md-4" id="destaques">';

				$return .= '

					'.$novo.'

					'.$tag.'

	                 <p class="thumb-img">

	                 <a href="'.URL::to($linha.'/'.strtolower($modelo->url)).'" title="'.$modelo->modelo.' - '.$modelo->cor.'">

 					';

					$img_normal = URL::asset("img/".$modelo->thumb);
 					$img_return = "this.src='".URL::asset("img/".$modelo->thumb)."'";
 					$img_over = "this.src='".URL::asset("img/".$modelo->over)."'";

					$return .= '<img src="'.$img_normal.'" 

					onmouseover="'.$img_over.'" 

					onmouseout="'.$img_return.'" 

					class="thumb-over" />


	                 </a>

	                 </p>
	                 
	                 <p class="thumb-texto">

	                 <a href="'.URL::to($linha.'/'.strtolower($modelo->url)).'" title="'.$modelo->modelo.' - '.$modelo->cor.'">

	                 <span class="thumb-titulo"><b>'.strtoupper($linha).'</b></span>

	                 <br /><span class="thumb-descricao">'.$modelo->modelo.' - '.strtoupper($modelo->cor).'</span><br />

					';

	                 if ($modelo->esgotado == 1) {
	                 	$return .= '<span class="thumb-descricao">ESGOTADO</span>';
	                 }else {
						$return .= '<span class="thumb-descricao">R$ '.number_format($modelo->preco,0,",",".").'</span>';
	                 }

	                 $return .= '

	                 </a>

	                 </p>

	                 ';

	              $return .= "</div>";

				$i++;

            }

            // $return .= "</div>";

            $return .= "</div>";

		return $return;

      }  
	  
	  
// yahan khatam ho raha hai	  

	public static function Produtos (){

		$linhas = Produto::where('ativo','=',1)->get();

		$return = '<div class="row" style="padding:0px"> ';

		foreach ($linhas as $linha) {
			
			$return .= '

				<div class="col-md-12" style="text-align:left;margin-bottom:10px;">

				<h2 style="font-size:40px;font-weight:normal;border-bottom:1px solid #000;">

				<a href="'.URL::to($linha->menu).'" style="color:#000;">'.strtoupper($linha->menu).'</a>

				</h2>

	            <br /><p>'.$linha->descricao.'</p><br />';

				if ($linha->id_produto == 1) {

					$nome_linha = "projection";
					$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";

				}elseif ($linha->id_produto == 2) {

					$nome_linha = "raid";
					$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span><span class='tag_sport'>SPORT</span></p>";

				}elseif ($linha->id_produto == 3) {

					$nome_linha = "ultra";
					$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";

				}elseif ($linha->id_produto == 4) {

					$nome_linha = "kizo";
					$tag = "<p class='tags'><span class='tag_kids'>KIDS</span></p>";

				}elseif ($linha->id_produto == 5) {

					$nome_linha = "titanium";
					$tag = "<p class='tags'><span class='tag_adulto'>ADULTO</span></p>";
				}

		         $modelos = Modelo::where('id_produto','=',$linha->id_produto)->where('ativo','=',1)->get();



				$count = count($modelos);

		        $return .= '<div class="row">

		        <div class="cold-md-12" style="text-align:center;">';

		        $i=1;
		        $j=1;
		        $k=1;

		        foreach ($modelos as $modelo) {

	            	if ($modelo->novo == 1) {
						$novo = "<p class='novo'>NOVO</p><p class='novo-over'>NOVO</p>";
	            	}else {
	            		$novo = "";
	            	}

					if ($nome_linha == "kizo" && $modelo->modelo == "KI_03" && $i==1) {
		        		$return .= "<h3 class='titulo' style='text-align:left;margin-left:20px;'>PARA CRIANÇAS DE 5 A 10 ANOS</h3><br />";
		        		$i++;
		        	}

		        	if ($nome_linha == "kizo" && $modelo->modelo == "KI_13" && $k==5) {
		        		$return .= "<br style='clear:both;' />";
		        		$return .= "<h3 class='titulo' style='text-align:left;margin-left:20px;'>PARA CRIANÇAS DE 8 A 13 ANOS</h3><br />";
		        	}

		        	$k++;

		        	if (($j==4 || $j==8) && ($nome_linha == "kizo" || $nome_linha == "raid")) {

			        	$return .= '<div class="col-md-4" id="new-thumb" style="margin-left:313px;">';	 

			        }elseif ($j==4 && ($nome_linha == "ultra" || $nome_linha == "titanium")) {

						$return .= '<div class="col-md-4" id="new-thumb" style="margin-left:155px;">';	  

			        }else {

						$return .= '<div class="col-md-4" id="new-thumb">';	        	
			        }
					
						$return .= '

							'.$novo.'

							'.$tag.'

			                 <p class="thumb-img">

			                 <a href="'.URL::to($nome_linha.'/'.strtolower($modelo->url)).'" title="'.$modelo->modelo.' - '.$modelo->cor.'">

		 					';

							$img_normal = URL::asset("img/".$modelo->thumb);
		 					$img_return = "this.src='".URL::asset("img/".$modelo->thumb)."'";
		 					$img_over = "this.src='".URL::asset("img/".$modelo->over)."'";

							$return .= '<img src="'.$img_normal.'" 

							onmouseover="'.$img_over.'" 

							onmouseout="'.$img_return.'" 

							class="thumb-over" />


			                 </a>

			                 </p>
			                 
			                 <p class="thumb-texto">

			                 <a href="'.URL::to($nome_linha.'/'.strtolower($modelo->url)).'" title="'.$modelo->modelo.' - '.$modelo->cor.'">

			                 <span class="thumb-titulo"><b>'.strtoupper($nome_linha).'</b></span>

			                 <br /><span class="thumb-descricao">'.$modelo->modelo.' - '.strtoupper($modelo->cor).'</span><br />

			                 ';

			                 if ($modelo->esgotado == 1) {
			                 	$return .= '<span class="thumb-descricao">ESGOTADO</span>';
			                 }else {
								$return .= '<span class="thumb-descricao">R$ '.number_format($modelo->preco,0,",",".").'</span>';
			                 }

			                 $return .= '

			                 </a>

			                 </p>

			                 ';


						$return .= '</div>';

		              $j++;

			        }

					$return .= '</div>';

				$return .= '</div>';

			$return .= '</div>';

        }

        $return .= "</div>";

		$return .= '</div>';

		$return .= '</div>';

		return $return;

      }        

	  public static function CalculaTotal(){

		$all_skus = Modelo::where("ativo","=",1)->get();

      	foreach ($all_skus as $modelos) {

      		$verificar_beta = $modelos->sku;

      		$array_verificador = range(1,15);

			foreach ($array_verificador as $value) {

				$beta = $verificar_beta."-".$value;

				if (Session::has($beta)) {

					$item = Session::get($beta);

					// echo '<pre>';
					// var_dump($item);
					// exit();

					if (Session::has('voucher-diadospais')) {

						if ($item["sku"] == "PR_01_01" || $item["sku"] == "PR_01_02" ||  $item["sku"] == "PR_02_03"
							|| $item["sku"] == "RA_02_02" || $item["sku"] == "RA_02_01" || $item["sku"] == "RA_03_02"
							|| $item["sku"] == "RA_03_04") {
						
							$total_final_semlente[] = (($item["subtotal"]-$item["valor_lente"])-150);		

						}else {

							$total_final_semlente[] = (($item["subtotal"]-$item["valor_lente"]));									
						}

					}else {

						$total_final_semlente[] = ($item["subtotal"]-$item["valor_lente"]);
					}

					$total_final[] = $item["subtotal"];

					$total_lentes[] = $item["valor_lente"];

      			} // se existir o indice da session 

			} // fecha foreach array verificador

      	} // fecha foreach de sku

      	if (!empty($total_final)) {

      		if (Session::has('voucher')) {

				$desconto = 0.9*(array_sum($total_final_semlente));

				$total = ($desconto+array_sum($total_lentes));	      			

				$return = $total;			

			}elseif (Session::has('voucherclube')) {

				$previa_total = (array_sum($total_final)-447);

				if ($previa_total<=0) {

					$return = 0.00;
				}else {

					$return = $previa_total;

				}			

			}elseif (Session::has('voucher-pd')) {

				$desconto = 0.85*(array_sum($total_final_semlente));

				$total = ($desconto+array_sum($total_lentes));	      			

				$return = $total;

			}elseif (Session::has('voucher-diadospais')) {

				$desconto = array_sum($total_final_semlente);

				$total = ($desconto+array_sum($total_lentes));	      			

				$return = $total;

			}elseif (Session::has('voucher-fabio')) {

				$desconto = (array_sum($total_final_semlente)-367);

				$total = ($desconto+array_sum($total_lentes));	      			

				$return = $total;

			}else {

				$return = array_sum($total_final);

			}

      	}else {

      		$return = "";
      	}
      	
      	return $return;

      }  

	public static function VisualizarPedido($id_pedido,$tipo=1){

      	$itens = PedidoItem::where('id_pedido', '=', $id_pedido)->get();
      	
      	$return = "";
		
		$return .= "

      		<table class='table table-striped tcart'>

      		<tr class='revisao-header'>

      		<td class='revisao-header'>PRODUTO</td>

      		<td class='revisao-header'>PREÇO</td>

      		</tr>

      		";

      	foreach ($itens as $item) {

      		$dados_item = Modelo::where('sku','=',$item->sku)->first();

			$return .= "<tr>";

			$return .= "<td><img src=".URL::asset('img/'.$dados_item->thumb)." class='img-responsive' style='max-width:120px' /> <br />";

			$return .= "".$dados_item->modelo." - ".$dados_item->cor."</td>";

			$return .= "<td>R$ ".number_format($item->preco,2,",",".")."</td>";

			$return .= "</tr>";

			$return .= "<tr>";

			$return .= "<td>".strtoupper($item->lente)." - ".strtoupper($item->tipo_lente)."</td>";

			$return .= "<td>R$ ".number_format($item->valor_lente,2,",",".")."</td>";

			$return .= "</tr>";

			$total_final[] = $item->subtotal;

			$total_final_semlente[] = ($item->subtotal-$item->valor_lente);

			$total_lentes[] = $item->valor_lente;


      		}

			$total_final = array_sum($total_final);
      		
      		if ($tipo==1) {

		      	$return .= "</table>";

		      }

      	return $return;

      }  

public static function Receita($id_pedido){

      	$itens = PedidoItem::where('id_pedido', '=', $id_pedido)->get();
      	
      	$receita = 0;

      	foreach ($itens as $item) {

      		$dados_item = Modelo::where('sku','=',$item->sku)->first();

      		if ($item->lente == "simples") {

      			$receita = 1;
      		}

      	}
      		
      	return $receita;

      }  

	public static function ConfirmacaoPedido($id_pedido){

      	$itens = PedidoItem::where('id_pedido', '=', $id_pedido)->get();
      	
		$return = "

      		<table style='width:600px;border-collapse:collapse;text-align:center'>

      		<tr style='background:#f1f1f1;'>

      		<td style='border:1px solid #ccc;'><b>PRODUTO</b></td>

      		<td style='border:1px solid #ccc;'><b>SUBTOTAL</b></td>

      		</tr>

      		";

      	foreach ($itens as $item) {

      		$dados_item = Modelo::where('sku','=',$item->sku)->first();

			$return .= "<tr>";

			$return .= "<td style='border:1px solid #ccc;'><img src='http://www.hatsu.com.br/public/img/".$item->thumb."' class='img-responsive' style='max-width:120px' /> <br />";

			$return .= $dados_item->modelo." - ".$dados_item->cor."</td>";

			$return .= "<td style='border:1px solid #ccc;'>R$ ".number_format($item->preco,2,",",".")."</td>";

			$return .= "</tr>";

			$return .= "<tr>";

			$return .= "<td style='border:1px solid #ccc;'>".strtoupper($item->lente)." - ".strtoupper($item->tipo_lente)."</td>";

			$return .= "<td style='border:1px solid #ccc;'>R$ ".number_format($item->valor_lente,2,",",".")."</td>";

			$return .= "</tr>";

			$total_final[] = $item->subtotal;

			$total_final_semlente[] = ($item->subtotal-$item->valor_lente);

			$total_lentes[] = $item->valor_lente;


      		}

			$total_final = array_sum($total_final);
      		
      	return $return;

      }  


      public static function RevisaoPedido(){

      	$all_skus = Modelo::where("ativo","=",1)->get();

		$return = "

      		<table id='revisao'>

      		<tr class='revisao-header'>

      		<td class='revisao-header'>PRODUTO</td>

      		<td class='revisao-header'>QTD.</td>

      		<td class='revisao-header'>PREÇO</td>

      		</tr>

      		";

      	foreach ($all_skus as $modelos) {

      		$verificar_beta = $modelos->sku;

      		$array_verificador = range(1,15);

			foreach ($array_verificador as $value) {

				$beta = $verificar_beta."-".$value;

				if (Session::has($beta)) {

					$item = Session::get($beta);

					if (Session::has('voucher-diadospais')) {

						if ($item["sku"] == "PR_01_01" || $item["sku"] == "PR_01_02" ||  $item["sku"] == "PR_02_03"
							|| $item["sku"] == "RA_02_02" || $item["sku"] == "RA_02_01" || $item["sku"] == "RA_03_02"
							|| $item["sku"] == "RA_03_04") {
						
							$total_final_semlente[] = (($item["subtotal"]-$item["valor_lente"])-150);		

						}else {

							$total_final_semlente[] = ($item["subtotal"]-$item["valor_lente"]);
						}

					}else {

						$total_final_semlente[] = ($item["subtotal"]-$item["valor_lente"]);
					}

					$return .= "<tr>";

					$return .= "<td><img src=".URL::asset('img/'.$item["thumb"])." class='img-responsive' style='max-width:120px' /> <br />";

					$return .= "".$modelos->modelo." - ".$modelos->cor."</td>";

	  				$return .= "<td>".$item["quantidade"]."</td>";

	  				$return .= "<td>R$ ".number_format($item["preco"],2,",",".")."</td>";

	  				$return .= "</tr>";

	  				$return .= "<tr>";

					$return .= "<td>".strtoupper($item["lente"])." - ".strtoupper($item["tipo"])."</td>";

	  				$return .= "<td>1</td>";

	  				$return .= "<td>R$ ".number_format($item["valor_lente"],2,",",".")."</td>";

					// $return .= '<td rowspan="2"><b>'.'R$ '.number_format($item["subtotal"],2,",",".").'</b></td>';

					$return .= "</tr>";

					$total_final[] = $item["subtotal"];

					// $total_final_semlente[] = ($item["subtotal"]-$item["valor_lente"]);

					$total_lentes[] = $item["valor_lente"];

				}

      		}
      		
      	}

      	// $return .= "</table>";

		if (Session::has('voucher')) {

			$desconto = 0.9*(array_sum($total_final_semlente));

			$total = ($desconto+array_sum($total_lentes));	      			

			$total_final = $total;			

		}elseif (Session::has('voucherclube')) {

			$previa_total = (array_sum($total_final)-447);

			if ($previa_total<=0) {

				$total_final = 0.00;

			}else {

				$total_final = $previa_total;

			}

		}elseif (Session::has('voucher-pd')) {

			$desconto = 0.85*(array_sum($total_final_semlente));

			$total = ($desconto+array_sum($total_lentes));	      			

			$total_final = $total;			

		}elseif (Session::has('voucher-diadospais')) {

			$desconto = (array_sum($total_final_semlente));

			$total = ($desconto+array_sum($total_lentes));	      			

			$total_final = $total;	

		}elseif (Session::has('voucher-fabio')) {

			$desconto = (array_sum($total_final_semlente)-367);

			$total = ($desconto+array_sum($total_lentes));	      			

			$total_final = $total;			

		}else {

			$total_final = array_sum($total_final);
		}

      	$return .= "<tr><td colspan='3'><hr style='max-width:280px;' /></td></tr>";

      	$return .= "<tr><td colspan='2'>TOTAL</td><td><b>R$ ".number_format($total_final,2,",",".")."</b></td></tr>";

      	$return .= "</table>";

      	$return .= "<br />";

      	return $return;

      }  

		public static function topoCarrinho(){

	      	$all_skus = Modelo::where("ativo","=",1)->get();

	      	$retornar = 0;
	      	$total_itens = 0;

			foreach ($all_skus as $modelos) {

	      		$verificar_beta = $modelos->sku;

	      		$array_verificador = range(1,15);

				foreach ($array_verificador as $value) {

					$beta = $verificar_beta."-".$value;

					if (Session::has($beta)) {

		      			$total_itens++;

		      			$retornar = 1;

					
		      		}
		      		
		      	}

		     }

	      	if ($retornar == 1) {

				$return = "

				| <span>".$total_itens."</span><a style='color:#000;' href=".URL::to('carrinho')." title='Acesse seu carrinho de compras e edite seu pedido'>CARRINHO DE COMPRAS</a>";

		      	return $return;

		      }else {

		      	return "";
		      }

		}



		public static function topCarrinho(){

	      	$all_skus = Modelo::where("ativo","=",1)->get();

	      	$retornar = 0;
	      	$total_itens = 0;

			foreach ($all_skus as $modelos) {

	      		$verificar_beta = $modelos->sku;

	      		$array_verificador = range(1,15);

				foreach ($array_verificador as $value) {

					$beta = $verificar_beta."-".$value;

					if (Session::has($beta)) {

		      			$total_itens++;

		      			$retornar = 1;

					
		      		}
		      		
		      	}

		     }

	      	if ($retornar == 1) {

				$return = "

				<li style='border-left:1px solid #ccc;'><a href=".URL::to('carrinho')." title='Acesse seu carrinho de compras e edite seu pedido'><img src='".URL::asset("img/menu/cart.png")."' style='vertical-align:middle;width:20px;height:20px;'> <span class='badge' style='background:#77A302 !important;'>".$total_itens."</span></a></li>";

		      	return $return;

		      	// colocar símbolo

		      }else {

				$return = "

				<li style='border-left:1px solid #ccc;'><a href='#'><img src='".URL::asset("img/menu/cart.png")."' title='Seu carrinho de compras está vazio' style='vertical-align:middle;width:20px;height:20px;'> <span class='badge' style='background:#77A302 !important;'>0</span></a></li>";

		      	return $return;

		      }

		}

		public static function limpaCarrinho(){

	      	$all_skus = Modelo::where("ativo","=",1)->get();

	      	$retornar = 0;
	      	$total_itens = 0;
			foreach ($all_skus as $modelos) {

	      		$verificar_beta = $modelos->sku;

	      		$array_verificador = range(1,15);

				foreach ($array_verificador as $value) {

					$beta = $verificar_beta."-".$value;

					if (Session::has($beta)) {

		      			Session::forget($beta);

					
		      		}
		      		
		      	}

		     }

		}




	public static function AtualizaEstoque($id_pedido){

      	$itens = PedidoItem::where('id_pedido', '=', $id_pedido)->get();
      	
      	foreach ($itens as $item) {

      		$dados_item = Modelo::where('sku','=',$item->sku)->first();

      		$nova_quantidade = ($dados_item->quantidade-1);


      		if ($nova_quantidade<=0) {

      			$item = Modelo::find($dados_item->id);
      			$item->ativo = "0";
      			$item->save();
      		
      		}else { 

      			$item = Modelo::find($dados_item->id);
      			$item->quantidade = $nova_quantidade;
      			$item->save();


      		}

		}

      }  


	}

