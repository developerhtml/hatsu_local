<?php

class LogConfigTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('logs_config')->delete();
        
		\DB::table('logs_config')->insert(array (
			0 => 
			array (
				'id' => 1,
				'tempo_expiracao' => '30',
				'created_at' => '2014-03-24 12:30:49',
				'updated_at' => '2014-03-24 12:30:49',
			),
		));
	}

}
