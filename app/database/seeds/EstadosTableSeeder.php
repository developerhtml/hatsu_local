<?php

class EstadosTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('estados')->truncate();
        
		\DB::table('estados')->insert(array (
			0 => 
			array (
				'id' => 1,
				'nome' => 'Acre',
				'uf' => 'AC',
				'ibge' => 12,
			),
			1 => 
			array (
				'id' => 2,
				'nome' => 'Alagoas',
				'uf' => 'AL',
				'ibge' => 27,
			),
			2 => 
			array (
				'id' => 3,
				'nome' => 'Amazonas',
				'uf' => 'AM',
				'ibge' => 13,
			),
			3 => 
			array (
				'id' => 4,
				'nome' => 'Amapá',
				'uf' => 'AP',
				'ibge' => 16,
			),
			4 => 
			array (
				'id' => 5,
				'nome' => 'Bahia',
				'uf' => 'BA',
				'ibge' => 29,
			),
			5 => 
			array (
				'id' => 6,
				'nome' => 'Ceará',
				'uf' => 'CE',
				'ibge' => 23,
			),
			6 => 
			array (
				'id' => 7,
				'nome' => 'Brasília',
				'uf' => 'DF',
				'ibge' => 53,
			),
			7 => 
			array (
				'id' => 8,
				'nome' => 'Espírito Santo',
				'uf' => 'ES',
				'ibge' => 32,
			),
			8 => 
			array (
				'id' => 9,
				'nome' => 'Goiás',
				'uf' => 'GO',
				'ibge' => 52,
			),
			9 => 
			array (
				'id' => 10,
				'nome' => 'Maranhão',
				'uf' => 'MA',
				'ibge' => 21,
			),
			10 => 
			array (
				'id' => 11,
				'nome' => 'Minas Gerais',
				'uf' => 'MG',
				'ibge' => 31,
			),
			11 => 
			array (
				'id' => 12,
				'nome' => 'Mato Grosso do Sul',
				'uf' => 'MS',
				'ibge' => 50,
			),
			12 => 
			array (
				'id' => 13,
				'nome' => 'Mato Grosso',
				'uf' => 'MT',
				'ibge' => 51,
			),
			13 => 
			array (
				'id' => 14,
				'nome' => 'Pará',
				'uf' => 'PA',
				'ibge' => 15,
			),
			14 => 
			array (
				'id' => 15,
				'nome' => 'Paraíba',
				'uf' => 'PB',
				'ibge' => 25,
			),
			15 => 
			array (
				'id' => 16,
				'nome' => 'Pernambuco',
				'uf' => 'PE',
				'ibge' => 26,
			),
			16 => 
			array (
				'id' => 17,
				'nome' => 'Piauí',
				'uf' => 'PI',
				'ibge' => 22,
			),
			17 => 
			array (
				'id' => 18,
				'nome' => 'Paraná',
				'uf' => 'PR',
				'ibge' => 41,
			),
			18 => 
			array (
				'id' => 19,
				'nome' => 'Rio de Janeiro',
				'uf' => 'RJ',
				'ibge' => 33,
			),
			19 => 
			array (
				'id' => 20,
				'nome' => 'Rio Grande do Norte',
				'uf' => 'RN',
				'ibge' => 24,
			),
			20 => 
			array (
				'id' => 21,
				'nome' => 'Rondônia',
				'uf' => 'RO',
				'ibge' => 11,
			),
			21 => 
			array (
				'id' => 22,
				'nome' => 'Roraima',
				'uf' => 'RR',
				'ibge' => 14,
			),
			22 => 
			array (
				'id' => 23,
				'nome' => 'Rio Grande do Sul',
				'uf' => 'RS',
				'ibge' => 43,
			),
			23 => 
			array (
				'id' => 24,
				'nome' => 'Santa Catarina',
				'uf' => 'SC',
				'ibge' => 42,
			),
			24 => 
			array (
				'id' => 25,
				'nome' => 'Sergipe',
				'uf' => 'SE',
				'ibge' => 28,
			),
			25 => 
			array (
				'id' => 26,
				'nome' => 'São Paulo',
				'uf' => 'SP',
				'ibge' => 35,
			),
			26 => 
			array (
				'id' => 27,
				'nome' => 'Tocantins',
				'uf' => 'TO',
				'ibge' => 17,
			),
		));
	}

}
