<?php

class UsersTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('users')->delete();
        
		\DB::table('users')->insert(array (
			0 => 
			array (
				'id' => 1,
				'tipo' => '1',
				'nome' => 'Administrador do Sistema',
				'email' => 'suporte@18digital.com.br',
				'telefone' => '(11)2937-9356',
				'celular' => '(11)99999-4444',
				'username' => 'admin',
				'password' => Hash::make('admin'),
			),
		));
	}

}
