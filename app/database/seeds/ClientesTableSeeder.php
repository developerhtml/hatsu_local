<?php

class ClientesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('clientes')->delete();
        
		\DB::table('clientes')->insert(array (
			0 => 
			array (
				'id_cliente' => 1,
				'tipo' => 'P',
				'cpf' => '368.207.828-29',
				'cnpj' => '',
				'nome' => 'João da Silva Rodrigues',
				'endereco' => 'Av. Angélica',
				'numero' => 2136,
				'complemento' => 'CJ 93',
				'bairro' => 'Consolação',
				'cidade' => '9668',
				'uf' => '26',
				'cep' => '05438300',
				'telefone' => '(11)4323-2323',
				'celular' => '(11)97878-7878',
				'email' => 'joao.silva@18digital.com.br',
				'endereco_cobranca' => 'Av. Angélica',
				'numero_cobranca' => 2136,
				'complemento_cobranca' => 'CJ 93',
				'bairro_cobranca' => 'Consolação',
				'cidade_cobranca' => '9668',
				'uf_cobranca' => '26',
				'apelido' => 'João',
				'deleted_at' => NULL,
				'created_at' => '2014-03-26 20:31:48',
				'updated_at' => '2014-03-26 20:31:48',
			),
		));
	}

}
