<?php

class LogTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('logs')->delete();
        
		\DB::table('logs')->insert(array (
			0 => 
			array (
				'id_log' => 1,
				'nome_tabela' => 'users',
				'registro' => '1',
				'tipo_alteracao' => 'CADASTRO USUARIO',
				'id_usuario' => '1',
				'data' => '2014-04-08',
				'created_at' => '2014-03-24 12:30:49',
				'updated_at' => '2014-03-24 12:30:49',
			),
		));
	}

}
