<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		
		$this->call('UsersTableSeeder');
		$this->call('CidadesTableSeeder');
		$this->call('EstadosTableSeeder');
		$this->call('ClientesTableSeeder');
		$this->call('FornecedoresTableSeeder');
		$this->call('FuncionariosTableSeeder');
		$this->call('ContasCorrentesTableSeeder');
		$this->call('GruposTableSeeder');
		$this->call('DescricoesTableSeeder');
		$this->call('ImoveisTableSeeder'); 
		$this->call('FornecedoresImoveisTableSeeder');
		$this->call('ContasPagarTableSeeder');
		$this->call('ContasReceberTableSeeder');
		$this->call('LogTableSeeder');
		$this->call('LogConfigTableSeeder');
	}

}