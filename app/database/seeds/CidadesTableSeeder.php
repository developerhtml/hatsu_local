<?php

class CidadesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('cidades')->truncate();
        
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 1,
				'nome' => 'Acrelândia',
				'uf' => 'AC',
				'cep2' => '1200013',
				'estado_cod' => 1,
				'cep' => '69945-000',
			),
			1 => 
			array (
				'id' => 2,
				'nome' => 'Assis Brasil',
				'uf' => 'AC',
				'cep2' => '1200054',
				'estado_cod' => 1,
				'cep' => '69935-000',
			),
			2 => 
			array (
				'id' => 3,
				'nome' => 'Brasiléia',
				'uf' => 'AC',
				'cep2' => '1200104',
				'estado_cod' => 1,
				'cep' => '69932-000',
			),
			3 => 
			array (
				'id' => 4,
				'nome' => 'Bujari',
				'uf' => 'AC',
				'cep2' => '1200138',
				'estado_cod' => 1,
				'cep' => '69926-000',
			),
			4 => 
			array (
				'id' => 5,
				'nome' => 'Capixaba',
				'uf' => 'AC',
				'cep2' => '1200179',
				'estado_cod' => 1,
				'cep' => '69931-000',
			),
			5 => 
			array (
				'id' => 6,
				'nome' => 'Cruzeiro do Sul',
				'uf' => 'AC',
				'cep2' => '1200203',
				'estado_cod' => 1,
				'cep' => '69980-000',
			),
			6 => 
			array (
				'id' => 7,
				'nome' => 'Epitaciolândia',
				'uf' => 'AC',
				'cep2' => '1200252',
				'estado_cod' => 1,
				'cep' => '69934-000',
			),
			7 => 
			array (
				'id' => 8,
				'nome' => 'Feijó',
				'uf' => 'AC',
				'cep2' => '1200302',
				'estado_cod' => 1,
				'cep' => '69960-000',
			),
			8 => 
			array (
				'id' => 9,
				'nome' => 'Jordão',
				'uf' => 'AC',
				'cep2' => '1200328',
				'estado_cod' => 1,
				'cep' => '69975-000',
			),
			9 => 
			array (
				'id' => 10,
				'nome' => 'Mâncio Lima',
				'uf' => 'AC',
				'cep2' => '1200336',
				'estado_cod' => 1,
				'cep' => '69990-000',
			),
			10 => 
			array (
				'id' => 11,
				'nome' => 'Manoel Urbano',
				'uf' => 'AC',
				'cep2' => '1200344',
				'estado_cod' => 1,
				'cep' => '69950-000',
			),
			11 => 
			array (
				'id' => 12,
				'nome' => 'Marechal Thaumaturgo',
				'uf' => 'AC',
				'cep2' => '1200351',
				'estado_cod' => 1,
				'cep' => '69983-000',
			),
			12 => 
			array (
				'id' => 13,
				'nome' => 'Plácido de Castro',
				'uf' => 'AC',
				'cep2' => '1200385',
				'estado_cod' => 1,
				'cep' => '69928-000',
			),
			13 => 
			array (
				'id' => 14,
				'nome' => 'Porto Acre',
				'uf' => 'AC',
				'cep2' => '1200807',
				'estado_cod' => 1,
				'cep' => '69927-000',
			),
			14 => 
			array (
				'id' => 15,
				'nome' => 'Porto Walter',
				'uf' => 'AC',
				'cep2' => '1200393',
				'estado_cod' => 1,
				'cep' => '69982-000',
			),
			15 => 
			array (
				'id' => 16,
				'nome' => 'Rio Branco',
				'uf' => 'AC',
				'cep2' => '1200401',
				'estado_cod' => 1,
				'cep' => 'LOC',
			),
			16 => 
			array (
				'id' => 17,
				'nome' => 'Rodrigues Alves',
				'uf' => 'AC',
				'cep2' => '1200427',
				'estado_cod' => 1,
				'cep' => '69985-000',
			),
			17 => 
			array (
				'id' => 18,
				'nome' => 'Santa Rosa do Purus',
				'uf' => 'AC',
				'cep2' => '1200435',
				'estado_cod' => 1,
				'cep' => '69955-000',
			),
			18 => 
			array (
				'id' => 19,
				'nome' => 'Sena Madureira',
				'uf' => 'AC',
				'cep2' => '1200500',
				'estado_cod' => 1,
				'cep' => '69940-000',
			),
			19 => 
			array (
				'id' => 20,
				'nome' => 'Senador Guiomard',
				'uf' => 'AC',
				'cep2' => '1200450',
				'estado_cod' => 1,
				'cep' => '69925-000',
			),
			20 => 
			array (
				'id' => 21,
				'nome' => 'Tarauacá',
				'uf' => 'AC',
				'cep2' => '1200609',
				'estado_cod' => 1,
				'cep' => '69970-000',
			),
			21 => 
			array (
				'id' => 22,
				'nome' => 'Xapuri',
				'uf' => 'AC',
				'cep2' => '',
				'estado_cod' => 1,
				'cep' => '69930-000',
			),
			22 => 
			array (
				'id' => 23,
				'nome' => 'Água Branca',
				'uf' => 'AL',
				'cep2' => '2700102',
				'estado_cod' => 2,
				'cep' => '57490-000',
			),
			23 => 
			array (
				'id' => 26,
				'nome' => 'Anadia',
				'uf' => 'AL',
				'cep2' => '2700201',
				'estado_cod' => 2,
				'cep' => '57660-000',
			),
			24 => 
			array (
				'id' => 30,
				'nome' => 'Arapiraca',
				'uf' => 'AL',
				'cep2' => '2700300',
				'estado_cod' => 2,
				'cep' => 'LOC',
			),
			25 => 
			array (
				'id' => 31,
				'nome' => 'Atalaia',
				'uf' => 'AL',
				'cep2' => '2700409',
				'estado_cod' => 2,
				'cep' => '57690-000',
			),
			26 => 
			array (
				'id' => 36,
				'nome' => 'Barra de Santo Antônio',
				'uf' => 'AL',
				'cep2' => '2700508',
				'estado_cod' => 2,
				'cep' => '57925-000',
			),
			27 => 
			array (
				'id' => 37,
				'nome' => 'Barra de São Miguel',
				'uf' => 'AL',
				'cep2' => '2700607',
				'estado_cod' => 2,
				'cep' => '57180-000',
			),
			28 => 
			array (
				'id' => 40,
				'nome' => 'Batalha',
				'uf' => 'AL',
				'cep2' => '2700706',
				'estado_cod' => 2,
				'cep' => '57420-000',
			),
			29 => 
			array (
				'id' => 42,
				'nome' => 'Belém',
				'uf' => 'AL',
				'cep2' => '2700805',
				'estado_cod' => 2,
				'cep' => '57630-000',
			),
			30 => 
			array (
				'id' => 43,
				'nome' => 'Belo Monte',
				'uf' => 'AL',
				'cep2' => '2700904',
				'estado_cod' => 2,
				'cep' => '57435-000',
			),
			31 => 
			array (
				'id' => 47,
				'nome' => 'Boca da Mata',
				'uf' => 'AL',
				'cep2' => '2701001',
				'estado_cod' => 2,
				'cep' => '57680-000',
			),
			32 => 
			array (
				'id' => 50,
				'nome' => 'Branquinha',
				'uf' => 'AL',
				'cep2' => '2701100',
				'estado_cod' => 2,
				'cep' => '57830-000',
			),
			33 => 
			array (
				'id' => 51,
				'nome' => 'Cacimbinhas',
				'uf' => 'AL',
				'cep2' => '2701209',
				'estado_cod' => 2,
				'cep' => '57570-000',
			),
			34 => 
			array (
				'id' => 53,
				'nome' => 'Cajueiro',
				'uf' => 'AL',
				'cep2' => '2701308',
				'estado_cod' => 2,
				'cep' => '57770-000',
			),
			35 => 
			array (
				'id' => 56,
				'nome' => 'Campestre',
				'uf' => 'AL',
				'cep2' => '2701357',
				'estado_cod' => 2,
				'cep' => '57968-000',
			),
			36 => 
			array (
				'id' => 57,
				'nome' => 'Campo Alegre',
				'uf' => 'AL',
				'cep2' => '2701407',
				'estado_cod' => 2,
				'cep' => '57250-000',
			),
			37 => 
			array (
				'id' => 58,
				'nome' => 'Campo Grande',
				'uf' => 'AL',
				'cep2' => '2701506',
				'estado_cod' => 2,
				'cep' => '57350-000',
			),
			38 => 
			array (
				'id' => 61,
				'nome' => 'Canapi',
				'uf' => 'AL',
				'cep2' => '2701605',
				'estado_cod' => 2,
				'cep' => '57530-000',
			),
			39 => 
			array (
				'id' => 64,
				'nome' => 'Capela',
				'uf' => 'AL',
				'cep2' => '2701704',
				'estado_cod' => 2,
				'cep' => '57780-000',
			),
			40 => 
			array (
				'id' => 65,
				'nome' => 'Carneiros',
				'uf' => 'AL',
				'cep2' => '2701803',
				'estado_cod' => 2,
				'cep' => '57535-000',
			),
			41 => 
			array (
				'id' => 67,
				'nome' => 'Chã Preta',
				'uf' => 'AL',
				'cep2' => '2701902',
				'estado_cod' => 2,
				'cep' => '57760-000',
			),
			42 => 
			array (
				'id' => 68,
				'nome' => 'Coité do Nóia',
				'uf' => 'AL',
				'cep2' => '2702009',
				'estado_cod' => 2,
				'cep' => '57325-000',
			),
			43 => 
			array (
				'id' => 69,
				'nome' => 'Colônia Leopoldina',
				'uf' => 'AL',
				'cep2' => '2702108',
				'estado_cod' => 2,
				'cep' => '57975-000',
			),
			44 => 
			array (
				'id' => 70,
				'nome' => 'Coqueiro Seco',
				'uf' => 'AL',
				'cep2' => '2702207',
				'estado_cod' => 2,
				'cep' => '57140-000',
			),
			45 => 
			array (
				'id' => 71,
				'nome' => 'Coruripe',
				'uf' => 'AL',
				'cep2' => '2702306',
				'estado_cod' => 2,
				'cep' => '57230-000',
			),
			46 => 
			array (
				'id' => 73,
				'nome' => 'Craíbas',
				'uf' => 'AL',
				'cep2' => '2702355',
				'estado_cod' => 2,
				'cep' => '57320-000',
			),
			47 => 
			array (
				'id' => 74,
				'nome' => 'Delmiro Gouveia',
				'uf' => 'AL',
				'cep2' => '2702405',
				'estado_cod' => 2,
				'cep' => '57480-000',
			),
			48 => 
			array (
				'id' => 75,
				'nome' => 'Dois Riachos',
				'uf' => 'AL',
				'cep2' => '2702504',
				'estado_cod' => 2,
				'cep' => '57560-000',
			),
			49 => 
			array (
				'id' => 77,
				'nome' => 'Estrela de Alagoas',
				'uf' => 'AL',
				'cep2' => '2702553',
				'estado_cod' => 2,
				'cep' => '57625-000',
			),
			50 => 
			array (
				'id' => 78,
				'nome' => 'Feira Grande',
				'uf' => 'AL',
				'cep2' => '2702603',
				'estado_cod' => 2,
				'cep' => '57340-000',
			),
			51 => 
			array (
				'id' => 79,
				'nome' => 'Feliz Deserto',
				'uf' => 'AL',
				'cep2' => '2702702',
				'estado_cod' => 2,
				'cep' => '57220-000',
			),
			52 => 
			array (
				'id' => 81,
				'nome' => 'Flexeiras',
				'uf' => 'AL',
				'cep2' => '2702801',
				'estado_cod' => 2,
				'cep' => '57995-000',
			),
			53 => 
			array (
				'id' => 84,
				'nome' => 'Girau do Ponciano',
				'uf' => 'AL',
				'cep2' => '2702900',
				'estado_cod' => 2,
				'cep' => '57360-000',
			),
			54 => 
			array (
				'id' => 85,
				'nome' => 'Ibateguara',
				'uf' => 'AL',
				'cep2' => '2703007',
				'estado_cod' => 2,
				'cep' => '57890-000',
			),
			55 => 
			array (
				'id' => 86,
				'nome' => 'Igaci',
				'uf' => 'AL',
				'cep2' => '2703106',
				'estado_cod' => 2,
				'cep' => '57620-000',
			),
			56 => 
			array (
				'id' => 87,
				'nome' => 'Igreja Nova',
				'uf' => 'AL',
				'cep2' => '2703205',
				'estado_cod' => 2,
				'cep' => '57280-000',
			),
			57 => 
			array (
				'id' => 88,
				'nome' => 'Inhapi',
				'uf' => 'AL',
				'cep2' => '2703304',
				'estado_cod' => 2,
				'cep' => '57545-000',
			),
			58 => 
			array (
				'id' => 89,
				'nome' => 'Jacaré dos Homens',
				'uf' => 'AL',
				'cep2' => '2703403',
				'estado_cod' => 2,
				'cep' => '57430-000',
			),
			59 => 
			array (
				'id' => 90,
				'nome' => 'Jacuípe',
				'uf' => 'AL',
				'cep2' => '2703502',
				'estado_cod' => 2,
				'cep' => '57960-000',
			),
			60 => 
			array (
				'id' => 91,
				'nome' => 'Japaratinga',
				'uf' => 'AL',
				'cep2' => '2703601',
				'estado_cod' => 2,
				'cep' => '57950-000',
			),
			61 => 
			array (
				'id' => 92,
				'nome' => 'Jaramataia',
				'uf' => 'AL',
				'cep2' => '2703700',
				'estado_cod' => 2,
				'cep' => '57425-000',
			),
			62 => 
			array (
				'id' => 94,
				'nome' => 'Joaquim Gomes',
				'uf' => 'AL',
				'cep2' => '2703809',
				'estado_cod' => 2,
				'cep' => '57980-000',
			),
			63 => 
			array (
				'id' => 95,
				'nome' => 'Jundiá',
				'uf' => 'AL',
				'cep2' => '2703908',
				'estado_cod' => 2,
				'cep' => '57965-000',
			),
			64 => 
			array (
				'id' => 96,
				'nome' => 'Junqueiro',
				'uf' => 'AL',
				'cep2' => '2704005',
				'estado_cod' => 2,
				'cep' => '57270-000',
			),
			65 => 
			array (
				'id' => 98,
				'nome' => 'Lagoa da Canoa',
				'uf' => 'AL',
				'cep2' => '2704104',
				'estado_cod' => 2,
				'cep' => '57330-000',
			),
			66 => 
			array (
				'id' => 108,
				'nome' => 'Limoeiro de Anadia',
				'uf' => 'AL',
				'cep2' => '2704203',
				'estado_cod' => 2,
				'cep' => '57260-000',
			),
			67 => 
			array (
				'id' => 109,
				'nome' => 'Maceió',
				'uf' => 'AL',
				'cep2' => '2704302',
				'estado_cod' => 2,
				'cep' => 'LOC',
			),
			68 => 
			array (
				'id' => 110,
				'nome' => 'Major Isidoro',
				'uf' => 'AL',
				'cep2' => '2704401',
				'estado_cod' => 2,
				'cep' => '57580-000',
			),
			69 => 
			array (
				'id' => 111,
				'nome' => 'Mar Vermelho',
				'uf' => 'AL',
				'cep2' => '2704906',
				'estado_cod' => 2,
				'cep' => '57730-000',
			),
			70 => 
			array (
				'id' => 112,
				'nome' => 'Maragogi',
				'uf' => 'AL',
				'cep2' => '2704500',
				'estado_cod' => 2,
				'cep' => '57955-000',
			),
			71 => 
			array (
				'id' => 113,
				'nome' => 'Maravilha',
				'uf' => 'AL',
				'cep2' => '2704609',
				'estado_cod' => 2,
				'cep' => '57520-000',
			),
			72 => 
			array (
				'id' => 114,
				'nome' => 'Marechal Deodoro',
				'uf' => 'AL',
				'cep2' => '2704708',
				'estado_cod' => 2,
				'cep' => '57160-000',
			),
			73 => 
			array (
				'id' => 115,
				'nome' => 'Maribondo',
				'uf' => 'AL',
				'cep2' => '2704807',
				'estado_cod' => 2,
				'cep' => '57670-000',
			),
			74 => 
			array (
				'id' => 117,
				'nome' => 'Mata Grande',
				'uf' => 'AL',
				'cep2' => '2705002',
				'estado_cod' => 2,
				'cep' => '57540-000',
			),
			75 => 
			array (
				'id' => 118,
				'nome' => 'Matriz de Camaragibe',
				'uf' => 'AL',
				'cep2' => '2705101',
				'estado_cod' => 2,
				'cep' => '57910-000',
			),
			76 => 
			array (
				'id' => 119,
				'nome' => 'Messias',
				'uf' => 'AL',
				'cep2' => '2705200',
				'estado_cod' => 2,
				'cep' => '57990-000',
			),
			77 => 
			array (
				'id' => 120,
				'nome' => 'Minador do Negrão',
				'uf' => 'AL',
				'cep2' => '2705309',
				'estado_cod' => 2,
				'cep' => '57615-000',
			),
			78 => 
			array (
				'id' => 121,
				'nome' => 'Monteirópolis',
				'uf' => 'AL',
				'cep2' => '2705408',
				'estado_cod' => 2,
				'cep' => '57440-000',
			),
			79 => 
			array (
				'id' => 124,
				'nome' => 'Murici',
				'uf' => 'AL',
				'cep2' => '2705507',
				'estado_cod' => 2,
				'cep' => '57820-000',
			),
			80 => 
			array (
				'id' => 125,
				'nome' => 'Novo Lino',
				'uf' => 'AL',
				'cep2' => '2705606',
				'estado_cod' => 2,
				'cep' => '57970-000',
			),
			81 => 
			array (
				'id' => 126,
				'nome' => 'Olho D\'Água Grande',
				'uf' => 'AL',
				'cep2' => '2705903',
				'estado_cod' => 2,
				'cep' => '57390-000',
			),
			82 => 
			array (
				'id' => 127,
				'nome' => 'Olho D\'Água das Flores',
				'uf' => 'AL',
				'cep2' => '2705705',
				'estado_cod' => 2,
				'cep' => '57442-000',
			),
			83 => 
			array (
				'id' => 129,
				'nome' => 'Olho D\'Água do Casado',
				'uf' => 'AL',
				'cep2' => '2705804',
				'estado_cod' => 2,
				'cep' => '57470-000',
			),
			84 => 
			array (
				'id' => 131,
				'nome' => 'Olivença',
				'uf' => 'AL',
				'cep2' => '2706000',
				'estado_cod' => 2,
				'cep' => '57550-000',
			),
			85 => 
			array (
				'id' => 132,
				'nome' => 'Ouro Branco',
				'uf' => 'AL',
				'cep2' => '2706109',
				'estado_cod' => 2,
				'cep' => '57525-000',
			),
			86 => 
			array (
				'id' => 133,
				'nome' => 'Palestina',
				'uf' => 'AL',
				'cep2' => '2706208',
				'estado_cod' => 2,
				'cep' => '57410-000',
			),
			87 => 
			array (
				'id' => 135,
				'nome' => 'Palmeira dos Índios',
				'uf' => 'AL',
				'cep2' => '2706307',
				'estado_cod' => 2,
				'cep' => 'LOC',
			),
			88 => 
			array (
				'id' => 136,
				'nome' => 'Pão de Açúcar',
				'uf' => 'AL',
				'cep2' => '2706406',
				'estado_cod' => 2,
				'cep' => '57400-000',
			),
			89 => 
			array (
				'id' => 137,
				'nome' => 'Pariconha',
				'uf' => 'AL',
				'cep2' => '2706422',
				'estado_cod' => 2,
				'cep' => '57475-000',
			),
			90 => 
			array (
				'id' => 138,
				'nome' => 'Paripueira',
				'uf' => 'AL',
				'cep2' => '2706448',
				'estado_cod' => 2,
				'cep' => '57935-000',
			),
			91 => 
			array (
				'id' => 139,
				'nome' => 'Passo de Camaragibe',
				'uf' => 'AL',
				'cep2' => '2706505',
				'estado_cod' => 2,
				'cep' => '57930-000',
			),
			92 => 
			array (
				'id' => 142,
				'nome' => 'Paulo Jacinto',
				'uf' => 'AL',
				'cep2' => '2706604',
				'estado_cod' => 2,
				'cep' => '57740-000',
			),
			93 => 
			array (
				'id' => 143,
				'nome' => 'Penedo',
				'uf' => 'AL',
				'cep2' => '2706703',
				'estado_cod' => 2,
				'cep' => '57200-000',
			),
			94 => 
			array (
				'id' => 144,
				'nome' => 'Piaçabuçu',
				'uf' => 'AL',
				'cep2' => '2706802',
				'estado_cod' => 2,
				'cep' => '57210-000',
			),
			95 => 
			array (
				'id' => 145,
				'nome' => 'Pilar',
				'uf' => 'AL',
				'cep2' => '2706901',
				'estado_cod' => 2,
				'cep' => '57150-000',
			),
			96 => 
			array (
				'id' => 146,
				'nome' => 'Pindoba',
				'uf' => 'AL',
				'cep2' => '2707008',
				'estado_cod' => 2,
				'cep' => '57720-000',
			),
			97 => 
			array (
				'id' => 147,
				'nome' => 'Piranhas',
				'uf' => 'AL',
				'cep2' => '2707107',
				'estado_cod' => 2,
				'cep' => '57460-000',
			),
			98 => 
			array (
				'id' => 150,
				'nome' => 'Poço das Trincheiras',
				'uf' => 'AL',
				'cep2' => '2707206',
				'estado_cod' => 2,
				'cep' => '57510-000',
			),
			99 => 
			array (
				'id' => 151,
				'nome' => 'Porto Calvo',
				'uf' => 'AL',
				'cep2' => '2707305',
				'estado_cod' => 2,
				'cep' => '57900-000',
			),
			100 => 
			array (
				'id' => 152,
				'nome' => 'Porto de Pedras',
				'uf' => 'AL',
				'cep2' => '2707404',
				'estado_cod' => 2,
				'cep' => '57945-000',
			),
			101 => 
			array (
				'id' => 153,
				'nome' => 'Porto Real do Colégio',
				'uf' => 'AL',
				'cep2' => '2707503',
				'estado_cod' => 2,
				'cep' => '57290-000',
			),
			102 => 
			array (
				'id' => 155,
				'nome' => 'Quebrangulo',
				'uf' => 'AL',
				'cep2' => '',
				'estado_cod' => 2,
				'cep' => '57750-000',
			),
			103 => 
			array (
				'id' => 158,
				'nome' => 'Rio Largo',
				'uf' => 'AL',
				'cep2' => '2707701',
				'estado_cod' => 2,
				'cep' => '57100-000',
			),
			104 => 
			array (
				'id' => 160,
				'nome' => 'Roteiro',
				'uf' => 'AL',
				'cep2' => '2707800',
				'estado_cod' => 2,
				'cep' => '57246-000',
			),
			105 => 
			array (
				'id' => 162,
				'nome' => 'Santa Luzia do Norte',
				'uf' => 'AL',
				'cep2' => '2707909',
				'estado_cod' => 2,
				'cep' => '57130-000',
			),
			106 => 
			array (
				'id' => 163,
				'nome' => 'Santana do Ipanema',
				'uf' => 'AL',
				'cep2' => '2708006',
				'estado_cod' => 2,
				'cep' => '57500-000',
			),
			107 => 
			array (
				'id' => 164,
				'nome' => 'Santana do Mundaú',
				'uf' => 'AL',
				'cep2' => '2708105',
				'estado_cod' => 2,
				'cep' => '57840-000',
			),
			108 => 
			array (
				'id' => 166,
				'nome' => 'São Brás',
				'uf' => 'AL',
				'cep2' => '2708204',
				'estado_cod' => 2,
				'cep' => '57380-000',
			),
			109 => 
			array (
				'id' => 167,
				'nome' => 'São José da Laje',
				'uf' => 'AL',
				'cep2' => '2708303',
				'estado_cod' => 2,
				'cep' => '57860-000',
			),
			110 => 
			array (
				'id' => 168,
				'nome' => 'São José da Tapera',
				'uf' => 'AL',
				'cep2' => '2708402',
				'estado_cod' => 2,
				'cep' => '57445-000',
			),
			111 => 
			array (
				'id' => 169,
				'nome' => 'São Luís do Quitunde',
				'uf' => 'AL',
				'cep2' => '2708501',
				'estado_cod' => 2,
				'cep' => '57920-000',
			),
			112 => 
			array (
				'id' => 170,
				'nome' => 'São Miguel dos Campos',
				'uf' => 'AL',
				'cep2' => '2708600',
				'estado_cod' => 2,
				'cep' => '57240-000',
			),
			113 => 
			array (
				'id' => 171,
				'nome' => 'São Miguel dos Milagres',
				'uf' => 'AL',
				'cep2' => '2708709',
				'estado_cod' => 2,
				'cep' => '57940-000',
			),
			114 => 
			array (
				'id' => 172,
				'nome' => 'São Sebastião',
				'uf' => 'AL',
				'cep2' => '2708808',
				'estado_cod' => 2,
				'cep' => '57275-000',
			),
			115 => 
			array (
				'id' => 175,
				'nome' => 'Satuba',
				'uf' => 'AL',
				'cep2' => '2708907',
				'estado_cod' => 2,
				'cep' => '57120-000',
			),
			116 => 
			array (
				'id' => 176,
				'nome' => 'Senador Rui Palmeira',
				'uf' => 'AL',
				'cep2' => '2708956',
				'estado_cod' => 2,
				'cep' => '57515-000',
			),
			117 => 
			array (
				'id' => 181,
				'nome' => 'Tanque D\'Arca',
				'uf' => 'AL',
				'cep2' => '2709004',
				'estado_cod' => 2,
				'cep' => '57635-000',
			),
			118 => 
			array (
				'id' => 182,
				'nome' => 'Taquarana',
				'uf' => 'AL',
				'cep2' => '2709103',
				'estado_cod' => 2,
				'cep' => '57640-000',
			),
			119 => 
			array (
				'id' => 184,
				'nome' => 'Teotônio Vilela',
				'uf' => 'AL',
				'cep2' => '2709152',
				'estado_cod' => 2,
				'cep' => '57265-000',
			),
			120 => 
			array (
				'id' => 185,
				'nome' => 'Traipu',
				'uf' => 'AL',
				'cep2' => '2709202',
				'estado_cod' => 2,
				'cep' => '57370-000',
			),
			121 => 
			array (
				'id' => 186,
				'nome' => 'União dos Palmares',
				'uf' => 'AL',
				'cep2' => '2709301',
				'estado_cod' => 2,
				'cep' => '57800-000',
			),
			122 => 
			array (
				'id' => 188,
				'nome' => 'Viçosa',
				'uf' => 'AL',
				'cep2' => '2709400',
				'estado_cod' => 2,
				'cep' => '57700-000',
			),
			123 => 
			array (
				'id' => 191,
				'nome' => 'Alvarães',
				'uf' => 'AM',
				'cep2' => '1300029',
				'estado_cod' => 3,
				'cep' => '69475-000',
			),
			124 => 
			array (
				'id' => 193,
				'nome' => 'Amaturá',
				'uf' => 'AM',
				'cep2' => '1300060',
				'estado_cod' => 3,
				'cep' => '69620-000',
			),
			125 => 
			array (
				'id' => 194,
				'nome' => 'Anamã',
				'uf' => 'AM',
				'cep2' => '1300086',
				'estado_cod' => 3,
				'cep' => '69445-000',
			),
			126 => 
			array (
				'id' => 195,
				'nome' => 'Anori',
				'uf' => 'AM',
				'cep2' => '1300102',
				'estado_cod' => 3,
				'cep' => '69440-000',
			),
			127 => 
			array (
				'id' => 196,
				'nome' => 'Apuí',
				'uf' => 'AM',
				'cep2' => '1300144',
				'estado_cod' => 3,
				'cep' => '69265-000',
			),
			128 => 
			array (
				'id' => 198,
				'nome' => 'Atalaia do Norte',
				'uf' => 'AM',
				'cep2' => '1300201',
				'estado_cod' => 3,
				'cep' => '69650-000',
			),
			129 => 
			array (
				'id' => 200,
				'nome' => 'Autazes',
				'uf' => 'AM',
				'cep2' => '1300300',
				'estado_cod' => 3,
				'cep' => '69240-000',
			),
			130 => 
			array (
				'id' => 204,
				'nome' => 'Barcelos',
				'uf' => 'AM',
				'cep2' => '1300409',
				'estado_cod' => 3,
				'cep' => '69700-000',
			),
			131 => 
			array (
				'id' => 205,
				'nome' => 'Barreirinha',
				'uf' => 'AM',
				'cep2' => '1300508',
				'estado_cod' => 3,
				'cep' => '69160-000',
			),
			132 => 
			array (
				'id' => 206,
				'nome' => 'Benjamin Constant',
				'uf' => 'AM',
				'cep2' => '1300607',
				'estado_cod' => 3,
				'cep' => '69630-000',
			),
			133 => 
			array (
				'id' => 207,
				'nome' => 'Beruri',
				'uf' => 'AM',
				'cep2' => '1300631',
				'estado_cod' => 3,
				'cep' => '69430-000',
			),
			134 => 
			array (
				'id' => 208,
				'nome' => 'Boa Vista do Ramos',
				'uf' => 'AM',
				'cep2' => '1300680',
				'estado_cod' => 3,
				'cep' => '69195-000',
			),
			135 => 
			array (
				'id' => 209,
				'nome' => 'Boca do Acre',
				'uf' => 'AM',
				'cep2' => '1300706',
				'estado_cod' => 3,
				'cep' => '69850-000',
			),
			136 => 
			array (
				'id' => 210,
				'nome' => 'Borba',
				'uf' => 'AM',
				'cep2' => '1300805',
				'estado_cod' => 3,
				'cep' => '69200-000',
			),
			137 => 
			array (
				'id' => 211,
				'nome' => 'Caapiranga',
				'uf' => 'AM',
				'cep2' => '1300839',
				'estado_cod' => 3,
				'cep' => '69425-000',
			),
			138 => 
			array (
				'id' => 214,
				'nome' => 'Canutama',
				'uf' => 'AM',
				'cep2' => '1300904',
				'estado_cod' => 3,
				'cep' => '69820-000',
			),
			139 => 
			array (
				'id' => 215,
				'nome' => 'Carauari',
				'uf' => 'AM',
				'cep2' => '1301001',
				'estado_cod' => 3,
				'cep' => '69500-000',
			),
			140 => 
			array (
				'id' => 216,
				'nome' => 'Careiro',
				'uf' => 'AM',
				'cep2' => '1301100',
				'estado_cod' => 3,
				'cep' => '69250-000',
			),
			141 => 
			array (
				'id' => 217,
				'nome' => 'Careiro da Várzea',
				'uf' => 'AM',
				'cep2' => '1301159',
				'estado_cod' => 3,
				'cep' => '69255-000',
			),
			142 => 
			array (
				'id' => 219,
				'nome' => 'Coari',
				'uf' => 'AM',
				'cep2' => '1301209',
				'estado_cod' => 3,
				'cep' => '69460-000',
			),
			143 => 
			array (
				'id' => 220,
				'nome' => 'Codajás',
				'uf' => 'AM',
				'cep2' => '1301308',
				'estado_cod' => 3,
				'cep' => '69450-000',
			),
			144 => 
			array (
				'id' => 222,
				'nome' => 'Eirunepé',
				'uf' => 'AM',
				'cep2' => '1301407',
				'estado_cod' => 3,
				'cep' => '69880-000',
			),
			145 => 
			array (
				'id' => 223,
				'nome' => 'Envira',
				'uf' => 'AM',
				'cep2' => '1301506',
				'estado_cod' => 3,
				'cep' => '69870-000',
			),
			146 => 
			array (
				'id' => 225,
				'nome' => 'Fonte Boa',
				'uf' => 'AM',
				'cep2' => '1301605',
				'estado_cod' => 3,
				'cep' => '69670-000',
			),
			147 => 
			array (
				'id' => 227,
				'nome' => 'Guajará',
				'uf' => 'AM',
				'cep2' => '1301654',
				'estado_cod' => 3,
				'cep' => '69895-000',
			),
			148 => 
			array (
				'id' => 228,
				'nome' => 'Humaitá',
				'uf' => 'AM',
				'cep2' => '1301704',
				'estado_cod' => 3,
				'cep' => '69800-000',
			),
			149 => 
			array (
				'id' => 231,
				'nome' => 'Ipixuna',
				'uf' => 'AM',
				'cep2' => '1301803',
				'estado_cod' => 3,
				'cep' => '69890-000',
			),
			150 => 
			array (
				'id' => 232,
				'nome' => 'Iranduba',
				'uf' => 'AM',
				'cep2' => '1301852',
				'estado_cod' => 3,
				'cep' => '69415-000',
			),
			151 => 
			array (
				'id' => 233,
				'nome' => 'Itacoatiara',
				'uf' => 'AM',
				'cep2' => '1301902',
				'estado_cod' => 3,
				'cep' => 'LOC',
			),
			152 => 
			array (
				'id' => 234,
				'nome' => 'Itamarati',
				'uf' => 'AM',
				'cep2' => '1301951',
				'estado_cod' => 3,
				'cep' => '69510-000',
			),
			153 => 
			array (
				'id' => 235,
				'nome' => 'Itapiranga',
				'uf' => 'AM',
				'cep2' => '1302009',
				'estado_cod' => 3,
				'cep' => '69120-000',
			),
			154 => 
			array (
				'id' => 236,
				'nome' => 'Japurá',
				'uf' => 'AM',
				'cep2' => '1302108',
				'estado_cod' => 3,
				'cep' => '69495-000',
			),
			155 => 
			array (
				'id' => 237,
				'nome' => 'Juruá',
				'uf' => 'AM',
				'cep2' => '1302207',
				'estado_cod' => 3,
				'cep' => '69520-000',
			),
			156 => 
			array (
				'id' => 238,
				'nome' => 'Jutaí',
				'uf' => 'AM',
				'cep2' => '1302306',
				'estado_cod' => 3,
				'cep' => '69660-000',
			),
			157 => 
			array (
				'id' => 239,
				'nome' => 'Lábrea',
				'uf' => 'AM',
				'cep2' => '1302405',
				'estado_cod' => 3,
				'cep' => '69830-000',
			),
			158 => 
			array (
				'id' => 241,
				'nome' => 'Manacapuru',
				'uf' => 'AM',
				'cep2' => '1302504',
				'estado_cod' => 3,
				'cep' => '69400-000',
			),
			159 => 
			array (
				'id' => 242,
				'nome' => 'Manaquiri',
				'uf' => 'AM',
				'cep2' => '1302553',
				'estado_cod' => 3,
				'cep' => '69435-000',
			),
			160 => 
			array (
				'id' => 243,
				'nome' => 'Manaus',
				'uf' => 'AM',
				'cep2' => '1302603',
				'estado_cod' => 3,
				'cep' => 'LOC',
			),
			161 => 
			array (
				'id' => 244,
				'nome' => 'Manicoré',
				'uf' => 'AM',
				'cep2' => '1302702',
				'estado_cod' => 3,
				'cep' => '69280-000',
			),
			162 => 
			array (
				'id' => 245,
				'nome' => 'Maraã',
				'uf' => 'AM',
				'cep2' => '1302801',
				'estado_cod' => 3,
				'cep' => '69490-000',
			),
			163 => 
			array (
				'id' => 247,
				'nome' => 'Maués',
				'uf' => 'AM',
				'cep2' => '1302900',
				'estado_cod' => 3,
				'cep' => '69190-000',
			),
			164 => 
			array (
				'id' => 251,
				'nome' => 'Nhamundá',
				'uf' => 'AM',
				'cep2' => '1303007',
				'estado_cod' => 3,
				'cep' => '69140-000',
			),
			165 => 
			array (
				'id' => 252,
				'nome' => 'Nova Olinda do Norte',
				'uf' => 'AM',
				'cep2' => '1303106',
				'estado_cod' => 3,
				'cep' => '69230-000',
			),
			166 => 
			array (
				'id' => 253,
				'nome' => 'Novo Airão',
				'uf' => 'AM',
				'cep2' => '1303205',
				'estado_cod' => 3,
				'cep' => '69730-000',
			),
			167 => 
			array (
				'id' => 254,
				'nome' => 'Novo Aripuanã',
				'uf' => 'AM',
				'cep2' => '1303304',
				'estado_cod' => 3,
				'cep' => '69260-000',
			),
			168 => 
			array (
				'id' => 256,
				'nome' => 'Parintins',
				'uf' => 'AM',
				'cep2' => '1303403',
				'estado_cod' => 3,
				'cep' => 'LOC',
			),
			169 => 
			array (
				'id' => 257,
				'nome' => 'Pauini',
				'uf' => 'AM',
				'cep2' => '1303502',
				'estado_cod' => 3,
				'cep' => '69860-000',
			),
			170 => 
			array (
				'id' => 259,
				'nome' => 'Presidente Figueiredo',
				'uf' => 'AM',
				'cep2' => '1303536',
				'estado_cod' => 3,
				'cep' => '69735-000',
			),
			171 => 
			array (
				'id' => 261,
				'nome' => 'Rio Preto da Eva',
				'uf' => 'AM',
				'cep2' => '1303569',
				'estado_cod' => 3,
				'cep' => '69117-000',
			),
			172 => 
			array (
				'id' => 262,
				'nome' => 'Santa Isabel do Rio Negro',
				'uf' => 'AM',
				'cep2' => '1303601',
				'estado_cod' => 3,
				'cep' => '69740-000',
			),
			173 => 
			array (
				'id' => 264,
				'nome' => 'Santo Antônio do Içá',
				'uf' => 'AM',
				'cep2' => '',
				'estado_cod' => 3,
				'cep' => '69680-000',
			),
			174 => 
			array (
				'id' => 266,
				'nome' => 'São Gabriel da Cachoeira',
				'uf' => 'AM',
				'cep2' => '1303809',
				'estado_cod' => 3,
				'cep' => '69750-000',
			),
			175 => 
			array (
				'id' => 267,
				'nome' => 'São Paulo de Olivença',
				'uf' => 'AM',
				'cep2' => '1303908',
				'estado_cod' => 3,
				'cep' => '69600-000',
			),
			176 => 
			array (
				'id' => 268,
				'nome' => 'São Sebastião do Uatumã',
				'uf' => 'AM',
				'cep2' => '1303957',
				'estado_cod' => 3,
				'cep' => '69135-000',
			),
			177 => 
			array (
				'id' => 269,
				'nome' => 'Silves',
				'uf' => 'AM',
				'cep2' => '1304005',
				'estado_cod' => 3,
				'cep' => '69114-000',
			),
			178 => 
			array (
				'id' => 270,
				'nome' => 'Tabatinga',
				'uf' => 'AM',
				'cep2' => '1304062',
				'estado_cod' => 3,
				'cep' => '69640-000',
			),
			179 => 
			array (
				'id' => 271,
				'nome' => 'Tapauá',
				'uf' => 'AM',
				'cep2' => '1304104',
				'estado_cod' => 3,
				'cep' => '69480-000',
			),
			180 => 
			array (
				'id' => 272,
				'nome' => 'Tefé',
				'uf' => 'AM',
				'cep2' => '1304203',
				'estado_cod' => 3,
				'cep' => '69470-000',
			),
			181 => 
			array (
				'id' => 273,
				'nome' => 'Tonantins',
				'uf' => 'AM',
				'cep2' => '1304237',
				'estado_cod' => 3,
				'cep' => '69685-000',
			),
			182 => 
			array (
				'id' => 274,
				'nome' => 'Uarini',
				'uf' => 'AM',
				'cep2' => '1304260',
				'estado_cod' => 3,
				'cep' => '69485-000',
			),
			183 => 
			array (
				'id' => 275,
				'nome' => 'Urucará',
				'uf' => 'AM',
				'cep2' => '1304302',
				'estado_cod' => 3,
				'cep' => '69130-000',
			),
			184 => 
			array (
				'id' => 276,
				'nome' => 'Urucurituba',
				'uf' => 'AM',
				'cep2' => '1304401',
				'estado_cod' => 3,
				'cep' => '69180-000',
			),
			185 => 
			array (
				'id' => 280,
				'nome' => 'Amapá',
				'uf' => 'AP',
				'cep2' => '1600105',
				'estado_cod' => 4,
				'cep' => '68950-000',
			),
			186 => 
			array (
				'id' => 281,
				'nome' => 'Amapari',
				'uf' => 'AP',
				'cep2' => 'nao tem',
				'estado_cod' => 4,
				'cep' => '68945-000',
			),
			187 => 
			array (
				'id' => 287,
				'nome' => 'Calçoene',
				'uf' => 'AP',
				'cep2' => '1600204',
				'estado_cod' => 4,
				'cep' => '68960-000',
			),
			188 => 
			array (
				'id' => 294,
				'nome' => 'Cutias',
				'uf' => 'AP',
				'cep2' => '1600212',
				'estado_cod' => 4,
				'cep' => '68973-000',
			),
			189 => 
			array (
				'id' => 296,
				'nome' => 'Ferreira Gomes',
				'uf' => 'AP',
				'cep2' => '1600238',
				'estado_cod' => 4,
				'cep' => '68915-000',
			),
			190 => 
			array (
				'id' => 303,
				'nome' => 'Itaubal',
				'uf' => 'AP',
				'cep2' => '1600253',
				'estado_cod' => 4,
				'cep' => '68976-000',
			),
			191 => 
			array (
				'id' => 304,
				'nome' => 'Laranjal do Jari',
				'uf' => 'AP',
				'cep2' => '1600279',
				'estado_cod' => 4,
				'cep' => '68920-000',
			),
			192 => 
			array (
				'id' => 307,
				'nome' => 'Macapá',
				'uf' => 'AP',
				'cep2' => '1600303',
				'estado_cod' => 4,
				'cep' => 'LOC',
			),
			193 => 
			array (
				'id' => 308,
				'nome' => 'Mazagão',
				'uf' => 'AP',
				'cep2' => '1600402',
				'estado_cod' => 4,
				'cep' => '68940-000',
			),
			194 => 
			array (
				'id' => 310,
				'nome' => 'Oiapoque',
				'uf' => 'AP',
				'cep2' => '1600501',
				'estado_cod' => 4,
				'cep' => '68980-000',
			),
			195 => 
			array (
				'id' => 312,
				'nome' => 'Porto Grande',
				'uf' => 'AP',
				'cep2' => '1600535',
				'estado_cod' => 4,
				'cep' => '68997-000',
			),
			196 => 
			array (
				'id' => 313,
				'nome' => 'Pracuúba',
				'uf' => 'AP',
				'cep2' => '1600550',
				'estado_cod' => 4,
				'cep' => '68918-000',
			),
			197 => 
			array (
				'id' => 316,
				'nome' => 'Santana',
				'uf' => 'AP',
				'cep2' => '1600600',
				'estado_cod' => 4,
				'cep' => '68925-000',
			),
			198 => 
			array (
				'id' => 322,
				'nome' => 'Tartarugalzinho',
				'uf' => 'AP',
				'cep2' => '1600709',
				'estado_cod' => 4,
				'cep' => '68990-000',
			),
			199 => 
			array (
				'id' => 324,
				'nome' => 'Vitória do Jari',
				'uf' => 'AP',
				'cep2' => '1600808',
				'estado_cod' => 4,
				'cep' => '68924-000',
			),
			200 => 
			array (
				'id' => 326,
				'nome' => 'Abaíra',
				'uf' => 'BA',
				'cep2' => '2900108',
				'estado_cod' => 5,
				'cep' => '46690-000',
			),
			201 => 
			array (
				'id' => 327,
				'nome' => 'Abaré',
				'uf' => 'BA',
				'cep2' => '2900207',
				'estado_cod' => 5,
				'cep' => '48680-000',
			),
			202 => 
			array (
				'id' => 331,
				'nome' => 'Acajutiba',
				'uf' => 'BA',
				'cep2' => '2900306',
				'estado_cod' => 5,
				'cep' => '48360-000',
			),
			203 => 
			array (
				'id' => 335,
				'nome' => 'Adustina',
				'uf' => 'BA',
				'cep2' => '2900355',
				'estado_cod' => 5,
				'cep' => '48435-000',
			),
			204 => 
			array (
				'id' => 339,
				'nome' => 'Água Fria',
				'uf' => 'BA',
				'cep2' => '2900405',
				'estado_cod' => 5,
				'cep' => '48170-000',
			),
			205 => 
			array (
				'id' => 341,
				'nome' => 'Aiquara',
				'uf' => 'BA',
				'cep2' => '2900603',
				'estado_cod' => 5,
				'cep' => '45220-000',
			),
			206 => 
			array (
				'id' => 342,
				'nome' => 'Alagoinhas',
				'uf' => 'BA',
				'cep2' => '2900702',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			207 => 
			array (
				'id' => 343,
				'nome' => 'Alcobaça',
				'uf' => 'BA',
				'cep2' => '2900801',
				'estado_cod' => 5,
				'cep' => '45990-000',
			),
			208 => 
			array (
				'id' => 347,
				'nome' => 'Almadina',
				'uf' => 'BA',
				'cep2' => '2900900',
				'estado_cod' => 5,
				'cep' => '45640-000',
			),
			209 => 
			array (
				'id' => 351,
				'nome' => 'Amargosa',
				'uf' => 'BA',
				'cep2' => '2901007',
				'estado_cod' => 5,
				'cep' => '45300-000',
			),
			210 => 
			array (
				'id' => 352,
				'nome' => 'Amélia Rodrigues',
				'uf' => 'BA',
				'cep2' => '2901106',
				'estado_cod' => 5,
				'cep' => '44230-000',
			),
			211 => 
			array (
				'id' => 353,
				'nome' => 'América Dourada',
				'uf' => 'BA',
				'cep2' => '2901155',
				'estado_cod' => 5,
				'cep' => '44910-000',
			),
			212 => 
			array (
				'id' => 355,
				'nome' => 'Anagé',
				'uf' => 'BA',
				'cep2' => '2901205',
				'estado_cod' => 5,
				'cep' => '45180-000',
			),
			213 => 
			array (
				'id' => 356,
				'nome' => 'Andaraí',
				'uf' => 'BA',
				'cep2' => '2901304',
				'estado_cod' => 5,
				'cep' => '46830-000',
			),
			214 => 
			array (
				'id' => 357,
				'nome' => 'Andorinha',
				'uf' => 'BA',
				'cep2' => '2901353',
				'estado_cod' => 5,
				'cep' => '48990-000',
			),
			215 => 
			array (
				'id' => 358,
				'nome' => 'Angical',
				'uf' => 'BA',
				'cep2' => '2901403',
				'estado_cod' => 5,
				'cep' => '47960-000',
			),
			216 => 
			array (
				'id' => 360,
				'nome' => 'Anguera',
				'uf' => 'BA',
				'cep2' => '2901502',
				'estado_cod' => 5,
				'cep' => '44670-000',
			),
			217 => 
			array (
				'id' => 361,
				'nome' => 'Antas',
				'uf' => 'BA',
				'cep2' => '2901601',
				'estado_cod' => 5,
				'cep' => '48420-000',
			),
			218 => 
			array (
				'id' => 362,
				'nome' => 'Antônio Cardoso',
				'uf' => 'BA',
				'cep2' => '2901700',
				'estado_cod' => 5,
				'cep' => '44180-000',
			),
			219 => 
			array (
				'id' => 363,
				'nome' => 'Antônio Gonçalves',
				'uf' => 'BA',
				'cep2' => '2901809',
				'estado_cod' => 5,
				'cep' => '44780-000',
			),
			220 => 
			array (
				'id' => 364,
				'nome' => 'Aporá',
				'uf' => 'BA',
				'cep2' => '2901908',
				'estado_cod' => 5,
				'cep' => '48350-000',
			),
			221 => 
			array (
				'id' => 365,
				'nome' => 'Apuarema',
				'uf' => 'BA',
				'cep2' => '2901957',
				'estado_cod' => 5,
				'cep' => '45355-000',
			),
			222 => 
			array (
				'id' => 366,
				'nome' => 'Araçás',
				'uf' => 'BA',
				'cep2' => '2902054',
				'estado_cod' => 5,
				'cep' => '48108-000',
			),
			223 => 
			array (
				'id' => 367,
				'nome' => 'Aracatu',
				'uf' => 'BA',
				'cep2' => '2902005',
				'estado_cod' => 5,
				'cep' => '46130-000',
			),
			224 => 
			array (
				'id' => 368,
				'nome' => 'Araci',
				'uf' => 'BA',
				'cep2' => '2902104',
				'estado_cod' => 5,
				'cep' => '48760-000',
			),
			225 => 
			array (
				'id' => 369,
				'nome' => 'Aramari',
				'uf' => 'BA',
				'cep2' => '2902203',
				'estado_cod' => 5,
				'cep' => '48130-000',
			),
			226 => 
			array (
				'id' => 371,
				'nome' => 'Arataca',
				'uf' => 'BA',
				'cep2' => '2902252',
				'estado_cod' => 5,
				'cep' => '45695-000',
			),
			227 => 
			array (
				'id' => 372,
				'nome' => 'Aratuípe',
				'uf' => 'BA',
				'cep2' => '2902302',
				'estado_cod' => 5,
				'cep' => '44490-000',
			),
			228 => 
			array (
				'id' => 379,
				'nome' => 'Aurelino Leal',
				'uf' => 'BA',
				'cep2' => '2902401',
				'estado_cod' => 5,
				'cep' => '45675-000',
			),
			229 => 
			array (
				'id' => 380,
				'nome' => 'Baianópolis',
				'uf' => 'BA',
				'cep2' => '2902500',
				'estado_cod' => 5,
				'cep' => '47830-000',
			),
			230 => 
			array (
				'id' => 382,
				'nome' => 'Baixa Grande',
				'uf' => 'BA',
				'cep2' => '2902609',
				'estado_cod' => 5,
				'cep' => '44620-000',
			),
			231 => 
			array (
				'id' => 391,
				'nome' => 'Banzaê',
				'uf' => 'BA',
				'cep2' => '2902658',
				'estado_cod' => 5,
				'cep' => '48405-000',
			),
			232 => 
			array (
				'id' => 394,
				'nome' => 'Barra',
				'uf' => 'BA',
				'cep2' => '2902708',
				'estado_cod' => 5,
				'cep' => '47100-000',
			),
			233 => 
			array (
				'id' => 395,
				'nome' => 'Barra da Estiva',
				'uf' => 'BA',
				'cep2' => '2902807',
				'estado_cod' => 5,
				'cep' => '46650-000',
			),
			234 => 
			array (
				'id' => 396,
				'nome' => 'Barra do Choça',
				'uf' => 'BA',
				'cep2' => '2902906',
				'estado_cod' => 5,
				'cep' => '45120-000',
			),
			235 => 
			array (
				'id' => 398,
				'nome' => 'Barra do Mendes',
				'uf' => 'BA',
				'cep2' => '2903003',
				'estado_cod' => 5,
				'cep' => '44990-000',
			),
			236 => 
			array (
				'id' => 400,
				'nome' => 'Barra do Rocha',
				'uf' => 'BA',
				'cep2' => '2903102',
				'estado_cod' => 5,
				'cep' => '45560-000',
			),
			237 => 
			array (
				'id' => 403,
				'nome' => 'Barreiras',
				'uf' => 'BA',
				'cep2' => '2903201',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			238 => 
			array (
				'id' => 404,
				'nome' => 'Barro Alto',
				'uf' => 'BA',
				'cep2' => '2903235',
				'estado_cod' => 5,
				'cep' => '44895-000',
			),
			239 => 
			array (
				'id' => 405,
				'nome' => 'Barro Preto',
				'uf' => 'BA',
				'cep2' => '2903300',
				'estado_cod' => 5,
				'cep' => '45625-000',
			),
			240 => 
			array (
				'id' => 407,
				'nome' => 'Barrocas',
				'uf' => 'BA',
				'cep2' => '2903276',
				'estado_cod' => 5,
				'cep' => '48705-000',
			),
			241 => 
			array (
				'id' => 413,
				'nome' => 'Belmonte',
				'uf' => 'BA',
				'cep2' => '2903409',
				'estado_cod' => 5,
				'cep' => '45800-000',
			),
			242 => 
			array (
				'id' => 414,
				'nome' => 'Belo Campo',
				'uf' => 'BA',
				'cep2' => '2903508',
				'estado_cod' => 5,
				'cep' => '45160-000',
			),
			243 => 
			array (
				'id' => 419,
				'nome' => 'Biritinga',
				'uf' => 'BA',
				'cep2' => '2903607',
				'estado_cod' => 5,
				'cep' => '48780-000',
			),
			244 => 
			array (
				'id' => 421,
				'nome' => 'Boa Nova',
				'uf' => 'BA',
				'cep2' => '2903706',
				'estado_cod' => 5,
				'cep' => '45250-000',
			),
			245 => 
			array (
				'id' => 424,
				'nome' => 'Boa Vista do Tupim',
				'uf' => 'BA',
				'cep2' => '2903805',
				'estado_cod' => 5,
				'cep' => '46850-000',
			),
			246 => 
			array (
				'id' => 427,
				'nome' => 'Bom Jesus da Lapa',
				'uf' => 'BA',
				'cep2' => '2903904',
				'estado_cod' => 5,
				'cep' => '47600-000',
			),
			247 => 
			array (
				'id' => 428,
				'nome' => 'Bom Jesus da Serra',
				'uf' => 'BA',
				'cep2' => '2903953',
				'estado_cod' => 5,
				'cep' => '45263-000',
			),
			248 => 
			array (
				'id' => 431,
				'nome' => 'Boninal',
				'uf' => 'BA',
				'cep2' => '2904001',
				'estado_cod' => 5,
				'cep' => '46740-000',
			),
			249 => 
			array (
				'id' => 432,
				'nome' => 'Bonito',
				'uf' => 'BA',
				'cep2' => '2904050',
				'estado_cod' => 5,
				'cep' => '46820-000',
			),
			250 => 
			array (
				'id' => 433,
				'nome' => 'Boquira',
				'uf' => 'BA',
				'cep2' => '2904100',
				'estado_cod' => 5,
				'cep' => '46530-000',
			),
			251 => 
			array (
				'id' => 434,
				'nome' => 'Botuporã',
				'uf' => 'BA',
				'cep2' => '2904209',
				'estado_cod' => 5,
				'cep' => '46570-000',
			),
			252 => 
			array (
				'id' => 440,
				'nome' => 'Brejões',
				'uf' => 'BA',
				'cep2' => '2904308',
				'estado_cod' => 5,
				'cep' => '45325-000',
			),
			253 => 
			array (
				'id' => 441,
				'nome' => 'Brejolândia',
				'uf' => 'BA',
				'cep2' => '2904407',
				'estado_cod' => 5,
				'cep' => '47750-000',
			),
			254 => 
			array (
				'id' => 442,
				'nome' => 'Brotas de Macaúbas',
				'uf' => 'BA',
				'cep2' => '2904506',
				'estado_cod' => 5,
				'cep' => '47560-000',
			),
			255 => 
			array (
				'id' => 443,
				'nome' => 'Brumado',
				'uf' => 'BA',
				'cep2' => '2904605',
				'estado_cod' => 5,
				'cep' => '46100-000',
			),
			256 => 
			array (
				'id' => 445,
				'nome' => 'Buerarema',
				'uf' => 'BA',
				'cep2' => '2904704',
				'estado_cod' => 5,
				'cep' => '45615-000',
			),
			257 => 
			array (
				'id' => 450,
				'nome' => 'Buritirama',
				'uf' => 'BA',
				'cep2' => '2904753',
				'estado_cod' => 5,
				'cep' => '47120-000',
			),
			258 => 
			array (
				'id' => 451,
				'nome' => 'Caatiba',
				'uf' => 'BA',
				'cep2' => '2904803',
				'estado_cod' => 5,
				'cep' => '45130-000',
			),
			259 => 
			array (
				'id' => 452,
				'nome' => 'Cabaceiras do Paraguaçu',
				'uf' => 'BA',
				'cep2' => '2904852',
				'estado_cod' => 5,
				'cep' => '44345-000',
			),
			260 => 
			array (
				'id' => 455,
				'nome' => 'Cachoeira',
				'uf' => 'BA',
				'cep2' => '2904902',
				'estado_cod' => 5,
				'cep' => '44300-000',
			),
			261 => 
			array (
				'id' => 457,
				'nome' => 'Caculé',
				'uf' => 'BA',
				'cep2' => '2905008',
				'estado_cod' => 5,
				'cep' => '46300-000',
			),
			262 => 
			array (
				'id' => 458,
				'nome' => 'Caém',
				'uf' => 'BA',
				'cep2' => '2905107',
				'estado_cod' => 5,
				'cep' => '44730-000',
			),
			263 => 
			array (
				'id' => 459,
				'nome' => 'Caetanos',
				'uf' => 'BA',
				'cep2' => '2905156',
				'estado_cod' => 5,
				'cep' => '45265-000',
			),
			264 => 
			array (
				'id' => 461,
				'nome' => 'Caetité',
				'uf' => 'BA',
				'cep2' => '2905206',
				'estado_cod' => 5,
				'cep' => '46400-000',
			),
			265 => 
			array (
				'id' => 462,
				'nome' => 'Cafarnaum',
				'uf' => 'BA',
				'cep2' => '2905305',
				'estado_cod' => 5,
				'cep' => '44880-000',
			),
			266 => 
			array (
				'id' => 465,
				'nome' => 'Cairu',
				'uf' => 'BA',
				'cep2' => '2905404',
				'estado_cod' => 5,
				'cep' => '45420-000',
			),
			267 => 
			array (
				'id' => 470,
				'nome' => 'Caldeirão Grande',
				'uf' => 'BA',
				'cep2' => '2905503',
				'estado_cod' => 5,
				'cep' => '44750-000',
			),
			268 => 
			array (
				'id' => 472,
				'nome' => 'Camacan',
				'uf' => 'BA',
				'cep2' => '2905602',
				'estado_cod' => 5,
				'cep' => '45880-000',
			),
			269 => 
			array (
				'id' => 473,
				'nome' => 'Camaçari',
				'uf' => 'BA',
				'cep2' => '2905701',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			270 => 
			array (
				'id' => 474,
				'nome' => 'Camamu',
				'uf' => 'BA',
				'cep2' => '2905800',
				'estado_cod' => 5,
				'cep' => '45445-000',
			),
			271 => 
			array (
				'id' => 478,
				'nome' => 'Campo Alegre de Lourdes',
				'uf' => 'BA',
				'cep2' => '2905909',
				'estado_cod' => 5,
				'cep' => '47220-000',
			),
			272 => 
			array (
				'id' => 479,
				'nome' => 'Campo Formoso',
				'uf' => 'BA',
				'cep2' => '2906006',
				'estado_cod' => 5,
				'cep' => '44790-000',
			),
			273 => 
			array (
				'id' => 483,
				'nome' => 'Canápolis',
				'uf' => 'BA',
				'cep2' => '2906105',
				'estado_cod' => 5,
				'cep' => '47730-000',
			),
			274 => 
			array (
				'id' => 484,
				'nome' => 'Canarana',
				'uf' => 'BA',
				'cep2' => '2906204',
				'estado_cod' => 5,
				'cep' => '44890-000',
			),
			275 => 
			array (
				'id' => 486,
				'nome' => 'Canavieiras',
				'uf' => 'BA',
				'cep2' => '2906303',
				'estado_cod' => 5,
				'cep' => '45860-000',
			),
			276 => 
			array (
				'id' => 488,
				'nome' => 'Candeal',
				'uf' => 'BA',
				'cep2' => '2906402',
				'estado_cod' => 5,
				'cep' => '48710-000',
			),
			277 => 
			array (
				'id' => 489,
				'nome' => 'Candeias',
				'uf' => 'BA',
				'cep2' => '2906501',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			278 => 
			array (
				'id' => 490,
				'nome' => 'Candiba',
				'uf' => 'BA',
				'cep2' => '2906600',
				'estado_cod' => 5,
				'cep' => '46380-000',
			),
			279 => 
			array (
				'id' => 491,
				'nome' => 'Cândido Sales',
				'uf' => 'BA',
				'cep2' => '2906709',
				'estado_cod' => 5,
				'cep' => '45157-000',
			),
			280 => 
			array (
				'id' => 493,
				'nome' => 'Cansanção',
				'uf' => 'BA',
				'cep2' => '2906808',
				'estado_cod' => 5,
				'cep' => '48840-000',
			),
			281 => 
			array (
				'id' => 495,
				'nome' => 'Canudos',
				'uf' => 'BA',
				'cep2' => '2906824',
				'estado_cod' => 5,
				'cep' => '48520-000',
			),
			282 => 
			array (
				'id' => 498,
				'nome' => 'Capela do Alto Alegre',
				'uf' => 'BA',
				'cep2' => '2906857',
				'estado_cod' => 5,
				'cep' => '44645-000',
			),
			283 => 
			array (
				'id' => 499,
				'nome' => 'Capim Grosso',
				'uf' => 'BA',
				'cep2' => '2906873',
				'estado_cod' => 5,
				'cep' => '44695-000',
			),
			284 => 
			array (
				'id' => 501,
				'nome' => 'Caraíbas',
				'uf' => 'BA',
				'cep2' => '2906899',
				'estado_cod' => 5,
				'cep' => '45177-000',
			),
			285 => 
			array (
				'id' => 505,
				'nome' => 'Caravelas',
				'uf' => 'BA',
				'cep2' => '2906907',
				'estado_cod' => 5,
				'cep' => '45900-000',
			),
			286 => 
			array (
				'id' => 506,
				'nome' => 'Cardeal da Silva',
				'uf' => 'BA',
				'cep2' => '2907004',
				'estado_cod' => 5,
				'cep' => '48390-000',
			),
			287 => 
			array (
				'id' => 507,
				'nome' => 'Carinhanha',
				'uf' => 'BA',
				'cep2' => '2907103',
				'estado_cod' => 5,
				'cep' => '46445-000',
			),
			288 => 
			array (
				'id' => 511,
				'nome' => 'Casa Nova',
				'uf' => 'BA',
				'cep2' => '2907202',
				'estado_cod' => 5,
				'cep' => '47300-000',
			),
			289 => 
			array (
				'id' => 513,
				'nome' => 'Castro Alves',
				'uf' => 'BA',
				'cep2' => '2907301',
				'estado_cod' => 5,
				'cep' => '44500-000',
			),
			290 => 
			array (
				'id' => 516,
				'nome' => 'Catolândia',
				'uf' => 'BA',
				'cep2' => '2907400',
				'estado_cod' => 5,
				'cep' => '47815-000',
			),
			291 => 
			array (
				'id' => 519,
				'nome' => 'Catu',
				'uf' => 'BA',
				'cep2' => '2907509',
				'estado_cod' => 5,
				'cep' => '48110-000',
			),
			292 => 
			array (
				'id' => 521,
				'nome' => 'Caturama',
				'uf' => 'BA',
				'cep2' => '2907558',
				'estado_cod' => 5,
				'cep' => '46575-000',
			),
			293 => 
			array (
				'id' => 523,
				'nome' => 'Central',
				'uf' => 'BA',
				'cep2' => '2907608',
				'estado_cod' => 5,
				'cep' => '44940-000',
			),
			294 => 
			array (
				'id' => 525,
				'nome' => 'Chorrochó',
				'uf' => 'BA',
				'cep2' => '2907707',
				'estado_cod' => 5,
				'cep' => '48660-000',
			),
			295 => 
			array (
				'id' => 526,
				'nome' => 'Cícero Dantas',
				'uf' => 'BA',
				'cep2' => '2907806',
				'estado_cod' => 5,
				'cep' => '48410-000',
			),
			296 => 
			array (
				'id' => 528,
				'nome' => 'Cipó',
				'uf' => 'BA',
				'cep2' => '2907905',
				'estado_cod' => 5,
				'cep' => '48450-000',
			),
			297 => 
			array (
				'id' => 529,
				'nome' => 'Coaraci',
				'uf' => 'BA',
				'cep2' => '2908002',
				'estado_cod' => 5,
				'cep' => '45638-000',
			),
			298 => 
			array (
				'id' => 530,
				'nome' => 'Cocos',
				'uf' => 'BA',
				'cep2' => '2908101',
				'estado_cod' => 5,
				'cep' => '47680-000',
			),
			299 => 
			array (
				'id' => 533,
				'nome' => 'Conceição da Feira',
				'uf' => 'BA',
				'cep2' => '2908200',
				'estado_cod' => 5,
				'cep' => '44320-000',
			),
			300 => 
			array (
				'id' => 534,
				'nome' => 'Conceição do Almeida',
				'uf' => 'BA',
				'cep2' => '2908309',
				'estado_cod' => 5,
				'cep' => '44540-000',
			),
			301 => 
			array (
				'id' => 535,
				'nome' => 'Conceição do Coité',
				'uf' => 'BA',
				'cep2' => '2908408',
				'estado_cod' => 5,
				'cep' => '48730-000',
			),
			302 => 
			array (
				'id' => 536,
				'nome' => 'Conceição do Jacuípe',
				'uf' => 'BA',
				'cep2' => '2908507',
				'estado_cod' => 5,
				'cep' => '44245-000',
			),
			303 => 
			array (
				'id' => 537,
				'nome' => 'Conde',
				'uf' => 'BA',
				'cep2' => '2908606',
				'estado_cod' => 5,
				'cep' => '48300-000',
			),
			304 => 
			array (
				'id' => 538,
				'nome' => 'Condeúba',
				'uf' => 'BA',
				'cep2' => '2908705',
				'estado_cod' => 5,
				'cep' => '46200-000',
			),
			305 => 
			array (
				'id' => 539,
				'nome' => 'Contendas do Sincorá',
				'uf' => 'BA',
				'cep2' => '2908804',
				'estado_cod' => 5,
				'cep' => '46620-000',
			),
			306 => 
			array (
				'id' => 543,
				'nome' => 'Coração de Maria',
				'uf' => 'BA',
				'cep2' => '2908903',
				'estado_cod' => 5,
				'cep' => '44250-000',
			),
			307 => 
			array (
				'id' => 544,
				'nome' => 'Cordeiros',
				'uf' => 'BA',
				'cep2' => '2909000',
				'estado_cod' => 5,
				'cep' => '46280-000',
			),
			308 => 
			array (
				'id' => 545,
				'nome' => 'Coribe',
				'uf' => 'BA',
				'cep2' => '2909109',
				'estado_cod' => 5,
				'cep' => '47690-000',
			),
			309 => 
			array (
				'id' => 546,
				'nome' => 'Coronel João Sá',
				'uf' => 'BA',
				'cep2' => '2909208',
				'estado_cod' => 5,
				'cep' => '48590-000',
			),
			310 => 
			array (
				'id' => 547,
				'nome' => 'Correntina',
				'uf' => 'BA',
				'cep2' => '2909307',
				'estado_cod' => 5,
				'cep' => '47650-000',
			),
			311 => 
			array (
				'id' => 549,
				'nome' => 'Cotegipe',
				'uf' => 'BA',
				'cep2' => '2909406',
				'estado_cod' => 5,
				'cep' => '47900-000',
			),
			312 => 
			array (
				'id' => 551,
				'nome' => 'Cravolândia',
				'uf' => 'BA',
				'cep2' => '2909505',
				'estado_cod' => 5,
				'cep' => '45330-000',
			),
			313 => 
			array (
				'id' => 552,
				'nome' => 'Crisópolis',
				'uf' => 'BA',
				'cep2' => '2909604',
				'estado_cod' => 5,
				'cep' => '48480-000',
			),
			314 => 
			array (
				'id' => 554,
				'nome' => 'Cristópolis',
				'uf' => 'BA',
				'cep2' => '2909703',
				'estado_cod' => 5,
				'cep' => '47950-000',
			),
			315 => 
			array (
				'id' => 556,
				'nome' => 'Cruz das Almas',
				'uf' => 'BA',
				'cep2' => '2909802',
				'estado_cod' => 5,
				'cep' => '44380-000',
			),
			316 => 
			array (
				'id' => 559,
				'nome' => 'Curaçá',
				'uf' => 'BA',
				'cep2' => '2909901',
				'estado_cod' => 5,
				'cep' => '48930-000',
			),
			317 => 
			array (
				'id' => 561,
				'nome' => 'Dário Meira',
				'uf' => 'BA',
				'cep2' => '2910008',
				'estado_cod' => 5,
				'cep' => '45590-000',
			),
			318 => 
			array (
				'id' => 565,
				'nome' => 'Dias D\'Ávila',
				'uf' => 'BA',
				'cep2' => '2910057',
				'estado_cod' => 5,
				'cep' => '42850-000',
			),
			319 => 
			array (
				'id' => 567,
				'nome' => 'Dom Basílio',
				'uf' => 'BA',
				'cep2' => '2910107',
				'estado_cod' => 5,
				'cep' => '46165-000',
			),
			320 => 
			array (
				'id' => 568,
				'nome' => 'Dom Macedo Costa',
				'uf' => 'BA',
				'cep2' => '2910206',
				'estado_cod' => 5,
				'cep' => '44560-000',
			),
			321 => 
			array (
				'id' => 571,
				'nome' => 'Elísio Medrado',
				'uf' => 'BA',
				'cep2' => '2910305',
				'estado_cod' => 5,
				'cep' => '45305-000',
			),
			322 => 
			array (
				'id' => 572,
				'nome' => 'Encruzilhada',
				'uf' => 'BA',
				'cep2' => '2910404',
				'estado_cod' => 5,
				'cep' => '45150-000',
			),
			323 => 
			array (
				'id' => 575,
				'nome' => 'Entre Rios',
				'uf' => 'BA',
				'cep2' => '2910503',
				'estado_cod' => 5,
				'cep' => '48180-000',
			),
			324 => 
			array (
				'id' => 576,
				'nome' => 'Érico Cardoso',
				'uf' => 'BA',
				'cep2' => '2900504',
				'estado_cod' => 5,
				'cep' => '46180-000',
			),
			325 => 
			array (
				'id' => 577,
				'nome' => 'Esplanada',
				'uf' => 'BA',
				'cep2' => '2910602',
				'estado_cod' => 5,
				'cep' => '48370-000',
			),
			326 => 
			array (
				'id' => 578,
				'nome' => 'Euclides da Cunha',
				'uf' => 'BA',
				'cep2' => '2910701',
				'estado_cod' => 5,
				'cep' => '48500-000',
			),
			327 => 
			array (
				'id' => 579,
				'nome' => 'Eunápolis',
				'uf' => 'BA',
				'cep2' => '2910727',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			328 => 
			array (
				'id' => 580,
				'nome' => 'Fátima',
				'uf' => 'BA',
				'cep2' => '2910750',
				'estado_cod' => 5,
				'cep' => '48415-000',
			),
			329 => 
			array (
				'id' => 581,
				'nome' => 'Feira da Mata',
				'uf' => 'BA',
				'cep2' => '2910776',
				'estado_cod' => 5,
				'cep' => '46446-000',
			),
			330 => 
			array (
				'id' => 582,
				'nome' => 'Feira de Santana',
				'uf' => 'BA',
				'cep2' => '2910800',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			331 => 
			array (
				'id' => 584,
				'nome' => 'Filadélfia',
				'uf' => 'BA',
				'cep2' => '2910859',
				'estado_cod' => 5,
				'cep' => '44775-000',
			),
			332 => 
			array (
				'id' => 586,
				'nome' => 'Firmino Alves',
				'uf' => 'BA',
				'cep2' => '2910909',
				'estado_cod' => 5,
				'cep' => '45720-000',
			),
			333 => 
			array (
				'id' => 587,
				'nome' => 'Floresta Azul',
				'uf' => 'BA',
				'cep2' => '2911006',
				'estado_cod' => 5,
				'cep' => '45740-000',
			),
			334 => 
			array (
				'id' => 588,
				'nome' => 'Formosa do Rio Preto',
				'uf' => 'BA',
				'cep2' => '2911105',
				'estado_cod' => 5,
				'cep' => '47990-000',
			),
			335 => 
			array (
				'id' => 595,
				'nome' => 'Gandu',
				'uf' => 'BA',
				'cep2' => '2911204',
				'estado_cod' => 5,
				'cep' => '45450-000',
			),
			336 => 
			array (
				'id' => 596,
				'nome' => 'Gavião',
				'uf' => 'BA',
				'cep2' => '2911253',
				'estado_cod' => 5,
				'cep' => '44650-000',
			),
			337 => 
			array (
				'id' => 597,
				'nome' => 'Gentio do Ouro',
				'uf' => 'BA',
				'cep2' => '2911303',
				'estado_cod' => 5,
				'cep' => '47450-000',
			),
			338 => 
			array (
				'id' => 599,
				'nome' => 'Glória',
				'uf' => 'BA',
				'cep2' => '2911402',
				'estado_cod' => 5,
				'cep' => '48620-000',
			),
			339 => 
			array (
				'id' => 600,
				'nome' => 'Gongogi',
				'uf' => 'BA',
				'cep2' => '2911501',
				'estado_cod' => 5,
				'cep' => '45540-000',
			),
			340 => 
			array (
				'id' => 602,
				'nome' => 'Governador Mangabeira',
				'uf' => 'BA',
				'cep2' => '2911600',
				'estado_cod' => 5,
				'cep' => '44350-000',
			),
			341 => 
			array (
				'id' => 605,
				'nome' => 'Guajeru',
				'uf' => 'BA',
				'cep2' => '2911659',
				'estado_cod' => 5,
				'cep' => '46205-000',
			),
			342 => 
			array (
				'id' => 606,
				'nome' => 'Guanambi',
				'uf' => 'BA',
				'cep2' => '2911709',
				'estado_cod' => 5,
				'cep' => '46430-000',
			),
			343 => 
			array (
				'id' => 609,
				'nome' => 'Guaratinga',
				'uf' => 'BA',
				'cep2' => '2911808',
				'estado_cod' => 5,
				'cep' => '45840-000',
			),
			344 => 
			array (
				'id' => 614,
				'nome' => 'Heliópolis',
				'uf' => 'BA',
				'cep2' => '2911857',
				'estado_cod' => 5,
				'cep' => '48445-000',
			),
			345 => 
			array (
				'id' => 618,
				'nome' => 'Iaçu',
				'uf' => 'BA',
				'cep2' => '2911907',
				'estado_cod' => 5,
				'cep' => '46860-000',
			),
			346 => 
			array (
				'id' => 623,
				'nome' => 'Ibiassucê',
				'uf' => 'BA',
				'cep2' => '2912004',
				'estado_cod' => 5,
				'cep' => '46390-000',
			),
			347 => 
			array (
				'id' => 624,
				'nome' => 'Ibicaraí',
				'uf' => 'BA',
				'cep2' => '2912103',
				'estado_cod' => 5,
				'cep' => '45745-000',
			),
			348 => 
			array (
				'id' => 625,
				'nome' => 'Ibicoara',
				'uf' => 'BA',
				'cep2' => '2912202',
				'estado_cod' => 5,
				'cep' => '46760-000',
			),
			349 => 
			array (
				'id' => 626,
				'nome' => 'Ibicuí',
				'uf' => 'BA',
				'cep2' => '2912301',
				'estado_cod' => 5,
				'cep' => '45290-000',
			),
			350 => 
			array (
				'id' => 627,
				'nome' => 'Ibipeba',
				'uf' => 'BA',
				'cep2' => '2912400',
				'estado_cod' => 5,
				'cep' => '44970-000',
			),
			351 => 
			array (
				'id' => 629,
				'nome' => 'Ibipitanga',
				'uf' => 'BA',
				'cep2' => '2912509',
				'estado_cod' => 5,
				'cep' => '46540-000',
			),
			352 => 
			array (
				'id' => 630,
				'nome' => 'Ibiquera',
				'uf' => 'BA',
				'cep2' => '2912608',
				'estado_cod' => 5,
				'cep' => '46840-000',
			),
			353 => 
			array (
				'id' => 634,
				'nome' => 'Ibirapitanga',
				'uf' => 'BA',
				'cep2' => '2912707',
				'estado_cod' => 5,
				'cep' => '45500-000',
			),
			354 => 
			array (
				'id' => 635,
				'nome' => 'Ibirapuã',
				'uf' => 'BA',
				'cep2' => '2912806',
				'estado_cod' => 5,
				'cep' => '45940-000',
			),
			355 => 
			array (
				'id' => 636,
				'nome' => 'Ibirataia',
				'uf' => 'BA',
				'cep2' => '2912905',
				'estado_cod' => 5,
				'cep' => '45580-000',
			),
			356 => 
			array (
				'id' => 637,
				'nome' => 'Ibitiara',
				'uf' => 'BA',
				'cep2' => '2913002',
				'estado_cod' => 5,
				'cep' => '46700-000',
			),
			357 => 
			array (
				'id' => 640,
				'nome' => 'Ibititá',
				'uf' => 'BA',
				'cep2' => '2913101',
				'estado_cod' => 5,
				'cep' => '44960-000',
			),
			358 => 
			array (
				'id' => 644,
				'nome' => 'Ibotirama',
				'uf' => 'BA',
				'cep2' => '2913200',
				'estado_cod' => 5,
				'cep' => '47520-000',
			),
			359 => 
			array (
				'id' => 645,
				'nome' => 'Ichu',
				'uf' => 'BA',
				'cep2' => '2913309',
				'estado_cod' => 5,
				'cep' => '48725-000',
			),
			360 => 
			array (
				'id' => 647,
				'nome' => 'Igaporã',
				'uf' => 'BA',
				'cep2' => '2913408',
				'estado_cod' => 5,
				'cep' => '46490-000',
			),
			361 => 
			array (
				'id' => 651,
				'nome' => 'Igrapiúna',
				'uf' => 'BA',
				'cep2' => '2913457',
				'estado_cod' => 5,
				'cep' => '45443-000',
			),
			362 => 
			array (
				'id' => 653,
				'nome' => 'Iguaí',
				'uf' => 'BA',
				'cep2' => '2913507',
				'estado_cod' => 5,
				'cep' => '45280-000',
			),
			363 => 
			array (
				'id' => 659,
				'nome' => 'Ilhéus',
				'uf' => 'BA',
				'cep2' => '2913606',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			364 => 
			array (
				'id' => 662,
				'nome' => 'Inhambupe',
				'uf' => 'BA',
				'cep2' => '2913705',
				'estado_cod' => 5,
				'cep' => '48490-000',
			),
			365 => 
			array (
				'id' => 667,
				'nome' => 'Ipecaetá',
				'uf' => 'BA',
				'cep2' => '2913804',
				'estado_cod' => 5,
				'cep' => '44680-000',
			),
			366 => 
			array (
				'id' => 668,
				'nome' => 'Ipiaú',
				'uf' => 'BA',
				'cep2' => '2913903',
				'estado_cod' => 5,
				'cep' => '45570-000',
			),
			367 => 
			array (
				'id' => 669,
				'nome' => 'Ipirá',
				'uf' => 'BA',
				'cep2' => '2914000',
				'estado_cod' => 5,
				'cep' => '44600-000',
			),
			368 => 
			array (
				'id' => 672,
				'nome' => 'Ipupiara',
				'uf' => 'BA',
				'cep2' => '2914109',
				'estado_cod' => 5,
				'cep' => '47590-000',
			),
			369 => 
			array (
				'id' => 673,
				'nome' => 'Irajuba',
				'uf' => 'BA',
				'cep2' => '2914208',
				'estado_cod' => 5,
				'cep' => '45370-000',
			),
			370 => 
			array (
				'id' => 674,
				'nome' => 'Iramaia',
				'uf' => 'BA',
				'cep2' => '2914307',
				'estado_cod' => 5,
				'cep' => '46770-000',
			),
			371 => 
			array (
				'id' => 676,
				'nome' => 'Iraquara',
				'uf' => 'BA',
				'cep2' => '2914406',
				'estado_cod' => 5,
				'cep' => '46980-000',
			),
			372 => 
			array (
				'id' => 677,
				'nome' => 'Irará',
				'uf' => 'BA',
				'cep2' => '2914505',
				'estado_cod' => 5,
				'cep' => '44255-000',
			),
			373 => 
			array (
				'id' => 678,
				'nome' => 'Irecê',
				'uf' => 'BA',
				'cep2' => '2914604',
				'estado_cod' => 5,
				'cep' => '44900-000',
			),
			374 => 
			array (
				'id' => 681,
				'nome' => 'Itabela',
				'uf' => 'BA',
				'cep2' => '2914653',
				'estado_cod' => 5,
				'cep' => '45848-000',
			),
			375 => 
			array (
				'id' => 682,
				'nome' => 'Itaberaba',
				'uf' => 'BA',
				'cep2' => '2914703',
				'estado_cod' => 5,
				'cep' => '46880-000',
			),
			376 => 
			array (
				'id' => 683,
				'nome' => 'Itabuna',
				'uf' => 'BA',
				'cep2' => '2914802',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			377 => 
			array (
				'id' => 684,
				'nome' => 'Itacaré',
				'uf' => 'BA',
				'cep2' => '2914901',
				'estado_cod' => 5,
				'cep' => '45530-000',
			),
			378 => 
			array (
				'id' => 688,
				'nome' => 'Itaeté',
				'uf' => 'BA',
				'cep2' => '2915007',
				'estado_cod' => 5,
				'cep' => '46790-000',
			),
			379 => 
			array (
				'id' => 689,
				'nome' => 'Itagi',
				'uf' => 'BA',
				'cep2' => '2915106',
				'estado_cod' => 5,
				'cep' => '45230-000',
			),
			380 => 
			array (
				'id' => 690,
				'nome' => 'Itagibá',
				'uf' => 'BA',
				'cep2' => '2915205',
				'estado_cod' => 5,
				'cep' => '45585-000',
			),
			381 => 
			array (
				'id' => 691,
				'nome' => 'Itagimirim',
				'uf' => 'BA',
				'cep2' => '2915304',
				'estado_cod' => 5,
				'cep' => '45850-000',
			),
			382 => 
			array (
				'id' => 692,
				'nome' => 'Itaguaçu da Bahia',
				'uf' => 'BA',
				'cep2' => '2915353',
				'estado_cod' => 5,
				'cep' => '47440-000',
			),
			383 => 
			array (
				'id' => 698,
				'nome' => 'Itaju do Colônia',
				'uf' => 'BA',
				'cep2' => '2915403',
				'estado_cod' => 5,
				'cep' => '45730-000',
			),
			384 => 
			array (
				'id' => 700,
				'nome' => 'Itajuípe',
				'uf' => 'BA',
				'cep2' => '2915502',
				'estado_cod' => 5,
				'cep' => '45630-000',
			),
			385 => 
			array (
				'id' => 702,
				'nome' => 'Itamaraju',
				'uf' => 'BA',
				'cep2' => '2915601',
				'estado_cod' => 5,
				'cep' => '45836-000',
			),
			386 => 
			array (
				'id' => 703,
				'nome' => 'Itamari',
				'uf' => 'BA',
				'cep2' => '2915700',
				'estado_cod' => 5,
				'cep' => '45455-000',
			),
			387 => 
			array (
				'id' => 704,
				'nome' => 'Itambé',
				'uf' => 'BA',
				'cep2' => '2915809',
				'estado_cod' => 5,
				'cep' => '45140-000',
			),
			388 => 
			array (
				'id' => 708,
				'nome' => 'Itanagra',
				'uf' => 'BA',
				'cep2' => '2915908',
				'estado_cod' => 5,
				'cep' => '48290-000',
			),
			389 => 
			array (
				'id' => 709,
				'nome' => 'Itanhém',
				'uf' => 'BA',
				'cep2' => '2916005',
				'estado_cod' => 5,
				'cep' => '45970-000',
			),
			390 => 
			array (
				'id' => 711,
				'nome' => 'Itaparica',
				'uf' => 'BA',
				'cep2' => '2916104',
				'estado_cod' => 5,
				'cep' => '44460-000',
			),
			391 => 
			array (
				'id' => 712,
				'nome' => 'Itapé',
				'uf' => 'BA',
				'cep2' => '2916203',
				'estado_cod' => 5,
				'cep' => '45750-000',
			),
			392 => 
			array (
				'id' => 713,
				'nome' => 'Itapebi',
				'uf' => 'BA',
				'cep2' => '2916302',
				'estado_cod' => 5,
				'cep' => '45855-000',
			),
			393 => 
			array (
				'id' => 715,
				'nome' => 'Itapetinga',
				'uf' => 'BA',
				'cep2' => '2916401',
				'estado_cod' => 5,
				'cep' => '45700-000',
			),
			394 => 
			array (
				'id' => 716,
				'nome' => 'Itapicuru',
				'uf' => 'BA',
				'cep2' => '2916500',
				'estado_cod' => 5,
				'cep' => '48475-000',
			),
			395 => 
			array (
				'id' => 718,
				'nome' => 'Itapitanga',
				'uf' => 'BA',
				'cep2' => '2916609',
				'estado_cod' => 5,
				'cep' => '45645-000',
			),
			396 => 
			array (
				'id' => 721,
				'nome' => 'Itaquara',
				'uf' => 'BA',
				'cep2' => '2916708',
				'estado_cod' => 5,
				'cep' => '45340-000',
			),
			397 => 
			array (
				'id' => 723,
				'nome' => 'Itarantim',
				'uf' => 'BA',
				'cep2' => '2916807',
				'estado_cod' => 5,
				'cep' => '45780-000',
			),
			398 => 
			array (
				'id' => 725,
				'nome' => 'Itatim',
				'uf' => 'BA',
				'cep2' => '2916856',
				'estado_cod' => 5,
				'cep' => '46875-000',
			),
			399 => 
			array (
				'id' => 727,
				'nome' => 'Itiruçu',
				'uf' => 'BA',
				'cep2' => '2916906',
				'estado_cod' => 5,
				'cep' => '45350-000',
			),
			400 => 
			array (
				'id' => 728,
				'nome' => 'Itiúba',
				'uf' => 'BA',
				'cep2' => '2917003',
				'estado_cod' => 5,
				'cep' => '48850-000',
			),
			401 => 
			array (
				'id' => 729,
				'nome' => 'Itororó',
				'uf' => 'BA',
				'cep2' => '2917102',
				'estado_cod' => 5,
				'cep' => '45710-000',
			),
			402 => 
			array (
				'id' => 730,
				'nome' => 'Ituaçu',
				'uf' => 'BA',
				'cep2' => '2917201',
				'estado_cod' => 5,
				'cep' => '46640-000',
			),
			403 => 
			array (
				'id' => 731,
				'nome' => 'Ituberá',
				'uf' => 'BA',
				'cep2' => '2917300',
				'estado_cod' => 5,
				'cep' => '45435-000',
			),
			404 => 
			array (
				'id' => 733,
				'nome' => 'Iuiu',
				'uf' => 'BA',
				'cep2' => '',
				'estado_cod' => 5,
				'cep' => '46438-000',
			),
			405 => 
			array (
				'id' => 734,
				'nome' => 'Jaborandi',
				'uf' => 'BA',
				'cep2' => '2917359',
				'estado_cod' => 5,
				'cep' => '47655-000',
			),
			406 => 
			array (
				'id' => 735,
				'nome' => 'Jacaraci',
				'uf' => 'BA',
				'cep2' => '2917409',
				'estado_cod' => 5,
				'cep' => '46310-000',
			),
			407 => 
			array (
				'id' => 736,
				'nome' => 'Jacobina',
				'uf' => 'BA',
				'cep2' => '2917508',
				'estado_cod' => 5,
				'cep' => '44700-000',
			),
			408 => 
			array (
				'id' => 740,
				'nome' => 'Jaguaquara',
				'uf' => 'BA',
				'cep2' => '2917607',
				'estado_cod' => 5,
				'cep' => '45345-000',
			),
			409 => 
			array (
				'id' => 742,
				'nome' => 'Jaguarari',
				'uf' => 'BA',
				'cep2' => '2917706',
				'estado_cod' => 5,
				'cep' => '48960-000',
			),
			410 => 
			array (
				'id' => 743,
				'nome' => 'Jaguaripe',
				'uf' => 'BA',
				'cep2' => '2917805',
				'estado_cod' => 5,
				'cep' => '44480-000',
			),
			411 => 
			array (
				'id' => 745,
				'nome' => 'Jandaíra',
				'uf' => 'BA',
				'cep2' => '2917904',
				'estado_cod' => 5,
				'cep' => '48310-000',
			),
			412 => 
			array (
				'id' => 749,
				'nome' => 'Jequié',
				'uf' => 'BA',
				'cep2' => '2918001',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			413 => 
			array (
				'id' => 750,
				'nome' => 'Jequiriçá',
				'uf' => 'BA',
				'cep2' => '',
				'estado_cod' => 5,
				'cep' => '45470-000',
			),
			414 => 
			array (
				'id' => 751,
				'nome' => 'Jeremoabo',
				'uf' => 'BA',
				'cep2' => '2918100',
				'estado_cod' => 5,
				'cep' => '48540-000',
			),
			415 => 
			array (
				'id' => 753,
				'nome' => 'Jitaúna',
				'uf' => 'BA',
				'cep2' => '2918308',
				'estado_cod' => 5,
				'cep' => '45225-000',
			),
			416 => 
			array (
				'id' => 756,
				'nome' => 'João Dourado',
				'uf' => 'BA',
				'cep2' => '2918357',
				'estado_cod' => 5,
				'cep' => '44920-000',
			),
			417 => 
			array (
				'id' => 759,
				'nome' => 'Juazeiro',
				'uf' => 'BA',
				'cep2' => '2918407',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			418 => 
			array (
				'id' => 760,
				'nome' => 'Jucuruçu',
				'uf' => 'BA',
				'cep2' => '2918456',
				'estado_cod' => 5,
				'cep' => '45834-000',
			),
			419 => 
			array (
				'id' => 766,
				'nome' => 'Jussara',
				'uf' => 'BA',
				'cep2' => '2918506',
				'estado_cod' => 5,
				'cep' => '44925-000',
			),
			420 => 
			array (
				'id' => 767,
				'nome' => 'Jussari',
				'uf' => 'BA',
				'cep2' => '2918555',
				'estado_cod' => 5,
				'cep' => '45610-000',
			),
			421 => 
			array (
				'id' => 768,
				'nome' => 'Jussiape',
				'uf' => 'BA',
				'cep2' => '2918605',
				'estado_cod' => 5,
				'cep' => '46670-000',
			),
			422 => 
			array (
				'id' => 770,
				'nome' => 'Lafaiete Coutinho',
				'uf' => 'BA',
				'cep2' => '2918704',
				'estado_cod' => 5,
				'cep' => '45215-000',
			),
			423 => 
			array (
				'id' => 778,
				'nome' => 'Lagoa Real',
				'uf' => 'BA',
				'cep2' => '2918753',
				'estado_cod' => 5,
				'cep' => '46425-000',
			),
			424 => 
			array (
				'id' => 779,
				'nome' => 'Laje',
				'uf' => 'BA',
				'cep2' => '2918803',
				'estado_cod' => 5,
				'cep' => '45490-000',
			),
			425 => 
			array (
				'id' => 781,
				'nome' => 'Lajedão',
				'uf' => 'BA',
				'cep2' => '2918902',
				'estado_cod' => 5,
				'cep' => '45950-000',
			),
			426 => 
			array (
				'id' => 782,
				'nome' => 'Lajedinho',
				'uf' => 'BA',
				'cep2' => '2919009',
				'estado_cod' => 5,
				'cep' => '46825-000',
			),
			427 => 
			array (
				'id' => 784,
				'nome' => 'Lajedo do Tabocal',
				'uf' => 'BA',
				'cep2' => '2919058',
				'estado_cod' => 5,
				'cep' => '45365-000',
			),
			428 => 
			array (
				'id' => 785,
				'nome' => 'Lamarão',
				'uf' => 'BA',
				'cep2' => '2919108',
				'estado_cod' => 5,
				'cep' => '48720-000',
			),
			429 => 
			array (
				'id' => 787,
				'nome' => 'Lapão',
				'uf' => 'BA',
				'cep2' => '2919157',
				'estado_cod' => 5,
				'cep' => '44905-000',
			),
			430 => 
			array (
				'id' => 789,
				'nome' => 'Lauro de Freitas',
				'uf' => 'BA',
				'cep2' => '2919207',
				'estado_cod' => 5,
				'cep' => '42700-000',
			),
			431 => 
			array (
				'id' => 790,
				'nome' => 'Lençóis',
				'uf' => 'BA',
				'cep2' => '2919306',
				'estado_cod' => 5,
				'cep' => '46960-000',
			),
			432 => 
			array (
				'id' => 791,
				'nome' => 'Licínio de Almeida',
				'uf' => 'BA',
				'cep2' => '2919405',
				'estado_cod' => 5,
				'cep' => '46330-000',
			),
			433 => 
			array (
				'id' => 793,
				'nome' => 'Livramento de Nossa Senhora',
				'uf' => 'BA',
				'cep2' => '2919504',
				'estado_cod' => 5,
				'cep' => '46140-000',
			),
			434 => 
			array (
				'id' => 797,
				'nome' => 'Macajuba',
				'uf' => 'BA',
				'cep2' => '2919603',
				'estado_cod' => 5,
				'cep' => '46805-000',
			),
			435 => 
			array (
				'id' => 798,
				'nome' => 'Macarani',
				'uf' => 'BA',
				'cep2' => '2919702',
				'estado_cod' => 5,
				'cep' => '45760-000',
			),
			436 => 
			array (
				'id' => 799,
				'nome' => 'Macaúbas',
				'uf' => 'BA',
				'cep2' => '2919801',
				'estado_cod' => 5,
				'cep' => '46500-000',
			),
			437 => 
			array (
				'id' => 800,
				'nome' => 'Macururé',
				'uf' => 'BA',
				'cep2' => '2919900',
				'estado_cod' => 5,
				'cep' => '48650-000',
			),
			438 => 
			array (
				'id' => 801,
				'nome' => 'Madre de Deus',
				'uf' => 'BA',
				'cep2' => '2919926',
				'estado_cod' => 5,
				'cep' => '42600-000',
			),
			439 => 
			array (
				'id' => 802,
				'nome' => 'Maetinga',
				'uf' => 'BA',
				'cep2' => '2919959',
				'estado_cod' => 5,
				'cep' => '46255-000',
			),
			440 => 
			array (
				'id' => 803,
				'nome' => 'Maiquinique',
				'uf' => 'BA',
				'cep2' => '2920007',
				'estado_cod' => 5,
				'cep' => '45770-000',
			),
			441 => 
			array (
				'id' => 804,
				'nome' => 'Mairi',
				'uf' => 'BA',
				'cep2' => '2920106',
				'estado_cod' => 5,
				'cep' => '44630-000',
			),
			442 => 
			array (
				'id' => 805,
				'nome' => 'Malhada',
				'uf' => 'BA',
				'cep2' => '2920205',
				'estado_cod' => 5,
				'cep' => '46440-000',
			),
			443 => 
			array (
				'id' => 806,
				'nome' => 'Malhada de Pedras',
				'uf' => 'BA',
				'cep2' => '2920304',
				'estado_cod' => 5,
				'cep' => '46110-000',
			),
			444 => 
			array (
				'id' => 810,
				'nome' => 'Manoel Vitorino',
				'uf' => 'BA',
				'cep2' => '2920403',
				'estado_cod' => 5,
				'cep' => '45240-000',
			),
			445 => 
			array (
				'id' => 811,
				'nome' => 'Mansidão',
				'uf' => 'BA',
				'cep2' => '2920452',
				'estado_cod' => 5,
				'cep' => '47160-000',
			),
			446 => 
			array (
				'id' => 814,
				'nome' => 'Maracás',
				'uf' => 'BA',
				'cep2' => '2920502',
				'estado_cod' => 5,
				'cep' => '45360-000',
			),
			447 => 
			array (
				'id' => 815,
				'nome' => 'Maragogipe',
				'uf' => 'BA',
				'cep2' => '2920601',
				'estado_cod' => 5,
				'cep' => '44420-000',
			),
			448 => 
			array (
				'id' => 817,
				'nome' => 'Maraú',
				'uf' => 'BA',
				'cep2' => '2920700',
				'estado_cod' => 5,
				'cep' => '45520-000',
			),
			449 => 
			array (
				'id' => 818,
				'nome' => 'Marcionílio Souza',
				'uf' => 'BA',
				'cep2' => '2920809',
				'estado_cod' => 5,
				'cep' => '46780-000',
			),
			450 => 
			array (
				'id' => 823,
				'nome' => 'Mascote',
				'uf' => 'BA',
				'cep2' => '2920908',
				'estado_cod' => 5,
				'cep' => '45870-000',
			),
			451 => 
			array (
				'id' => 827,
				'nome' => 'Mata de São João',
				'uf' => 'BA',
				'cep2' => '2921005',
				'estado_cod' => 5,
				'cep' => '48280-000',
			),
			452 => 
			array (
				'id' => 829,
				'nome' => 'Matina',
				'uf' => 'BA',
				'cep2' => '2921054',
				'estado_cod' => 5,
				'cep' => '46480-000',
			),
			453 => 
			array (
				'id' => 831,
				'nome' => 'Medeiros Neto',
				'uf' => 'BA',
				'cep2' => '2921104',
				'estado_cod' => 5,
				'cep' => '45960-000',
			),
			454 => 
			array (
				'id' => 832,
				'nome' => 'Miguel Calmon',
				'uf' => 'BA',
				'cep2' => '2921203',
				'estado_cod' => 5,
				'cep' => '44720-000',
			),
			455 => 
			array (
				'id' => 833,
				'nome' => 'Milagres',
				'uf' => 'BA',
				'cep2' => '2921302',
				'estado_cod' => 5,
				'cep' => '45315-000',
			),
			456 => 
			array (
				'id' => 834,
				'nome' => 'Luís Eduardo Magalhães',
				'uf' => 'BA',
				'cep2' => '',
				'estado_cod' => 5,
				'cep' => '47850-000',
			),
			457 => 
			array (
				'id' => 839,
				'nome' => 'Mirangaba',
				'uf' => 'BA',
				'cep2' => '2921401',
				'estado_cod' => 5,
				'cep' => '44745-000',
			),
			458 => 
			array (
				'id' => 840,
				'nome' => 'Mirante',
				'uf' => 'BA',
				'cep2' => '2921450',
				'estado_cod' => 5,
				'cep' => '45255-000',
			),
			459 => 
			array (
				'id' => 846,
				'nome' => 'Monte Santo',
				'uf' => 'BA',
				'cep2' => '2921500',
				'estado_cod' => 5,
				'cep' => '48800-000',
			),
			460 => 
			array (
				'id' => 847,
				'nome' => 'Morpará',
				'uf' => 'BA',
				'cep2' => '2921609',
				'estado_cod' => 5,
				'cep' => '47580-000',
			),
			461 => 
			array (
				'id' => 851,
				'nome' => 'Morro do Chapéu',
				'uf' => 'BA',
				'cep2' => '2921708',
				'estado_cod' => 5,
				'cep' => '44850-000',
			),
			462 => 
			array (
				'id' => 852,
				'nome' => 'Mortugaba',
				'uf' => 'BA',
				'cep2' => '2921807',
				'estado_cod' => 5,
				'cep' => '46290-000',
			),
			463 => 
			array (
				'id' => 853,
				'nome' => 'Mucugê',
				'uf' => 'BA',
				'cep2' => '2921906',
				'estado_cod' => 5,
				'cep' => '46750-000',
			),
			464 => 
			array (
				'id' => 854,
				'nome' => 'Mucuri',
				'uf' => 'BA',
				'cep2' => '2922003',
				'estado_cod' => 5,
				'cep' => '45930-000',
			),
			465 => 
			array (
				'id' => 855,
				'nome' => 'Mulungu do Morro',
				'uf' => 'BA',
				'cep2' => '2922052',
				'estado_cod' => 5,
				'cep' => '44885-000',
			),
			466 => 
			array (
				'id' => 856,
				'nome' => 'Mundo Novo',
				'uf' => 'BA',
				'cep2' => '2922102',
				'estado_cod' => 5,
				'cep' => '44800-000',
			),
			467 => 
			array (
				'id' => 857,
				'nome' => 'Muniz Ferreira',
				'uf' => 'BA',
				'cep2' => '2922201',
				'estado_cod' => 5,
				'cep' => '44575-000',
			),
			468 => 
			array (
				'id' => 858,
				'nome' => 'Muquém de São Francisco',
				'uf' => 'BA',
				'cep2' => '',
				'estado_cod' => 5,
				'cep' => '47115-000',
			),
			469 => 
			array (
				'id' => 859,
				'nome' => 'Muritiba',
				'uf' => 'BA',
				'cep2' => '2922300',
				'estado_cod' => 5,
				'cep' => '44340-000',
			),
			470 => 
			array (
				'id' => 861,
				'nome' => 'Mutuípe',
				'uf' => 'BA',
				'cep2' => '2922409',
				'estado_cod' => 5,
				'cep' => '45480-000',
			),
			471 => 
			array (
				'id' => 864,
				'nome' => 'Nazaré',
				'uf' => 'BA',
				'cep2' => '2922508',
				'estado_cod' => 5,
				'cep' => '44400-000',
			),
			472 => 
			array (
				'id' => 865,
				'nome' => 'Nilo Peçanha',
				'uf' => 'BA',
				'cep2' => '2922607',
				'estado_cod' => 5,
				'cep' => '45440-000',
			),
			473 => 
			array (
				'id' => 866,
				'nome' => 'Nordestina',
				'uf' => 'BA',
				'cep2' => '2922656',
				'estado_cod' => 5,
				'cep' => '48870-000',
			),
			474 => 
			array (
				'id' => 869,
				'nome' => 'Nova Canaã',
				'uf' => 'BA',
				'cep2' => '2922706',
				'estado_cod' => 5,
				'cep' => '45270-000',
			),
			475 => 
			array (
				'id' => 870,
				'nome' => 'Nova Fátima',
				'uf' => 'BA',
				'cep2' => '2922730',
				'estado_cod' => 5,
				'cep' => '44642-000',
			),
			476 => 
			array (
				'id' => 871,
				'nome' => 'Nova Ibiá',
				'uf' => 'BA',
				'cep2' => '2922755',
				'estado_cod' => 5,
				'cep' => '45452-000',
			),
			477 => 
			array (
				'id' => 873,
				'nome' => 'Nova Itarana',
				'uf' => 'BA',
				'cep2' => '2922805',
				'estado_cod' => 5,
				'cep' => '45390-000',
			),
			478 => 
			array (
				'id' => 875,
				'nome' => 'Nova Redenção',
				'uf' => 'BA',
				'cep2' => '2922854',
				'estado_cod' => 5,
				'cep' => '46835-000',
			),
			479 => 
			array (
				'id' => 876,
				'nome' => 'Nova Soure',
				'uf' => 'BA',
				'cep2' => '2922904',
				'estado_cod' => 5,
				'cep' => '48460-000',
			),
			480 => 
			array (
				'id' => 877,
				'nome' => 'Nova Viçosa',
				'uf' => 'BA',
				'cep2' => '2923001',
				'estado_cod' => 5,
				'cep' => '45920-000',
			),
			481 => 
			array (
				'id' => 879,
				'nome' => 'Novo Horizonte',
				'uf' => 'BA',
				'cep2' => '2923035',
				'estado_cod' => 5,
				'cep' => '46730-000',
			),
			482 => 
			array (
				'id' => 880,
				'nome' => 'Novo Triunfo',
				'uf' => 'BA',
				'cep2' => '2923050',
				'estado_cod' => 5,
				'cep' => '48455-000',
			),
			483 => 
			array (
				'id' => 885,
				'nome' => 'Olindina',
				'uf' => 'BA',
				'cep2' => '2923100',
				'estado_cod' => 5,
				'cep' => '48470-000',
			),
			484 => 
			array (
				'id' => 886,
				'nome' => 'Oliveira dos Brejinhos',
				'uf' => 'BA',
				'cep2' => '2923209',
				'estado_cod' => 5,
				'cep' => '47530-000',
			),
			485 => 
			array (
				'id' => 891,
				'nome' => 'Ouriçangas',
				'uf' => 'BA',
				'cep2' => '2923308',
				'estado_cod' => 5,
				'cep' => '48150-000',
			),
			486 => 
			array (
				'id' => 893,
				'nome' => 'Ourolândia',
				'uf' => 'BA',
				'cep2' => '2923357',
				'estado_cod' => 5,
				'cep' => '44718-000',
			),
			487 => 
			array (
				'id' => 898,
				'nome' => 'Palmas de Monte Alto',
				'uf' => 'BA',
				'cep2' => '2923407',
				'estado_cod' => 5,
				'cep' => '46460-000',
			),
			488 => 
			array (
				'id' => 899,
				'nome' => 'Palmeiras',
				'uf' => 'BA',
				'cep2' => '2923506',
				'estado_cod' => 5,
				'cep' => '46930-000',
			),
			489 => 
			array (
				'id' => 901,
				'nome' => 'Paramirim',
				'uf' => 'BA',
				'cep2' => '2923605',
				'estado_cod' => 5,
				'cep' => '46190-000',
			),
			490 => 
			array (
				'id' => 903,
				'nome' => 'Paratinga',
				'uf' => 'BA',
				'cep2' => '2923704',
				'estado_cod' => 5,
				'cep' => '47500-000',
			),
			491 => 
			array (
				'id' => 904,
				'nome' => 'Paripiranga',
				'uf' => 'BA',
				'cep2' => '2923803',
				'estado_cod' => 5,
				'cep' => '48430-000',
			),
			492 => 
			array (
				'id' => 908,
				'nome' => 'Pau Brasil',
				'uf' => 'BA',
				'cep2' => '2923902',
				'estado_cod' => 5,
				'cep' => '45890-000',
			),
			493 => 
			array (
				'id' => 909,
				'nome' => 'Paulo Afonso',
				'uf' => 'BA',
				'cep2' => '2924009',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			494 => 
			array (
				'id' => 910,
				'nome' => 'Pé de Serra',
				'uf' => 'BA',
				'cep2' => '2924058',
				'estado_cod' => 5,
				'cep' => '44655-000',
			),
			495 => 
			array (
				'id' => 911,
				'nome' => 'Pedrão',
				'uf' => 'BA',
				'cep2' => '2924108',
				'estado_cod' => 5,
				'cep' => '48140-000',
			),
			496 => 
			array (
				'id' => 913,
				'nome' => 'Pedro Alexandre',
				'uf' => 'BA',
				'cep2' => '2924207',
				'estado_cod' => 5,
				'cep' => '48580-000',
			),
			497 => 
			array (
				'id' => 917,
				'nome' => 'Piatã',
				'uf' => 'BA',
				'cep2' => '2924306',
				'estado_cod' => 5,
				'cep' => '46765-000',
			),
			498 => 
			array (
				'id' => 919,
				'nome' => 'Pilão Arcado',
				'uf' => 'BA',
				'cep2' => '2924405',
				'estado_cod' => 5,
				'cep' => '47240-000',
			),
			499 => 
			array (
				'id' => 921,
				'nome' => 'Pindaí',
				'uf' => 'BA',
				'cep2' => '2924504',
				'estado_cod' => 5,
				'cep' => '46360-000',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 922,
				'nome' => 'Pindobaçu',
				'uf' => 'BA',
				'cep2' => '2924603',
				'estado_cod' => 5,
				'cep' => '44770-000',
			),
			1 => 
			array (
				'id' => 924,
				'nome' => 'Pintadas',
				'uf' => 'BA',
				'cep2' => '2924652',
				'estado_cod' => 5,
				'cep' => '44610-000',
			),
			2 => 
			array (
				'id' => 926,
				'nome' => 'Piraí do Norte',
				'uf' => 'BA',
				'cep2' => '2924678',
				'estado_cod' => 5,
				'cep' => '45436-000',
			),
			3 => 
			array (
				'id' => 930,
				'nome' => 'Piripá',
				'uf' => 'BA',
				'cep2' => '2924702',
				'estado_cod' => 5,
				'cep' => '46270-000',
			),
			4 => 
			array (
				'id' => 931,
				'nome' => 'Piritiba',
				'uf' => 'BA',
				'cep2' => '2924801',
				'estado_cod' => 5,
				'cep' => '44830-000',
			),
			5 => 
			array (
				'id' => 933,
				'nome' => 'Planaltino',
				'uf' => 'BA',
				'cep2' => '2924900',
				'estado_cod' => 5,
				'cep' => '45375-000',
			),
			6 => 
			array (
				'id' => 934,
				'nome' => 'Planalto',
				'uf' => 'BA',
				'cep2' => '2925006',
				'estado_cod' => 5,
				'cep' => '45190-000',
			),
			7 => 
			array (
				'id' => 937,
				'nome' => 'Poções',
				'uf' => 'BA',
				'cep2' => '2925105',
				'estado_cod' => 5,
				'cep' => '45260-000',
			),
			8 => 
			array (
				'id' => 939,
				'nome' => 'Pojuca',
				'uf' => 'BA',
				'cep2' => '2925204',
				'estado_cod' => 5,
				'cep' => '48120-000',
			),
			9 => 
			array (
				'id' => 942,
				'nome' => 'Ponto Novo',
				'uf' => 'BA',
				'cep2' => '2925253',
				'estado_cod' => 5,
				'cep' => '44755-000',
			),
			10 => 
			array (
				'id' => 944,
				'nome' => 'Porto Seguro',
				'uf' => 'BA',
				'cep2' => '2925303',
				'estado_cod' => 5,
				'cep' => '45810-000',
			),
			11 => 
			array (
				'id' => 946,
				'nome' => 'Potiraguá',
				'uf' => 'BA',
				'cep2' => '2925402',
				'estado_cod' => 5,
				'cep' => '45790-000',
			),
			12 => 
			array (
				'id' => 948,
				'nome' => 'Prado',
				'uf' => 'BA',
				'cep2' => '2925501',
				'estado_cod' => 5,
				'cep' => '45980-000',
			),
			13 => 
			array (
				'id' => 949,
				'nome' => 'Presidente Dutra',
				'uf' => 'BA',
				'cep2' => '2925600',
				'estado_cod' => 5,
				'cep' => '44930-000',
			),
			14 => 
			array (
				'id' => 950,
				'nome' => 'Presidente Jânio Quadros',
				'uf' => 'BA',
				'cep2' => '2925709',
				'estado_cod' => 5,
				'cep' => '46250-000',
			),
			15 => 
			array (
				'id' => 951,
				'nome' => 'Presidente Tancredo Neves',
				'uf' => 'BA',
				'cep2' => '2925758',
				'estado_cod' => 5,
				'cep' => '45416-000',
			),
			16 => 
			array (
				'id' => 954,
				'nome' => 'Queimadas',
				'uf' => 'BA',
				'cep2' => '2925808',
				'estado_cod' => 5,
				'cep' => '48860-000',
			),
			17 => 
			array (
				'id' => 955,
				'nome' => 'Quijingue',
				'uf' => 'BA',
				'cep2' => '2925907',
				'estado_cod' => 5,
				'cep' => '48830-000',
			),
			18 => 
			array (
				'id' => 957,
				'nome' => 'Quixabeira',
				'uf' => 'BA',
				'cep2' => '2925931',
				'estado_cod' => 5,
				'cep' => '44713-000',
			),
			19 => 
			array (
				'id' => 958,
				'nome' => 'Rafael Jambeiro',
				'uf' => 'BA',
				'cep2' => '2925956',
				'estado_cod' => 5,
				'cep' => '44520-000',
			),
			20 => 
			array (
				'id' => 960,
				'nome' => 'Remanso',
				'uf' => 'BA',
				'cep2' => '2926004',
				'estado_cod' => 5,
				'cep' => '47200-000',
			),
			21 => 
			array (
				'id' => 962,
				'nome' => 'Retirolândia',
				'uf' => 'BA',
				'cep2' => '2926103',
				'estado_cod' => 5,
				'cep' => '48750-000',
			),
			22 => 
			array (
				'id' => 963,
				'nome' => 'Riachão das Neves',
				'uf' => 'BA',
				'cep2' => '2926202',
				'estado_cod' => 5,
				'cep' => '47970-000',
			),
			23 => 
			array (
				'id' => 964,
				'nome' => 'Riachão do Jacuípe',
				'uf' => 'BA',
				'cep2' => '2926301',
				'estado_cod' => 5,
				'cep' => '44640-000',
			),
			24 => 
			array (
				'id' => 967,
				'nome' => 'Riacho de Santana',
				'uf' => 'BA',
				'cep2' => '2926400',
				'estado_cod' => 5,
				'cep' => '46470-000',
			),
			25 => 
			array (
				'id' => 969,
				'nome' => 'Ribeira do Amparo',
				'uf' => 'BA',
				'cep2' => '2926509',
				'estado_cod' => 5,
				'cep' => '48440-000',
			),
			26 => 
			array (
				'id' => 970,
				'nome' => 'Ribeira do Pombal',
				'uf' => 'BA',
				'cep2' => '2926608',
				'estado_cod' => 5,
				'cep' => '48400-000',
			),
			27 => 
			array (
				'id' => 971,
				'nome' => 'Ribeirão do Largo',
				'uf' => 'BA',
				'cep2' => '2926657',
				'estado_cod' => 5,
				'cep' => '45155-000',
			),
			28 => 
			array (
				'id' => 974,
				'nome' => 'Rio de Contas',
				'uf' => 'BA',
				'cep2' => '2926707',
				'estado_cod' => 5,
				'cep' => '46170-000',
			),
			29 => 
			array (
				'id' => 975,
				'nome' => 'Rio do Antônio',
				'uf' => 'BA',
				'cep2' => '2926806',
				'estado_cod' => 5,
				'cep' => '46220-000',
			),
			30 => 
			array (
				'id' => 978,
				'nome' => 'Rio do Pires',
				'uf' => 'BA',
				'cep2' => '2926905',
				'estado_cod' => 5,
				'cep' => '46550-000',
			),
			31 => 
			array (
				'id' => 980,
				'nome' => 'Rio Real',
				'uf' => 'BA',
				'cep2' => '2927002',
				'estado_cod' => 5,
				'cep' => '48330-000',
			),
			32 => 
			array (
				'id' => 981,
				'nome' => 'Rodelas',
				'uf' => 'BA',
				'cep2' => '2927101',
				'estado_cod' => 5,
				'cep' => '48630-000',
			),
			33 => 
			array (
				'id' => 982,
				'nome' => 'Ruy Barbosa',
				'uf' => 'BA',
				'cep2' => '2927200',
				'estado_cod' => 5,
				'cep' => '46800-000',
			),
			34 => 
			array (
				'id' => 985,
				'nome' => 'Salinas da Margarida',
				'uf' => 'BA',
				'cep2' => '2927309',
				'estado_cod' => 5,
				'cep' => '44450-000',
			),
			35 => 
			array (
				'id' => 988,
				'nome' => 'Salvador',
				'uf' => 'BA',
				'cep2' => '2927408',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			36 => 
			array (
				'id' => 990,
				'nome' => 'Santa Bárbara',
				'uf' => 'BA',
				'cep2' => '2927507',
				'estado_cod' => 5,
				'cep' => '44150-000',
			),
			37 => 
			array (
				'id' => 991,
				'nome' => 'Santa Brígida',
				'uf' => 'BA',
				'cep2' => '2927606',
				'estado_cod' => 5,
				'cep' => '48570-000',
			),
			38 => 
			array (
				'id' => 992,
				'nome' => 'Santa Cruz Cabrália',
				'uf' => 'BA',
				'cep2' => '2927705',
				'estado_cod' => 5,
				'cep' => '45807-000',
			),
			39 => 
			array (
				'id' => 993,
				'nome' => 'Santa Cruz da Vitória',
				'uf' => 'BA',
				'cep2' => '2927804',
				'estado_cod' => 5,
				'cep' => '45725-000',
			),
			40 => 
			array (
				'id' => 994,
				'nome' => 'Santa Inês',
				'uf' => 'BA',
				'cep2' => '2927903',
				'estado_cod' => 5,
				'cep' => '45320-000',
			),
			41 => 
			array (
				'id' => 995,
				'nome' => 'Santa Luzia',
				'uf' => 'BA',
				'cep2' => '2928059',
				'estado_cod' => 5,
				'cep' => '45865-000',
			),
			42 => 
			array (
				'id' => 996,
				'nome' => 'Santa Maria da Vitória',
				'uf' => 'BA',
				'cep2' => '2928109',
				'estado_cod' => 5,
				'cep' => '47640-000',
			),
			43 => 
			array (
				'id' => 997,
				'nome' => 'Santa Rita de Cássia',
				'uf' => 'BA',
				'cep2' => '2928406',
				'estado_cod' => 5,
				'cep' => '47150-000',
			),
			44 => 
			array (
				'id' => 998,
				'nome' => 'Santa Terezinha',
				'uf' => 'BA',
				'cep2' => '',
				'estado_cod' => 5,
				'cep' => '44590-000',
			),
			45 => 
			array (
				'id' => 999,
				'nome' => 'Santaluz',
				'uf' => 'BA',
				'cep2' => '2928000',
				'estado_cod' => 5,
				'cep' => '48880-000',
			),
			46 => 
			array (
				'id' => 1000,
				'nome' => 'Santana',
				'uf' => 'BA',
				'cep2' => '2928208',
				'estado_cod' => 5,
				'cep' => '47700-000',
			),
			47 => 
			array (
				'id' => 1002,
				'nome' => 'Santanópolis',
				'uf' => 'BA',
				'cep2' => '2928307',
				'estado_cod' => 5,
				'cep' => '44260-000',
			),
			48 => 
			array (
				'id' => 1004,
				'nome' => 'Santo Amaro',
				'uf' => 'BA',
				'cep2' => '2928604',
				'estado_cod' => 5,
				'cep' => '44200-000',
			),
			49 => 
			array (
				'id' => 1006,
				'nome' => 'Santo Antônio de Jesus',
				'uf' => 'BA',
				'cep2' => '2928703',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			50 => 
			array (
				'id' => 1007,
				'nome' => 'Santo Estevão',
				'uf' => 'BA',
				'cep2' => '',
				'estado_cod' => 5,
				'cep' => '44190-000',
			),
			51 => 
			array (
				'id' => 1009,
				'nome' => 'São Desidério',
				'uf' => 'BA',
				'cep2' => '2928901',
				'estado_cod' => 5,
				'cep' => '47820-000',
			),
			52 => 
			array (
				'id' => 1010,
				'nome' => 'São Domingos',
				'uf' => 'BA',
				'cep2' => '2928950',
				'estado_cod' => 5,
				'cep' => '48895-000',
			),
			53 => 
			array (
				'id' => 1011,
				'nome' => 'São Felipe',
				'uf' => 'BA',
				'cep2' => '2929107',
				'estado_cod' => 5,
				'cep' => '44550-000',
			),
			54 => 
			array (
				'id' => 1012,
				'nome' => 'São Félix',
				'uf' => 'BA',
				'cep2' => '2929008',
				'estado_cod' => 5,
				'cep' => '44360-000',
			),
			55 => 
			array (
				'id' => 1013,
				'nome' => 'São Félix do Coribe',
				'uf' => 'BA',
				'cep2' => '2929057',
				'estado_cod' => 5,
				'cep' => '47665-000',
			),
			56 => 
			array (
				'id' => 1014,
				'nome' => 'São Francisco do Conde',
				'uf' => 'BA',
				'cep2' => '2929206',
				'estado_cod' => 5,
				'cep' => '43900-000',
			),
			57 => 
			array (
				'id' => 1015,
				'nome' => 'São Gabriel',
				'uf' => 'BA',
				'cep2' => '2929255',
				'estado_cod' => 5,
				'cep' => '44915-000',
			),
			58 => 
			array (
				'id' => 1016,
				'nome' => 'São Gonçalo dos Campos',
				'uf' => 'BA',
				'cep2' => '2929305',
				'estado_cod' => 5,
				'cep' => '44330-000',
			),
			59 => 
			array (
				'id' => 1019,
				'nome' => 'São José da Vitória',
				'uf' => 'BA',
				'cep2' => '2929354',
				'estado_cod' => 5,
				'cep' => '45620-000',
			),
			60 => 
			array (
				'id' => 1021,
				'nome' => 'São José do Jacuípe',
				'uf' => 'BA',
				'cep2' => '2929370',
				'estado_cod' => 5,
				'cep' => '44698-000',
			),
			61 => 
			array (
				'id' => 1024,
				'nome' => 'São Miguel das Matas',
				'uf' => 'BA',
				'cep2' => '2929404',
				'estado_cod' => 5,
				'cep' => '44580-000',
			),
			62 => 
			array (
				'id' => 1027,
				'nome' => 'São Sebastião do Passe',
				'uf' => 'BA',
				'cep2' => '',
				'estado_cod' => 5,
				'cep' => '43850-000',
			),
			63 => 
			array (
				'id' => 1029,
				'nome' => 'Sapeaçu',
				'uf' => 'BA',
				'cep2' => '2929602',
				'estado_cod' => 5,
				'cep' => '44530-000',
			),
			64 => 
			array (
				'id' => 1030,
				'nome' => 'Sátiro Dias',
				'uf' => 'BA',
				'cep2' => '2929701',
				'estado_cod' => 5,
				'cep' => '48485-000',
			),
			65 => 
			array (
				'id' => 1031,
				'nome' => 'Saubara',
				'uf' => 'BA',
				'cep2' => '2929750',
				'estado_cod' => 5,
				'cep' => '44220-000',
			),
			66 => 
			array (
				'id' => 1033,
				'nome' => 'Saúde',
				'uf' => 'BA',
				'cep2' => '2929800',
				'estado_cod' => 5,
				'cep' => '44740-000',
			),
			67 => 
			array (
				'id' => 1034,
				'nome' => 'Seabra',
				'uf' => 'BA',
				'cep2' => '2929909',
				'estado_cod' => 5,
				'cep' => '46900-000',
			),
			68 => 
			array (
				'id' => 1035,
				'nome' => 'Sebastião Laranjeiras',
				'uf' => 'BA',
				'cep2' => '2930006',
				'estado_cod' => 5,
				'cep' => '46450-000',
			),
			69 => 
			array (
				'id' => 1036,
				'nome' => 'Senhor do Bonfim',
				'uf' => 'BA',
				'cep2' => '2930105',
				'estado_cod' => 5,
				'cep' => '48970-000',
			),
			70 => 
			array (
				'id' => 1037,
				'nome' => 'Sento Sé',
				'uf' => 'BA',
				'cep2' => '2930204',
				'estado_cod' => 5,
				'cep' => '47350-000',
			),
			71 => 
			array (
				'id' => 1040,
				'nome' => 'Serra do Ramalho',
				'uf' => 'BA',
				'cep2' => '2930154',
				'estado_cod' => 5,
				'cep' => '47630-000',
			),
			72 => 
			array (
				'id' => 1041,
				'nome' => 'Serra Dourada',
				'uf' => 'BA',
				'cep2' => '2930303',
				'estado_cod' => 5,
				'cep' => '47740-000',
			),
			73 => 
			array (
				'id' => 1044,
				'nome' => 'Serra Preta',
				'uf' => 'BA',
				'cep2' => '2930402',
				'estado_cod' => 5,
				'cep' => '44660-000',
			),
			74 => 
			array (
				'id' => 1045,
				'nome' => 'Serrinha',
				'uf' => 'BA',
				'cep2' => '2930501',
				'estado_cod' => 5,
				'cep' => '48700-000',
			),
			75 => 
			array (
				'id' => 1046,
				'nome' => 'Serrolândia',
				'uf' => 'BA',
				'cep2' => '2930600',
				'estado_cod' => 5,
				'cep' => '44710-000',
			),
			76 => 
			array (
				'id' => 1047,
				'nome' => 'Simões Filho',
				'uf' => 'BA',
				'cep2' => '2930709',
				'estado_cod' => 5,
				'cep' => '43700-000',
			),
			77 => 
			array (
				'id' => 1049,
				'nome' => 'Sítio do Mato',
				'uf' => 'BA',
				'cep2' => '2930758',
				'estado_cod' => 5,
				'cep' => '47610-000',
			),
			78 => 
			array (
				'id' => 1051,
				'nome' => 'Sítio do Quinto',
				'uf' => 'BA',
				'cep2' => '2930766',
				'estado_cod' => 5,
				'cep' => '48565-000',
			),
			79 => 
			array (
				'id' => 1055,
				'nome' => 'Sobradinho',
				'uf' => 'BA',
				'cep2' => '2930774',
				'estado_cod' => 5,
				'cep' => '48925-000',
			),
			80 => 
			array (
				'id' => 1056,
				'nome' => 'Souto Soares',
				'uf' => 'BA',
				'cep2' => '2930808',
				'estado_cod' => 5,
				'cep' => '46990-000',
			),
			81 => 
			array (
				'id' => 1059,
				'nome' => 'Tabocas do Brejo Velho',
				'uf' => 'BA',
				'cep2' => '2930907',
				'estado_cod' => 5,
				'cep' => '47760-000',
			),
			82 => 
			array (
				'id' => 1064,
				'nome' => 'Tanhaçu',
				'uf' => 'BA',
				'cep2' => '2931004',
				'estado_cod' => 5,
				'cep' => '46600-000',
			),
			83 => 
			array (
				'id' => 1065,
				'nome' => 'Tanque Novo',
				'uf' => 'BA',
				'cep2' => '2931053',
				'estado_cod' => 5,
				'cep' => '46580-000',
			),
			84 => 
			array (
				'id' => 1066,
				'nome' => 'Tanquinho',
				'uf' => 'BA',
				'cep2' => '2931103',
				'estado_cod' => 5,
				'cep' => '44160-000',
			),
			85 => 
			array (
				'id' => 1068,
				'nome' => 'Taperoá',
				'uf' => 'BA',
				'cep2' => '2931202',
				'estado_cod' => 5,
				'cep' => '45430-000',
			),
			86 => 
			array (
				'id' => 1071,
				'nome' => 'Tapiramutá',
				'uf' => 'BA',
				'cep2' => '2931301',
				'estado_cod' => 5,
				'cep' => '44840-000',
			),
			87 => 
			array (
				'id' => 1078,
				'nome' => 'Teixeira de Freitas',
				'uf' => 'BA',
				'cep2' => '2931350',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			88 => 
			array (
				'id' => 1079,
				'nome' => 'Teodoro Sampaio',
				'uf' => 'BA',
				'cep2' => '2931400',
				'estado_cod' => 5,
				'cep' => '44280-000',
			),
			89 => 
			array (
				'id' => 1080,
				'nome' => 'Teofilândia',
				'uf' => 'BA',
				'cep2' => '2931509',
				'estado_cod' => 5,
				'cep' => '48770-000',
			),
			90 => 
			array (
				'id' => 1081,
				'nome' => 'Teolândia',
				'uf' => 'BA',
				'cep2' => '2931608',
				'estado_cod' => 5,
				'cep' => '45465-000',
			),
			91 => 
			array (
				'id' => 1082,
				'nome' => 'Terra Nova',
				'uf' => 'BA',
				'cep2' => '2931707',
				'estado_cod' => 5,
				'cep' => '44270-000',
			),
			92 => 
			array (
				'id' => 1085,
				'nome' => 'Tremedal',
				'uf' => 'BA',
				'cep2' => '2931806',
				'estado_cod' => 5,
				'cep' => '45170-000',
			),
			93 => 
			array (
				'id' => 1087,
				'nome' => 'Tucano',
				'uf' => 'BA',
				'cep2' => '2931905',
				'estado_cod' => 5,
				'cep' => '48790-000',
			),
			94 => 
			array (
				'id' => 1088,
				'nome' => 'Uauá',
				'uf' => 'BA',
				'cep2' => '2932002',
				'estado_cod' => 5,
				'cep' => '48950-000',
			),
			95 => 
			array (
				'id' => 1089,
				'nome' => 'Ubaíra',
				'uf' => 'BA',
				'cep2' => '2932101',
				'estado_cod' => 5,
				'cep' => '45310-000',
			),
			96 => 
			array (
				'id' => 1090,
				'nome' => 'Ubaitaba',
				'uf' => 'BA',
				'cep2' => '2932200',
				'estado_cod' => 5,
				'cep' => '45545-000',
			),
			97 => 
			array (
				'id' => 1091,
				'nome' => 'Ubatã',
				'uf' => 'BA',
				'cep2' => '2932309',
				'estado_cod' => 5,
				'cep' => '45550-000',
			),
			98 => 
			array (
				'id' => 1094,
				'nome' => 'Uibaí',
				'uf' => 'BA',
				'cep2' => '2932408',
				'estado_cod' => 5,
				'cep' => '44950-000',
			),
			99 => 
			array (
				'id' => 1095,
				'nome' => 'Umburanas',
				'uf' => 'BA',
				'cep2' => '2932457',
				'estado_cod' => 5,
				'cep' => '44798-000',
			),
			100 => 
			array (
				'id' => 1097,
				'nome' => 'Una',
				'uf' => 'BA',
				'cep2' => '2932507',
				'estado_cod' => 5,
				'cep' => '45690-000',
			),
			101 => 
			array (
				'id' => 1098,
				'nome' => 'Urandi',
				'uf' => 'BA',
				'cep2' => '2932606',
				'estado_cod' => 5,
				'cep' => '46350-000',
			),
			102 => 
			array (
				'id' => 1099,
				'nome' => 'Uruçuca',
				'uf' => 'BA',
				'cep2' => '2932705',
				'estado_cod' => 5,
				'cep' => '45680-000',
			),
			103 => 
			array (
				'id' => 1100,
				'nome' => 'Utinga',
				'uf' => 'BA',
				'cep2' => '2932804',
				'estado_cod' => 5,
				'cep' => '46810-000',
			),
			104 => 
			array (
				'id' => 1102,
				'nome' => 'Valença',
				'uf' => 'BA',
				'cep2' => '2932903',
				'estado_cod' => 5,
				'cep' => '45400-000',
			),
			105 => 
			array (
				'id' => 1103,
				'nome' => 'Valente',
				'uf' => 'BA',
				'cep2' => '2933000',
				'estado_cod' => 5,
				'cep' => '48890-000',
			),
			106 => 
			array (
				'id' => 1104,
				'nome' => 'Várzea da Roça',
				'uf' => 'BA',
				'cep2' => '2933059',
				'estado_cod' => 5,
				'cep' => '44635-000',
			),
			107 => 
			array (
				'id' => 1107,
				'nome' => 'Várzea do Poço',
				'uf' => 'BA',
				'cep2' => '2933109',
				'estado_cod' => 5,
				'cep' => '44715-000',
			),
			108 => 
			array (
				'id' => 1108,
				'nome' => 'Várzea Nova',
				'uf' => 'BA',
				'cep2' => '2933158',
				'estado_cod' => 5,
				'cep' => '44690-000',
			),
			109 => 
			array (
				'id' => 1110,
				'nome' => 'Varzedo',
				'uf' => 'BA',
				'cep2' => '2933174',
				'estado_cod' => 5,
				'cep' => '44565-000',
			),
			110 => 
			array (
				'id' => 1113,
				'nome' => 'Vera Cruz',
				'uf' => 'BA',
				'cep2' => '2933208',
				'estado_cod' => 5,
				'cep' => '44470-000',
			),
			111 => 
			array (
				'id' => 1114,
				'nome' => 'Vereda',
				'uf' => 'BA',
				'cep2' => '2933257',
				'estado_cod' => 5,
				'cep' => '45955-000',
			),
			112 => 
			array (
				'id' => 1116,
				'nome' => 'Vitória da Conquista',
				'uf' => 'BA',
				'cep2' => '2933307',
				'estado_cod' => 5,
				'cep' => 'LOC',
			),
			113 => 
			array (
				'id' => 1118,
				'nome' => 'Wagner',
				'uf' => 'BA',
				'cep2' => '2933406',
				'estado_cod' => 5,
				'cep' => '46970-000',
			),
			114 => 
			array (
				'id' => 1119,
				'nome' => 'Wanderley',
				'uf' => 'BA',
				'cep2' => '2933455',
				'estado_cod' => 5,
				'cep' => '47940-000',
			),
			115 => 
			array (
				'id' => 1120,
				'nome' => 'Wenceslau Guimarães',
				'uf' => 'BA',
				'cep2' => '2933505',
				'estado_cod' => 5,
				'cep' => '45460-000',
			),
			116 => 
			array (
				'id' => 1121,
				'nome' => 'Xique-Xique',
				'uf' => 'BA',
				'cep2' => '2933604',
				'estado_cod' => 5,
				'cep' => '47400-000',
			),
			117 => 
			array (
				'id' => 1122,
				'nome' => 'Abaiara',
				'uf' => 'CE',
				'cep2' => '2300101',
				'estado_cod' => 6,
				'cep' => '63240-000',
			),
			118 => 
			array (
				'id' => 1124,
				'nome' => 'Acarape',
				'uf' => 'CE',
				'cep2' => '2300150',
				'estado_cod' => 6,
				'cep' => '62785-000',
			),
			119 => 
			array (
				'id' => 1125,
				'nome' => 'Acaraú',
				'uf' => 'CE',
				'cep2' => '2300200',
				'estado_cod' => 6,
				'cep' => '62580-000',
			),
			120 => 
			array (
				'id' => 1126,
				'nome' => 'Acopiara',
				'uf' => 'CE',
				'cep2' => '2300309',
				'estado_cod' => 6,
				'cep' => '63560-000',
			),
			121 => 
			array (
				'id' => 1131,
				'nome' => 'Aiuaba',
				'uf' => 'CE',
				'cep2' => '2300408',
				'estado_cod' => 6,
				'cep' => '63575-000',
			),
			122 => 
			array (
				'id' => 1134,
				'nome' => 'Alcântaras',
				'uf' => 'CE',
				'cep2' => '2300507',
				'estado_cod' => 6,
				'cep' => '62120-000',
			),
			123 => 
			array (
				'id' => 1137,
				'nome' => 'Altaneira',
				'uf' => 'CE',
				'cep2' => '2300606',
				'estado_cod' => 6,
				'cep' => '63195-000',
			),
			124 => 
			array (
				'id' => 1138,
				'nome' => 'Alto Santo',
				'uf' => 'CE',
				'cep2' => '2300705',
				'estado_cod' => 6,
				'cep' => '62970-000',
			),
			125 => 
			array (
				'id' => 1145,
				'nome' => 'Amontada',
				'uf' => 'CE',
				'cep2' => '2300754',
				'estado_cod' => 6,
				'cep' => '62540-000',
			),
			126 => 
			array (
				'id' => 1149,
				'nome' => 'Antonina do Norte',
				'uf' => 'CE',
				'cep2' => '2300804',
				'estado_cod' => 6,
				'cep' => '63570-000',
			),
			127 => 
			array (
				'id' => 1154,
				'nome' => 'Apuiarés',
				'uf' => 'CE',
				'cep2' => '2300903',
				'estado_cod' => 6,
				'cep' => '62630-000',
			),
			128 => 
			array (
				'id' => 1156,
				'nome' => 'Aquiraz',
				'uf' => 'CE',
				'cep2' => '2301000',
				'estado_cod' => 6,
				'cep' => '61700-000',
			),
			129 => 
			array (
				'id' => 1158,
				'nome' => 'Aracati',
				'uf' => 'CE',
				'cep2' => '2301109',
				'estado_cod' => 6,
				'cep' => '62800-000',
			),
			130 => 
			array (
				'id' => 1161,
				'nome' => 'Aracoiaba',
				'uf' => 'CE',
				'cep2' => '',
				'estado_cod' => 6,
				'cep' => '62750-000',
			),
			131 => 
			array (
				'id' => 1168,
				'nome' => 'Ararendá',
				'uf' => 'CE',
				'cep2' => '2301257',
				'estado_cod' => 6,
				'cep' => '62210-000',
			),
			132 => 
			array (
				'id' => 1169,
				'nome' => 'Araripe',
				'uf' => 'CE',
				'cep2' => '2301307',
				'estado_cod' => 6,
				'cep' => '63170-000',
			),
			133 => 
			array (
				'id' => 1173,
				'nome' => 'Aratuba',
				'uf' => 'CE',
				'cep2' => '2301406',
				'estado_cod' => 6,
				'cep' => '62762-000',
			),
			134 => 
			array (
				'id' => 1176,
				'nome' => 'Arneiroz',
				'uf' => 'CE',
				'cep2' => '2301505',
				'estado_cod' => 6,
				'cep' => '63670-000',
			),
			135 => 
			array (
				'id' => 1180,
				'nome' => 'Assaré',
				'uf' => 'CE',
				'cep2' => '2301604',
				'estado_cod' => 6,
				'cep' => '63140-000',
			),
			136 => 
			array (
				'id' => 1183,
				'nome' => 'Aurora',
				'uf' => 'CE',
				'cep2' => '2301703',
				'estado_cod' => 6,
				'cep' => '63360-000',
			),
			137 => 
			array (
				'id' => 1185,
				'nome' => 'Baixio',
				'uf' => 'CE',
				'cep2' => '2301802',
				'estado_cod' => 6,
				'cep' => '63320-000',
			),
			138 => 
			array (
				'id' => 1187,
				'nome' => 'Banabuiú',
				'uf' => 'CE',
				'cep2' => '2301851',
				'estado_cod' => 6,
				'cep' => '63960-000',
			),
			139 => 
			array (
				'id' => 1190,
				'nome' => 'Barbalha',
				'uf' => 'CE',
				'cep2' => '2301901',
				'estado_cod' => 6,
				'cep' => '63180-000',
			),
			140 => 
			array (
				'id' => 1196,
				'nome' => 'Barreira',
				'uf' => 'CE',
				'cep2' => '2301950',
				'estado_cod' => 6,
				'cep' => '62795-000',
			),
			141 => 
			array (
				'id' => 1203,
				'nome' => 'Barro',
				'uf' => 'CE',
				'cep2' => '2302008',
				'estado_cod' => 6,
				'cep' => '63380-000',
			),
			142 => 
			array (
				'id' => 1205,
				'nome' => 'Barroquinha',
				'uf' => 'CE',
				'cep2' => '2302057',
				'estado_cod' => 6,
				'cep' => '62410-000',
			),
			143 => 
			array (
				'id' => 1206,
				'nome' => 'Baturité',
				'uf' => 'CE',
				'cep2' => '2302107',
				'estado_cod' => 6,
				'cep' => '62760-000',
			),
			144 => 
			array (
				'id' => 1208,
				'nome' => 'Beberibe',
				'uf' => 'CE',
				'cep2' => '2302206',
				'estado_cod' => 6,
				'cep' => '62840-000',
			),
			145 => 
			array (
				'id' => 1209,
				'nome' => 'Bela Cruz',
				'uf' => 'CE',
				'cep2' => '2302305',
				'estado_cod' => 6,
				'cep' => '62570-000',
			),
			146 => 
			array (
				'id' => 1218,
				'nome' => 'Boa Viagem',
				'uf' => 'CE',
				'cep2' => '2302404',
				'estado_cod' => 6,
				'cep' => '63870-000',
			),
			147 => 
			array (
				'id' => 1229,
				'nome' => 'Brejo Santo',
				'uf' => 'CE',
				'cep2' => '2302503',
				'estado_cod' => 6,
				'cep' => '63260-000',
			),
			148 => 
			array (
				'id' => 1247,
				'nome' => 'Camocim',
				'uf' => 'CE',
				'cep2' => '2302602',
				'estado_cod' => 6,
				'cep' => '62400-000',
			),
			149 => 
			array (
				'id' => 1250,
				'nome' => 'Campos Sales',
				'uf' => 'CE',
				'cep2' => '2302701',
				'estado_cod' => 6,
				'cep' => '63150-000',
			),
			150 => 
			array (
				'id' => 1256,
				'nome' => 'Canindé',
				'uf' => 'CE',
				'cep2' => '2302800',
				'estado_cod' => 6,
				'cep' => '62700-000',
			),
			151 => 
			array (
				'id' => 1260,
				'nome' => 'Capistrano',
				'uf' => 'CE',
				'cep2' => '2302909',
				'estado_cod' => 6,
				'cep' => '62748-000',
			),
			152 => 
			array (
				'id' => 1264,
				'nome' => 'Caridade',
				'uf' => 'CE',
				'cep2' => '2303006',
				'estado_cod' => 6,
				'cep' => '62730-000',
			),
			153 => 
			array (
				'id' => 1265,
				'nome' => 'Cariré',
				'uf' => 'CE',
				'cep2' => '2303105',
				'estado_cod' => 6,
				'cep' => '62184-000',
			),
			154 => 
			array (
				'id' => 1266,
				'nome' => 'Caririaçu',
				'uf' => 'CE',
				'cep2' => '2303204',
				'estado_cod' => 6,
				'cep' => '63220-000',
			),
			155 => 
			array (
				'id' => 1267,
				'nome' => 'Cariús',
				'uf' => 'CE',
				'cep2' => '2303303',
				'estado_cod' => 6,
				'cep' => '63530-000',
			),
			156 => 
			array (
				'id' => 1270,
				'nome' => 'Carnaubal',
				'uf' => 'CE',
				'cep2' => '2303402',
				'estado_cod' => 6,
				'cep' => '62375-000',
			),
			157 => 
			array (
				'id' => 1278,
				'nome' => 'Cascavel',
				'uf' => 'CE',
				'cep2' => '2303501',
				'estado_cod' => 6,
				'cep' => '62850-000',
			),
			158 => 
			array (
				'id' => 1280,
				'nome' => 'Catarina',
				'uf' => 'CE',
				'cep2' => '2303600',
				'estado_cod' => 6,
				'cep' => '63595-000',
			),
			159 => 
			array (
				'id' => 1283,
				'nome' => 'Catunda',
				'uf' => 'CE',
				'cep2' => '2303659',
				'estado_cod' => 6,
				'cep' => '62297-000',
			),
			160 => 
			array (
				'id' => 1284,
				'nome' => 'Caucaia',
				'uf' => 'CE',
				'cep2' => '2303709',
				'estado_cod' => 6,
				'cep' => 'LOC',
			),
			161 => 
			array (
				'id' => 1287,
				'nome' => 'Cedro',
				'uf' => 'CE',
				'cep2' => '2303808',
				'estado_cod' => 6,
				'cep' => '63400-000',
			),
			162 => 
			array (
				'id' => 1289,
				'nome' => 'Chaval',
				'uf' => 'CE',
				'cep2' => '2303907',
				'estado_cod' => 6,
				'cep' => '62420-000',
			),
			163 => 
			array (
				'id' => 1290,
				'nome' => 'Choró',
				'uf' => 'CE',
				'cep2' => '2303931',
				'estado_cod' => 6,
				'cep' => '63950-000',
			),
			164 => 
			array (
				'id' => 1291,
				'nome' => 'Chorozinho',
				'uf' => 'CE',
				'cep2' => '2303956',
				'estado_cod' => 6,
				'cep' => '62875-000',
			),
			165 => 
			array (
				'id' => 1298,
				'nome' => 'Coreaú',
				'uf' => 'CE',
				'cep2' => '2304004',
				'estado_cod' => 6,
				'cep' => '62160-000',
			),
			166 => 
			array (
				'id' => 1300,
				'nome' => 'Crateús',
				'uf' => 'CE',
				'cep2' => '2304103',
				'estado_cod' => 6,
				'cep' => '63700-000',
			),
			167 => 
			array (
				'id' => 1301,
				'nome' => 'Crato',
				'uf' => 'CE',
				'cep2' => '2304202',
				'estado_cod' => 6,
				'cep' => 'LOC',
			),
			168 => 
			array (
				'id' => 1304,
				'nome' => 'Croatá',
				'uf' => 'CE',
				'cep2' => '2304236',
				'estado_cod' => 6,
				'cep' => '62390-000',
			),
			169 => 
			array (
				'id' => 1308,
				'nome' => 'Cruz',
				'uf' => 'CE',
				'cep2' => '2304251',
				'estado_cod' => 6,
				'cep' => '62595-000',
			),
			170 => 
			array (
				'id' => 1318,
				'nome' => 'Deputado Irapuan Pinheiro',
				'uf' => 'CE',
				'cep2' => '2304269',
				'estado_cod' => 6,
				'cep' => '63645-000',
			),
			171 => 
			array (
				'id' => 1333,
				'nome' => 'Ererê',
				'uf' => 'CE',
				'cep2' => '2304277',
				'estado_cod' => 6,
				'cep' => '63470-000',
			),
			172 => 
			array (
				'id' => 1337,
				'nome' => 'Eusébio',
				'uf' => 'CE',
				'cep2' => '2304285',
				'estado_cod' => 6,
				'cep' => '61760-000',
			),
			173 => 
			array (
				'id' => 1338,
				'nome' => 'Farias Brito',
				'uf' => 'CE',
				'cep2' => '2304301',
				'estado_cod' => 6,
				'cep' => '63185-000',
			),
			174 => 
			array (
				'id' => 1346,
				'nome' => 'Forquilha',
				'uf' => 'CE',
				'cep2' => '2304350',
				'estado_cod' => 6,
				'cep' => '62115-000',
			),
			175 => 
			array (
				'id' => 1347,
				'nome' => 'Fortaleza',
				'uf' => 'CE',
				'cep2' => '2304400',
				'estado_cod' => 6,
				'cep' => 'LOC',
			),
			176 => 
			array (
				'id' => 1348,
				'nome' => 'Fortim',
				'uf' => 'CE',
				'cep2' => '2304459',
				'estado_cod' => 6,
				'cep' => '62815-000',
			),
			177 => 
			array (
				'id' => 1349,
				'nome' => 'Frecheirinha',
				'uf' => 'CE',
				'cep2' => '2304509',
				'estado_cod' => 6,
				'cep' => '62340-000',
			),
			178 => 
			array (
				'id' => 1355,
				'nome' => 'General Sampaio',
				'uf' => 'CE',
				'cep2' => '2304608',
				'estado_cod' => 6,
				'cep' => '62738-000',
			),
			179 => 
			array (
				'id' => 1361,
				'nome' => 'Graça',
				'uf' => 'CE',
				'cep2' => '2304657',
				'estado_cod' => 6,
				'cep' => '62365-000',
			),
			180 => 
			array (
				'id' => 1362,
				'nome' => 'Granja',
				'uf' => 'CE',
				'cep2' => '2304707',
				'estado_cod' => 6,
				'cep' => '62430-000',
			),
			181 => 
			array (
				'id' => 1363,
				'nome' => 'Granjeiro',
				'uf' => 'CE',
				'cep2' => '2304806',
				'estado_cod' => 6,
				'cep' => '63230-000',
			),
			182 => 
			array (
				'id' => 1364,
				'nome' => 'Groairas',
				'uf' => 'CE',
				'cep2' => '',
				'estado_cod' => 6,
				'cep' => '62190-000',
			),
			183 => 
			array (
				'id' => 1365,
				'nome' => 'Guaiúba',
				'uf' => 'CE',
				'cep2' => '2304954',
				'estado_cod' => 6,
				'cep' => '61890-000',
			),
			184 => 
			array (
				'id' => 1368,
				'nome' => 'Guaraciaba do Norte',
				'uf' => 'CE',
				'cep2' => '2305001',
				'estado_cod' => 6,
				'cep' => '62380-000',
			),
			185 => 
			array (
				'id' => 1369,
				'nome' => 'Guaramiranga',
				'uf' => 'CE',
				'cep2' => '2305100',
				'estado_cod' => 6,
				'cep' => '62766-000',
			),
			186 => 
			array (
				'id' => 1375,
				'nome' => 'Hidrolândia',
				'uf' => 'CE',
				'cep2' => '2305209',
				'estado_cod' => 6,
				'cep' => '62270-000',
			),
			187 => 
			array (
				'id' => 1377,
				'nome' => 'Horizonte',
				'uf' => 'CE',
				'cep2' => '2305233',
				'estado_cod' => 6,
				'cep' => '62880-000',
			),
			188 => 
			array (
				'id' => 1380,
				'nome' => 'Ibaretama',
				'uf' => 'CE',
				'cep2' => '2305266',
				'estado_cod' => 6,
				'cep' => '63970-000',
			),
			189 => 
			array (
				'id' => 1382,
				'nome' => 'Ibiapina',
				'uf' => 'CE',
				'cep2' => '2305308',
				'estado_cod' => 6,
				'cep' => '62360-000',
			),
			190 => 
			array (
				'id' => 1386,
				'nome' => 'Ibicuitinga',
				'uf' => 'CE',
				'cep2' => '2305332',
				'estado_cod' => 6,
				'cep' => '62955-000',
			),
			191 => 
			array (
				'id' => 1390,
				'nome' => 'Icapuí',
				'uf' => 'CE',
				'cep2' => '2305357',
				'estado_cod' => 6,
				'cep' => '62810-000',
			),
			192 => 
			array (
				'id' => 1392,
				'nome' => 'Icó',
				'uf' => 'CE',
				'cep2' => '2305407',
				'estado_cod' => 6,
				'cep' => '63430-000',
			),
			193 => 
			array (
				'id' => 1396,
				'nome' => 'Iguatu',
				'uf' => 'CE',
				'cep2' => '2305506',
				'estado_cod' => 6,
				'cep' => '63500-000',
			),
			194 => 
			array (
				'id' => 1397,
				'nome' => 'Independência',
				'uf' => 'CE',
				'cep2' => '2305605',
				'estado_cod' => 6,
				'cep' => '63640-000',
			),
			195 => 
			array (
				'id' => 1402,
				'nome' => 'Ipaporanga',
				'uf' => 'CE',
				'cep2' => '2305654',
				'estado_cod' => 6,
				'cep' => '62215-000',
			),
			196 => 
			array (
				'id' => 1403,
				'nome' => 'Ipaumirim',
				'uf' => 'CE',
				'cep2' => '2305704',
				'estado_cod' => 6,
				'cep' => '63340-000',
			),
			197 => 
			array (
				'id' => 1404,
				'nome' => 'Ipu',
				'uf' => 'CE',
				'cep2' => '2305803',
				'estado_cod' => 6,
				'cep' => '62250-000',
			),
			198 => 
			array (
				'id' => 1405,
				'nome' => 'Ipueiras',
				'uf' => 'CE',
				'cep2' => '2305902',
				'estado_cod' => 6,
				'cep' => '62230-000',
			),
			199 => 
			array (
				'id' => 1407,
				'nome' => 'Iracema',
				'uf' => 'CE',
				'cep2' => '2306009',
				'estado_cod' => 6,
				'cep' => '62980-000',
			),
			200 => 
			array (
				'id' => 1411,
				'nome' => 'Irauçuba',
				'uf' => 'CE',
				'cep2' => '2306108',
				'estado_cod' => 6,
				'cep' => '62620-000',
			),
			201 => 
			array (
				'id' => 1415,
				'nome' => 'Itaiçaba',
				'uf' => 'CE',
				'cep2' => '2306207',
				'estado_cod' => 6,
				'cep' => '62820-000',
			),
			202 => 
			array (
				'id' => 1417,
				'nome' => 'Itaitinga',
				'uf' => 'CE',
				'cep2' => '2306256',
				'estado_cod' => 6,
				'cep' => '61880-000',
			),
			203 => 
			array (
				'id' => 1419,
				'nome' => 'Itapajé',
				'uf' => 'CE',
				'cep2' => '',
				'estado_cod' => 6,
				'cep' => '62600-000',
			),
			204 => 
			array (
				'id' => 1422,
				'nome' => 'Itapipoca',
				'uf' => 'CE',
				'cep2' => '2306405',
				'estado_cod' => 6,
				'cep' => '62500-000',
			),
			205 => 
			array (
				'id' => 1423,
				'nome' => 'Itapiúna',
				'uf' => 'CE',
				'cep2' => '2306504',
				'estado_cod' => 6,
				'cep' => '62740-000',
			),
			206 => 
			array (
				'id' => 1425,
				'nome' => 'Itarema',
				'uf' => 'CE',
				'cep2' => '2306553',
				'estado_cod' => 6,
				'cep' => '62590-000',
			),
			207 => 
			array (
				'id' => 1426,
				'nome' => 'Itatira',
				'uf' => 'CE',
				'cep2' => '2306603',
				'estado_cod' => 6,
				'cep' => '62720-000',
			),
			208 => 
			array (
				'id' => 1432,
				'nome' => 'Jaguaretama',
				'uf' => 'CE',
				'cep2' => '2306702',
				'estado_cod' => 6,
				'cep' => '63480-000',
			),
			209 => 
			array (
				'id' => 1433,
				'nome' => 'Jaguaribara',
				'uf' => 'CE',
				'cep2' => '2306801',
				'estado_cod' => 6,
				'cep' => '63490-000',
			),
			210 => 
			array (
				'id' => 1434,
				'nome' => 'Jaguaribe',
				'uf' => 'CE',
				'cep2' => '2306900',
				'estado_cod' => 6,
				'cep' => '63475-000',
			),
			211 => 
			array (
				'id' => 1435,
				'nome' => 'Jaguaruana',
				'uf' => 'CE',
				'cep2' => '2307007',
				'estado_cod' => 6,
				'cep' => '62823-000',
			),
			212 => 
			array (
				'id' => 1439,
				'nome' => 'Jardim',
				'uf' => 'CE',
				'cep2' => '2307106',
				'estado_cod' => 6,
				'cep' => '63290-000',
			),
			213 => 
			array (
				'id' => 1442,
				'nome' => 'Jati',
				'uf' => 'CE',
				'cep2' => '2307205',
				'estado_cod' => 6,
				'cep' => '63275-000',
			),
			214 => 
			array (
				'id' => 1443,
				'nome' => 'Jijoca de Jericoacoara',
				'uf' => 'CE',
				'cep2' => '2307254',
				'estado_cod' => 6,
				'cep' => '62598-000',
			),
			215 => 
			array (
				'id' => 1451,
				'nome' => 'Juazeiro do Norte',
				'uf' => 'CE',
				'cep2' => '2307304',
				'estado_cod' => 6,
				'cep' => 'LOC',
			),
			216 => 
			array (
				'id' => 1453,
				'nome' => 'Jucás',
				'uf' => 'CE',
				'cep2' => '2307403',
				'estado_cod' => 6,
				'cep' => '63580-000',
			),
			217 => 
			array (
				'id' => 1468,
				'nome' => 'Lavras da Mangabeira',
				'uf' => 'CE',
				'cep2' => '2307502',
				'estado_cod' => 6,
				'cep' => '63300-000',
			),
			218 => 
			array (
				'id' => 1470,
				'nome' => 'Limoeiro do Norte',
				'uf' => 'CE',
				'cep2' => '2307601',
				'estado_cod' => 6,
				'cep' => '62930-000',
			),
			219 => 
			array (
				'id' => 1478,
				'nome' => 'Madalena',
				'uf' => 'CE',
				'cep2' => '2307635',
				'estado_cod' => 6,
				'cep' => '63860-000',
			),
			220 => 
			array (
				'id' => 1486,
				'nome' => 'Maracanaú',
				'uf' => 'CE',
				'cep2' => '2307650',
				'estado_cod' => 6,
				'cep' => 'LOC',
			),
			221 => 
			array (
				'id' => 1488,
				'nome' => 'Maranguape',
				'uf' => 'CE',
				'cep2' => '2307700',
				'estado_cod' => 6,
				'cep' => 'LOC',
			),
			222 => 
			array (
				'id' => 1490,
				'nome' => 'Marco',
				'uf' => 'CE',
				'cep2' => '2307809',
				'estado_cod' => 6,
				'cep' => '62560-000',
			),
			223 => 
			array (
				'id' => 1495,
				'nome' => 'Martinópole',
				'uf' => 'CE',
				'cep2' => '2307908',
				'estado_cod' => 6,
				'cep' => '62450-000',
			),
			224 => 
			array (
				'id' => 1496,
				'nome' => 'Massapê',
				'uf' => 'CE',
				'cep2' => '2308005',
				'estado_cod' => 6,
				'cep' => '62140-000',
			),
			225 => 
			array (
				'id' => 1500,
				'nome' => 'Mauriti',
				'uf' => 'CE',
				'cep2' => '2308104',
				'estado_cod' => 6,
				'cep' => '63210-000',
			),
			226 => 
			array (
				'id' => 1502,
				'nome' => 'Meruoca',
				'uf' => 'CE',
				'cep2' => '2308203',
				'estado_cod' => 6,
				'cep' => '62130-000',
			),
			227 => 
			array (
				'id' => 1505,
				'nome' => 'Milagres',
				'uf' => 'CE',
				'cep2' => '2308302',
				'estado_cod' => 6,
				'cep' => '63250-000',
			),
			228 => 
			array (
				'id' => 1506,
				'nome' => 'Milhã',
				'uf' => 'CE',
				'cep2' => '2308351',
				'estado_cod' => 6,
				'cep' => '63635-000',
			),
			229 => 
			array (
				'id' => 1510,
				'nome' => 'Miraíma',
				'uf' => 'CE',
				'cep2' => '2308377',
				'estado_cod' => 6,
				'cep' => '62530-000',
			),
			230 => 
			array (
				'id' => 1513,
				'nome' => 'Missão Velha',
				'uf' => 'CE',
				'cep2' => '2308401',
				'estado_cod' => 6,
				'cep' => '63200-000',
			),
			231 => 
			array (
				'id' => 1516,
				'nome' => 'Mombaça',
				'uf' => 'CE',
				'cep2' => '2308500',
				'estado_cod' => 6,
				'cep' => '63610-000',
			),
			232 => 
			array (
				'id' => 1519,
				'nome' => 'Monsenhor Tabosa',
				'uf' => 'CE',
				'cep2' => '2308609',
				'estado_cod' => 6,
				'cep' => '63780-000',
			),
			233 => 
			array (
				'id' => 1526,
				'nome' => 'Morada Nova',
				'uf' => 'CE',
				'cep2' => '2308708',
				'estado_cod' => 6,
				'cep' => '62940-000',
			),
			234 => 
			array (
				'id' => 1527,
				'nome' => 'Moraújo',
				'uf' => 'CE',
				'cep2' => '2308807',
				'estado_cod' => 6,
				'cep' => '62480-000',
			),
			235 => 
			array (
				'id' => 1528,
				'nome' => 'Morrinhos',
				'uf' => 'CE',
				'cep2' => '2308906',
				'estado_cod' => 6,
				'cep' => '62550-000',
			),
			236 => 
			array (
				'id' => 1531,
				'nome' => 'Mucambo',
				'uf' => 'CE',
				'cep2' => '2309003',
				'estado_cod' => 6,
				'cep' => '62170-000',
			),
			237 => 
			array (
				'id' => 1532,
				'nome' => 'Mulungu',
				'uf' => 'CE',
				'cep2' => '2309102',
				'estado_cod' => 6,
				'cep' => '62764-000',
			),
			238 => 
			array (
				'id' => 1547,
				'nome' => 'Nova Olinda',
				'uf' => 'CE',
				'cep2' => '2309201',
				'estado_cod' => 6,
				'cep' => '63165-000',
			),
			239 => 
			array (
				'id' => 1548,
				'nome' => 'Nova Russas',
				'uf' => 'CE',
				'cep2' => '2309300',
				'estado_cod' => 6,
				'cep' => '62200-000',
			),
			240 => 
			array (
				'id' => 1551,
				'nome' => 'Novo Oriente',
				'uf' => 'CE',
				'cep2' => '2309409',
				'estado_cod' => 6,
				'cep' => '63740-000',
			),
			241 => 
			array (
				'id' => 1553,
				'nome' => 'Ocara',
				'uf' => 'CE',
				'cep2' => '2309458',
				'estado_cod' => 6,
				'cep' => '62755-000',
			),
			242 => 
			array (
				'id' => 1559,
				'nome' => 'Orós',
				'uf' => 'CE',
				'cep2' => '2309508',
				'estado_cod' => 6,
				'cep' => '63520-000',
			),
			243 => 
			array (
				'id' => 1560,
				'nome' => 'Pacajus',
				'uf' => 'CE',
				'cep2' => '2309607',
				'estado_cod' => 6,
				'cep' => '62870-000',
			),
			244 => 
			array (
				'id' => 1561,
				'nome' => 'Pacatuba',
				'uf' => 'CE',
				'cep2' => '2309706',
				'estado_cod' => 6,
				'cep' => '61800-000',
			),
			245 => 
			array (
				'id' => 1562,
				'nome' => 'Pacoti',
				'uf' => 'CE',
				'cep2' => '2309805',
				'estado_cod' => 6,
				'cep' => '62770-000',
			),
			246 => 
			array (
				'id' => 1563,
				'nome' => 'Pacujá',
				'uf' => 'CE',
				'cep2' => '2309904',
				'estado_cod' => 6,
				'cep' => '62180-000',
			),
			247 => 
			array (
				'id' => 1572,
				'nome' => 'Palhano',
				'uf' => 'CE',
				'cep2' => '2310001',
				'estado_cod' => 6,
				'cep' => '62910-000',
			),
			248 => 
			array (
				'id' => 1573,
				'nome' => 'Palmácia',
				'uf' => 'CE',
				'cep2' => '2310100',
				'estado_cod' => 6,
				'cep' => '62780-000',
			),
			249 => 
			array (
				'id' => 1577,
				'nome' => 'Paracuru',
				'uf' => 'CE',
				'cep2' => '2310209',
				'estado_cod' => 6,
				'cep' => '62680-000',
			),
			250 => 
			array (
				'id' => 1578,
				'nome' => 'Paraipaba',
				'uf' => 'CE',
				'cep2' => '2310258',
				'estado_cod' => 6,
				'cep' => '62685-000',
			),
			251 => 
			array (
				'id' => 1580,
				'nome' => 'Parambu',
				'uf' => 'CE',
				'cep2' => '2310308',
				'estado_cod' => 6,
				'cep' => '63680-000',
			),
			252 => 
			array (
				'id' => 1581,
				'nome' => 'Paramoti',
				'uf' => 'CE',
				'cep2' => '2310407',
				'estado_cod' => 6,
				'cep' => '62736-000',
			),
			253 => 
			array (
				'id' => 1594,
				'nome' => 'Pedra Branca',
				'uf' => 'CE',
				'cep2' => '2310506',
				'estado_cod' => 6,
				'cep' => '63630-000',
			),
			254 => 
			array (
				'id' => 1600,
				'nome' => 'Penaforte',
				'uf' => 'CE',
				'cep2' => '2310605',
				'estado_cod' => 6,
				'cep' => '63280-000',
			),
			255 => 
			array (
				'id' => 1601,
				'nome' => 'Pentecoste',
				'uf' => 'CE',
				'cep2' => '2310704',
				'estado_cod' => 6,
				'cep' => '62640-000',
			),
			256 => 
			array (
				'id' => 1602,
				'nome' => 'Pereiro',
				'uf' => 'CE',
				'cep2' => '2310803',
				'estado_cod' => 6,
				'cep' => '63460-000',
			),
			257 => 
			array (
				'id' => 1606,
				'nome' => 'Pindoretama',
				'uf' => 'CE',
				'cep2' => '2310852',
				'estado_cod' => 6,
				'cep' => '62860-000',
			),
			258 => 
			array (
				'id' => 1608,
				'nome' => 'Piquet Carneiro',
				'uf' => 'CE',
				'cep2' => '2310902',
				'estado_cod' => 6,
				'cep' => '63605-000',
			),
			259 => 
			array (
				'id' => 1611,
				'nome' => 'Pires Ferreira',
				'uf' => 'CE',
				'cep2' => '2310951',
				'estado_cod' => 6,
				'cep' => '62255-000',
			),
			260 => 
			array (
				'id' => 1621,
				'nome' => 'Poranga',
				'uf' => 'CE',
				'cep2' => '2311009',
				'estado_cod' => 6,
				'cep' => '62220-000',
			),
			261 => 
			array (
				'id' => 1623,
				'nome' => 'Porteiras',
				'uf' => 'CE',
				'cep2' => '2311108',
				'estado_cod' => 6,
				'cep' => '63270-000',
			),
			262 => 
			array (
				'id' => 1624,
				'nome' => 'Potengi',
				'uf' => 'CE',
				'cep2' => '2311207',
				'estado_cod' => 6,
				'cep' => '63160-000',
			),
			263 => 
			array (
				'id' => 1626,
				'nome' => 'Potiretama',
				'uf' => 'CE',
				'cep2' => '2311231',
				'estado_cod' => 6,
				'cep' => '62990-000',
			),
			264 => 
			array (
				'id' => 1635,
				'nome' => 'Quiterianópolis',
				'uf' => 'CE',
				'cep2' => '2311264',
				'estado_cod' => 6,
				'cep' => '63650-000',
			),
			265 => 
			array (
				'id' => 1636,
				'nome' => 'Quixadá',
				'uf' => 'CE',
				'cep2' => '2311306',
				'estado_cod' => 6,
				'cep' => '63900-000',
			),
			266 => 
			array (
				'id' => 1638,
				'nome' => 'Quixelô',
				'uf' => 'CE',
				'cep2' => '2311355',
				'estado_cod' => 6,
				'cep' => '63515-000',
			),
			267 => 
			array (
				'id' => 1639,
				'nome' => 'Quixeramobim',
				'uf' => 'CE',
				'cep2' => '2311405',
				'estado_cod' => 6,
				'cep' => '63800-000',
			),
			268 => 
			array (
				'id' => 1640,
				'nome' => 'Quixeré',
				'uf' => 'CE',
				'cep2' => '2311504',
				'estado_cod' => 6,
				'cep' => '62920-000',
			),
			269 => 
			array (
				'id' => 1643,
				'nome' => 'Redenção',
				'uf' => 'CE',
				'cep2' => '2311603',
				'estado_cod' => 6,
				'cep' => '62790-000',
			),
			270 => 
			array (
				'id' => 1644,
				'nome' => 'Reriutaba',
				'uf' => 'CE',
				'cep2' => '2311702',
				'estado_cod' => 6,
				'cep' => '62260-000',
			),
			271 => 
			array (
				'id' => 1651,
				'nome' => 'Russas',
				'uf' => 'CE',
				'cep2' => '2311801',
				'estado_cod' => 6,
				'cep' => '62900-000',
			),
			272 => 
			array (
				'id' => 1653,
				'nome' => 'Saboeiro',
				'uf' => 'CE',
				'cep2' => '2311900',
				'estado_cod' => 6,
				'cep' => '63590-000',
			),
			273 => 
			array (
				'id' => 1656,
				'nome' => 'Salitre',
				'uf' => 'CE',
				'cep2' => '2311959',
				'estado_cod' => 6,
				'cep' => '63155-000',
			),
			274 => 
			array (
				'id' => 1662,
				'nome' => 'Santa Quitéria',
				'uf' => 'CE',
				'cep2' => '2312205',
				'estado_cod' => 6,
				'cep' => '62280-000',
			),
			275 => 
			array (
				'id' => 1666,
				'nome' => 'Santana do Acaraú',
				'uf' => 'CE',
				'cep2' => '2312007',
				'estado_cod' => 6,
				'cep' => '62150-000',
			),
			276 => 
			array (
				'id' => 1667,
				'nome' => 'Santana do Cariri',
				'uf' => 'CE',
				'cep2' => '2312106',
				'estado_cod' => 6,
				'cep' => '63190-000',
			),
			277 => 
			array (
				'id' => 1676,
				'nome' => 'São Benedito',
				'uf' => 'CE',
				'cep2' => '2312304',
				'estado_cod' => 6,
				'cep' => '62370-000',
			),
			278 => 
			array (
				'id' => 1682,
				'nome' => 'São Gonçalo do Amarante',
				'uf' => 'CE',
				'cep2' => '2312403',
				'estado_cod' => 6,
				'cep' => '62670-000',
			),
			279 => 
			array (
				'id' => 1685,
				'nome' => 'São João do Jaguaribe',
				'uf' => 'CE',
				'cep2' => '2312502',
				'estado_cod' => 6,
				'cep' => '62965-000',
			),
			280 => 
			array (
				'id' => 1696,
				'nome' => 'São Luís do Curu',
				'uf' => 'CE',
				'cep2' => '2312601',
				'estado_cod' => 6,
				'cep' => '62665-000',
			),
			281 => 
			array (
				'id' => 1710,
				'nome' => 'Senador Pompeu',
				'uf' => 'CE',
				'cep2' => '2312700',
				'estado_cod' => 6,
				'cep' => '63600-000',
			),
			282 => 
			array (
				'id' => 1711,
				'nome' => 'Senador Sá',
				'uf' => 'CE',
				'cep2' => '2312809',
				'estado_cod' => 6,
				'cep' => '62470-000',
			),
			283 => 
			array (
				'id' => 1722,
				'nome' => 'Sobral',
				'uf' => 'CE',
				'cep2' => '2312908',
				'estado_cod' => 6,
				'cep' => 'LOC',
			),
			284 => 
			array (
				'id' => 1724,
				'nome' => 'Solonópole',
				'uf' => 'CE',
				'cep2' => '2313005',
				'estado_cod' => 6,
				'cep' => '63620-000',
			),
			285 => 
			array (
				'id' => 1731,
				'nome' => 'Tabuleiro do Norte',
				'uf' => 'CE',
				'cep2' => '2313104',
				'estado_cod' => 6,
				'cep' => '62960-000',
			),
			286 => 
			array (
				'id' => 1733,
				'nome' => 'Tamboril',
				'uf' => 'CE',
				'cep2' => '2313203',
				'estado_cod' => 6,
				'cep' => '63750-000',
			),
			287 => 
			array (
				'id' => 1739,
				'nome' => 'Tarrafas',
				'uf' => 'CE',
				'cep2' => '2313252',
				'estado_cod' => 6,
				'cep' => '63145-000',
			),
			288 => 
			array (
				'id' => 1740,
				'nome' => 'Tauá',
				'uf' => 'CE',
				'cep2' => '2313302',
				'estado_cod' => 6,
				'cep' => '63660-000',
			),
			289 => 
			array (
				'id' => 1741,
				'nome' => 'Tejuçuoca',
				'uf' => 'CE',
				'cep2' => '2313351',
				'estado_cod' => 6,
				'cep' => '62610-000',
			),
			290 => 
			array (
				'id' => 1742,
				'nome' => 'Tianguá',
				'uf' => 'CE',
				'cep2' => '2313401',
				'estado_cod' => 6,
				'cep' => '62320-000',
			),
			291 => 
			array (
				'id' => 1746,
				'nome' => 'Trairi',
				'uf' => 'CE',
				'cep2' => '2313500',
				'estado_cod' => 6,
				'cep' => '62690-000',
			),
			292 => 
			array (
				'id' => 1755,
				'nome' => 'Tururu',
				'uf' => 'CE',
				'cep2' => '2313559',
				'estado_cod' => 6,
				'cep' => '62655-000',
			),
			293 => 
			array (
				'id' => 1756,
				'nome' => 'Ubajara',
				'uf' => 'CE',
				'cep2' => '2313609',
				'estado_cod' => 6,
				'cep' => '62350-000',
			),
			294 => 
			array (
				'id' => 1760,
				'nome' => 'Umari',
				'uf' => 'CE',
				'cep2' => '2313708',
				'estado_cod' => 6,
				'cep' => '63310-000',
			),
			295 => 
			array (
				'id' => 1763,
				'nome' => 'Umirim',
				'uf' => 'CE',
				'cep2' => '2313757',
				'estado_cod' => 6,
				'cep' => '62660-000',
			),
			296 => 
			array (
				'id' => 1764,
				'nome' => 'Uruburetama',
				'uf' => 'CE',
				'cep2' => '2313807',
				'estado_cod' => 6,
				'cep' => '62650-000',
			),
			297 => 
			array (
				'id' => 1765,
				'nome' => 'Uruoca',
				'uf' => 'CE',
				'cep2' => '2313906',
				'estado_cod' => 6,
				'cep' => '62460-000',
			),
			298 => 
			array (
				'id' => 1767,
				'nome' => 'Varjota',
				'uf' => 'CE',
				'cep2' => '2313955',
				'estado_cod' => 6,
				'cep' => '62265-000',
			),
			299 => 
			array (
				'id' => 1769,
				'nome' => 'Várzea Alegre',
				'uf' => 'CE',
				'cep2' => '2314003',
				'estado_cod' => 6,
				'cep' => '63540-000',
			),
			300 => 
			array (
				'id' => 1776,
				'nome' => 'Viçosa do Ceará',
				'uf' => 'CE',
				'cep2' => '2314102',
				'estado_cod' => 6,
				'cep' => '62300-000',
			),
			301 => 
			array (
				'id' => 1778,
				'nome' => 'Brasília',
				'uf' => 'DF',
				'cep2' => '5300108',
				'estado_cod' => 7,
				'cep' => 'LOC',
			),
			302 => 
			array (
				'id' => 1798,
				'nome' => 'Afonso Cláudio',
				'uf' => 'ES',
				'cep2' => '3200102',
				'estado_cod' => 8,
				'cep' => '29600-000',
			),
			303 => 
			array (
				'id' => 1800,
				'nome' => 'Água Doce do Norte',
				'uf' => 'ES',
				'cep2' => '3200169',
				'estado_cod' => 8,
				'cep' => '29820-000',
			),
			304 => 
			array (
				'id' => 1801,
				'nome' => 'Águia Branca',
				'uf' => 'ES',
				'cep2' => '3200136',
				'estado_cod' => 8,
				'cep' => '29795-000',
			),
			305 => 
			array (
				'id' => 1803,
				'nome' => 'Alegre',
				'uf' => 'ES',
				'cep2' => '3200201',
				'estado_cod' => 8,
				'cep' => '29500-000',
			),
			306 => 
			array (
				'id' => 1804,
				'nome' => 'Alfredo Chaves',
				'uf' => 'ES',
				'cep2' => '3200300',
				'estado_cod' => 8,
				'cep' => '29240-000',
			),
			307 => 
			array (
				'id' => 1808,
				'nome' => 'Alto Rio Novo',
				'uf' => 'ES',
				'cep2' => '3200359',
				'estado_cod' => 8,
				'cep' => '29760-000',
			),
			308 => 
			array (
				'id' => 1810,
				'nome' => 'Anchieta',
				'uf' => 'ES',
				'cep2' => '3200409',
				'estado_cod' => 8,
				'cep' => '29230-000',
			),
			309 => 
			array (
				'id' => 1813,
				'nome' => 'Apiacá',
				'uf' => 'ES',
				'cep2' => '3200508',
				'estado_cod' => 8,
				'cep' => '29450-000',
			),
			310 => 
			array (
				'id' => 1816,
				'nome' => 'Aracruz',
				'uf' => 'ES',
				'cep2' => '3200607',
				'estado_cod' => 8,
				'cep' => 'LOC',
			),
			311 => 
			array (
				'id' => 1821,
				'nome' => 'Atílio Vivácqua',
				'uf' => 'ES',
				'cep2' => '',
				'estado_cod' => 8,
				'cep' => '29490-000',
			),
			312 => 
			array (
				'id' => 1823,
				'nome' => 'Baixo Guandu',
				'uf' => 'ES',
				'cep2' => '3200805',
				'estado_cod' => 8,
				'cep' => '29730-000',
			),
			313 => 
			array (
				'id' => 1825,
				'nome' => 'Barra de São Francisco',
				'uf' => 'ES',
				'cep2' => '3200904',
				'estado_cod' => 8,
				'cep' => '29800-000',
			),
			314 => 
			array (
				'id' => 1831,
				'nome' => 'Boa Esperança',
				'uf' => 'ES',
				'cep2' => '3201001',
				'estado_cod' => 8,
				'cep' => '29845-000',
			),
			315 => 
			array (
				'id' => 1833,
				'nome' => 'Bom Jesus do Norte',
				'uf' => 'ES',
				'cep2' => '3201100',
				'estado_cod' => 8,
				'cep' => '29460-000',
			),
			316 => 
			array (
				'id' => 1836,
				'nome' => 'Brejetuba',
				'uf' => 'ES',
				'cep2' => '3201159',
				'estado_cod' => 8,
				'cep' => '29630-000',
			),
			317 => 
			array (
				'id' => 1839,
				'nome' => 'Cachoeiro de Itapemirim',
				'uf' => 'ES',
				'cep2' => '3201209',
				'estado_cod' => 8,
				'cep' => 'LOC',
			),
			318 => 
			array (
				'id' => 1844,
				'nome' => 'Cariacica',
				'uf' => 'ES',
				'cep2' => '3201308',
				'estado_cod' => 8,
				'cep' => 'LOC',
			),
			319 => 
			array (
				'id' => 1845,
				'nome' => 'Castelo',
				'uf' => 'ES',
				'cep2' => '3201407',
				'estado_cod' => 8,
				'cep' => '29360-000',
			),
			320 => 
			array (
				'id' => 1847,
				'nome' => 'Colatina',
				'uf' => 'ES',
				'cep2' => '3201506',
				'estado_cod' => 8,
				'cep' => 'LOC',
			),
			321 => 
			array (
				'id' => 1848,
				'nome' => 'Conceição da Barra',
				'uf' => 'ES',
				'cep2' => '3201605',
				'estado_cod' => 8,
				'cep' => '29960-000',
			),
			322 => 
			array (
				'id' => 1849,
				'nome' => 'Conceição do Castelo',
				'uf' => 'ES',
				'cep2' => '3201704',
				'estado_cod' => 8,
				'cep' => '29370-000',
			),
			323 => 
			array (
				'id' => 1858,
				'nome' => 'Divino de São Lourenço',
				'uf' => 'ES',
				'cep2' => '3201803',
				'estado_cod' => 8,
				'cep' => '29590-000',
			),
			324 => 
			array (
				'id' => 1861,
				'nome' => 'Domingos Martins',
				'uf' => 'ES',
				'cep2' => '3201902',
				'estado_cod' => 8,
				'cep' => '29260-000',
			),
			325 => 
			array (
				'id' => 1863,
				'nome' => 'Dores do Rio Preto',
				'uf' => 'ES',
				'cep2' => '3202009',
				'estado_cod' => 8,
				'cep' => '29580-000',
			),
			326 => 
			array (
				'id' => 1865,
				'nome' => 'Ecoporanga',
				'uf' => 'ES',
				'cep2' => '3202108',
				'estado_cod' => 8,
				'cep' => '29850-000',
			),
			327 => 
			array (
				'id' => 1869,
				'nome' => 'Fundão',
				'uf' => 'ES',
				'cep2' => '3202207',
				'estado_cod' => 8,
				'cep' => '29185-000',
			),
			328 => 
			array (
				'id' => 1874,
				'nome' => 'Governador Lindenberg',
				'uf' => 'ES',
				'cep2' => '3202256',
				'estado_cod' => 8,
				'cep' => '29720-000',
			),
			329 => 
			array (
				'id' => 1877,
				'nome' => 'Guaçuí',
				'uf' => 'ES',
				'cep2' => '3202306',
				'estado_cod' => 8,
				'cep' => '29560-000',
			),
			330 => 
			array (
				'id' => 1879,
				'nome' => 'Guarapari',
				'uf' => 'ES',
				'cep2' => '3202405',
				'estado_cod' => 8,
				'cep' => 'LOC',
			),
			331 => 
			array (
				'id' => 1881,
				'nome' => 'Ibatiba',
				'uf' => 'ES',
				'cep2' => '3202454',
				'estado_cod' => 8,
				'cep' => '29395-000',
			),
			332 => 
			array (
				'id' => 1884,
				'nome' => 'Ibiraçu',
				'uf' => 'ES',
				'cep2' => '3202504',
				'estado_cod' => 8,
				'cep' => '29670-000',
			),
			333 => 
			array (
				'id' => 1885,
				'nome' => 'Ibitirama',
				'uf' => 'ES',
				'cep2' => '3202553',
				'estado_cod' => 8,
				'cep' => '29540-000',
			),
			334 => 
			array (
				'id' => 1888,
				'nome' => 'Iconha',
				'uf' => 'ES',
				'cep2' => '3202603',
				'estado_cod' => 8,
				'cep' => '29280-000',
			),
			335 => 
			array (
				'id' => 1892,
				'nome' => 'Irupi',
				'uf' => 'ES',
				'cep2' => '3202652',
				'estado_cod' => 8,
				'cep' => '29398-000',
			),
			336 => 
			array (
				'id' => 1896,
				'nome' => 'Itaguaçu',
				'uf' => 'ES',
				'cep2' => '3202702',
				'estado_cod' => 8,
				'cep' => '29690-000',
			),
			337 => 
			array (
				'id' => 1903,
				'nome' => 'Itapemirim',
				'uf' => 'ES',
				'cep2' => '3202801',
				'estado_cod' => 8,
				'cep' => '29330-000',
			),
			338 => 
			array (
				'id' => 1907,
				'nome' => 'Itarana',
				'uf' => 'ES',
				'cep2' => '3202900',
				'estado_cod' => 8,
				'cep' => '29620-000',
			),
			339 => 
			array (
				'id' => 1910,
				'nome' => 'Iúna',
				'uf' => 'ES',
				'cep2' => '3203007',
				'estado_cod' => 8,
				'cep' => '29390-000',
			),
			340 => 
			array (
				'id' => 1915,
				'nome' => 'Jaguaré',
				'uf' => 'ES',
				'cep2' => '3203056',
				'estado_cod' => 8,
				'cep' => '29950-000',
			),
			341 => 
			array (
				'id' => 1916,
				'nome' => 'Jerônimo Monteiro',
				'uf' => 'ES',
				'cep2' => '3203106',
				'estado_cod' => 8,
				'cep' => '29550-000',
			),
			342 => 
			array (
				'id' => 1918,
				'nome' => 'João Neiva',
				'uf' => 'ES',
				'cep2' => '3203130',
				'estado_cod' => 8,
				'cep' => '29680-000',
			),
			343 => 
			array (
				'id' => 1923,
				'nome' => 'Laranja da Terra',
				'uf' => 'ES',
				'cep2' => '3203163',
				'estado_cod' => 8,
				'cep' => '29615-000',
			),
			344 => 
			array (
				'id' => 1925,
				'nome' => 'Linhares',
				'uf' => 'ES',
				'cep2' => '3203205',
				'estado_cod' => 8,
				'cep' => 'LOC',
			),
			345 => 
			array (
				'id' => 1927,
				'nome' => 'Mantenópolis',
				'uf' => 'ES',
				'cep2' => '3203304',
				'estado_cod' => 8,
				'cep' => '29770-000',
			),
			346 => 
			array (
				'id' => 1928,
				'nome' => 'Marataízes',
				'uf' => 'ES',
				'cep2' => '',
				'estado_cod' => 8,
				'cep' => '29345-000',
			),
			347 => 
			array (
				'id' => 1929,
				'nome' => 'Marechal Floriano',
				'uf' => 'ES',
				'cep2' => '3203346',
				'estado_cod' => 8,
				'cep' => '29255-000',
			),
			348 => 
			array (
				'id' => 1930,
				'nome' => 'Marilândia',
				'uf' => 'ES',
				'cep2' => '3203353',
				'estado_cod' => 8,
				'cep' => '29725-000',
			),
			349 => 
			array (
				'id' => 1934,
				'nome' => 'Mimoso do Sul',
				'uf' => 'ES',
				'cep2' => '3203403',
				'estado_cod' => 8,
				'cep' => '29400-000',
			),
			350 => 
			array (
				'id' => 1935,
				'nome' => 'Montanha',
				'uf' => 'ES',
				'cep2' => '3203502',
				'estado_cod' => 8,
				'cep' => '29890-000',
			),
			351 => 
			array (
				'id' => 1939,
				'nome' => 'Mucurici',
				'uf' => 'ES',
				'cep2' => '3203601',
				'estado_cod' => 8,
				'cep' => '29880-000',
			),
			352 => 
			array (
				'id' => 1941,
				'nome' => 'Muniz Freire',
				'uf' => 'ES',
				'cep2' => '3203700',
				'estado_cod' => 8,
				'cep' => '29380-000',
			),
			353 => 
			array (
				'id' => 1942,
				'nome' => 'Muqui',
				'uf' => 'ES',
				'cep2' => '3203809',
				'estado_cod' => 8,
				'cep' => '29480-000',
			),
			354 => 
			array (
				'id' => 1946,
				'nome' => 'Nova Venécia',
				'uf' => 'ES',
				'cep2' => '3203908',
				'estado_cod' => 8,
				'cep' => '29830-000',
			),
			355 => 
			array (
				'id' => 1954,
				'nome' => 'Pancas',
				'uf' => 'ES',
				'cep2' => '3204005',
				'estado_cod' => 8,
				'cep' => '29750-000',
			),
			356 => 
			array (
				'id' => 1957,
				'nome' => 'Pedro Canário',
				'uf' => 'ES',
				'cep2' => '3204054',
				'estado_cod' => 8,
				'cep' => '29970-000',
			),
			357 => 
			array (
				'id' => 1962,
				'nome' => 'Pinheiros',
				'uf' => 'ES',
				'cep2' => '3204104',
				'estado_cod' => 8,
				'cep' => '29980-000',
			),
			358 => 
			array (
				'id' => 1964,
				'nome' => 'Piúma',
				'uf' => 'ES',
				'cep2' => '3204203',
				'estado_cod' => 8,
				'cep' => '29285-000',
			),
			359 => 
			array (
				'id' => 1966,
				'nome' => 'Ponto Belo',
				'uf' => 'ES',
				'cep2' => '3204252',
				'estado_cod' => 8,
				'cep' => '29885-000',
			),
			360 => 
			array (
				'id' => 1971,
				'nome' => 'Presidente Kennedy',
				'uf' => 'ES',
				'cep2' => '3204302',
				'estado_cod' => 8,
				'cep' => '29350-000',
			),
			361 => 
			array (
				'id' => 1978,
				'nome' => 'Rio Bananal',
				'uf' => 'ES',
				'cep2' => '3204351',
				'estado_cod' => 8,
				'cep' => '29920-000',
			),
			362 => 
			array (
				'id' => 1981,
				'nome' => 'Rio Novo do Sul',
				'uf' => 'ES',
				'cep2' => '3204401',
				'estado_cod' => 8,
				'cep' => '29290-000',
			),
			363 => 
			array (
				'id' => 1988,
				'nome' => 'Santa Leopoldina',
				'uf' => 'ES',
				'cep2' => '3204500',
				'estado_cod' => 8,
				'cep' => '29640-000',
			),
			364 => 
			array (
				'id' => 1992,
				'nome' => 'Santa Maria de Jetibá',
				'uf' => 'ES',
				'cep2' => '3204559',
				'estado_cod' => 8,
				'cep' => '29645-000',
			),
			365 => 
			array (
				'id' => 1994,
				'nome' => 'Santa Teresa',
				'uf' => 'ES',
				'cep2' => '3204609',
				'estado_cod' => 8,
				'cep' => '29650-000',
			),
			366 => 
			array (
				'id' => 2003,
				'nome' => 'São Domingos do Norte',
				'uf' => 'ES',
				'cep2' => '3204658',
				'estado_cod' => 8,
				'cep' => '29745-000',
			),
			367 => 
			array (
				'id' => 2006,
				'nome' => 'São Gabriel da Palha',
				'uf' => 'ES',
				'cep2' => '3204708',
				'estado_cod' => 8,
				'cep' => '29780-000',
			),
			368 => 
			array (
				'id' => 2017,
				'nome' => 'São José do Calçado',
				'uf' => 'ES',
				'cep2' => '3204807',
				'estado_cod' => 8,
				'cep' => '29470-000',
			),
			369 => 
			array (
				'id' => 2019,
				'nome' => 'São Mateus',
				'uf' => 'ES',
				'cep2' => '3204906',
				'estado_cod' => 8,
				'cep' => 'LOC',
			),
			370 => 
			array (
				'id' => 2024,
				'nome' => 'São Roque do Canaã',
				'uf' => 'ES',
				'cep2' => '3204955',
				'estado_cod' => 8,
				'cep' => '29665-000',
			),
			371 => 
			array (
				'id' => 2028,
				'nome' => 'Serra',
				'uf' => 'ES',
				'cep2' => '3205002',
				'estado_cod' => 8,
				'cep' => 'LOC',
			),
			372 => 
			array (
				'id' => 2031,
				'nome' => 'Sooretama',
				'uf' => 'ES',
				'cep2' => '3205010',
				'estado_cod' => 8,
				'cep' => '29927-000',
			),
			373 => 
			array (
				'id' => 2035,
				'nome' => 'Vargem Alta',
				'uf' => 'ES',
				'cep2' => '3205036',
				'estado_cod' => 8,
				'cep' => '29295-000',
			),
			374 => 
			array (
				'id' => 2037,
				'nome' => 'Venda Nova do Imigrante',
				'uf' => 'ES',
				'cep2' => '3205069',
				'estado_cod' => 8,
				'cep' => '29375-000',
			),
			375 => 
			array (
				'id' => 2038,
				'nome' => 'Viana',
				'uf' => 'ES',
				'cep2' => '3205101',
				'estado_cod' => 8,
				'cep' => '29135-000',
			),
			376 => 
			array (
				'id' => 2042,
				'nome' => 'Vila Pavão',
				'uf' => 'ES',
				'cep2' => '3205150',
				'estado_cod' => 8,
				'cep' => '29843-000',
			),
			377 => 
			array (
				'id' => 2043,
				'nome' => 'Vila Valério',
				'uf' => 'ES',
				'cep2' => '3205176',
				'estado_cod' => 8,
				'cep' => '29785-000',
			),
			378 => 
			array (
				'id' => 2044,
				'nome' => 'Vila Velha',
				'uf' => 'ES',
				'cep2' => '3205200',
				'estado_cod' => 8,
				'cep' => 'LOC',
			),
			379 => 
			array (
				'id' => 2048,
				'nome' => 'Vitória',
				'uf' => 'ES',
				'cep2' => '3205309',
				'estado_cod' => 8,
				'cep' => 'LOC',
			),
			380 => 
			array (
				'id' => 2049,
				'nome' => 'Abadia de Goiás',
				'uf' => 'GO',
				'cep2' => '5200050',
				'estado_cod' => 9,
				'cep' => '75345-000',
			),
			381 => 
			array (
				'id' => 2050,
				'nome' => 'Abadiânia',
				'uf' => 'GO',
				'cep2' => '5200100',
				'estado_cod' => 9,
				'cep' => '72940-000',
			),
			382 => 
			array (
				'id' => 2051,
				'nome' => 'Acreúna',
				'uf' => 'GO',
				'cep2' => '5200134',
				'estado_cod' => 9,
				'cep' => '75960-000',
			),
			383 => 
			array (
				'id' => 2052,
				'nome' => 'Adelândia',
				'uf' => 'GO',
				'cep2' => '5200159',
				'estado_cod' => 9,
				'cep' => '76155-000',
			),
			384 => 
			array (
				'id' => 2053,
				'nome' => 'Água Fria de Goiás',
				'uf' => 'GO',
				'cep2' => '5200175',
				'estado_cod' => 9,
				'cep' => '73780-000',
			),
			385 => 
			array (
				'id' => 2054,
				'nome' => 'Água Limpa',
				'uf' => 'GO',
				'cep2' => '5200209',
				'estado_cod' => 9,
				'cep' => '75665-000',
			),
			386 => 
			array (
				'id' => 2055,
				'nome' => 'Águas Lindas de Goiás',
				'uf' => 'GO',
				'cep2' => '5200258',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			387 => 
			array (
				'id' => 2056,
				'nome' => 'Alexânia',
				'uf' => 'GO',
				'cep2' => '5200308',
				'estado_cod' => 9,
				'cep' => '72930-000',
			),
			388 => 
			array (
				'id' => 2057,
				'nome' => 'Aloândia',
				'uf' => 'GO',
				'cep2' => '5200506',
				'estado_cod' => 9,
				'cep' => '75615-000',
			),
			389 => 
			array (
				'id' => 2059,
				'nome' => 'Alto Horizonte',
				'uf' => 'GO',
				'cep2' => '5200555',
				'estado_cod' => 9,
				'cep' => '76560-000',
			),
			390 => 
			array (
				'id' => 2060,
				'nome' => 'Alto Paraíso de Goiás',
				'uf' => 'GO',
				'cep2' => '5200605',
				'estado_cod' => 9,
				'cep' => '73770-000',
			),
			391 => 
			array (
				'id' => 2061,
				'nome' => 'Alvorada do Norte',
				'uf' => 'GO',
				'cep2' => '5200803',
				'estado_cod' => 9,
				'cep' => '73950-000',
			),
			392 => 
			array (
				'id' => 2062,
				'nome' => 'Amaralina',
				'uf' => 'GO',
				'cep2' => '5200829',
				'estado_cod' => 9,
				'cep' => '76493-000',
			),
			393 => 
			array (
				'id' => 2063,
				'nome' => 'Americano do Brasil',
				'uf' => 'GO',
				'cep2' => '5200852',
				'estado_cod' => 9,
				'cep' => '76165-000',
			),
			394 => 
			array (
				'id' => 2064,
				'nome' => 'Amorinópolis',
				'uf' => 'GO',
				'cep2' => '5200902',
				'estado_cod' => 9,
				'cep' => '76140-000',
			),
			395 => 
			array (
				'id' => 2065,
				'nome' => 'Anápolis',
				'uf' => 'GO',
				'cep2' => '5201108',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			396 => 
			array (
				'id' => 2066,
				'nome' => 'Anhangüera',
				'uf' => 'GO',
				'cep2' => '5201207',
				'estado_cod' => 9,
				'cep' => '75770-000',
			),
			397 => 
			array (
				'id' => 2067,
				'nome' => 'Anicuns',
				'uf' => 'GO',
				'cep2' => '5201306',
				'estado_cod' => 9,
				'cep' => '76170-000',
			),
			398 => 
			array (
				'id' => 2069,
				'nome' => 'Aparecida de Goiânia',
				'uf' => 'GO',
				'cep2' => '5201405',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			399 => 
			array (
				'id' => 2072,
				'nome' => 'Aparecida do Rio Doce',
				'uf' => 'GO',
				'cep2' => '5201454',
				'estado_cod' => 9,
				'cep' => '75827-000',
			),
			400 => 
			array (
				'id' => 2073,
				'nome' => 'Aporé',
				'uf' => 'GO',
				'cep2' => '5201504',
				'estado_cod' => 9,
				'cep' => '75825-000',
			),
			401 => 
			array (
				'id' => 2074,
				'nome' => 'Araçu',
				'uf' => 'GO',
				'cep2' => '5201603',
				'estado_cod' => 9,
				'cep' => '75410-000',
			),
			402 => 
			array (
				'id' => 2075,
				'nome' => 'Aragarças',
				'uf' => 'GO',
				'cep2' => '5201702',
				'estado_cod' => 9,
				'cep' => '76240-000',
			),
			403 => 
			array (
				'id' => 2076,
				'nome' => 'Aragoiânia',
				'uf' => 'GO',
				'cep2' => '5201801',
				'estado_cod' => 9,
				'cep' => '75360-000',
			),
			404 => 
			array (
				'id' => 2077,
				'nome' => 'Araguapaz',
				'uf' => 'GO',
				'cep2' => '5202155',
				'estado_cod' => 9,
				'cep' => '76720-000',
			),
			405 => 
			array (
				'id' => 2078,
				'nome' => 'Arenópolis',
				'uf' => 'GO',
				'cep2' => '5202353',
				'estado_cod' => 9,
				'cep' => '76235-000',
			),
			406 => 
			array (
				'id' => 2079,
				'nome' => 'Aruanã',
				'uf' => 'GO',
				'cep2' => '5202502',
				'estado_cod' => 9,
				'cep' => '76710-000',
			),
			407 => 
			array (
				'id' => 2080,
				'nome' => 'Aurilândia',
				'uf' => 'GO',
				'cep2' => '5202601',
				'estado_cod' => 9,
				'cep' => '76120-000',
			),
			408 => 
			array (
				'id' => 2082,
				'nome' => 'Avelinópolis',
				'uf' => 'GO',
				'cep2' => '5202809',
				'estado_cod' => 9,
				'cep' => '75395-000',
			),
			409 => 
			array (
				'id' => 2084,
				'nome' => 'Baliza',
				'uf' => 'GO',
				'cep2' => '5203104',
				'estado_cod' => 9,
				'cep' => '76250-000',
			),
			410 => 
			array (
				'id' => 2087,
				'nome' => 'Barro Alto',
				'uf' => 'GO',
				'cep2' => '5203203',
				'estado_cod' => 9,
				'cep' => '76390-000',
			),
			411 => 
			array (
				'id' => 2088,
				'nome' => 'Bela Vista de Goiás',
				'uf' => 'GO',
				'cep2' => '5203302',
				'estado_cod' => 9,
				'cep' => '75240-000',
			),
			412 => 
			array (
				'id' => 2089,
				'nome' => 'Bom Jardim de Goiás',
				'uf' => 'GO',
				'cep2' => '5203401',
				'estado_cod' => 9,
				'cep' => '76245-000',
			),
			413 => 
			array (
				'id' => 2090,
				'nome' => 'Bom Jesus',
				'uf' => 'GO',
				'cep2' => '',
				'estado_cod' => 9,
				'cep' => '75570-000',
			),
			414 => 
			array (
				'id' => 2091,
				'nome' => 'Bonfinópolis',
				'uf' => 'GO',
				'cep2' => '5203559',
				'estado_cod' => 9,
				'cep' => '75195-000',
			),
			415 => 
			array (
				'id' => 2092,
				'nome' => 'Bonópolis',
				'uf' => 'GO',
				'cep2' => '5203575',
				'estado_cod' => 9,
				'cep' => '76555-000',
			),
			416 => 
			array (
				'id' => 2093,
				'nome' => 'Brazabrantes',
				'uf' => 'GO',
				'cep2' => '5203609',
				'estado_cod' => 9,
				'cep' => '75440-000',
			),
			417 => 
			array (
				'id' => 2094,
				'nome' => 'Britânia',
				'uf' => 'GO',
				'cep2' => '5203807',
				'estado_cod' => 9,
				'cep' => '76280-000',
			),
			418 => 
			array (
				'id' => 2096,
				'nome' => 'Buriti Alegre',
				'uf' => 'GO',
				'cep2' => '5203906',
				'estado_cod' => 9,
				'cep' => '75660-000',
			),
			419 => 
			array (
				'id' => 2097,
				'nome' => 'Buriti de Goiás',
				'uf' => 'GO',
				'cep2' => '5203939',
				'estado_cod' => 9,
				'cep' => '76152-000',
			),
			420 => 
			array (
				'id' => 2098,
				'nome' => 'Buritinópolis',
				'uf' => 'GO',
				'cep2' => '5203962',
				'estado_cod' => 9,
				'cep' => '73975-000',
			),
			421 => 
			array (
				'id' => 2099,
				'nome' => 'Cabeceiras',
				'uf' => 'GO',
				'cep2' => '5204003',
				'estado_cod' => 9,
				'cep' => '73870-000',
			),
			422 => 
			array (
				'id' => 2100,
				'nome' => 'Cachoeira Alta',
				'uf' => 'GO',
				'cep2' => '5204102',
				'estado_cod' => 9,
				'cep' => '75870-000',
			),
			423 => 
			array (
				'id' => 2101,
				'nome' => 'Cachoeira de Goiás',
				'uf' => 'GO',
				'cep2' => '5204201',
				'estado_cod' => 9,
				'cep' => '76125-000',
			),
			424 => 
			array (
				'id' => 2102,
				'nome' => 'Cachoeira Dourada',
				'uf' => 'GO',
				'cep2' => '5204250',
				'estado_cod' => 9,
				'cep' => '75560-000',
			),
			425 => 
			array (
				'id' => 2103,
				'nome' => 'Caçu',
				'uf' => 'GO',
				'cep2' => '5204300',
				'estado_cod' => 9,
				'cep' => '75813-000',
			),
			426 => 
			array (
				'id' => 2104,
				'nome' => 'Caiapônia',
				'uf' => 'GO',
				'cep2' => '5204409',
				'estado_cod' => 9,
				'cep' => '75850-000',
			),
			427 => 
			array (
				'id' => 2107,
				'nome' => 'Caldas Novas',
				'uf' => 'GO',
				'cep2' => '5204508',
				'estado_cod' => 9,
				'cep' => '75690-000',
			),
			428 => 
			array (
				'id' => 2108,
				'nome' => 'Caldazinha',
				'uf' => 'GO',
				'cep2' => '5204557',
				'estado_cod' => 9,
				'cep' => '75245-000',
			),
			429 => 
			array (
				'id' => 2110,
				'nome' => 'Campestre de Goiás',
				'uf' => 'GO',
				'cep2' => '5204607',
				'estado_cod' => 9,
				'cep' => '75385-000',
			),
			430 => 
			array (
				'id' => 2111,
				'nome' => 'Campinaçu',
				'uf' => 'GO',
				'cep2' => '5204656',
				'estado_cod' => 9,
				'cep' => '76440-000',
			),
			431 => 
			array (
				'id' => 2112,
				'nome' => 'Campinorte',
				'uf' => 'GO',
				'cep2' => '5204706',
				'estado_cod' => 9,
				'cep' => '76410-000',
			),
			432 => 
			array (
				'id' => 2113,
				'nome' => 'Campo Alegre de Goiás',
				'uf' => 'GO',
				'cep2' => '5204805',
				'estado_cod' => 9,
				'cep' => '75795-000',
			),
			433 => 
			array (
				'id' => 2116,
				'nome' => 'Campos Belos',
				'uf' => 'GO',
				'cep2' => '5204904',
				'estado_cod' => 9,
				'cep' => '73840-000',
			),
			434 => 
			array (
				'id' => 2117,
				'nome' => 'Campos Verdes',
				'uf' => 'GO',
				'cep2' => '5204953',
				'estado_cod' => 9,
				'cep' => '76515-000',
			),
			435 => 
			array (
				'id' => 2122,
				'nome' => 'Carmo do Rio Verde',
				'uf' => 'GO',
				'cep2' => '5205000',
				'estado_cod' => 9,
				'cep' => '76340-000',
			),
			436 => 
			array (
				'id' => 2123,
				'nome' => 'Castelândia',
				'uf' => 'GO',
				'cep2' => '5205059',
				'estado_cod' => 9,
				'cep' => '75925-000',
			),
			437 => 
			array (
				'id' => 2125,
				'nome' => 'Catalão',
				'uf' => 'GO',
				'cep2' => '5205109',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			438 => 
			array (
				'id' => 2126,
				'nome' => 'Caturaí',
				'uf' => 'GO',
				'cep2' => '5205208',
				'estado_cod' => 9,
				'cep' => '75430-000',
			),
			439 => 
			array (
				'id' => 2127,
				'nome' => 'Cavalcante',
				'uf' => 'GO',
				'cep2' => '5205307',
				'estado_cod' => 9,
				'cep' => '73790-000',
			),
			440 => 
			array (
				'id' => 2130,
				'nome' => 'Ceres',
				'uf' => 'GO',
				'cep2' => '5205406',
				'estado_cod' => 9,
				'cep' => '76300-000',
			),
			441 => 
			array (
				'id' => 2131,
				'nome' => 'Cezarina',
				'uf' => 'GO',
				'cep2' => '5205455',
				'estado_cod' => 9,
				'cep' => '76195-000',
			),
			442 => 
			array (
				'id' => 2132,
				'nome' => 'Chapadão do Céu',
				'uf' => 'GO',
				'cep2' => '5205471',
				'estado_cod' => 9,
				'cep' => '75828-000',
			),
			443 => 
			array (
				'id' => 2135,
				'nome' => 'Cidade Ocidental',
				'uf' => 'GO',
				'cep2' => '5205497',
				'estado_cod' => 9,
				'cep' => '72880-000',
			),
			444 => 
			array (
				'id' => 2137,
				'nome' => 'Cocalzinho de Goiás',
				'uf' => 'GO',
				'cep2' => '5205513',
				'estado_cod' => 9,
				'cep' => '72975-000',
			),
			445 => 
			array (
				'id' => 2138,
				'nome' => 'Colinas do Sul',
				'uf' => 'GO',
				'cep2' => '5205521',
				'estado_cod' => 9,
				'cep' => '73740-000',
			),
			446 => 
			array (
				'id' => 2139,
				'nome' => 'Córrego do Ouro',
				'uf' => 'GO',
				'cep2' => '5205703',
				'estado_cod' => 9,
				'cep' => '76145-000',
			),
			447 => 
			array (
				'id' => 2141,
				'nome' => 'Corumbá de Goiás',
				'uf' => 'GO',
				'cep2' => '5205802',
				'estado_cod' => 9,
				'cep' => '72960-000',
			),
			448 => 
			array (
				'id' => 2142,
				'nome' => 'Corumbaíba',
				'uf' => 'GO',
				'cep2' => '5205901',
				'estado_cod' => 9,
				'cep' => '75680-000',
			),
			449 => 
			array (
				'id' => 2143,
				'nome' => 'Cristalina',
				'uf' => 'GO',
				'cep2' => '5206206',
				'estado_cod' => 9,
				'cep' => '73850-000',
			),
			450 => 
			array (
				'id' => 2144,
				'nome' => 'Cristianópolis',
				'uf' => 'GO',
				'cep2' => '5206305',
				'estado_cod' => 9,
				'cep' => '75230-000',
			),
			451 => 
			array (
				'id' => 2145,
				'nome' => 'Crixás',
				'uf' => 'GO',
				'cep2' => '5206404',
				'estado_cod' => 9,
				'cep' => '76510-000',
			),
			452 => 
			array (
				'id' => 2146,
				'nome' => 'Cromínia',
				'uf' => 'GO',
				'cep2' => '5206503',
				'estado_cod' => 9,
				'cep' => '75635-000',
			),
			453 => 
			array (
				'id' => 2148,
				'nome' => 'Cumari',
				'uf' => 'GO',
				'cep2' => '5206602',
				'estado_cod' => 9,
				'cep' => '75760-000',
			),
			454 => 
			array (
				'id' => 2149,
				'nome' => 'Damianópolis',
				'uf' => 'GO',
				'cep2' => '5206701',
				'estado_cod' => 9,
				'cep' => '73980-000',
			),
			455 => 
			array (
				'id' => 2150,
				'nome' => 'Damolândia',
				'uf' => 'GO',
				'cep2' => '5206800',
				'estado_cod' => 9,
				'cep' => '75420-000',
			),
			456 => 
			array (
				'id' => 2152,
				'nome' => 'Davinópolis',
				'uf' => 'GO',
				'cep2' => '5206909',
				'estado_cod' => 9,
				'cep' => '75730-000',
			),
			457 => 
			array (
				'id' => 2154,
				'nome' => 'Diorama',
				'uf' => 'GO',
				'cep2' => '5207105',
				'estado_cod' => 9,
				'cep' => '76260-000',
			),
			458 => 
			array (
				'id' => 2155,
				'nome' => 'Divinópolis de Goiás',
				'uf' => 'GO',
				'cep2' => '5208301',
				'estado_cod' => 9,
				'cep' => '73865-000',
			),
			459 => 
			array (
				'id' => 2157,
				'nome' => 'Doverlândia',
				'uf' => 'GO',
				'cep2' => '5207253',
				'estado_cod' => 9,
				'cep' => '75855-000',
			),
			460 => 
			array (
				'id' => 2158,
				'nome' => 'Edealina',
				'uf' => 'GO',
				'cep2' => '5207352',
				'estado_cod' => 9,
				'cep' => '75945-000',
			),
			461 => 
			array (
				'id' => 2159,
				'nome' => 'Edéia',
				'uf' => 'GO',
				'cep2' => '5207402',
				'estado_cod' => 9,
				'cep' => '75940-000',
			),
			462 => 
			array (
				'id' => 2160,
				'nome' => 'Estrela do Norte',
				'uf' => 'GO',
				'cep2' => '5207501',
				'estado_cod' => 9,
				'cep' => '76485-000',
			),
			463 => 
			array (
				'id' => 2161,
				'nome' => 'Faina',
				'uf' => 'GO',
				'cep2' => '5207535',
				'estado_cod' => 9,
				'cep' => '76740-000',
			),
			464 => 
			array (
				'id' => 2162,
				'nome' => 'Fazenda Nova',
				'uf' => 'GO',
				'cep2' => '5207600',
				'estado_cod' => 9,
				'cep' => '76220-000',
			),
			465 => 
			array (
				'id' => 2163,
				'nome' => 'Firminópolis',
				'uf' => 'GO',
				'cep2' => '5207808',
				'estado_cod' => 9,
				'cep' => '76105-000',
			),
			466 => 
			array (
				'id' => 2164,
				'nome' => 'Flores de Goiás',
				'uf' => 'GO',
				'cep2' => '5207907',
				'estado_cod' => 9,
				'cep' => '73890-000',
			),
			467 => 
			array (
				'id' => 2165,
				'nome' => 'Formosa',
				'uf' => 'GO',
				'cep2' => '5208004',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			468 => 
			array (
				'id' => 2166,
				'nome' => 'Formoso',
				'uf' => 'GO',
				'cep2' => '5208103',
				'estado_cod' => 9,
				'cep' => '76470-000',
			),
			469 => 
			array (
				'id' => 2168,
				'nome' => 'Gameleira de Goiás',
				'uf' => 'GO',
				'cep2' => '5208152',
				'estado_cod' => 9,
				'cep' => '75184-000',
			),
			470 => 
			array (
				'id' => 2171,
				'nome' => 'Goianápolis',
				'uf' => 'GO',
				'cep2' => '5208400',
				'estado_cod' => 9,
				'cep' => '75170-000',
			),
			471 => 
			array (
				'id' => 2172,
				'nome' => 'Goiandira',
				'uf' => 'GO',
				'cep2' => '5208509',
				'estado_cod' => 9,
				'cep' => '75740-000',
			),
			472 => 
			array (
				'id' => 2173,
				'nome' => 'Goianésia',
				'uf' => 'GO',
				'cep2' => '5208608',
				'estado_cod' => 9,
				'cep' => '76380-000',
			),
			473 => 
			array (
				'id' => 2174,
				'nome' => 'Goiânia',
				'uf' => 'GO',
				'cep2' => '5208707',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			474 => 
			array (
				'id' => 2175,
				'nome' => 'Goianira',
				'uf' => 'GO',
				'cep2' => '5208806',
				'estado_cod' => 9,
				'cep' => '75370-000',
			),
			475 => 
			array (
				'id' => 2176,
				'nome' => 'Goiás',
				'uf' => 'GO',
				'cep2' => '5208905',
				'estado_cod' => 9,
				'cep' => '76600-000',
			),
			476 => 
			array (
				'id' => 2177,
				'nome' => 'Goiatuba',
				'uf' => 'GO',
				'cep2' => '5209101',
				'estado_cod' => 9,
				'cep' => '75600-000',
			),
			477 => 
			array (
				'id' => 2178,
				'nome' => 'Gouvelândia',
				'uf' => 'GO',
				'cep2' => '5209150',
				'estado_cod' => 9,
				'cep' => '75865-000',
			),
			478 => 
			array (
				'id' => 2179,
				'nome' => 'Guapó',
				'uf' => 'GO',
				'cep2' => '5209200',
				'estado_cod' => 9,
				'cep' => '75350-000',
			),
			479 => 
			array (
				'id' => 2180,
				'nome' => 'Guaraíta',
				'uf' => 'GO',
				'cep2' => '5209291',
				'estado_cod' => 9,
				'cep' => '76690-000',
			),
			480 => 
			array (
				'id' => 2181,
				'nome' => 'Guarani de Goiás',
				'uf' => 'GO',
				'cep2' => '5209408',
				'estado_cod' => 9,
				'cep' => '73910-000',
			),
			481 => 
			array (
				'id' => 2182,
				'nome' => 'Guarinos',
				'uf' => 'GO',
				'cep2' => '5209457',
				'estado_cod' => 9,
				'cep' => '76385-000',
			),
			482 => 
			array (
				'id' => 2183,
				'nome' => 'Heitoraí',
				'uf' => 'GO',
				'cep2' => '5209606',
				'estado_cod' => 9,
				'cep' => '76670-000',
			),
			483 => 
			array (
				'id' => 2184,
				'nome' => 'Hidrolândia',
				'uf' => 'GO',
				'cep2' => '5209705',
				'estado_cod' => 9,
				'cep' => '75340-000',
			),
			484 => 
			array (
				'id' => 2185,
				'nome' => 'Hidrolina',
				'uf' => 'GO',
				'cep2' => '5209804',
				'estado_cod' => 9,
				'cep' => '76375-000',
			),
			485 => 
			array (
				'id' => 2186,
				'nome' => 'Iaciara',
				'uf' => 'GO',
				'cep2' => '5209903',
				'estado_cod' => 9,
				'cep' => '73920-000',
			),
			486 => 
			array (
				'id' => 2187,
				'nome' => 'Inaciolândia',
				'uf' => 'GO',
				'cep2' => '5209937',
				'estado_cod' => 9,
				'cep' => '75550-000',
			),
			487 => 
			array (
				'id' => 2188,
				'nome' => 'Indiara',
				'uf' => 'GO',
				'cep2' => '5209952',
				'estado_cod' => 9,
				'cep' => '75955-000',
			),
			488 => 
			array (
				'id' => 2189,
				'nome' => 'Inhumas',
				'uf' => 'GO',
				'cep2' => '5210000',
				'estado_cod' => 9,
				'cep' => '75400-000',
			),
			489 => 
			array (
				'id' => 2191,
				'nome' => 'Ipameri',
				'uf' => 'GO',
				'cep2' => '5210109',
				'estado_cod' => 9,
				'cep' => '75780-000',
			),
			490 => 
			array (
				'id' => 2192,
				'nome' => 'Iporá',
				'uf' => 'GO',
				'cep2' => '5210208',
				'estado_cod' => 9,
				'cep' => '76200-000',
			),
			491 => 
			array (
				'id' => 2193,
				'nome' => 'Israelândia',
				'uf' => 'GO',
				'cep2' => '5210307',
				'estado_cod' => 9,
				'cep' => '76205-000',
			),
			492 => 
			array (
				'id' => 2194,
				'nome' => 'Itaberaí',
				'uf' => 'GO',
				'cep2' => '5210406',
				'estado_cod' => 9,
				'cep' => '76630-000',
			),
			493 => 
			array (
				'id' => 2196,
				'nome' => 'Itaguari',
				'uf' => 'GO',
				'cep2' => '5210562',
				'estado_cod' => 9,
				'cep' => '76650-000',
			),
			494 => 
			array (
				'id' => 2197,
				'nome' => 'Itaguaru',
				'uf' => 'GO',
				'cep2' => '5210604',
				'estado_cod' => 9,
				'cep' => '76660-000',
			),
			495 => 
			array (
				'id' => 2198,
				'nome' => 'Itajá',
				'uf' => 'GO',
				'cep2' => '5210802',
				'estado_cod' => 9,
				'cep' => '75815-000',
			),
			496 => 
			array (
				'id' => 2199,
				'nome' => 'Itapaci',
				'uf' => 'GO',
				'cep2' => '5210901',
				'estado_cod' => 9,
				'cep' => '76360-000',
			),
			497 => 
			array (
				'id' => 2200,
				'nome' => 'Itapirapuã',
				'uf' => 'GO',
				'cep2' => '5211008',
				'estado_cod' => 9,
				'cep' => '76290-000',
			),
			498 => 
			array (
				'id' => 2201,
				'nome' => 'Itapuranga',
				'uf' => 'GO',
				'cep2' => '5211206',
				'estado_cod' => 9,
				'cep' => '76680-000',
			),
			499 => 
			array (
				'id' => 2202,
				'nome' => 'Itarumã',
				'uf' => 'GO',
				'cep2' => '',
				'estado_cod' => 9,
				'cep' => '75810-000',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 2203,
				'nome' => 'Itauçu',
				'uf' => 'GO',
				'cep2' => '5211404',
				'estado_cod' => 9,
				'cep' => '75450-000',
			),
			1 => 
			array (
				'id' => 2204,
				'nome' => 'Itumbiara',
				'uf' => 'GO',
				'cep2' => '5211503',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			2 => 
			array (
				'id' => 2205,
				'nome' => 'Ivolândia',
				'uf' => 'GO',
				'cep2' => '5211602',
				'estado_cod' => 9,
				'cep' => '76130-000',
			),
			3 => 
			array (
				'id' => 2207,
				'nome' => 'Jandaia',
				'uf' => 'GO',
				'cep2' => '5211701',
				'estado_cod' => 9,
				'cep' => '75950-000',
			),
			4 => 
			array (
				'id' => 2208,
				'nome' => 'Jaraguá',
				'uf' => 'GO',
				'cep2' => '5211800',
				'estado_cod' => 9,
				'cep' => '76330-000',
			),
			5 => 
			array (
				'id' => 2209,
				'nome' => 'Jataí',
				'uf' => 'GO',
				'cep2' => '5211909',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			6 => 
			array (
				'id' => 2210,
				'nome' => 'Jaupaci',
				'uf' => 'GO',
				'cep2' => '5212006',
				'estado_cod' => 9,
				'cep' => '76210-000',
			),
			7 => 
			array (
				'id' => 2212,
				'nome' => 'Jesúpolis',
				'uf' => 'GO',
				'cep2' => '5212055',
				'estado_cod' => 9,
				'cep' => '75495-000',
			),
			8 => 
			array (
				'id' => 2214,
				'nome' => 'Joviânia',
				'uf' => 'GO',
				'cep2' => '5212105',
				'estado_cod' => 9,
				'cep' => '75610-000',
			),
			9 => 
			array (
				'id' => 2217,
				'nome' => 'Jussara',
				'uf' => 'GO',
				'cep2' => '5212204',
				'estado_cod' => 9,
				'cep' => '76270-000',
			),
			10 => 
			array (
				'id' => 2220,
				'nome' => 'Leopoldo de Bulhões',
				'uf' => 'GO',
				'cep2' => '5212303',
				'estado_cod' => 9,
				'cep' => '75190-000',
			),
			11 => 
			array (
				'id' => 2222,
				'nome' => 'Luziânia',
				'uf' => 'GO',
				'cep2' => '5212501',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			12 => 
			array (
				'id' => 2223,
				'nome' => 'Mairipotaba',
				'uf' => 'GO',
				'cep2' => '5212600',
				'estado_cod' => 9,
				'cep' => '75630-000',
			),
			13 => 
			array (
				'id' => 2224,
				'nome' => 'Mambaí',
				'uf' => 'GO',
				'cep2' => '5212709',
				'estado_cod' => 9,
				'cep' => '73970-000',
			),
			14 => 
			array (
				'id' => 2225,
				'nome' => 'Mara Rosa',
				'uf' => 'GO',
				'cep2' => '5212808',
				'estado_cod' => 9,
				'cep' => '76490-000',
			),
			15 => 
			array (
				'id' => 2227,
				'nome' => 'Marzagão',
				'uf' => 'GO',
				'cep2' => '5212907',
				'estado_cod' => 9,
				'cep' => '75670-000',
			),
			16 => 
			array (
				'id' => 2228,
				'nome' => 'Matrinchã',
				'uf' => 'GO',
				'cep2' => '5212956',
				'estado_cod' => 9,
				'cep' => '76730-000',
			),
			17 => 
			array (
				'id' => 2229,
				'nome' => 'Maurilândia',
				'uf' => 'GO',
				'cep2' => '5213004',
				'estado_cod' => 9,
				'cep' => '75930-000',
			),
			18 => 
			array (
				'id' => 2232,
				'nome' => 'Mimoso de Goiás',
				'uf' => 'GO',
				'cep2' => '5213053',
				'estado_cod' => 9,
				'cep' => '73730-000',
			),
			19 => 
			array (
				'id' => 2233,
				'nome' => 'Minaçu',
				'uf' => 'GO',
				'cep2' => '5213087',
				'estado_cod' => 9,
				'cep' => '76450-000',
			),
			20 => 
			array (
				'id' => 2234,
				'nome' => 'Mineiros',
				'uf' => 'GO',
				'cep2' => '5213103',
				'estado_cod' => 9,
				'cep' => '75830-000',
			),
			21 => 
			array (
				'id' => 2235,
				'nome' => 'Moiporá',
				'uf' => 'GO',
				'cep2' => '5213400',
				'estado_cod' => 9,
				'cep' => '76135-000',
			),
			22 => 
			array (
				'id' => 2236,
				'nome' => 'Monte Alegre de Goiás',
				'uf' => 'GO',
				'cep2' => '5213509',
				'estado_cod' => 9,
				'cep' => '73830-000',
			),
			23 => 
			array (
				'id' => 2237,
				'nome' => 'Montes Claros de Goiás',
				'uf' => 'GO',
				'cep2' => '5213707',
				'estado_cod' => 9,
				'cep' => '76255-000',
			),
			24 => 
			array (
				'id' => 2238,
				'nome' => 'Montividiu',
				'uf' => 'GO',
				'cep2' => '5213756',
				'estado_cod' => 9,
				'cep' => '75915-000',
			),
			25 => 
			array (
				'id' => 2239,
				'nome' => 'Montividiu do Norte',
				'uf' => 'GO',
				'cep2' => '5213772',
				'estado_cod' => 9,
				'cep' => '76465-000',
			),
			26 => 
			array (
				'id' => 2240,
				'nome' => 'Morrinhos',
				'uf' => 'GO',
				'cep2' => '5213806',
				'estado_cod' => 9,
				'cep' => '75650-000',
			),
			27 => 
			array (
				'id' => 2241,
				'nome' => 'Morro Agudo de Goiás',
				'uf' => 'GO',
				'cep2' => '5213855',
				'estado_cod' => 9,
				'cep' => '76355-000',
			),
			28 => 
			array (
				'id' => 2242,
				'nome' => 'Mossâmedes',
				'uf' => 'GO',
				'cep2' => '5213905',
				'estado_cod' => 9,
				'cep' => '76150-000',
			),
			29 => 
			array (
				'id' => 2243,
				'nome' => 'Mozarlândia',
				'uf' => 'GO',
				'cep2' => '5214002',
				'estado_cod' => 9,
				'cep' => '76700-000',
			),
			30 => 
			array (
				'id' => 2244,
				'nome' => 'Mundo Novo',
				'uf' => 'GO',
				'cep2' => '5214051',
				'estado_cod' => 9,
				'cep' => '76530-000',
			),
			31 => 
			array (
				'id' => 2245,
				'nome' => 'Mutunópolis',
				'uf' => 'GO',
				'cep2' => '5214101',
				'estado_cod' => 9,
				'cep' => '76540-000',
			),
			32 => 
			array (
				'id' => 2247,
				'nome' => 'Nazário',
				'uf' => 'GO',
				'cep2' => '5214408',
				'estado_cod' => 9,
				'cep' => '76180-000',
			),
			33 => 
			array (
				'id' => 2248,
				'nome' => 'Nerópolis',
				'uf' => 'GO',
				'cep2' => '5214507',
				'estado_cod' => 9,
				'cep' => '75460-000',
			),
			34 => 
			array (
				'id' => 2249,
				'nome' => 'Niquelândia',
				'uf' => 'GO',
				'cep2' => '5214606',
				'estado_cod' => 9,
				'cep' => '76420-000',
			),
			35 => 
			array (
				'id' => 2250,
				'nome' => 'Nova América',
				'uf' => 'GO',
				'cep2' => '5214705',
				'estado_cod' => 9,
				'cep' => '76345-000',
			),
			36 => 
			array (
				'id' => 2251,
				'nome' => 'Nova Aurora',
				'uf' => 'GO',
				'cep2' => '5214804',
				'estado_cod' => 9,
				'cep' => '75750-000',
			),
			37 => 
			array (
				'id' => 2252,
				'nome' => 'Nova Crixás',
				'uf' => 'GO',
				'cep2' => '5214838',
				'estado_cod' => 9,
				'cep' => '76520-000',
			),
			38 => 
			array (
				'id' => 2253,
				'nome' => 'Nova Glória',
				'uf' => 'GO',
				'cep2' => '5214861',
				'estado_cod' => 9,
				'cep' => '76305-000',
			),
			39 => 
			array (
				'id' => 2254,
				'nome' => 'Nova Iguaçu de Goiás',
				'uf' => 'GO',
				'cep2' => '5214879',
				'estado_cod' => 9,
				'cep' => '76495-000',
			),
			40 => 
			array (
				'id' => 2255,
				'nome' => 'Nova Roma',
				'uf' => 'GO',
				'cep2' => '5214903',
				'estado_cod' => 9,
				'cep' => '73820-000',
			),
			41 => 
			array (
				'id' => 2256,
				'nome' => 'Nova Veneza',
				'uf' => 'GO',
				'cep2' => '5215009',
				'estado_cod' => 9,
				'cep' => '75470-000',
			),
			42 => 
			array (
				'id' => 2257,
				'nome' => 'Novo Brasil',
				'uf' => 'GO',
				'cep2' => '5215207',
				'estado_cod' => 9,
				'cep' => '76285-000',
			),
			43 => 
			array (
				'id' => 2258,
				'nome' => 'Novo Gama',
				'uf' => 'GO',
				'cep2' => '5215231',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			44 => 
			array (
				'id' => 2259,
				'nome' => 'Novo Planalto',
				'uf' => 'GO',
				'cep2' => '5215256',
				'estado_cod' => 9,
				'cep' => '76580-000',
			),
			45 => 
			array (
				'id' => 2262,
				'nome' => 'Orizona',
				'uf' => 'GO',
				'cep2' => '5215306',
				'estado_cod' => 9,
				'cep' => '75280-000',
			),
			46 => 
			array (
				'id' => 2263,
				'nome' => 'Ouro Verde de Goiás',
				'uf' => 'GO',
				'cep2' => '5215405',
				'estado_cod' => 9,
				'cep' => '75165-000',
			),
			47 => 
			array (
				'id' => 2265,
				'nome' => 'Ouvidor',
				'uf' => 'GO',
				'cep2' => '5215504',
				'estado_cod' => 9,
				'cep' => '75715-000',
			),
			48 => 
			array (
				'id' => 2266,
				'nome' => 'Padre Bernardo',
				'uf' => 'GO',
				'cep2' => '5215603',
				'estado_cod' => 9,
				'cep' => '73700-000',
			),
			49 => 
			array (
				'id' => 2267,
				'nome' => 'Palestina de Goiás',
				'uf' => 'GO',
				'cep2' => '5215652',
				'estado_cod' => 9,
				'cep' => '75845-000',
			),
			50 => 
			array (
				'id' => 2268,
				'nome' => 'Palmeiras de Goiás',
				'uf' => 'GO',
				'cep2' => '5215702',
				'estado_cod' => 9,
				'cep' => '76190-000',
			),
			51 => 
			array (
				'id' => 2269,
				'nome' => 'Palmelo',
				'uf' => 'GO',
				'cep2' => '5215801',
				'estado_cod' => 9,
				'cep' => '75210-000',
			),
			52 => 
			array (
				'id' => 2270,
				'nome' => 'Palminópolis',
				'uf' => 'GO',
				'cep2' => '5215900',
				'estado_cod' => 9,
				'cep' => '75990-000',
			),
			53 => 
			array (
				'id' => 2271,
				'nome' => 'Panamá',
				'uf' => 'GO',
				'cep2' => '5216007',
				'estado_cod' => 9,
				'cep' => '75580-000',
			),
			54 => 
			array (
				'id' => 2272,
				'nome' => 'Paranaiguara',
				'uf' => 'GO',
				'cep2' => '5216304',
				'estado_cod' => 9,
				'cep' => '75880-000',
			),
			55 => 
			array (
				'id' => 2273,
				'nome' => 'Paraúna',
				'uf' => 'GO',
				'cep2' => '5216403',
				'estado_cod' => 9,
				'cep' => '75980-000',
			),
			56 => 
			array (
				'id' => 2276,
				'nome' => 'Perolândia',
				'uf' => 'GO',
				'cep2' => '5216452',
				'estado_cod' => 9,
				'cep' => '75823-000',
			),
			57 => 
			array (
				'id' => 2277,
				'nome' => 'Petrolina de Goiás',
				'uf' => 'GO',
				'cep2' => '5216809',
				'estado_cod' => 9,
				'cep' => '75480-000',
			),
			58 => 
			array (
				'id' => 2278,
				'nome' => 'Pilar de Goiás',
				'uf' => 'GO',
				'cep2' => '5216908',
				'estado_cod' => 9,
				'cep' => '76370-000',
			),
			59 => 
			array (
				'id' => 2280,
				'nome' => 'Piracanjuba',
				'uf' => 'GO',
				'cep2' => '5217104',
				'estado_cod' => 9,
				'cep' => '75640-000',
			),
			60 => 
			array (
				'id' => 2281,
				'nome' => 'Piranhas',
				'uf' => 'GO',
				'cep2' => '5217203',
				'estado_cod' => 9,
				'cep' => '76230-000',
			),
			61 => 
			array (
				'id' => 2282,
				'nome' => 'Pirenópolis',
				'uf' => 'GO',
				'cep2' => '5217302',
				'estado_cod' => 9,
				'cep' => '72980-000',
			),
			62 => 
			array (
				'id' => 2284,
				'nome' => 'Pires do Rio',
				'uf' => 'GO',
				'cep2' => '5217401',
				'estado_cod' => 9,
				'cep' => '75200-000',
			),
			63 => 
			array (
				'id' => 2285,
				'nome' => 'Planaltina de Goiás',
				'uf' => 'GO',
				'cep2' => '',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			64 => 
			array (
				'id' => 2286,
				'nome' => 'Pontalina',
				'uf' => 'GO',
				'cep2' => '5217708',
				'estado_cod' => 9,
				'cep' => '75620-000',
			),
			65 => 
			array (
				'id' => 2287,
				'nome' => 'Porangatu',
				'uf' => 'GO',
				'cep2' => '5218003',
				'estado_cod' => 9,
				'cep' => '76550-000',
			),
			66 => 
			array (
				'id' => 2288,
				'nome' => 'Porteirão',
				'uf' => 'GO',
				'cep2' => '5218052',
				'estado_cod' => 9,
				'cep' => '75603-000',
			),
			67 => 
			array (
				'id' => 2289,
				'nome' => 'Portelândia',
				'uf' => 'GO',
				'cep2' => '5218102',
				'estado_cod' => 9,
				'cep' => '75835-000',
			),
			68 => 
			array (
				'id' => 2290,
				'nome' => 'Posse',
				'uf' => 'GO',
				'cep2' => '5218300',
				'estado_cod' => 9,
				'cep' => '73900-000',
			),
			69 => 
			array (
				'id' => 2292,
				'nome' => 'Professor Jamil',
				'uf' => 'GO',
				'cep2' => '5218391',
				'estado_cod' => 9,
				'cep' => '75645-000',
			),
			70 => 
			array (
				'id' => 2293,
				'nome' => 'Quirinópolis',
				'uf' => 'GO',
				'cep2' => '5218508',
				'estado_cod' => 9,
				'cep' => '75860-000',
			),
			71 => 
			array (
				'id' => 2295,
				'nome' => 'Rianápolis',
				'uf' => 'GO',
				'cep2' => '5218706',
				'estado_cod' => 9,
				'cep' => '76315-000',
			),
			72 => 
			array (
				'id' => 2297,
				'nome' => 'Rio Quente',
				'uf' => 'GO',
				'cep2' => '5218789',
				'estado_cod' => 9,
				'cep' => '75695-000',
			),
			73 => 
			array (
				'id' => 2298,
				'nome' => 'Rio Verde',
				'uf' => 'GO',
				'cep2' => '5218805',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			74 => 
			array (
				'id' => 2300,
				'nome' => 'Campo Limpo de Goiás',
				'uf' => 'GO',
				'cep2' => '5204854',
				'estado_cod' => 9,
				'cep' => '75160-000',
			),
			75 => 
			array (
				'id' => 2302,
				'nome' => 'Rubiataba',
				'uf' => 'GO',
				'cep2' => '5218904',
				'estado_cod' => 9,
				'cep' => '76350-000',
			),
			76 => 
			array (
				'id' => 2303,
				'nome' => 'Sanclerlândia',
				'uf' => 'GO',
				'cep2' => '5219001',
				'estado_cod' => 9,
				'cep' => '76160-000',
			),
			77 => 
			array (
				'id' => 2304,
				'nome' => 'Santa Bárbara de Goiás',
				'uf' => 'GO',
				'cep2' => '5219100',
				'estado_cod' => 9,
				'cep' => '75390-000',
			),
			78 => 
			array (
				'id' => 2306,
				'nome' => 'Santa Cruz de Goiás',
				'uf' => 'GO',
				'cep2' => '5219209',
				'estado_cod' => 9,
				'cep' => '75220-000',
			),
			79 => 
			array (
				'id' => 2307,
				'nome' => 'Santa Fé de Goiás',
				'uf' => 'GO',
				'cep2' => '5219258',
				'estado_cod' => 9,
				'cep' => '76265-000',
			),
			80 => 
			array (
				'id' => 2308,
				'nome' => 'Santa Helena de Goiás',
				'uf' => 'GO',
				'cep2' => '5219308',
				'estado_cod' => 9,
				'cep' => '75920-000',
			),
			81 => 
			array (
				'id' => 2309,
				'nome' => 'Santa Isabel',
				'uf' => 'GO',
				'cep2' => '5219357',
				'estado_cod' => 9,
				'cep' => '76320-000',
			),
			82 => 
			array (
				'id' => 2310,
				'nome' => 'Santa Rita do Araguaia',
				'uf' => 'GO',
				'cep2' => '5219407',
				'estado_cod' => 9,
				'cep' => '75840-000',
			),
			83 => 
			array (
				'id' => 2311,
				'nome' => 'Santa Rita do Novo Destino',
				'uf' => 'GO',
				'cep2' => '5219456',
				'estado_cod' => 9,
				'cep' => '76395-000',
			),
			84 => 
			array (
				'id' => 2313,
				'nome' => 'Santa Rosa de Goiás',
				'uf' => 'GO',
				'cep2' => '5219506',
				'estado_cod' => 9,
				'cep' => '75455-000',
			),
			85 => 
			array (
				'id' => 2314,
				'nome' => 'Santa Tereza de Goiás',
				'uf' => 'GO',
				'cep2' => '5219605',
				'estado_cod' => 9,
				'cep' => '76480-000',
			),
			86 => 
			array (
				'id' => 2315,
				'nome' => 'Santa Terezinha de Goiás',
				'uf' => 'GO',
				'cep2' => '5219704',
				'estado_cod' => 9,
				'cep' => '76500-000',
			),
			87 => 
			array (
				'id' => 2316,
				'nome' => 'Santo Antônio da Barra',
				'uf' => 'GO',
				'cep2' => '5219712',
				'estado_cod' => 9,
				'cep' => '75935-000',
			),
			88 => 
			array (
				'id' => 2317,
				'nome' => 'Santo Antônio de Goiás',
				'uf' => 'GO',
				'cep2' => '5219738',
				'estado_cod' => 9,
				'cep' => '75375-000',
			),
			89 => 
			array (
				'id' => 2318,
				'nome' => 'Santo Antônio do Descoberto',
				'uf' => 'GO',
				'cep2' => '5219753',
				'estado_cod' => 9,
				'cep' => '72900-000',
			),
			90 => 
			array (
				'id' => 2320,
				'nome' => 'São Domingos',
				'uf' => 'GO',
				'cep2' => '5219803',
				'estado_cod' => 9,
				'cep' => '73860-000',
			),
			91 => 
			array (
				'id' => 2321,
				'nome' => 'São Francisco de Goiás',
				'uf' => 'GO',
				'cep2' => '5219902',
				'estado_cod' => 9,
				'cep' => '75490-000',
			),
			92 => 
			array (
				'id' => 2324,
				'nome' => 'São João D\'Aliança',
				'uf' => 'GO',
				'cep2' => '5220009',
				'estado_cod' => 9,
				'cep' => '73760-000',
			),
			93 => 
			array (
				'id' => 2325,
				'nome' => 'São João da Paraúna',
				'uf' => 'GO',
				'cep2' => '5220058',
				'estado_cod' => 9,
				'cep' => '75985-000',
			),
			94 => 
			array (
				'id' => 2326,
				'nome' => 'São Luís de Montes Belos',
				'uf' => 'GO',
				'cep2' => '5220108',
				'estado_cod' => 9,
				'cep' => '76100-000',
			),
			95 => 
			array (
				'id' => 2327,
				'nome' => 'São Luiz do Norte',
				'uf' => 'GO',
				'cep2' => '5220157',
				'estado_cod' => 9,
				'cep' => '76365-000',
			),
			96 => 
			array (
				'id' => 2329,
				'nome' => 'São Miguel do Araguaia',
				'uf' => 'GO',
				'cep2' => '5220207',
				'estado_cod' => 9,
				'cep' => '76590-000',
			),
			97 => 
			array (
				'id' => 2330,
				'nome' => 'São Miguel do Passa Quatro',
				'uf' => 'GO',
				'cep2' => '5220264',
				'estado_cod' => 9,
				'cep' => '75185-000',
			),
			98 => 
			array (
				'id' => 2331,
				'nome' => 'São Patrício',
				'uf' => 'GO',
				'cep2' => '5220280',
				'estado_cod' => 9,
				'cep' => '76343-000',
			),
			99 => 
			array (
				'id' => 2333,
				'nome' => 'São Simão',
				'uf' => 'GO',
				'cep2' => '5220405',
				'estado_cod' => 9,
				'cep' => '75890-000',
			),
			100 => 
			array (
				'id' => 2336,
				'nome' => 'Senador Canedo',
				'uf' => 'GO',
				'cep2' => '5220454',
				'estado_cod' => 9,
				'cep' => '75250-000',
			),
			101 => 
			array (
				'id' => 2338,
				'nome' => 'Serranópolis',
				'uf' => 'GO',
				'cep2' => '5220504',
				'estado_cod' => 9,
				'cep' => '75820-000',
			),
			102 => 
			array (
				'id' => 2339,
				'nome' => 'Silvânia',
				'uf' => 'GO',
				'cep2' => '5220603',
				'estado_cod' => 9,
				'cep' => '75180-000',
			),
			103 => 
			array (
				'id' => 2340,
				'nome' => 'Simolândia',
				'uf' => 'GO',
				'cep2' => '5220686',
				'estado_cod' => 9,
				'cep' => '73930-000',
			),
			104 => 
			array (
				'id' => 2341,
				'nome' => 'Sítio D\'Abadia',
				'uf' => 'GO',
				'cep2' => '5220702',
				'estado_cod' => 9,
				'cep' => '73990-000',
			),
			105 => 
			array (
				'id' => 2343,
				'nome' => 'Taquaral de Goiás',
				'uf' => 'GO',
				'cep2' => '5221007',
				'estado_cod' => 9,
				'cep' => '76640-000',
			),
			106 => 
			array (
				'id' => 2345,
				'nome' => 'Teresina de Goiás',
				'uf' => 'GO',
				'cep2' => '5221080',
				'estado_cod' => 9,
				'cep' => '73795-000',
			),
			107 => 
			array (
				'id' => 2346,
				'nome' => 'Terezópolis de Goiás',
				'uf' => 'GO',
				'cep2' => '5221197',
				'estado_cod' => 9,
				'cep' => '75175-000',
			),
			108 => 
			array (
				'id' => 2347,
				'nome' => 'Lagoa Santa',
				'uf' => 'GO',
				'cep2' => '5212253',
				'estado_cod' => 9,
				'cep' => '75819-000',
			),
			109 => 
			array (
				'id' => 2348,
				'nome' => 'Três Ranchos',
				'uf' => 'GO',
				'cep2' => '5221304',
				'estado_cod' => 9,
				'cep' => '75720-000',
			),
			110 => 
			array (
				'id' => 2349,
				'nome' => 'Trindade',
				'uf' => 'GO',
				'cep2' => '5221403',
				'estado_cod' => 9,
				'cep' => '75380-000',
			),
			111 => 
			array (
				'id' => 2350,
				'nome' => 'Trombas',
				'uf' => 'GO',
				'cep2' => '5221452',
				'estado_cod' => 9,
				'cep' => '76460-000',
			),
			112 => 
			array (
				'id' => 2352,
				'nome' => 'Turvânia',
				'uf' => 'GO',
				'cep2' => '5221502',
				'estado_cod' => 9,
				'cep' => '76110-000',
			),
			113 => 
			array (
				'id' => 2353,
				'nome' => 'Turvelândia',
				'uf' => 'GO',
				'cep2' => '5221551',
				'estado_cod' => 9,
				'cep' => '75970-000',
			),
			114 => 
			array (
				'id' => 2354,
				'nome' => 'Uirapuru',
				'uf' => 'GO',
				'cep2' => '5221577',
				'estado_cod' => 9,
				'cep' => '76525-000',
			),
			115 => 
			array (
				'id' => 2355,
				'nome' => 'Uruaçu',
				'uf' => 'GO',
				'cep2' => '5221601',
				'estado_cod' => 9,
				'cep' => '76400-000',
			),
			116 => 
			array (
				'id' => 2356,
				'nome' => 'Uruana',
				'uf' => 'GO',
				'cep2' => '5221700',
				'estado_cod' => 9,
				'cep' => '76335-000',
			),
			117 => 
			array (
				'id' => 2358,
				'nome' => 'Urutaí',
				'uf' => 'GO',
				'cep2' => '5221809',
				'estado_cod' => 9,
				'cep' => '75790-000',
			),
			118 => 
			array (
				'id' => 2361,
				'nome' => 'Valparaíso de Goiás',
				'uf' => 'GO',
				'cep2' => '5221858',
				'estado_cod' => 9,
				'cep' => 'LOC',
			),
			119 => 
			array (
				'id' => 2362,
				'nome' => 'Varjão',
				'uf' => 'GO',
				'cep2' => '5221908',
				'estado_cod' => 9,
				'cep' => '75355-000',
			),
			120 => 
			array (
				'id' => 2363,
				'nome' => 'Vianópolis',
				'uf' => 'GO',
				'cep2' => '5222005',
				'estado_cod' => 9,
				'cep' => '75260-000',
			),
			121 => 
			array (
				'id' => 2364,
				'nome' => 'Vicentinópolis',
				'uf' => 'GO',
				'cep2' => '5222054',
				'estado_cod' => 9,
				'cep' => '75555-000',
			),
			122 => 
			array (
				'id' => 2365,
				'nome' => 'Vila Boa',
				'uf' => 'GO',
				'cep2' => '5222203',
				'estado_cod' => 9,
				'cep' => '73825-000',
			),
			123 => 
			array (
				'id' => 2367,
				'nome' => 'Vila Propício',
				'uf' => 'GO',
				'cep2' => '5222302',
				'estado_cod' => 9,
				'cep' => '76393-000',
			),
			124 => 
			array (
				'id' => 2369,
				'nome' => 'Açailândia',
				'uf' => 'MA',
				'cep2' => '2100055',
				'estado_cod' => 10,
				'cep' => '65930-000',
			),
			125 => 
			array (
				'id' => 2370,
				'nome' => 'Afonso Cunha',
				'uf' => 'MA',
				'cep2' => '2100105',
				'estado_cod' => 10,
				'cep' => '65505-000',
			),
			126 => 
			array (
				'id' => 2371,
				'nome' => 'Água Doce do Maranhão',
				'uf' => 'MA',
				'cep2' => '2100154',
				'estado_cod' => 10,
				'cep' => '65578-000',
			),
			127 => 
			array (
				'id' => 2372,
				'nome' => 'Alcântara',
				'uf' => 'MA',
				'cep2' => '2100204',
				'estado_cod' => 10,
				'cep' => '65250-000',
			),
			128 => 
			array (
				'id' => 2373,
				'nome' => 'Aldeias Altas',
				'uf' => 'MA',
				'cep2' => '2100303',
				'estado_cod' => 10,
				'cep' => '65610-000',
			),
			129 => 
			array (
				'id' => 2374,
				'nome' => 'Altamira do Maranhão',
				'uf' => 'MA',
				'cep2' => '2100402',
				'estado_cod' => 10,
				'cep' => '65310-000',
			),
			130 => 
			array (
				'id' => 2375,
				'nome' => 'Alto Alegre do Maranhão',
				'uf' => 'MA',
				'cep2' => '2100436',
				'estado_cod' => 10,
				'cep' => '65413-000',
			),
			131 => 
			array (
				'id' => 2376,
				'nome' => 'Alto Alegre do Pindaré',
				'uf' => 'MA',
				'cep2' => '2100477',
				'estado_cod' => 10,
				'cep' => '65398-000',
			),
			132 => 
			array (
				'id' => 2377,
				'nome' => 'Alto Parnaíba',
				'uf' => 'MA',
				'cep2' => '2100501',
				'estado_cod' => 10,
				'cep' => '65810-000',
			),
			133 => 
			array (
				'id' => 2378,
				'nome' => 'Amapá do Maranhão',
				'uf' => 'MA',
				'cep2' => '2100550',
				'estado_cod' => 10,
				'cep' => '65293-000',
			),
			134 => 
			array (
				'id' => 2379,
				'nome' => 'Amarante do Maranhão',
				'uf' => 'MA',
				'cep2' => '2100600',
				'estado_cod' => 10,
				'cep' => '65923-000',
			),
			135 => 
			array (
				'id' => 2380,
				'nome' => 'Anajatuba',
				'uf' => 'MA',
				'cep2' => '2100709',
				'estado_cod' => 10,
				'cep' => '65490-000',
			),
			136 => 
			array (
				'id' => 2381,
				'nome' => 'Anapurus',
				'uf' => 'MA',
				'cep2' => '2100808',
				'estado_cod' => 10,
				'cep' => '65525-000',
			),
			137 => 
			array (
				'id' => 2383,
				'nome' => 'Apicum-Açu',
				'uf' => 'MA',
				'cep2' => '2100832',
				'estado_cod' => 10,
				'cep' => '65275-000',
			),
			138 => 
			array (
				'id' => 2384,
				'nome' => 'Araguanã',
				'uf' => 'MA',
				'cep2' => '2100873',
				'estado_cod' => 10,
				'cep' => '65368-000',
			),
			139 => 
			array (
				'id' => 2385,
				'nome' => 'Araióses',
				'uf' => 'MA',
				'cep2' => '',
				'estado_cod' => 10,
				'cep' => '65570-000',
			),
			140 => 
			array (
				'id' => 2386,
				'nome' => 'Arame',
				'uf' => 'MA',
				'cep2' => '2100956',
				'estado_cod' => 10,
				'cep' => '65945-000',
			),
			141 => 
			array (
				'id' => 2387,
				'nome' => 'Arari',
				'uf' => 'MA',
				'cep2' => '2101004',
				'estado_cod' => 10,
				'cep' => '65480-000',
			),
			142 => 
			array (
				'id' => 2389,
				'nome' => 'Axixá',
				'uf' => 'MA',
				'cep2' => '2101103',
				'estado_cod' => 10,
				'cep' => '65148-000',
			),
			143 => 
			array (
				'id' => 2390,
				'nome' => 'Bacabal',
				'uf' => 'MA',
				'cep2' => '2101202',
				'estado_cod' => 10,
				'cep' => '65700-000',
			),
			144 => 
			array (
				'id' => 2391,
				'nome' => 'Bacabeira',
				'uf' => 'MA',
				'cep2' => '2101251',
				'estado_cod' => 10,
				'cep' => '65143-000',
			),
			145 => 
			array (
				'id' => 2393,
				'nome' => 'Bacuri',
				'uf' => 'MA',
				'cep2' => '2101301',
				'estado_cod' => 10,
				'cep' => '65270-000',
			),
			146 => 
			array (
				'id' => 2394,
				'nome' => 'Bacurituba',
				'uf' => 'MA',
				'cep2' => '2101350',
				'estado_cod' => 10,
				'cep' => '65233-000',
			),
			147 => 
			array (
				'id' => 2395,
				'nome' => 'Balsas',
				'uf' => 'MA',
				'cep2' => '2101400',
				'estado_cod' => 10,
				'cep' => '65800-000',
			),
			148 => 
			array (
				'id' => 2396,
				'nome' => 'Barão de Grajaú',
				'uf' => 'MA',
				'cep2' => '2101509',
				'estado_cod' => 10,
				'cep' => '65660-000',
			),
			149 => 
			array (
				'id' => 2398,
				'nome' => 'Barra do Corda',
				'uf' => 'MA',
				'cep2' => '2101608',
				'estado_cod' => 10,
				'cep' => '65950-000',
			),
			150 => 
			array (
				'id' => 2399,
				'nome' => 'Barreirinhas',
				'uf' => 'MA',
				'cep2' => '2101707',
				'estado_cod' => 10,
				'cep' => '65590-000',
			),
			151 => 
			array (
				'id' => 2401,
				'nome' => 'Bela Vista do Maranhão',
				'uf' => 'MA',
				'cep2' => '2101772',
				'estado_cod' => 10,
				'cep' => '65335-000',
			),
			152 => 
			array (
				'id' => 2402,
				'nome' => 'Belágua',
				'uf' => 'MA',
				'cep2' => '2101731',
				'estado_cod' => 10,
				'cep' => '65535-000',
			),
			153 => 
			array (
				'id' => 2403,
				'nome' => 'Benedito Leite',
				'uf' => 'MA',
				'cep2' => '2101806',
				'estado_cod' => 10,
				'cep' => '65885-000',
			),
			154 => 
			array (
				'id' => 2404,
				'nome' => 'Bequimão',
				'uf' => 'MA',
				'cep2' => '2101905',
				'estado_cod' => 10,
				'cep' => '65248-000',
			),
			155 => 
			array (
				'id' => 2405,
				'nome' => 'Bernardo do Mearim',
				'uf' => 'MA',
				'cep2' => '2101939',
				'estado_cod' => 10,
				'cep' => '65723-000',
			),
			156 => 
			array (
				'id' => 2406,
				'nome' => 'Boa Vista do Gurupi',
				'uf' => 'MA',
				'cep2' => '2101970',
				'estado_cod' => 10,
				'cep' => '65292-000',
			),
			157 => 
			array (
				'id' => 2408,
				'nome' => 'Bom Jardim',
				'uf' => 'MA',
				'cep2' => '2102002',
				'estado_cod' => 10,
				'cep' => '65380-000',
			),
			158 => 
			array (
				'id' => 2409,
				'nome' => 'Bom Jesus das Selvas',
				'uf' => 'MA',
				'cep2' => '2102036',
				'estado_cod' => 10,
				'cep' => '65395-000',
			),
			159 => 
			array (
				'id' => 2410,
				'nome' => 'Bom Lugar',
				'uf' => 'MA',
				'cep2' => '2102077',
				'estado_cod' => 10,
				'cep' => '65704-000',
			),
			160 => 
			array (
				'id' => 2412,
				'nome' => 'Brejo',
				'uf' => 'MA',
				'cep2' => '2102101',
				'estado_cod' => 10,
				'cep' => '65520-000',
			),
			161 => 
			array (
				'id' => 2413,
				'nome' => 'Brejo de Areia',
				'uf' => 'MA',
				'cep2' => '2102150',
				'estado_cod' => 10,
				'cep' => '65315-000',
			),
			162 => 
			array (
				'id' => 2415,
				'nome' => 'Buriti',
				'uf' => 'MA',
				'cep2' => '2102200',
				'estado_cod' => 10,
				'cep' => '65515-000',
			),
			163 => 
			array (
				'id' => 2416,
				'nome' => 'Buriti Bravo',
				'uf' => 'MA',
				'cep2' => '2102309',
				'estado_cod' => 10,
				'cep' => '65685-000',
			),
			164 => 
			array (
				'id' => 2418,
				'nome' => 'Buriticupu',
				'uf' => 'MA',
				'cep2' => '2102325',
				'estado_cod' => 10,
				'cep' => '65393-000',
			),
			165 => 
			array (
				'id' => 2419,
				'nome' => 'Buritirama',
				'uf' => 'MA',
				'cep2' => '',
				'estado_cod' => 10,
				'cep' => '65935-500',
			),
			166 => 
			array (
				'id' => 2420,
				'nome' => 'Cachoeira Grande',
				'uf' => 'MA',
				'cep2' => '2102374',
				'estado_cod' => 10,
				'cep' => '65165-000',
			),
			167 => 
			array (
				'id' => 2421,
				'nome' => 'Cajapió',
				'uf' => 'MA',
				'cep2' => '2102408',
				'estado_cod' => 10,
				'cep' => '65230-000',
			),
			168 => 
			array (
				'id' => 2422,
				'nome' => 'Cajari',
				'uf' => 'MA',
				'cep2' => '2102507',
				'estado_cod' => 10,
				'cep' => '65210-000',
			),
			169 => 
			array (
				'id' => 2423,
				'nome' => 'Campestre do Maranhão',
				'uf' => 'MA',
				'cep2' => '2102556',
				'estado_cod' => 10,
				'cep' => '65968-000',
			),
			170 => 
			array (
				'id' => 2424,
				'nome' => 'Cândido Mendes',
				'uf' => 'MA',
				'cep2' => '2102606',
				'estado_cod' => 10,
				'cep' => '65280-000',
			),
			171 => 
			array (
				'id' => 2425,
				'nome' => 'Cantanhede',
				'uf' => 'MA',
				'cep2' => '2102705',
				'estado_cod' => 10,
				'cep' => '65465-000',
			),
			172 => 
			array (
				'id' => 2426,
				'nome' => 'Capinzal do Norte',
				'uf' => 'MA',
				'cep2' => '2102754',
				'estado_cod' => 10,
				'cep' => '65735-000',
			),
			173 => 
			array (
				'id' => 2428,
				'nome' => 'Carolina',
				'uf' => 'MA',
				'cep2' => '2102804',
				'estado_cod' => 10,
				'cep' => '65980-000',
			),
			174 => 
			array (
				'id' => 2429,
				'nome' => 'Carutapera',
				'uf' => 'MA',
				'cep2' => '2102903',
				'estado_cod' => 10,
				'cep' => '65295-000',
			),
			175 => 
			array (
				'id' => 2430,
				'nome' => 'Caxias',
				'uf' => 'MA',
				'cep2' => '2103000',
				'estado_cod' => 10,
				'cep' => 'LOC',
			),
			176 => 
			array (
				'id' => 2431,
				'nome' => 'Cedral',
				'uf' => 'MA',
				'cep2' => '2103109',
				'estado_cod' => 10,
				'cep' => '65260-000',
			),
			177 => 
			array (
				'id' => 2432,
				'nome' => 'Central do Maranhão',
				'uf' => 'MA',
				'cep2' => '2103125',
				'estado_cod' => 10,
				'cep' => '65267-000',
			),
			178 => 
			array (
				'id' => 2433,
				'nome' => 'Centro do Guilherme',
				'uf' => 'MA',
				'cep2' => '2103158',
				'estado_cod' => 10,
				'cep' => '65288-000',
			),
			179 => 
			array (
				'id' => 2434,
				'nome' => 'Centro Novo do Maranhão',
				'uf' => 'MA',
				'cep2' => '2103174',
				'estado_cod' => 10,
				'cep' => '65299-000',
			),
			180 => 
			array (
				'id' => 2435,
				'nome' => 'Chapadinha',
				'uf' => 'MA',
				'cep2' => '2103208',
				'estado_cod' => 10,
				'cep' => '65500-000',
			),
			181 => 
			array (
				'id' => 2436,
				'nome' => 'Cidelândia',
				'uf' => 'MA',
				'cep2' => '2103257',
				'estado_cod' => 10,
				'cep' => '65921-000',
			),
			182 => 
			array (
				'id' => 2437,
				'nome' => 'Codó',
				'uf' => 'MA',
				'cep2' => '2103307',
				'estado_cod' => 10,
				'cep' => '65400-000',
			),
			183 => 
			array (
				'id' => 2439,
				'nome' => 'Coelho Neto',
				'uf' => 'MA',
				'cep2' => '2103406',
				'estado_cod' => 10,
				'cep' => '65620-000',
			),
			184 => 
			array (
				'id' => 2440,
				'nome' => 'Colinas',
				'uf' => 'MA',
				'cep2' => '2103505',
				'estado_cod' => 10,
				'cep' => '65690-000',
			),
			185 => 
			array (
				'id' => 2441,
				'nome' => 'Conceição do Lago-Açu',
				'uf' => 'MA',
				'cep2' => '2103554',
				'estado_cod' => 10,
				'cep' => '65340-000',
			),
			186 => 
			array (
				'id' => 2442,
				'nome' => 'Coroatá',
				'uf' => 'MA',
				'cep2' => '2103604',
				'estado_cod' => 10,
				'cep' => '65415-000',
			),
			187 => 
			array (
				'id' => 2444,
				'nome' => 'Cururupu',
				'uf' => 'MA',
				'cep2' => '2103703',
				'estado_cod' => 10,
				'cep' => '65268-000',
			),
			188 => 
			array (
				'id' => 2447,
				'nome' => 'Davinópolis',
				'uf' => 'MA',
				'cep2' => '2103752',
				'estado_cod' => 10,
				'cep' => '65927-000',
			),
			189 => 
			array (
				'id' => 2448,
				'nome' => 'Dom Pedro',
				'uf' => 'MA',
				'cep2' => '2103802',
				'estado_cod' => 10,
				'cep' => '65765-000',
			),
			190 => 
			array (
				'id' => 2449,
				'nome' => 'Duque Bacelar',
				'uf' => 'MA',
				'cep2' => '2103901',
				'estado_cod' => 10,
				'cep' => '65625-000',
			),
			191 => 
			array (
				'id' => 2450,
				'nome' => 'Esperantinópolis',
				'uf' => 'MA',
				'cep2' => '2104008',
				'estado_cod' => 10,
				'cep' => '65750-000',
			),
			192 => 
			array (
				'id' => 2452,
				'nome' => 'Estreito',
				'uf' => 'MA',
				'cep2' => '2104057',
				'estado_cod' => 10,
				'cep' => '65975-000',
			),
			193 => 
			array (
				'id' => 2453,
				'nome' => 'Feira Nova do Maranhão',
				'uf' => 'MA',
				'cep2' => '2104073',
				'estado_cod' => 10,
				'cep' => '65995-000',
			),
			194 => 
			array (
				'id' => 2454,
				'nome' => 'Fernando Falcão',
				'uf' => 'MA',
				'cep2' => '2104081',
				'estado_cod' => 10,
				'cep' => '65964-000',
			),
			195 => 
			array (
				'id' => 2455,
				'nome' => 'Formosa da Serra Negra',
				'uf' => 'MA',
				'cep2' => '2104099',
				'estado_cod' => 10,
				'cep' => '65943-000',
			),
			196 => 
			array (
				'id' => 2456,
				'nome' => 'Fortaleza dos Nogueiras',
				'uf' => 'MA',
				'cep2' => '2104107',
				'estado_cod' => 10,
				'cep' => '65805-000',
			),
			197 => 
			array (
				'id' => 2457,
				'nome' => 'Fortuna',
				'uf' => 'MA',
				'cep2' => '2104206',
				'estado_cod' => 10,
				'cep' => '65695-000',
			),
			198 => 
			array (
				'id' => 2459,
				'nome' => 'Godofredo Viana',
				'uf' => 'MA',
				'cep2' => '2104305',
				'estado_cod' => 10,
				'cep' => '65285-000',
			),
			199 => 
			array (
				'id' => 2460,
				'nome' => 'Gonçalves Dias',
				'uf' => 'MA',
				'cep2' => '2104404',
				'estado_cod' => 10,
				'cep' => '65775-000',
			),
			200 => 
			array (
				'id' => 2461,
				'nome' => 'Governador Archer',
				'uf' => 'MA',
				'cep2' => '2104503',
				'estado_cod' => 10,
				'cep' => '65770-000',
			),
			201 => 
			array (
				'id' => 2462,
				'nome' => 'Governador Edson Lobão',
				'uf' => 'MA',
				'cep2' => '',
				'estado_cod' => 10,
				'cep' => '65928-000',
			),
			202 => 
			array (
				'id' => 2463,
				'nome' => 'Governador Eugênio Barros',
				'uf' => 'MA',
				'cep2' => '2104602',
				'estado_cod' => 10,
				'cep' => '65780-000',
			),
			203 => 
			array (
				'id' => 2464,
				'nome' => 'Governador Luiz Rocha',
				'uf' => 'MA',
				'cep2' => '2104628',
				'estado_cod' => 10,
				'cep' => '65795-000',
			),
			204 => 
			array (
				'id' => 2465,
				'nome' => 'Governador Newton Bello',
				'uf' => 'MA',
				'cep2' => '2104651',
				'estado_cod' => 10,
				'cep' => '65363-000',
			),
			205 => 
			array (
				'id' => 2466,
				'nome' => 'Governador Nunes Freire',
				'uf' => 'MA',
				'cep2' => '2104677',
				'estado_cod' => 10,
				'cep' => '65284-000',
			),
			206 => 
			array (
				'id' => 2467,
				'nome' => 'Graça Aranha',
				'uf' => 'MA',
				'cep2' => '2104701',
				'estado_cod' => 10,
				'cep' => '65785-000',
			),
			207 => 
			array (
				'id' => 2468,
				'nome' => 'Grajaú',
				'uf' => 'MA',
				'cep2' => '2104800',
				'estado_cod' => 10,
				'cep' => '65940-000',
			),
			208 => 
			array (
				'id' => 2469,
				'nome' => 'Guimarães',
				'uf' => 'MA',
				'cep2' => '2104909',
				'estado_cod' => 10,
				'cep' => '65255-000',
			),
			209 => 
			array (
				'id' => 2470,
				'nome' => 'Humberto de Campos',
				'uf' => 'MA',
				'cep2' => '2105005',
				'estado_cod' => 10,
				'cep' => '65180-000',
			),
			210 => 
			array (
				'id' => 2472,
				'nome' => 'Icatu',
				'uf' => 'MA',
				'cep2' => '2105104',
				'estado_cod' => 10,
				'cep' => '65170-000',
			),
			211 => 
			array (
				'id' => 2473,
				'nome' => 'Igarapé do Meio',
				'uf' => 'MA',
				'cep2' => '2105153',
				'estado_cod' => 10,
				'cep' => '65345-000',
			),
			212 => 
			array (
				'id' => 2474,
				'nome' => 'Igarapé Grande',
				'uf' => 'MA',
				'cep2' => '2105203',
				'estado_cod' => 10,
				'cep' => '65720-000',
			),
			213 => 
			array (
				'id' => 2475,
				'nome' => 'Imperatriz',
				'uf' => 'MA',
				'cep2' => '2105302',
				'estado_cod' => 10,
				'cep' => 'LOC',
			),
			214 => 
			array (
				'id' => 2476,
				'nome' => 'Itaipava do Grajaú',
				'uf' => 'MA',
				'cep2' => '2105351',
				'estado_cod' => 10,
				'cep' => '65948-000',
			),
			215 => 
			array (
				'id' => 2478,
				'nome' => 'Itapecuru Mirim',
				'uf' => 'MA',
				'cep2' => '2105401',
				'estado_cod' => 10,
				'cep' => '65485-000',
			),
			216 => 
			array (
				'id' => 2480,
				'nome' => 'Itinga do Maranhão',
				'uf' => 'MA',
				'cep2' => '2105427',
				'estado_cod' => 10,
				'cep' => '65939-000',
			),
			217 => 
			array (
				'id' => 2481,
				'nome' => 'Jatobá',
				'uf' => 'MA',
				'cep2' => '2105450',
				'estado_cod' => 10,
				'cep' => '65693-000',
			),
			218 => 
			array (
				'id' => 2482,
				'nome' => 'Jenipapo dos Vieiras',
				'uf' => 'MA',
				'cep2' => '2105476',
				'estado_cod' => 10,
				'cep' => '65962-000',
			),
			219 => 
			array (
				'id' => 2483,
				'nome' => 'João Lisboa',
				'uf' => 'MA',
				'cep2' => '2105500',
				'estado_cod' => 10,
				'cep' => '65922-000',
			),
			220 => 
			array (
				'id' => 2484,
				'nome' => 'Joselândia',
				'uf' => 'MA',
				'cep2' => '2105609',
				'estado_cod' => 10,
				'cep' => '65755-000',
			),
			221 => 
			array (
				'id' => 2485,
				'nome' => 'Junco do Maranhão',
				'uf' => 'MA',
				'cep2' => '2105658',
				'estado_cod' => 10,
				'cep' => '65294-000',
			),
			222 => 
			array (
				'id' => 2486,
				'nome' => 'Lago da Pedra',
				'uf' => 'MA',
				'cep2' => '2105708',
				'estado_cod' => 10,
				'cep' => '65715-000',
			),
			223 => 
			array (
				'id' => 2487,
				'nome' => 'Lago do Junco',
				'uf' => 'MA',
				'cep2' => '2105807',
				'estado_cod' => 10,
				'cep' => '65710-000',
			),
			224 => 
			array (
				'id' => 2488,
				'nome' => 'Lago dos Rodrigues',
				'uf' => 'MA',
				'cep2' => '2105948',
				'estado_cod' => 10,
				'cep' => '65712-000',
			),
			225 => 
			array (
				'id' => 2489,
				'nome' => 'Lago Verde',
				'uf' => 'MA',
				'cep2' => '2105906',
				'estado_cod' => 10,
				'cep' => '65705-000',
			),
			226 => 
			array (
				'id' => 2490,
				'nome' => 'Lagoa do Mato',
				'uf' => 'MA',
				'cep2' => '2105922',
				'estado_cod' => 10,
				'cep' => '65683-000',
			),
			227 => 
			array (
				'id' => 2491,
				'nome' => 'Lagoa Grande do Maranhão',
				'uf' => 'MA',
				'cep2' => '2105963',
				'estado_cod' => 10,
				'cep' => '65718-000',
			),
			228 => 
			array (
				'id' => 2492,
				'nome' => 'Lajeado Novo',
				'uf' => 'MA',
				'cep2' => '2105989',
				'estado_cod' => 10,
				'cep' => '65937-000',
			),
			229 => 
			array (
				'id' => 2495,
				'nome' => 'Lima Campos',
				'uf' => 'MA',
				'cep2' => '2106003',
				'estado_cod' => 10,
				'cep' => '65728-000',
			),
			230 => 
			array (
				'id' => 2496,
				'nome' => 'Loreto',
				'uf' => 'MA',
				'cep2' => '2106102',
				'estado_cod' => 10,
				'cep' => '65895-000',
			),
			231 => 
			array (
				'id' => 2497,
				'nome' => 'Luís Domingues',
				'uf' => 'MA',
				'cep2' => '2106201',
				'estado_cod' => 10,
				'cep' => '65290-000',
			),
			232 => 
			array (
				'id' => 2498,
				'nome' => 'Magalhães de Almeida',
				'uf' => 'MA',
				'cep2' => '2106300',
				'estado_cod' => 10,
				'cep' => '65560-000',
			),
			233 => 
			array (
				'id' => 2500,
				'nome' => 'Maracaçumé',
				'uf' => 'MA',
				'cep2' => '2106326',
				'estado_cod' => 10,
				'cep' => '65289-000',
			),
			234 => 
			array (
				'id' => 2501,
				'nome' => 'Marajá do Sena',
				'uf' => 'MA',
				'cep2' => '2106359',
				'estado_cod' => 10,
				'cep' => '65714-000',
			),
			235 => 
			array (
				'id' => 2502,
				'nome' => 'Maranhãozinho',
				'uf' => 'MA',
				'cep2' => '2106375',
				'estado_cod' => 10,
				'cep' => '65283-000',
			),
			236 => 
			array (
				'id' => 2505,
				'nome' => 'Mata Roma',
				'uf' => 'MA',
				'cep2' => '2106409',
				'estado_cod' => 10,
				'cep' => '65510-000',
			),
			237 => 
			array (
				'id' => 2506,
				'nome' => 'Matinha',
				'uf' => 'MA',
				'cep2' => '2106508',
				'estado_cod' => 10,
				'cep' => '65218-000',
			),
			238 => 
			array (
				'id' => 2507,
				'nome' => 'Matões',
				'uf' => 'MA',
				'cep2' => '2106607',
				'estado_cod' => 10,
				'cep' => '65645-000',
			),
			239 => 
			array (
				'id' => 2508,
				'nome' => 'Matões do Norte',
				'uf' => 'MA',
				'cep2' => '2106631',
				'estado_cod' => 10,
				'cep' => '65468-000',
			),
			240 => 
			array (
				'id' => 2509,
				'nome' => 'Milagres do Maranhão',
				'uf' => 'MA',
				'cep2' => '2106672',
				'estado_cod' => 10,
				'cep' => '65545-000',
			),
			241 => 
			array (
				'id' => 2510,
				'nome' => 'Mirador',
				'uf' => 'MA',
				'cep2' => '2106706',
				'estado_cod' => 10,
				'cep' => '65850-000',
			),
			242 => 
			array (
				'id' => 2511,
				'nome' => 'Miranda do Norte',
				'uf' => 'MA',
				'cep2' => '2106755',
				'estado_cod' => 10,
				'cep' => '65495-000',
			),
			243 => 
			array (
				'id' => 2512,
				'nome' => 'Mirinzal',
				'uf' => 'MA',
				'cep2' => '2106805',
				'estado_cod' => 10,
				'cep' => '65265-000',
			),
			244 => 
			array (
				'id' => 2513,
				'nome' => 'Monção',
				'uf' => 'MA',
				'cep2' => '2106904',
				'estado_cod' => 10,
				'cep' => '65360-000',
			),
			245 => 
			array (
				'id' => 2514,
				'nome' => 'Montes Altos',
				'uf' => 'MA',
				'cep2' => '2107001',
				'estado_cod' => 10,
				'cep' => '65936-000',
			),
			246 => 
			array (
				'id' => 2515,
				'nome' => 'Morros',
				'uf' => 'MA',
				'cep2' => '2107100',
				'estado_cod' => 10,
				'cep' => '65160-000',
			),
			247 => 
			array (
				'id' => 2516,
				'nome' => 'Nina Rodrigues',
				'uf' => 'MA',
				'cep2' => '2107209',
				'estado_cod' => 10,
				'cep' => '65450-000',
			),
			248 => 
			array (
				'id' => 2517,
				'nome' => 'Nova Colinas',
				'uf' => 'MA',
				'cep2' => '2107258',
				'estado_cod' => 10,
				'cep' => '65808-000',
			),
			249 => 
			array (
				'id' => 2518,
				'nome' => 'Nova Iorque',
				'uf' => 'MA',
				'cep2' => '2107308',
				'estado_cod' => 10,
				'cep' => '65880-000',
			),
			250 => 
			array (
				'id' => 2519,
				'nome' => 'Nova Olinda do Maranhão',
				'uf' => 'MA',
				'cep2' => '2107357',
				'estado_cod' => 10,
				'cep' => '65274-000',
			),
			251 => 
			array (
				'id' => 2520,
				'nome' => 'Olho D\'Água das Cunhãs',
				'uf' => 'MA',
				'cep2' => '2107407',
				'estado_cod' => 10,
				'cep' => '65706-000',
			),
			252 => 
			array (
				'id' => 2521,
				'nome' => 'Olinda Nova do Maranhão',
				'uf' => 'MA',
				'cep2' => '2107456',
				'estado_cod' => 10,
				'cep' => '65223-000',
			),
			253 => 
			array (
				'id' => 2522,
				'nome' => 'Paço do Lumiar',
				'uf' => 'MA',
				'cep2' => '2107506',
				'estado_cod' => 10,
				'cep' => '65130-000',
			),
			254 => 
			array (
				'id' => 2523,
				'nome' => 'Palmeirândia',
				'uf' => 'MA',
				'cep2' => '2107605',
				'estado_cod' => 10,
				'cep' => '65238-000',
			),
			255 => 
			array (
				'id' => 2525,
				'nome' => 'Paraibano',
				'uf' => 'MA',
				'cep2' => '2107704',
				'estado_cod' => 10,
				'cep' => '65670-000',
			),
			256 => 
			array (
				'id' => 2526,
				'nome' => 'Parnarama',
				'uf' => 'MA',
				'cep2' => '2107803',
				'estado_cod' => 10,
				'cep' => '65640-000',
			),
			257 => 
			array (
				'id' => 2527,
				'nome' => 'Passagem Franca',
				'uf' => 'MA',
				'cep2' => '2107902',
				'estado_cod' => 10,
				'cep' => '65680-000',
			),
			258 => 
			array (
				'id' => 2528,
				'nome' => 'Pastos Bons',
				'uf' => 'MA',
				'cep2' => '2108009',
				'estado_cod' => 10,
				'cep' => '65870-000',
			),
			259 => 
			array (
				'id' => 2529,
				'nome' => 'Paulino Neves',
				'uf' => 'MA',
				'cep2' => '2108058',
				'estado_cod' => 10,
				'cep' => '65585-000',
			),
			260 => 
			array (
				'id' => 2530,
				'nome' => 'Paulo Ramos',
				'uf' => 'MA',
				'cep2' => '2108108',
				'estado_cod' => 10,
				'cep' => '65716-000',
			),
			261 => 
			array (
				'id' => 2531,
				'nome' => 'Pedreiras',
				'uf' => 'MA',
				'cep2' => '2108207',
				'estado_cod' => 10,
				'cep' => '65725-000',
			),
			262 => 
			array (
				'id' => 2532,
				'nome' => 'Pedro do Rosário',
				'uf' => 'MA',
				'cep2' => '2108256',
				'estado_cod' => 10,
				'cep' => '65206-000',
			),
			263 => 
			array (
				'id' => 2533,
				'nome' => 'Penalva',
				'uf' => 'MA',
				'cep2' => '2108306',
				'estado_cod' => 10,
				'cep' => '65213-000',
			),
			264 => 
			array (
				'id' => 2534,
				'nome' => 'Peri Mirim',
				'uf' => 'MA',
				'cep2' => '2108405',
				'estado_cod' => 10,
				'cep' => '65245-000',
			),
			265 => 
			array (
				'id' => 2535,
				'nome' => 'Peritoró',
				'uf' => 'MA',
				'cep2' => '2108454',
				'estado_cod' => 10,
				'cep' => '65418-000',
			),
			266 => 
			array (
				'id' => 2537,
				'nome' => 'Pindaré Mirim',
				'uf' => 'MA',
				'cep2' => '2108504',
				'estado_cod' => 10,
				'cep' => '65370-000',
			),
			267 => 
			array (
				'id' => 2538,
				'nome' => 'Pinheiro',
				'uf' => 'MA',
				'cep2' => '2108603',
				'estado_cod' => 10,
				'cep' => '65200-000',
			),
			268 => 
			array (
				'id' => 2539,
				'nome' => 'Pio XII',
				'uf' => 'MA',
				'cep2' => '2108702',
				'estado_cod' => 10,
				'cep' => '65707-000',
			),
			269 => 
			array (
				'id' => 2540,
				'nome' => 'Pirapemas',
				'uf' => 'MA',
				'cep2' => '2108801',
				'estado_cod' => 10,
				'cep' => '65460-000',
			),
			270 => 
			array (
				'id' => 2541,
				'nome' => 'Poção de Pedras',
				'uf' => 'MA',
				'cep2' => '2108900',
				'estado_cod' => 10,
				'cep' => '65740-000',
			),
			271 => 
			array (
				'id' => 2543,
				'nome' => 'Porto Franco',
				'uf' => 'MA',
				'cep2' => '2109007',
				'estado_cod' => 10,
				'cep' => '65970-000',
			),
			272 => 
			array (
				'id' => 2544,
				'nome' => 'Porto Rico do Maranhão',
				'uf' => 'MA',
				'cep2' => '2109056',
				'estado_cod' => 10,
				'cep' => '65263-000',
			),
			273 => 
			array (
				'id' => 2545,
				'nome' => 'Presidente Dutra',
				'uf' => 'MA',
				'cep2' => '2109106',
				'estado_cod' => 10,
				'cep' => '65760-000',
			),
			274 => 
			array (
				'id' => 2546,
				'nome' => 'Presidente Juscelino',
				'uf' => 'MA',
				'cep2' => '2109205',
				'estado_cod' => 10,
				'cep' => '65140-000',
			),
			275 => 
			array (
				'id' => 2547,
				'nome' => 'Presidente Médici',
				'uf' => 'MA',
				'cep2' => '2109239',
				'estado_cod' => 10,
				'cep' => '65279-000',
			),
			276 => 
			array (
				'id' => 2548,
				'nome' => 'Presidente Sarney',
				'uf' => 'MA',
				'cep2' => '2109270',
				'estado_cod' => 10,
				'cep' => '65204-000',
			),
			277 => 
			array (
				'id' => 2549,
				'nome' => 'Presidente Vargas',
				'uf' => 'MA',
				'cep2' => '2109304',
				'estado_cod' => 10,
				'cep' => '65455-000',
			),
			278 => 
			array (
				'id' => 2550,
				'nome' => 'Primeira Cruz',
				'uf' => 'MA',
				'cep2' => '2109403',
				'estado_cod' => 10,
				'cep' => '65190-000',
			),
			279 => 
			array (
				'id' => 2551,
				'nome' => 'Raposa',
				'uf' => 'MA',
				'cep2' => '2109452',
				'estado_cod' => 10,
				'cep' => '65138-000',
			),
			280 => 
			array (
				'id' => 2553,
				'nome' => 'Riachão',
				'uf' => 'MA',
				'cep2' => '2109502',
				'estado_cod' => 10,
				'cep' => '65990-000',
			),
			281 => 
			array (
				'id' => 2554,
				'nome' => 'Ribamar Fiquene',
				'uf' => 'MA',
				'cep2' => '2109551',
				'estado_cod' => 10,
				'cep' => '65938-000',
			),
			282 => 
			array (
				'id' => 2558,
				'nome' => 'Rosário',
				'uf' => 'MA',
				'cep2' => '2109601',
				'estado_cod' => 10,
				'cep' => '65150-000',
			),
			283 => 
			array (
				'id' => 2559,
				'nome' => 'Sambaíba',
				'uf' => 'MA',
				'cep2' => '2109700',
				'estado_cod' => 10,
				'cep' => '65830-000',
			),
			284 => 
			array (
				'id' => 2560,
				'nome' => 'Santa Filomena do Maranhão',
				'uf' => 'MA',
				'cep2' => '2109759',
				'estado_cod' => 10,
				'cep' => '65768-000',
			),
			285 => 
			array (
				'id' => 2561,
				'nome' => 'Santa Helena',
				'uf' => 'MA',
				'cep2' => '2109809',
				'estado_cod' => 10,
				'cep' => '65208-000',
			),
			286 => 
			array (
				'id' => 2562,
				'nome' => 'Santa Inês',
				'uf' => 'MA',
				'cep2' => '2109908',
				'estado_cod' => 10,
				'cep' => '65300-000',
			),
			287 => 
			array (
				'id' => 2563,
				'nome' => 'Santa Luzia',
				'uf' => 'MA',
				'cep2' => '2110005',
				'estado_cod' => 10,
				'cep' => '65390-000',
			),
			288 => 
			array (
				'id' => 2564,
				'nome' => 'Santa Luzia do Paruá',
				'uf' => 'MA',
				'cep2' => '2110039',
				'estado_cod' => 10,
				'cep' => '65272-000',
			),
			289 => 
			array (
				'id' => 2565,
				'nome' => 'Santa Quitéria do Maranhão',
				'uf' => 'MA',
				'cep2' => '2110104',
				'estado_cod' => 10,
				'cep' => '65540-000',
			),
			290 => 
			array (
				'id' => 2566,
				'nome' => 'Santa Rita',
				'uf' => 'MA',
				'cep2' => '2110203',
				'estado_cod' => 10,
				'cep' => '65145-000',
			),
			291 => 
			array (
				'id' => 2567,
				'nome' => 'Santana do Maranhão',
				'uf' => 'MA',
				'cep2' => '2110237',
				'estado_cod' => 10,
				'cep' => '65555-000',
			),
			292 => 
			array (
				'id' => 2568,
				'nome' => 'Santo Amaro',
				'uf' => 'MA',
				'cep2' => '',
				'estado_cod' => 10,
				'cep' => '65195-000',
			),
			293 => 
			array (
				'id' => 2569,
				'nome' => 'Santo Antônio dos Lopes',
				'uf' => 'MA',
				'cep2' => '2110302',
				'estado_cod' => 10,
				'cep' => '65730-000',
			),
			294 => 
			array (
				'id' => 2570,
				'nome' => 'São Benedito do Rio Preto',
				'uf' => 'MA',
				'cep2' => '2110401',
				'estado_cod' => 10,
				'cep' => '65440-000',
			),
			295 => 
			array (
				'id' => 2571,
				'nome' => 'São Bento',
				'uf' => 'MA',
				'cep2' => '2110500',
				'estado_cod' => 10,
				'cep' => '65235-000',
			),
			296 => 
			array (
				'id' => 2572,
				'nome' => 'São Bernardo',
				'uf' => 'MA',
				'cep2' => '2110609',
				'estado_cod' => 10,
				'cep' => '65550-000',
			),
			297 => 
			array (
				'id' => 2573,
				'nome' => 'São Domingos do Azeitão',
				'uf' => 'MA',
				'cep2' => '2110658',
				'estado_cod' => 10,
				'cep' => '65888-000',
			),
			298 => 
			array (
				'id' => 2574,
				'nome' => 'São Domingos do Maranhão',
				'uf' => 'MA',
				'cep2' => '2110708',
				'estado_cod' => 10,
				'cep' => '65790-000',
			),
			299 => 
			array (
				'id' => 2575,
				'nome' => 'São Félix de Balsas',
				'uf' => 'MA',
				'cep2' => '2110807',
				'estado_cod' => 10,
				'cep' => '65890-000',
			),
			300 => 
			array (
				'id' => 2576,
				'nome' => 'São Francisco do Brejão',
				'uf' => 'MA',
				'cep2' => '2110856',
				'estado_cod' => 10,
				'cep' => '65929-000',
			),
			301 => 
			array (
				'id' => 2577,
				'nome' => 'São Francisco do Maranhão',
				'uf' => 'MA',
				'cep2' => '2110906',
				'estado_cod' => 10,
				'cep' => '65650-000',
			),
			302 => 
			array (
				'id' => 2578,
				'nome' => 'São João Batista',
				'uf' => 'MA',
				'cep2' => '2111003',
				'estado_cod' => 10,
				'cep' => '65225-000',
			),
			303 => 
			array (
				'id' => 2580,
				'nome' => 'São João do Carú',
				'uf' => 'MA',
				'cep2' => '',
				'estado_cod' => 10,
				'cep' => '65385-000',
			),
			304 => 
			array (
				'id' => 2581,
				'nome' => 'São João do Paraíso',
				'uf' => 'MA',
				'cep2' => '2111052',
				'estado_cod' => 10,
				'cep' => '65973-000',
			),
			305 => 
			array (
				'id' => 2582,
				'nome' => 'São João do Sóter',
				'uf' => 'MA',
				'cep2' => '',
				'estado_cod' => 10,
				'cep' => '65615-000',
			),
			306 => 
			array (
				'id' => 2583,
				'nome' => 'São João dos Patos',
				'uf' => 'MA',
				'cep2' => '2111102',
				'estado_cod' => 10,
				'cep' => '65665-000',
			),
			307 => 
			array (
				'id' => 2585,
				'nome' => 'São José de Ribamar',
				'uf' => 'MA',
				'cep2' => '2111201',
				'estado_cod' => 10,
				'cep' => '65110-000',
			),
			308 => 
			array (
				'id' => 2586,
				'nome' => 'São José dos Basílios',
				'uf' => 'MA',
				'cep2' => '2111250',
				'estado_cod' => 10,
				'cep' => '65762-000',
			),
			309 => 
			array (
				'id' => 2587,
				'nome' => 'São Luís',
				'uf' => 'MA',
				'cep2' => '2111300',
				'estado_cod' => 10,
				'cep' => 'LOC',
			),
			310 => 
			array (
				'id' => 2588,
				'nome' => 'São Luís Gonzaga do Maranhão',
				'uf' => 'MA',
				'cep2' => '2111409',
				'estado_cod' => 10,
				'cep' => '65708-000',
			),
			311 => 
			array (
				'id' => 2589,
				'nome' => 'São Mateus do Maranhão',
				'uf' => 'MA',
				'cep2' => '2111508',
				'estado_cod' => 10,
				'cep' => '65470-000',
			),
			312 => 
			array (
				'id' => 2590,
				'nome' => 'São Pedro da Água Branca',
				'uf' => 'MA',
				'cep2' => '2111532',
				'estado_cod' => 10,
				'cep' => '65920-000',
			),
			313 => 
			array (
				'id' => 2591,
				'nome' => 'São Pedro dos Crentes',
				'uf' => 'MA',
				'cep2' => '2111573',
				'estado_cod' => 10,
				'cep' => '65978-000',
			),
			314 => 
			array (
				'id' => 2592,
				'nome' => 'São Raimundo das Mangabeiras',
				'uf' => 'MA',
				'cep2' => '2111607',
				'estado_cod' => 10,
				'cep' => '65840-000',
			),
			315 => 
			array (
				'id' => 2594,
				'nome' => 'São Raimundo do Doca Bezerra',
				'uf' => 'MA',
				'cep2' => '2111631',
				'estado_cod' => 10,
				'cep' => '65753-000',
			),
			316 => 
			array (
				'id' => 2595,
				'nome' => 'São Roberto',
				'uf' => 'MA',
				'cep2' => '2111672',
				'estado_cod' => 10,
				'cep' => '65758-000',
			),
			317 => 
			array (
				'id' => 2596,
				'nome' => 'São Vicente Ferrer',
				'uf' => 'MA',
				'cep2' => '2111706',
				'estado_cod' => 10,
				'cep' => '65220-000',
			),
			318 => 
			array (
				'id' => 2597,
				'nome' => 'Satubinha',
				'uf' => 'MA',
				'cep2' => '2111722',
				'estado_cod' => 10,
				'cep' => '65709-000',
			),
			319 => 
			array (
				'id' => 2598,
				'nome' => 'Senador Alexandre Costa',
				'uf' => 'MA',
				'cep2' => '2111748',
				'estado_cod' => 10,
				'cep' => '65783-000',
			),
			320 => 
			array (
				'id' => 2599,
				'nome' => 'Senador La Roque',
				'uf' => 'MA',
				'cep2' => '',
				'estado_cod' => 10,
				'cep' => '65935-000',
			),
			321 => 
			array (
				'id' => 2600,
				'nome' => 'Serrano do Maranhão',
				'uf' => 'MA',
				'cep2' => '2111789',
				'estado_cod' => 10,
				'cep' => '65269-000',
			),
			322 => 
			array (
				'id' => 2601,
				'nome' => 'Sítio Novo',
				'uf' => 'MA',
				'cep2' => '2111805',
				'estado_cod' => 10,
				'cep' => '65925-000',
			),
			323 => 
			array (
				'id' => 2602,
				'nome' => 'Sucupira do Norte',
				'uf' => 'MA',
				'cep2' => '2111904',
				'estado_cod' => 10,
				'cep' => '65860-000',
			),
			324 => 
			array (
				'id' => 2603,
				'nome' => 'Sucupira do Riachão',
				'uf' => 'MA',
				'cep2' => '2111953',
				'estado_cod' => 10,
				'cep' => '65668-000',
			),
			325 => 
			array (
				'id' => 2604,
				'nome' => 'Tasso Fragoso',
				'uf' => 'MA',
				'cep2' => '2112001',
				'estado_cod' => 10,
				'cep' => '65820-000',
			),
			326 => 
			array (
				'id' => 2605,
				'nome' => 'Timbiras',
				'uf' => 'MA',
				'cep2' => '2112100',
				'estado_cod' => 10,
				'cep' => '65420-000',
			),
			327 => 
			array (
				'id' => 2606,
				'nome' => 'Timon',
				'uf' => 'MA',
				'cep2' => '2112209',
				'estado_cod' => 10,
				'cep' => 'LOC',
			),
			328 => 
			array (
				'id' => 2607,
				'nome' => 'Trizidela do Vale',
				'uf' => 'MA',
				'cep2' => '2112233',
				'estado_cod' => 10,
				'cep' => '65727-000',
			),
			329 => 
			array (
				'id' => 2608,
				'nome' => 'Tufilândia',
				'uf' => 'MA',
				'cep2' => '2112274',
				'estado_cod' => 10,
				'cep' => '65378-000',
			),
			330 => 
			array (
				'id' => 2609,
				'nome' => 'Tuntum',
				'uf' => 'MA',
				'cep2' => '2112308',
				'estado_cod' => 10,
				'cep' => '65763-000',
			),
			331 => 
			array (
				'id' => 2610,
				'nome' => 'Turiaçu',
				'uf' => 'MA',
				'cep2' => '2112407',
				'estado_cod' => 10,
				'cep' => '65278-000',
			),
			332 => 
			array (
				'id' => 2611,
				'nome' => 'Turilândia',
				'uf' => 'MA',
				'cep2' => '2112456',
				'estado_cod' => 10,
				'cep' => '65276-000',
			),
			333 => 
			array (
				'id' => 2612,
				'nome' => 'Tutóia',
				'uf' => 'MA',
				'cep2' => '2112506',
				'estado_cod' => 10,
				'cep' => '65580-000',
			),
			334 => 
			array (
				'id' => 2613,
				'nome' => 'Urbano Santos',
				'uf' => 'MA',
				'cep2' => '2112605',
				'estado_cod' => 10,
				'cep' => '65530-000',
			),
			335 => 
			array (
				'id' => 2614,
				'nome' => 'Vargem Grande',
				'uf' => 'MA',
				'cep2' => '2112704',
				'estado_cod' => 10,
				'cep' => '65430-000',
			),
			336 => 
			array (
				'id' => 2615,
				'nome' => 'Viana',
				'uf' => 'MA',
				'cep2' => '2112803',
				'estado_cod' => 10,
				'cep' => '65215-000',
			),
			337 => 
			array (
				'id' => 2616,
				'nome' => 'Vila Nova dos Martírios',
				'uf' => 'MA',
				'cep2' => '2112852',
				'estado_cod' => 10,
				'cep' => '65924-000',
			),
			338 => 
			array (
				'id' => 2617,
				'nome' => 'Vitória do Mearim',
				'uf' => 'MA',
				'cep2' => '2112902',
				'estado_cod' => 10,
				'cep' => '65350-000',
			),
			339 => 
			array (
				'id' => 2618,
				'nome' => 'Vitorino Freire',
				'uf' => 'MA',
				'cep2' => '2113009',
				'estado_cod' => 10,
				'cep' => '65320-000',
			),
			340 => 
			array (
				'id' => 2619,
				'nome' => 'Zé Doca',
				'uf' => 'MA',
				'cep2' => '2114007',
				'estado_cod' => 10,
				'cep' => '65365-000',
			),
			341 => 
			array (
				'id' => 2620,
				'nome' => 'Abadia dos Dourados',
				'uf' => 'MG',
				'cep2' => '3100104',
				'estado_cod' => 11,
				'cep' => '38540-000',
			),
			342 => 
			array (
				'id' => 2621,
				'nome' => 'Abaeté',
				'uf' => 'MG',
				'cep2' => '3100203',
				'estado_cod' => 11,
				'cep' => '35620-000',
			),
			343 => 
			array (
				'id' => 2624,
				'nome' => 'Abre Campo',
				'uf' => 'MG',
				'cep2' => '3100302',
				'estado_cod' => 11,
				'cep' => '35365-000',
			),
			344 => 
			array (
				'id' => 2626,
				'nome' => 'Acaiaca',
				'uf' => 'MG',
				'cep2' => '3100401',
				'estado_cod' => 11,
				'cep' => '35438-000',
			),
			345 => 
			array (
				'id' => 2627,
				'nome' => 'Açucena',
				'uf' => 'MG',
				'cep2' => '3100500',
				'estado_cod' => 11,
				'cep' => '35150-000',
			),
			346 => 
			array (
				'id' => 2630,
				'nome' => 'Água Boa',
				'uf' => 'MG',
				'cep2' => '3100609',
				'estado_cod' => 11,
				'cep' => '39790-000',
			),
			347 => 
			array (
				'id' => 2632,
				'nome' => 'Água Comprida',
				'uf' => 'MG',
				'cep2' => '3100708',
				'estado_cod' => 11,
				'cep' => '38110-000',
			),
			348 => 
			array (
				'id' => 2634,
				'nome' => 'Aguanil',
				'uf' => 'MG',
				'cep2' => '3100807',
				'estado_cod' => 11,
				'cep' => '37273-000',
			),
			349 => 
			array (
				'id' => 2638,
				'nome' => 'Águas Formosas',
				'uf' => 'MG',
				'cep2' => '3100906',
				'estado_cod' => 11,
				'cep' => '39880-000',
			),
			350 => 
			array (
				'id' => 2639,
				'nome' => 'Águas Vermelhas',
				'uf' => 'MG',
				'cep2' => '3101003',
				'estado_cod' => 11,
				'cep' => '39990-000',
			),
			351 => 
			array (
				'id' => 2640,
				'nome' => 'Aimorés',
				'uf' => 'MG',
				'cep2' => '3101102',
				'estado_cod' => 11,
				'cep' => '35200-000',
			),
			352 => 
			array (
				'id' => 2641,
				'nome' => 'Aiuruoca',
				'uf' => 'MG',
				'cep2' => '3101201',
				'estado_cod' => 11,
				'cep' => '37450-000',
			),
			353 => 
			array (
				'id' => 2642,
				'nome' => 'Alagoa',
				'uf' => 'MG',
				'cep2' => '3101300',
				'estado_cod' => 11,
				'cep' => '37458-000',
			),
			354 => 
			array (
				'id' => 2643,
				'nome' => 'Albertina',
				'uf' => 'MG',
				'cep2' => '3101409',
				'estado_cod' => 11,
				'cep' => '37596-000',
			),
			355 => 
			array (
				'id' => 2649,
				'nome' => 'Além Paraíba',
				'uf' => 'MG',
				'cep2' => '3101508',
				'estado_cod' => 11,
				'cep' => '36660-000',
			),
			356 => 
			array (
				'id' => 2651,
				'nome' => 'Alfenas',
				'uf' => 'MG',
				'cep2' => '3101607',
				'estado_cod' => 11,
				'cep' => '37130-000',
			),
			357 => 
			array (
				'id' => 2652,
				'nome' => 'Alfredo Vasconcelos',
				'uf' => 'MG',
				'cep2' => '3101631',
				'estado_cod' => 11,
				'cep' => '36272-000',
			),
			358 => 
			array (
				'id' => 2654,
				'nome' => 'Almenara',
				'uf' => 'MG',
				'cep2' => '3101706',
				'estado_cod' => 11,
				'cep' => '39900-000',
			),
			359 => 
			array (
				'id' => 2655,
				'nome' => 'Alpercata',
				'uf' => 'MG',
				'cep2' => '3101805',
				'estado_cod' => 11,
				'cep' => '35138-000',
			),
			360 => 
			array (
				'id' => 2656,
				'nome' => 'Alpinópolis',
				'uf' => 'MG',
				'cep2' => '3101904',
				'estado_cod' => 11,
				'cep' => '37940-000',
			),
			361 => 
			array (
				'id' => 2657,
				'nome' => 'Alterosa',
				'uf' => 'MG',
				'cep2' => '3102001',
				'estado_cod' => 11,
				'cep' => '37145-000',
			),
			362 => 
			array (
				'id' => 2658,
				'nome' => 'Alto Caparaó',
				'uf' => 'MG',
				'cep2' => '3102050',
				'estado_cod' => 11,
				'cep' => '36979-000',
			),
			363 => 
			array (
				'id' => 2661,
				'nome' => 'Alto Jequitibá',
				'uf' => 'MG',
				'cep2' => '3153509',
				'estado_cod' => 11,
				'cep' => '36976-000',
			),
			364 => 
			array (
				'id' => 2663,
				'nome' => 'Alto Rio Doce',
				'uf' => 'MG',
				'cep2' => '3102100',
				'estado_cod' => 11,
				'cep' => '36260-000',
			),
			365 => 
			array (
				'id' => 2666,
				'nome' => 'Alvarenga',
				'uf' => 'MG',
				'cep2' => '3102209',
				'estado_cod' => 11,
				'cep' => '35249-000',
			),
			366 => 
			array (
				'id' => 2667,
				'nome' => 'Alvinópolis',
				'uf' => 'MG',
				'cep2' => '3102308',
				'estado_cod' => 11,
				'cep' => '35950-000',
			),
			367 => 
			array (
				'id' => 2669,
				'nome' => 'Alvorada de Minas',
				'uf' => 'MG',
				'cep2' => '3102407',
				'estado_cod' => 11,
				'cep' => '39140-000',
			),
			368 => 
			array (
				'id' => 2673,
				'nome' => 'Amparo da Serra',
				'uf' => 'MG',
				'cep2' => '3102506',
				'estado_cod' => 11,
				'cep' => '35444-000',
			),
			369 => 
			array (
				'id' => 2675,
				'nome' => 'Andradas',
				'uf' => 'MG',
				'cep2' => '3102605',
				'estado_cod' => 11,
				'cep' => '37795-000',
			),
			370 => 
			array (
				'id' => 2676,
				'nome' => 'Andrelândia',
				'uf' => 'MG',
				'cep2' => '3102803',
				'estado_cod' => 11,
				'cep' => '37300-000',
			),
			371 => 
			array (
				'id' => 2679,
				'nome' => 'Angelândia',
				'uf' => 'MG',
				'cep2' => '3102852',
				'estado_cod' => 11,
				'cep' => '39685-000',
			),
			372 => 
			array (
				'id' => 2683,
				'nome' => 'Antônio Carlos',
				'uf' => 'MG',
				'cep2' => '3102902',
				'estado_cod' => 11,
				'cep' => '36220-000',
			),
			373 => 
			array (
				'id' => 2684,
				'nome' => 'Antônio Dias',
				'uf' => 'MG',
				'cep2' => '3103009',
				'estado_cod' => 11,
				'cep' => '35177-000',
			),
			374 => 
			array (
				'id' => 2688,
				'nome' => 'Antônio Prado de Minas',
				'uf' => 'MG',
				'cep2' => '3103108',
				'estado_cod' => 11,
				'cep' => '36850-000',
			),
			375 => 
			array (
				'id' => 2691,
				'nome' => 'Araçaí',
				'uf' => 'MG',
				'cep2' => '3103207',
				'estado_cod' => 11,
				'cep' => '35777-000',
			),
			376 => 
			array (
				'id' => 2693,
				'nome' => 'Aracitaba',
				'uf' => 'MG',
				'cep2' => '3103306',
				'estado_cod' => 11,
				'cep' => '36255-000',
			),
			377 => 
			array (
				'id' => 2694,
				'nome' => 'Araçuaí',
				'uf' => 'MG',
				'cep2' => '3103405',
				'estado_cod' => 11,
				'cep' => '39600-000',
			),
			378 => 
			array (
				'id' => 2695,
				'nome' => 'Araguari',
				'uf' => 'MG',
				'cep2' => '3103504',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			379 => 
			array (
				'id' => 2698,
				'nome' => 'Arantina',
				'uf' => 'MG',
				'cep2' => '3103603',
				'estado_cod' => 11,
				'cep' => '37360-000',
			),
			380 => 
			array (
				'id' => 2699,
				'nome' => 'Araponga',
				'uf' => 'MG',
				'cep2' => '3103702',
				'estado_cod' => 11,
				'cep' => '36594-000',
			),
			381 => 
			array (
				'id' => 2700,
				'nome' => 'Araporã',
				'uf' => 'MG',
				'cep2' => '3103751',
				'estado_cod' => 11,
				'cep' => '38435-000',
			),
			382 => 
			array (
				'id' => 2701,
				'nome' => 'Arapuá',
				'uf' => 'MG',
				'cep2' => '3103801',
				'estado_cod' => 11,
				'cep' => '38860-000',
			),
			383 => 
			array (
				'id' => 2702,
				'nome' => 'Araújos',
				'uf' => 'MG',
				'cep2' => '3103900',
				'estado_cod' => 11,
				'cep' => '35603-000',
			),
			384 => 
			array (
				'id' => 2704,
				'nome' => 'Araxá',
				'uf' => 'MG',
				'cep2' => '3104007',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			385 => 
			array (
				'id' => 2706,
				'nome' => 'Arceburgo',
				'uf' => 'MG',
				'cep2' => '3104106',
				'estado_cod' => 11,
				'cep' => '37820-000',
			),
			386 => 
			array (
				'id' => 2707,
				'nome' => 'Arcos',
				'uf' => 'MG',
				'cep2' => '3104205',
				'estado_cod' => 11,
				'cep' => '35588-000',
			),
			387 => 
			array (
				'id' => 2708,
				'nome' => 'Areado',
				'uf' => 'MG',
				'cep2' => '3104304',
				'estado_cod' => 11,
				'cep' => '37140-000',
			),
			388 => 
			array (
				'id' => 2710,
				'nome' => 'Argirita',
				'uf' => 'MG',
				'cep2' => '3104403',
				'estado_cod' => 11,
				'cep' => '36710-000',
			),
			389 => 
			array (
				'id' => 2711,
				'nome' => 'Aricanduva',
				'uf' => 'MG',
				'cep2' => '3104452',
				'estado_cod' => 11,
				'cep' => '39678-000',
			),
			390 => 
			array (
				'id' => 2712,
				'nome' => 'Arinos',
				'uf' => 'MG',
				'cep2' => '3104502',
				'estado_cod' => 11,
				'cep' => '38680-000',
			),
			391 => 
			array (
				'id' => 2716,
				'nome' => 'Astolfo Dutra',
				'uf' => 'MG',
				'cep2' => '3104601',
				'estado_cod' => 11,
				'cep' => '36780-000',
			),
			392 => 
			array (
				'id' => 2717,
				'nome' => 'Ataléia',
				'uf' => 'MG',
				'cep2' => '3104700',
				'estado_cod' => 11,
				'cep' => '39850-000',
			),
			393 => 
			array (
				'id' => 2718,
				'nome' => 'Augusto de Lima',
				'uf' => 'MG',
				'cep2' => '3104809',
				'estado_cod' => 11,
				'cep' => '39220-000',
			),
			394 => 
			array (
				'id' => 2723,
				'nome' => 'Baependi',
				'uf' => 'MG',
				'cep2' => '3104908',
				'estado_cod' => 11,
				'cep' => '37443-000',
			),
			395 => 
			array (
				'id' => 2728,
				'nome' => 'Baldim',
				'uf' => 'MG',
				'cep2' => '3105004',
				'estado_cod' => 11,
				'cep' => '35706-000',
			),
			396 => 
			array (
				'id' => 2729,
				'nome' => 'Bambuí',
				'uf' => 'MG',
				'cep2' => '3105103',
				'estado_cod' => 11,
				'cep' => '38900-000',
			),
			397 => 
			array (
				'id' => 2730,
				'nome' => 'Bandeira',
				'uf' => 'MG',
				'cep2' => '3105202',
				'estado_cod' => 11,
				'cep' => '39917-000',
			),
			398 => 
			array (
				'id' => 2731,
				'nome' => 'Bandeira do Sul',
				'uf' => 'MG',
				'cep2' => '3105301',
				'estado_cod' => 11,
				'cep' => '37740-000',
			),
			399 => 
			array (
				'id' => 2733,
				'nome' => 'Barão de Cocais',
				'uf' => 'MG',
				'cep2' => '3105400',
				'estado_cod' => 11,
				'cep' => '35970-000',
			),
			400 => 
			array (
				'id' => 2734,
				'nome' => 'Barão de Monte Alto',
				'uf' => 'MG',
				'cep2' => '3105509',
				'estado_cod' => 11,
				'cep' => '36870-000',
			),
			401 => 
			array (
				'id' => 2735,
				'nome' => 'Barbacena',
				'uf' => 'MG',
				'cep2' => '3105608',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			402 => 
			array (
				'id' => 2741,
				'nome' => 'Barra Longa',
				'uf' => 'MG',
				'cep2' => '3105707',
				'estado_cod' => 11,
				'cep' => '35447-000',
			),
			403 => 
			array (
				'id' => 2749,
				'nome' => 'Barroso',
				'uf' => 'MG',
				'cep2' => '3105905',
				'estado_cod' => 11,
				'cep' => '36212-000',
			),
			404 => 
			array (
				'id' => 2751,
				'nome' => 'Bela Vista de Minas',
				'uf' => 'MG',
				'cep2' => '3106002',
				'estado_cod' => 11,
				'cep' => '35938-000',
			),
			405 => 
			array (
				'id' => 2753,
				'nome' => 'Belmiro Braga',
				'uf' => 'MG',
				'cep2' => '3106101',
				'estado_cod' => 11,
				'cep' => '36126-000',
			),
			406 => 
			array (
				'id' => 2754,
				'nome' => 'Belo Horizonte',
				'uf' => 'MG',
				'cep2' => '3106200',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			407 => 
			array (
				'id' => 2755,
				'nome' => 'Belo Oriente',
				'uf' => 'MG',
				'cep2' => '3106309',
				'estado_cod' => 11,
				'cep' => '35195-000',
			),
			408 => 
			array (
				'id' => 2757,
				'nome' => 'Belo Vale',
				'uf' => 'MG',
				'cep2' => '3106408',
				'estado_cod' => 11,
				'cep' => '35473-000',
			),
			409 => 
			array (
				'id' => 2759,
				'nome' => 'Berilo',
				'uf' => 'MG',
				'cep2' => '3106507',
				'estado_cod' => 11,
				'cep' => '39640-000',
			),
			410 => 
			array (
				'id' => 2760,
				'nome' => 'Berizal',
				'uf' => 'MG',
				'cep2' => '3106655',
				'estado_cod' => 11,
				'cep' => '39555-000',
			),
			411 => 
			array (
				'id' => 2761,
				'nome' => 'Bertópolis',
				'uf' => 'MG',
				'cep2' => '3106606',
				'estado_cod' => 11,
				'cep' => '39875-000',
			),
			412 => 
			array (
				'id' => 2762,
				'nome' => 'Betim',
				'uf' => 'MG',
				'cep2' => '3106705',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			413 => 
			array (
				'id' => 2763,
				'nome' => 'Bias Fortes',
				'uf' => 'MG',
				'cep2' => '3106804',
				'estado_cod' => 11,
				'cep' => '36230-000',
			),
			414 => 
			array (
				'id' => 2764,
				'nome' => 'Bicas',
				'uf' => 'MG',
				'cep2' => '3106903',
				'estado_cod' => 11,
				'cep' => '36600-000',
			),
			415 => 
			array (
				'id' => 2766,
				'nome' => 'Biquinhas',
				'uf' => 'MG',
				'cep2' => '3107000',
				'estado_cod' => 11,
				'cep' => '35621-000',
			),
			416 => 
			array (
				'id' => 2768,
				'nome' => 'Boa Esperança',
				'uf' => 'MG',
				'cep2' => '3107109',
				'estado_cod' => 11,
				'cep' => '37170-000',
			),
			417 => 
			array (
				'id' => 2772,
				'nome' => 'Bocaina de Minas',
				'uf' => 'MG',
				'cep2' => '3107208',
				'estado_cod' => 11,
				'cep' => '37340-000',
			),
			418 => 
			array (
				'id' => 2773,
				'nome' => 'Bocaiúva',
				'uf' => 'MG',
				'cep2' => '3107307',
				'estado_cod' => 11,
				'cep' => '39390-000',
			),
			419 => 
			array (
				'id' => 2774,
				'nome' => 'Bom Despacho',
				'uf' => 'MG',
				'cep2' => '3107406',
				'estado_cod' => 11,
				'cep' => '35600-000',
			),
			420 => 
			array (
				'id' => 2775,
				'nome' => 'Bom Jardim de Minas',
				'uf' => 'MG',
				'cep2' => '3107505',
				'estado_cod' => 11,
				'cep' => '37310-000',
			),
			421 => 
			array (
				'id' => 2777,
				'nome' => 'Bom Jesus da Penha',
				'uf' => 'MG',
				'cep2' => '3107604',
				'estado_cod' => 11,
				'cep' => '37948-000',
			),
			422 => 
			array (
				'id' => 2779,
				'nome' => 'Bom Jesus do Amparo',
				'uf' => 'MG',
				'cep2' => '3107703',
				'estado_cod' => 11,
				'cep' => '35908-000',
			),
			423 => 
			array (
				'id' => 2781,
				'nome' => 'Bom Jesus do Galho',
				'uf' => 'MG',
				'cep2' => '3107802',
				'estado_cod' => 11,
				'cep' => '35340-000',
			),
			424 => 
			array (
				'id' => 2784,
				'nome' => 'Bom Repouso',
				'uf' => 'MG',
				'cep2' => '3107901',
				'estado_cod' => 11,
				'cep' => '37610-000',
			),
			425 => 
			array (
				'id' => 2786,
				'nome' => 'Bom Sucesso',
				'uf' => 'MG',
				'cep2' => '3108008',
				'estado_cod' => 11,
				'cep' => '37220-000',
			),
			426 => 
			array (
				'id' => 2789,
				'nome' => 'Bonfim',
				'uf' => 'MG',
				'cep2' => '3108107',
				'estado_cod' => 11,
				'cep' => '35521-000',
			),
			427 => 
			array (
				'id' => 2790,
				'nome' => 'Bonfinópolis de Minas',
				'uf' => 'MG',
				'cep2' => '3108206',
				'estado_cod' => 11,
				'cep' => '38650-000',
			),
			428 => 
			array (
				'id' => 2791,
				'nome' => 'Bonito de Minas',
				'uf' => 'MG',
				'cep2' => '3108255',
				'estado_cod' => 11,
				'cep' => '39490-000',
			),
			429 => 
			array (
				'id' => 2793,
				'nome' => 'Borda da Mata',
				'uf' => 'MG',
				'cep2' => '3108305',
				'estado_cod' => 11,
				'cep' => '37564-000',
			),
			430 => 
			array (
				'id' => 2794,
				'nome' => 'Botelhos',
				'uf' => 'MG',
				'cep2' => '3108404',
				'estado_cod' => 11,
				'cep' => '37720-000',
			),
			431 => 
			array (
				'id' => 2795,
				'nome' => 'Botumirim',
				'uf' => 'MG',
				'cep2' => '3108503',
				'estado_cod' => 11,
				'cep' => '39596-000',
			),
			432 => 
			array (
				'id' => 2796,
				'nome' => 'Brás Pires',
				'uf' => 'MG',
				'cep2' => '3108701',
				'estado_cod' => 11,
				'cep' => '36542-000',
			),
			433 => 
			array (
				'id' => 2797,
				'nome' => 'Brasilândia de Minas',
				'uf' => 'MG',
				'cep2' => '3108552',
				'estado_cod' => 11,
				'cep' => '38779-000',
			),
			434 => 
			array (
				'id' => 2798,
				'nome' => 'Brasília de Minas',
				'uf' => 'MG',
				'cep2' => '3108602',
				'estado_cod' => 11,
				'cep' => '39330-000',
			),
			435 => 
			array (
				'id' => 2799,
				'nome' => 'Brasópolis',
				'uf' => 'MG',
				'cep2' => '3108909',
				'estado_cod' => 11,
				'cep' => '37530-000',
			),
			436 => 
			array (
				'id' => 2800,
				'nome' => 'Braúnas',
				'uf' => 'MG',
				'cep2' => '3108800',
				'estado_cod' => 11,
				'cep' => '35169-000',
			),
			437 => 
			array (
				'id' => 2805,
				'nome' => 'Brumadinho',
				'uf' => 'MG',
				'cep2' => '3109006',
				'estado_cod' => 11,
				'cep' => '35460-000',
			),
			438 => 
			array (
				'id' => 2809,
				'nome' => 'Bueno Brandão',
				'uf' => 'MG',
				'cep2' => '3109105',
				'estado_cod' => 11,
				'cep' => '37578-000',
			),
			439 => 
			array (
				'id' => 2810,
				'nome' => 'Buenópolis',
				'uf' => 'MG',
				'cep2' => '3109204',
				'estado_cod' => 11,
				'cep' => '39230-000',
			),
			440 => 
			array (
				'id' => 2811,
				'nome' => 'Bugre',
				'uf' => 'MG',
				'cep2' => '3109253',
				'estado_cod' => 11,
				'cep' => '35193-000',
			),
			441 => 
			array (
				'id' => 2812,
				'nome' => 'Buritis',
				'uf' => 'MG',
				'cep2' => '3109303',
				'estado_cod' => 11,
				'cep' => '38660-000',
			),
			442 => 
			array (
				'id' => 2813,
				'nome' => 'Buritizeiro',
				'uf' => 'MG',
				'cep2' => '3109402',
				'estado_cod' => 11,
				'cep' => '39280-000',
			),
			443 => 
			array (
				'id' => 2815,
				'nome' => 'Cabeceira Grande',
				'uf' => 'MG',
				'cep2' => '3109451',
				'estado_cod' => 11,
				'cep' => '38625-000',
			),
			444 => 
			array (
				'id' => 2816,
				'nome' => 'Cabo Verde',
				'uf' => 'MG',
				'cep2' => '3109501',
				'estado_cod' => 11,
				'cep' => '37880-000',
			),
			445 => 
			array (
				'id' => 2821,
				'nome' => 'Cachoeira da Prata',
				'uf' => 'MG',
				'cep2' => '3109600',
				'estado_cod' => 11,
				'cep' => '35765-000',
			),
			446 => 
			array (
				'id' => 2822,
				'nome' => 'Cachoeira de Minas',
				'uf' => 'MG',
				'cep2' => '3109709',
				'estado_cod' => 11,
				'cep' => '37545-000',
			),
			447 => 
			array (
				'id' => 2823,
				'nome' => 'Cachoeira de Pajeú',
				'uf' => 'MG',
				'cep2' => '3102704',
				'estado_cod' => 11,
				'cep' => '39980-000',
			),
			448 => 
			array (
				'id' => 2830,
				'nome' => 'Cachoeira Dourada',
				'uf' => 'MG',
				'cep2' => '3109808',
				'estado_cod' => 11,
				'cep' => '38370-000',
			),
			449 => 
			array (
				'id' => 2833,
				'nome' => 'Caetanópolis',
				'uf' => 'MG',
				'cep2' => '3109907',
				'estado_cod' => 11,
				'cep' => '35770-000',
			),
			450 => 
			array (
				'id' => 2834,
				'nome' => 'Caeté',
				'uf' => 'MG',
				'cep2' => '3110004',
				'estado_cod' => 11,
				'cep' => '34800-000',
			),
			451 => 
			array (
				'id' => 2836,
				'nome' => 'Caiana',
				'uf' => 'MG',
				'cep2' => '3110103',
				'estado_cod' => 11,
				'cep' => '36832-000',
			),
			452 => 
			array (
				'id' => 2838,
				'nome' => 'Cajuri',
				'uf' => 'MG',
				'cep2' => '3110202',
				'estado_cod' => 11,
				'cep' => '36560-000',
			),
			453 => 
			array (
				'id' => 2839,
				'nome' => 'Caldas',
				'uf' => 'MG',
				'cep2' => '3110301',
				'estado_cod' => 11,
				'cep' => '37780-000',
			),
			454 => 
			array (
				'id' => 2841,
				'nome' => 'Camacho',
				'uf' => 'MG',
				'cep2' => '3110400',
				'estado_cod' => 11,
				'cep' => '35555-000',
			),
			455 => 
			array (
				'id' => 2842,
				'nome' => 'Camanducaia',
				'uf' => 'MG',
				'cep2' => '3110509',
				'estado_cod' => 11,
				'cep' => '37650-000',
			),
			456 => 
			array (
				'id' => 2844,
				'nome' => 'Cambuí',
				'uf' => 'MG',
				'cep2' => '3110608',
				'estado_cod' => 11,
				'cep' => '37600-000',
			),
			457 => 
			array (
				'id' => 2845,
				'nome' => 'Cambuquira',
				'uf' => 'MG',
				'cep2' => '3110707',
				'estado_cod' => 11,
				'cep' => '37420-000',
			),
			458 => 
			array (
				'id' => 2846,
				'nome' => 'Campanário',
				'uf' => 'MG',
				'cep2' => '3110806',
				'estado_cod' => 11,
				'cep' => '39835-000',
			),
			459 => 
			array (
				'id' => 2847,
				'nome' => 'Campanha',
				'uf' => 'MG',
				'cep2' => '3110905',
				'estado_cod' => 11,
				'cep' => '37400-000',
			),
			460 => 
			array (
				'id' => 2848,
				'nome' => 'Campestre',
				'uf' => 'MG',
				'cep2' => '3111002',
				'estado_cod' => 11,
				'cep' => '37730-000',
			),
			461 => 
			array (
				'id' => 2850,
				'nome' => 'Campina Verde',
				'uf' => 'MG',
				'cep2' => '3111101',
				'estado_cod' => 11,
				'cep' => '38270-000',
			),
			462 => 
			array (
				'id' => 2853,
				'nome' => 'Campo Azul',
				'uf' => 'MG',
				'cep2' => '3111150',
				'estado_cod' => 11,
				'cep' => '39338-000',
			),
			463 => 
			array (
				'id' => 2854,
				'nome' => 'Campo Belo',
				'uf' => 'MG',
				'cep2' => '3111200',
				'estado_cod' => 11,
				'cep' => '37270-000',
			),
			464 => 
			array (
				'id' => 2855,
				'nome' => 'Campo do Meio',
				'uf' => 'MG',
				'cep2' => '3111309',
				'estado_cod' => 11,
				'cep' => '37165-000',
			),
			465 => 
			array (
				'id' => 2856,
				'nome' => 'Campo Florido',
				'uf' => 'MG',
				'cep2' => '3111408',
				'estado_cod' => 11,
				'cep' => '38130-000',
			),
			466 => 
			array (
				'id' => 2859,
				'nome' => 'Campos Altos',
				'uf' => 'MG',
				'cep2' => '3111507',
				'estado_cod' => 11,
				'cep' => '38970-000',
			),
			467 => 
			array (
				'id' => 2860,
				'nome' => 'Campos Gerais',
				'uf' => 'MG',
				'cep2' => '3111606',
				'estado_cod' => 11,
				'cep' => '37160-000',
			),
			468 => 
			array (
				'id' => 2861,
				'nome' => 'Cana Verde',
				'uf' => 'MG',
				'cep2' => '3111903',
				'estado_cod' => 11,
				'cep' => '37267-000',
			),
			469 => 
			array (
				'id' => 2862,
				'nome' => 'Canaã',
				'uf' => 'MG',
				'cep2' => '3111705',
				'estado_cod' => 11,
				'cep' => '36592-000',
			),
			470 => 
			array (
				'id' => 2865,
				'nome' => 'Canápolis',
				'uf' => 'MG',
				'cep2' => '3111804',
				'estado_cod' => 11,
				'cep' => '38380-000',
			),
			471 => 
			array (
				'id' => 2867,
				'nome' => 'Candeias',
				'uf' => 'MG',
				'cep2' => '3112000',
				'estado_cod' => 11,
				'cep' => '37280-000',
			),
			472 => 
			array (
				'id' => 2869,
				'nome' => 'Cantagalo',
				'uf' => 'MG',
				'cep2' => '3112059',
				'estado_cod' => 11,
				'cep' => '39703-000',
			),
			473 => 
			array (
				'id' => 2870,
				'nome' => 'Caparaó',
				'uf' => 'MG',
				'cep2' => '3112109',
				'estado_cod' => 11,
				'cep' => '36834-000',
			),
			474 => 
			array (
				'id' => 2871,
				'nome' => 'Capela Nova',
				'uf' => 'MG',
				'cep2' => '3112208',
				'estado_cod' => 11,
				'cep' => '36290-000',
			),
			475 => 
			array (
				'id' => 2872,
				'nome' => 'Capelinha',
				'uf' => 'MG',
				'cep2' => '3112307',
				'estado_cod' => 11,
				'cep' => '39680-000',
			),
			476 => 
			array (
				'id' => 2873,
				'nome' => 'Capetinga',
				'uf' => 'MG',
				'cep2' => '3112406',
				'estado_cod' => 11,
				'cep' => '37993-000',
			),
			477 => 
			array (
				'id' => 2874,
				'nome' => 'Capim Branco',
				'uf' => 'MG',
				'cep2' => '3112505',
				'estado_cod' => 11,
				'cep' => '35730-000',
			),
			478 => 
			array (
				'id' => 2875,
				'nome' => 'Capinópolis',
				'uf' => 'MG',
				'cep2' => '3112604',
				'estado_cod' => 11,
				'cep' => '38360-000',
			),
			479 => 
			array (
				'id' => 2877,
				'nome' => 'Capitão Andrade',
				'uf' => 'MG',
				'cep2' => '3112653',
				'estado_cod' => 11,
				'cep' => '35123-000',
			),
			480 => 
			array (
				'id' => 2878,
				'nome' => 'Capitão Enéas',
				'uf' => 'MG',
				'cep2' => '3112703',
				'estado_cod' => 11,
				'cep' => '39445-000',
			),
			481 => 
			array (
				'id' => 2879,
				'nome' => 'Capitólio',
				'uf' => 'MG',
				'cep2' => '3112802',
				'estado_cod' => 11,
				'cep' => '37930-000',
			),
			482 => 
			array (
				'id' => 2880,
				'nome' => 'Caputira',
				'uf' => 'MG',
				'cep2' => '3112901',
				'estado_cod' => 11,
				'cep' => '36925-000',
			),
			483 => 
			array (
				'id' => 2881,
				'nome' => 'Caraí',
				'uf' => 'MG',
				'cep2' => '3113008',
				'estado_cod' => 11,
				'cep' => '39810-000',
			),
			484 => 
			array (
				'id' => 2882,
				'nome' => 'Caranaíba',
				'uf' => 'MG',
				'cep2' => '3113107',
				'estado_cod' => 11,
				'cep' => '36428-000',
			),
			485 => 
			array (
				'id' => 2883,
				'nome' => 'Carandaí',
				'uf' => 'MG',
				'cep2' => '3113206',
				'estado_cod' => 11,
				'cep' => '36280-000',
			),
			486 => 
			array (
				'id' => 2884,
				'nome' => 'Carangola',
				'uf' => 'MG',
				'cep2' => '3113305',
				'estado_cod' => 11,
				'cep' => '36800-000',
			),
			487 => 
			array (
				'id' => 2885,
				'nome' => 'Caratinga',
				'uf' => 'MG',
				'cep2' => '3113404',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			488 => 
			array (
				'id' => 2886,
				'nome' => 'Carbonita',
				'uf' => 'MG',
				'cep2' => '3113503',
				'estado_cod' => 11,
				'cep' => '39665-000',
			),
			489 => 
			array (
				'id' => 2888,
				'nome' => 'Careaçu',
				'uf' => 'MG',
				'cep2' => '3113602',
				'estado_cod' => 11,
				'cep' => '37556-000',
			),
			490 => 
			array (
				'id' => 2891,
				'nome' => 'Carlos Chagas',
				'uf' => 'MG',
				'cep2' => '3113701',
				'estado_cod' => 11,
				'cep' => '39864-000',
			),
			491 => 
			array (
				'id' => 2892,
				'nome' => 'Carmésia',
				'uf' => 'MG',
				'cep2' => '3113800',
				'estado_cod' => 11,
				'cep' => '35878-000',
			),
			492 => 
			array (
				'id' => 2893,
				'nome' => 'Carmo da Cachoeira',
				'uf' => 'MG',
				'cep2' => '3113909',
				'estado_cod' => 11,
				'cep' => '37225-000',
			),
			493 => 
			array (
				'id' => 2894,
				'nome' => 'Carmo da Mata',
				'uf' => 'MG',
				'cep2' => '3114006',
				'estado_cod' => 11,
				'cep' => '35547-000',
			),
			494 => 
			array (
				'id' => 2895,
				'nome' => 'Carmo de Minas',
				'uf' => 'MG',
				'cep2' => '3114105',
				'estado_cod' => 11,
				'cep' => '37472-000',
			),
			495 => 
			array (
				'id' => 2896,
				'nome' => 'Carmo do Cajuru',
				'uf' => 'MG',
				'cep2' => '3114204',
				'estado_cod' => 11,
				'cep' => '35510-000',
			),
			496 => 
			array (
				'id' => 2897,
				'nome' => 'Carmo do Paranaíba',
				'uf' => 'MG',
				'cep2' => '3114303',
				'estado_cod' => 11,
				'cep' => '38840-000',
			),
			497 => 
			array (
				'id' => 2898,
				'nome' => 'Carmo do Rio Claro',
				'uf' => 'MG',
				'cep2' => '3114402',
				'estado_cod' => 11,
				'cep' => '37150-000',
			),
			498 => 
			array (
				'id' => 2899,
				'nome' => 'Carmópolis de Minas',
				'uf' => 'MG',
				'cep2' => '3114501',
				'estado_cod' => 11,
				'cep' => '35534-000',
			),
			499 => 
			array (
				'id' => 2900,
				'nome' => 'Carneirinho',
				'uf' => 'MG',
				'cep2' => '3114550',
				'estado_cod' => 11,
				'cep' => '38290-000',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 2901,
				'nome' => 'Carrancas',
				'uf' => 'MG',
				'cep2' => '3114600',
				'estado_cod' => 11,
				'cep' => '37245-000',
			),
			1 => 
			array (
				'id' => 2903,
				'nome' => 'Carvalhópolis',
				'uf' => 'MG',
				'cep2' => '3114709',
				'estado_cod' => 11,
				'cep' => '37760-000',
			),
			2 => 
			array (
				'id' => 2904,
				'nome' => 'Carvalhos',
				'uf' => 'MG',
				'cep2' => '3114808',
				'estado_cod' => 11,
				'cep' => '37456-000',
			),
			3 => 
			array (
				'id' => 2905,
				'nome' => 'Casa Grande',
				'uf' => 'MG',
				'cep2' => '3114907',
				'estado_cod' => 11,
				'cep' => '36422-000',
			),
			4 => 
			array (
				'id' => 2906,
				'nome' => 'Cascalho Rico',
				'uf' => 'MG',
				'cep2' => '3115003',
				'estado_cod' => 11,
				'cep' => '38460-000',
			),
			5 => 
			array (
				'id' => 2907,
				'nome' => 'Cássia',
				'uf' => 'MG',
				'cep2' => '3115102',
				'estado_cod' => 11,
				'cep' => '37980-000',
			),
			6 => 
			array (
				'id' => 2909,
				'nome' => 'Cataguases',
				'uf' => 'MG',
				'cep2' => '3115300',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			7 => 
			array (
				'id' => 2911,
				'nome' => 'Catas Altas',
				'uf' => 'MG',
				'cep2' => '3115359',
				'estado_cod' => 11,
				'cep' => '35969-000',
			),
			8 => 
			array (
				'id' => 2912,
				'nome' => 'Catas Altas da Noruega',
				'uf' => 'MG',
				'cep2' => '3115409',
				'estado_cod' => 11,
				'cep' => '36450-000',
			),
			9 => 
			array (
				'id' => 2914,
				'nome' => 'Catuji',
				'uf' => 'MG',
				'cep2' => '3115458',
				'estado_cod' => 11,
				'cep' => '39816-000',
			),
			10 => 
			array (
				'id' => 2917,
				'nome' => 'Catuti',
				'uf' => 'MG',
				'cep2' => '3115474',
				'estado_cod' => 11,
				'cep' => '39526-000',
			),
			11 => 
			array (
				'id' => 2918,
				'nome' => 'Caxambu',
				'uf' => 'MG',
				'cep2' => '3115508',
				'estado_cod' => 11,
				'cep' => '37440-000',
			),
			12 => 
			array (
				'id' => 2919,
				'nome' => 'Cedro do Abaeté',
				'uf' => 'MG',
				'cep2' => '3115607',
				'estado_cod' => 11,
				'cep' => '35624-000',
			),
			13 => 
			array (
				'id' => 2921,
				'nome' => 'Central de Minas',
				'uf' => 'MG',
				'cep2' => '3115706',
				'estado_cod' => 11,
				'cep' => '35260-000',
			),
			14 => 
			array (
				'id' => 2923,
				'nome' => 'Centralina',
				'uf' => 'MG',
				'cep2' => '3115805',
				'estado_cod' => 11,
				'cep' => '38390-000',
			),
			15 => 
			array (
				'id' => 2925,
				'nome' => 'Chácara',
				'uf' => 'MG',
				'cep2' => '3115904',
				'estado_cod' => 11,
				'cep' => '36110-000',
			),
			16 => 
			array (
				'id' => 2926,
				'nome' => 'Chalé',
				'uf' => 'MG',
				'cep2' => '3116001',
				'estado_cod' => 11,
				'cep' => '36985-000',
			),
			17 => 
			array (
				'id' => 2928,
				'nome' => 'Chapada do Norte',
				'uf' => 'MG',
				'cep2' => '3116100',
				'estado_cod' => 11,
				'cep' => '39648-000',
			),
			18 => 
			array (
				'id' => 2929,
				'nome' => 'Chapada Gaúcha',
				'uf' => 'MG',
				'cep2' => '3116159',
				'estado_cod' => 11,
				'cep' => '39314-000',
			),
			19 => 
			array (
				'id' => 2931,
				'nome' => 'Chiador',
				'uf' => 'MG',
				'cep2' => '3116209',
				'estado_cod' => 11,
				'cep' => '36630-000',
			),
			20 => 
			array (
				'id' => 2934,
				'nome' => 'Cipotânea',
				'uf' => 'MG',
				'cep2' => '3116308',
				'estado_cod' => 11,
				'cep' => '36265-000',
			),
			21 => 
			array (
				'id' => 2937,
				'nome' => 'Claraval',
				'uf' => 'MG',
				'cep2' => '3116407',
				'estado_cod' => 11,
				'cep' => '37997-000',
			),
			22 => 
			array (
				'id' => 2939,
				'nome' => 'Claro dos Poções',
				'uf' => 'MG',
				'cep2' => '3116506',
				'estado_cod' => 11,
				'cep' => '39380-000',
			),
			23 => 
			array (
				'id' => 2940,
				'nome' => 'Cláudio',
				'uf' => 'MG',
				'cep2' => '3116605',
				'estado_cod' => 11,
				'cep' => '35530-000',
			),
			24 => 
			array (
				'id' => 2944,
				'nome' => 'Coimbra',
				'uf' => 'MG',
				'cep2' => '3116704',
				'estado_cod' => 11,
				'cep' => '36550-000',
			),
			25 => 
			array (
				'id' => 2945,
				'nome' => 'Coluna',
				'uf' => 'MG',
				'cep2' => '3116803',
				'estado_cod' => 11,
				'cep' => '39770-000',
			),
			26 => 
			array (
				'id' => 2946,
				'nome' => 'Comendador Gomes',
				'uf' => 'MG',
				'cep2' => '3116902',
				'estado_cod' => 11,
				'cep' => '38250-000',
			),
			27 => 
			array (
				'id' => 2947,
				'nome' => 'Comercinho',
				'uf' => 'MG',
				'cep2' => '3117009',
				'estado_cod' => 11,
				'cep' => '39628-000',
			),
			28 => 
			array (
				'id' => 2948,
				'nome' => 'Conceição da Aparecida',
				'uf' => 'MG',
				'cep2' => '3117108',
				'estado_cod' => 11,
				'cep' => '37148-000',
			),
			29 => 
			array (
				'id' => 2949,
				'nome' => 'Conceição da Barra de Minas',
				'uf' => 'MG',
				'cep2' => '3115201',
				'estado_cod' => 11,
				'cep' => '36360-000',
			),
			30 => 
			array (
				'id' => 2953,
				'nome' => 'Conceição das Alagoas',
				'uf' => 'MG',
				'cep2' => '3117306',
				'estado_cod' => 11,
				'cep' => '38120-000',
			),
			31 => 
			array (
				'id' => 2954,
				'nome' => 'Conceição das Pedras',
				'uf' => 'MG',
				'cep2' => '3117207',
				'estado_cod' => 11,
				'cep' => '37527-000',
			),
			32 => 
			array (
				'id' => 2955,
				'nome' => 'Conceição de Ipanema',
				'uf' => 'MG',
				'cep2' => '3117405',
				'estado_cod' => 11,
				'cep' => '36947-000',
			),
			33 => 
			array (
				'id' => 2962,
				'nome' => 'Conceição do Mato Dentro',
				'uf' => 'MG',
				'cep2' => '3117504',
				'estado_cod' => 11,
				'cep' => '35860-000',
			),
			34 => 
			array (
				'id' => 2963,
				'nome' => 'Conceição do Pará',
				'uf' => 'MG',
				'cep2' => '3117603',
				'estado_cod' => 11,
				'cep' => '35668-000',
			),
			35 => 
			array (
				'id' => 2965,
				'nome' => 'Conceição do Rio Verde',
				'uf' => 'MG',
				'cep2' => '3117702',
				'estado_cod' => 11,
				'cep' => '37430-000',
			),
			36 => 
			array (
				'id' => 2966,
				'nome' => 'Conceição dos Ouros',
				'uf' => 'MG',
				'cep2' => '3117801',
				'estado_cod' => 11,
				'cep' => '37548-000',
			),
			37 => 
			array (
				'id' => 2970,
				'nome' => 'Cônego Marinho',
				'uf' => 'MG',
				'cep2' => '3117836',
				'estado_cod' => 11,
				'cep' => '39489-000',
			),
			38 => 
			array (
				'id' => 2971,
				'nome' => 'Confins',
				'uf' => 'MG',
				'cep2' => '3117876',
				'estado_cod' => 11,
				'cep' => '33500-000',
			),
			39 => 
			array (
				'id' => 2972,
				'nome' => 'Congonhal',
				'uf' => 'MG',
				'cep2' => '3117900',
				'estado_cod' => 11,
				'cep' => '37557-000',
			),
			40 => 
			array (
				'id' => 2973,
				'nome' => 'Congonhas',
				'uf' => 'MG',
				'cep2' => '3118007',
				'estado_cod' => 11,
				'cep' => '36415-000',
			),
			41 => 
			array (
				'id' => 2974,
				'nome' => 'Congonhas do Norte',
				'uf' => 'MG',
				'cep2' => '3118106',
				'estado_cod' => 11,
				'cep' => '35850-000',
			),
			42 => 
			array (
				'id' => 2975,
				'nome' => 'Conquista',
				'uf' => 'MG',
				'cep2' => '3118205',
				'estado_cod' => 11,
				'cep' => '38195-000',
			),
			43 => 
			array (
				'id' => 2976,
				'nome' => 'Conselheiro Lafaiete',
				'uf' => 'MG',
				'cep2' => '3118304',
				'estado_cod' => 11,
				'cep' => '36400-000',
			),
			44 => 
			array (
				'id' => 2978,
				'nome' => 'Conselheiro Pena',
				'uf' => 'MG',
				'cep2' => '3118403',
				'estado_cod' => 11,
				'cep' => '35240-000',
			),
			45 => 
			array (
				'id' => 2979,
				'nome' => 'Consolação',
				'uf' => 'MG',
				'cep2' => '3118502',
				'estado_cod' => 11,
				'cep' => '37670-000',
			),
			46 => 
			array (
				'id' => 2980,
				'nome' => 'Contagem',
				'uf' => 'MG',
				'cep2' => '3118601',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			47 => 
			array (
				'id' => 2983,
				'nome' => 'Coqueiral',
				'uf' => 'MG',
				'cep2' => '3118700',
				'estado_cod' => 11,
				'cep' => '37235-000',
			),
			48 => 
			array (
				'id' => 2984,
				'nome' => 'Coração de Jesus',
				'uf' => 'MG',
				'cep2' => '3118809',
				'estado_cod' => 11,
				'cep' => '39340-000',
			),
			49 => 
			array (
				'id' => 2985,
				'nome' => 'Cordisburgo',
				'uf' => 'MG',
				'cep2' => '3118908',
				'estado_cod' => 11,
				'cep' => '35780-000',
			),
			50 => 
			array (
				'id' => 2986,
				'nome' => 'Cordislândia',
				'uf' => 'MG',
				'cep2' => '3119005',
				'estado_cod' => 11,
				'cep' => '37498-000',
			),
			51 => 
			array (
				'id' => 2987,
				'nome' => 'Corinto',
				'uf' => 'MG',
				'cep2' => '3119104',
				'estado_cod' => 11,
				'cep' => '39200-000',
			),
			52 => 
			array (
				'id' => 2988,
				'nome' => 'Coroaci',
				'uf' => 'MG',
				'cep2' => '3119203',
				'estado_cod' => 11,
				'cep' => '39710-000',
			),
			53 => 
			array (
				'id' => 2989,
				'nome' => 'Coromandel',
				'uf' => 'MG',
				'cep2' => '3119302',
				'estado_cod' => 11,
				'cep' => '38550-000',
			),
			54 => 
			array (
				'id' => 2990,
				'nome' => 'Coronel Fabriciano',
				'uf' => 'MG',
				'cep2' => '3119401',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			55 => 
			array (
				'id' => 2991,
				'nome' => 'Coronel Murta',
				'uf' => 'MG',
				'cep2' => '3119500',
				'estado_cod' => 11,
				'cep' => '39635-000',
			),
			56 => 
			array (
				'id' => 2992,
				'nome' => 'Coronel Pacheco',
				'uf' => 'MG',
				'cep2' => '3119609',
				'estado_cod' => 11,
				'cep' => '36155-000',
			),
			57 => 
			array (
				'id' => 2993,
				'nome' => 'Coronel Xavier Chaves',
				'uf' => 'MG',
				'cep2' => '3119708',
				'estado_cod' => 11,
				'cep' => '36330-000',
			),
			58 => 
			array (
				'id' => 2994,
				'nome' => 'Córrego Danta',
				'uf' => 'MG',
				'cep2' => '3119807',
				'estado_cod' => 11,
				'cep' => '38990-000',
			),
			59 => 
			array (
				'id' => 2996,
				'nome' => 'Córrego do Bom Jesus',
				'uf' => 'MG',
				'cep2' => '3119906',
				'estado_cod' => 11,
				'cep' => '37605-000',
			),
			60 => 
			array (
				'id' => 2998,
				'nome' => 'Córrego Fundo',
				'uf' => 'MG',
				'cep2' => '3119955',
				'estado_cod' => 11,
				'cep' => '35578-000',
			),
			61 => 
			array (
				'id' => 2999,
				'nome' => 'Córrego Novo',
				'uf' => 'MG',
				'cep2' => '3120003',
				'estado_cod' => 11,
				'cep' => '35345-000',
			),
			62 => 
			array (
				'id' => 3006,
				'nome' => 'Couto de Magalhães de Minas',
				'uf' => 'MG',
				'cep2' => '3120102',
				'estado_cod' => 11,
				'cep' => '39188-000',
			),
			63 => 
			array (
				'id' => 3008,
				'nome' => 'Crisólita',
				'uf' => 'MG',
				'cep2' => '3120151',
				'estado_cod' => 11,
				'cep' => '39885-000',
			),
			64 => 
			array (
				'id' => 3010,
				'nome' => 'Cristais',
				'uf' => 'MG',
				'cep2' => '3120201',
				'estado_cod' => 11,
				'cep' => '37275-000',
			),
			65 => 
			array (
				'id' => 3011,
				'nome' => 'Cristália',
				'uf' => 'MG',
				'cep2' => '3120300',
				'estado_cod' => 11,
				'cep' => '39598-000',
			),
			66 => 
			array (
				'id' => 3012,
				'nome' => 'Cristiano Otoni',
				'uf' => 'MG',
				'cep2' => '3120409',
				'estado_cod' => 11,
				'cep' => '36426-000',
			),
			67 => 
			array (
				'id' => 3013,
				'nome' => 'Cristina',
				'uf' => 'MG',
				'cep2' => '3120508',
				'estado_cod' => 11,
				'cep' => '37476-000',
			),
			68 => 
			array (
				'id' => 3014,
				'nome' => 'Crucilândia',
				'uf' => 'MG',
				'cep2' => '3120607',
				'estado_cod' => 11,
				'cep' => '35520-000',
			),
			69 => 
			array (
				'id' => 3015,
				'nome' => 'Cruzeiro da Fortaleza',
				'uf' => 'MG',
				'cep2' => '3120706',
				'estado_cod' => 11,
				'cep' => '38735-000',
			),
			70 => 
			array (
				'id' => 3017,
				'nome' => 'Cruzília',
				'uf' => 'MG',
				'cep2' => '3120805',
				'estado_cod' => 11,
				'cep' => '37445-000',
			),
			71 => 
			array (
				'id' => 3020,
				'nome' => 'Cuparaque',
				'uf' => 'MG',
				'cep2' => '3120839',
				'estado_cod' => 11,
				'cep' => '35246-000',
			),
			72 => 
			array (
				'id' => 3022,
				'nome' => 'Curral de Dentro',
				'uf' => 'MG',
				'cep2' => '3120870',
				'estado_cod' => 11,
				'cep' => '39569-000',
			),
			73 => 
			array (
				'id' => 3023,
				'nome' => 'Curvelo',
				'uf' => 'MG',
				'cep2' => '3120904',
				'estado_cod' => 11,
				'cep' => '35790-000',
			),
			74 => 
			array (
				'id' => 3024,
				'nome' => 'Datas',
				'uf' => 'MG',
				'cep2' => '3121001',
				'estado_cod' => 11,
				'cep' => '39130-000',
			),
			75 => 
			array (
				'id' => 3025,
				'nome' => 'Delfim Moreira',
				'uf' => 'MG',
				'cep2' => '3121100',
				'estado_cod' => 11,
				'cep' => '37514-000',
			),
			76 => 
			array (
				'id' => 3026,
				'nome' => 'Delfinópolis',
				'uf' => 'MG',
				'cep2' => '3121209',
				'estado_cod' => 11,
				'cep' => '37910-000',
			),
			77 => 
			array (
				'id' => 3027,
				'nome' => 'Delta',
				'uf' => 'MG',
				'cep2' => '3121258',
				'estado_cod' => 11,
				'cep' => '38108-000',
			),
			78 => 
			array (
				'id' => 3030,
				'nome' => 'Descoberto',
				'uf' => 'MG',
				'cep2' => '3121308',
				'estado_cod' => 11,
				'cep' => '36690-000',
			),
			79 => 
			array (
				'id' => 3033,
				'nome' => 'Desterro de Entre Rios',
				'uf' => 'MG',
				'cep2' => '3121407',
				'estado_cod' => 11,
				'cep' => '35494-000',
			),
			80 => 
			array (
				'id' => 3034,
				'nome' => 'Desterro do Melo',
				'uf' => 'MG',
				'cep2' => '3121506',
				'estado_cod' => 11,
				'cep' => '36210-000',
			),
			81 => 
			array (
				'id' => 3036,
				'nome' => 'Diamantina',
				'uf' => 'MG',
				'cep2' => '3121605',
				'estado_cod' => 11,
				'cep' => '39100-000',
			),
			82 => 
			array (
				'id' => 3039,
				'nome' => 'Diogo de Vasconcelos',
				'uf' => 'MG',
				'cep2' => '3121704',
				'estado_cod' => 11,
				'cep' => '35437-000',
			),
			83 => 
			array (
				'id' => 3040,
				'nome' => 'Dionísio',
				'uf' => 'MG',
				'cep2' => '3121803',
				'estado_cod' => 11,
				'cep' => '35984-000',
			),
			84 => 
			array (
				'id' => 3041,
				'nome' => 'Divinésia',
				'uf' => 'MG',
				'cep2' => '3121902',
				'estado_cod' => 11,
				'cep' => '36546-000',
			),
			85 => 
			array (
				'id' => 3042,
				'nome' => 'Divino',
				'uf' => 'MG',
				'cep2' => '3122009',
				'estado_cod' => 11,
				'cep' => '36820-000',
			),
			86 => 
			array (
				'id' => 3043,
				'nome' => 'Divino das Laranjeiras',
				'uf' => 'MG',
				'cep2' => '3122108',
				'estado_cod' => 11,
				'cep' => '35265-000',
			),
			87 => 
			array (
				'id' => 3046,
				'nome' => 'Divinolândia de Minas',
				'uf' => 'MG',
				'cep2' => '3122207',
				'estado_cod' => 11,
				'cep' => '39735-000',
			),
			88 => 
			array (
				'id' => 3047,
				'nome' => 'Divinópolis',
				'uf' => 'MG',
				'cep2' => '3122306',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			89 => 
			array (
				'id' => 3048,
				'nome' => 'Divisa Alegre',
				'uf' => 'MG',
				'cep2' => '3122355',
				'estado_cod' => 11,
				'cep' => '39995-000',
			),
			90 => 
			array (
				'id' => 3049,
				'nome' => 'Divisa Nova',
				'uf' => 'MG',
				'cep2' => '3122405',
				'estado_cod' => 11,
				'cep' => '37134-000',
			),
			91 => 
			array (
				'id' => 3050,
				'nome' => 'Divisópolis',
				'uf' => 'MG',
				'cep2' => '3122454',
				'estado_cod' => 11,
				'cep' => '39912-000',
			),
			92 => 
			array (
				'id' => 3052,
				'nome' => 'Dom Bosco',
				'uf' => 'MG',
				'cep2' => '3122470',
				'estado_cod' => 11,
				'cep' => '38654-000',
			),
			93 => 
			array (
				'id' => 3053,
				'nome' => 'Dom Cavati',
				'uf' => 'MG',
				'cep2' => '3122504',
				'estado_cod' => 11,
				'cep' => '35148-000',
			),
			94 => 
			array (
				'id' => 3054,
				'nome' => 'Dom Joaquim',
				'uf' => 'MG',
				'cep2' => '3122603',
				'estado_cod' => 11,
				'cep' => '35865-000',
			),
			95 => 
			array (
				'id' => 3057,
				'nome' => 'Dom Silvério',
				'uf' => 'MG',
				'cep2' => '3122702',
				'estado_cod' => 11,
				'cep' => '35440-000',
			),
			96 => 
			array (
				'id' => 3058,
				'nome' => 'Dom Viçoso',
				'uf' => 'MG',
				'cep2' => '3122801',
				'estado_cod' => 11,
				'cep' => '37474-000',
			),
			97 => 
			array (
				'id' => 3059,
				'nome' => 'Dona Euzébia',
				'uf' => 'MG',
				'cep2' => '3122900',
				'estado_cod' => 11,
				'cep' => '36784-000',
			),
			98 => 
			array (
				'id' => 3061,
				'nome' => 'Dores de Campos',
				'uf' => 'MG',
				'cep2' => '3123007',
				'estado_cod' => 11,
				'cep' => '36213-000',
			),
			99 => 
			array (
				'id' => 3062,
				'nome' => 'Dores de Guanhães',
				'uf' => 'MG',
				'cep2' => '3123106',
				'estado_cod' => 11,
				'cep' => '35894-000',
			),
			100 => 
			array (
				'id' => 3063,
				'nome' => 'Dores do Indaiá',
				'uf' => 'MG',
				'cep2' => '3123205',
				'estado_cod' => 11,
				'cep' => '35610-000',
			),
			101 => 
			array (
				'id' => 3065,
				'nome' => 'Dores do Turvo',
				'uf' => 'MG',
				'cep2' => '3123304',
				'estado_cod' => 11,
				'cep' => '36513-000',
			),
			102 => 
			array (
				'id' => 3066,
				'nome' => 'Doresópolis',
				'uf' => 'MG',
				'cep2' => '3123403',
				'estado_cod' => 11,
				'cep' => '37926-000',
			),
			103 => 
			array (
				'id' => 3068,
				'nome' => 'Douradoquara',
				'uf' => 'MG',
				'cep2' => '3123502',
				'estado_cod' => 11,
				'cep' => '38530-000',
			),
			104 => 
			array (
				'id' => 3071,
				'nome' => 'Durandé',
				'uf' => 'MG',
				'cep2' => '3123528',
				'estado_cod' => 11,
				'cep' => '36974-000',
			),
			105 => 
			array (
				'id' => 3073,
				'nome' => 'Elói Mendes',
				'uf' => 'MG',
				'cep2' => '3123601',
				'estado_cod' => 11,
				'cep' => '37110-000',
			),
			106 => 
			array (
				'id' => 3075,
				'nome' => 'Engenheiro Caldas',
				'uf' => 'MG',
				'cep2' => '3123700',
				'estado_cod' => 11,
				'cep' => '35130-000',
			),
			107 => 
			array (
				'id' => 3077,
				'nome' => 'Engenheiro Navarro',
				'uf' => 'MG',
				'cep2' => '3123809',
				'estado_cod' => 11,
				'cep' => '39417-000',
			),
			108 => 
			array (
				'id' => 3081,
				'nome' => 'Entre Folhas',
				'uf' => 'MG',
				'cep2' => '3123858',
				'estado_cod' => 11,
				'cep' => '35324-000',
			),
			109 => 
			array (
				'id' => 3082,
				'nome' => 'Entre Rios de Minas',
				'uf' => 'MG',
				'cep2' => '3123908',
				'estado_cod' => 11,
				'cep' => '35490-000',
			),
			110 => 
			array (
				'id' => 3085,
				'nome' => 'Ervália',
				'uf' => 'MG',
				'cep2' => '3124005',
				'estado_cod' => 11,
				'cep' => '36555-000',
			),
			111 => 
			array (
				'id' => 3086,
				'nome' => 'Esmeraldas',
				'uf' => 'MG',
				'cep2' => '3124104',
				'estado_cod' => 11,
				'cep' => '35740-000',
			),
			112 => 
			array (
				'id' => 3088,
				'nome' => 'Espera Feliz',
				'uf' => 'MG',
				'cep2' => '3124203',
				'estado_cod' => 11,
				'cep' => '36830-000',
			),
			113 => 
			array (
				'id' => 3089,
				'nome' => 'Espinosa',
				'uf' => 'MG',
				'cep2' => '3124302',
				'estado_cod' => 11,
				'cep' => '39510-000',
			),
			114 => 
			array (
				'id' => 3090,
				'nome' => 'Espírito Santo do Dourado',
				'uf' => 'MG',
				'cep2' => '3124401',
				'estado_cod' => 11,
				'cep' => '37566-000',
			),
			115 => 
			array (
				'id' => 3093,
				'nome' => 'Estiva',
				'uf' => 'MG',
				'cep2' => '3124500',
				'estado_cod' => 11,
				'cep' => '37542-000',
			),
			116 => 
			array (
				'id' => 3095,
				'nome' => 'Estrela Dalva',
				'uf' => 'MG',
				'cep2' => '3124609',
				'estado_cod' => 11,
				'cep' => '36725-000',
			),
			117 => 
			array (
				'id' => 3097,
				'nome' => 'Estrela do Indaiá',
				'uf' => 'MG',
				'cep2' => '3124708',
				'estado_cod' => 11,
				'cep' => '35613-000',
			),
			118 => 
			array (
				'id' => 3098,
				'nome' => 'Estrela do Sul',
				'uf' => 'MG',
				'cep2' => '3124807',
				'estado_cod' => 11,
				'cep' => '38525-000',
			),
			119 => 
			array (
				'id' => 3099,
				'nome' => 'Eugenópolis',
				'uf' => 'MG',
				'cep2' => '3124906',
				'estado_cod' => 11,
				'cep' => '36855-000',
			),
			120 => 
			array (
				'id' => 3101,
				'nome' => 'Ewbank da Câmara',
				'uf' => 'MG',
				'cep2' => '3125002',
				'estado_cod' => 11,
				'cep' => '36108-000',
			),
			121 => 
			array (
				'id' => 3104,
				'nome' => 'Extrema',
				'uf' => 'MG',
				'cep2' => '3125101',
				'estado_cod' => 11,
				'cep' => '37640-000',
			),
			122 => 
			array (
				'id' => 3105,
				'nome' => 'Fama',
				'uf' => 'MG',
				'cep2' => '3125200',
				'estado_cod' => 11,
				'cep' => '37138-000',
			),
			123 => 
			array (
				'id' => 3106,
				'nome' => 'Faria Lemos',
				'uf' => 'MG',
				'cep2' => '3125309',
				'estado_cod' => 11,
				'cep' => '36840-000',
			),
			124 => 
			array (
				'id' => 3110,
				'nome' => 'Felício dos Santos',
				'uf' => 'MG',
				'cep2' => '3125408',
				'estado_cod' => 11,
				'cep' => '39180-000',
			),
			125 => 
			array (
				'id' => 3111,
				'nome' => 'Felisburgo',
				'uf' => 'MG',
				'cep2' => '3125606',
				'estado_cod' => 11,
				'cep' => '39895-000',
			),
			126 => 
			array (
				'id' => 3112,
				'nome' => 'Felixlândia',
				'uf' => 'MG',
				'cep2' => '3125705',
				'estado_cod' => 11,
				'cep' => '35794-000',
			),
			127 => 
			array (
				'id' => 3113,
				'nome' => 'Fernandes Tourinho',
				'uf' => 'MG',
				'cep2' => '3125804',
				'estado_cod' => 11,
				'cep' => '35135-000',
			),
			128 => 
			array (
				'id' => 3117,
				'nome' => 'Ferros',
				'uf' => 'MG',
				'cep2' => '3125903',
				'estado_cod' => 11,
				'cep' => '35800-000',
			),
			129 => 
			array (
				'id' => 3119,
				'nome' => 'Fervedouro',
				'uf' => 'MG',
				'cep2' => '3125952',
				'estado_cod' => 11,
				'cep' => '36815-000',
			),
			130 => 
			array (
				'id' => 3125,
				'nome' => 'Florestal',
				'uf' => 'MG',
				'cep2' => '3126000',
				'estado_cod' => 11,
				'cep' => '35690-000',
			),
			131 => 
			array (
				'id' => 3128,
				'nome' => 'Formiga',
				'uf' => 'MG',
				'cep2' => '3126109',
				'estado_cod' => 11,
				'cep' => '35570-000',
			),
			132 => 
			array (
				'id' => 3129,
				'nome' => 'Formoso',
				'uf' => 'MG',
				'cep2' => '3126208',
				'estado_cod' => 11,
				'cep' => '38690-000',
			),
			133 => 
			array (
				'id' => 3130,
				'nome' => 'Fortaleza de Minas',
				'uf' => 'MG',
				'cep2' => '3126307',
				'estado_cod' => 11,
				'cep' => '37905-000',
			),
			134 => 
			array (
				'id' => 3131,
				'nome' => 'Fortuna de Minas',
				'uf' => 'MG',
				'cep2' => '3126406',
				'estado_cod' => 11,
				'cep' => '35760-000',
			),
			135 => 
			array (
				'id' => 3132,
				'nome' => 'Francisco Badaró',
				'uf' => 'MG',
				'cep2' => '3126505',
				'estado_cod' => 11,
				'cep' => '39644-000',
			),
			136 => 
			array (
				'id' => 3133,
				'nome' => 'Francisco Dumont',
				'uf' => 'MG',
				'cep2' => '3126604',
				'estado_cod' => 11,
				'cep' => '39387-000',
			),
			137 => 
			array (
				'id' => 3134,
				'nome' => 'Francisco Sá',
				'uf' => 'MG',
				'cep2' => '3126703',
				'estado_cod' => 11,
				'cep' => '39580-000',
			),
			138 => 
			array (
				'id' => 3135,
				'nome' => 'Franciscópolis',
				'uf' => 'MG',
				'cep2' => '3126752',
				'estado_cod' => 11,
				'cep' => '39695-000',
			),
			139 => 
			array (
				'id' => 3137,
				'nome' => 'Frei Gaspar',
				'uf' => 'MG',
				'cep2' => '3126802',
				'estado_cod' => 11,
				'cep' => '39840-000',
			),
			140 => 
			array (
				'id' => 3138,
				'nome' => 'Frei Inocêncio',
				'uf' => 'MG',
				'cep2' => '3126901',
				'estado_cod' => 11,
				'cep' => '35112-000',
			),
			141 => 
			array (
				'id' => 3139,
				'nome' => 'Frei Lagonegro',
				'uf' => 'MG',
				'cep2' => '3126950',
				'estado_cod' => 11,
				'cep' => '39708-000',
			),
			142 => 
			array (
				'id' => 3143,
				'nome' => 'Fronteira',
				'uf' => 'MG',
				'cep2' => '3127008',
				'estado_cod' => 11,
				'cep' => '38230-000',
			),
			143 => 
			array (
				'id' => 3144,
				'nome' => 'Fronteira dos Vales',
				'uf' => 'MG',
				'cep2' => '3127057',
				'estado_cod' => 11,
				'cep' => '39870-000',
			),
			144 => 
			array (
				'id' => 3145,
				'nome' => 'Fruta de Leite',
				'uf' => 'MG',
				'cep2' => '3127073',
				'estado_cod' => 11,
				'cep' => '39558-000',
			),
			145 => 
			array (
				'id' => 3146,
				'nome' => 'Frutal',
				'uf' => 'MG',
				'cep2' => '3127107',
				'estado_cod' => 11,
				'cep' => '38200-000',
			),
			146 => 
			array (
				'id' => 3148,
				'nome' => 'Funilândia',
				'uf' => 'MG',
				'cep2' => '3127206',
				'estado_cod' => 11,
				'cep' => '35709-000',
			),
			147 => 
			array (
				'id' => 3153,
				'nome' => 'Galiléia',
				'uf' => 'MG',
				'cep2' => '3127305',
				'estado_cod' => 11,
				'cep' => '35250-000',
			),
			148 => 
			array (
				'id' => 3155,
				'nome' => 'Gameleiras',
				'uf' => 'MG',
				'cep2' => '3127339',
				'estado_cod' => 11,
				'cep' => '39505-000',
			),
			149 => 
			array (
				'id' => 3158,
				'nome' => 'Glaucilândia',
				'uf' => 'MG',
				'cep2' => '3127354',
				'estado_cod' => 11,
				'cep' => '39592-000',
			),
			150 => 
			array (
				'id' => 3161,
				'nome' => 'Goiabeira',
				'uf' => 'MG',
				'cep2' => '3127370',
				'estado_cod' => 11,
				'cep' => '35248-000',
			),
			151 => 
			array (
				'id' => 3162,
				'nome' => 'Goianá',
				'uf' => 'MG',
				'cep2' => '3127388',
				'estado_cod' => 11,
				'cep' => '36152-000',
			),
			152 => 
			array (
				'id' => 3164,
				'nome' => 'Gonçalves',
				'uf' => 'MG',
				'cep2' => '3127404',
				'estado_cod' => 11,
				'cep' => '37680-000',
			),
			153 => 
			array (
				'id' => 3165,
				'nome' => 'Gonzaga',
				'uf' => 'MG',
				'cep2' => '3127503',
				'estado_cod' => 11,
				'cep' => '39720-000',
			),
			154 => 
			array (
				'id' => 3168,
				'nome' => 'Gouveia',
				'uf' => 'MG',
				'cep2' => '',
				'estado_cod' => 11,
				'cep' => '39120-000',
			),
			155 => 
			array (
				'id' => 3169,
				'nome' => 'Governador Valadares',
				'uf' => 'MG',
				'cep2' => '3127701',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			156 => 
			array (
				'id' => 3172,
				'nome' => 'Grão Mogol',
				'uf' => 'MG',
				'cep2' => '3127800',
				'estado_cod' => 11,
				'cep' => '39570-000',
			),
			157 => 
			array (
				'id' => 3174,
				'nome' => 'Grupiara',
				'uf' => 'MG',
				'cep2' => '3127909',
				'estado_cod' => 11,
				'cep' => '38470-000',
			),
			158 => 
			array (
				'id' => 3177,
				'nome' => 'Guanhães',
				'uf' => 'MG',
				'cep2' => '3128006',
				'estado_cod' => 11,
				'cep' => '39740-000',
			),
			159 => 
			array (
				'id' => 3178,
				'nome' => 'Guapé',
				'uf' => 'MG',
				'cep2' => '3128105',
				'estado_cod' => 11,
				'cep' => '37177-000',
			),
			160 => 
			array (
				'id' => 3179,
				'nome' => 'Guaraciaba',
				'uf' => 'MG',
				'cep2' => '3128204',
				'estado_cod' => 11,
				'cep' => '35436-000',
			),
			161 => 
			array (
				'id' => 3180,
				'nome' => 'Guaraciama',
				'uf' => 'MG',
				'cep2' => '3128253',
				'estado_cod' => 11,
				'cep' => '39397-000',
			),
			162 => 
			array (
				'id' => 3181,
				'nome' => 'Guaranésia',
				'uf' => 'MG',
				'cep2' => '3128303',
				'estado_cod' => 11,
				'cep' => '37810-000',
			),
			163 => 
			array (
				'id' => 3182,
				'nome' => 'Guarani',
				'uf' => 'MG',
				'cep2' => '3128402',
				'estado_cod' => 11,
				'cep' => '36160-000',
			),
			164 => 
			array (
				'id' => 3184,
				'nome' => 'Guarará',
				'uf' => 'MG',
				'cep2' => '3128501',
				'estado_cod' => 11,
				'cep' => '36606-000',
			),
			165 => 
			array (
				'id' => 3187,
				'nome' => 'Guarda-Mor',
				'uf' => 'MG',
				'cep2' => '3128600',
				'estado_cod' => 11,
				'cep' => '38570-000',
			),
			166 => 
			array (
				'id' => 3190,
				'nome' => 'Guaxupé',
				'uf' => 'MG',
				'cep2' => '3128709',
				'estado_cod' => 11,
				'cep' => '37800-000',
			),
			167 => 
			array (
				'id' => 3191,
				'nome' => 'Guidoval',
				'uf' => 'MG',
				'cep2' => '3128808',
				'estado_cod' => 11,
				'cep' => '36515-000',
			),
			168 => 
			array (
				'id' => 3192,
				'nome' => 'Guimarânia',
				'uf' => 'MG',
				'cep2' => '3128907',
				'estado_cod' => 11,
				'cep' => '38730-000',
			),
			169 => 
			array (
				'id' => 3194,
				'nome' => 'Guiricema',
				'uf' => 'MG',
				'cep2' => '3129004',
				'estado_cod' => 11,
				'cep' => '36525-000',
			),
			170 => 
			array (
				'id' => 3195,
				'nome' => 'Gurinhatã',
				'uf' => 'MG',
				'cep2' => '3129103',
				'estado_cod' => 11,
				'cep' => '38310-000',
			),
			171 => 
			array (
				'id' => 3196,
				'nome' => 'Heliodora',
				'uf' => 'MG',
				'cep2' => '3129202',
				'estado_cod' => 11,
				'cep' => '37484-000',
			),
			172 => 
			array (
				'id' => 3200,
				'nome' => 'Iapu',
				'uf' => 'MG',
				'cep2' => '3129301',
				'estado_cod' => 11,
				'cep' => '35190-000',
			),
			173 => 
			array (
				'id' => 3201,
				'nome' => 'Ibertioga',
				'uf' => 'MG',
				'cep2' => '3129400',
				'estado_cod' => 11,
				'cep' => '36225-000',
			),
			174 => 
			array (
				'id' => 3202,
				'nome' => 'Ibiá',
				'uf' => 'MG',
				'cep2' => '3129509',
				'estado_cod' => 11,
				'cep' => '38950-000',
			),
			175 => 
			array (
				'id' => 3203,
				'nome' => 'Ibiaí',
				'uf' => 'MG',
				'cep2' => '3129608',
				'estado_cod' => 11,
				'cep' => '39350-000',
			),
			176 => 
			array (
				'id' => 3204,
				'nome' => 'Ibiracatu',
				'uf' => 'MG',
				'cep2' => '3129657',
				'estado_cod' => 11,
				'cep' => '39455-000',
			),
			177 => 
			array (
				'id' => 3205,
				'nome' => 'Ibiraci',
				'uf' => 'MG',
				'cep2' => '3129707',
				'estado_cod' => 11,
				'cep' => '37990-000',
			),
			178 => 
			array (
				'id' => 3206,
				'nome' => 'Ibirité',
				'uf' => 'MG',
				'cep2' => '3129806',
				'estado_cod' => 11,
				'cep' => '32400-000',
			),
			179 => 
			array (
				'id' => 3208,
				'nome' => 'Ibitiúra de Minas',
				'uf' => 'MG',
				'cep2' => '3129905',
				'estado_cod' => 11,
				'cep' => '37790-000',
			),
			180 => 
			array (
				'id' => 3209,
				'nome' => 'Ibituruna',
				'uf' => 'MG',
				'cep2' => '3130002',
				'estado_cod' => 11,
				'cep' => '37223-000',
			),
			181 => 
			array (
				'id' => 3210,
				'nome' => 'Icaraí de Minas',
				'uf' => 'MG',
				'cep2' => '3130051',
				'estado_cod' => 11,
				'cep' => '39318-000',
			),
			182 => 
			array (
				'id' => 3211,
				'nome' => 'Igarapé',
				'uf' => 'MG',
				'cep2' => '3130101',
				'estado_cod' => 11,
				'cep' => '32900-000',
			),
			183 => 
			array (
				'id' => 3212,
				'nome' => 'Igaratinga',
				'uf' => 'MG',
				'cep2' => '3130200',
				'estado_cod' => 11,
				'cep' => '35695-000',
			),
			184 => 
			array (
				'id' => 3213,
				'nome' => 'Iguatama',
				'uf' => 'MG',
				'cep2' => '3130309',
				'estado_cod' => 11,
				'cep' => '38910-000',
			),
			185 => 
			array (
				'id' => 3214,
				'nome' => 'Ijaci',
				'uf' => 'MG',
				'cep2' => '3130408',
				'estado_cod' => 11,
				'cep' => '37205-000',
			),
			186 => 
			array (
				'id' => 3216,
				'nome' => 'Ilicínea',
				'uf' => 'MG',
				'cep2' => '3130507',
				'estado_cod' => 11,
				'cep' => '37175-000',
			),
			187 => 
			array (
				'id' => 3217,
				'nome' => 'Imbé de Minas',
				'uf' => 'MG',
				'cep2' => '3130556',
				'estado_cod' => 11,
				'cep' => '35323-000',
			),
			188 => 
			array (
				'id' => 3218,
				'nome' => 'Inconfidentes',
				'uf' => 'MG',
				'cep2' => '3130606',
				'estado_cod' => 11,
				'cep' => '37576-000',
			),
			189 => 
			array (
				'id' => 3219,
				'nome' => 'Indaiabira',
				'uf' => 'MG',
				'cep2' => '3130655',
				'estado_cod' => 11,
				'cep' => '39536-000',
			),
			190 => 
			array (
				'id' => 3221,
				'nome' => 'Indianópolis',
				'uf' => 'MG',
				'cep2' => '3130705',
				'estado_cod' => 11,
				'cep' => '38490-000',
			),
			191 => 
			array (
				'id' => 3222,
				'nome' => 'Ingaí',
				'uf' => 'MG',
				'cep2' => '3130804',
				'estado_cod' => 11,
				'cep' => '37215-000',
			),
			192 => 
			array (
				'id' => 3224,
				'nome' => 'Inhapim',
				'uf' => 'MG',
				'cep2' => '3130903',
				'estado_cod' => 11,
				'cep' => '35330-000',
			),
			193 => 
			array (
				'id' => 3225,
				'nome' => 'Inhaúma',
				'uf' => 'MG',
				'cep2' => '3131000',
				'estado_cod' => 11,
				'cep' => '35710-000',
			),
			194 => 
			array (
				'id' => 3226,
				'nome' => 'Inimutaba',
				'uf' => 'MG',
				'cep2' => '3131109',
				'estado_cod' => 11,
				'cep' => '35796-000',
			),
			195 => 
			array (
				'id' => 3227,
				'nome' => 'Ipaba',
				'uf' => 'MG',
				'cep2' => '3131158',
				'estado_cod' => 11,
				'cep' => '35198-000',
			),
			196 => 
			array (
				'id' => 3228,
				'nome' => 'Ipanema',
				'uf' => 'MG',
				'cep2' => '3131208',
				'estado_cod' => 11,
				'cep' => '36950-000',
			),
			197 => 
			array (
				'id' => 3229,
				'nome' => 'Ipatinga',
				'uf' => 'MG',
				'cep2' => '3131307',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			198 => 
			array (
				'id' => 3230,
				'nome' => 'Ipiaçu',
				'uf' => 'MG',
				'cep2' => '3131406',
				'estado_cod' => 11,
				'cep' => '38350-000',
			),
			199 => 
			array (
				'id' => 3232,
				'nome' => 'Ipuiúna',
				'uf' => 'MG',
				'cep2' => '3131505',
				'estado_cod' => 11,
				'cep' => '37559-000',
			),
			200 => 
			array (
				'id' => 3233,
				'nome' => 'Iraí de Minas',
				'uf' => 'MG',
				'cep2' => '3131604',
				'estado_cod' => 11,
				'cep' => '38510-000',
			),
			201 => 
			array (
				'id' => 3234,
				'nome' => 'Itabira',
				'uf' => 'MG',
				'cep2' => '3131703',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			202 => 
			array (
				'id' => 3235,
				'nome' => 'Itabirinha',
				'uf' => 'MG',
				'cep2' => '',
				'estado_cod' => 11,
				'cep' => '35280-000',
			),
			203 => 
			array (
				'id' => 3236,
				'nome' => 'Itabirito',
				'uf' => 'MG',
				'cep2' => '3131901',
				'estado_cod' => 11,
				'cep' => '35450-000',
			),
			204 => 
			array (
				'id' => 3238,
				'nome' => 'Itacambira',
				'uf' => 'MG',
				'cep2' => '3132008',
				'estado_cod' => 11,
				'cep' => '39594-000',
			),
			205 => 
			array (
				'id' => 3239,
				'nome' => 'Itacarambi',
				'uf' => 'MG',
				'cep2' => '3132107',
				'estado_cod' => 11,
				'cep' => '39470-000',
			),
			206 => 
			array (
				'id' => 3242,
				'nome' => 'Itaguara',
				'uf' => 'MG',
				'cep2' => '3132206',
				'estado_cod' => 11,
				'cep' => '35514-000',
			),
			207 => 
			array (
				'id' => 3244,
				'nome' => 'Itaipé',
				'uf' => 'MG',
				'cep2' => '3132305',
				'estado_cod' => 11,
				'cep' => '39815-000',
			),
			208 => 
			array (
				'id' => 3245,
				'nome' => 'Itajubá',
				'uf' => 'MG',
				'cep2' => '3132404',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			209 => 
			array (
				'id' => 3247,
				'nome' => 'Itamarandiba',
				'uf' => 'MG',
				'cep2' => '3132503',
				'estado_cod' => 11,
				'cep' => '39670-000',
			),
			210 => 
			array (
				'id' => 3249,
				'nome' => 'Itamarati de Minas',
				'uf' => 'MG',
				'cep2' => '3132602',
				'estado_cod' => 11,
				'cep' => '36788-000',
			),
			211 => 
			array (
				'id' => 3250,
				'nome' => 'Itambacuri',
				'uf' => 'MG',
				'cep2' => '3132701',
				'estado_cod' => 11,
				'cep' => '39830-000',
			),
			212 => 
			array (
				'id' => 3251,
				'nome' => 'Itambé do Mato Dentro',
				'uf' => 'MG',
				'cep2' => '3132800',
				'estado_cod' => 11,
				'cep' => '35820-000',
			),
			213 => 
			array (
				'id' => 3253,
				'nome' => 'Itamogi',
				'uf' => 'MG',
				'cep2' => '3132909',
				'estado_cod' => 11,
				'cep' => '37955-000',
			),
			214 => 
			array (
				'id' => 3254,
				'nome' => 'Itamonte',
				'uf' => 'MG',
				'cep2' => '3133006',
				'estado_cod' => 11,
				'cep' => '37466-000',
			),
			215 => 
			array (
				'id' => 3256,
				'nome' => 'Itanhandu',
				'uf' => 'MG',
				'cep2' => '3133105',
				'estado_cod' => 11,
				'cep' => '37464-000',
			),
			216 => 
			array (
				'id' => 3257,
				'nome' => 'Itanhomi',
				'uf' => 'MG',
				'cep2' => '3133204',
				'estado_cod' => 11,
				'cep' => '35120-000',
			),
			217 => 
			array (
				'id' => 3258,
				'nome' => 'Itaobim',
				'uf' => 'MG',
				'cep2' => '3133303',
				'estado_cod' => 11,
				'cep' => '39625-000',
			),
			218 => 
			array (
				'id' => 3259,
				'nome' => 'Itapagipe',
				'uf' => 'MG',
				'cep2' => '3133402',
				'estado_cod' => 11,
				'cep' => '38240-000',
			),
			219 => 
			array (
				'id' => 3261,
				'nome' => 'Itapecerica',
				'uf' => 'MG',
				'cep2' => '3133501',
				'estado_cod' => 11,
				'cep' => '35550-000',
			),
			220 => 
			array (
				'id' => 3262,
				'nome' => 'Itapeva',
				'uf' => 'MG',
				'cep2' => '3133600',
				'estado_cod' => 11,
				'cep' => '37655-000',
			),
			221 => 
			array (
				'id' => 3265,
				'nome' => 'Itatiaiuçu',
				'uf' => 'MG',
				'cep2' => '3133709',
				'estado_cod' => 11,
				'cep' => '35685-000',
			),
			222 => 
			array (
				'id' => 3266,
				'nome' => 'Itaú de Minas',
				'uf' => 'MG',
				'cep2' => '3133758',
				'estado_cod' => 11,
				'cep' => '37975-000',
			),
			223 => 
			array (
				'id' => 3267,
				'nome' => 'Itaúna',
				'uf' => 'MG',
				'cep2' => '3133808',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			224 => 
			array (
				'id' => 3269,
				'nome' => 'Itaverava',
				'uf' => 'MG',
				'cep2' => '3133907',
				'estado_cod' => 11,
				'cep' => '36440-000',
			),
			225 => 
			array (
				'id' => 3271,
				'nome' => 'Itinga',
				'uf' => 'MG',
				'cep2' => '3134004',
				'estado_cod' => 11,
				'cep' => '39610-000',
			),
			226 => 
			array (
				'id' => 3273,
				'nome' => 'Itueta',
				'uf' => 'MG',
				'cep2' => '3134103',
				'estado_cod' => 11,
				'cep' => '35220-000',
			),
			227 => 
			array (
				'id' => 3275,
				'nome' => 'Ituiutaba',
				'uf' => 'MG',
				'cep2' => '3134202',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			228 => 
			array (
				'id' => 3276,
				'nome' => 'Itumirim',
				'uf' => 'MG',
				'cep2' => '3134301',
				'estado_cod' => 11,
				'cep' => '37210-000',
			),
			229 => 
			array (
				'id' => 3277,
				'nome' => 'Iturama',
				'uf' => 'MG',
				'cep2' => '3134400',
				'estado_cod' => 11,
				'cep' => '38280-000',
			),
			230 => 
			array (
				'id' => 3278,
				'nome' => 'Itutinga',
				'uf' => 'MG',
				'cep2' => '3134509',
				'estado_cod' => 11,
				'cep' => '36390-000',
			),
			231 => 
			array (
				'id' => 3279,
				'nome' => 'Jaboticatubas',
				'uf' => 'MG',
				'cep2' => '3134608',
				'estado_cod' => 11,
				'cep' => '35830-000',
			),
			232 => 
			array (
				'id' => 3282,
				'nome' => 'Jacinto',
				'uf' => 'MG',
				'cep2' => '3134707',
				'estado_cod' => 11,
				'cep' => '39930-000',
			),
			233 => 
			array (
				'id' => 3283,
				'nome' => 'Jacuí',
				'uf' => 'MG',
				'cep2' => '3134806',
				'estado_cod' => 11,
				'cep' => '37965-000',
			),
			234 => 
			array (
				'id' => 3284,
				'nome' => 'Jacutinga',
				'uf' => 'MG',
				'cep2' => '3134905',
				'estado_cod' => 11,
				'cep' => '37590-000',
			),
			235 => 
			array (
				'id' => 3285,
				'nome' => 'Jaguaraçu',
				'uf' => 'MG',
				'cep2' => '3135001',
				'estado_cod' => 11,
				'cep' => '35188-000',
			),
			236 => 
			array (
				'id' => 3288,
				'nome' => 'Jaíba',
				'uf' => 'MG',
				'cep2' => '3135050',
				'estado_cod' => 11,
				'cep' => '39508-000',
			),
			237 => 
			array (
				'id' => 3289,
				'nome' => 'Jampruca',
				'uf' => 'MG',
				'cep2' => '3135076',
				'estado_cod' => 11,
				'cep' => '39837-000',
			),
			238 => 
			array (
				'id' => 3290,
				'nome' => 'Janaúba',
				'uf' => 'MG',
				'cep2' => '3135100',
				'estado_cod' => 11,
				'cep' => '39440-000',
			),
			239 => 
			array (
				'id' => 3291,
				'nome' => 'Januária',
				'uf' => 'MG',
				'cep2' => '3135209',
				'estado_cod' => 11,
				'cep' => '39480-000',
			),
			240 => 
			array (
				'id' => 3292,
				'nome' => 'Japaraíba',
				'uf' => 'MG',
				'cep2' => '3135308',
				'estado_cod' => 11,
				'cep' => '35580-000',
			),
			241 => 
			array (
				'id' => 3293,
				'nome' => 'Japonvar',
				'uf' => 'MG',
				'cep2' => '3135357',
				'estado_cod' => 11,
				'cep' => '39335-000',
			),
			242 => 
			array (
				'id' => 3295,
				'nome' => 'Jeceaba',
				'uf' => 'MG',
				'cep2' => '3135407',
				'estado_cod' => 11,
				'cep' => '35498-000',
			),
			243 => 
			array (
				'id' => 3296,
				'nome' => 'Jenipapo de Minas',
				'uf' => 'MG',
				'cep2' => '3135456',
				'estado_cod' => 11,
				'cep' => '39645-000',
			),
			244 => 
			array (
				'id' => 3297,
				'nome' => 'Jequeri',
				'uf' => 'MG',
				'cep2' => '3135506',
				'estado_cod' => 11,
				'cep' => '35390-000',
			),
			245 => 
			array (
				'id' => 3298,
				'nome' => 'Jequitaí',
				'uf' => 'MG',
				'cep2' => '3135605',
				'estado_cod' => 11,
				'cep' => '39370-000',
			),
			246 => 
			array (
				'id' => 3299,
				'nome' => 'Jequitibá',
				'uf' => 'MG',
				'cep2' => '3135704',
				'estado_cod' => 11,
				'cep' => '35767-000',
			),
			247 => 
			array (
				'id' => 3300,
				'nome' => 'Jequitinhonha',
				'uf' => 'MG',
				'cep2' => '3135803',
				'estado_cod' => 11,
				'cep' => '39960-000',
			),
			248 => 
			array (
				'id' => 3301,
				'nome' => 'Jesuânia',
				'uf' => 'MG',
				'cep2' => '3135902',
				'estado_cod' => 11,
				'cep' => '37485-000',
			),
			249 => 
			array (
				'id' => 3302,
				'nome' => 'Joaíma',
				'uf' => 'MG',
				'cep2' => '3136009',
				'estado_cod' => 11,
				'cep' => '39890-000',
			),
			250 => 
			array (
				'id' => 3303,
				'nome' => 'Joanésia',
				'uf' => 'MG',
				'cep2' => '3136108',
				'estado_cod' => 11,
				'cep' => '35168-000',
			),
			251 => 
			array (
				'id' => 3304,
				'nome' => 'João Monlevade',
				'uf' => 'MG',
				'cep2' => '3136207',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			252 => 
			array (
				'id' => 3305,
				'nome' => 'João Pinheiro',
				'uf' => 'MG',
				'cep2' => '3136306',
				'estado_cod' => 11,
				'cep' => '38770-000',
			),
			253 => 
			array (
				'id' => 3306,
				'nome' => 'Joaquim Felício',
				'uf' => 'MG',
				'cep2' => '3136405',
				'estado_cod' => 11,
				'cep' => '39240-000',
			),
			254 => 
			array (
				'id' => 3307,
				'nome' => 'Jordânia',
				'uf' => 'MG',
				'cep2' => '3136504',
				'estado_cod' => 11,
				'cep' => '39920-000',
			),
			255 => 
			array (
				'id' => 3308,
				'nome' => 'José Gonçalves de Minas',
				'uf' => 'MG',
				'cep2' => '3136520',
				'estado_cod' => 11,
				'cep' => '39642-000',
			),
			256 => 
			array (
				'id' => 3309,
				'nome' => 'José Raydan',
				'uf' => 'MG',
				'cep2' => '3136553',
				'estado_cod' => 11,
				'cep' => '39775-000',
			),
			257 => 
			array (
				'id' => 3311,
				'nome' => 'Josenópolis',
				'uf' => 'MG',
				'cep2' => '3136579',
				'estado_cod' => 11,
				'cep' => '39575-000',
			),
			258 => 
			array (
				'id' => 3312,
				'nome' => 'Juatuba',
				'uf' => 'MG',
				'cep2' => '3136652',
				'estado_cod' => 11,
				'cep' => '35675-000',
			),
			259 => 
			array (
				'id' => 3315,
				'nome' => 'Juiz de Fora',
				'uf' => 'MG',
				'cep2' => '3136702',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			260 => 
			array (
				'id' => 3317,
				'nome' => 'Juramento',
				'uf' => 'MG',
				'cep2' => '3136801',
				'estado_cod' => 11,
				'cep' => '39590-000',
			),
			261 => 
			array (
				'id' => 3319,
				'nome' => 'Juruaia',
				'uf' => 'MG',
				'cep2' => '3136900',
				'estado_cod' => 11,
				'cep' => '37805-000',
			),
			262 => 
			array (
				'id' => 3322,
				'nome' => 'Juvenília',
				'uf' => 'MG',
				'cep2' => '3136959',
				'estado_cod' => 11,
				'cep' => '39467-000',
			),
			263 => 
			array (
				'id' => 3324,
				'nome' => 'Ladainha',
				'uf' => 'MG',
				'cep2' => '3137007',
				'estado_cod' => 11,
				'cep' => '39825-000',
			),
			264 => 
			array (
				'id' => 3325,
				'nome' => 'Lagamar',
				'uf' => 'MG',
				'cep2' => '3137106',
				'estado_cod' => 11,
				'cep' => '38785-000',
			),
			265 => 
			array (
				'id' => 3327,
				'nome' => 'Lagoa da Prata',
				'uf' => 'MG',
				'cep2' => '3137205',
				'estado_cod' => 11,
				'cep' => '35590-000',
			),
			266 => 
			array (
				'id' => 3328,
				'nome' => 'Lagoa dos Patos',
				'uf' => 'MG',
				'cep2' => '3137304',
				'estado_cod' => 11,
				'cep' => '39360-000',
			),
			267 => 
			array (
				'id' => 3329,
				'nome' => 'Lagoa Dourada',
				'uf' => 'MG',
				'cep2' => '3137403',
				'estado_cod' => 11,
				'cep' => '36345-000',
			),
			268 => 
			array (
				'id' => 3330,
				'nome' => 'Lagoa Formosa',
				'uf' => 'MG',
				'cep2' => '3137502',
				'estado_cod' => 11,
				'cep' => '38720-000',
			),
			269 => 
			array (
				'id' => 3331,
				'nome' => 'Lagoa Grande',
				'uf' => 'MG',
				'cep2' => '3137536',
				'estado_cod' => 11,
				'cep' => '38755-000',
			),
			270 => 
			array (
				'id' => 3332,
				'nome' => 'Lagoa Santa',
				'uf' => 'MG',
				'cep2' => '3137601',
				'estado_cod' => 11,
				'cep' => '33400-000',
			),
			271 => 
			array (
				'id' => 3333,
				'nome' => 'Lajinha',
				'uf' => 'MG',
				'cep2' => '3137700',
				'estado_cod' => 11,
				'cep' => '36980-000',
			),
			272 => 
			array (
				'id' => 3334,
				'nome' => 'Lambari',
				'uf' => 'MG',
				'cep2' => '3137809',
				'estado_cod' => 11,
				'cep' => '37480-000',
			),
			273 => 
			array (
				'id' => 3335,
				'nome' => 'Lamim',
				'uf' => 'MG',
				'cep2' => '3137908',
				'estado_cod' => 11,
				'cep' => '36455-000',
			),
			274 => 
			array (
				'id' => 3338,
				'nome' => 'Laranjal',
				'uf' => 'MG',
				'cep2' => '3138005',
				'estado_cod' => 11,
				'cep' => '36760-000',
			),
			275 => 
			array (
				'id' => 3340,
				'nome' => 'Lassance',
				'uf' => 'MG',
				'cep2' => '3138104',
				'estado_cod' => 11,
				'cep' => '39250-000',
			),
			276 => 
			array (
				'id' => 3341,
				'nome' => 'Lavras',
				'uf' => 'MG',
				'cep2' => '3138203',
				'estado_cod' => 11,
				'cep' => '37200-000',
			),
			277 => 
			array (
				'id' => 3342,
				'nome' => 'Leandro Ferreira',
				'uf' => 'MG',
				'cep2' => '3138302',
				'estado_cod' => 11,
				'cep' => '35657-000',
			),
			278 => 
			array (
				'id' => 3343,
				'nome' => 'Leme do Prado',
				'uf' => 'MG',
				'cep2' => '3138351',
				'estado_cod' => 11,
				'cep' => '39655-000',
			),
			279 => 
			array (
				'id' => 3344,
				'nome' => 'Leopoldina',
				'uf' => 'MG',
				'cep2' => '3138401',
				'estado_cod' => 11,
				'cep' => '36700-000',
			),
			280 => 
			array (
				'id' => 3346,
				'nome' => 'Liberdade',
				'uf' => 'MG',
				'cep2' => '3138500',
				'estado_cod' => 11,
				'cep' => '37350-000',
			),
			281 => 
			array (
				'id' => 3347,
				'nome' => 'Lima Duarte',
				'uf' => 'MG',
				'cep2' => '3138609',
				'estado_cod' => 11,
				'cep' => '36140-000',
			),
			282 => 
			array (
				'id' => 3348,
				'nome' => 'Limeira do Oeste',
				'uf' => 'MG',
				'cep2' => '3138625',
				'estado_cod' => 11,
				'cep' => '38295-000',
			),
			283 => 
			array (
				'id' => 3351,
				'nome' => 'Lontra',
				'uf' => 'MG',
				'cep2' => '3138658',
				'estado_cod' => 11,
				'cep' => '39437-000',
			),
			284 => 
			array (
				'id' => 3354,
				'nome' => 'Luisburgo',
				'uf' => 'MG',
				'cep2' => '3138674',
				'estado_cod' => 11,
				'cep' => '36923-000',
			),
			285 => 
			array (
				'id' => 3355,
				'nome' => 'Luislândia',
				'uf' => 'MG',
				'cep2' => '3138682',
				'estado_cod' => 11,
				'cep' => '39336-000',
			),
			286 => 
			array (
				'id' => 3358,
				'nome' => 'Luminárias',
				'uf' => 'MG',
				'cep2' => '3138708',
				'estado_cod' => 11,
				'cep' => '37240-000',
			),
			287 => 
			array (
				'id' => 3360,
				'nome' => 'Luz',
				'uf' => 'MG',
				'cep2' => '3138807',
				'estado_cod' => 11,
				'cep' => '35595-000',
			),
			288 => 
			array (
				'id' => 3362,
				'nome' => 'Machacalis',
				'uf' => 'MG',
				'cep2' => '3138906',
				'estado_cod' => 11,
				'cep' => '39873-000',
			),
			289 => 
			array (
				'id' => 3363,
				'nome' => 'Machado',
				'uf' => 'MG',
				'cep2' => '3139003',
				'estado_cod' => 11,
				'cep' => '37750-000',
			),
			290 => 
			array (
				'id' => 3366,
				'nome' => 'Madre de Deus de Minas',
				'uf' => 'MG',
				'cep2' => '3139102',
				'estado_cod' => 11,
				'cep' => '37305-000',
			),
			291 => 
			array (
				'id' => 3370,
				'nome' => 'Malacacheta',
				'uf' => 'MG',
				'cep2' => '3139201',
				'estado_cod' => 11,
				'cep' => '39690-000',
			),
			292 => 
			array (
				'id' => 3371,
				'nome' => 'Mamonas',
				'uf' => 'MG',
				'cep2' => '3139250',
				'estado_cod' => 11,
				'cep' => '39516-000',
			),
			293 => 
			array (
				'id' => 3372,
				'nome' => 'Manga',
				'uf' => 'MG',
				'cep2' => '3139300',
				'estado_cod' => 11,
				'cep' => '39460-000',
			),
			294 => 
			array (
				'id' => 3373,
				'nome' => 'Manhuaçu',
				'uf' => 'MG',
				'cep2' => '3139409',
				'estado_cod' => 11,
				'cep' => '36900-000',
			),
			295 => 
			array (
				'id' => 3374,
				'nome' => 'Manhumirim',
				'uf' => 'MG',
				'cep2' => '3139508',
				'estado_cod' => 11,
				'cep' => '36970-000',
			),
			296 => 
			array (
				'id' => 3375,
				'nome' => 'Mantena',
				'uf' => 'MG',
				'cep2' => '3139607',
				'estado_cod' => 11,
				'cep' => '35290-000',
			),
			297 => 
			array (
				'id' => 3378,
				'nome' => 'Mar de Espanha',
				'uf' => 'MG',
				'cep2' => '3139805',
				'estado_cod' => 11,
				'cep' => '36640-000',
			),
			298 => 
			array (
				'id' => 3380,
				'nome' => 'Maravilhas',
				'uf' => 'MG',
				'cep2' => '3139706',
				'estado_cod' => 11,
				'cep' => '35666-000',
			),
			299 => 
			array (
				'id' => 3381,
				'nome' => 'Maria da Fé',
				'uf' => 'MG',
				'cep2' => '3139904',
				'estado_cod' => 11,
				'cep' => '37517-000',
			),
			300 => 
			array (
				'id' => 3382,
				'nome' => 'Mariana',
				'uf' => 'MG',
				'cep2' => '3140001',
				'estado_cod' => 11,
				'cep' => '35420-000',
			),
			301 => 
			array (
				'id' => 3383,
				'nome' => 'Marilac',
				'uf' => 'MG',
				'cep2' => '3140100',
				'estado_cod' => 11,
				'cep' => '35115-000',
			),
			302 => 
			array (
				'id' => 3385,
				'nome' => 'Mário Campos',
				'uf' => 'MG',
				'cep2' => '3140159',
				'estado_cod' => 11,
				'cep' => '32470-000',
			),
			303 => 
			array (
				'id' => 3386,
				'nome' => 'Maripá de Minas',
				'uf' => 'MG',
				'cep2' => '3140209',
				'estado_cod' => 11,
				'cep' => '36608-000',
			),
			304 => 
			array (
				'id' => 3387,
				'nome' => 'Marliéria',
				'uf' => 'MG',
				'cep2' => '3140308',
				'estado_cod' => 11,
				'cep' => '35185-000',
			),
			305 => 
			array (
				'id' => 3388,
				'nome' => 'Marmelópolis',
				'uf' => 'MG',
				'cep2' => '3140407',
				'estado_cod' => 11,
				'cep' => '37516-000',
			),
			306 => 
			array (
				'id' => 3390,
				'nome' => 'Martinho Campos',
				'uf' => 'MG',
				'cep2' => '3140506',
				'estado_cod' => 11,
				'cep' => '35606-000',
			),
			307 => 
			array (
				'id' => 3392,
				'nome' => 'Martins Soares',
				'uf' => 'MG',
				'cep2' => '3140530',
				'estado_cod' => 11,
				'cep' => '36972-000',
			),
			308 => 
			array (
				'id' => 3393,
				'nome' => 'Mata Verde',
				'uf' => 'MG',
				'cep2' => '3140555',
				'estado_cod' => 11,
				'cep' => '39915-000',
			),
			309 => 
			array (
				'id' => 3394,
				'nome' => 'Materlândia',
				'uf' => 'MG',
				'cep2' => '3140605',
				'estado_cod' => 11,
				'cep' => '39755-000',
			),
			310 => 
			array (
				'id' => 3395,
				'nome' => 'Mateus Leme',
				'uf' => 'MG',
				'cep2' => '3140704',
				'estado_cod' => 11,
				'cep' => '35670-000',
			),
			311 => 
			array (
				'id' => 3396,
				'nome' => 'Mathias Lobato',
				'uf' => 'MG',
				'cep2' => '3171501',
				'estado_cod' => 11,
				'cep' => '35110-000',
			),
			312 => 
			array (
				'id' => 3397,
				'nome' => 'Matias Barbosa',
				'uf' => 'MG',
				'cep2' => '3140803',
				'estado_cod' => 11,
				'cep' => '36120-000',
			),
			313 => 
			array (
				'id' => 3398,
				'nome' => 'Matias Cardoso',
				'uf' => 'MG',
				'cep2' => '3140852',
				'estado_cod' => 11,
				'cep' => '39478-000',
			),
			314 => 
			array (
				'id' => 3399,
				'nome' => 'Matipó',
				'uf' => 'MG',
				'cep2' => '3140902',
				'estado_cod' => 11,
				'cep' => '35367-000',
			),
			315 => 
			array (
				'id' => 3400,
				'nome' => 'Mato Verde',
				'uf' => 'MG',
				'cep2' => '3141009',
				'estado_cod' => 11,
				'cep' => '39527-000',
			),
			316 => 
			array (
				'id' => 3401,
				'nome' => 'Matozinhos',
				'uf' => 'MG',
				'cep2' => '3141108',
				'estado_cod' => 11,
				'cep' => '35720-000',
			),
			317 => 
			array (
				'id' => 3402,
				'nome' => 'Matutina',
				'uf' => 'MG',
				'cep2' => '3141207',
				'estado_cod' => 11,
				'cep' => '38870-000',
			),
			318 => 
			array (
				'id' => 3403,
				'nome' => 'Medeiros',
				'uf' => 'MG',
				'cep2' => '3141306',
				'estado_cod' => 11,
				'cep' => '38930-000',
			),
			319 => 
			array (
				'id' => 3404,
				'nome' => 'Medina',
				'uf' => 'MG',
				'cep2' => '3141405',
				'estado_cod' => 11,
				'cep' => '39620-000',
			),
			320 => 
			array (
				'id' => 3407,
				'nome' => 'Mendes Pimentel',
				'uf' => 'MG',
				'cep2' => '3141504',
				'estado_cod' => 11,
				'cep' => '35270-000',
			),
			321 => 
			array (
				'id' => 3409,
				'nome' => 'Mercês',
				'uf' => 'MG',
				'cep2' => '3141603',
				'estado_cod' => 11,
				'cep' => '36190-000',
			),
			322 => 
			array (
				'id' => 3411,
				'nome' => 'Mesquita',
				'uf' => 'MG',
				'cep2' => '3141702',
				'estado_cod' => 11,
				'cep' => '35166-000',
			),
			323 => 
			array (
				'id' => 3416,
				'nome' => 'Minas Novas',
				'uf' => 'MG',
				'cep2' => '3141801',
				'estado_cod' => 11,
				'cep' => '39650-000',
			),
			324 => 
			array (
				'id' => 3417,
				'nome' => 'Minduri',
				'uf' => 'MG',
				'cep2' => '3141900',
				'estado_cod' => 11,
				'cep' => '37447-000',
			),
			325 => 
			array (
				'id' => 3418,
				'nome' => 'Mirabela',
				'uf' => 'MG',
				'cep2' => '3142007',
				'estado_cod' => 11,
				'cep' => '39420-000',
			),
			326 => 
			array (
				'id' => 3419,
				'nome' => 'Miradouro',
				'uf' => 'MG',
				'cep2' => '3142106',
				'estado_cod' => 11,
				'cep' => '36893-000',
			),
			327 => 
			array (
				'id' => 3421,
				'nome' => 'Miraí',
				'uf' => 'MG',
				'cep2' => '3142205',
				'estado_cod' => 11,
				'cep' => '36790-000',
			),
			328 => 
			array (
				'id' => 3425,
				'nome' => 'Miravânia',
				'uf' => 'MG',
				'cep2' => '3142254',
				'estado_cod' => 11,
				'cep' => '39465-000',
			),
			329 => 
			array (
				'id' => 3429,
				'nome' => 'Moeda',
				'uf' => 'MG',
				'cep2' => '3142304',
				'estado_cod' => 11,
				'cep' => '35470-000',
			),
			330 => 
			array (
				'id' => 3430,
				'nome' => 'Moema',
				'uf' => 'MG',
				'cep2' => '3142403',
				'estado_cod' => 11,
				'cep' => '35604-000',
			),
			331 => 
			array (
				'id' => 3432,
				'nome' => 'Monjolos',
				'uf' => 'MG',
				'cep2' => '3142502',
				'estado_cod' => 11,
				'cep' => '39215-000',
			),
			332 => 
			array (
				'id' => 3436,
				'nome' => 'Monsenhor Paulo',
				'uf' => 'MG',
				'cep2' => '3142601',
				'estado_cod' => 11,
				'cep' => '37405-000',
			),
			333 => 
			array (
				'id' => 3437,
				'nome' => 'Montalvânia',
				'uf' => 'MG',
				'cep2' => '3142700',
				'estado_cod' => 11,
				'cep' => '39495-000',
			),
			334 => 
			array (
				'id' => 3438,
				'nome' => 'Monte Alegre de Minas',
				'uf' => 'MG',
				'cep2' => '3142809',
				'estado_cod' => 11,
				'cep' => '38420-000',
			),
			335 => 
			array (
				'id' => 3439,
				'nome' => 'Monte Azul',
				'uf' => 'MG',
				'cep2' => '3142908',
				'estado_cod' => 11,
				'cep' => '39500-000',
			),
			336 => 
			array (
				'id' => 3440,
				'nome' => 'Monte Belo',
				'uf' => 'MG',
				'cep2' => '3143005',
				'estado_cod' => 11,
				'cep' => '37115-000',
			),
			337 => 
			array (
				'id' => 3441,
				'nome' => 'Monte Carmelo',
				'uf' => 'MG',
				'cep2' => '3143104',
				'estado_cod' => 11,
				'cep' => '38500-000',
			),
			338 => 
			array (
				'id' => 3443,
				'nome' => 'Monte Formoso',
				'uf' => 'MG',
				'cep2' => '3143153',
				'estado_cod' => 11,
				'cep' => '39893-000',
			),
			339 => 
			array (
				'id' => 3445,
				'nome' => 'Monte Santo de Minas',
				'uf' => 'MG',
				'cep2' => '3143203',
				'estado_cod' => 11,
				'cep' => '37958-000',
			),
			340 => 
			array (
				'id' => 3446,
				'nome' => 'Monte Sião',
				'uf' => 'MG',
				'cep2' => '3143401',
				'estado_cod' => 11,
				'cep' => '37580-000',
			),
			341 => 
			array (
				'id' => 3448,
				'nome' => 'Montes Claros',
				'uf' => 'MG',
				'cep2' => '3143302',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			342 => 
			array (
				'id' => 3449,
				'nome' => 'Montezuma',
				'uf' => 'MG',
				'cep2' => '3143450',
				'estado_cod' => 11,
				'cep' => '39547-000',
			),
			343 => 
			array (
				'id' => 3450,
				'nome' => 'Morada Nova de Minas',
				'uf' => 'MG',
				'cep2' => '3143500',
				'estado_cod' => 11,
				'cep' => '35628-000',
			),
			344 => 
			array (
				'id' => 3452,
				'nome' => 'Morro da Garça',
				'uf' => 'MG',
				'cep2' => '3143609',
				'estado_cod' => 11,
				'cep' => '35798-000',
			),
			345 => 
			array (
				'id' => 3454,
				'nome' => 'Morro do Pilar',
				'uf' => 'MG',
				'cep2' => '3143708',
				'estado_cod' => 11,
				'cep' => '35875-000',
			),
			346 => 
			array (
				'id' => 3458,
				'nome' => 'Munhoz',
				'uf' => 'MG',
				'cep2' => '3143807',
				'estado_cod' => 11,
				'cep' => '37620-000',
			),
			347 => 
			array (
				'id' => 3460,
				'nome' => 'Muriaé',
				'uf' => 'MG',
				'cep2' => '3143906',
				'estado_cod' => 11,
				'cep' => '36880-000',
			),
			348 => 
			array (
				'id' => 3461,
				'nome' => 'Mutum',
				'uf' => 'MG',
				'cep2' => '3144003',
				'estado_cod' => 11,
				'cep' => '36955-000',
			),
			349 => 
			array (
				'id' => 3462,
				'nome' => 'Muzambinho',
				'uf' => 'MG',
				'cep2' => '3144102',
				'estado_cod' => 11,
				'cep' => '37890-000',
			),
			350 => 
			array (
				'id' => 3463,
				'nome' => 'Nacip Raydan',
				'uf' => 'MG',
				'cep2' => '3144201',
				'estado_cod' => 11,
				'cep' => '39718-000',
			),
			351 => 
			array (
				'id' => 3464,
				'nome' => 'Nanuque',
				'uf' => 'MG',
				'cep2' => '3144300',
				'estado_cod' => 11,
				'cep' => '39860-000',
			),
			352 => 
			array (
				'id' => 3465,
				'nome' => 'Naque',
				'uf' => 'MG',
				'cep2' => '3144359',
				'estado_cod' => 11,
				'cep' => '35157-000',
			),
			353 => 
			array (
				'id' => 3467,
				'nome' => 'Natalândia',
				'uf' => 'MG',
				'cep2' => '3144375',
				'estado_cod' => 11,
				'cep' => '38658-000',
			),
			354 => 
			array (
				'id' => 3468,
				'nome' => 'Natércia',
				'uf' => 'MG',
				'cep2' => '3144409',
				'estado_cod' => 11,
				'cep' => '37524-000',
			),
			355 => 
			array (
				'id' => 3470,
				'nome' => 'Nazareno',
				'uf' => 'MG',
				'cep2' => '3144508',
				'estado_cod' => 11,
				'cep' => '36370-000',
			),
			356 => 
			array (
				'id' => 3473,
				'nome' => 'Nepomuceno',
				'uf' => 'MG',
				'cep2' => '3144607',
				'estado_cod' => 11,
				'cep' => '37250-000',
			),
			357 => 
			array (
				'id' => 3476,
				'nome' => 'Ninheira',
				'uf' => 'MG',
				'cep2' => '3144656',
				'estado_cod' => 11,
				'cep' => '39553-000',
			),
			358 => 
			array (
				'id' => 3477,
				'nome' => 'Nova Belém',
				'uf' => 'MG',
				'cep2' => '3144672',
				'estado_cod' => 11,
				'cep' => '35298-000',
			),
			359 => 
			array (
				'id' => 3478,
				'nome' => 'Nova Era',
				'uf' => 'MG',
				'cep2' => '3144706',
				'estado_cod' => 11,
				'cep' => '35920-000',
			),
			360 => 
			array (
				'id' => 3480,
				'nome' => 'Nova Lima',
				'uf' => 'MG',
				'cep2' => '3144805',
				'estado_cod' => 11,
				'cep' => '34000-000',
			),
			361 => 
			array (
				'id' => 3482,
				'nome' => 'Nova Módica',
				'uf' => 'MG',
				'cep2' => '3144904',
				'estado_cod' => 11,
				'cep' => '35113-000',
			),
			362 => 
			array (
				'id' => 3483,
				'nome' => 'Nova Ponte',
				'uf' => 'MG',
				'cep2' => '3145000',
				'estado_cod' => 11,
				'cep' => '38160-000',
			),
			363 => 
			array (
				'id' => 3484,
				'nome' => 'Nova Porteirinha',
				'uf' => 'MG',
				'cep2' => '3145059',
				'estado_cod' => 11,
				'cep' => '39525-000',
			),
			364 => 
			array (
				'id' => 3485,
				'nome' => 'Nova Resende',
				'uf' => 'MG',
				'cep2' => '3145109',
				'estado_cod' => 11,
				'cep' => '37860-000',
			),
			365 => 
			array (
				'id' => 3486,
				'nome' => 'Nova Serrana',
				'uf' => 'MG',
				'cep2' => '3145208',
				'estado_cod' => 11,
				'cep' => '35519-000',
			),
			366 => 
			array (
				'id' => 3487,
				'nome' => 'Nova União',
				'uf' => 'MG',
				'cep2' => '3136603',
				'estado_cod' => 11,
				'cep' => '34990-000',
			),
			367 => 
			array (
				'id' => 3489,
				'nome' => 'Novo Cruzeiro',
				'uf' => 'MG',
				'cep2' => '3145307',
				'estado_cod' => 11,
				'cep' => '39820-000',
			),
			368 => 
			array (
				'id' => 3491,
				'nome' => 'Novo Oriente de Minas',
				'uf' => 'MG',
				'cep2' => '3145356',
				'estado_cod' => 11,
				'cep' => '39817-000',
			),
			369 => 
			array (
				'id' => 3492,
				'nome' => 'Novorizonte',
				'uf' => 'MG',
				'cep2' => '3145372',
				'estado_cod' => 11,
				'cep' => '39568-000',
			),
			370 => 
			array (
				'id' => 3494,
				'nome' => 'Olaria',
				'uf' => 'MG',
				'cep2' => '3145406',
				'estado_cod' => 11,
				'cep' => '36145-000',
			),
			371 => 
			array (
				'id' => 3497,
				'nome' => 'Olhos D\'Água',
				'uf' => 'MG',
				'cep2' => '',
				'estado_cod' => 11,
				'cep' => '39398-000',
			),
			372 => 
			array (
				'id' => 3499,
				'nome' => 'Olímpio Noronha',
				'uf' => 'MG',
				'cep2' => '3145505',
				'estado_cod' => 11,
				'cep' => '37488-000',
			),
			373 => 
			array (
				'id' => 3500,
				'nome' => 'Oliveira',
				'uf' => 'MG',
				'cep2' => '3145604',
				'estado_cod' => 11,
				'cep' => '35540-000',
			),
			374 => 
			array (
				'id' => 3501,
				'nome' => 'Oliveira Fortes',
				'uf' => 'MG',
				'cep2' => '3145703',
				'estado_cod' => 11,
				'cep' => '36250-000',
			),
			375 => 
			array (
				'id' => 3502,
				'nome' => 'Onça de Pitangui',
				'uf' => 'MG',
				'cep2' => '3145802',
				'estado_cod' => 11,
				'cep' => '35655-000',
			),
			376 => 
			array (
				'id' => 3503,
				'nome' => 'Oratórios',
				'uf' => 'MG',
				'cep2' => '3145851',
				'estado_cod' => 11,
				'cep' => '35439-000',
			),
			377 => 
			array (
				'id' => 3504,
				'nome' => 'Orizânia',
				'uf' => 'MG',
				'cep2' => '3145877',
				'estado_cod' => 11,
				'cep' => '36828-000',
			),
			378 => 
			array (
				'id' => 3505,
				'nome' => 'Ouro Branco',
				'uf' => 'MG',
				'cep2' => '3145901',
				'estado_cod' => 11,
				'cep' => '36420-000',
			),
			379 => 
			array (
				'id' => 3506,
				'nome' => 'Ouro Fino',
				'uf' => 'MG',
				'cep2' => '3146008',
				'estado_cod' => 11,
				'cep' => '37570-000',
			),
			380 => 
			array (
				'id' => 3507,
				'nome' => 'Ouro Preto',
				'uf' => 'MG',
				'cep2' => '3146107',
				'estado_cod' => 11,
				'cep' => '35400-000',
			),
			381 => 
			array (
				'id' => 3508,
				'nome' => 'Ouro Verde de Minas',
				'uf' => 'MG',
				'cep2' => '3146206',
				'estado_cod' => 11,
				'cep' => '39855-000',
			),
			382 => 
			array (
				'id' => 3511,
				'nome' => 'Padre Carvalho',
				'uf' => 'MG',
				'cep2' => '3146255',
				'estado_cod' => 11,
				'cep' => '39573-000',
			),
			383 => 
			array (
				'id' => 3516,
				'nome' => 'Padre Paraíso',
				'uf' => 'MG',
				'cep2' => '3146305',
				'estado_cod' => 11,
				'cep' => '39818-000',
			),
			384 => 
			array (
				'id' => 3519,
				'nome' => 'Pai Pedro',
				'uf' => 'MG',
				'cep2' => '3146552',
				'estado_cod' => 11,
				'cep' => '39517-000',
			),
			385 => 
			array (
				'id' => 3520,
				'nome' => 'Paineiras',
				'uf' => 'MG',
				'cep2' => '3146404',
				'estado_cod' => 11,
				'cep' => '35622-000',
			),
			386 => 
			array (
				'id' => 3521,
				'nome' => 'Pains',
				'uf' => 'MG',
				'cep2' => '3146503',
				'estado_cod' => 11,
				'cep' => '35582-000',
			),
			387 => 
			array (
				'id' => 3523,
				'nome' => 'Paiva',
				'uf' => 'MG',
				'cep2' => '3146602',
				'estado_cod' => 11,
				'cep' => '36195-000',
			),
			388 => 
			array (
				'id' => 3524,
				'nome' => 'Palma',
				'uf' => 'MG',
				'cep2' => '3146701',
				'estado_cod' => 11,
				'cep' => '36750-000',
			),
			389 => 
			array (
				'id' => 3528,
				'nome' => 'Palmópolis',
				'uf' => 'MG',
				'cep2' => '3146750',
				'estado_cod' => 11,
				'cep' => '39945-000',
			),
			390 => 
			array (
				'id' => 3530,
				'nome' => 'Papagaios',
				'uf' => 'MG',
				'cep2' => '3146909',
				'estado_cod' => 11,
				'cep' => '35669-000',
			),
			391 => 
			array (
				'id' => 3531,
				'nome' => 'Pará de Minas',
				'uf' => 'MG',
				'cep2' => '3147105',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			392 => 
			array (
				'id' => 3532,
				'nome' => 'Paracatu',
				'uf' => 'MG',
				'cep2' => '3147006',
				'estado_cod' => 11,
				'cep' => '38600-000',
			),
			393 => 
			array (
				'id' => 3533,
				'nome' => 'Paraguaçu',
				'uf' => 'MG',
				'cep2' => '3147204',
				'estado_cod' => 11,
				'cep' => '37120-000',
			),
			394 => 
			array (
				'id' => 3536,
				'nome' => 'Paraisópolis',
				'uf' => 'MG',
				'cep2' => '3147303',
				'estado_cod' => 11,
				'cep' => '37660-000',
			),
			395 => 
			array (
				'id' => 3537,
				'nome' => 'Paraopeba',
				'uf' => 'MG',
				'cep2' => '3147402',
				'estado_cod' => 11,
				'cep' => '35774-000',
			),
			396 => 
			array (
				'id' => 3542,
				'nome' => 'Passa Quatro',
				'uf' => 'MG',
				'cep2' => '3147600',
				'estado_cod' => 11,
				'cep' => '37460-000',
			),
			397 => 
			array (
				'id' => 3543,
				'nome' => 'Passa Tempo',
				'uf' => 'MG',
				'cep2' => '3147709',
				'estado_cod' => 11,
				'cep' => '35537-000',
			),
			398 => 
			array (
				'id' => 3544,
				'nome' => 'Passa Vinte',
				'uf' => 'MG',
				'cep2' => '3147808',
				'estado_cod' => 11,
				'cep' => '37330-000',
			),
			399 => 
			array (
				'id' => 3545,
				'nome' => 'Passabém',
				'uf' => 'MG',
				'cep2' => '3147501',
				'estado_cod' => 11,
				'cep' => '35810-000',
			),
			400 => 
			array (
				'id' => 3547,
				'nome' => 'Passos',
				'uf' => 'MG',
				'cep2' => '3147907',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			401 => 
			array (
				'id' => 3548,
				'nome' => 'Patis',
				'uf' => 'MG',
				'cep2' => '3147956',
				'estado_cod' => 11,
				'cep' => '39425-000',
			),
			402 => 
			array (
				'id' => 3549,
				'nome' => 'Patos de Minas',
				'uf' => 'MG',
				'cep2' => '3148004',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			403 => 
			array (
				'id' => 3551,
				'nome' => 'Patrocínio',
				'uf' => 'MG',
				'cep2' => '3148103',
				'estado_cod' => 11,
				'cep' => '38740-000',
			),
			404 => 
			array (
				'id' => 3552,
				'nome' => 'Patrocínio do Muriaé',
				'uf' => 'MG',
				'cep2' => '3148202',
				'estado_cod' => 11,
				'cep' => '36860-000',
			),
			405 => 
			array (
				'id' => 3553,
				'nome' => 'Paula Cândido',
				'uf' => 'MG',
				'cep2' => '3148301',
				'estado_cod' => 11,
				'cep' => '36544-000',
			),
			406 => 
			array (
				'id' => 3555,
				'nome' => 'Paulistas',
				'uf' => 'MG',
				'cep2' => '3148400',
				'estado_cod' => 11,
				'cep' => '39765-000',
			),
			407 => 
			array (
				'id' => 3556,
				'nome' => 'Pavão',
				'uf' => 'MG',
				'cep2' => '3148509',
				'estado_cod' => 11,
				'cep' => '39814-000',
			),
			408 => 
			array (
				'id' => 3558,
				'nome' => 'Peçanha',
				'uf' => 'MG',
				'cep2' => '3148608',
				'estado_cod' => 11,
				'cep' => '39700-000',
			),
			409 => 
			array (
				'id' => 3559,
				'nome' => 'Pedra Azul',
				'uf' => 'MG',
				'cep2' => '3148707',
				'estado_cod' => 11,
				'cep' => '39970-000',
			),
			410 => 
			array (
				'id' => 3560,
				'nome' => 'Pedra Bonita',
				'uf' => 'MG',
				'cep2' => '3148756',
				'estado_cod' => 11,
				'cep' => '35364-000',
			),
			411 => 
			array (
				'id' => 3562,
				'nome' => 'Pedra do Anta',
				'uf' => 'MG',
				'cep2' => '3148806',
				'estado_cod' => 11,
				'cep' => '36585-000',
			),
			412 => 
			array (
				'id' => 3563,
				'nome' => 'Pedra do Indaiá',
				'uf' => 'MG',
				'cep2' => '3148905',
				'estado_cod' => 11,
				'cep' => '35565-000',
			),
			413 => 
			array (
				'id' => 3565,
				'nome' => 'Pedra Dourada',
				'uf' => 'MG',
				'cep2' => '3149002',
				'estado_cod' => 11,
				'cep' => '36847-000',
			),
			414 => 
			array (
				'id' => 3568,
				'nome' => 'Pedralva',
				'uf' => 'MG',
				'cep2' => '3149101',
				'estado_cod' => 11,
				'cep' => '37520-000',
			),
			415 => 
			array (
				'id' => 3569,
				'nome' => 'Pedras de Maria da Cruz',
				'uf' => 'MG',
				'cep2' => '3149150',
				'estado_cod' => 11,
				'cep' => '39492-000',
			),
			416 => 
			array (
				'id' => 3570,
				'nome' => 'Pedrinópolis',
				'uf' => 'MG',
				'cep2' => '3149200',
				'estado_cod' => 11,
				'cep' => '38178-000',
			),
			417 => 
			array (
				'id' => 3571,
				'nome' => 'Pedro Leopoldo',
				'uf' => 'MG',
				'cep2' => '3149309',
				'estado_cod' => 11,
				'cep' => '33600-000',
			),
			418 => 
			array (
				'id' => 3573,
				'nome' => 'Pedro Teixeira',
				'uf' => 'MG',
				'cep2' => '3149408',
				'estado_cod' => 11,
				'cep' => '36148-000',
			),
			419 => 
			array (
				'id' => 3581,
				'nome' => 'Pequeri',
				'uf' => 'MG',
				'cep2' => '3149507',
				'estado_cod' => 11,
				'cep' => '36610-000',
			),
			420 => 
			array (
				'id' => 3582,
				'nome' => 'Pequi',
				'uf' => 'MG',
				'cep2' => '3149606',
				'estado_cod' => 11,
				'cep' => '35667-000',
			),
			421 => 
			array (
				'id' => 3583,
				'nome' => 'Perdigão',
				'uf' => 'MG',
				'cep2' => '3149705',
				'estado_cod' => 11,
				'cep' => '35515-000',
			),
			422 => 
			array (
				'id' => 3585,
				'nome' => 'Perdizes',
				'uf' => 'MG',
				'cep2' => '3149804',
				'estado_cod' => 11,
				'cep' => '38170-000',
			),
			423 => 
			array (
				'id' => 3586,
				'nome' => 'Perdões',
				'uf' => 'MG',
				'cep2' => '3149903',
				'estado_cod' => 11,
				'cep' => '37260-000',
			),
			424 => 
			array (
				'id' => 3588,
				'nome' => 'Periquito',
				'uf' => 'MG',
				'cep2' => '3149952',
				'estado_cod' => 11,
				'cep' => '35156-000',
			),
			425 => 
			array (
				'id' => 3590,
				'nome' => 'Pescador',
				'uf' => 'MG',
				'cep2' => '3150000',
				'estado_cod' => 11,
				'cep' => '35114-000',
			),
			426 => 
			array (
				'id' => 3594,
				'nome' => 'Piau',
				'uf' => 'MG',
				'cep2' => '3150109',
				'estado_cod' => 11,
				'cep' => '36157-000',
			),
			427 => 
			array (
				'id' => 3596,
				'nome' => 'Piedade de Caratinga',
				'uf' => 'MG',
				'cep2' => '3150158',
				'estado_cod' => 11,
				'cep' => '35325-000',
			),
			428 => 
			array (
				'id' => 3597,
				'nome' => 'Piedade de Ponte Nova',
				'uf' => 'MG',
				'cep2' => '3150208',
				'estado_cod' => 11,
				'cep' => '35382-000',
			),
			429 => 
			array (
				'id' => 3599,
				'nome' => 'Piedade do Rio Grande',
				'uf' => 'MG',
				'cep2' => '3150307',
				'estado_cod' => 11,
				'cep' => '36227-000',
			),
			430 => 
			array (
				'id' => 3600,
				'nome' => 'Piedade dos Gerais',
				'uf' => 'MG',
				'cep2' => '3150406',
				'estado_cod' => 11,
				'cep' => '35526-000',
			),
			431 => 
			array (
				'id' => 3602,
				'nome' => 'Pimenta',
				'uf' => 'MG',
				'cep2' => '3150505',
				'estado_cod' => 11,
				'cep' => '35585-000',
			),
			432 => 
			array (
				'id' => 3604,
				'nome' => 'Pingo-D\'Água',
				'uf' => 'MG',
				'cep2' => '',
				'estado_cod' => 11,
				'cep' => '35348-000',
			),
			433 => 
			array (
				'id' => 3608,
				'nome' => 'Pintópolis',
				'uf' => 'MG',
				'cep2' => '3150570',
				'estado_cod' => 11,
				'cep' => '39317-000',
			),
			434 => 
			array (
				'id' => 3611,
				'nome' => 'Piracema',
				'uf' => 'MG',
				'cep2' => '3150604',
				'estado_cod' => 11,
				'cep' => '35536-000',
			),
			435 => 
			array (
				'id' => 3612,
				'nome' => 'Pirajuba',
				'uf' => 'MG',
				'cep2' => '3150703',
				'estado_cod' => 11,
				'cep' => '38210-000',
			),
			436 => 
			array (
				'id' => 3613,
				'nome' => 'Piranga',
				'uf' => 'MG',
				'cep2' => '3150802',
				'estado_cod' => 11,
				'cep' => '36480-000',
			),
			437 => 
			array (
				'id' => 3614,
				'nome' => 'Piranguçu',
				'uf' => 'MG',
				'cep2' => '3150901',
				'estado_cod' => 11,
				'cep' => '37511-000',
			),
			438 => 
			array (
				'id' => 3615,
				'nome' => 'Piranguinho',
				'uf' => 'MG',
				'cep2' => '3151008',
				'estado_cod' => 11,
				'cep' => '37508-000',
			),
			439 => 
			array (
				'id' => 3618,
				'nome' => 'Pirapetinga',
				'uf' => 'MG',
				'cep2' => '3151107',
				'estado_cod' => 11,
				'cep' => '36730-000',
			),
			440 => 
			array (
				'id' => 3619,
				'nome' => 'Pirapora',
				'uf' => 'MG',
				'cep2' => '3151206',
				'estado_cod' => 11,
				'cep' => '39270-000',
			),
			441 => 
			array (
				'id' => 3620,
				'nome' => 'Piraúba',
				'uf' => 'MG',
				'cep2' => '3151305',
				'estado_cod' => 11,
				'cep' => '36170-000',
			),
			442 => 
			array (
				'id' => 3623,
				'nome' => 'Pitangui',
				'uf' => 'MG',
				'cep2' => '3151404',
				'estado_cod' => 11,
				'cep' => '35650-000',
			),
			443 => 
			array (
				'id' => 3625,
				'nome' => 'Piumhi',
				'uf' => 'MG',
				'cep2' => '3151503',
				'estado_cod' => 11,
				'cep' => '37925-000',
			),
			444 => 
			array (
				'id' => 3627,
				'nome' => 'Planura',
				'uf' => 'MG',
				'cep2' => '3151602',
				'estado_cod' => 11,
				'cep' => '38220-000',
			),
			445 => 
			array (
				'id' => 3630,
				'nome' => 'Poço Fundo',
				'uf' => 'MG',
				'cep2' => '3151701',
				'estado_cod' => 11,
				'cep' => '37757-000',
			),
			446 => 
			array (
				'id' => 3632,
				'nome' => 'Poços de Caldas',
				'uf' => 'MG',
				'cep2' => '3151800',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			447 => 
			array (
				'id' => 3633,
				'nome' => 'Pocrane',
				'uf' => 'MG',
				'cep2' => '3151909',
				'estado_cod' => 11,
				'cep' => '36960-000',
			),
			448 => 
			array (
				'id' => 3634,
				'nome' => 'Pompéu',
				'uf' => 'MG',
				'cep2' => '3152006',
				'estado_cod' => 11,
				'cep' => '35640-000',
			),
			449 => 
			array (
				'id' => 3641,
				'nome' => 'Ponte Nova',
				'uf' => 'MG',
				'cep2' => '3152105',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			450 => 
			array (
				'id' => 3644,
				'nome' => 'Ponto Chique',
				'uf' => 'MG',
				'cep2' => '3152131',
				'estado_cod' => 11,
				'cep' => '39328-000',
			),
			451 => 
			array (
				'id' => 3647,
				'nome' => 'Ponto dos Volantes',
				'uf' => 'MG',
				'cep2' => '3152170',
				'estado_cod' => 11,
				'cep' => '39615-000',
			),
			452 => 
			array (
				'id' => 3648,
				'nome' => 'Porteirinha',
				'uf' => 'MG',
				'cep2' => '3152204',
				'estado_cod' => 11,
				'cep' => '39520-000',
			),
			453 => 
			array (
				'id' => 3652,
				'nome' => 'Porto Firme',
				'uf' => 'MG',
				'cep2' => '3152303',
				'estado_cod' => 11,
				'cep' => '36576-000',
			),
			454 => 
			array (
				'id' => 3653,
				'nome' => 'Poté',
				'uf' => 'MG',
				'cep2' => '3152402',
				'estado_cod' => 11,
				'cep' => '39827-000',
			),
			455 => 
			array (
				'id' => 3654,
				'nome' => 'Pouso Alegre',
				'uf' => 'MG',
				'cep2' => '3152501',
				'estado_cod' => 11,
				'cep' => '37550-000',
			),
			456 => 
			array (
				'id' => 3655,
				'nome' => 'Pouso Alto',
				'uf' => 'MG',
				'cep2' => '3152600',
				'estado_cod' => 11,
				'cep' => '37468-000',
			),
			457 => 
			array (
				'id' => 3656,
				'nome' => 'Prados',
				'uf' => 'MG',
				'cep2' => '3152709',
				'estado_cod' => 11,
				'cep' => '36320-000',
			),
			458 => 
			array (
				'id' => 3657,
				'nome' => 'Prata',
				'uf' => 'MG',
				'cep2' => '3152808',
				'estado_cod' => 11,
				'cep' => '38140-000',
			),
			459 => 
			array (
				'id' => 3658,
				'nome' => 'Pratápolis',
				'uf' => 'MG',
				'cep2' => '3152907',
				'estado_cod' => 11,
				'cep' => '37970-000',
			),
			460 => 
			array (
				'id' => 3659,
				'nome' => 'Pratinha',
				'uf' => 'MG',
				'cep2' => '3153004',
				'estado_cod' => 11,
				'cep' => '38960-000',
			),
			461 => 
			array (
				'id' => 3660,
				'nome' => 'Presidente Bernardes',
				'uf' => 'MG',
				'cep2' => '3153103',
				'estado_cod' => 11,
				'cep' => '36475-000',
			),
			462 => 
			array (
				'id' => 3661,
				'nome' => 'Presidente Juscelino',
				'uf' => 'MG',
				'cep2' => '3153202',
				'estado_cod' => 11,
				'cep' => '35797-000',
			),
			463 => 
			array (
				'id' => 3662,
				'nome' => 'Presidente Kubitschek',
				'uf' => 'MG',
				'cep2' => '3153301',
				'estado_cod' => 11,
				'cep' => '39135-000',
			),
			464 => 
			array (
				'id' => 3663,
				'nome' => 'Presidente Olegário',
				'uf' => 'MG',
				'cep2' => '3153400',
				'estado_cod' => 11,
				'cep' => '38750-000',
			),
			465 => 
			array (
				'id' => 3667,
				'nome' => 'Prudente de Morais',
				'uf' => 'MG',
				'cep2' => '3153608',
				'estado_cod' => 11,
				'cep' => '35715-000',
			),
			466 => 
			array (
				'id' => 3670,
				'nome' => 'Quartel Geral',
				'uf' => 'MG',
				'cep2' => '3153707',
				'estado_cod' => 11,
				'cep' => '35625-000',
			),
			467 => 
			array (
				'id' => 3673,
				'nome' => 'Queluzito',
				'uf' => 'MG',
				'cep2' => '',
				'estado_cod' => 11,
				'cep' => '36424-000',
			),
			468 => 
			array (
				'id' => 3677,
				'nome' => 'Raposos',
				'uf' => 'MG',
				'cep2' => '3153905',
				'estado_cod' => 11,
				'cep' => '34400-000',
			),
			469 => 
			array (
				'id' => 3678,
				'nome' => 'Raul Soares',
				'uf' => 'MG',
				'cep2' => '3154002',
				'estado_cod' => 11,
				'cep' => '35350-000',
			),
			470 => 
			array (
				'id' => 3681,
				'nome' => 'Recreio',
				'uf' => 'MG',
				'cep2' => '3154101',
				'estado_cod' => 11,
				'cep' => '36740-000',
			),
			471 => 
			array (
				'id' => 3682,
				'nome' => 'Reduto',
				'uf' => 'MG',
				'cep2' => '3154150',
				'estado_cod' => 11,
				'cep' => '36920-000',
			),
			472 => 
			array (
				'id' => 3683,
				'nome' => 'Resende Costa',
				'uf' => 'MG',
				'cep2' => '3154200',
				'estado_cod' => 11,
				'cep' => '36340-000',
			),
			473 => 
			array (
				'id' => 3684,
				'nome' => 'Resplendor',
				'uf' => 'MG',
				'cep2' => '3154309',
				'estado_cod' => 11,
				'cep' => '35230-000',
			),
			474 => 
			array (
				'id' => 3685,
				'nome' => 'Ressaquinha',
				'uf' => 'MG',
				'cep2' => '3154408',
				'estado_cod' => 11,
				'cep' => '36270-000',
			),
			475 => 
			array (
				'id' => 3686,
				'nome' => 'Riachinho',
				'uf' => 'MG',
				'cep2' => '3154457',
				'estado_cod' => 11,
				'cep' => '38640-000',
			),
			476 => 
			array (
				'id' => 3688,
				'nome' => 'Riacho dos Machados',
				'uf' => 'MG',
				'cep2' => '3154507',
				'estado_cod' => 11,
				'cep' => '39529-000',
			),
			477 => 
			array (
				'id' => 3689,
				'nome' => 'Ribeirão das Neves',
				'uf' => 'MG',
				'cep2' => '3154606',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			478 => 
			array (
				'id' => 3691,
				'nome' => 'Ribeirão Vermelho',
				'uf' => 'MG',
				'cep2' => '3154705',
				'estado_cod' => 11,
				'cep' => '37264-000',
			),
			479 => 
			array (
				'id' => 3694,
				'nome' => 'Rio Acima',
				'uf' => 'MG',
				'cep2' => '3154804',
				'estado_cod' => 11,
				'cep' => '34300-000',
			),
			480 => 
			array (
				'id' => 3695,
				'nome' => 'Rio Casca',
				'uf' => 'MG',
				'cep2' => '3154903',
				'estado_cod' => 11,
				'cep' => '35370-000',
			),
			481 => 
			array (
				'id' => 3697,
				'nome' => 'Rio do Prado',
				'uf' => 'MG',
				'cep2' => '3155108',
				'estado_cod' => 11,
				'cep' => '39940-000',
			),
			482 => 
			array (
				'id' => 3698,
				'nome' => 'Rio Doce',
				'uf' => 'MG',
				'cep2' => '3155009',
				'estado_cod' => 11,
				'cep' => '35442-000',
			),
			483 => 
			array (
				'id' => 3699,
				'nome' => 'Rio Espera',
				'uf' => 'MG',
				'cep2' => '3155207',
				'estado_cod' => 11,
				'cep' => '36460-000',
			),
			484 => 
			array (
				'id' => 3700,
				'nome' => 'Rio Manso',
				'uf' => 'MG',
				'cep2' => '3155306',
				'estado_cod' => 11,
				'cep' => '35525-000',
			),
			485 => 
			array (
				'id' => 3702,
				'nome' => 'Rio Novo',
				'uf' => 'MG',
				'cep2' => '3155405',
				'estado_cod' => 11,
				'cep' => '36150-000',
			),
			486 => 
			array (
				'id' => 3703,
				'nome' => 'Rio Paranaíba',
				'uf' => 'MG',
				'cep2' => '3155504',
				'estado_cod' => 11,
				'cep' => '38810-000',
			),
			487 => 
			array (
				'id' => 3704,
				'nome' => 'Rio Pardo de Minas',
				'uf' => 'MG',
				'cep2' => '3155603',
				'estado_cod' => 11,
				'cep' => '39530-000',
			),
			488 => 
			array (
				'id' => 3705,
				'nome' => 'Rio Piracicaba',
				'uf' => 'MG',
				'cep2' => '3155702',
				'estado_cod' => 11,
				'cep' => '35940-000',
			),
			489 => 
			array (
				'id' => 3706,
				'nome' => 'Rio Pomba',
				'uf' => 'MG',
				'cep2' => '3155801',
				'estado_cod' => 11,
				'cep' => '36180-000',
			),
			490 => 
			array (
				'id' => 3708,
				'nome' => 'Rio Preto',
				'uf' => 'MG',
				'cep2' => '3155900',
				'estado_cod' => 11,
				'cep' => '36130-000',
			),
			491 => 
			array (
				'id' => 3709,
				'nome' => 'Rio Vermelho',
				'uf' => 'MG',
				'cep2' => '3156007',
				'estado_cod' => 11,
				'cep' => '39170-000',
			),
			492 => 
			array (
				'id' => 3710,
				'nome' => 'Ritápolis',
				'uf' => 'MG',
				'cep2' => '3156106',
				'estado_cod' => 11,
				'cep' => '36335-000',
			),
			493 => 
			array (
				'id' => 3713,
				'nome' => 'Rochedo de Minas',
				'uf' => 'MG',
				'cep2' => '3156205',
				'estado_cod' => 11,
				'cep' => '36604-000',
			),
			494 => 
			array (
				'id' => 3715,
				'nome' => 'Rodeiro',
				'uf' => 'MG',
				'cep2' => '3156304',
				'estado_cod' => 11,
				'cep' => '36510-000',
			),
			495 => 
			array (
				'id' => 3717,
				'nome' => 'Romaria',
				'uf' => 'MG',
				'cep2' => '3156403',
				'estado_cod' => 11,
				'cep' => '38520-000',
			),
			496 => 
			array (
				'id' => 3718,
				'nome' => 'Rosário da Limeira',
				'uf' => 'MG',
				'cep2' => '3156452',
				'estado_cod' => 11,
				'cep' => '36878-000',
			),
			497 => 
			array (
				'id' => 3722,
				'nome' => 'Rubelita',
				'uf' => 'MG',
				'cep2' => '3156502',
				'estado_cod' => 11,
				'cep' => '39565-000',
			),
			498 => 
			array (
				'id' => 3723,
				'nome' => 'Rubim',
				'uf' => 'MG',
				'cep2' => '3156601',
				'estado_cod' => 11,
				'cep' => '39950-000',
			),
			499 => 
			array (
				'id' => 3724,
				'nome' => 'Sabará',
				'uf' => 'MG',
				'cep2' => '3156700',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 3725,
				'nome' => 'Sabinópolis',
				'uf' => 'MG',
				'cep2' => '3156809',
				'estado_cod' => 11,
				'cep' => '39750-000',
			),
			1 => 
			array (
				'id' => 3726,
				'nome' => 'Sacramento',
				'uf' => 'MG',
				'cep2' => '3156908',
				'estado_cod' => 11,
				'cep' => '38190-000',
			),
			2 => 
			array (
				'id' => 3727,
				'nome' => 'Salinas',
				'uf' => 'MG',
				'cep2' => '3157005',
				'estado_cod' => 11,
				'cep' => '39560-000',
			),
			3 => 
			array (
				'id' => 3729,
				'nome' => 'Salto da Divisa',
				'uf' => 'MG',
				'cep2' => '3157104',
				'estado_cod' => 11,
				'cep' => '39925-000',
			),
			4 => 
			array (
				'id' => 3731,
				'nome' => 'Santa Bárbara',
				'uf' => 'MG',
				'cep2' => '3157203',
				'estado_cod' => 11,
				'cep' => '35960-000',
			),
			5 => 
			array (
				'id' => 3732,
				'nome' => 'Santa Bárbara do Leste',
				'uf' => 'MG',
				'cep2' => '3157252',
				'estado_cod' => 11,
				'cep' => '35328-000',
			),
			6 => 
			array (
				'id' => 3733,
				'nome' => 'Santa Bárbara do Monte Verde',
				'uf' => 'MG',
				'cep2' => '3157278',
				'estado_cod' => 11,
				'cep' => '36132-000',
			),
			7 => 
			array (
				'id' => 3734,
				'nome' => 'Santa Bárbara do Tugúrio',
				'uf' => 'MG',
				'cep2' => '3157302',
				'estado_cod' => 11,
				'cep' => '36215-000',
			),
			8 => 
			array (
				'id' => 3737,
				'nome' => 'Santa Cruz de Minas',
				'uf' => 'MG',
				'cep2' => '3157336',
				'estado_cod' => 11,
				'cep' => '36328-000',
			),
			9 => 
			array (
				'id' => 3738,
				'nome' => 'Santa Cruz de Salinas',
				'uf' => 'MG',
				'cep2' => '3157377',
				'estado_cod' => 11,
				'cep' => '39563-000',
			),
			10 => 
			array (
				'id' => 3739,
				'nome' => 'Santa Cruz do Escalvado',
				'uf' => 'MG',
				'cep2' => '3157401',
				'estado_cod' => 11,
				'cep' => '35384-000',
			),
			11 => 
			array (
				'id' => 3743,
				'nome' => 'Santa Efigênia de Minas',
				'uf' => 'MG',
				'cep2' => '3157500',
				'estado_cod' => 11,
				'cep' => '39725-000',
			),
			12 => 
			array (
				'id' => 3744,
				'nome' => 'Santa Fé de Minas',
				'uf' => 'MG',
				'cep2' => '3157609',
				'estado_cod' => 11,
				'cep' => '39295-000',
			),
			13 => 
			array (
				'id' => 3746,
				'nome' => 'Santa Helena de Minas',
				'uf' => 'MG',
				'cep2' => '3157658',
				'estado_cod' => 11,
				'cep' => '39874-000',
			),
			14 => 
			array (
				'id' => 3748,
				'nome' => 'Santa Juliana',
				'uf' => 'MG',
				'cep2' => '3157708',
				'estado_cod' => 11,
				'cep' => '38175-000',
			),
			15 => 
			array (
				'id' => 3749,
				'nome' => 'Santa Luzia',
				'uf' => 'MG',
				'cep2' => '3157807',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			16 => 
			array (
				'id' => 3751,
				'nome' => 'Santa Margarida',
				'uf' => 'MG',
				'cep2' => '3157906',
				'estado_cod' => 11,
				'cep' => '36910-000',
			),
			17 => 
			array (
				'id' => 3752,
				'nome' => 'Santa Maria de Itabira',
				'uf' => 'MG',
				'cep2' => '3158003',
				'estado_cod' => 11,
				'cep' => '35910-000',
			),
			18 => 
			array (
				'id' => 3753,
				'nome' => 'Santa Maria do Salto',
				'uf' => 'MG',
				'cep2' => '3158102',
				'estado_cod' => 11,
				'cep' => '39928-000',
			),
			19 => 
			array (
				'id' => 3754,
				'nome' => 'Santa Maria do Suaçuí',
				'uf' => 'MG',
				'cep2' => '3158201',
				'estado_cod' => 11,
				'cep' => '39780-000',
			),
			20 => 
			array (
				'id' => 3756,
				'nome' => 'Santa Rita de Caldas',
				'uf' => 'MG',
				'cep2' => '3159209',
				'estado_cod' => 11,
				'cep' => '37775-000',
			),
			21 => 
			array (
				'id' => 3757,
				'nome' => 'Santa Rita de Jacutinga',
				'uf' => 'MG',
				'cep2' => '3159308',
				'estado_cod' => 11,
				'cep' => '36135-000',
			),
			22 => 
			array (
				'id' => 3758,
				'nome' => 'Santa Rita de Minas',
				'uf' => 'MG',
				'cep2' => '3159357',
				'estado_cod' => 11,
				'cep' => '35326-000',
			),
			23 => 
			array (
				'id' => 3761,
				'nome' => 'Santa Rita do Ibitipoca',
				'uf' => 'MG',
				'cep2' => '',
				'estado_cod' => 11,
				'cep' => '36235-000',
			),
			24 => 
			array (
				'id' => 3762,
				'nome' => 'Santa Rita do Itueto',
				'uf' => 'MG',
				'cep2' => '3159506',
				'estado_cod' => 11,
				'cep' => '35225-000',
			),
			25 => 
			array (
				'id' => 3764,
				'nome' => 'Santa Rita do Sapucaí',
				'uf' => 'MG',
				'cep2' => '3159605',
				'estado_cod' => 11,
				'cep' => '37540-000',
			),
			26 => 
			array (
				'id' => 3766,
				'nome' => 'Santa Rosa da Serra',
				'uf' => 'MG',
				'cep2' => '3159704',
				'estado_cod' => 11,
				'cep' => '38805-000',
			),
			27 => 
			array (
				'id' => 3771,
				'nome' => 'Santa Vitória',
				'uf' => 'MG',
				'cep2' => '3159803',
				'estado_cod' => 11,
				'cep' => '38320-000',
			),
			28 => 
			array (
				'id' => 3772,
				'nome' => 'Santana da Vargem',
				'uf' => 'MG',
				'cep2' => '3158300',
				'estado_cod' => 11,
				'cep' => '37195-000',
			),
			29 => 
			array (
				'id' => 3774,
				'nome' => 'Santana de Cataguases',
				'uf' => 'MG',
				'cep2' => '3158409',
				'estado_cod' => 11,
				'cep' => '36795-000',
			),
			30 => 
			array (
				'id' => 3776,
				'nome' => 'Santana de Pirapama',
				'uf' => 'MG',
				'cep2' => '3158508',
				'estado_cod' => 11,
				'cep' => '35785-000',
			),
			31 => 
			array (
				'id' => 3781,
				'nome' => 'Santana do Deserto',
				'uf' => 'MG',
				'cep2' => '3158607',
				'estado_cod' => 11,
				'cep' => '36620-000',
			),
			32 => 
			array (
				'id' => 3782,
				'nome' => 'Santana do Garambéu',
				'uf' => 'MG',
				'cep2' => '3158706',
				'estado_cod' => 11,
				'cep' => '36146-000',
			),
			33 => 
			array (
				'id' => 3783,
				'nome' => 'Santana do Jacaré',
				'uf' => 'MG',
				'cep2' => '3158805',
				'estado_cod' => 11,
				'cep' => '37278-000',
			),
			34 => 
			array (
				'id' => 3784,
				'nome' => 'Santana do Manhuaçu',
				'uf' => 'MG',
				'cep2' => '3158904',
				'estado_cod' => 11,
				'cep' => '36940-000',
			),
			35 => 
			array (
				'id' => 3785,
				'nome' => 'Santana do Paraíso',
				'uf' => 'MG',
				'cep2' => '3158953',
				'estado_cod' => 11,
				'cep' => '35167-000',
			),
			36 => 
			array (
				'id' => 3787,
				'nome' => 'Santana do Riacho',
				'uf' => 'MG',
				'cep2' => '3159001',
				'estado_cod' => 11,
				'cep' => '35845-000',
			),
			37 => 
			array (
				'id' => 3789,
				'nome' => 'Santana dos Montes',
				'uf' => 'MG',
				'cep2' => '3159100',
				'estado_cod' => 11,
				'cep' => '36430-000',
			),
			38 => 
			array (
				'id' => 3793,
				'nome' => 'Santo Antônio do Amparo',
				'uf' => 'MG',
				'cep2' => '3159902',
				'estado_cod' => 11,
				'cep' => '37262-000',
			),
			39 => 
			array (
				'id' => 3794,
				'nome' => 'Santo Antônio do Aventureiro',
				'uf' => 'MG',
				'cep2' => '3160009',
				'estado_cod' => 11,
				'cep' => '36670-000',
			),
			40 => 
			array (
				'id' => 3798,
				'nome' => 'Santo Antônio do Grama',
				'uf' => 'MG',
				'cep2' => '3160108',
				'estado_cod' => 11,
				'cep' => '35388-000',
			),
			41 => 
			array (
				'id' => 3799,
				'nome' => 'Santo Antônio do Itambé',
				'uf' => 'MG',
				'cep2' => '3160207',
				'estado_cod' => 11,
				'cep' => '39160-000',
			),
			42 => 
			array (
				'id' => 3800,
				'nome' => 'Santo Antônio do Jacinto',
				'uf' => 'MG',
				'cep2' => '3160306',
				'estado_cod' => 11,
				'cep' => '39935-000',
			),
			43 => 
			array (
				'id' => 3803,
				'nome' => 'Santo Antônio do Monte',
				'uf' => 'MG',
				'cep2' => '3160405',
				'estado_cod' => 11,
				'cep' => '35560-000',
			),
			44 => 
			array (
				'id' => 3807,
				'nome' => 'Santo Antônio do Retiro',
				'uf' => 'MG',
				'cep2' => '3160454',
				'estado_cod' => 11,
				'cep' => '39538-000',
			),
			45 => 
			array (
				'id' => 3808,
				'nome' => 'Santo Antônio do Rio Abaixo',
				'uf' => 'MG',
				'cep2' => '3160504',
				'estado_cod' => 11,
				'cep' => '35880-000',
			),
			46 => 
			array (
				'id' => 3812,
				'nome' => 'Santo Hipólito',
				'uf' => 'MG',
				'cep2' => '3160603',
				'estado_cod' => 11,
				'cep' => '39210-000',
			),
			47 => 
			array (
				'id' => 3813,
				'nome' => 'Santos Dumont',
				'uf' => 'MG',
				'cep2' => '3160702',
				'estado_cod' => 11,
				'cep' => '36240-000',
			),
			48 => 
			array (
				'id' => 3816,
				'nome' => 'São Bento Abade',
				'uf' => 'MG',
				'cep2' => '3160801',
				'estado_cod' => 11,
				'cep' => '37414-000',
			),
			49 => 
			array (
				'id' => 3818,
				'nome' => 'São Brás do Suaçuí',
				'uf' => 'MG',
				'cep2' => '3160900',
				'estado_cod' => 11,
				'cep' => '35495-000',
			),
			50 => 
			array (
				'id' => 3823,
				'nome' => 'São Domingos das Dores',
				'uf' => 'MG',
				'cep2' => '3160959',
				'estado_cod' => 11,
				'cep' => '35335-000',
			),
			51 => 
			array (
				'id' => 3824,
				'nome' => 'São Domingos do Prata',
				'uf' => 'MG',
				'cep2' => '3161007',
				'estado_cod' => 11,
				'cep' => '35995-000',
			),
			52 => 
			array (
				'id' => 3825,
				'nome' => 'São Félix de Minas',
				'uf' => 'MG',
				'cep2' => '3161056',
				'estado_cod' => 11,
				'cep' => '35275-000',
			),
			53 => 
			array (
				'id' => 3826,
				'nome' => 'São Francisco',
				'uf' => 'MG',
				'cep2' => '3161106',
				'estado_cod' => 11,
				'cep' => '39300-000',
			),
			54 => 
			array (
				'id' => 3827,
				'nome' => 'São Francisco de Paula',
				'uf' => 'MG',
				'cep2' => '3161205',
				'estado_cod' => 11,
				'cep' => '35543-000',
			),
			55 => 
			array (
				'id' => 3828,
				'nome' => 'São Francisco de Sales',
				'uf' => 'MG',
				'cep2' => '3161304',
				'estado_cod' => 11,
				'cep' => '38260-000',
			),
			56 => 
			array (
				'id' => 3829,
				'nome' => 'São Francisco do Glória',
				'uf' => 'MG',
				'cep2' => '3161403',
				'estado_cod' => 11,
				'cep' => '36810-000',
			),
			57 => 
			array (
				'id' => 3831,
				'nome' => 'São Geraldo',
				'uf' => 'MG',
				'cep2' => '3161502',
				'estado_cod' => 11,
				'cep' => '36530-000',
			),
			58 => 
			array (
				'id' => 3833,
				'nome' => 'São Geraldo da Piedade',
				'uf' => 'MG',
				'cep2' => '3161601',
				'estado_cod' => 11,
				'cep' => '39723-000',
			),
			59 => 
			array (
				'id' => 3836,
				'nome' => 'São Geraldo do Baixio',
				'uf' => 'MG',
				'cep2' => '3161650',
				'estado_cod' => 11,
				'cep' => '35258-000',
			),
			60 => 
			array (
				'id' => 3838,
				'nome' => 'São Gonçalo do Abaeté',
				'uf' => 'MG',
				'cep2' => '3161700',
				'estado_cod' => 11,
				'cep' => '38790-000',
			),
			61 => 
			array (
				'id' => 3840,
				'nome' => 'São Gonçalo do Pará',
				'uf' => 'MG',
				'cep2' => '3161809',
				'estado_cod' => 11,
				'cep' => '35516-000',
			),
			62 => 
			array (
				'id' => 3841,
				'nome' => 'São Gonçalo do Rio Abaixo',
				'uf' => 'MG',
				'cep2' => '3161908',
				'estado_cod' => 11,
				'cep' => '35935-000',
			),
			63 => 
			array (
				'id' => 3843,
				'nome' => 'São Gonçalo do Rio Preto',
				'uf' => 'MG',
				'cep2' => '3125507',
				'estado_cod' => 11,
				'cep' => '39185-000',
			),
			64 => 
			array (
				'id' => 3844,
				'nome' => 'São Gonçalo do Sapucaí',
				'uf' => 'MG',
				'cep2' => '3162005',
				'estado_cod' => 11,
				'cep' => '37490-000',
			),
			65 => 
			array (
				'id' => 3845,
				'nome' => 'São Gotardo',
				'uf' => 'MG',
				'cep2' => '3162104',
				'estado_cod' => 11,
				'cep' => '38800-000',
			),
			66 => 
			array (
				'id' => 3847,
				'nome' => 'São João Batista do Glória',
				'uf' => 'MG',
				'cep2' => '3162203',
				'estado_cod' => 11,
				'cep' => '37920-000',
			),
			67 => 
			array (
				'id' => 3849,
				'nome' => 'São João da Lagoa',
				'uf' => 'MG',
				'cep2' => '3162252',
				'estado_cod' => 11,
				'cep' => '39355-000',
			),
			68 => 
			array (
				'id' => 3850,
				'nome' => 'São João da Mata',
				'uf' => 'MG',
				'cep2' => '3162302',
				'estado_cod' => 11,
				'cep' => '37568-000',
			),
			69 => 
			array (
				'id' => 3851,
				'nome' => 'São João da Ponte',
				'uf' => 'MG',
				'cep2' => '3162401',
				'estado_cod' => 11,
				'cep' => '39430-000',
			),
			70 => 
			array (
				'id' => 3856,
				'nome' => 'São João das Missões',
				'uf' => 'MG',
				'cep2' => '3162450',
				'estado_cod' => 11,
				'cep' => '39475-000',
			),
			71 => 
			array (
				'id' => 3857,
				'nome' => 'São João Del Rei',
				'uf' => 'MG',
				'cep2' => '3162500',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			72 => 
			array (
				'id' => 3860,
				'nome' => 'São João do Manhuaçu',
				'uf' => 'MG',
				'cep2' => '3162559',
				'estado_cod' => 11,
				'cep' => '36918-000',
			),
			73 => 
			array (
				'id' => 3861,
				'nome' => 'São João do Manteninha',
				'uf' => 'MG',
				'cep2' => '3162575',
				'estado_cod' => 11,
				'cep' => '35277-000',
			),
			74 => 
			array (
				'id' => 3862,
				'nome' => 'São João do Oriente',
				'uf' => 'MG',
				'cep2' => '3162609',
				'estado_cod' => 11,
				'cep' => '35146-000',
			),
			75 => 
			array (
				'id' => 3863,
				'nome' => 'São João do Pacuí',
				'uf' => 'MG',
				'cep2' => '3162658',
				'estado_cod' => 11,
				'cep' => '39365-000',
			),
			76 => 
			array (
				'id' => 3864,
				'nome' => 'São João do Paraíso',
				'uf' => 'MG',
				'cep2' => '3162708',
				'estado_cod' => 11,
				'cep' => '39540-000',
			),
			77 => 
			array (
				'id' => 3865,
				'nome' => 'São João Evangelista',
				'uf' => 'MG',
				'cep2' => '3162807',
				'estado_cod' => 11,
				'cep' => '39705-000',
			),
			78 => 
			array (
				'id' => 3866,
				'nome' => 'São João Nepomuceno',
				'uf' => 'MG',
				'cep2' => '3162906',
				'estado_cod' => 11,
				'cep' => '36680-000',
			),
			79 => 
			array (
				'id' => 3869,
				'nome' => 'São Joaquim de Bicas',
				'uf' => 'MG',
				'cep2' => '3162922',
				'estado_cod' => 11,
				'cep' => '32920-000',
			),
			80 => 
			array (
				'id' => 3870,
				'nome' => 'São José da Barra',
				'uf' => 'MG',
				'cep2' => '3162948',
				'estado_cod' => 11,
				'cep' => '37945-000',
			),
			81 => 
			array (
				'id' => 3872,
				'nome' => 'São José da Lapa',
				'uf' => 'MG',
				'cep2' => '3162955',
				'estado_cod' => 11,
				'cep' => '33350-000',
			),
			82 => 
			array (
				'id' => 3874,
				'nome' => 'São José da Safira',
				'uf' => 'MG',
				'cep2' => '3163003',
				'estado_cod' => 11,
				'cep' => '39785-000',
			),
			83 => 
			array (
				'id' => 3875,
				'nome' => 'São José da Varginha',
				'uf' => 'MG',
				'cep2' => '3163102',
				'estado_cod' => 11,
				'cep' => '35694-000',
			),
			84 => 
			array (
				'id' => 3878,
				'nome' => 'São José do Alegre',
				'uf' => 'MG',
				'cep2' => '3163201',
				'estado_cod' => 11,
				'cep' => '37510-000',
			),
			85 => 
			array (
				'id' => 3882,
				'nome' => 'São José do Divino',
				'uf' => 'MG',
				'cep2' => '3163300',
				'estado_cod' => 11,
				'cep' => '39848-000',
			),
			86 => 
			array (
				'id' => 3883,
				'nome' => 'São José do Goiabal',
				'uf' => 'MG',
				'cep2' => '3163409',
				'estado_cod' => 11,
				'cep' => '35986-000',
			),
			87 => 
			array (
				'id' => 3885,
				'nome' => 'São José do Jacuri',
				'uf' => 'MG',
				'cep2' => '3163508',
				'estado_cod' => 11,
				'cep' => '39707-000',
			),
			88 => 
			array (
				'id' => 3886,
				'nome' => 'São José do Mantimento',
				'uf' => 'MG',
				'cep2' => '3163607',
				'estado_cod' => 11,
				'cep' => '36990-000',
			),
			89 => 
			array (
				'id' => 3892,
				'nome' => 'São Lourenço',
				'uf' => 'MG',
				'cep2' => '3163706',
				'estado_cod' => 11,
				'cep' => '37470-000',
			),
			90 => 
			array (
				'id' => 3895,
				'nome' => 'São Miguel do Anta',
				'uf' => 'MG',
				'cep2' => '3163805',
				'estado_cod' => 11,
				'cep' => '36590-000',
			),
			91 => 
			array (
				'id' => 3897,
				'nome' => 'São Pedro da União',
				'uf' => 'MG',
				'cep2' => '3163904',
				'estado_cod' => 11,
				'cep' => '37855-000',
			),
			92 => 
			array (
				'id' => 3903,
				'nome' => 'São Pedro do Suaçuí',
				'uf' => 'MG',
				'cep2' => '3164100',
				'estado_cod' => 11,
				'cep' => '39784-000',
			),
			93 => 
			array (
				'id' => 3904,
				'nome' => 'São Pedro dos Ferros',
				'uf' => 'MG',
				'cep2' => '3164001',
				'estado_cod' => 11,
				'cep' => '35360-000',
			),
			94 => 
			array (
				'id' => 3906,
				'nome' => 'São Romão',
				'uf' => 'MG',
				'cep2' => '3164209',
				'estado_cod' => 11,
				'cep' => '39290-000',
			),
			95 => 
			array (
				'id' => 3907,
				'nome' => 'São Roque de Minas',
				'uf' => 'MG',
				'cep2' => '3164308',
				'estado_cod' => 11,
				'cep' => '37928-000',
			),
			96 => 
			array (
				'id' => 3909,
				'nome' => 'São Sebastião da Bela Vista',
				'uf' => 'MG',
				'cep2' => '3164407',
				'estado_cod' => 11,
				'cep' => '37567-000',
			),
			97 => 
			array (
				'id' => 3911,
				'nome' => 'São Sebastião da Vargem Alegre',
				'uf' => 'MG',
				'cep2' => '3164431',
				'estado_cod' => 11,
				'cep' => '36793-000',
			),
			98 => 
			array (
				'id' => 3914,
				'nome' => 'São Sebastião do Anta',
				'uf' => 'MG',
				'cep2' => '3164472',
				'estado_cod' => 11,
				'cep' => '35334-000',
			),
			99 => 
			array (
				'id' => 3921,
				'nome' => 'São Sebastião do Maranhão',
				'uf' => 'MG',
				'cep2' => '3164506',
				'estado_cod' => 11,
				'cep' => '39795-000',
			),
			100 => 
			array (
				'id' => 3923,
				'nome' => 'São Sebastião do Oeste',
				'uf' => 'MG',
				'cep2' => '3164605',
				'estado_cod' => 11,
				'cep' => '35506-000',
			),
			101 => 
			array (
				'id' => 3924,
				'nome' => 'São Sebastião do Paraíso',
				'uf' => 'MG',
				'cep2' => '3164704',
				'estado_cod' => 11,
				'cep' => '37950-000',
			),
			102 => 
			array (
				'id' => 3926,
				'nome' => 'São Sebastião do Rio Preto',
				'uf' => 'MG',
				'cep2' => '3164803',
				'estado_cod' => 11,
				'cep' => '35815-000',
			),
			103 => 
			array (
				'id' => 3927,
				'nome' => 'São Sebastião do Rio Verde',
				'uf' => 'MG',
				'cep2' => '3164902',
				'estado_cod' => 11,
				'cep' => '37467-000',
			),
			104 => 
			array (
				'id' => 3933,
				'nome' => 'São Tiago',
				'uf' => 'MG',
				'cep2' => '3165008',
				'estado_cod' => 11,
				'cep' => '36350-000',
			),
			105 => 
			array (
				'id' => 3934,
				'nome' => 'São Tomás de Aquino',
				'uf' => 'MG',
				'cep2' => '3165107',
				'estado_cod' => 11,
				'cep' => '37960-000',
			),
			106 => 
			array (
				'id' => 3935,
				'nome' => 'São Thomé das Letras',
				'uf' => 'MG',
				'cep2' => '',
				'estado_cod' => 11,
				'cep' => '37418-000',
			),
			107 => 
			array (
				'id' => 3938,
				'nome' => 'São Vicente de Minas',
				'uf' => 'MG',
				'cep2' => '3165305',
				'estado_cod' => 11,
				'cep' => '37370-000',
			),
			108 => 
			array (
				'id' => 3943,
				'nome' => 'Sapucaí-Mirim',
				'uf' => 'MG',
				'cep2' => '3165404',
				'estado_cod' => 11,
				'cep' => '37690-000',
			),
			109 => 
			array (
				'id' => 3948,
				'nome' => 'Sardoá',
				'uf' => 'MG',
				'cep2' => '3165503',
				'estado_cod' => 11,
				'cep' => '39728-000',
			),
			110 => 
			array (
				'id' => 3949,
				'nome' => 'Sarzedo',
				'uf' => 'MG',
				'cep2' => '3165537',
				'estado_cod' => 11,
				'cep' => '32450-000',
			),
			111 => 
			array (
				'id' => 3951,
				'nome' => 'Sem Peixe',
				'uf' => 'MG',
				'cep2' => '',
				'estado_cod' => 11,
				'cep' => '35441-000',
			),
			112 => 
			array (
				'id' => 3952,
				'nome' => 'Senador Amaral',
				'uf' => 'MG',
				'cep2' => '3165578',
				'estado_cod' => 11,
				'cep' => '37615-000',
			),
			113 => 
			array (
				'id' => 3953,
				'nome' => 'Senador Cortes',
				'uf' => 'MG',
				'cep2' => '3165602',
				'estado_cod' => 11,
				'cep' => '36650-000',
			),
			114 => 
			array (
				'id' => 3954,
				'nome' => 'Senador Firmino',
				'uf' => 'MG',
				'cep2' => '3165701',
				'estado_cod' => 11,
				'cep' => '36540-000',
			),
			115 => 
			array (
				'id' => 3955,
				'nome' => 'Senador José Bento',
				'uf' => 'MG',
				'cep2' => '3165800',
				'estado_cod' => 11,
				'cep' => '37558-000',
			),
			116 => 
			array (
				'id' => 3957,
				'nome' => 'Senador Modestino Gonçalves',
				'uf' => 'MG',
				'cep2' => '3165909',
				'estado_cod' => 11,
				'cep' => '39190-000',
			),
			117 => 
			array (
				'id' => 3962,
				'nome' => 'Senhora de Oliveira',
				'uf' => 'MG',
				'cep2' => '3166006',
				'estado_cod' => 11,
				'cep' => '36470-000',
			),
			118 => 
			array (
				'id' => 3964,
				'nome' => 'Senhora do Porto',
				'uf' => 'MG',
				'cep2' => '3166105',
				'estado_cod' => 11,
				'cep' => '39745-000',
			),
			119 => 
			array (
				'id' => 3965,
				'nome' => 'Senhora dos Remédios',
				'uf' => 'MG',
				'cep2' => '3166204',
				'estado_cod' => 11,
				'cep' => '36275-000',
			),
			120 => 
			array (
				'id' => 3967,
				'nome' => 'Sericita',
				'uf' => 'MG',
				'cep2' => '3166303',
				'estado_cod' => 11,
				'cep' => '35368-000',
			),
			121 => 
			array (
				'id' => 3968,
				'nome' => 'Seritinga',
				'uf' => 'MG',
				'cep2' => '3166402',
				'estado_cod' => 11,
				'cep' => '37454-000',
			),
			122 => 
			array (
				'id' => 3970,
				'nome' => 'Serra Azul de Minas',
				'uf' => 'MG',
				'cep2' => '3166501',
				'estado_cod' => 11,
				'cep' => '39165-000',
			),
			123 => 
			array (
				'id' => 3973,
				'nome' => 'Serra da Saudade',
				'uf' => 'MG',
				'cep2' => '3166600',
				'estado_cod' => 11,
				'cep' => '35617-000',
			),
			124 => 
			array (
				'id' => 3976,
				'nome' => 'Serra do Salitre',
				'uf' => 'MG',
				'cep2' => '3166808',
				'estado_cod' => 11,
				'cep' => '38760-000',
			),
			125 => 
			array (
				'id' => 3977,
				'nome' => 'Serra dos Aimorés',
				'uf' => 'MG',
				'cep2' => '3166709',
				'estado_cod' => 11,
				'cep' => '39868-000',
			),
			126 => 
			array (
				'id' => 3980,
				'nome' => 'Serrania',
				'uf' => 'MG',
				'cep2' => '3166907',
				'estado_cod' => 11,
				'cep' => '37136-000',
			),
			127 => 
			array (
				'id' => 3981,
				'nome' => 'Serranópolis de Minas',
				'uf' => 'MG',
				'cep2' => '3166956',
				'estado_cod' => 11,
				'cep' => '39518-000',
			),
			128 => 
			array (
				'id' => 3982,
				'nome' => 'Serranos',
				'uf' => 'MG',
				'cep2' => '3167004',
				'estado_cod' => 11,
				'cep' => '37452-000',
			),
			129 => 
			array (
				'id' => 3983,
				'nome' => 'Serro',
				'uf' => 'MG',
				'cep2' => '3167103',
				'estado_cod' => 11,
				'cep' => '39150-000',
			),
			130 => 
			array (
				'id' => 3986,
				'nome' => 'Sete Lagoas',
				'uf' => 'MG',
				'cep2' => '3167202',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			131 => 
			array (
				'id' => 3987,
				'nome' => 'Setubinha',
				'uf' => 'MG',
				'cep2' => '3165552',
				'estado_cod' => 11,
				'cep' => '39688-000',
			),
			132 => 
			array (
				'id' => 3992,
				'nome' => 'Silveirânia',
				'uf' => 'MG',
				'cep2' => '3167301',
				'estado_cod' => 11,
				'cep' => '36185-000',
			),
			133 => 
			array (
				'id' => 3994,
				'nome' => 'Silvianópolis',
				'uf' => 'MG',
				'cep2' => '3167400',
				'estado_cod' => 11,
				'cep' => '37560-000',
			),
			134 => 
			array (
				'id' => 3996,
				'nome' => 'Simão Pereira',
				'uf' => 'MG',
				'cep2' => '3167509',
				'estado_cod' => 11,
				'cep' => '36123-000',
			),
			135 => 
			array (
				'id' => 3997,
				'nome' => 'Simonésia',
				'uf' => 'MG',
				'cep2' => '3167608',
				'estado_cod' => 11,
				'cep' => '36930-000',
			),
			136 => 
			array (
				'id' => 3999,
				'nome' => 'Sobrália',
				'uf' => 'MG',
				'cep2' => '3167707',
				'estado_cod' => 11,
				'cep' => '35145-000',
			),
			137 => 
			array (
				'id' => 4000,
				'nome' => 'Soledade de Minas',
				'uf' => 'MG',
				'cep2' => '3167806',
				'estado_cod' => 11,
				'cep' => '37478-000',
			),
			138 => 
			array (
				'id' => 4005,
				'nome' => 'Tabuleiro',
				'uf' => 'MG',
				'cep2' => '3167905',
				'estado_cod' => 11,
				'cep' => '36165-000',
			),
			139 => 
			array (
				'id' => 4006,
				'nome' => 'Taiobeiras',
				'uf' => 'MG',
				'cep2' => '3168002',
				'estado_cod' => 11,
				'cep' => '39550-000',
			),
			140 => 
			array (
				'id' => 4007,
				'nome' => 'Taparuba',
				'uf' => 'MG',
				'cep2' => '3168051',
				'estado_cod' => 11,
				'cep' => '36953-000',
			),
			141 => 
			array (
				'id' => 4008,
				'nome' => 'Tapira',
				'uf' => 'MG',
				'cep2' => '3168101',
				'estado_cod' => 11,
				'cep' => '38185-000',
			),
			142 => 
			array (
				'id' => 4009,
				'nome' => 'Tapiraí',
				'uf' => 'MG',
				'cep2' => '3168200',
				'estado_cod' => 11,
				'cep' => '38980-000',
			),
			143 => 
			array (
				'id' => 4011,
				'nome' => 'Taquaraçu de Minas',
				'uf' => 'MG',
				'cep2' => '3168309',
				'estado_cod' => 11,
				'cep' => '33980-000',
			),
			144 => 
			array (
				'id' => 4013,
				'nome' => 'Tarumirim',
				'uf' => 'MG',
				'cep2' => '3168408',
				'estado_cod' => 11,
				'cep' => '35140-000',
			),
			145 => 
			array (
				'id' => 4015,
				'nome' => 'Teixeiras',
				'uf' => 'MG',
				'cep2' => '3168507',
				'estado_cod' => 11,
				'cep' => '36580-000',
			),
			146 => 
			array (
				'id' => 4017,
				'nome' => 'Teófilo Otoni',
				'uf' => 'MG',
				'cep2' => '3168606',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			147 => 
			array (
				'id' => 4019,
				'nome' => 'Timóteo',
				'uf' => 'MG',
				'cep2' => '3168705',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			148 => 
			array (
				'id' => 4020,
				'nome' => 'Tiradentes',
				'uf' => 'MG',
				'cep2' => '3168804',
				'estado_cod' => 11,
				'cep' => '36325-000',
			),
			149 => 
			array (
				'id' => 4021,
				'nome' => 'Tiros',
				'uf' => 'MG',
				'cep2' => '3168903',
				'estado_cod' => 11,
				'cep' => '38880-000',
			),
			150 => 
			array (
				'id' => 4024,
				'nome' => 'Tocantins',
				'uf' => 'MG',
				'cep2' => '3169000',
				'estado_cod' => 11,
				'cep' => '36512-000',
			),
			151 => 
			array (
				'id' => 4025,
				'nome' => 'Tocos do Moji',
				'uf' => 'MG',
				'cep2' => '3169059',
				'estado_cod' => 11,
				'cep' => '37563-000',
			),
			152 => 
			array (
				'id' => 4026,
				'nome' => 'Toledo',
				'uf' => 'MG',
				'cep2' => '3169109',
				'estado_cod' => 11,
				'cep' => '37630-000',
			),
			153 => 
			array (
				'id' => 4028,
				'nome' => 'Tombos',
				'uf' => 'MG',
				'cep2' => '3169208',
				'estado_cod' => 11,
				'cep' => '36844-000',
			),
			154 => 
			array (
				'id' => 4032,
				'nome' => 'Três Corações',
				'uf' => 'MG',
				'cep2' => '3169307',
				'estado_cod' => 11,
				'cep' => '37410-000',
			),
			155 => 
			array (
				'id' => 4034,
				'nome' => 'Três Marias',
				'uf' => 'MG',
				'cep2' => '3169356',
				'estado_cod' => 11,
				'cep' => '39205-000',
			),
			156 => 
			array (
				'id' => 4035,
				'nome' => 'Três Pontas',
				'uf' => 'MG',
				'cep2' => '3169406',
				'estado_cod' => 11,
				'cep' => '37190-000',
			),
			157 => 
			array (
				'id' => 4038,
				'nome' => 'Tumiritinga',
				'uf' => 'MG',
				'cep2' => '3169505',
				'estado_cod' => 11,
				'cep' => '35125-000',
			),
			158 => 
			array (
				'id' => 4039,
				'nome' => 'Tupaciguara',
				'uf' => 'MG',
				'cep2' => '3169604',
				'estado_cod' => 11,
				'cep' => '38430-000',
			),
			159 => 
			array (
				'id' => 4041,
				'nome' => 'Turmalina',
				'uf' => 'MG',
				'cep2' => '3169703',
				'estado_cod' => 11,
				'cep' => '39660-000',
			),
			160 => 
			array (
				'id' => 4042,
				'nome' => 'Turvolândia',
				'uf' => 'MG',
				'cep2' => '3169802',
				'estado_cod' => 11,
				'cep' => '37496-000',
			),
			161 => 
			array (
				'id' => 4043,
				'nome' => 'Ubá',
				'uf' => 'MG',
				'cep2' => '3169901',
				'estado_cod' => 11,
				'cep' => '36500-000',
			),
			162 => 
			array (
				'id' => 4044,
				'nome' => 'Ubaí',
				'uf' => 'MG',
				'cep2' => '3170008',
				'estado_cod' => 11,
				'cep' => '39320-000',
			),
			163 => 
			array (
				'id' => 4045,
				'nome' => 'Ubaporanga',
				'uf' => 'MG',
				'cep2' => '3170057',
				'estado_cod' => 11,
				'cep' => '35338-000',
			),
			164 => 
			array (
				'id' => 4047,
				'nome' => 'Uberaba',
				'uf' => 'MG',
				'cep2' => '3170107',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			165 => 
			array (
				'id' => 4048,
				'nome' => 'Uberlândia',
				'uf' => 'MG',
				'cep2' => '3170206',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			166 => 
			array (
				'id' => 4049,
				'nome' => 'Umburatiba',
				'uf' => 'MG',
				'cep2' => '3170305',
				'estado_cod' => 11,
				'cep' => '39878-000',
			),
			167 => 
			array (
				'id' => 4051,
				'nome' => 'Unaí',
				'uf' => 'MG',
				'cep2' => '3170404',
				'estado_cod' => 11,
				'cep' => '38610-000',
			),
			168 => 
			array (
				'id' => 4052,
				'nome' => 'União de Minas',
				'uf' => 'MG',
				'cep2' => '3170438',
				'estado_cod' => 11,
				'cep' => '38288-000',
			),
			169 => 
			array (
				'id' => 4053,
				'nome' => 'Uruana de Minas',
				'uf' => 'MG',
				'cep2' => '3170479',
				'estado_cod' => 11,
				'cep' => '38630-000',
			),
			170 => 
			array (
				'id' => 4054,
				'nome' => 'Urucânia',
				'uf' => 'MG',
				'cep2' => '3170503',
				'estado_cod' => 11,
				'cep' => '35380-000',
			),
			171 => 
			array (
				'id' => 4055,
				'nome' => 'Urucuia',
				'uf' => 'MG',
				'cep2' => '3170529',
				'estado_cod' => 11,
				'cep' => '39315-000',
			),
			172 => 
			array (
				'id' => 4062,
				'nome' => 'Vargem Alegre',
				'uf' => 'MG',
				'cep2' => '3170578',
				'estado_cod' => 11,
				'cep' => '35199-000',
			),
			173 => 
			array (
				'id' => 4063,
				'nome' => 'Vargem Bonita',
				'uf' => 'MG',
				'cep2' => '3170602',
				'estado_cod' => 11,
				'cep' => '37922-000',
			),
			174 => 
			array (
				'id' => 4064,
				'nome' => 'Vargem Grande do Rio Pardo',
				'uf' => 'MG',
				'cep2' => '3170651',
				'estado_cod' => 11,
				'cep' => '39535-000',
			),
			175 => 
			array (
				'id' => 4066,
				'nome' => 'Varginha',
				'uf' => 'MG',
				'cep2' => '3170701',
				'estado_cod' => 11,
				'cep' => 'LOC',
			),
			176 => 
			array (
				'id' => 4067,
				'nome' => 'Varjão de Minas',
				'uf' => 'MG',
				'cep2' => '3170750',
				'estado_cod' => 11,
				'cep' => '38794-000',
			),
			177 => 
			array (
				'id' => 4068,
				'nome' => 'Várzea da Palma',
				'uf' => 'MG',
				'cep2' => '3170800',
				'estado_cod' => 11,
				'cep' => '39260-000',
			),
			178 => 
			array (
				'id' => 4069,
				'nome' => 'Varzelândia',
				'uf' => 'MG',
				'cep2' => '3170909',
				'estado_cod' => 11,
				'cep' => '39450-000',
			),
			179 => 
			array (
				'id' => 4071,
				'nome' => 'Vazante',
				'uf' => 'MG',
				'cep2' => '3171006',
				'estado_cod' => 11,
				'cep' => '38780-000',
			),
			180 => 
			array (
				'id' => 4073,
				'nome' => 'Verdelândia',
				'uf' => 'MG',
				'cep2' => '3171030',
				'estado_cod' => 11,
				'cep' => '39458-000',
			),
			181 => 
			array (
				'id' => 4076,
				'nome' => 'Veredinha',
				'uf' => 'MG',
				'cep2' => '3171071',
				'estado_cod' => 11,
				'cep' => '39663-000',
			),
			182 => 
			array (
				'id' => 4077,
				'nome' => 'Veríssimo',
				'uf' => 'MG',
				'cep2' => '3171105',
				'estado_cod' => 11,
				'cep' => '38150-000',
			),
			183 => 
			array (
				'id' => 4079,
				'nome' => 'Vermelho Novo',
				'uf' => 'MG',
				'cep2' => '3171154',
				'estado_cod' => 11,
				'cep' => '35359-000',
			),
			184 => 
			array (
				'id' => 4081,
				'nome' => 'Vespasiano',
				'uf' => 'MG',
				'cep2' => '3171204',
				'estado_cod' => 11,
				'cep' => '33200-000',
			),
			185 => 
			array (
				'id' => 4082,
				'nome' => 'Viçosa',
				'uf' => 'MG',
				'cep2' => '3171303',
				'estado_cod' => 11,
				'cep' => '36570-000',
			),
			186 => 
			array (
				'id' => 4083,
				'nome' => 'Vieiras',
				'uf' => 'MG',
				'cep2' => '3171402',
				'estado_cod' => 11,
				'cep' => '36895-000',
			),
			187 => 
			array (
				'id' => 4090,
				'nome' => 'Virgem da Lapa',
				'uf' => 'MG',
				'cep2' => '3171600',
				'estado_cod' => 11,
				'cep' => '39630-000',
			),
			188 => 
			array (
				'id' => 4091,
				'nome' => 'Virgínia',
				'uf' => 'MG',
				'cep2' => '3171709',
				'estado_cod' => 11,
				'cep' => '37465-000',
			),
			189 => 
			array (
				'id' => 4092,
				'nome' => 'Virginópolis',
				'uf' => 'MG',
				'cep2' => '3171808',
				'estado_cod' => 11,
				'cep' => '39730-000',
			),
			190 => 
			array (
				'id' => 4093,
				'nome' => 'Virgolândia',
				'uf' => 'MG',
				'cep2' => '3171907',
				'estado_cod' => 11,
				'cep' => '39715-000',
			),
			191 => 
			array (
				'id' => 4094,
				'nome' => 'Visconde do Rio Branco',
				'uf' => 'MG',
				'cep2' => '3172004',
				'estado_cod' => 11,
				'cep' => '36520-000',
			),
			192 => 
			array (
				'id' => 4098,
				'nome' => 'Volta Grande',
				'uf' => 'MG',
				'cep2' => '3172103',
				'estado_cod' => 11,
				'cep' => '36720-000',
			),
			193 => 
			array (
				'id' => 4099,
				'nome' => 'Wenceslau Braz',
				'uf' => 'MG',
				'cep2' => '3172202',
				'estado_cod' => 11,
				'cep' => '37512-000',
			),
			194 => 
			array (
				'id' => 4103,
				'nome' => 'Água Clara',
				'uf' => 'MS',
				'cep2' => '5000203',
				'estado_cod' => 12,
				'cep' => '79680-000',
			),
			195 => 
			array (
				'id' => 4105,
				'nome' => 'Alcinópolis',
				'uf' => 'MS',
				'cep2' => '5000252',
				'estado_cod' => 12,
				'cep' => '79530-000',
			),
			196 => 
			array (
				'id' => 4107,
				'nome' => 'Amambaí',
				'uf' => 'MS',
				'cep2' => '5000609',
				'estado_cod' => 12,
				'cep' => '79990-000',
			),
			197 => 
			array (
				'id' => 4110,
				'nome' => 'Anastácio',
				'uf' => 'MS',
				'cep2' => '5000708',
				'estado_cod' => 12,
				'cep' => '79210-000',
			),
			198 => 
			array (
				'id' => 4111,
				'nome' => 'Anaurilândia',
				'uf' => 'MS',
				'cep2' => '5000807',
				'estado_cod' => 12,
				'cep' => '79770-000',
			),
			199 => 
			array (
				'id' => 4112,
				'nome' => 'Angélica',
				'uf' => 'MS',
				'cep2' => '5000856',
				'estado_cod' => 12,
				'cep' => '79785-000',
			),
			200 => 
			array (
				'id' => 4114,
				'nome' => 'Antônio João',
				'uf' => 'MS',
				'cep2' => '5000906',
				'estado_cod' => 12,
				'cep' => '79910-000',
			),
			201 => 
			array (
				'id' => 4115,
				'nome' => 'Aparecida do Taboado',
				'uf' => 'MS',
				'cep2' => '5001003',
				'estado_cod' => 12,
				'cep' => '79570-000',
			),
			202 => 
			array (
				'id' => 4116,
				'nome' => 'Aquidauana',
				'uf' => 'MS',
				'cep2' => '5001102',
				'estado_cod' => 12,
				'cep' => '79200-000',
			),
			203 => 
			array (
				'id' => 4117,
				'nome' => 'Aral Moreira',
				'uf' => 'MS',
				'cep2' => '5001243',
				'estado_cod' => 12,
				'cep' => '79930-000',
			),
			204 => 
			array (
				'id' => 4123,
				'nome' => 'Bandeirantes',
				'uf' => 'MS',
				'cep2' => '5001508',
				'estado_cod' => 12,
				'cep' => '79430-000',
			),
			205 => 
			array (
				'id' => 4124,
				'nome' => 'Bataguassu',
				'uf' => 'MS',
				'cep2' => '5001904',
				'estado_cod' => 12,
				'cep' => '79780-000',
			),
			206 => 
			array (
				'id' => 4125,
				'nome' => 'Batayporã',
				'uf' => 'MS',
				'cep2' => '5002001',
				'estado_cod' => 12,
				'cep' => '79760-000',
			),
			207 => 
			array (
				'id' => 4127,
				'nome' => 'Bela Vista',
				'uf' => 'MS',
				'cep2' => '5002100',
				'estado_cod' => 12,
				'cep' => '79260-000',
			),
			208 => 
			array (
				'id' => 4130,
				'nome' => 'Bodoquena',
				'uf' => 'MS',
				'cep2' => '5002159',
				'estado_cod' => 12,
				'cep' => '79390-000',
			),
			209 => 
			array (
				'id' => 4132,
				'nome' => 'Bonito',
				'uf' => 'MS',
				'cep2' => '5002209',
				'estado_cod' => 12,
				'cep' => '79290-000',
			),
			210 => 
			array (
				'id' => 4134,
				'nome' => 'Brasilândia',
				'uf' => 'MS',
				'cep2' => '5002308',
				'estado_cod' => 12,
				'cep' => '79670-000',
			),
			211 => 
			array (
				'id' => 4135,
				'nome' => 'Caarapó',
				'uf' => 'MS',
				'cep2' => '5002407',
				'estado_cod' => 12,
				'cep' => '79940-000',
			),
			212 => 
			array (
				'id' => 4138,
				'nome' => 'Camapuã',
				'uf' => 'MS',
				'cep2' => '5002605',
				'estado_cod' => 12,
				'cep' => '79420-000',
			),
			213 => 
			array (
				'id' => 4141,
				'nome' => 'Campo Grande',
				'uf' => 'MS',
				'cep2' => '5002704',
				'estado_cod' => 12,
				'cep' => 'LOC',
			),
			214 => 
			array (
				'id' => 4143,
				'nome' => 'Caracol',
				'uf' => 'MS',
				'cep2' => '5002803',
				'estado_cod' => 12,
				'cep' => '79270-000',
			),
			215 => 
			array (
				'id' => 4145,
				'nome' => 'Cassilândia',
				'uf' => 'MS',
				'cep2' => '5002902',
				'estado_cod' => 12,
				'cep' => '79540-000',
			),
			216 => 
			array (
				'id' => 4146,
				'nome' => 'Chapadão do Sul',
				'uf' => 'MS',
				'cep2' => '5002951',
				'estado_cod' => 12,
				'cep' => '79560-000',
			),
			217 => 
			array (
				'id' => 4150,
				'nome' => 'Corguinho',
				'uf' => 'MS',
				'cep2' => '5003108',
				'estado_cod' => 12,
				'cep' => '79460-000',
			),
			218 => 
			array (
				'id' => 4151,
				'nome' => 'Coronel Sapucaia',
				'uf' => 'MS',
				'cep2' => '5003157',
				'estado_cod' => 12,
				'cep' => '79995-000',
			),
			219 => 
			array (
				'id' => 4152,
				'nome' => 'Corumbá',
				'uf' => 'MS',
				'cep2' => '5003207',
				'estado_cod' => 12,
				'cep' => 'LOC',
			),
			220 => 
			array (
				'id' => 4153,
				'nome' => 'Costa Rica',
				'uf' => 'MS',
				'cep2' => '5003256',
				'estado_cod' => 12,
				'cep' => '79550-000',
			),
			221 => 
			array (
				'id' => 4154,
				'nome' => 'Coxim',
				'uf' => 'MS',
				'cep2' => '5003306',
				'estado_cod' => 12,
				'cep' => '79400-000',
			),
			222 => 
			array (
				'id' => 4160,
				'nome' => 'Deodápolis',
				'uf' => 'MS',
				'cep2' => '5003454',
				'estado_cod' => 12,
				'cep' => '79790-000',
			),
			223 => 
			array (
				'id' => 4161,
				'nome' => 'Dois Irmãos do Buriti',
				'uf' => 'MS',
				'cep2' => '5003488',
				'estado_cod' => 12,
				'cep' => '79215-000',
			),
			224 => 
			array (
				'id' => 4162,
				'nome' => 'Douradina',
				'uf' => 'MS',
				'cep2' => '5003504',
				'estado_cod' => 12,
				'cep' => '79880-000',
			),
			225 => 
			array (
				'id' => 4163,
				'nome' => 'Dourados',
				'uf' => 'MS',
				'cep2' => '5003702',
				'estado_cod' => 12,
				'cep' => 'LOC',
			),
			226 => 
			array (
				'id' => 4164,
				'nome' => 'Eldorado',
				'uf' => 'MS',
				'cep2' => '5003751',
				'estado_cod' => 12,
				'cep' => '79970-000',
			),
			227 => 
			array (
				'id' => 4165,
				'nome' => 'Fátima do Sul',
				'uf' => 'MS',
				'cep2' => '5003801',
				'estado_cod' => 12,
				'cep' => '79700-000',
			),
			228 => 
			array (
				'id' => 4166,
				'nome' => 'Figueirão',
				'uf' => 'MS',
				'cep2' => '5003900',
				'estado_cod' => 12,
				'cep' => '79428-000',
			),
			229 => 
			array (
				'id' => 4168,
				'nome' => 'Glória de Dourados',
				'uf' => 'MS',
				'cep2' => '5004007',
				'estado_cod' => 12,
				'cep' => '79730-000',
			),
			230 => 
			array (
				'id' => 4172,
				'nome' => 'Guia Lopes da Laguna',
				'uf' => 'MS',
				'cep2' => '5004106',
				'estado_cod' => 12,
				'cep' => '79230-000',
			),
			231 => 
			array (
				'id' => 4173,
				'nome' => 'Iguatemi',
				'uf' => 'MS',
				'cep2' => '5004304',
				'estado_cod' => 12,
				'cep' => '79960-000',
			),
			232 => 
			array (
				'id' => 4179,
				'nome' => 'Inocência',
				'uf' => 'MS',
				'cep2' => '5004403',
				'estado_cod' => 12,
				'cep' => '79580-000',
			),
			233 => 
			array (
				'id' => 4182,
				'nome' => 'Itaporã',
				'uf' => 'MS',
				'cep2' => '5004502',
				'estado_cod' => 12,
				'cep' => '79890-000',
			),
			234 => 
			array (
				'id' => 4183,
				'nome' => 'Itaquiraí',
				'uf' => 'MS',
				'cep2' => '5004601',
				'estado_cod' => 12,
				'cep' => '79965-000',
			),
			235 => 
			array (
				'id' => 4184,
				'nome' => 'Ivinhema',
				'uf' => 'MS',
				'cep2' => '5004700',
				'estado_cod' => 12,
				'cep' => '79740-000',
			),
			236 => 
			array (
				'id' => 4187,
				'nome' => 'Japorã',
				'uf' => 'MS',
				'cep2' => '5004809',
				'estado_cod' => 12,
				'cep' => '79985-000',
			),
			237 => 
			array (
				'id' => 4188,
				'nome' => 'Jaraguari',
				'uf' => 'MS',
				'cep2' => '5004908',
				'estado_cod' => 12,
				'cep' => '79440-000',
			),
			238 => 
			array (
				'id' => 4189,
				'nome' => 'Jardim',
				'uf' => 'MS',
				'cep2' => '5005004',
				'estado_cod' => 12,
				'cep' => '79240-000',
			),
			239 => 
			array (
				'id' => 4190,
				'nome' => 'Jateí',
				'uf' => 'MS',
				'cep2' => '5005103',
				'estado_cod' => 12,
				'cep' => '79720-000',
			),
			240 => 
			array (
				'id' => 4193,
				'nome' => 'Jutí',
				'uf' => 'MS',
				'cep2' => '',
				'estado_cod' => 12,
				'cep' => '79955-000',
			),
			241 => 
			array (
				'id' => 4194,
				'nome' => 'Ladário',
				'uf' => 'MS',
				'cep2' => '5005202',
				'estado_cod' => 12,
				'cep' => '79370-000',
			),
			242 => 
			array (
				'id' => 4196,
				'nome' => 'Laguna Carapã',
				'uf' => 'MS',
				'cep2' => '5005251',
				'estado_cod' => 12,
				'cep' => '79920-000',
			),
			243 => 
			array (
				'id' => 4197,
				'nome' => 'Maracaju',
				'uf' => 'MS',
				'cep2' => '5005400',
				'estado_cod' => 12,
				'cep' => '79150-000',
			),
			244 => 
			array (
				'id' => 4198,
				'nome' => 'Miranda',
				'uf' => 'MS',
				'cep2' => '5005608',
				'estado_cod' => 12,
				'cep' => '79380-000',
			),
			245 => 
			array (
				'id' => 4203,
				'nome' => 'Mundo Novo',
				'uf' => 'MS',
				'cep2' => '5005681',
				'estado_cod' => 12,
				'cep' => '79980-000',
			),
			246 => 
			array (
				'id' => 4204,
				'nome' => 'Naviraí',
				'uf' => 'MS',
				'cep2' => '5005707',
				'estado_cod' => 12,
				'cep' => '79950-000',
			),
			247 => 
			array (
				'id' => 4206,
				'nome' => 'Nioaque',
				'uf' => 'MS',
				'cep2' => '5005806',
				'estado_cod' => 12,
				'cep' => '79220-000',
			),
			248 => 
			array (
				'id' => 4208,
				'nome' => 'Nova Alvorada do Sul',
				'uf' => 'MS',
				'cep2' => '5006002',
				'estado_cod' => 12,
				'cep' => '79140-000',
			),
			249 => 
			array (
				'id' => 4210,
				'nome' => 'Nova Andradina',
				'uf' => 'MS',
				'cep2' => '5006200',
				'estado_cod' => 12,
				'cep' => '79750-000',
			),
			250 => 
			array (
				'id' => 4214,
				'nome' => 'Novo Horizonte do Sul',
				'uf' => 'MS',
				'cep2' => '5006259',
				'estado_cod' => 12,
				'cep' => '79745-000',
			),
			251 => 
			array (
				'id' => 4220,
				'nome' => 'Paranaíba',
				'uf' => 'MS',
				'cep2' => '5006309',
				'estado_cod' => 12,
				'cep' => '79500-000',
			),
			252 => 
			array (
				'id' => 4221,
				'nome' => 'Paranhos',
				'uf' => 'MS',
				'cep2' => '5006358',
				'estado_cod' => 12,
				'cep' => '79925-000',
			),
			253 => 
			array (
				'id' => 4222,
				'nome' => 'Pedro Gomes',
				'uf' => 'MS',
				'cep2' => '5006408',
				'estado_cod' => 12,
				'cep' => '79410-000',
			),
			254 => 
			array (
				'id' => 4226,
				'nome' => 'Ponta Porã',
				'uf' => 'MS',
				'cep2' => '5006606',
				'estado_cod' => 12,
				'cep' => 'LOC',
			),
			255 => 
			array (
				'id' => 4230,
				'nome' => 'Porto Murtinho',
				'uf' => 'MS',
				'cep2' => '5006903',
				'estado_cod' => 12,
				'cep' => '79280-000',
			),
			256 => 
			array (
				'id' => 4237,
				'nome' => 'Ribas do Rio Pardo',
				'uf' => 'MS',
				'cep2' => '5007109',
				'estado_cod' => 12,
				'cep' => '79180-000',
			),
			257 => 
			array (
				'id' => 4238,
				'nome' => 'Rio Brilhante',
				'uf' => 'MS',
				'cep2' => '5007208',
				'estado_cod' => 12,
				'cep' => '79130-000',
			),
			258 => 
			array (
				'id' => 4239,
				'nome' => 'Rio Negro',
				'uf' => 'MS',
				'cep2' => '5007307',
				'estado_cod' => 12,
				'cep' => '79470-000',
			),
			259 => 
			array (
				'id' => 4240,
				'nome' => 'Rio Verde de Mato Grosso',
				'uf' => 'MS',
				'cep2' => '5007406',
				'estado_cod' => 12,
				'cep' => '79480-000',
			),
			260 => 
			array (
				'id' => 4242,
				'nome' => 'Rochedo',
				'uf' => 'MS',
				'cep2' => '5007505',
				'estado_cod' => 12,
				'cep' => '79450-000',
			),
			261 => 
			array (
				'id' => 4244,
				'nome' => 'Santa Rita do Pardo',
				'uf' => 'MS',
				'cep2' => '5007554',
				'estado_cod' => 12,
				'cep' => '79690-000',
			),
			262 => 
			array (
				'id' => 4246,
				'nome' => 'São Gabriel do Oeste',
				'uf' => 'MS',
				'cep2' => '5007695',
				'estado_cod' => 12,
				'cep' => '79490-000',
			),
			263 => 
			array (
				'id' => 4253,
				'nome' => 'Selvíria',
				'uf' => 'MS',
				'cep2' => '5007802',
				'estado_cod' => 12,
				'cep' => '79590-000',
			),
			264 => 
			array (
				'id' => 4254,
				'nome' => 'Sete Quedas',
				'uf' => 'MS',
				'cep2' => '5007703',
				'estado_cod' => 12,
				'cep' => '79935-000',
			),
			265 => 
			array (
				'id' => 4255,
				'nome' => 'Sidrolândia',
				'uf' => 'MS',
				'cep2' => '5007901',
				'estado_cod' => 12,
				'cep' => '79170-000',
			),
			266 => 
			array (
				'id' => 4256,
				'nome' => 'Sonora',
				'uf' => 'MS',
				'cep2' => '5007935',
				'estado_cod' => 12,
				'cep' => '79415-000',
			),
			267 => 
			array (
				'id' => 4257,
				'nome' => 'Tacuru',
				'uf' => 'MS',
				'cep2' => '5007950',
				'estado_cod' => 12,
				'cep' => '79975-000',
			),
			268 => 
			array (
				'id' => 4260,
				'nome' => 'Taquarussu',
				'uf' => 'MS',
				'cep2' => '5007976',
				'estado_cod' => 12,
				'cep' => '79765-000',
			),
			269 => 
			array (
				'id' => 4262,
				'nome' => 'Terenos',
				'uf' => 'MS',
				'cep2' => '5008008',
				'estado_cod' => 12,
				'cep' => '79190-000',
			),
			270 => 
			array (
				'id' => 4263,
				'nome' => 'Três Lagoas',
				'uf' => 'MS',
				'cep2' => '5008305',
				'estado_cod' => 12,
				'cep' => 'LOC',
			),
			271 => 
			array (
				'id' => 4265,
				'nome' => 'Vicentina',
				'uf' => 'MS',
				'cep2' => '5008404',
				'estado_cod' => 12,
				'cep' => '79710-000',
			),
			272 => 
			array (
				'id' => 4272,
				'nome' => 'Acorizal',
				'uf' => 'MT',
				'cep2' => '5100102',
				'estado_cod' => 13,
				'cep' => '78480-000',
			),
			273 => 
			array (
				'id' => 4273,
				'nome' => 'Água Boa',
				'uf' => 'MT',
				'cep2' => '5100201',
				'estado_cod' => 13,
				'cep' => '78635-000',
			),
			274 => 
			array (
				'id' => 4280,
				'nome' => 'Alta Floresta',
				'uf' => 'MT',
				'cep2' => '5100250',
				'estado_cod' => 13,
				'cep' => '78580-000',
			),
			275 => 
			array (
				'id' => 4281,
				'nome' => 'Alto Araguaia',
				'uf' => 'MT',
				'cep2' => '5100300',
				'estado_cod' => 13,
				'cep' => '78780-000',
			),
			276 => 
			array (
				'id' => 4282,
				'nome' => 'Alto Boa Vista',
				'uf' => 'MT',
				'cep2' => '5100359',
				'estado_cod' => 13,
				'cep' => '78665-000',
			),
			277 => 
			array (
				'id' => 4284,
				'nome' => 'Alto Garças',
				'uf' => 'MT',
				'cep2' => '5100409',
				'estado_cod' => 13,
				'cep' => '78770-000',
			),
			278 => 
			array (
				'id' => 4286,
				'nome' => 'Alto Paraguai',
				'uf' => 'MT',
				'cep2' => '5100508',
				'estado_cod' => 13,
				'cep' => '78410-000',
			),
			279 => 
			array (
				'id' => 4288,
				'nome' => 'Alto Taquari',
				'uf' => 'MT',
				'cep2' => '5100607',
				'estado_cod' => 13,
				'cep' => '78785-000',
			),
			280 => 
			array (
				'id' => 4290,
				'nome' => 'Apiacás',
				'uf' => 'MT',
				'cep2' => '5100805',
				'estado_cod' => 13,
				'cep' => '78595-000',
			),
			281 => 
			array (
				'id' => 4291,
				'nome' => 'Araguaiana',
				'uf' => 'MT',
				'cep2' => '5101001',
				'estado_cod' => 13,
				'cep' => '78685-000',
			),
			282 => 
			array (
				'id' => 4292,
				'nome' => 'Araguainha',
				'uf' => 'MT',
				'cep2' => '5101209',
				'estado_cod' => 13,
				'cep' => '78615-000',
			),
			283 => 
			array (
				'id' => 4293,
				'nome' => 'Araputanga',
				'uf' => 'MT',
				'cep2' => '5101258',
				'estado_cod' => 13,
				'cep' => '78260-000',
			),
			284 => 
			array (
				'id' => 4294,
				'nome' => 'Arenápolis',
				'uf' => 'MT',
				'cep2' => '5101308',
				'estado_cod' => 13,
				'cep' => '78420-000',
			),
			285 => 
			array (
				'id' => 4295,
				'nome' => 'Aripuanã',
				'uf' => 'MT',
				'cep2' => '5101407',
				'estado_cod' => 13,
				'cep' => '78325-000',
			),
			286 => 
			array (
				'id' => 4298,
				'nome' => 'Barão de Melgaço',
				'uf' => 'MT',
				'cep2' => '5101605',
				'estado_cod' => 13,
				'cep' => '78190-000',
			),
			287 => 
			array (
				'id' => 4299,
				'nome' => 'Barra do Bugres',
				'uf' => 'MT',
				'cep2' => '5101704',
				'estado_cod' => 13,
				'cep' => '78390-000',
			),
			288 => 
			array (
				'id' => 4300,
				'nome' => 'Barra do Garças',
				'uf' => 'MT',
				'cep2' => '5101803',
				'estado_cod' => 13,
				'cep' => '78600-000',
			),
			289 => 
			array (
				'id' => 4310,
				'nome' => 'Brasnorte',
				'uf' => 'MT',
				'cep2' => '5101902',
				'estado_cod' => 13,
				'cep' => '78350-000',
			),
			290 => 
			array (
				'id' => 4313,
				'nome' => 'Cáceres',
				'uf' => 'MT',
				'cep2' => '5102504',
				'estado_cod' => 13,
				'cep' => '78200-000',
			),
			291 => 
			array (
				'id' => 4315,
				'nome' => 'Campinápolis',
				'uf' => 'MT',
				'cep2' => '5102603',
				'estado_cod' => 13,
				'cep' => '78630-000',
			),
			292 => 
			array (
				'id' => 4316,
				'nome' => 'Campo Novo do Parecis',
				'uf' => 'MT',
				'cep2' => '5102637',
				'estado_cod' => 13,
				'cep' => '78360-000',
			),
			293 => 
			array (
				'id' => 4317,
				'nome' => 'Campo Verde',
				'uf' => 'MT',
				'cep2' => '5102678',
				'estado_cod' => 13,
				'cep' => '78840-000',
			),
			294 => 
			array (
				'id' => 4318,
				'nome' => 'Campos de Júlio',
				'uf' => 'MT',
				'cep2' => '5102686',
				'estado_cod' => 13,
				'cep' => '78307-000',
			),
			295 => 
			array (
				'id' => 4320,
				'nome' => 'Canabrava do Norte',
				'uf' => 'MT',
				'cep2' => '5102694',
				'estado_cod' => 13,
				'cep' => '78658-000',
			),
			296 => 
			array (
				'id' => 4321,
				'nome' => 'Canarana',
				'uf' => 'MT',
				'cep2' => '5102702',
				'estado_cod' => 13,
				'cep' => '78640-000',
			),
			297 => 
			array (
				'id' => 4327,
				'nome' => 'Carlinda',
				'uf' => 'MT',
				'cep2' => '5102793',
				'estado_cod' => 13,
				'cep' => '78587-000',
			),
			298 => 
			array (
				'id' => 4329,
				'nome' => 'Castanheira',
				'uf' => 'MT',
				'cep2' => '5102850',
				'estado_cod' => 13,
				'cep' => '78345-000',
			),
			299 => 
			array (
				'id' => 4331,
				'nome' => 'Chapada dos Guimarães',
				'uf' => 'MT',
				'cep2' => '5103007',
				'estado_cod' => 13,
				'cep' => '78195-000',
			),
			300 => 
			array (
				'id' => 4333,
				'nome' => 'Cláudia',
				'uf' => 'MT',
				'cep2' => '5103056',
				'estado_cod' => 13,
				'cep' => '78540-000',
			),
			301 => 
			array (
				'id' => 4334,
				'nome' => 'Cocalinho',
				'uf' => 'MT',
				'cep2' => '5103106',
				'estado_cod' => 13,
				'cep' => '78680-000',
			),
			302 => 
			array (
				'id' => 4335,
				'nome' => 'Colíder',
				'uf' => 'MT',
				'cep2' => '5103205',
				'estado_cod' => 13,
				'cep' => '78500-000',
			),
			303 => 
			array (
				'id' => 4337,
				'nome' => 'Comodoro',
				'uf' => 'MT',
				'cep2' => '5103304',
				'estado_cod' => 13,
				'cep' => '78310-000',
			),
			304 => 
			array (
				'id' => 4338,
				'nome' => 'Confresa',
				'uf' => 'MT',
				'cep2' => '5103353',
				'estado_cod' => 13,
				'cep' => '78652-000',
			),
			305 => 
			array (
				'id' => 4341,
				'nome' => 'Cotriguaçu',
				'uf' => 'MT',
				'cep2' => '5103379',
				'estado_cod' => 13,
				'cep' => '78330-000',
			),
			306 => 
			array (
				'id' => 4347,
				'nome' => 'Cuiabá',
				'uf' => 'MT',
				'cep2' => '5103403',
				'estado_cod' => 13,
				'cep' => 'LOC',
			),
			307 => 
			array (
				'id' => 4348,
				'nome' => 'Curvelândia',
				'uf' => 'MT',
				'cep2' => '5103437',
				'estado_cod' => 13,
				'cep' => '78237-000',
			),
			308 => 
			array (
				'id' => 4350,
				'nome' => 'Denise',
				'uf' => 'MT',
				'cep2' => '5103452',
				'estado_cod' => 13,
				'cep' => '78380-000',
			),
			309 => 
			array (
				'id' => 4351,
				'nome' => 'Diamantino',
				'uf' => 'MT',
				'cep2' => '5103502',
				'estado_cod' => 13,
				'cep' => '78400-000',
			),
			310 => 
			array (
				'id' => 4352,
				'nome' => 'Dom Aquino',
				'uf' => 'MT',
				'cep2' => '5103601',
				'estado_cod' => 13,
				'cep' => '78830-000',
			),
			311 => 
			array (
				'id' => 4359,
				'nome' => 'Feliz Natal',
				'uf' => 'MT',
				'cep2' => '5103700',
				'estado_cod' => 13,
				'cep' => '78885-000',
			),
			312 => 
			array (
				'id' => 4360,
				'nome' => 'Figueirópolis D\'Oeste',
				'uf' => 'MT',
				'cep2' => '5103809',
				'estado_cod' => 13,
				'cep' => '78290-000',
			),
			313 => 
			array (
				'id' => 4364,
				'nome' => 'Gaúcha do Norte',
				'uf' => 'MT',
				'cep2' => '5103858',
				'estado_cod' => 13,
				'cep' => '78875-000',
			),
			314 => 
			array (
				'id' => 4365,
				'nome' => 'General Carneiro',
				'uf' => 'MT',
				'cep2' => '5103908',
				'estado_cod' => 13,
				'cep' => '78620-000',
			),
			315 => 
			array (
				'id' => 4366,
				'nome' => 'Glória D\'Oeste',
				'uf' => 'MT',
				'cep2' => '5103957',
				'estado_cod' => 13,
				'cep' => '78293-000',
			),
			316 => 
			array (
				'id' => 4367,
				'nome' => 'Guarantã do Norte',
				'uf' => 'MT',
				'cep2' => '5104104',
				'estado_cod' => 13,
				'cep' => '78520-000',
			),
			317 => 
			array (
				'id' => 4369,
				'nome' => 'Guiratinga',
				'uf' => 'MT',
				'cep2' => '5104203',
				'estado_cod' => 13,
				'cep' => '78760-000',
			),
			318 => 
			array (
				'id' => 4372,
				'nome' => 'Indiavaí',
				'uf' => 'MT',
				'cep2' => '5104500',
				'estado_cod' => 13,
				'cep' => '78295-000',
			),
			319 => 
			array (
				'id' => 4375,
				'nome' => 'Itaúba',
				'uf' => 'MT',
				'cep2' => '5104559',
				'estado_cod' => 13,
				'cep' => '78510-000',
			),
			320 => 
			array (
				'id' => 4376,
				'nome' => 'Itiquira',
				'uf' => 'MT',
				'cep2' => '5104609',
				'estado_cod' => 13,
				'cep' => '78790-000',
			),
			321 => 
			array (
				'id' => 4377,
				'nome' => 'Jaciara',
				'uf' => 'MT',
				'cep2' => '5104807',
				'estado_cod' => 13,
				'cep' => '78820-000',
			),
			322 => 
			array (
				'id' => 4378,
				'nome' => 'Jangada',
				'uf' => 'MT',
				'cep2' => '5104906',
				'estado_cod' => 13,
				'cep' => '78490-000',
			),
			323 => 
			array (
				'id' => 4381,
				'nome' => 'Jauru',
				'uf' => 'MT',
				'cep2' => '5105002',
				'estado_cod' => 13,
				'cep' => '78255-000',
			),
			324 => 
			array (
				'id' => 4383,
				'nome' => 'Juara',
				'uf' => 'MT',
				'cep2' => '5105101',
				'estado_cod' => 13,
				'cep' => '78575-000',
			),
			325 => 
			array (
				'id' => 4384,
				'nome' => 'Juína',
				'uf' => 'MT',
				'cep2' => '5105150',
				'estado_cod' => 13,
				'cep' => '78320-000',
			),
			326 => 
			array (
				'id' => 4385,
				'nome' => 'Juruena',
				'uf' => 'MT',
				'cep2' => '5105176',
				'estado_cod' => 13,
				'cep' => '78340-000',
			),
			327 => 
			array (
				'id' => 4386,
				'nome' => 'Juscimeira',
				'uf' => 'MT',
				'cep2' => '5105200',
				'estado_cod' => 13,
				'cep' => '78810-000',
			),
			328 => 
			array (
				'id' => 4387,
				'nome' => 'Lambari D\'Oeste',
				'uf' => 'MT',
				'cep2' => '5105234',
				'estado_cod' => 13,
				'cep' => '78278-000',
			),
			329 => 
			array (
				'id' => 4389,
				'nome' => 'Lucas do Rio Verde',
				'uf' => 'MT',
				'cep2' => '5105259',
				'estado_cod' => 13,
				'cep' => '78455-000',
			),
			330 => 
			array (
				'id' => 4391,
				'nome' => 'Luciara',
				'uf' => 'MT',
				'cep2' => '5105309',
				'estado_cod' => 13,
				'cep' => '78660-000',
			),
			331 => 
			array (
				'id' => 4393,
				'nome' => 'Marcelândia',
				'uf' => 'MT',
				'cep2' => '5105580',
				'estado_cod' => 13,
				'cep' => '78535-000',
			),
			332 => 
			array (
				'id' => 4396,
				'nome' => 'Matupá',
				'uf' => 'MT',
				'cep2' => '5105606',
				'estado_cod' => 13,
				'cep' => '78525-000',
			),
			333 => 
			array (
				'id' => 4398,
				'nome' => 'Mirassol D\'Oeste',
				'uf' => 'MT',
				'cep2' => '5105622',
				'estado_cod' => 13,
				'cep' => '78280-000',
			),
			334 => 
			array (
				'id' => 4399,
				'nome' => 'Nobres',
				'uf' => 'MT',
				'cep2' => '5105903',
				'estado_cod' => 13,
				'cep' => '78460-000',
			),
			335 => 
			array (
				'id' => 4401,
				'nome' => 'Nortelândia',
				'uf' => 'MT',
				'cep2' => '5106000',
				'estado_cod' => 13,
				'cep' => '78430-000',
			),
			336 => 
			array (
				'id' => 4403,
				'nome' => 'Nossa Senhora do Livramento',
				'uf' => 'MT',
				'cep2' => '5106109',
				'estado_cod' => 13,
				'cep' => '78170-000',
			),
			337 => 
			array (
				'id' => 4405,
				'nome' => 'Nova Bandeirantes',
				'uf' => 'MT',
				'cep2' => '5106158',
				'estado_cod' => 13,
				'cep' => '78565-000',
			),
			338 => 
			array (
				'id' => 4406,
				'nome' => 'Nova Brasilândia',
				'uf' => 'MT',
				'cep2' => '5106208',
				'estado_cod' => 13,
				'cep' => '78860-000',
			),
			339 => 
			array (
				'id' => 4408,
				'nome' => 'Nova Canaã do Norte',
				'uf' => 'MT',
				'cep2' => '5106216',
				'estado_cod' => 13,
				'cep' => '78515-000',
			),
			340 => 
			array (
				'id' => 4411,
				'nome' => 'Nova Guarita',
				'uf' => 'MT',
				'cep2' => '5108808',
				'estado_cod' => 13,
				'cep' => '78508-000',
			),
			341 => 
			array (
				'id' => 4412,
				'nome' => 'Nova Lacerda',
				'uf' => 'MT',
				'cep2' => '5106182',
				'estado_cod' => 13,
				'cep' => '78243-000',
			),
			342 => 
			array (
				'id' => 4413,
				'nome' => 'Nova Marilândia',
				'uf' => 'MT',
				'cep2' => '5108857',
				'estado_cod' => 13,
				'cep' => '78415-000',
			),
			343 => 
			array (
				'id' => 4414,
				'nome' => 'Nova Maringá',
				'uf' => 'MT',
				'cep2' => '5108907',
				'estado_cod' => 13,
				'cep' => '78445-000',
			),
			344 => 
			array (
				'id' => 4415,
				'nome' => 'Nova Monte Verde',
				'uf' => 'MT',
				'cep2' => '5108956',
				'estado_cod' => 13,
				'cep' => '78593-000',
			),
			345 => 
			array (
				'id' => 4416,
				'nome' => 'Nova Mutum',
				'uf' => 'MT',
				'cep2' => '5106224',
				'estado_cod' => 13,
				'cep' => '78450-000',
			),
			346 => 
			array (
				'id' => 4417,
				'nome' => 'Nova Olímpia',
				'uf' => 'MT',
				'cep2' => '5106232',
				'estado_cod' => 13,
				'cep' => '78370-000',
			),
			347 => 
			array (
				'id' => 4418,
				'nome' => 'Nova Ubiratã',
				'uf' => 'MT',
				'cep2' => '5106240',
				'estado_cod' => 13,
				'cep' => '78888-000',
			),
			348 => 
			array (
				'id' => 4419,
				'nome' => 'Nova Xavantina',
				'uf' => 'MT',
				'cep2' => '5106257',
				'estado_cod' => 13,
				'cep' => '78690-000',
			),
			349 => 
			array (
				'id' => 4422,
				'nome' => 'Novo Horizonte do Norte',
				'uf' => 'MT',
				'cep2' => '5106273',
				'estado_cod' => 13,
				'cep' => '78570-000',
			),
			350 => 
			array (
				'id' => 4423,
				'nome' => 'Novo Mundo',
				'uf' => 'MT',
				'cep2' => '5106265',
				'estado_cod' => 13,
				'cep' => '78528-000',
			),
			351 => 
			array (
				'id' => 4425,
				'nome' => 'Novo São Joaquim',
				'uf' => 'MT',
				'cep2' => '5106281',
				'estado_cod' => 13,
				'cep' => '78625-000',
			),
			352 => 
			array (
				'id' => 4429,
				'nome' => 'Paranaitá',
				'uf' => 'MT',
				'cep2' => '',
				'estado_cod' => 13,
				'cep' => '78590-000',
			),
			353 => 
			array (
				'id' => 4430,
				'nome' => 'Paranatinga',
				'uf' => 'MT',
				'cep2' => '5106307',
				'estado_cod' => 13,
				'cep' => '78870-000',
			),
			354 => 
			array (
				'id' => 4432,
				'nome' => 'Pedra Preta',
				'uf' => 'MT',
				'cep2' => '5106372',
				'estado_cod' => 13,
				'cep' => '78795-000',
			),
			355 => 
			array (
				'id' => 4433,
				'nome' => 'Peixoto de Azevedo',
				'uf' => 'MT',
				'cep2' => '5106422',
				'estado_cod' => 13,
				'cep' => '78530-000',
			),
			356 => 
			array (
				'id' => 4436,
				'nome' => 'Planalto da Serra',
				'uf' => 'MT',
				'cep2' => '5106455',
				'estado_cod' => 13,
				'cep' => '78855-000',
			),
			357 => 
			array (
				'id' => 4437,
				'nome' => 'Poconé',
				'uf' => 'MT',
				'cep2' => '5106505',
				'estado_cod' => 13,
				'cep' => '78175-000',
			),
			358 => 
			array (
				'id' => 4439,
				'nome' => 'Pontal do Araguaia',
				'uf' => 'MT',
				'cep2' => '5106653',
				'estado_cod' => 13,
				'cep' => '78698-000',
			),
			359 => 
			array (
				'id' => 4440,
				'nome' => 'Ponte Branca',
				'uf' => 'MT',
				'cep2' => '5106703',
				'estado_cod' => 13,
				'cep' => '78610-000',
			),
			360 => 
			array (
				'id' => 4442,
				'nome' => 'Pontes e Lacerda',
				'uf' => 'MT',
				'cep2' => '5106752',
				'estado_cod' => 13,
				'cep' => '78250-000',
			),
			361 => 
			array (
				'id' => 4444,
				'nome' => 'Porto Alegre do Norte',
				'uf' => 'MT',
				'cep2' => '5106778',
				'estado_cod' => 13,
				'cep' => '78655-000',
			),
			362 => 
			array (
				'id' => 4445,
				'nome' => 'Porto dos Gaúchos',
				'uf' => 'MT',
				'cep2' => '5106802',
				'estado_cod' => 13,
				'cep' => '78560-000',
			),
			363 => 
			array (
				'id' => 4446,
				'nome' => 'Porto Esperidião',
				'uf' => 'MT',
				'cep2' => '5106828',
				'estado_cod' => 13,
				'cep' => '78240-000',
			),
			364 => 
			array (
				'id' => 4447,
				'nome' => 'Porto Estrela',
				'uf' => 'MT',
				'cep2' => '5106851',
				'estado_cod' => 13,
				'cep' => '78398-000',
			),
			365 => 
			array (
				'id' => 4448,
				'nome' => 'Poxoréu',
				'uf' => 'MT',
				'cep2' => '',
				'estado_cod' => 13,
				'cep' => '78800-000',
			),
			366 => 
			array (
				'id' => 4451,
				'nome' => 'Primavera do Leste',
				'uf' => 'MT',
				'cep2' => '5107040',
				'estado_cod' => 13,
				'cep' => '78850-000',
			),
			367 => 
			array (
				'id' => 4453,
				'nome' => 'Querência',
				'uf' => 'MT',
				'cep2' => '5107065',
				'estado_cod' => 13,
				'cep' => '78643-000',
			),
			368 => 
			array (
				'id' => 4455,
				'nome' => 'Reserva do Cabaçal',
				'uf' => 'MT',
				'cep2' => '5107156',
				'estado_cod' => 13,
				'cep' => '78265-000',
			),
			369 => 
			array (
				'id' => 4456,
				'nome' => 'Ribeirão Cascalheira',
				'uf' => 'MT',
				'cep2' => '5107180',
				'estado_cod' => 13,
				'cep' => '78675-000',
			),
			370 => 
			array (
				'id' => 4458,
				'nome' => 'Ribeirãozinho',
				'uf' => 'MT',
				'cep2' => '5107198',
				'estado_cod' => 13,
				'cep' => '78613-000',
			),
			371 => 
			array (
				'id' => 4459,
				'nome' => 'Rio Branco',
				'uf' => 'MT',
				'cep2' => '5107206',
				'estado_cod' => 13,
				'cep' => '78275-000',
			),
			372 => 
			array (
				'id' => 4462,
				'nome' => 'Rondonópolis',
				'uf' => 'MT',
				'cep2' => '5107602',
				'estado_cod' => 13,
				'cep' => 'LOC',
			),
			373 => 
			array (
				'id' => 4463,
				'nome' => 'Rosário Oeste',
				'uf' => 'MT',
				'cep2' => '5107701',
				'estado_cod' => 13,
				'cep' => '78470-000',
			),
			374 => 
			array (
				'id' => 4464,
				'nome' => 'Salto do Céu',
				'uf' => 'MT',
				'cep2' => '5107750',
				'estado_cod' => 13,
				'cep' => '78270-000',
			),
			375 => 
			array (
				'id' => 4466,
				'nome' => 'Santa Carmem',
				'uf' => 'MT',
				'cep2' => '5107248',
				'estado_cod' => 13,
				'cep' => '78545-000',
			),
			376 => 
			array (
				'id' => 4470,
				'nome' => 'Santa Terezinha',
				'uf' => 'MT',
				'cep2' => '5107776',
				'estado_cod' => 13,
				'cep' => '78650-000',
			),
			377 => 
			array (
				'id' => 4472,
				'nome' => 'Santo Afonso',
				'uf' => 'MT',
				'cep2' => '5107263',
				'estado_cod' => 13,
				'cep' => '78425-000',
			),
			378 => 
			array (
				'id' => 4473,
				'nome' => 'Santo Antônio do Leverger',
				'uf' => 'MT',
				'cep2' => '5107800',
				'estado_cod' => 13,
				'cep' => '78180-000',
			),
			379 => 
			array (
				'id' => 4476,
				'nome' => 'Vale de São Domingos',
				'uf' => 'MT',
				'cep2' => '5108352',
				'estado_cod' => 13,
				'cep' => '78253-000',
			),
			380 => 
			array (
				'id' => 4477,
				'nome' => 'São Félix do Araguaia',
				'uf' => 'MT',
				'cep2' => '5107859',
				'estado_cod' => 13,
				'cep' => '78670-000',
			),
			381 => 
			array (
				'id' => 4483,
				'nome' => 'São José do Povo',
				'uf' => 'MT',
				'cep2' => '5107297',
				'estado_cod' => 13,
				'cep' => '78773-000',
			),
			382 => 
			array (
				'id' => 4484,
				'nome' => 'São José do Rio Claro',
				'uf' => 'MT',
				'cep2' => '5107305',
				'estado_cod' => 13,
				'cep' => '78435-000',
			),
			383 => 
			array (
				'id' => 4485,
				'nome' => 'São José do Xingu',
				'uf' => 'MT',
				'cep2' => '5107354',
				'estado_cod' => 13,
				'cep' => '78663-000',
			),
			384 => 
			array (
				'id' => 4486,
				'nome' => 'São José dos Quatro Marcos',
				'uf' => 'MT',
				'cep2' => '5107107',
				'estado_cod' => 13,
				'cep' => '78285-000',
			),
			385 => 
			array (
				'id' => 4488,
				'nome' => 'São Pedro da Cipa',
				'uf' => 'MT',
				'cep2' => '5107404',
				'estado_cod' => 13,
				'cep' => '78835-000',
			),
			386 => 
			array (
				'id' => 4490,
				'nome' => 'Sapezal',
				'uf' => 'MT',
				'cep2' => '5107875',
				'estado_cod' => 13,
				'cep' => '78365-000',
			),
			387 => 
			array (
				'id' => 4492,
				'nome' => 'Serra Nova Dourada',
				'uf' => 'MT',
				'cep2' => '5107883',
				'estado_cod' => 13,
				'cep' => '78668-000',
			),
			388 => 
			array (
				'id' => 4493,
				'nome' => 'Sinop',
				'uf' => 'MT',
				'cep2' => '5107909',
				'estado_cod' => 13,
				'cep' => 'LOC',
			),
			389 => 
			array (
				'id' => 4495,
				'nome' => 'Sorriso',
				'uf' => 'MT',
				'cep2' => '5107925',
				'estado_cod' => 13,
				'cep' => '78890-000',
			),
			390 => 
			array (
				'id' => 4497,
				'nome' => 'Tabaporã',
				'uf' => 'MT',
				'cep2' => '5107941',
				'estado_cod' => 13,
				'cep' => '78563-000',
			),
			391 => 
			array (
				'id' => 4498,
				'nome' => 'Tangará da Serra',
				'uf' => 'MT',
				'cep2' => '5107958',
				'estado_cod' => 13,
				'cep' => '78300-000',
			),
			392 => 
			array (
				'id' => 4500,
				'nome' => 'Tapurah',
				'uf' => 'MT',
				'cep2' => '5108006',
				'estado_cod' => 13,
				'cep' => '78573-000',
			),
			393 => 
			array (
				'id' => 4501,
				'nome' => 'Terra Nova do Norte',
				'uf' => 'MT',
				'cep2' => '5108055',
				'estado_cod' => 13,
				'cep' => '78505-000',
			),
			394 => 
			array (
				'id' => 4503,
				'nome' => 'Tesouro',
				'uf' => 'MT',
				'cep2' => '5108105',
				'estado_cod' => 13,
				'cep' => '78775-000',
			),
			395 => 
			array (
				'id' => 4505,
				'nome' => 'Torixoréu',
				'uf' => 'MT',
				'cep2' => '5108204',
				'estado_cod' => 13,
				'cep' => '78695-000',
			),
			396 => 
			array (
				'id' => 4507,
				'nome' => 'União do Sul',
				'uf' => 'MT',
				'cep2' => '5108303',
				'estado_cod' => 13,
				'cep' => '78543-000',
			),
			397 => 
			array (
				'id' => 4511,
				'nome' => 'Várzea Grande',
				'uf' => 'MT',
				'cep2' => '5108402',
				'estado_cod' => 13,
				'cep' => 'LOC',
			),
			398 => 
			array (
				'id' => 4512,
				'nome' => 'Vera',
				'uf' => 'MT',
				'cep2' => '5108501',
				'estado_cod' => 13,
				'cep' => '78880-000',
			),
			399 => 
			array (
				'id' => 4514,
				'nome' => 'Vila Bela da Santíssima Trindade',
				'uf' => 'MT',
				'cep2' => '5105507',
				'estado_cod' => 13,
				'cep' => '78245-000',
			),
			400 => 
			array (
				'id' => 4520,
				'nome' => 'Vila Rica',
				'uf' => 'MT',
				'cep2' => '5108600',
				'estado_cod' => 13,
				'cep' => '78645-000',
			),
			401 => 
			array (
				'id' => 4521,
				'nome' => 'Novo Santo Antônio',
				'uf' => 'MT',
				'cep2' => '2206951',
				'estado_cod' => 13,
				'cep' => '78674-000',
			),
			402 => 
			array (
				'id' => 4522,
				'nome' => 'Abaetetuba',
				'uf' => 'PA',
				'cep2' => '1500107',
				'estado_cod' => 14,
				'cep' => '68440-000',
			),
			403 => 
			array (
				'id' => 4523,
				'nome' => 'Abel Figueiredo',
				'uf' => 'PA',
				'cep2' => '1500131',
				'estado_cod' => 14,
				'cep' => '68527-000',
			),
			404 => 
			array (
				'id' => 4524,
				'nome' => 'Acará',
				'uf' => 'PA',
				'cep2' => '1500206',
				'estado_cod' => 14,
				'cep' => '68690-000',
			),
			405 => 
			array (
				'id' => 4525,
				'nome' => 'Afuá',
				'uf' => 'PA',
				'cep2' => '1500305',
				'estado_cod' => 14,
				'cep' => '68890-000',
			),
			406 => 
			array (
				'id' => 4527,
				'nome' => 'Água Azul do Norte',
				'uf' => 'PA',
				'cep2' => '1500347',
				'estado_cod' => 14,
				'cep' => '68533-000',
			),
			407 => 
			array (
				'id' => 4529,
				'nome' => 'Alenquer',
				'uf' => 'PA',
				'cep2' => '1500404',
				'estado_cod' => 14,
				'cep' => '68200-000',
			),
			408 => 
			array (
				'id' => 4531,
				'nome' => 'Almeirim',
				'uf' => 'PA',
				'cep2' => '1500503',
				'estado_cod' => 14,
				'cep' => '68230-000',
			),
			409 => 
			array (
				'id' => 4534,
				'nome' => 'Altamira',
				'uf' => 'PA',
				'cep2' => '1500602',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			410 => 
			array (
				'id' => 4538,
				'nome' => 'Anajás',
				'uf' => 'PA',
				'cep2' => '1500701',
				'estado_cod' => 14,
				'cep' => '68810-000',
			),
			411 => 
			array (
				'id' => 4539,
				'nome' => 'Ananindeua',
				'uf' => 'PA',
				'cep2' => '1500800',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			412 => 
			array (
				'id' => 4540,
				'nome' => 'Anapu',
				'uf' => 'PA',
				'cep2' => '1500859',
				'estado_cod' => 14,
				'cep' => '68365-000',
			),
			413 => 
			array (
				'id' => 4552,
				'nome' => 'Augusto Corrêa',
				'uf' => 'PA',
				'cep2' => '1500909',
				'estado_cod' => 14,
				'cep' => '68610-000',
			),
			414 => 
			array (
				'id' => 4553,
				'nome' => 'Aurora do Pará',
				'uf' => 'PA',
				'cep2' => '1500958',
				'estado_cod' => 14,
				'cep' => '68658-000',
			),
			415 => 
			array (
				'id' => 4554,
				'nome' => 'Aveiro',
				'uf' => 'PA',
				'cep2' => '1501006',
				'estado_cod' => 14,
				'cep' => '68150-000',
			),
			416 => 
			array (
				'id' => 4555,
				'nome' => 'Bagre',
				'uf' => 'PA',
				'cep2' => '1501105',
				'estado_cod' => 14,
				'cep' => '68475-000',
			),
			417 => 
			array (
				'id' => 4556,
				'nome' => 'Baião',
				'uf' => 'PA',
				'cep2' => '1501204',
				'estado_cod' => 14,
				'cep' => '68465-000',
			),
			418 => 
			array (
				'id' => 4557,
				'nome' => 'Bannach',
				'uf' => 'PA',
				'cep2' => '1501253',
				'estado_cod' => 14,
				'cep' => '68388-000',
			),
			419 => 
			array (
				'id' => 4558,
				'nome' => 'Barcarena',
				'uf' => 'PA',
				'cep2' => '1501303',
				'estado_cod' => 14,
				'cep' => '68445-000',
			),
			420 => 
			array (
				'id' => 4565,
				'nome' => 'Belém',
				'uf' => 'PA',
				'cep2' => '1501402',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			421 => 
			array (
				'id' => 4566,
				'nome' => 'Belterra',
				'uf' => 'PA',
				'cep2' => '1501451',
				'estado_cod' => 14,
				'cep' => '68143-000',
			),
			422 => 
			array (
				'id' => 4567,
				'nome' => 'Benevides',
				'uf' => 'PA',
				'cep2' => '1501501',
				'estado_cod' => 14,
				'cep' => '68795-000',
			),
			423 => 
			array (
				'id' => 4576,
				'nome' => 'Bom Jesus do Tocantins',
				'uf' => 'PA',
				'cep2' => '1501576',
				'estado_cod' => 14,
				'cep' => '68525-000',
			),
			424 => 
			array (
				'id' => 4577,
				'nome' => 'Bonito',
				'uf' => 'PA',
				'cep2' => '1501600',
				'estado_cod' => 14,
				'cep' => '68645-000',
			),
			425 => 
			array (
				'id' => 4578,
				'nome' => 'Bragança',
				'uf' => 'PA',
				'cep2' => '1501709',
				'estado_cod' => 14,
				'cep' => '68600-000',
			),
			426 => 
			array (
				'id' => 4579,
				'nome' => 'Brasil Novo',
				'uf' => 'PA',
				'cep2' => '1501725',
				'estado_cod' => 14,
				'cep' => '68148-000',
			),
			427 => 
			array (
				'id' => 4582,
				'nome' => 'Brejo Grande do Araguaia',
				'uf' => 'PA',
				'cep2' => '1501758',
				'estado_cod' => 14,
				'cep' => '68521-000',
			),
			428 => 
			array (
				'id' => 4583,
				'nome' => 'Breu Branco',
				'uf' => 'PA',
				'cep2' => '1501782',
				'estado_cod' => 14,
				'cep' => '68488-000',
			),
			429 => 
			array (
				'id' => 4584,
				'nome' => 'Breves',
				'uf' => 'PA',
				'cep2' => '1501808',
				'estado_cod' => 14,
				'cep' => '68800-000',
			),
			430 => 
			array (
				'id' => 4585,
				'nome' => 'Bujaru',
				'uf' => 'PA',
				'cep2' => '1501907',
				'estado_cod' => 14,
				'cep' => '68670-000',
			),
			431 => 
			array (
				'id' => 4586,
				'nome' => 'Cachoeira do Piriá',
				'uf' => 'PA',
				'cep2' => '1501956',
				'estado_cod' => 14,
				'cep' => '68617-000',
			),
			432 => 
			array (
				'id' => 4587,
				'nome' => 'Cachoeira do Arari',
				'uf' => 'PA',
				'cep2' => '1502004',
				'estado_cod' => 14,
				'cep' => '68840-000',
			),
			433 => 
			array (
				'id' => 4593,
				'nome' => 'Cametá',
				'uf' => 'PA',
				'cep2' => '1502103',
				'estado_cod' => 14,
				'cep' => '68400-000',
			),
			434 => 
			array (
				'id' => 4595,
				'nome' => 'Canaã dos Carajás',
				'uf' => 'PA',
				'cep2' => '1502152',
				'estado_cod' => 14,
				'cep' => '68537-000',
			),
			435 => 
			array (
				'id' => 4596,
				'nome' => 'Capanema',
				'uf' => 'PA',
				'cep2' => '1502202',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			436 => 
			array (
				'id' => 4597,
				'nome' => 'Capitão Poço',
				'uf' => 'PA',
				'cep2' => '1502301',
				'estado_cod' => 14,
				'cep' => '68650-000',
			),
			437 => 
			array (
				'id' => 4605,
				'nome' => 'Castanhal',
				'uf' => 'PA',
				'cep2' => '1502400',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			438 => 
			array (
				'id' => 4607,
				'nome' => 'Chaves',
				'uf' => 'PA',
				'cep2' => '1502509',
				'estado_cod' => 14,
				'cep' => '68880-000',
			),
			439 => 
			array (
				'id' => 4608,
				'nome' => 'Colares',
				'uf' => 'PA',
				'cep2' => '1502608',
				'estado_cod' => 14,
				'cep' => '68785-000',
			),
			440 => 
			array (
				'id' => 4610,
				'nome' => 'Conceição do Araguaia',
				'uf' => 'PA',
				'cep2' => '1502707',
				'estado_cod' => 14,
				'cep' => '68540-000',
			),
			441 => 
			array (
				'id' => 4611,
				'nome' => 'Concórdia do Pará',
				'uf' => 'PA',
				'cep2' => '1502756',
				'estado_cod' => 14,
				'cep' => '68685-000',
			),
			442 => 
			array (
				'id' => 4617,
				'nome' => 'Cumaru do Norte',
				'uf' => 'PA',
				'cep2' => '1502764',
				'estado_cod' => 14,
				'cep' => '68398-000',
			),
			443 => 
			array (
				'id' => 4618,
				'nome' => 'Curionópolis',
				'uf' => 'PA',
				'cep2' => '1502772',
				'estado_cod' => 14,
				'cep' => '68523-000',
			),
			444 => 
			array (
				'id' => 4619,
				'nome' => 'Curralinho',
				'uf' => 'PA',
				'cep2' => '1502806',
				'estado_cod' => 14,
				'cep' => '68815-000',
			),
			445 => 
			array (
				'id' => 4620,
				'nome' => 'Curuá',
				'uf' => 'PA',
				'cep2' => '1502855',
				'estado_cod' => 14,
				'cep' => '68210-000',
			),
			446 => 
			array (
				'id' => 4622,
				'nome' => 'Curuçá',
				'uf' => 'PA',
				'cep2' => '1502905',
				'estado_cod' => 14,
				'cep' => '68750-000',
			),
			447 => 
			array (
				'id' => 4625,
				'nome' => 'Dom Eliseu',
				'uf' => 'PA',
				'cep2' => '1502939',
				'estado_cod' => 14,
				'cep' => '68633-000',
			),
			448 => 
			array (
				'id' => 4626,
				'nome' => 'Eldorado dos Carajás',
				'uf' => 'PA',
				'cep2' => '1502954',
				'estado_cod' => 14,
				'cep' => '68524-000',
			),
			449 => 
			array (
				'id' => 4629,
				'nome' => 'Faro',
				'uf' => 'PA',
				'cep2' => '1503002',
				'estado_cod' => 14,
				'cep' => '68280-000',
			),
			450 => 
			array (
				'id' => 4633,
				'nome' => 'Floresta do Araguaia',
				'uf' => 'PA',
				'cep2' => '1503044',
				'estado_cod' => 14,
				'cep' => '68543-000',
			),
			451 => 
			array (
				'id' => 4634,
				'nome' => 'Garrafão do Norte',
				'uf' => 'PA',
				'cep2' => '1503077',
				'estado_cod' => 14,
				'cep' => '68665-000',
			),
			452 => 
			array (
				'id' => 4635,
				'nome' => 'Goianésia do Pará',
				'uf' => 'PA',
				'cep2' => '1503093',
				'estado_cod' => 14,
				'cep' => '68639-000',
			),
			453 => 
			array (
				'id' => 4639,
				'nome' => 'Gurupá',
				'uf' => 'PA',
				'cep2' => '1503101',
				'estado_cod' => 14,
				'cep' => '68300-000',
			),
			454 => 
			array (
				'id' => 4645,
				'nome' => 'Igarapé-Açu',
				'uf' => 'PA',
				'cep2' => '1503200',
				'estado_cod' => 14,
				'cep' => '68725-000',
			),
			455 => 
			array (
				'id' => 4646,
				'nome' => 'Igarapé-Miri',
				'uf' => 'PA',
				'cep2' => '1503309',
				'estado_cod' => 14,
				'cep' => '68430-000',
			),
			456 => 
			array (
				'id' => 4648,
				'nome' => 'Inhangapi',
				'uf' => 'PA',
				'cep2' => '1503408',
				'estado_cod' => 14,
				'cep' => '68770-000',
			),
			457 => 
			array (
				'id' => 4649,
				'nome' => 'Ipixuna do Pará',
				'uf' => 'PA',
				'cep2' => '1503457',
				'estado_cod' => 14,
				'cep' => '68637-000',
			),
			458 => 
			array (
				'id' => 4650,
				'nome' => 'Irituia',
				'uf' => 'PA',
				'cep2' => '1503507',
				'estado_cod' => 14,
				'cep' => '68655-000',
			),
			459 => 
			array (
				'id' => 4651,
				'nome' => 'Itaituba',
				'uf' => 'PA',
				'cep2' => '1503606',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			460 => 
			array (
				'id' => 4654,
				'nome' => 'Itupiranga',
				'uf' => 'PA',
				'cep2' => '1503705',
				'estado_cod' => 14,
				'cep' => '68580-000',
			),
			461 => 
			array (
				'id' => 4655,
				'nome' => 'Jacareacanga',
				'uf' => 'PA',
				'cep2' => '1503754',
				'estado_cod' => 14,
				'cep' => '68195-000',
			),
			462 => 
			array (
				'id' => 4656,
				'nome' => 'Jacundá',
				'uf' => 'PA',
				'cep2' => '1503804',
				'estado_cod' => 14,
				'cep' => '68590-000',
			),
			463 => 
			array (
				'id' => 4667,
				'nome' => 'Juruti',
				'uf' => 'PA',
				'cep2' => '1503903',
				'estado_cod' => 14,
				'cep' => '68170-000',
			),
			464 => 
			array (
				'id' => 4672,
				'nome' => 'Limoeiro do Ajuru',
				'uf' => 'PA',
				'cep2' => '1504000',
				'estado_cod' => 14,
				'cep' => '68415-000',
			),
			465 => 
			array (
				'id' => 4673,
				'nome' => 'Mãe do Rio',
				'uf' => 'PA',
				'cep2' => '1504059',
				'estado_cod' => 14,
				'cep' => '68675-000',
			),
			466 => 
			array (
				'id' => 4674,
				'nome' => 'Magalhães Barata',
				'uf' => 'PA',
				'cep2' => '1504109',
				'estado_cod' => 14,
				'cep' => '68722-000',
			),
			467 => 
			array (
				'id' => 4677,
				'nome' => 'Marabá',
				'uf' => 'PA',
				'cep2' => '1504208',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			468 => 
			array (
				'id' => 4678,
				'nome' => 'Maracanã',
				'uf' => 'PA',
				'cep2' => '1504307',
				'estado_cod' => 14,
				'cep' => '68710-000',
			),
			469 => 
			array (
				'id' => 4680,
				'nome' => 'Marapanim',
				'uf' => 'PA',
				'cep2' => '1504406',
				'estado_cod' => 14,
				'cep' => '68760-000',
			),
			470 => 
			array (
				'id' => 4681,
				'nome' => 'Marituba',
				'uf' => 'PA',
				'cep2' => '1504422',
				'estado_cod' => 14,
				'cep' => '67200-000',
			),
			471 => 
			array (
				'id' => 4685,
				'nome' => 'Medicilândia',
				'uf' => 'PA',
				'cep2' => '1504455',
				'estado_cod' => 14,
				'cep' => '68145-000',
			),
			472 => 
			array (
				'id' => 4686,
				'nome' => 'Melgaço',
				'uf' => 'PA',
				'cep2' => '1504505',
				'estado_cod' => 14,
				'cep' => '68490-000',
			),
			473 => 
			array (
				'id' => 4691,
				'nome' => 'Mocajuba',
				'uf' => 'PA',
				'cep2' => '1504604',
				'estado_cod' => 14,
				'cep' => '68420-000',
			),
			474 => 
			array (
				'id' => 4693,
				'nome' => 'Moju',
				'uf' => 'PA',
				'cep2' => '1504703',
				'estado_cod' => 14,
				'cep' => '68450-000',
			),
			475 => 
			array (
				'id' => 4695,
				'nome' => 'Monte Alegre',
				'uf' => 'PA',
				'cep2' => '1504802',
				'estado_cod' => 14,
				'cep' => '68220-000',
			),
			476 => 
			array (
				'id' => 4700,
				'nome' => 'Muaná',
				'uf' => 'PA',
				'cep2' => '1504901',
				'estado_cod' => 14,
				'cep' => '68825-000',
			),
			477 => 
			array (
				'id' => 4709,
				'nome' => 'Nova Esperança do Piriá',
				'uf' => 'PA',
				'cep2' => '1504950',
				'estado_cod' => 14,
				'cep' => '68618-000',
			),
			478 => 
			array (
				'id' => 4710,
				'nome' => 'Nova Ipixuna',
				'uf' => 'PA',
				'cep2' => '1504976',
				'estado_cod' => 14,
				'cep' => '68585-000',
			),
			479 => 
			array (
				'id' => 4712,
				'nome' => 'Nova Timboteua',
				'uf' => 'PA',
				'cep2' => '1505007',
				'estado_cod' => 14,
				'cep' => '68730-000',
			),
			480 => 
			array (
				'id' => 4714,
				'nome' => 'Novo Progresso',
				'uf' => 'PA',
				'cep2' => '1505031',
				'estado_cod' => 14,
				'cep' => '68193-000',
			),
			481 => 
			array (
				'id' => 4715,
				'nome' => 'Novo Repartimento',
				'uf' => 'PA',
				'cep2' => '1505064',
				'estado_cod' => 14,
				'cep' => '68473-000',
			),
			482 => 
			array (
				'id' => 4717,
				'nome' => 'Óbidos',
				'uf' => 'PA',
				'cep2' => '1505106',
				'estado_cod' => 14,
				'cep' => '68250-000',
			),
			483 => 
			array (
				'id' => 4718,
				'nome' => 'Oeiras do Pará',
				'uf' => 'PA',
				'cep2' => '1505205',
				'estado_cod' => 14,
				'cep' => '68470-000',
			),
			484 => 
			array (
				'id' => 4719,
				'nome' => 'Oriximiná',
				'uf' => 'PA',
				'cep2' => '1505304',
				'estado_cod' => 14,
				'cep' => '68270-000',
			),
			485 => 
			array (
				'id' => 4722,
				'nome' => 'Ourém',
				'uf' => 'PA',
				'cep2' => '1505403',
				'estado_cod' => 14,
				'cep' => '68640-000',
			),
			486 => 
			array (
				'id' => 4723,
				'nome' => 'Ourilândia do Norte',
				'uf' => 'PA',
				'cep2' => '1505437',
				'estado_cod' => 14,
				'cep' => '68390-000',
			),
			487 => 
			array (
				'id' => 4725,
				'nome' => 'Pacajá',
				'uf' => 'PA',
				'cep2' => '1505486',
				'estado_cod' => 14,
				'cep' => '68485-000',
			),
			488 => 
			array (
				'id' => 4727,
				'nome' => 'Palestina do Pará',
				'uf' => 'PA',
				'cep2' => '1505494',
				'estado_cod' => 14,
				'cep' => '68535-000',
			),
			489 => 
			array (
				'id' => 4728,
				'nome' => 'Paragominas',
				'uf' => 'PA',
				'cep2' => '1505502',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			490 => 
			array (
				'id' => 4730,
				'nome' => 'Parauapebas',
				'uf' => 'PA',
				'cep2' => '1505536',
				'estado_cod' => 14,
				'cep' => '68515-000',
			),
			491 => 
			array (
				'id' => 4731,
				'nome' => 'Pau D\'Arco',
				'uf' => 'PA',
				'cep2' => '1505551',
				'estado_cod' => 14,
				'cep' => '68545-000',
			),
			492 => 
			array (
				'id' => 4733,
				'nome' => 'Peixe-Boi',
				'uf' => 'PA',
				'cep2' => '1505601',
				'estado_cod' => 14,
				'cep' => '68734-000',
			),
			493 => 
			array (
				'id' => 4738,
				'nome' => 'Piçarra',
				'uf' => 'PA',
				'cep2' => '1505635',
				'estado_cod' => 14,
				'cep' => '68575-000',
			),
			494 => 
			array (
				'id' => 4743,
				'nome' => 'Placas',
				'uf' => 'PA',
				'cep2' => '1505650',
				'estado_cod' => 14,
				'cep' => '68138-000',
			),
			495 => 
			array (
				'id' => 4744,
				'nome' => 'Ponta de Pedras',
				'uf' => 'PA',
				'cep2' => '1505700',
				'estado_cod' => 14,
				'cep' => '68830-000',
			),
			496 => 
			array (
				'id' => 4746,
				'nome' => 'Portel',
				'uf' => 'PA',
				'cep2' => '1505809',
				'estado_cod' => 14,
				'cep' => '68480-000',
			),
			497 => 
			array (
				'id' => 4747,
				'nome' => 'Porto de Moz',
				'uf' => 'PA',
				'cep2' => '1505908',
				'estado_cod' => 14,
				'cep' => '68330-000',
			),
			498 => 
			array (
				'id' => 4750,
				'nome' => 'Prainha',
				'uf' => 'PA',
				'cep2' => '1506005',
				'estado_cod' => 14,
				'cep' => '68130-000',
			),
			499 => 
			array (
				'id' => 4751,
				'nome' => 'Primavera',
				'uf' => 'PA',
				'cep2' => '1506104',
				'estado_cod' => 14,
				'cep' => '68707-000',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 4752,
				'nome' => 'Quatipuru',
				'uf' => 'PA',
				'cep2' => '1506112',
				'estado_cod' => 14,
				'cep' => '68709-000',
			),
			1 => 
			array (
				'id' => 4754,
				'nome' => 'Redenção',
				'uf' => 'PA',
				'cep2' => '1506138',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			2 => 
			array (
				'id' => 4757,
				'nome' => 'Rio Maria',
				'uf' => 'PA',
				'cep2' => '1506161',
				'estado_cod' => 14,
				'cep' => '68530-000',
			),
			3 => 
			array (
				'id' => 4760,
				'nome' => 'Rondon do Pará',
				'uf' => 'PA',
				'cep2' => '1506187',
				'estado_cod' => 14,
				'cep' => '68638-000',
			),
			4 => 
			array (
				'id' => 4761,
				'nome' => 'Rurópolis',
				'uf' => 'PA',
				'cep2' => '1506195',
				'estado_cod' => 14,
				'cep' => '68165-000',
			),
			5 => 
			array (
				'id' => 4762,
				'nome' => 'Salinópolis',
				'uf' => 'PA',
				'cep2' => '1506203',
				'estado_cod' => 14,
				'cep' => '68721-000',
			),
			6 => 
			array (
				'id' => 4763,
				'nome' => 'Salvaterra',
				'uf' => 'PA',
				'cep2' => '1506302',
				'estado_cod' => 14,
				'cep' => '68860-000',
			),
			7 => 
			array (
				'id' => 4764,
				'nome' => 'Santa Bárbara do Pará',
				'uf' => 'PA',
				'cep2' => '1506351',
				'estado_cod' => 14,
				'cep' => '68798-000',
			),
			8 => 
			array (
				'id' => 4765,
				'nome' => 'Santa Cruz do Arari',
				'uf' => 'PA',
				'cep2' => '1506401',
				'estado_cod' => 14,
				'cep' => '68850-000',
			),
			9 => 
			array (
				'id' => 4766,
				'nome' => 'Santa Isabel do Pará',
				'uf' => 'PA',
				'cep2' => '1506500',
				'estado_cod' => 14,
				'cep' => '68790-000',
			),
			10 => 
			array (
				'id' => 4767,
				'nome' => 'Santa Luzia do Pará',
				'uf' => 'PA',
				'cep2' => '1506559',
				'estado_cod' => 14,
				'cep' => '68644-000',
			),
			11 => 
			array (
				'id' => 4769,
				'nome' => 'Santa Maria das Barreiras',
				'uf' => 'PA',
				'cep2' => '1506583',
				'estado_cod' => 14,
				'cep' => '68565-000',
			),
			12 => 
			array (
				'id' => 4770,
				'nome' => 'Santa Maria do Pará',
				'uf' => 'PA',
				'cep2' => '1506609',
				'estado_cod' => 14,
				'cep' => '68738-000',
			),
			13 => 
			array (
				'id' => 4773,
				'nome' => 'Santana do Araguaia',
				'uf' => 'PA',
				'cep2' => '1506708',
				'estado_cod' => 14,
				'cep' => '68560-000',
			),
			14 => 
			array (
				'id' => 4774,
				'nome' => 'Santarém',
				'uf' => 'PA',
				'cep2' => '1506807',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			15 => 
			array (
				'id' => 4775,
				'nome' => 'Santarém Novo',
				'uf' => 'PA',
				'cep2' => '1506906',
				'estado_cod' => 14,
				'cep' => '68720-000',
			),
			16 => 
			array (
				'id' => 4777,
				'nome' => 'Santo Antônio do Tauá',
				'uf' => 'PA',
				'cep2' => '1507003',
				'estado_cod' => 14,
				'cep' => '68786-000',
			),
			17 => 
			array (
				'id' => 4778,
				'nome' => 'São Caetano de Odivelas',
				'uf' => 'PA',
				'cep2' => '1507102',
				'estado_cod' => 14,
				'cep' => '68775-000',
			),
			18 => 
			array (
				'id' => 4779,
				'nome' => 'São Domingos do Araguaia',
				'uf' => 'PA',
				'cep2' => '1507151',
				'estado_cod' => 14,
				'cep' => '68520-000',
			),
			19 => 
			array (
				'id' => 4780,
				'nome' => 'São Domingos do Capim',
				'uf' => 'PA',
				'cep2' => '1507201',
				'estado_cod' => 14,
				'cep' => '68635-000',
			),
			20 => 
			array (
				'id' => 4781,
				'nome' => 'São Félix do Xingu',
				'uf' => 'PA',
				'cep2' => '1507300',
				'estado_cod' => 14,
				'cep' => '68380-000',
			),
			21 => 
			array (
				'id' => 4784,
				'nome' => 'São Francisco do Pará',
				'uf' => 'PA',
				'cep2' => '1507409',
				'estado_cod' => 14,
				'cep' => '68748-000',
			),
			22 => 
			array (
				'id' => 4785,
				'nome' => 'São Geraldo do Araguaia',
				'uf' => 'PA',
				'cep2' => '1507458',
				'estado_cod' => 14,
				'cep' => '68570-000',
			),
			23 => 
			array (
				'id' => 4786,
				'nome' => 'São João da Ponta',
				'uf' => 'PA',
				'cep2' => '1507466',
				'estado_cod' => 14,
				'cep' => '68774-000',
			),
			24 => 
			array (
				'id' => 4787,
				'nome' => 'São João de Pirabas',
				'uf' => 'PA',
				'cep2' => '1507474',
				'estado_cod' => 14,
				'cep' => '68719-000',
			),
			25 => 
			array (
				'id' => 4789,
				'nome' => 'São João do Araguaia',
				'uf' => 'PA',
				'cep2' => '1507508',
				'estado_cod' => 14,
				'cep' => '68518-000',
			),
			26 => 
			array (
				'id' => 4797,
				'nome' => 'São Miguel do Guamá',
				'uf' => 'PA',
				'cep2' => '1507607',
				'estado_cod' => 14,
				'cep' => '68660-000',
			),
			27 => 
			array (
				'id' => 4805,
				'nome' => 'São Sebastião da Boa Vista',
				'uf' => 'PA',
				'cep2' => '1507706',
				'estado_cod' => 14,
				'cep' => '68820-000',
			),
			28 => 
			array (
				'id' => 4807,
				'nome' => 'Sapucaia',
				'uf' => 'PA',
				'cep2' => '1507755',
				'estado_cod' => 14,
				'cep' => '68548-000',
			),
			29 => 
			array (
				'id' => 4808,
				'nome' => 'Senador José Porfírio',
				'uf' => 'PA',
				'cep2' => '1507805',
				'estado_cod' => 14,
				'cep' => '68360-000',
			),
			30 => 
			array (
				'id' => 4810,
				'nome' => 'Soure',
				'uf' => 'PA',
				'cep2' => '1507904',
				'estado_cod' => 14,
				'cep' => '68870-000',
			),
			31 => 
			array (
				'id' => 4811,
				'nome' => 'Tailândia',
				'uf' => 'PA',
				'cep2' => '1507953',
				'estado_cod' => 14,
				'cep' => '68695-000',
			),
			32 => 
			array (
				'id' => 4816,
				'nome' => 'Terra Alta',
				'uf' => 'PA',
				'cep2' => '1507961',
				'estado_cod' => 14,
				'cep' => '68773-000',
			),
			33 => 
			array (
				'id' => 4817,
				'nome' => 'Terra Santa',
				'uf' => 'PA',
				'cep2' => '1507979',
				'estado_cod' => 14,
				'cep' => '68285-000',
			),
			34 => 
			array (
				'id' => 4820,
				'nome' => 'Tomé-Açú',
				'uf' => 'PA',
				'cep2' => '',
				'estado_cod' => 14,
				'cep' => '68680-000',
			),
			35 => 
			array (
				'id' => 4821,
				'nome' => 'Tracuateua',
				'uf' => 'PA',
				'cep2' => '1508035',
				'estado_cod' => 14,
				'cep' => '68647-000',
			),
			36 => 
			array (
				'id' => 4822,
				'nome' => 'Trairão',
				'uf' => 'PA',
				'cep2' => '1508050',
				'estado_cod' => 14,
				'cep' => '68198-000',
			),
			37 => 
			array (
				'id' => 4823,
				'nome' => 'Tucumã',
				'uf' => 'PA',
				'cep2' => '1508084',
				'estado_cod' => 14,
				'cep' => '68385-000',
			),
			38 => 
			array (
				'id' => 4824,
				'nome' => 'Tucuruí',
				'uf' => 'PA',
				'cep2' => '1508100',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			39 => 
			array (
				'id' => 4825,
				'nome' => 'Ulianópolis',
				'uf' => 'PA',
				'cep2' => '1508126',
				'estado_cod' => 14,
				'cep' => '68632-000',
			),
			40 => 
			array (
				'id' => 4826,
				'nome' => 'Uruará',
				'uf' => 'PA',
				'cep2' => '1508159',
				'estado_cod' => 14,
				'cep' => '68140-000',
			),
			41 => 
			array (
				'id' => 4831,
				'nome' => 'Vigia',
				'uf' => 'PA',
				'cep2' => '1508209',
				'estado_cod' => 14,
				'cep' => '68780-000',
			),
			42 => 
			array (
				'id' => 4842,
				'nome' => 'Viseu',
				'uf' => 'PA',
				'cep2' => '1508308',
				'estado_cod' => 14,
				'cep' => '68620-000',
			),
			43 => 
			array (
				'id' => 4845,
				'nome' => 'Vitória do Xingu',
				'uf' => 'PA',
				'cep2' => '1508357',
				'estado_cod' => 14,
				'cep' => '68383-000',
			),
			44 => 
			array (
				'id' => 4846,
				'nome' => 'Xinguara',
				'uf' => 'PA',
				'cep2' => '1508407',
				'estado_cod' => 14,
				'cep' => 'LOC',
			),
			45 => 
			array (
				'id' => 4848,
				'nome' => 'Água Branca',
				'uf' => 'PB',
				'cep2' => '2500106',
				'estado_cod' => 15,
				'cep' => '58748-000',
			),
			46 => 
			array (
				'id' => 4849,
				'nome' => 'Aguiar',
				'uf' => 'PB',
				'cep2' => '2500205',
				'estado_cod' => 15,
				'cep' => '58778-000',
			),
			47 => 
			array (
				'id' => 4850,
				'nome' => 'Alagoa Grande',
				'uf' => 'PB',
				'cep2' => '2500304',
				'estado_cod' => 15,
				'cep' => '58388-000',
			),
			48 => 
			array (
				'id' => 4851,
				'nome' => 'Alagoa Nova',
				'uf' => 'PB',
				'cep2' => '2500403',
				'estado_cod' => 15,
				'cep' => '58125-000',
			),
			49 => 
			array (
				'id' => 4852,
				'nome' => 'Alagoinha',
				'uf' => 'PB',
				'cep2' => '2500502',
				'estado_cod' => 15,
				'cep' => '58390-000',
			),
			50 => 
			array (
				'id' => 4853,
				'nome' => 'Alcantil',
				'uf' => 'PB',
				'cep2' => '2500536',
				'estado_cod' => 15,
				'cep' => '58460-000',
			),
			51 => 
			array (
				'id' => 4854,
				'nome' => 'Algodão de Jandaíra',
				'uf' => 'PB',
				'cep2' => '2500577',
				'estado_cod' => 15,
				'cep' => '58399-000',
			),
			52 => 
			array (
				'id' => 4855,
				'nome' => 'Alhandra',
				'uf' => 'PB',
				'cep2' => '2500601',
				'estado_cod' => 15,
				'cep' => '58320-000',
			),
			53 => 
			array (
				'id' => 4856,
				'nome' => 'Amparo',
				'uf' => 'PB',
				'cep2' => '2500734',
				'estado_cod' => 15,
				'cep' => '58548-000',
			),
			54 => 
			array (
				'id' => 4857,
				'nome' => 'Aparecida',
				'uf' => 'PB',
				'cep2' => '2500775',
				'estado_cod' => 15,
				'cep' => '58823-000',
			),
			55 => 
			array (
				'id' => 4858,
				'nome' => 'Araçagi',
				'uf' => 'PB',
				'cep2' => '2500809',
				'estado_cod' => 15,
				'cep' => '58270-000',
			),
			56 => 
			array (
				'id' => 4859,
				'nome' => 'Arara',
				'uf' => 'PB',
				'cep2' => '2500908',
				'estado_cod' => 15,
				'cep' => '58396-000',
			),
			57 => 
			array (
				'id' => 4860,
				'nome' => 'Araruna',
				'uf' => 'PB',
				'cep2' => '2501005',
				'estado_cod' => 15,
				'cep' => '58233-000',
			),
			58 => 
			array (
				'id' => 4861,
				'nome' => 'Areia',
				'uf' => 'PB',
				'cep2' => '2501104',
				'estado_cod' => 15,
				'cep' => '58397-000',
			),
			59 => 
			array (
				'id' => 4862,
				'nome' => 'Areia de Baraúnas',
				'uf' => 'PB',
				'cep2' => '2501153',
				'estado_cod' => 15,
				'cep' => '58732-000',
			),
			60 => 
			array (
				'id' => 4863,
				'nome' => 'Areial',
				'uf' => 'PB',
				'cep2' => '2501203',
				'estado_cod' => 15,
				'cep' => '58140-000',
			),
			61 => 
			array (
				'id' => 4865,
				'nome' => 'Aroeiras',
				'uf' => 'PB',
				'cep2' => '2501302',
				'estado_cod' => 15,
				'cep' => '58489-000',
			),
			62 => 
			array (
				'id' => 4866,
				'nome' => 'Riachão do Bacamarte',
				'uf' => 'PB',
				'cep2' => '2512754',
				'estado_cod' => 15,
				'cep' => '58382-000',
			),
			63 => 
			array (
				'id' => 4867,
				'nome' => 'Assunção',
				'uf' => 'PB',
				'cep2' => '2501351',
				'estado_cod' => 15,
				'cep' => '58685-000',
			),
			64 => 
			array (
				'id' => 4868,
				'nome' => 'Baía da Traição',
				'uf' => 'PB',
				'cep2' => '2501401',
				'estado_cod' => 15,
				'cep' => '58295-000',
			),
			65 => 
			array (
				'id' => 4870,
				'nome' => 'Bananeiras',
				'uf' => 'PB',
				'cep2' => '2501500',
				'estado_cod' => 15,
				'cep' => '58220-000',
			),
			66 => 
			array (
				'id' => 4871,
				'nome' => 'Baraúna',
				'uf' => 'PB',
				'cep2' => '2501534',
				'estado_cod' => 15,
				'cep' => '58188-000',
			),
			67 => 
			array (
				'id' => 4872,
				'nome' => 'Barra de Santa Rosa',
				'uf' => 'PB',
				'cep2' => '2501609',
				'estado_cod' => 15,
				'cep' => '58170-000',
			),
			68 => 
			array (
				'id' => 4873,
				'nome' => 'Barra de Santana',
				'uf' => 'PB',
				'cep2' => '2501575',
				'estado_cod' => 15,
				'cep' => '58458-000',
			),
			69 => 
			array (
				'id' => 4874,
				'nome' => 'Barra de São Miguel',
				'uf' => 'PB',
				'cep2' => '2501708',
				'estado_cod' => 15,
				'cep' => '58483-000',
			),
			70 => 
			array (
				'id' => 4876,
				'nome' => 'Bayeux',
				'uf' => 'PB',
				'cep2' => '2501807',
				'estado_cod' => 15,
				'cep' => 'LOC',
			),
			71 => 
			array (
				'id' => 4877,
				'nome' => 'Belém',
				'uf' => 'PB',
				'cep2' => '2501906',
				'estado_cod' => 15,
				'cep' => '58255-000',
			),
			72 => 
			array (
				'id' => 4878,
				'nome' => 'Belém do Brejo do Cruz',
				'uf' => 'PB',
				'cep2' => '2502003',
				'estado_cod' => 15,
				'cep' => '58895-000',
			),
			73 => 
			array (
				'id' => 4879,
				'nome' => 'Bernardino Batista',
				'uf' => 'PB',
				'cep2' => '2502052',
				'estado_cod' => 15,
				'cep' => '58922-000',
			),
			74 => 
			array (
				'id' => 4880,
				'nome' => 'Boa Ventura',
				'uf' => 'PB',
				'cep2' => '2502102',
				'estado_cod' => 15,
				'cep' => '58993-000',
			),
			75 => 
			array (
				'id' => 4881,
				'nome' => 'Boa Vista',
				'uf' => 'PB',
				'cep2' => '2502151',
				'estado_cod' => 15,
				'cep' => '58123-000',
			),
			76 => 
			array (
				'id' => 4882,
				'nome' => 'Bom Jesus',
				'uf' => 'PB',
				'cep2' => '2502201',
				'estado_cod' => 15,
				'cep' => '58930-000',
			),
			77 => 
			array (
				'id' => 4883,
				'nome' => 'Bom Sucesso',
				'uf' => 'PB',
				'cep2' => '2502300',
				'estado_cod' => 15,
				'cep' => '58887-000',
			),
			78 => 
			array (
				'id' => 4885,
				'nome' => 'Bonito de Santa Fé',
				'uf' => 'PB',
				'cep2' => '2502409',
				'estado_cod' => 15,
				'cep' => '58960-000',
			),
			79 => 
			array (
				'id' => 4886,
				'nome' => 'Boqueirão',
				'uf' => 'PB',
				'cep2' => '2502508',
				'estado_cod' => 15,
				'cep' => '58450-000',
			),
			80 => 
			array (
				'id' => 4887,
				'nome' => 'Borborema',
				'uf' => 'PB',
				'cep2' => '2502706',
				'estado_cod' => 15,
				'cep' => '58394-000',
			),
			81 => 
			array (
				'id' => 4888,
				'nome' => 'Brejo do Cruz',
				'uf' => 'PB',
				'cep2' => '2502805',
				'estado_cod' => 15,
				'cep' => '58890-000',
			),
			82 => 
			array (
				'id' => 4889,
				'nome' => 'Brejo dos Santos',
				'uf' => 'PB',
				'cep2' => '2502904',
				'estado_cod' => 15,
				'cep' => '58880-000',
			),
			83 => 
			array (
				'id' => 4890,
				'nome' => 'Caaporã',
				'uf' => 'PB',
				'cep2' => '2503001',
				'estado_cod' => 15,
				'cep' => '58326-000',
			),
			84 => 
			array (
				'id' => 4891,
				'nome' => 'Cabaceiras',
				'uf' => 'PB',
				'cep2' => '2503100',
				'estado_cod' => 15,
				'cep' => '58480-000',
			),
			85 => 
			array (
				'id' => 4892,
				'nome' => 'Cabedelo',
				'uf' => 'PB',
				'cep2' => '2503209',
				'estado_cod' => 15,
				'cep' => '58310-000',
			),
			86 => 
			array (
				'id' => 4894,
				'nome' => 'Cachoeira dos Índios',
				'uf' => 'PB',
				'cep2' => '2503308',
				'estado_cod' => 15,
				'cep' => '58935-000',
			),
			87 => 
			array (
				'id' => 4896,
				'nome' => 'Cacimba de Areia',
				'uf' => 'PB',
				'cep2' => '2503407',
				'estado_cod' => 15,
				'cep' => '58730-000',
			),
			88 => 
			array (
				'id' => 4897,
				'nome' => 'Cacimba de Dentro',
				'uf' => 'PB',
				'cep2' => '2503506',
				'estado_cod' => 15,
				'cep' => '58230-000',
			),
			89 => 
			array (
				'id' => 4898,
				'nome' => 'Cacimbas',
				'uf' => 'PB',
				'cep2' => '2503555',
				'estado_cod' => 15,
				'cep' => '58698-000',
			),
			90 => 
			array (
				'id' => 4899,
				'nome' => 'Caiçara',
				'uf' => 'PB',
				'cep2' => '2503605',
				'estado_cod' => 15,
				'cep' => '58253-000',
			),
			91 => 
			array (
				'id' => 4900,
				'nome' => 'Cajazeiras',
				'uf' => 'PB',
				'cep2' => '2503704',
				'estado_cod' => 15,
				'cep' => '58900-000',
			),
			92 => 
			array (
				'id' => 4901,
				'nome' => 'Cajazeirinhas',
				'uf' => 'PB',
				'cep2' => '2503753',
				'estado_cod' => 15,
				'cep' => '58855-000',
			),
			93 => 
			array (
				'id' => 4902,
				'nome' => 'Caldas Brandão',
				'uf' => 'PB',
				'cep2' => '2503803',
				'estado_cod' => 15,
				'cep' => '58350-000',
			),
			94 => 
			array (
				'id' => 4903,
				'nome' => 'Camalaú',
				'uf' => 'PB',
				'cep2' => '2503902',
				'estado_cod' => 15,
				'cep' => '58530-000',
			),
			95 => 
			array (
				'id' => 4904,
				'nome' => 'Campina Grande',
				'uf' => 'PB',
				'cep2' => '2504009',
				'estado_cod' => 15,
				'cep' => 'LOC',
			),
			96 => 
			array (
				'id' => 4908,
				'nome' => 'Capim',
				'uf' => 'PB',
				'cep2' => '2504033',
				'estado_cod' => 15,
				'cep' => '58287-000',
			),
			97 => 
			array (
				'id' => 4909,
				'nome' => 'Caraúbas',
				'uf' => 'PB',
				'cep2' => '2504074',
				'estado_cod' => 15,
				'cep' => '58595-000',
			),
			98 => 
			array (
				'id' => 4911,
				'nome' => 'Carrapateira',
				'uf' => 'PB',
				'cep2' => '2504108',
				'estado_cod' => 15,
				'cep' => '58945-000',
			),
			99 => 
			array (
				'id' => 4913,
				'nome' => 'Casserengue',
				'uf' => 'PB',
				'cep2' => '2504157',
				'estado_cod' => 15,
				'cep' => '58238-000',
			),
			100 => 
			array (
				'id' => 4914,
				'nome' => 'Catingueira',
				'uf' => 'PB',
				'cep2' => '2504207',
				'estado_cod' => 15,
				'cep' => '58715-000',
			),
			101 => 
			array (
				'id' => 4916,
				'nome' => 'Catolé do Rocha',
				'uf' => 'PB',
				'cep2' => '2504306',
				'estado_cod' => 15,
				'cep' => '58884-000',
			),
			102 => 
			array (
				'id' => 4917,
				'nome' => 'Caturité',
				'uf' => 'PB',
				'cep2' => '2504355',
				'estado_cod' => 15,
				'cep' => '58455-000',
			),
			103 => 
			array (
				'id' => 4919,
				'nome' => 'Conceição',
				'uf' => 'PB',
				'cep2' => '2504405',
				'estado_cod' => 15,
				'cep' => '58970-000',
			),
			104 => 
			array (
				'id' => 4920,
				'nome' => 'Condado',
				'uf' => 'PB',
				'cep2' => '2504504',
				'estado_cod' => 15,
				'cep' => '58714-000',
			),
			105 => 
			array (
				'id' => 4921,
				'nome' => 'Conde',
				'uf' => 'PB',
				'cep2' => '2504603',
				'estado_cod' => 15,
				'cep' => '58322-000',
			),
			106 => 
			array (
				'id' => 4922,
				'nome' => 'Congo',
				'uf' => 'PB',
				'cep2' => '2504702',
				'estado_cod' => 15,
				'cep' => '58535-000',
			),
			107 => 
			array (
				'id' => 4923,
				'nome' => 'Coremas',
				'uf' => 'PB',
				'cep2' => '2504801',
				'estado_cod' => 15,
				'cep' => '58770-000',
			),
			108 => 
			array (
				'id' => 4925,
				'nome' => 'Coxixola',
				'uf' => 'PB',
				'cep2' => '2504850',
				'estado_cod' => 15,
				'cep' => '58588-000',
			),
			109 => 
			array (
				'id' => 4926,
				'nome' => 'Cruz do Espírito Santo',
				'uf' => 'PB',
				'cep2' => '2504900',
				'estado_cod' => 15,
				'cep' => '58337-000',
			),
			110 => 
			array (
				'id' => 4927,
				'nome' => 'Cubati',
				'uf' => 'PB',
				'cep2' => '2505006',
				'estado_cod' => 15,
				'cep' => '58167-000',
			),
			111 => 
			array (
				'id' => 4928,
				'nome' => 'Cuité',
				'uf' => 'PB',
				'cep2' => '2505105',
				'estado_cod' => 15,
				'cep' => '58175-000',
			),
			112 => 
			array (
				'id' => 4929,
				'nome' => 'Cuité de Mamanguape',
				'uf' => 'PB',
				'cep2' => '2505238',
				'estado_cod' => 15,
				'cep' => '58289-000',
			),
			113 => 
			array (
				'id' => 4930,
				'nome' => 'Cuitegi',
				'uf' => 'PB',
				'cep2' => '2505204',
				'estado_cod' => 15,
				'cep' => '58208-000',
			),
			114 => 
			array (
				'id' => 4932,
				'nome' => 'Curral de Cima',
				'uf' => 'PB',
				'cep2' => '2505279',
				'estado_cod' => 15,
				'cep' => '58291-000',
			),
			115 => 
			array (
				'id' => 4933,
				'nome' => 'Curral Velho',
				'uf' => 'PB',
				'cep2' => '2505303',
				'estado_cod' => 15,
				'cep' => '58990-000',
			),
			116 => 
			array (
				'id' => 4934,
				'nome' => 'Damião',
				'uf' => 'PB',
				'cep2' => '2505352',
				'estado_cod' => 15,
				'cep' => '58173-000',
			),
			117 => 
			array (
				'id' => 4935,
				'nome' => 'Desterro',
				'uf' => 'PB',
				'cep2' => '2505402',
				'estado_cod' => 15,
				'cep' => '58695-000',
			),
			118 => 
			array (
				'id' => 4936,
				'nome' => 'Diamante',
				'uf' => 'PB',
				'cep2' => '2505600',
				'estado_cod' => 15,
				'cep' => '58994-000',
			),
			119 => 
			array (
				'id' => 4937,
				'nome' => 'Dona Inês',
				'uf' => 'PB',
				'cep2' => '2505709',
				'estado_cod' => 15,
				'cep' => '58228-000',
			),
			120 => 
			array (
				'id' => 4938,
				'nome' => 'Duas Estradas',
				'uf' => 'PB',
				'cep2' => '2505808',
				'estado_cod' => 15,
				'cep' => '58265-000',
			),
			121 => 
			array (
				'id' => 4939,
				'nome' => 'Emas',
				'uf' => 'PB',
				'cep2' => '2505907',
				'estado_cod' => 15,
				'cep' => '58763-000',
			),
			122 => 
			array (
				'id' => 4941,
				'nome' => 'Esperança',
				'uf' => 'PB',
				'cep2' => '2506004',
				'estado_cod' => 15,
				'cep' => '58135-000',
			),
			123 => 
			array (
				'id' => 4942,
				'nome' => 'Fagundes',
				'uf' => 'PB',
				'cep2' => '2506103',
				'estado_cod' => 15,
				'cep' => '58487-000',
			),
			124 => 
			array (
				'id' => 4946,
				'nome' => 'Frei Martinho',
				'uf' => 'PB',
				'cep2' => '2506202',
				'estado_cod' => 15,
				'cep' => '58195-000',
			),
			125 => 
			array (
				'id' => 4947,
				'nome' => 'Gado Bravo',
				'uf' => 'PB',
				'cep2' => '2506251',
				'estado_cod' => 15,
				'cep' => '58492-000',
			),
			126 => 
			array (
				'id' => 4949,
				'nome' => 'Guarabira',
				'uf' => 'PB',
				'cep2' => '2506301',
				'estado_cod' => 15,
				'cep' => '58200-000',
			),
			127 => 
			array (
				'id' => 4951,
				'nome' => 'Gurinhém',
				'uf' => 'PB',
				'cep2' => '2506400',
				'estado_cod' => 15,
				'cep' => '58356-000',
			),
			128 => 
			array (
				'id' => 4952,
				'nome' => 'Gurjão',
				'uf' => 'PB',
				'cep2' => '2506509',
				'estado_cod' => 15,
				'cep' => '58670-000',
			),
			129 => 
			array (
				'id' => 4953,
				'nome' => 'Ibiara',
				'uf' => 'PB',
				'cep2' => '2506608',
				'estado_cod' => 15,
				'cep' => '58980-000',
			),
			130 => 
			array (
				'id' => 4954,
				'nome' => 'Igaracy',
				'uf' => 'PB',
				'cep2' => '2502607',
				'estado_cod' => 15,
				'cep' => '58775-000',
			),
			131 => 
			array (
				'id' => 4955,
				'nome' => 'Imaculada',
				'uf' => 'PB',
				'cep2' => '2506707',
				'estado_cod' => 15,
				'cep' => '58745-000',
			),
			132 => 
			array (
				'id' => 4956,
				'nome' => 'Ingá',
				'uf' => 'PB',
				'cep2' => '2506806',
				'estado_cod' => 15,
				'cep' => '58380-000',
			),
			133 => 
			array (
				'id' => 4957,
				'nome' => 'Itabaiana',
				'uf' => 'PB',
				'cep2' => '2506905',
				'estado_cod' => 15,
				'cep' => '58360-000',
			),
			134 => 
			array (
				'id' => 4959,
				'nome' => 'Itaporanga',
				'uf' => 'PB',
				'cep2' => '2507002',
				'estado_cod' => 15,
				'cep' => '58780-000',
			),
			135 => 
			array (
				'id' => 4960,
				'nome' => 'Itapororoca',
				'uf' => 'PB',
				'cep2' => '2507101',
				'estado_cod' => 15,
				'cep' => '58275-000',
			),
			136 => 
			array (
				'id' => 4961,
				'nome' => 'Itatuba',
				'uf' => 'PB',
				'cep2' => '2507200',
				'estado_cod' => 15,
				'cep' => '58378-000',
			),
			137 => 
			array (
				'id' => 4962,
				'nome' => 'Jacaraú',
				'uf' => 'PB',
				'cep2' => '2507309',
				'estado_cod' => 15,
				'cep' => '58278-000',
			),
			138 => 
			array (
				'id' => 4963,
				'nome' => 'Jericó',
				'uf' => 'PB',
				'cep2' => '2507408',
				'estado_cod' => 15,
				'cep' => '58830-000',
			),
			139 => 
			array (
				'id' => 4964,
				'nome' => 'João Pessoa',
				'uf' => 'PB',
				'cep2' => '2507507',
				'estado_cod' => 15,
				'cep' => 'LOC',
			),
			140 => 
			array (
				'id' => 4965,
				'nome' => 'Juarez Távora',
				'uf' => 'PB',
				'cep2' => '2507606',
				'estado_cod' => 15,
				'cep' => '58387-000',
			),
			141 => 
			array (
				'id' => 4966,
				'nome' => 'Juazeirinho',
				'uf' => 'PB',
				'cep2' => '2507705',
				'estado_cod' => 15,
				'cep' => '58660-000',
			),
			142 => 
			array (
				'id' => 4967,
				'nome' => 'Junco do Seridó',
				'uf' => 'PB',
				'cep2' => '2507804',
				'estado_cod' => 15,
				'cep' => '58640-000',
			),
			143 => 
			array (
				'id' => 4968,
				'nome' => 'Juripiranga',
				'uf' => 'PB',
				'cep2' => '2507903',
				'estado_cod' => 15,
				'cep' => '58330-000',
			),
			144 => 
			array (
				'id' => 4969,
				'nome' => 'Juru',
				'uf' => 'PB',
				'cep2' => '2508000',
				'estado_cod' => 15,
				'cep' => '58750-000',
			),
			145 => 
			array (
				'id' => 4970,
				'nome' => 'Lagoa',
				'uf' => 'PB',
				'cep2' => '2508109',
				'estado_cod' => 15,
				'cep' => '58835-000',
			),
			146 => 
			array (
				'id' => 4971,
				'nome' => 'Lagoa de Dentro',
				'uf' => 'PB',
				'cep2' => '2508208',
				'estado_cod' => 15,
				'cep' => '58250-000',
			),
			147 => 
			array (
				'id' => 4973,
				'nome' => 'Lagoa Seca',
				'uf' => 'PB',
				'cep2' => '2508307',
				'estado_cod' => 15,
				'cep' => '58117-000',
			),
			148 => 
			array (
				'id' => 4974,
				'nome' => 'Lastro',
				'uf' => 'PB',
				'cep2' => '2508406',
				'estado_cod' => 15,
				'cep' => '58820-000',
			),
			149 => 
			array (
				'id' => 4976,
				'nome' => 'Livramento',
				'uf' => 'PB',
				'cep2' => '2508505',
				'estado_cod' => 15,
				'cep' => '58690-000',
			),
			150 => 
			array (
				'id' => 4977,
				'nome' => 'Logradouro',
				'uf' => 'PB',
				'cep2' => '2508554',
				'estado_cod' => 15,
				'cep' => '58254-000',
			),
			151 => 
			array (
				'id' => 4978,
				'nome' => 'Lucena',
				'uf' => 'PB',
				'cep2' => '2508604',
				'estado_cod' => 15,
				'cep' => '58315-000',
			),
			152 => 
			array (
				'id' => 4979,
				'nome' => 'Mãe D\'Água',
				'uf' => 'PB',
				'cep2' => '2508703',
				'estado_cod' => 15,
				'cep' => '58740-000',
			),
			153 => 
			array (
				'id' => 4981,
				'nome' => 'Malta',
				'uf' => 'PB',
				'cep2' => '2508802',
				'estado_cod' => 15,
				'cep' => '58713-000',
			),
			154 => 
			array (
				'id' => 4982,
				'nome' => 'Mamanguape',
				'uf' => 'PB',
				'cep2' => '2508901',
				'estado_cod' => 15,
				'cep' => '58280-000',
			),
			155 => 
			array (
				'id' => 4983,
				'nome' => 'Manaíra',
				'uf' => 'PB',
				'cep2' => '2509008',
				'estado_cod' => 15,
				'cep' => '58995-000',
			),
			156 => 
			array (
				'id' => 4984,
				'nome' => 'Marcação',
				'uf' => 'PB',
				'cep2' => '2509057',
				'estado_cod' => 15,
				'cep' => '58294-000',
			),
			157 => 
			array (
				'id' => 4985,
				'nome' => 'Mari',
				'uf' => 'PB',
				'cep2' => '2509107',
				'estado_cod' => 15,
				'cep' => '58345-000',
			),
			158 => 
			array (
				'id' => 4986,
				'nome' => 'Marizópolis',
				'uf' => 'PB',
				'cep2' => '2509156',
				'estado_cod' => 15,
				'cep' => '58819-000',
			),
			159 => 
			array (
				'id' => 4987,
				'nome' => 'Massaranduba',
				'uf' => 'PB',
				'cep2' => '2509206',
				'estado_cod' => 15,
				'cep' => '58120-000',
			),
			160 => 
			array (
				'id' => 4990,
				'nome' => 'Mataraca',
				'uf' => 'PB',
				'cep2' => '2509305',
				'estado_cod' => 15,
				'cep' => '58292-000',
			),
			161 => 
			array (
				'id' => 4991,
				'nome' => 'Matinhas',
				'uf' => 'PB',
				'cep2' => '2509339',
				'estado_cod' => 15,
				'cep' => '58128-000',
			),
			162 => 
			array (
				'id' => 4992,
				'nome' => 'Mato Grosso',
				'uf' => 'PB',
				'cep2' => '2509370',
				'estado_cod' => 15,
				'cep' => '58832-000',
			),
			163 => 
			array (
				'id' => 4993,
				'nome' => 'Maturéia',
				'uf' => 'PB',
				'cep2' => '2509396',
				'estado_cod' => 15,
				'cep' => '58737-000',
			),
			164 => 
			array (
				'id' => 4995,
				'nome' => 'Mogeiro',
				'uf' => 'PB',
				'cep2' => '2509404',
				'estado_cod' => 15,
				'cep' => '58375-000',
			),
			165 => 
			array (
				'id' => 4996,
				'nome' => 'Montadas',
				'uf' => 'PB',
				'cep2' => '2509503',
				'estado_cod' => 15,
				'cep' => '58145-000',
			),
			166 => 
			array (
				'id' => 4997,
				'nome' => 'Monte Horebe',
				'uf' => 'PB',
				'cep2' => '2509602',
				'estado_cod' => 15,
				'cep' => '58950-000',
			),
			167 => 
			array (
				'id' => 4998,
				'nome' => 'Monteiro',
				'uf' => 'PB',
				'cep2' => '2509701',
				'estado_cod' => 15,
				'cep' => '58500-000',
			),
			168 => 
			array (
				'id' => 5000,
				'nome' => 'Mulungu',
				'uf' => 'PB',
				'cep2' => '2509800',
				'estado_cod' => 15,
				'cep' => '58354-000',
			),
			169 => 
			array (
				'id' => 5002,
				'nome' => 'Natuba',
				'uf' => 'PB',
				'cep2' => '2509909',
				'estado_cod' => 15,
				'cep' => '58494-000',
			),
			170 => 
			array (
				'id' => 5004,
				'nome' => 'Nazarezinho',
				'uf' => 'PB',
				'cep2' => '2510006',
				'estado_cod' => 15,
				'cep' => '58817-000',
			),
			171 => 
			array (
				'id' => 5006,
				'nome' => 'Nova Floresta',
				'uf' => 'PB',
				'cep2' => '2510105',
				'estado_cod' => 15,
				'cep' => '58178-000',
			),
			172 => 
			array (
				'id' => 5007,
				'nome' => 'Nova Olinda',
				'uf' => 'PB',
				'cep2' => '2510204',
				'estado_cod' => 15,
				'cep' => '58798-000',
			),
			173 => 
			array (
				'id' => 5008,
				'nome' => 'Nova Palmeira',
				'uf' => 'PB',
				'cep2' => '2510303',
				'estado_cod' => 15,
				'cep' => '58184-000',
			),
			174 => 
			array (
				'id' => 5012,
				'nome' => 'Olho D\'Água',
				'uf' => 'PB',
				'cep2' => '2510402',
				'estado_cod' => 15,
				'cep' => '58760-000',
			),
			175 => 
			array (
				'id' => 5013,
				'nome' => 'Olivedos',
				'uf' => 'PB',
				'cep2' => '2510501',
				'estado_cod' => 15,
				'cep' => '58160-000',
			),
			176 => 
			array (
				'id' => 5014,
				'nome' => 'Ouro Velho',
				'uf' => 'PB',
				'cep2' => '2510600',
				'estado_cod' => 15,
				'cep' => '58560-000',
			),
			177 => 
			array (
				'id' => 5015,
				'nome' => 'Parari',
				'uf' => 'PB',
				'cep2' => '2510659',
				'estado_cod' => 15,
				'cep' => '58575-000',
			),
			178 => 
			array (
				'id' => 5016,
				'nome' => 'Passagem',
				'uf' => 'PB',
				'cep2' => '2510709',
				'estado_cod' => 15,
				'cep' => '58734-000',
			),
			179 => 
			array (
				'id' => 5017,
				'nome' => 'Patos',
				'uf' => 'PB',
				'cep2' => '2510808',
				'estado_cod' => 15,
				'cep' => 'LOC',
			),
			180 => 
			array (
				'id' => 5018,
				'nome' => 'Paulista',
				'uf' => 'PB',
				'cep2' => '2510907',
				'estado_cod' => 15,
				'cep' => '58860-000',
			),
			181 => 
			array (
				'id' => 5019,
				'nome' => 'Pedra Branca',
				'uf' => 'PB',
				'cep2' => '2511004',
				'estado_cod' => 15,
				'cep' => '58790-000',
			),
			182 => 
			array (
				'id' => 5020,
				'nome' => 'Pedra Lavrada',
				'uf' => 'PB',
				'cep2' => '2511103',
				'estado_cod' => 15,
				'cep' => '58180-000',
			),
			183 => 
			array (
				'id' => 5021,
				'nome' => 'Pedras de Fogo',
				'uf' => 'PB',
				'cep2' => '2511202',
				'estado_cod' => 15,
				'cep' => '58328-000',
			),
			184 => 
			array (
				'id' => 5022,
				'nome' => 'Pedro Régis',
				'uf' => 'PB',
				'cep2' => '2512721',
				'estado_cod' => 15,
				'cep' => '58273-000',
			),
			185 => 
			array (
				'id' => 5025,
				'nome' => 'Piancó',
				'uf' => 'PB',
				'cep2' => '2511301',
				'estado_cod' => 15,
				'cep' => '58765-000',
			),
			186 => 
			array (
				'id' => 5026,
				'nome' => 'Picuí',
				'uf' => 'PB',
				'cep2' => '2511400',
				'estado_cod' => 15,
				'cep' => '58187-000',
			),
			187 => 
			array (
				'id' => 5027,
				'nome' => 'Pilar',
				'uf' => 'PB',
				'cep2' => '2511509',
				'estado_cod' => 15,
				'cep' => '58338-000',
			),
			188 => 
			array (
				'id' => 5028,
				'nome' => 'Pilões',
				'uf' => 'PB',
				'cep2' => '2511608',
				'estado_cod' => 15,
				'cep' => '58393-000',
			),
			189 => 
			array (
				'id' => 5029,
				'nome' => 'Pilõezinhos',
				'uf' => 'PB',
				'cep2' => '2511707',
				'estado_cod' => 15,
				'cep' => '58210-000',
			),
			190 => 
			array (
				'id' => 5033,
				'nome' => 'Pirpirituba',
				'uf' => 'PB',
				'cep2' => '2511806',
				'estado_cod' => 15,
				'cep' => '58213-000',
			),
			191 => 
			array (
				'id' => 5035,
				'nome' => 'Pitimbu',
				'uf' => 'PB',
				'cep2' => '2511905',
				'estado_cod' => 15,
				'cep' => '58324-000',
			),
			192 => 
			array (
				'id' => 5036,
				'nome' => 'Pocinhos',
				'uf' => 'PB',
				'cep2' => '2512002',
				'estado_cod' => 15,
				'cep' => '58150-000',
			),
			193 => 
			array (
				'id' => 5037,
				'nome' => 'Poço Dantas',
				'uf' => 'PB',
				'cep2' => '2512036',
				'estado_cod' => 15,
				'cep' => '58933-000',
			),
			194 => 
			array (
				'id' => 5038,
				'nome' => 'Poço de José de Moura',
				'uf' => 'PB',
				'cep2' => '2512077',
				'estado_cod' => 15,
				'cep' => '58908-000',
			),
			195 => 
			array (
				'id' => 5039,
				'nome' => 'Pombal',
				'uf' => 'PB',
				'cep2' => '2512101',
				'estado_cod' => 15,
				'cep' => '58840-000',
			),
			196 => 
			array (
				'id' => 5041,
				'nome' => 'Prata',
				'uf' => 'PB',
				'cep2' => '2512200',
				'estado_cod' => 15,
				'cep' => '58550-000',
			),
			197 => 
			array (
				'id' => 5042,
				'nome' => 'Princesa Isabel',
				'uf' => 'PB',
				'cep2' => '2512309',
				'estado_cod' => 15,
				'cep' => '58755-000',
			),
			198 => 
			array (
				'id' => 5043,
				'nome' => 'Puxinanã',
				'uf' => 'PB',
				'cep2' => '2512408',
				'estado_cod' => 15,
				'cep' => '58115-000',
			),
			199 => 
			array (
				'id' => 5044,
				'nome' => 'Queimadas',
				'uf' => 'PB',
				'cep2' => '2512507',
				'estado_cod' => 15,
				'cep' => '58475-000',
			),
			200 => 
			array (
				'id' => 5045,
				'nome' => 'Quixabá',
				'uf' => 'PB',
				'cep2' => '2512606',
				'estado_cod' => 15,
				'cep' => '58733-000',
			),
			201 => 
			array (
				'id' => 5047,
				'nome' => 'Remígio',
				'uf' => 'PB',
				'cep2' => '2512705',
				'estado_cod' => 15,
				'cep' => '58398-000',
			),
			202 => 
			array (
				'id' => 5048,
				'nome' => 'Riachão',
				'uf' => 'PB',
				'cep2' => '2512747',
				'estado_cod' => 15,
				'cep' => '58235-000',
			),
			203 => 
			array (
				'id' => 5049,
				'nome' => 'Riachão do Poço',
				'uf' => 'PB',
				'cep2' => '2512762',
				'estado_cod' => 15,
				'cep' => '58348-000',
			),
			204 => 
			array (
				'id' => 5050,
				'nome' => 'Riacho de Santo Antônio',
				'uf' => 'PB',
				'cep2' => '2512788',
				'estado_cod' => 15,
				'cep' => '58465-000',
			),
			205 => 
			array (
				'id' => 5051,
				'nome' => 'Riacho dos Cavalos',
				'uf' => 'PB',
				'cep2' => '2512804',
				'estado_cod' => 15,
				'cep' => '58870-000',
			),
			206 => 
			array (
				'id' => 5053,
				'nome' => 'Rio Tinto',
				'uf' => 'PB',
				'cep2' => '2512903',
				'estado_cod' => 15,
				'cep' => '58297-000',
			),
			207 => 
			array (
				'id' => 5056,
				'nome' => 'Salgadinho',
				'uf' => 'PB',
				'cep2' => '2513000',
				'estado_cod' => 15,
				'cep' => '58650-000',
			),
			208 => 
			array (
				'id' => 5057,
				'nome' => 'Salgado de São Félix',
				'uf' => 'PB',
				'cep2' => '2513109',
				'estado_cod' => 15,
				'cep' => '58370-000',
			),
			209 => 
			array (
				'id' => 5058,
				'nome' => 'Santa Cecília',
				'uf' => 'PB',
				'cep2' => '2513158',
				'estado_cod' => 15,
				'cep' => '58463-000',
			),
			210 => 
			array (
				'id' => 5059,
				'nome' => 'Santa Cruz',
				'uf' => 'PB',
				'cep2' => '2513208',
				'estado_cod' => 15,
				'cep' => '58824-000',
			),
			211 => 
			array (
				'id' => 5061,
				'nome' => 'Santa Helena',
				'uf' => 'PB',
				'cep2' => '2513307',
				'estado_cod' => 15,
				'cep' => '58925-000',
			),
			212 => 
			array (
				'id' => 5062,
				'nome' => 'Santa Inês',
				'uf' => 'PB',
				'cep2' => '2513356',
				'estado_cod' => 15,
				'cep' => '58978-000',
			),
			213 => 
			array (
				'id' => 5063,
				'nome' => 'Santa Luzia',
				'uf' => 'PB',
				'cep2' => '2513406',
				'estado_cod' => 15,
				'cep' => '58600-000',
			),
			214 => 
			array (
				'id' => 5066,
				'nome' => 'Santa Rita',
				'uf' => 'PB',
				'cep2' => '2513703',
				'estado_cod' => 15,
				'cep' => 'LOC',
			),
			215 => 
			array (
				'id' => 5068,
				'nome' => 'Santa Teresinha',
				'uf' => 'PB',
				'cep2' => '2513802',
				'estado_cod' => 15,
				'cep' => '58720-000',
			),
			216 => 
			array (
				'id' => 5070,
				'nome' => 'Santana de Mangueira',
				'uf' => 'PB',
				'cep2' => '2513505',
				'estado_cod' => 15,
				'cep' => '58985-000',
			),
			217 => 
			array (
				'id' => 5071,
				'nome' => 'Santana dos Garrotes',
				'uf' => 'PB',
				'cep2' => '2513604',
				'estado_cod' => 15,
				'cep' => '58795-000',
			),
			218 => 
			array (
				'id' => 5072,
				'nome' => 'Joca Claudino',
				'uf' => 'PB',
				'cep2' => '',
				'estado_cod' => 15,
				'cep' => '58928-000',
			),
			219 => 
			array (
				'id' => 5073,
				'nome' => 'Santo André',
				'uf' => 'PB',
				'cep2' => '2513851',
				'estado_cod' => 15,
				'cep' => '58675-000',
			),
			220 => 
			array (
				'id' => 5074,
				'nome' => 'São Bento',
				'uf' => 'PB',
				'cep2' => '2513901',
				'estado_cod' => 15,
				'cep' => '58865-000',
			),
			221 => 
			array (
				'id' => 5075,
				'nome' => 'São Bentinho',
				'uf' => 'PB',
				'cep2' => '2513927',
				'estado_cod' => 15,
				'cep' => '58857-000',
			),
			222 => 
			array (
				'id' => 5076,
				'nome' => 'São Domingos de Pombal',
				'uf' => 'PB',
				'cep2' => '2513968',
				'estado_cod' => 15,
				'cep' => '58853-000',
			),
			223 => 
			array (
				'id' => 5077,
				'nome' => 'São Domingos do Cariri',
				'uf' => 'PB',
				'cep2' => '2513943',
				'estado_cod' => 15,
				'cep' => '58485-000',
			),
			224 => 
			array (
				'id' => 5078,
				'nome' => 'São Francisco',
				'uf' => 'PB',
				'cep2' => '2513984',
				'estado_cod' => 15,
				'cep' => '58818-000',
			),
			225 => 
			array (
				'id' => 5081,
				'nome' => 'São João do Cariri',
				'uf' => 'PB',
				'cep2' => '2514008',
				'estado_cod' => 15,
				'cep' => '58590-000',
			),
			226 => 
			array (
				'id' => 5082,
				'nome' => 'São João do Rio do Peixe',
				'uf' => 'PB',
				'cep2' => '2500700',
				'estado_cod' => 15,
				'cep' => '58910-000',
			),
			227 => 
			array (
				'id' => 5083,
				'nome' => 'São João do Tigre',
				'uf' => 'PB',
				'cep2' => '2514107',
				'estado_cod' => 15,
				'cep' => '58520-000',
			),
			228 => 
			array (
				'id' => 5084,
				'nome' => 'São José da Lagoa Tapada',
				'uf' => 'PB',
				'cep2' => '2514206',
				'estado_cod' => 15,
				'cep' => '58815-000',
			),
			229 => 
			array (
				'id' => 5086,
				'nome' => 'São José de Caiana',
				'uf' => 'PB',
				'cep2' => '2514305',
				'estado_cod' => 15,
				'cep' => '58784-000',
			),
			230 => 
			array (
				'id' => 5087,
				'nome' => 'São José de Espinharas',
				'uf' => 'PB',
				'cep2' => '2514404',
				'estado_cod' => 15,
				'cep' => '58723-000',
			),
			231 => 
			array (
				'id' => 5089,
				'nome' => 'São José de Piranhas',
				'uf' => 'PB',
				'cep2' => '2514503',
				'estado_cod' => 15,
				'cep' => '58940-000',
			),
			232 => 
			array (
				'id' => 5090,
				'nome' => 'São José de Princesa',
				'uf' => 'PB',
				'cep2' => '2514552',
				'estado_cod' => 15,
				'cep' => '58758-000',
			),
			233 => 
			array (
				'id' => 5091,
				'nome' => 'São José do Bonfim',
				'uf' => 'PB',
				'cep2' => '2514602',
				'estado_cod' => 15,
				'cep' => '58725-000',
			),
			234 => 
			array (
				'id' => 5092,
				'nome' => 'São José do Brejo do Cruz',
				'uf' => 'PB',
				'cep2' => '2514651',
				'estado_cod' => 15,
				'cep' => '58893-000',
			),
			235 => 
			array (
				'id' => 5093,
				'nome' => 'São José do Sabugi',
				'uf' => 'PB',
				'cep2' => '2514701',
				'estado_cod' => 15,
				'cep' => '58610-000',
			),
			236 => 
			array (
				'id' => 5094,
				'nome' => 'São José dos Cordeiros',
				'uf' => 'PB',
				'cep2' => '2514800',
				'estado_cod' => 15,
				'cep' => '58570-000',
			),
			237 => 
			array (
				'id' => 5095,
				'nome' => 'São José dos Ramos',
				'uf' => 'PB',
				'cep2' => '2514453',
				'estado_cod' => 15,
				'cep' => '58339-000',
			),
			238 => 
			array (
				'id' => 5096,
				'nome' => 'São Mamede',
				'uf' => 'PB',
				'cep2' => '2514909',
				'estado_cod' => 15,
				'cep' => '58625-000',
			),
			239 => 
			array (
				'id' => 5097,
				'nome' => 'São Miguel de Taipu',
				'uf' => 'PB',
				'cep2' => '2515005',
				'estado_cod' => 15,
				'cep' => '58334-000',
			),
			240 => 
			array (
				'id' => 5099,
				'nome' => 'São Sebastião de Lagoa de Roça',
				'uf' => 'PB',
				'cep2' => '2515104',
				'estado_cod' => 15,
				'cep' => '58119-000',
			),
			241 => 
			array (
				'id' => 5100,
				'nome' => 'São Sebastião do Umbuzeiro',
				'uf' => 'PB',
				'cep2' => '2515203',
				'estado_cod' => 15,
				'cep' => '58510-000',
			),
			242 => 
			array (
				'id' => 5101,
				'nome' => 'Sapé',
				'uf' => 'PB',
				'cep2' => '2515302',
				'estado_cod' => 15,
				'cep' => '58340-000',
			),
			243 => 
			array (
				'id' => 5103,
				'nome' => 'São Vicente do Seridó',
				'uf' => 'PB',
				'cep2' => '',
				'estado_cod' => 15,
				'cep' => '58158-000',
			),
			244 => 
			array (
				'id' => 5104,
				'nome' => 'Serra Branca',
				'uf' => 'PB',
				'cep2' => '2515500',
				'estado_cod' => 15,
				'cep' => '58580-000',
			),
			245 => 
			array (
				'id' => 5105,
				'nome' => 'Serra da Raiz',
				'uf' => 'PB',
				'cep2' => '2515609',
				'estado_cod' => 15,
				'cep' => '58260-000',
			),
			246 => 
			array (
				'id' => 5106,
				'nome' => 'Serra Grande',
				'uf' => 'PB',
				'cep2' => '2515708',
				'estado_cod' => 15,
				'cep' => '58955-000',
			),
			247 => 
			array (
				'id' => 5107,
				'nome' => 'Serra Redonda',
				'uf' => 'PB',
				'cep2' => '2515807',
				'estado_cod' => 15,
				'cep' => '58385-000',
			),
			248 => 
			array (
				'id' => 5108,
				'nome' => 'Serraria',
				'uf' => 'PB',
				'cep2' => '2515906',
				'estado_cod' => 15,
				'cep' => '58395-000',
			),
			249 => 
			array (
				'id' => 5109,
				'nome' => 'Sertãozinho',
				'uf' => 'PB',
				'cep2' => '2515930',
				'estado_cod' => 15,
				'cep' => '58268-000',
			),
			250 => 
			array (
				'id' => 5110,
				'nome' => 'Sobrado',
				'uf' => 'PB',
				'cep2' => '2515971',
				'estado_cod' => 15,
				'cep' => '58342-000',
			),
			251 => 
			array (
				'id' => 5111,
				'nome' => 'Solânea',
				'uf' => 'PB',
				'cep2' => '2516003',
				'estado_cod' => 15,
				'cep' => '58225-000',
			),
			252 => 
			array (
				'id' => 5112,
				'nome' => 'Soledade',
				'uf' => 'PB',
				'cep2' => '2516102',
				'estado_cod' => 15,
				'cep' => '58155-000',
			),
			253 => 
			array (
				'id' => 5113,
				'nome' => 'Sossego',
				'uf' => 'PB',
				'cep2' => '2516151',
				'estado_cod' => 15,
				'cep' => '58177-000',
			),
			254 => 
			array (
				'id' => 5114,
				'nome' => 'Sousa',
				'uf' => 'PB',
				'cep2' => '2516201',
				'estado_cod' => 15,
				'cep' => 'LOC',
			),
			255 => 
			array (
				'id' => 5116,
				'nome' => 'Sumé',
				'uf' => 'PB',
				'cep2' => '2516300',
				'estado_cod' => 15,
				'cep' => '58540-000',
			),
			256 => 
			array (
				'id' => 5117,
				'nome' => 'Tacima',
				'uf' => 'PB',
				'cep2' => '',
				'estado_cod' => 15,
				'cep' => '58240-000',
			),
			257 => 
			array (
				'id' => 5120,
				'nome' => 'Taperoá',
				'uf' => 'PB',
				'cep2' => '2516508',
				'estado_cod' => 15,
				'cep' => '58680-000',
			),
			258 => 
			array (
				'id' => 5121,
				'nome' => 'Tavares',
				'uf' => 'PB',
				'cep2' => '2516607',
				'estado_cod' => 15,
				'cep' => '58753-000',
			),
			259 => 
			array (
				'id' => 5122,
				'nome' => 'Teixeira',
				'uf' => 'PB',
				'cep2' => '2516706',
				'estado_cod' => 15,
				'cep' => '58735-000',
			),
			260 => 
			array (
				'id' => 5123,
				'nome' => 'Tenório',
				'uf' => 'PB',
				'cep2' => '2516755',
				'estado_cod' => 15,
				'cep' => '58665-000',
			),
			261 => 
			array (
				'id' => 5124,
				'nome' => 'Triunfo',
				'uf' => 'PB',
				'cep2' => '2516805',
				'estado_cod' => 15,
				'cep' => '58920-000',
			),
			262 => 
			array (
				'id' => 5125,
				'nome' => 'Uiraúna',
				'uf' => 'PB',
				'cep2' => '2516904',
				'estado_cod' => 15,
				'cep' => '58915-000',
			),
			263 => 
			array (
				'id' => 5127,
				'nome' => 'Umbuzeiro',
				'uf' => 'PB',
				'cep2' => '2517001',
				'estado_cod' => 15,
				'cep' => '58497-000',
			),
			264 => 
			array (
				'id' => 5128,
				'nome' => 'Várzea',
				'uf' => 'PB',
				'cep2' => '2517100',
				'estado_cod' => 15,
				'cep' => '58620-000',
			),
			265 => 
			array (
				'id' => 5132,
				'nome' => 'Vieirópolis',
				'uf' => 'PB',
				'cep2' => '2517209',
				'estado_cod' => 15,
				'cep' => '58822-000',
			),
			266 => 
			array (
				'id' => 5133,
				'nome' => 'Vista Serrana',
				'uf' => 'PB',
				'cep2' => '2505501',
				'estado_cod' => 15,
				'cep' => '58710-000',
			),
			267 => 
			array (
				'id' => 5134,
				'nome' => 'Zabelê',
				'uf' => 'PB',
				'cep2' => '2517407',
				'estado_cod' => 15,
				'cep' => '58515-000',
			),
			268 => 
			array (
				'id' => 5135,
				'nome' => 'Abreu e Lima',
				'uf' => 'PE',
				'cep2' => '2600054',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			269 => 
			array (
				'id' => 5136,
				'nome' => 'Afogados da Ingazeira',
				'uf' => 'PE',
				'cep2' => '2600104',
				'estado_cod' => 16,
				'cep' => '56800-000',
			),
			270 => 
			array (
				'id' => 5137,
				'nome' => 'Afrânio',
				'uf' => 'PE',
				'cep2' => '2600203',
				'estado_cod' => 16,
				'cep' => '56360-000',
			),
			271 => 
			array (
				'id' => 5138,
				'nome' => 'Agrestina',
				'uf' => 'PE',
				'cep2' => '2600302',
				'estado_cod' => 16,
				'cep' => '55495-000',
			),
			272 => 
			array (
				'id' => 5140,
				'nome' => 'Água Preta',
				'uf' => 'PE',
				'cep2' => '2600401',
				'estado_cod' => 16,
				'cep' => '55550-000',
			),
			273 => 
			array (
				'id' => 5141,
				'nome' => 'Águas Belas',
				'uf' => 'PE',
				'cep2' => '2600500',
				'estado_cod' => 16,
				'cep' => '55340-000',
			),
			274 => 
			array (
				'id' => 5143,
				'nome' => 'Alagoinha',
				'uf' => 'PE',
				'cep2' => '2600609',
				'estado_cod' => 16,
				'cep' => '55260-000',
			),
			275 => 
			array (
				'id' => 5146,
				'nome' => 'Aliança',
				'uf' => 'PE',
				'cep2' => '2600708',
				'estado_cod' => 16,
				'cep' => '55890-000',
			),
			276 => 
			array (
				'id' => 5147,
				'nome' => 'Altinho',
				'uf' => 'PE',
				'cep2' => '2600807',
				'estado_cod' => 16,
				'cep' => '55490-000',
			),
			277 => 
			array (
				'id' => 5148,
				'nome' => 'Amaraji',
				'uf' => 'PE',
				'cep2' => '2600906',
				'estado_cod' => 16,
				'cep' => '55515-000',
			),
			278 => 
			array (
				'id' => 5150,
				'nome' => 'Angelim',
				'uf' => 'PE',
				'cep2' => '2601003',
				'estado_cod' => 16,
				'cep' => '55430-000',
			),
			279 => 
			array (
				'id' => 5152,
				'nome' => 'Araçoiaba',
				'uf' => 'PE',
				'cep2' => '2601052',
				'estado_cod' => 16,
				'cep' => '53690-000',
			),
			280 => 
			array (
				'id' => 5153,
				'nome' => 'Araripina',
				'uf' => 'PE',
				'cep2' => '2601102',
				'estado_cod' => 16,
				'cep' => '56280-000',
			),
			281 => 
			array (
				'id' => 5154,
				'nome' => 'Arcoverde',
				'uf' => 'PE',
				'cep2' => '2601201',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			282 => 
			array (
				'id' => 5158,
				'nome' => 'Barra de Guabiraba',
				'uf' => 'PE',
				'cep2' => '2601300',
				'estado_cod' => 16,
				'cep' => '55690-000',
			),
			283 => 
			array (
				'id' => 5165,
				'nome' => 'Barreiros',
				'uf' => 'PE',
				'cep2' => '2601409',
				'estado_cod' => 16,
				'cep' => '55560-000',
			),
			284 => 
			array (
				'id' => 5167,
				'nome' => 'Belém de Maria',
				'uf' => 'PE',
				'cep2' => '2601508',
				'estado_cod' => 16,
				'cep' => '55440-000',
			),
			285 => 
			array (
				'id' => 5168,
				'nome' => 'Belém do São Francisco',
				'uf' => 'PE',
				'cep2' => '',
				'estado_cod' => 16,
				'cep' => '56440-000',
			),
			286 => 
			array (
				'id' => 5169,
				'nome' => 'Belo Jardim',
				'uf' => 'PE',
				'cep2' => '2601706',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			287 => 
			array (
				'id' => 5173,
				'nome' => 'Betânia',
				'uf' => 'PE',
				'cep2' => '2601805',
				'estado_cod' => 16,
				'cep' => '56670-000',
			),
			288 => 
			array (
				'id' => 5174,
				'nome' => 'Bezerros',
				'uf' => 'PE',
				'cep2' => '2601904',
				'estado_cod' => 16,
				'cep' => '55660-000',
			),
			289 => 
			array (
				'id' => 5177,
				'nome' => 'Bodocó',
				'uf' => 'PE',
				'cep2' => '2602001',
				'estado_cod' => 16,
				'cep' => '56220-000',
			),
			290 => 
			array (
				'id' => 5178,
				'nome' => 'Bom Conselho',
				'uf' => 'PE',
				'cep2' => '2602100',
				'estado_cod' => 16,
				'cep' => '55330-000',
			),
			291 => 
			array (
				'id' => 5179,
				'nome' => 'Bom Jardim',
				'uf' => 'PE',
				'cep2' => '2602209',
				'estado_cod' => 16,
				'cep' => '55730-000',
			),
			292 => 
			array (
				'id' => 5182,
				'nome' => 'Bonito',
				'uf' => 'PE',
				'cep2' => '2602308',
				'estado_cod' => 16,
				'cep' => '55680-000',
			),
			293 => 
			array (
				'id' => 5183,
				'nome' => 'Brejão',
				'uf' => 'PE',
				'cep2' => '2602407',
				'estado_cod' => 16,
				'cep' => '55325-000',
			),
			294 => 
			array (
				'id' => 5184,
				'nome' => 'Brejinho',
				'uf' => 'PE',
				'cep2' => '2602506',
				'estado_cod' => 16,
				'cep' => '56740-000',
			),
			295 => 
			array (
				'id' => 5185,
				'nome' => 'Brejo da Madre de Deus',
				'uf' => 'PE',
				'cep2' => '2602605',
				'estado_cod' => 16,
				'cep' => '55170-000',
			),
			296 => 
			array (
				'id' => 5186,
				'nome' => 'Buenos Aires',
				'uf' => 'PE',
				'cep2' => '2602704',
				'estado_cod' => 16,
				'cep' => '55845-000',
			),
			297 => 
			array (
				'id' => 5187,
				'nome' => 'Buíque',
				'uf' => 'PE',
				'cep2' => '2602803',
				'estado_cod' => 16,
				'cep' => '56520-000',
			),
			298 => 
			array (
				'id' => 5189,
				'nome' => 'Cabo de Santo Agostinho',
				'uf' => 'PE',
				'cep2' => '2602902',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			299 => 
			array (
				'id' => 5190,
				'nome' => 'Cabrobó',
				'uf' => 'PE',
				'cep2' => '2603009',
				'estado_cod' => 16,
				'cep' => '56180-000',
			),
			300 => 
			array (
				'id' => 5192,
				'nome' => 'Cachoeirinha',
				'uf' => 'PE',
				'cep2' => '2603108',
				'estado_cod' => 16,
				'cep' => '55380-000',
			),
			301 => 
			array (
				'id' => 5193,
				'nome' => 'Caetés',
				'uf' => 'PE',
				'cep2' => '2603207',
				'estado_cod' => 16,
				'cep' => '55360-000',
			),
			302 => 
			array (
				'id' => 5195,
				'nome' => 'Calçado',
				'uf' => 'PE',
				'cep2' => '2603306',
				'estado_cod' => 16,
				'cep' => '55375-000',
			),
			303 => 
			array (
				'id' => 5197,
				'nome' => 'Calumbi',
				'uf' => 'PE',
				'cep2' => '2603405',
				'estado_cod' => 16,
				'cep' => '56930-000',
			),
			304 => 
			array (
				'id' => 5198,
				'nome' => 'Camaragibe',
				'uf' => 'PE',
				'cep2' => '2603454',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			305 => 
			array (
				'id' => 5200,
				'nome' => 'Camocim de São Félix',
				'uf' => 'PE',
				'cep2' => '2603504',
				'estado_cod' => 16,
				'cep' => '55665-000',
			),
			306 => 
			array (
				'id' => 5201,
				'nome' => 'Camutanga',
				'uf' => 'PE',
				'cep2' => '2603603',
				'estado_cod' => 16,
				'cep' => '55930-000',
			),
			307 => 
			array (
				'id' => 5203,
				'nome' => 'Canhotinho',
				'uf' => 'PE',
				'cep2' => '2603702',
				'estado_cod' => 16,
				'cep' => '55420-000',
			),
			308 => 
			array (
				'id' => 5204,
				'nome' => 'Capoeiras',
				'uf' => 'PE',
				'cep2' => '2603801',
				'estado_cod' => 16,
				'cep' => '55365-000',
			),
			309 => 
			array (
				'id' => 5211,
				'nome' => 'Carnaíba',
				'uf' => 'PE',
				'cep2' => '2603900',
				'estado_cod' => 16,
				'cep' => '56820-000',
			),
			310 => 
			array (
				'id' => 5212,
				'nome' => 'Carnaubeira da Penha',
				'uf' => 'PE',
				'cep2' => '2603926',
				'estado_cod' => 16,
				'cep' => '56420-000',
			),
			311 => 
			array (
				'id' => 5214,
				'nome' => 'Carpina',
				'uf' => 'PE',
				'cep2' => '2604007',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			312 => 
			array (
				'id' => 5216,
				'nome' => 'Caruaru',
				'uf' => 'PE',
				'cep2' => '2604106',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			313 => 
			array (
				'id' => 5217,
				'nome' => 'Casinhas',
				'uf' => 'PE',
				'cep2' => '2604155',
				'estado_cod' => 16,
				'cep' => '55755-000',
			),
			314 => 
			array (
				'id' => 5218,
				'nome' => 'Catende',
				'uf' => 'PE',
				'cep2' => '2604205',
				'estado_cod' => 16,
				'cep' => '55400-000',
			),
			315 => 
			array (
				'id' => 5222,
				'nome' => 'Cedro',
				'uf' => 'PE',
				'cep2' => '2604304',
				'estado_cod' => 16,
				'cep' => '56130-000',
			),
			316 => 
			array (
				'id' => 5223,
				'nome' => 'Chã de Alegria',
				'uf' => 'PE',
				'cep2' => '2604403',
				'estado_cod' => 16,
				'cep' => '55835-000',
			),
			317 => 
			array (
				'id' => 5225,
				'nome' => 'Chã Grande',
				'uf' => 'PE',
				'cep2' => '2604502',
				'estado_cod' => 16,
				'cep' => '55636-000',
			),
			318 => 
			array (
				'id' => 5231,
				'nome' => 'Condado',
				'uf' => 'PE',
				'cep2' => '2604601',
				'estado_cod' => 16,
				'cep' => '55940-000',
			),
			319 => 
			array (
				'id' => 5232,
				'nome' => 'Correntes',
				'uf' => 'PE',
				'cep2' => '2604700',
				'estado_cod' => 16,
				'cep' => '55315-000',
			),
			320 => 
			array (
				'id' => 5233,
				'nome' => 'Cortês',
				'uf' => 'PE',
				'cep2' => '2604809',
				'estado_cod' => 16,
				'cep' => '55525-000',
			),
			321 => 
			array (
				'id' => 5239,
				'nome' => 'Cumaru',
				'uf' => 'PE',
				'cep2' => '2604908',
				'estado_cod' => 16,
				'cep' => '55655-000',
			),
			322 => 
			array (
				'id' => 5240,
				'nome' => 'Cupira',
				'uf' => 'PE',
				'cep2' => '2605004',
				'estado_cod' => 16,
				'cep' => '55460-000',
			),
			323 => 
			array (
				'id' => 5242,
				'nome' => 'Custódia',
				'uf' => 'PE',
				'cep2' => '2605103',
				'estado_cod' => 16,
				'cep' => '56640-000',
			),
			324 => 
			array (
				'id' => 5244,
				'nome' => 'Dormentes',
				'uf' => 'PE',
				'cep2' => '2605152',
				'estado_cod' => 16,
				'cep' => '56355-000',
			),
			325 => 
			array (
				'id' => 5246,
				'nome' => 'Escada',
				'uf' => 'PE',
				'cep2' => '2605202',
				'estado_cod' => 16,
				'cep' => '55500-000',
			),
			326 => 
			array (
				'id' => 5248,
				'nome' => 'Exu',
				'uf' => 'PE',
				'cep2' => '2605301',
				'estado_cod' => 16,
				'cep' => '56230-000',
			),
			327 => 
			array (
				'id' => 5250,
				'nome' => 'Feira Nova',
				'uf' => 'PE',
				'cep2' => '2605400',
				'estado_cod' => 16,
				'cep' => '55715-000',
			),
			328 => 
			array (
				'id' => 5252,
				'nome' => 'Fernando de Noronha',
				'uf' => 'PE',
				'cep2' => '',
				'estado_cod' => 16,
				'cep' => '53990-000',
			),
			329 => 
			array (
				'id' => 5253,
				'nome' => 'Ferreiros',
				'uf' => 'PE',
				'cep2' => '2605509',
				'estado_cod' => 16,
				'cep' => '55880-000',
			),
			330 => 
			array (
				'id' => 5254,
				'nome' => 'Flores',
				'uf' => 'PE',
				'cep2' => '2605608',
				'estado_cod' => 16,
				'cep' => '56850-000',
			),
			331 => 
			array (
				'id' => 5255,
				'nome' => 'Floresta',
				'uf' => 'PE',
				'cep2' => '2605707',
				'estado_cod' => 16,
				'cep' => '56400-000',
			),
			332 => 
			array (
				'id' => 5256,
				'nome' => 'Frei Miguelinho',
				'uf' => 'PE',
				'cep2' => '2605806',
				'estado_cod' => 16,
				'cep' => '55780-000',
			),
			333 => 
			array (
				'id' => 5258,
				'nome' => 'Gameleira',
				'uf' => 'PE',
				'cep2' => '2605905',
				'estado_cod' => 16,
				'cep' => '55530-000',
			),
			334 => 
			array (
				'id' => 5259,
				'nome' => 'Garanhuns',
				'uf' => 'PE',
				'cep2' => '2606002',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			335 => 
			array (
				'id' => 5260,
				'nome' => 'Glória do Goitá',
				'uf' => 'PE',
				'cep2' => '2606101',
				'estado_cod' => 16,
				'cep' => '55620-000',
			),
			336 => 
			array (
				'id' => 5261,
				'nome' => 'Goiana',
				'uf' => 'PE',
				'cep2' => '2606200',
				'estado_cod' => 16,
				'cep' => '55900-000',
			),
			337 => 
			array (
				'id' => 5263,
				'nome' => 'Granito',
				'uf' => 'PE',
				'cep2' => '2606309',
				'estado_cod' => 16,
				'cep' => '56160-000',
			),
			338 => 
			array (
				'id' => 5264,
				'nome' => 'Gravatá',
				'uf' => 'PE',
				'cep2' => '2606408',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			339 => 
			array (
				'id' => 5270,
				'nome' => 'Iati',
				'uf' => 'PE',
				'cep2' => '2606507',
				'estado_cod' => 16,
				'cep' => '55345-000',
			),
			340 => 
			array (
				'id' => 5271,
				'nome' => 'Ibimirim',
				'uf' => 'PE',
				'cep2' => '2606606',
				'estado_cod' => 16,
				'cep' => '56580-000',
			),
			341 => 
			array (
				'id' => 5272,
				'nome' => 'Ibirajuba',
				'uf' => 'PE',
				'cep2' => '2606705',
				'estado_cod' => 16,
				'cep' => '55390-000',
			),
			342 => 
			array (
				'id' => 5281,
				'nome' => 'Igarassu',
				'uf' => 'PE',
				'cep2' => '2606804',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			343 => 
			array (
				'id' => 5282,
				'nome' => 'Iguaraci',
				'uf' => 'PE',
				'cep2' => '2606903',
				'estado_cod' => 16,
				'cep' => '56840-000',
			),
			344 => 
			array (
				'id' => 5283,
				'nome' => 'Inajá',
				'uf' => 'PE',
				'cep2' => '2607000',
				'estado_cod' => 16,
				'cep' => '56560-000',
			),
			345 => 
			array (
				'id' => 5284,
				'nome' => 'Ingazeira',
				'uf' => 'PE',
				'cep2' => '2607109',
				'estado_cod' => 16,
				'cep' => '56830-000',
			),
			346 => 
			array (
				'id' => 5285,
				'nome' => 'Ipojuca',
				'uf' => 'PE',
				'cep2' => '2607208',
				'estado_cod' => 16,
				'cep' => '55590-000',
			),
			347 => 
			array (
				'id' => 5286,
				'nome' => 'Ipubi',
				'uf' => 'PE',
				'cep2' => '2607307',
				'estado_cod' => 16,
				'cep' => '56260-000',
			),
			348 => 
			array (
				'id' => 5291,
				'nome' => 'Itacuruba',
				'uf' => 'PE',
				'cep2' => '2607406',
				'estado_cod' => 16,
				'cep' => '56430-000',
			),
			349 => 
			array (
				'id' => 5292,
				'nome' => 'Itaíba',
				'uf' => 'PE',
				'cep2' => '2607505',
				'estado_cod' => 16,
				'cep' => '56550-000',
			),
			350 => 
			array (
				'id' => 5293,
				'nome' => 'Ilha de Itamaracá',
				'uf' => 'PE',
				'cep2' => '',
				'estado_cod' => 16,
				'cep' => '53900-000',
			),
			351 => 
			array (
				'id' => 5294,
				'nome' => 'Itambé',
				'uf' => 'PE',
				'cep2' => '2607653',
				'estado_cod' => 16,
				'cep' => '55920-000',
			),
			352 => 
			array (
				'id' => 5295,
				'nome' => 'Itapetim',
				'uf' => 'PE',
				'cep2' => '2607703',
				'estado_cod' => 16,
				'cep' => '56720-000',
			),
			353 => 
			array (
				'id' => 5296,
				'nome' => 'Itapissuma',
				'uf' => 'PE',
				'cep2' => '2607752',
				'estado_cod' => 16,
				'cep' => '53700-000',
			),
			354 => 
			array (
				'id' => 5297,
				'nome' => 'Itaquitinga',
				'uf' => 'PE',
				'cep2' => '2607802',
				'estado_cod' => 16,
				'cep' => '55950-000',
			),
			355 => 
			array (
				'id' => 5302,
				'nome' => 'Jaboatão dos Guararapes',
				'uf' => 'PE',
				'cep2' => '2607901',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			356 => 
			array (
				'id' => 5304,
				'nome' => 'Jaqueira',
				'uf' => 'PE',
				'cep2' => '2607950',
				'estado_cod' => 16,
				'cep' => '55409-000',
			),
			357 => 
			array (
				'id' => 5305,
				'nome' => 'Jataúba',
				'uf' => 'PE',
				'cep2' => '2608008',
				'estado_cod' => 16,
				'cep' => '55180-000',
			),
			358 => 
			array (
				'id' => 5307,
				'nome' => 'Jatobá',
				'uf' => 'PE',
				'cep2' => '2608057',
				'estado_cod' => 16,
				'cep' => '56470-000',
			),
			359 => 
			array (
				'id' => 5309,
				'nome' => 'João Alfredo',
				'uf' => 'PE',
				'cep2' => '2608107',
				'estado_cod' => 16,
				'cep' => '55720-000',
			),
			360 => 
			array (
				'id' => 5310,
				'nome' => 'Joaquim Nabuco',
				'uf' => 'PE',
				'cep2' => '2608206',
				'estado_cod' => 16,
				'cep' => '55535-000',
			),
			361 => 
			array (
				'id' => 5314,
				'nome' => 'Jucati',
				'uf' => 'PE',
				'cep2' => '2608255',
				'estado_cod' => 16,
				'cep' => '55398-000',
			),
			362 => 
			array (
				'id' => 5315,
				'nome' => 'Jupi',
				'uf' => 'PE',
				'cep2' => '2608305',
				'estado_cod' => 16,
				'cep' => '55395-000',
			),
			363 => 
			array (
				'id' => 5316,
				'nome' => 'Jurema',
				'uf' => 'PE',
				'cep2' => '2608404',
				'estado_cod' => 16,
				'cep' => '55480-000',
			),
			364 => 
			array (
				'id' => 5321,
				'nome' => 'Lagoa do Carro',
				'uf' => 'PE',
				'cep2' => '2608453',
				'estado_cod' => 16,
				'cep' => '55820-000',
			),
			365 => 
			array (
				'id' => 5322,
				'nome' => 'Lagoa do Itaenga',
				'uf' => 'PE',
				'cep2' => '2608503',
				'estado_cod' => 16,
				'cep' => '55840-000',
			),
			366 => 
			array (
				'id' => 5323,
				'nome' => 'Lagoa do Ouro',
				'uf' => 'PE',
				'cep2' => '2608602',
				'estado_cod' => 16,
				'cep' => '55320-000',
			),
			367 => 
			array (
				'id' => 5325,
				'nome' => 'Lagoa dos Gatos',
				'uf' => 'PE',
				'cep2' => '2608701',
				'estado_cod' => 16,
				'cep' => '55450-000',
			),
			368 => 
			array (
				'id' => 5326,
				'nome' => 'Lagoa Grande',
				'uf' => 'PE',
				'cep2' => '2608750',
				'estado_cod' => 16,
				'cep' => '56395-000',
			),
			369 => 
			array (
				'id' => 5329,
				'nome' => 'Lajedo',
				'uf' => 'PE',
				'cep2' => '2608800',
				'estado_cod' => 16,
				'cep' => '55385-000',
			),
			370 => 
			array (
				'id' => 5331,
				'nome' => 'Limoeiro',
				'uf' => 'PE',
				'cep2' => '2608909',
				'estado_cod' => 16,
				'cep' => '55700-000',
			),
			371 => 
			array (
				'id' => 5334,
				'nome' => 'Macaparana',
				'uf' => 'PE',
				'cep2' => '2609006',
				'estado_cod' => 16,
				'cep' => '55865-000',
			),
			372 => 
			array (
				'id' => 5335,
				'nome' => 'Machados',
				'uf' => 'PE',
				'cep2' => '2609105',
				'estado_cod' => 16,
				'cep' => '55740-000',
			),
			373 => 
			array (
				'id' => 5337,
				'nome' => 'Manari',
				'uf' => 'PE',
				'cep2' => '2609154',
				'estado_cod' => 16,
				'cep' => '56565-000',
			),
			374 => 
			array (
				'id' => 5341,
				'nome' => 'Maraial',
				'uf' => 'PE',
				'cep2' => '2609204',
				'estado_cod' => 16,
				'cep' => '55405-000',
			),
			375 => 
			array (
				'id' => 5345,
				'nome' => 'Mirandiba',
				'uf' => 'PE',
				'cep2' => '2609303',
				'estado_cod' => 16,
				'cep' => '56980-000',
			),
			376 => 
			array (
				'id' => 5347,
				'nome' => 'Moreilândia',
				'uf' => 'PE',
				'cep2' => '2614303',
				'estado_cod' => 16,
				'cep' => '56150-000',
			),
			377 => 
			array (
				'id' => 5348,
				'nome' => 'Moreno',
				'uf' => 'PE',
				'cep2' => '2609402',
				'estado_cod' => 16,
				'cep' => '54800-000',
			),
			378 => 
			array (
				'id' => 5355,
				'nome' => 'Nazaré da Mata',
				'uf' => 'PE',
				'cep2' => '2609501',
				'estado_cod' => 16,
				'cep' => '55800-000',
			),
			379 => 
			array (
				'id' => 5362,
				'nome' => 'Olinda',
				'uf' => 'PE',
				'cep2' => '2609600',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			380 => 
			array (
				'id' => 5365,
				'nome' => 'Orobó',
				'uf' => 'PE',
				'cep2' => '2609709',
				'estado_cod' => 16,
				'cep' => '55745-000',
			),
			381 => 
			array (
				'id' => 5366,
				'nome' => 'Orocó',
				'uf' => 'PE',
				'cep2' => '2609808',
				'estado_cod' => 16,
				'cep' => '56170-000',
			),
			382 => 
			array (
				'id' => 5367,
				'nome' => 'Ouricuri',
				'uf' => 'PE',
				'cep2' => '2609907',
				'estado_cod' => 16,
				'cep' => '56200-000',
			),
			383 => 
			array (
				'id' => 5369,
				'nome' => 'Palmares',
				'uf' => 'PE',
				'cep2' => '2610004',
				'estado_cod' => 16,
				'cep' => '55540-000',
			),
			384 => 
			array (
				'id' => 5370,
				'nome' => 'Palmeirina',
				'uf' => 'PE',
				'cep2' => '2610103',
				'estado_cod' => 16,
				'cep' => '55310-000',
			),
			385 => 
			array (
				'id' => 5371,
				'nome' => 'Panelas',
				'uf' => 'PE',
				'cep2' => '2610202',
				'estado_cod' => 16,
				'cep' => '55470-000',
			),
			386 => 
			array (
				'id' => 5377,
				'nome' => 'Paranatama',
				'uf' => 'PE',
				'cep2' => '2610301',
				'estado_cod' => 16,
				'cep' => '55355-000',
			),
			387 => 
			array (
				'id' => 5379,
				'nome' => 'Parnamirim',
				'uf' => 'PE',
				'cep2' => '2610400',
				'estado_cod' => 16,
				'cep' => '56163-000',
			),
			388 => 
			array (
				'id' => 5381,
				'nome' => 'Passira',
				'uf' => 'PE',
				'cep2' => '2610509',
				'estado_cod' => 16,
				'cep' => '55650-000',
			),
			389 => 
			array (
				'id' => 5383,
				'nome' => 'Paudalho',
				'uf' => 'PE',
				'cep2' => '2610608',
				'estado_cod' => 16,
				'cep' => '55825-000',
			),
			390 => 
			array (
				'id' => 5384,
				'nome' => 'Paulista',
				'uf' => 'PE',
				'cep2' => '2610707',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			391 => 
			array (
				'id' => 5385,
				'nome' => 'Pedra',
				'uf' => 'PE',
				'cep2' => '2610806',
				'estado_cod' => 16,
				'cep' => '55280-000',
			),
			392 => 
			array (
				'id' => 5387,
				'nome' => 'Pesqueira',
				'uf' => 'PE',
				'cep2' => '2610905',
				'estado_cod' => 16,
				'cep' => '55200-000',
			),
			393 => 
			array (
				'id' => 5388,
				'nome' => 'Petrolândia',
				'uf' => 'PE',
				'cep2' => '2611002',
				'estado_cod' => 16,
				'cep' => '56460-000',
			),
			394 => 
			array (
				'id' => 5389,
				'nome' => 'Petrolina',
				'uf' => 'PE',
				'cep2' => '2611101',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			395 => 
			array (
				'id' => 5391,
				'nome' => 'Poção',
				'uf' => 'PE',
				'cep2' => '2611200',
				'estado_cod' => 16,
				'cep' => '55240-000',
			),
			396 => 
			array (
				'id' => 5395,
				'nome' => 'Pombos',
				'uf' => 'PE',
				'cep2' => '2611309',
				'estado_cod' => 16,
				'cep' => '55630-000',
			),
			397 => 
			array (
				'id' => 5399,
				'nome' => 'Primavera',
				'uf' => 'PE',
				'cep2' => '2611408',
				'estado_cod' => 16,
				'cep' => '55510-000',
			),
			398 => 
			array (
				'id' => 5400,
				'nome' => 'Quipapá',
				'uf' => 'PE',
				'cep2' => '2611507',
				'estado_cod' => 16,
				'cep' => '55415-000',
			),
			399 => 
			array (
				'id' => 5402,
				'nome' => 'Quixabá',
				'uf' => 'PE',
				'cep2' => '',
				'estado_cod' => 16,
				'cep' => '56828-000',
			),
			400 => 
			array (
				'id' => 5406,
				'nome' => 'Recife',
				'uf' => 'PE',
				'cep2' => '2611606',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			401 => 
			array (
				'id' => 5407,
				'nome' => 'Riacho das Almas',
				'uf' => 'PE',
				'cep2' => '2611705',
				'estado_cod' => 16,
				'cep' => '55120-000',
			),
			402 => 
			array (
				'id' => 5411,
				'nome' => 'Ribeirão',
				'uf' => 'PE',
				'cep2' => '2611804',
				'estado_cod' => 16,
				'cep' => '55520-000',
			),
			403 => 
			array (
				'id' => 5413,
				'nome' => 'Rio Formoso',
				'uf' => 'PE',
				'cep2' => '2611903',
				'estado_cod' => 16,
				'cep' => '55570-000',
			),
			404 => 
			array (
				'id' => 5414,
				'nome' => 'Sairé',
				'uf' => 'PE',
				'cep2' => '2612000',
				'estado_cod' => 16,
				'cep' => '55695-000',
			),
			405 => 
			array (
				'id' => 5415,
				'nome' => 'Salgadinho',
				'uf' => 'PE',
				'cep2' => '2612109',
				'estado_cod' => 16,
				'cep' => '55675-000',
			),
			406 => 
			array (
				'id' => 5416,
				'nome' => 'Salgueiro',
				'uf' => 'PE',
				'cep2' => '2612208',
				'estado_cod' => 16,
				'cep' => '56000-000',
			),
			407 => 
			array (
				'id' => 5417,
				'nome' => 'Saloá',
				'uf' => 'PE',
				'cep2' => '2612307',
				'estado_cod' => 16,
				'cep' => '55350-000',
			),
			408 => 
			array (
				'id' => 5419,
				'nome' => 'Sanharó',
				'uf' => 'PE',
				'cep2' => '2612406',
				'estado_cod' => 16,
				'cep' => '55250-000',
			),
			409 => 
			array (
				'id' => 5420,
				'nome' => 'Santa Cruz',
				'uf' => 'PE',
				'cep2' => '2612455',
				'estado_cod' => 16,
				'cep' => '56215-000',
			),
			410 => 
			array (
				'id' => 5421,
				'nome' => 'Santa Cruz da Baixa Verde',
				'uf' => 'PE',
				'cep2' => '2612471',
				'estado_cod' => 16,
				'cep' => '56895-000',
			),
			411 => 
			array (
				'id' => 5422,
				'nome' => 'Santa Cruz do Capibaribe',
				'uf' => 'PE',
				'cep2' => '2612505',
				'estado_cod' => 16,
				'cep' => '55190-000',
			),
			412 => 
			array (
				'id' => 5423,
				'nome' => 'Santa Filomena',
				'uf' => 'PE',
				'cep2' => '2612554',
				'estado_cod' => 16,
				'cep' => '56210-000',
			),
			413 => 
			array (
				'id' => 5424,
				'nome' => 'Santa Maria da Boa Vista',
				'uf' => 'PE',
				'cep2' => '2612604',
				'estado_cod' => 16,
				'cep' => '56380-000',
			),
			414 => 
			array (
				'id' => 5425,
				'nome' => 'Santa Maria do Cambucá',
				'uf' => 'PE',
				'cep2' => '2612703',
				'estado_cod' => 16,
				'cep' => '55765-000',
			),
			415 => 
			array (
				'id' => 5427,
				'nome' => 'Santa Terezinha',
				'uf' => 'PE',
				'cep2' => '2612802',
				'estado_cod' => 16,
				'cep' => '56750-000',
			),
			416 => 
			array (
				'id' => 5433,
				'nome' => 'São Benedito do Sul',
				'uf' => 'PE',
				'cep2' => '2612901',
				'estado_cod' => 16,
				'cep' => '55410-000',
			),
			417 => 
			array (
				'id' => 5434,
				'nome' => 'São Bento do Una',
				'uf' => 'PE',
				'cep2' => '2613008',
				'estado_cod' => 16,
				'cep' => '55370-000',
			),
			418 => 
			array (
				'id' => 5436,
				'nome' => 'São Caitano',
				'uf' => 'PE',
				'cep2' => '2613107',
				'estado_cod' => 16,
				'cep' => '55130-000',
			),
			419 => 
			array (
				'id' => 5438,
				'nome' => 'São João',
				'uf' => 'PE',
				'cep2' => '2613206',
				'estado_cod' => 16,
				'cep' => '55435-000',
			),
			420 => 
			array (
				'id' => 5439,
				'nome' => 'São Joaquim do Monte',
				'uf' => 'PE',
				'cep2' => '2613305',
				'estado_cod' => 16,
				'cep' => '55670-000',
			),
			421 => 
			array (
				'id' => 5441,
				'nome' => 'São José da Coroa Grande',
				'uf' => 'PE',
				'cep2' => '2613404',
				'estado_cod' => 16,
				'cep' => '55565-000',
			),
			422 => 
			array (
				'id' => 5442,
				'nome' => 'São José do Belmonte',
				'uf' => 'PE',
				'cep2' => '2613503',
				'estado_cod' => 16,
				'cep' => '56950-000',
			),
			423 => 
			array (
				'id' => 5443,
				'nome' => 'São José do Egito',
				'uf' => 'PE',
				'cep2' => '2613602',
				'estado_cod' => 16,
				'cep' => '56700-000',
			),
			424 => 
			array (
				'id' => 5445,
				'nome' => 'São Lourenço da Mata',
				'uf' => 'PE',
				'cep2' => '2613701',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			425 => 
			array (
				'id' => 5448,
				'nome' => 'São Vicente Ferrer',
				'uf' => 'PE',
				'cep2' => '2613800',
				'estado_cod' => 16,
				'cep' => '55860-000',
			),
			426 => 
			array (
				'id' => 5453,
				'nome' => 'Serra Talhada',
				'uf' => 'PE',
				'cep2' => '2613909',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			427 => 
			array (
				'id' => 5454,
				'nome' => 'Serrita',
				'uf' => 'PE',
				'cep2' => '2614006',
				'estado_cod' => 16,
				'cep' => '56140-000',
			),
			428 => 
			array (
				'id' => 5456,
				'nome' => 'Sertânia',
				'uf' => 'PE',
				'cep2' => '2614105',
				'estado_cod' => 16,
				'cep' => '56600-000',
			),
			429 => 
			array (
				'id' => 5459,
				'nome' => 'Sirinhaém',
				'uf' => 'PE',
				'cep2' => '2614204',
				'estado_cod' => 16,
				'cep' => '55580-000',
			),
			430 => 
			array (
				'id' => 5461,
				'nome' => 'Solidão',
				'uf' => 'PE',
				'cep2' => '2614402',
				'estado_cod' => 16,
				'cep' => '56795-000',
			),
			431 => 
			array (
				'id' => 5462,
				'nome' => 'Surubim',
				'uf' => 'PE',
				'cep2' => '2614501',
				'estado_cod' => 16,
				'cep' => '55750-000',
			),
			432 => 
			array (
				'id' => 5463,
				'nome' => 'Tabira',
				'uf' => 'PE',
				'cep2' => '2614600',
				'estado_cod' => 16,
				'cep' => '56780-000',
			),
			433 => 
			array (
				'id' => 5465,
				'nome' => 'Tacaimbó',
				'uf' => 'PE',
				'cep2' => '2614709',
				'estado_cod' => 16,
				'cep' => '55140-000',
			),
			434 => 
			array (
				'id' => 5466,
				'nome' => 'Tacaratu',
				'uf' => 'PE',
				'cep2' => '2614808',
				'estado_cod' => 16,
				'cep' => '56480-000',
			),
			435 => 
			array (
				'id' => 5467,
				'nome' => 'Tamandaré',
				'uf' => 'PE',
				'cep2' => '2614857',
				'estado_cod' => 16,
				'cep' => '55578-000',
			),
			436 => 
			array (
				'id' => 5470,
				'nome' => 'Taquaritinga do Norte',
				'uf' => 'PE',
				'cep2' => '2615003',
				'estado_cod' => 16,
				'cep' => '55790-000',
			),
			437 => 
			array (
				'id' => 5474,
				'nome' => 'Terezinha',
				'uf' => 'PE',
				'cep2' => '2615102',
				'estado_cod' => 16,
				'cep' => '55305-000',
			),
			438 => 
			array (
				'id' => 5475,
				'nome' => 'Terra Nova',
				'uf' => 'PE',
				'cep2' => '2615201',
				'estado_cod' => 16,
				'cep' => '56190-000',
			),
			439 => 
			array (
				'id' => 5476,
				'nome' => 'Timbaúba',
				'uf' => 'PE',
				'cep2' => '2615300',
				'estado_cod' => 16,
				'cep' => '55870-000',
			),
			440 => 
			array (
				'id' => 5478,
				'nome' => 'Toritama',
				'uf' => 'PE',
				'cep2' => '2615409',
				'estado_cod' => 16,
				'cep' => '55125-000',
			),
			441 => 
			array (
				'id' => 5479,
				'nome' => 'Tracunhaém',
				'uf' => 'PE',
				'cep2' => '2615508',
				'estado_cod' => 16,
				'cep' => '55805-000',
			),
			442 => 
			array (
				'id' => 5482,
				'nome' => 'Trindade',
				'uf' => 'PE',
				'cep2' => '2615607',
				'estado_cod' => 16,
				'cep' => '56250-000',
			),
			443 => 
			array (
				'id' => 5483,
				'nome' => 'Triunfo',
				'uf' => 'PE',
				'cep2' => '2615706',
				'estado_cod' => 16,
				'cep' => '56870-000',
			),
			444 => 
			array (
				'id' => 5485,
				'nome' => 'Tupanatinga',
				'uf' => 'PE',
				'cep2' => '2615805',
				'estado_cod' => 16,
				'cep' => '56540-000',
			),
			445 => 
			array (
				'id' => 5487,
				'nome' => 'Tuparetama',
				'uf' => 'PE',
				'cep2' => '2615904',
				'estado_cod' => 16,
				'cep' => '56760-000',
			),
			446 => 
			array (
				'id' => 5496,
				'nome' => 'Venturosa',
				'uf' => 'PE',
				'cep2' => '2616001',
				'estado_cod' => 16,
				'cep' => '55270-000',
			),
			447 => 
			array (
				'id' => 5497,
				'nome' => 'Verdejante',
				'uf' => 'PE',
				'cep2' => '2616100',
				'estado_cod' => 16,
				'cep' => '56120-000',
			),
			448 => 
			array (
				'id' => 5498,
				'nome' => 'Vertente do Lério',
				'uf' => 'PE',
				'cep2' => '2616183',
				'estado_cod' => 16,
				'cep' => '55760-000',
			),
			449 => 
			array (
				'id' => 5499,
				'nome' => 'Vertentes',
				'uf' => 'PE',
				'cep2' => '2616209',
				'estado_cod' => 16,
				'cep' => '55770-000',
			),
			450 => 
			array (
				'id' => 5500,
				'nome' => 'Vicência',
				'uf' => 'PE',
				'cep2' => '2616308',
				'estado_cod' => 16,
				'cep' => '55850-000',
			),
			451 => 
			array (
				'id' => 5503,
				'nome' => 'Vitória de Santo Antão',
				'uf' => 'PE',
				'cep2' => '2616407',
				'estado_cod' => 16,
				'cep' => 'LOC',
			),
			452 => 
			array (
				'id' => 5505,
				'nome' => 'Xexéu',
				'uf' => 'PE',
				'cep2' => '2616506',
				'estado_cod' => 16,
				'cep' => '55555-000',
			),
			453 => 
			array (
				'id' => 5508,
				'nome' => 'Acauã',
				'uf' => 'PI',
				'cep2' => '2200053',
				'estado_cod' => 17,
				'cep' => '64748-000',
			),
			454 => 
			array (
				'id' => 5509,
				'nome' => 'Agricolândia',
				'uf' => 'PI',
				'cep2' => '2200103',
				'estado_cod' => 17,
				'cep' => '64440-000',
			),
			455 => 
			array (
				'id' => 5510,
				'nome' => 'Água Branca',
				'uf' => 'PI',
				'cep2' => '2200202',
				'estado_cod' => 17,
				'cep' => '64460-000',
			),
			456 => 
			array (
				'id' => 5511,
				'nome' => 'Alagoinha do Piauí',
				'uf' => 'PI',
				'cep2' => '2200251',
				'estado_cod' => 17,
				'cep' => '64655-000',
			),
			457 => 
			array (
				'id' => 5512,
				'nome' => 'Alegrete do Piauí',
				'uf' => 'PI',
				'cep2' => '2200277',
				'estado_cod' => 17,
				'cep' => '64675-000',
			),
			458 => 
			array (
				'id' => 5513,
				'nome' => 'Alto Longá',
				'uf' => 'PI',
				'cep2' => '2200301',
				'estado_cod' => 17,
				'cep' => '64360-000',
			),
			459 => 
			array (
				'id' => 5514,
				'nome' => 'Altos',
				'uf' => 'PI',
				'cep2' => '2200400',
				'estado_cod' => 17,
				'cep' => '64290-000',
			),
			460 => 
			array (
				'id' => 5515,
				'nome' => 'Alvorada do Gurguéia',
				'uf' => 'PI',
				'cep2' => '2200459',
				'estado_cod' => 17,
				'cep' => '64923-000',
			),
			461 => 
			array (
				'id' => 5516,
				'nome' => 'Amarante',
				'uf' => 'PI',
				'cep2' => '2200509',
				'estado_cod' => 17,
				'cep' => '64400-000',
			),
			462 => 
			array (
				'id' => 5517,
				'nome' => 'Angical do Piauí',
				'uf' => 'PI',
				'cep2' => '2200608',
				'estado_cod' => 17,
				'cep' => '64410-000',
			),
			463 => 
			array (
				'id' => 5518,
				'nome' => 'Anísio de Abreu',
				'uf' => 'PI',
				'cep2' => '2200707',
				'estado_cod' => 17,
				'cep' => '64780-000',
			),
			464 => 
			array (
				'id' => 5519,
				'nome' => 'Antônio Almeida',
				'uf' => 'PI',
				'cep2' => '2200806',
				'estado_cod' => 17,
				'cep' => '64855-000',
			),
			465 => 
			array (
				'id' => 5520,
				'nome' => 'Aroazes',
				'uf' => 'PI',
				'cep2' => '2200905',
				'estado_cod' => 17,
				'cep' => '64310-000',
			),
			466 => 
			array (
				'id' => 5521,
				'nome' => 'Arraial',
				'uf' => 'PI',
				'cep2' => '2201002',
				'estado_cod' => 17,
				'cep' => '64480-000',
			),
			467 => 
			array (
				'id' => 5522,
				'nome' => 'Assunção do Piauí',
				'uf' => 'PI',
				'cep2' => '2201051',
				'estado_cod' => 17,
				'cep' => '64333-000',
			),
			468 => 
			array (
				'id' => 5523,
				'nome' => 'Avelino Lopes',
				'uf' => 'PI',
				'cep2' => '2201101',
				'estado_cod' => 17,
				'cep' => '64965-000',
			),
			469 => 
			array (
				'id' => 5524,
				'nome' => 'Baixa Grande do Ribeiro',
				'uf' => 'PI',
				'cep2' => '2201150',
				'estado_cod' => 17,
				'cep' => '64868-000',
			),
			470 => 
			array (
				'id' => 5525,
				'nome' => 'Barra D\'Alcântara',
				'uf' => 'PI',
				'cep2' => '2201176',
				'estado_cod' => 17,
				'cep' => '64528-000',
			),
			471 => 
			array (
				'id' => 5526,
				'nome' => 'Barras',
				'uf' => 'PI',
				'cep2' => '2201200',
				'estado_cod' => 17,
				'cep' => '64100-000',
			),
			472 => 
			array (
				'id' => 5527,
				'nome' => 'Barreiras do Piauí',
				'uf' => 'PI',
				'cep2' => '2201309',
				'estado_cod' => 17,
				'cep' => '64990-000',
			),
			473 => 
			array (
				'id' => 5528,
				'nome' => 'Barro Duro',
				'uf' => 'PI',
				'cep2' => '2201408',
				'estado_cod' => 17,
				'cep' => '64455-000',
			),
			474 => 
			array (
				'id' => 5529,
				'nome' => 'Batalha',
				'uf' => 'PI',
				'cep2' => '2201507',
				'estado_cod' => 17,
				'cep' => '64190-000',
			),
			475 => 
			array (
				'id' => 5530,
				'nome' => 'Bela Vista do Piauí',
				'uf' => 'PI',
				'cep2' => '2201556',
				'estado_cod' => 17,
				'cep' => '64705-000',
			),
			476 => 
			array (
				'id' => 5531,
				'nome' => 'Belém do Piauí',
				'uf' => 'PI',
				'cep2' => '2201572',
				'estado_cod' => 17,
				'cep' => '64678-000',
			),
			477 => 
			array (
				'id' => 5532,
				'nome' => 'Beneditinos',
				'uf' => 'PI',
				'cep2' => '2201606',
				'estado_cod' => 17,
				'cep' => '64380-000',
			),
			478 => 
			array (
				'id' => 5533,
				'nome' => 'Bertolínia',
				'uf' => 'PI',
				'cep2' => '2201705',
				'estado_cod' => 17,
				'cep' => '64870-000',
			),
			479 => 
			array (
				'id' => 5534,
				'nome' => 'Betânia do Piauí',
				'uf' => 'PI',
				'cep2' => '2201739',
				'estado_cod' => 17,
				'cep' => '64753-000',
			),
			480 => 
			array (
				'id' => 5535,
				'nome' => 'Boa Hora',
				'uf' => 'PI',
				'cep2' => '2201770',
				'estado_cod' => 17,
				'cep' => '64108-000',
			),
			481 => 
			array (
				'id' => 5536,
				'nome' => 'Bocaina',
				'uf' => 'PI',
				'cep2' => '2201804',
				'estado_cod' => 17,
				'cep' => '64630-000',
			),
			482 => 
			array (
				'id' => 5537,
				'nome' => 'Bom Jesus',
				'uf' => 'PI',
				'cep2' => '2201903',
				'estado_cod' => 17,
				'cep' => '64900-000',
			),
			483 => 
			array (
				'id' => 5538,
				'nome' => 'Bom Princípio do Piauí',
				'uf' => 'PI',
				'cep2' => '2201919',
				'estado_cod' => 17,
				'cep' => '64225-000',
			),
			484 => 
			array (
				'id' => 5539,
				'nome' => 'Bonfim do Piauí',
				'uf' => 'PI',
				'cep2' => '2201929',
				'estado_cod' => 17,
				'cep' => '64775-000',
			),
			485 => 
			array (
				'id' => 5540,
				'nome' => 'Boqueirão do Piauí',
				'uf' => 'PI',
				'cep2' => '2201945',
				'estado_cod' => 17,
				'cep' => '64283-000',
			),
			486 => 
			array (
				'id' => 5541,
				'nome' => 'Brasileira',
				'uf' => 'PI',
				'cep2' => '2201960',
				'estado_cod' => 17,
				'cep' => '64265-000',
			),
			487 => 
			array (
				'id' => 5542,
				'nome' => 'Brejo do Piauí',
				'uf' => 'PI',
				'cep2' => '2201988',
				'estado_cod' => 17,
				'cep' => '64895-000',
			),
			488 => 
			array (
				'id' => 5543,
				'nome' => 'Buriti dos Lopes',
				'uf' => 'PI',
				'cep2' => '2202000',
				'estado_cod' => 17,
				'cep' => '64230-000',
			),
			489 => 
			array (
				'id' => 5544,
				'nome' => 'Buriti dos Montes',
				'uf' => 'PI',
				'cep2' => '2202026',
				'estado_cod' => 17,
				'cep' => '64345-000',
			),
			490 => 
			array (
				'id' => 5545,
				'nome' => 'Cabeceiras do Piauí',
				'uf' => 'PI',
				'cep2' => '2202059',
				'estado_cod' => 17,
				'cep' => '64105-000',
			),
			491 => 
			array (
				'id' => 5546,
				'nome' => 'Cajazeiras do Piauí',
				'uf' => 'PI',
				'cep2' => '2202075',
				'estado_cod' => 17,
				'cep' => '64514-000',
			),
			492 => 
			array (
				'id' => 5547,
				'nome' => 'Cajueiro da Praia',
				'uf' => 'PI',
				'cep2' => '2202083',
				'estado_cod' => 17,
				'cep' => '64222-000',
			),
			493 => 
			array (
				'id' => 5548,
				'nome' => 'Caldeirão Grande do Piauí',
				'uf' => 'PI',
				'cep2' => '2202091',
				'estado_cod' => 17,
				'cep' => '64695-000',
			),
			494 => 
			array (
				'id' => 5549,
				'nome' => 'Campinas do Piauí',
				'uf' => 'PI',
				'cep2' => '2202109',
				'estado_cod' => 17,
				'cep' => '64730-000',
			),
			495 => 
			array (
				'id' => 5550,
				'nome' => 'Campo Alegre do Fidalgo',
				'uf' => 'PI',
				'cep2' => '2202117',
				'estado_cod' => 17,
				'cep' => '64767-000',
			),
			496 => 
			array (
				'id' => 5551,
				'nome' => 'Campo Grande do Piauí',
				'uf' => 'PI',
				'cep2' => '2202133',
				'estado_cod' => 17,
				'cep' => '64578-000',
			),
			497 => 
			array (
				'id' => 5552,
				'nome' => 'Campo Largo do Piauí',
				'uf' => 'PI',
				'cep2' => '2202174',
				'estado_cod' => 17,
				'cep' => '64148-000',
			),
			498 => 
			array (
				'id' => 5553,
				'nome' => 'Campo Maior',
				'uf' => 'PI',
				'cep2' => '2202208',
				'estado_cod' => 17,
				'cep' => '64280-000',
			),
			499 => 
			array (
				'id' => 5554,
				'nome' => 'Canavieira',
				'uf' => 'PI',
				'cep2' => '2202251',
				'estado_cod' => 17,
				'cep' => '64833-000',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 5555,
				'nome' => 'Canto do Buriti',
				'uf' => 'PI',
				'cep2' => '2202307',
				'estado_cod' => 17,
				'cep' => '64890-000',
			),
			1 => 
			array (
				'id' => 5556,
				'nome' => 'Capitão de Campos',
				'uf' => 'PI',
				'cep2' => '2202406',
				'estado_cod' => 17,
				'cep' => '64270-000',
			),
			2 => 
			array (
				'id' => 5557,
				'nome' => 'Capitão Gervásio Oliveira',
				'uf' => 'PI',
				'cep2' => '2202455',
				'estado_cod' => 17,
				'cep' => '64763-000',
			),
			3 => 
			array (
				'id' => 5558,
				'nome' => 'Caracol',
				'uf' => 'PI',
				'cep2' => '2202505',
				'estado_cod' => 17,
				'cep' => '64795-000',
			),
			4 => 
			array (
				'id' => 5559,
				'nome' => 'Caraúbas do Piauí',
				'uf' => 'PI',
				'cep2' => '2202539',
				'estado_cod' => 17,
				'cep' => '64233-000',
			),
			5 => 
			array (
				'id' => 5560,
				'nome' => 'Caridade do Piauí',
				'uf' => 'PI',
				'cep2' => '2202554',
				'estado_cod' => 17,
				'cep' => '64590-000',
			),
			6 => 
			array (
				'id' => 5561,
				'nome' => 'Castelo do Piauí',
				'uf' => 'PI',
				'cep2' => '2202604',
				'estado_cod' => 17,
				'cep' => '64340-000',
			),
			7 => 
			array (
				'id' => 5562,
				'nome' => 'Caxingó',
				'uf' => 'PI',
				'cep2' => '2202653',
				'estado_cod' => 17,
				'cep' => '64228-000',
			),
			8 => 
			array (
				'id' => 5563,
				'nome' => 'Cocal',
				'uf' => 'PI',
				'cep2' => '2202703',
				'estado_cod' => 17,
				'cep' => '64235-000',
			),
			9 => 
			array (
				'id' => 5564,
				'nome' => 'Cocal de Telha',
				'uf' => 'PI',
				'cep2' => '2202711',
				'estado_cod' => 17,
				'cep' => '64278-000',
			),
			10 => 
			array (
				'id' => 5565,
				'nome' => 'Cocal dos Alves',
				'uf' => 'PI',
				'cep2' => '2202729',
				'estado_cod' => 17,
				'cep' => '64238-000',
			),
			11 => 
			array (
				'id' => 5566,
				'nome' => 'Coivaras',
				'uf' => 'PI',
				'cep2' => '2202737',
				'estado_cod' => 17,
				'cep' => '64335-000',
			),
			12 => 
			array (
				'id' => 5567,
				'nome' => 'Colônia do Gurguéia',
				'uf' => 'PI',
				'cep2' => '2202752',
				'estado_cod' => 17,
				'cep' => '64885-000',
			),
			13 => 
			array (
				'id' => 5568,
				'nome' => 'Colônia do Piauí',
				'uf' => 'PI',
				'cep2' => '2202778',
				'estado_cod' => 17,
				'cep' => '64516-000',
			),
			14 => 
			array (
				'id' => 5569,
				'nome' => 'Conceição do Canindé',
				'uf' => 'PI',
				'cep2' => '2202802',
				'estado_cod' => 17,
				'cep' => '64740-000',
			),
			15 => 
			array (
				'id' => 5570,
				'nome' => 'Coronel José Dias',
				'uf' => 'PI',
				'cep2' => '2202851',
				'estado_cod' => 17,
				'cep' => '64793-000',
			),
			16 => 
			array (
				'id' => 5571,
				'nome' => 'Corrente',
				'uf' => 'PI',
				'cep2' => '2202901',
				'estado_cod' => 17,
				'cep' => '64980-000',
			),
			17 => 
			array (
				'id' => 5572,
				'nome' => 'Cristalândia do Piauí',
				'uf' => 'PI',
				'cep2' => '2203008',
				'estado_cod' => 17,
				'cep' => '64995-000',
			),
			18 => 
			array (
				'id' => 5573,
				'nome' => 'Cristino Castro',
				'uf' => 'PI',
				'cep2' => '2203107',
				'estado_cod' => 17,
				'cep' => '64920-000',
			),
			19 => 
			array (
				'id' => 5574,
				'nome' => 'Curimatá',
				'uf' => 'PI',
				'cep2' => '2203206',
				'estado_cod' => 17,
				'cep' => '64960-000',
			),
			20 => 
			array (
				'id' => 5575,
				'nome' => 'Currais',
				'uf' => 'PI',
				'cep2' => '2203230',
				'estado_cod' => 17,
				'cep' => '64905-000',
			),
			21 => 
			array (
				'id' => 5576,
				'nome' => 'Curral Novo do Piauí',
				'uf' => 'PI',
				'cep2' => '2203271',
				'estado_cod' => 17,
				'cep' => '64595-000',
			),
			22 => 
			array (
				'id' => 5577,
				'nome' => 'Curralinhos',
				'uf' => 'PI',
				'cep2' => '2203255',
				'estado_cod' => 17,
				'cep' => '64453-000',
			),
			23 => 
			array (
				'id' => 5578,
				'nome' => 'Demerval Lobão',
				'uf' => 'PI',
				'cep2' => '2203305',
				'estado_cod' => 17,
				'cep' => '64390-000',
			),
			24 => 
			array (
				'id' => 5579,
				'nome' => 'Dirceu Arcoverde',
				'uf' => 'PI',
				'cep2' => '2203354',
				'estado_cod' => 17,
				'cep' => '64785-000',
			),
			25 => 
			array (
				'id' => 5580,
				'nome' => 'Dom Expedito Lopes',
				'uf' => 'PI',
				'cep2' => '2203404',
				'estado_cod' => 17,
				'cep' => '64620-000',
			),
			26 => 
			array (
				'id' => 5581,
				'nome' => 'Dom Inocêncio',
				'uf' => 'PI',
				'cep2' => '2203453',
				'estado_cod' => 17,
				'cep' => '64790-000',
			),
			27 => 
			array (
				'id' => 5582,
				'nome' => 'Domingos Mourão',
				'uf' => 'PI',
				'cep2' => '2203420',
				'estado_cod' => 17,
				'cep' => '64250-000',
			),
			28 => 
			array (
				'id' => 5583,
				'nome' => 'Elesbão Veloso',
				'uf' => 'PI',
				'cep2' => '2203503',
				'estado_cod' => 17,
				'cep' => '64325-000',
			),
			29 => 
			array (
				'id' => 5584,
				'nome' => 'Eliseu Martins',
				'uf' => 'PI',
				'cep2' => '2203602',
				'estado_cod' => 17,
				'cep' => '64880-000',
			),
			30 => 
			array (
				'id' => 5585,
				'nome' => 'Esperantina',
				'uf' => 'PI',
				'cep2' => '2203701',
				'estado_cod' => 17,
				'cep' => '64180-000',
			),
			31 => 
			array (
				'id' => 5586,
				'nome' => 'Fartura do Piauí',
				'uf' => 'PI',
				'cep2' => '2203750',
				'estado_cod' => 17,
				'cep' => '64788-000',
			),
			32 => 
			array (
				'id' => 5587,
				'nome' => 'Flores do Piauí',
				'uf' => 'PI',
				'cep2' => '2203800',
				'estado_cod' => 17,
				'cep' => '64815-000',
			),
			33 => 
			array (
				'id' => 5588,
				'nome' => 'Floresta do Piauí',
				'uf' => 'PI',
				'cep2' => '2203859',
				'estado_cod' => 17,
				'cep' => '64563-000',
			),
			34 => 
			array (
				'id' => 5589,
				'nome' => 'Floriano',
				'uf' => 'PI',
				'cep2' => '2203909',
				'estado_cod' => 17,
				'cep' => '64800-000',
			),
			35 => 
			array (
				'id' => 5590,
				'nome' => 'Francinópolis',
				'uf' => 'PI',
				'cep2' => '2204006',
				'estado_cod' => 17,
				'cep' => '64520-000',
			),
			36 => 
			array (
				'id' => 5591,
				'nome' => 'Francisco Ayres',
				'uf' => 'PI',
				'cep2' => '2204105',
				'estado_cod' => 17,
				'cep' => '64475-000',
			),
			37 => 
			array (
				'id' => 5592,
				'nome' => 'Francisco Macedo',
				'uf' => 'PI',
				'cep2' => '2204154',
				'estado_cod' => 17,
				'cep' => '64683-000',
			),
			38 => 
			array (
				'id' => 5593,
				'nome' => 'Francisco Santos',
				'uf' => 'PI',
				'cep2' => '2204204',
				'estado_cod' => 17,
				'cep' => '64645-000',
			),
			39 => 
			array (
				'id' => 5594,
				'nome' => 'Fronteiras',
				'uf' => 'PI',
				'cep2' => '2204303',
				'estado_cod' => 17,
				'cep' => '64690-000',
			),
			40 => 
			array (
				'id' => 5595,
				'nome' => 'Geminiano',
				'uf' => 'PI',
				'cep2' => '2204352',
				'estado_cod' => 17,
				'cep' => '64613-000',
			),
			41 => 
			array (
				'id' => 5596,
				'nome' => 'Gilbués',
				'uf' => 'PI',
				'cep2' => '2204402',
				'estado_cod' => 17,
				'cep' => '64930-000',
			),
			42 => 
			array (
				'id' => 5597,
				'nome' => 'Guadalupe',
				'uf' => 'PI',
				'cep2' => '2204501',
				'estado_cod' => 17,
				'cep' => '64840-000',
			),
			43 => 
			array (
				'id' => 5598,
				'nome' => 'Guaribas',
				'uf' => 'PI',
				'cep2' => '2204550',
				'estado_cod' => 17,
				'cep' => '64798-000',
			),
			44 => 
			array (
				'id' => 5599,
				'nome' => 'Hugo Napoleão',
				'uf' => 'PI',
				'cep2' => '2204600',
				'estado_cod' => 17,
				'cep' => '64470-000',
			),
			45 => 
			array (
				'id' => 5600,
				'nome' => 'Ilha Grande',
				'uf' => 'PI',
				'cep2' => '2204659',
				'estado_cod' => 17,
				'cep' => '64224-000',
			),
			46 => 
			array (
				'id' => 5601,
				'nome' => 'Inhuma',
				'uf' => 'PI',
				'cep2' => '2204709',
				'estado_cod' => 17,
				'cep' => '64535-000',
			),
			47 => 
			array (
				'id' => 5602,
				'nome' => 'Ipiranga do Piauí',
				'uf' => 'PI',
				'cep2' => '2204808',
				'estado_cod' => 17,
				'cep' => '64540-000',
			),
			48 => 
			array (
				'id' => 5603,
				'nome' => 'Isaías Coelho',
				'uf' => 'PI',
				'cep2' => '2204907',
				'estado_cod' => 17,
				'cep' => '64570-000',
			),
			49 => 
			array (
				'id' => 5604,
				'nome' => 'Itainópolis',
				'uf' => 'PI',
				'cep2' => '2205003',
				'estado_cod' => 17,
				'cep' => '64565-000',
			),
			50 => 
			array (
				'id' => 5605,
				'nome' => 'Itaueira',
				'uf' => 'PI',
				'cep2' => '2205102',
				'estado_cod' => 17,
				'cep' => '64820-000',
			),
			51 => 
			array (
				'id' => 5606,
				'nome' => 'Jacobina do Piauí',
				'uf' => 'PI',
				'cep2' => '2205151',
				'estado_cod' => 17,
				'cep' => '64755-000',
			),
			52 => 
			array (
				'id' => 5607,
				'nome' => 'Jaicós',
				'uf' => 'PI',
				'cep2' => '2205201',
				'estado_cod' => 17,
				'cep' => '64575-000',
			),
			53 => 
			array (
				'id' => 5608,
				'nome' => 'Jardim do Mulato',
				'uf' => 'PI',
				'cep2' => '2205250',
				'estado_cod' => 17,
				'cep' => '64495-000',
			),
			54 => 
			array (
				'id' => 5609,
				'nome' => 'Jatobá do Piauí',
				'uf' => 'PI',
				'cep2' => '2205276',
				'estado_cod' => 17,
				'cep' => '64275-000',
			),
			55 => 
			array (
				'id' => 5610,
				'nome' => 'Jerumenha',
				'uf' => 'PI',
				'cep2' => '2205300',
				'estado_cod' => 17,
				'cep' => '64830-000',
			),
			56 => 
			array (
				'id' => 5611,
				'nome' => 'João Costa',
				'uf' => 'PI',
				'cep2' => '2205359',
				'estado_cod' => 17,
				'cep' => '64765-000',
			),
			57 => 
			array (
				'id' => 5612,
				'nome' => 'Joaquim Pires',
				'uf' => 'PI',
				'cep2' => '2205409',
				'estado_cod' => 17,
				'cep' => '64170-000',
			),
			58 => 
			array (
				'id' => 5613,
				'nome' => 'Joca Marques',
				'uf' => 'PI',
				'cep2' => '2205458',
				'estado_cod' => 17,
				'cep' => '64165-000',
			),
			59 => 
			array (
				'id' => 5614,
				'nome' => 'José de Freitas',
				'uf' => 'PI',
				'cep2' => '2205508',
				'estado_cod' => 17,
				'cep' => '64110-000',
			),
			60 => 
			array (
				'id' => 5615,
				'nome' => 'Juazeiro do Piauí',
				'uf' => 'PI',
				'cep2' => '2205516',
				'estado_cod' => 17,
				'cep' => '64343-000',
			),
			61 => 
			array (
				'id' => 5616,
				'nome' => 'Júlio Borges',
				'uf' => 'PI',
				'cep2' => '2205524',
				'estado_cod' => 17,
				'cep' => '64963-000',
			),
			62 => 
			array (
				'id' => 5617,
				'nome' => 'Jurema',
				'uf' => 'PI',
				'cep2' => '2205532',
				'estado_cod' => 17,
				'cep' => '64782-000',
			),
			63 => 
			array (
				'id' => 5618,
				'nome' => 'Lagoa Alegre',
				'uf' => 'PI',
				'cep2' => '2205557',
				'estado_cod' => 17,
				'cep' => '64138-000',
			),
			64 => 
			array (
				'id' => 5619,
				'nome' => 'Lagoa de São Francisco',
				'uf' => 'PI',
				'cep2' => '2205573',
				'estado_cod' => 17,
				'cep' => '64258-000',
			),
			65 => 
			array (
				'id' => 5620,
				'nome' => 'Lagoa do Barro do Piauí',
				'uf' => 'PI',
				'cep2' => '2205565',
				'estado_cod' => 17,
				'cep' => '64768-000',
			),
			66 => 
			array (
				'id' => 5621,
				'nome' => 'Lagoa do Piauí',
				'uf' => 'PI',
				'cep2' => '2205581',
				'estado_cod' => 17,
				'cep' => '64388-000',
			),
			67 => 
			array (
				'id' => 5622,
				'nome' => 'Lagoa do Sítio',
				'uf' => 'PI',
				'cep2' => '2205599',
				'estado_cod' => 17,
				'cep' => '64308-000',
			),
			68 => 
			array (
				'id' => 5623,
				'nome' => 'Lagoinha do Piauí',
				'uf' => 'PI',
				'cep2' => '2205540',
				'estado_cod' => 17,
				'cep' => '64465-000',
			),
			69 => 
			array (
				'id' => 5624,
				'nome' => 'Landri Sales',
				'uf' => 'PI',
				'cep2' => '2205607',
				'estado_cod' => 17,
				'cep' => '64850-000',
			),
			70 => 
			array (
				'id' => 5625,
				'nome' => 'Luís Correia',
				'uf' => 'PI',
				'cep2' => '2205706',
				'estado_cod' => 17,
				'cep' => '64220-000',
			),
			71 => 
			array (
				'id' => 5626,
				'nome' => 'Luzilândia',
				'uf' => 'PI',
				'cep2' => '2205805',
				'estado_cod' => 17,
				'cep' => '64160-000',
			),
			72 => 
			array (
				'id' => 5627,
				'nome' => 'Madeiro',
				'uf' => 'PI',
				'cep2' => '2205854',
				'estado_cod' => 17,
				'cep' => '64168-000',
			),
			73 => 
			array (
				'id' => 5628,
				'nome' => 'Manoel Emídio',
				'uf' => 'PI',
				'cep2' => '2205904',
				'estado_cod' => 17,
				'cep' => '64875-000',
			),
			74 => 
			array (
				'id' => 5629,
				'nome' => 'Marcolândia',
				'uf' => 'PI',
				'cep2' => '2205953',
				'estado_cod' => 17,
				'cep' => '64685-000',
			),
			75 => 
			array (
				'id' => 5630,
				'nome' => 'Marcos Parente',
				'uf' => 'PI',
				'cep2' => '2206001',
				'estado_cod' => 17,
				'cep' => '64845-000',
			),
			76 => 
			array (
				'id' => 5631,
				'nome' => 'Massapê do Piauí',
				'uf' => 'PI',
				'cep2' => '2206050',
				'estado_cod' => 17,
				'cep' => '64573-000',
			),
			77 => 
			array (
				'id' => 5632,
				'nome' => 'Matias Olímpio',
				'uf' => 'PI',
				'cep2' => '2206100',
				'estado_cod' => 17,
				'cep' => '64150-000',
			),
			78 => 
			array (
				'id' => 5633,
				'nome' => 'Miguel Alves',
				'uf' => 'PI',
				'cep2' => '2206209',
				'estado_cod' => 17,
				'cep' => '64130-000',
			),
			79 => 
			array (
				'id' => 5634,
				'nome' => 'Miguel Leão',
				'uf' => 'PI',
				'cep2' => '2206308',
				'estado_cod' => 17,
				'cep' => '64445-000',
			),
			80 => 
			array (
				'id' => 5635,
				'nome' => 'Milton Brandão',
				'uf' => 'PI',
				'cep2' => '2206357',
				'estado_cod' => 17,
				'cep' => '64253-000',
			),
			81 => 
			array (
				'id' => 5636,
				'nome' => 'Monsenhor Gil',
				'uf' => 'PI',
				'cep2' => '2206407',
				'estado_cod' => 17,
				'cep' => '64450-000',
			),
			82 => 
			array (
				'id' => 5637,
				'nome' => 'Monsenhor Hipólito',
				'uf' => 'PI',
				'cep2' => '2206506',
				'estado_cod' => 17,
				'cep' => '64650-000',
			),
			83 => 
			array (
				'id' => 5638,
				'nome' => 'Monte Alegre do Piauí',
				'uf' => 'PI',
				'cep2' => '2206605',
				'estado_cod' => 17,
				'cep' => '64940-000',
			),
			84 => 
			array (
				'id' => 5639,
				'nome' => 'Morro Cabeça no Tempo',
				'uf' => 'PI',
				'cep2' => '2206654',
				'estado_cod' => 17,
				'cep' => '64968-000',
			),
			85 => 
			array (
				'id' => 5640,
				'nome' => 'Morro do Chapéu do Piauí',
				'uf' => 'PI',
				'cep2' => '2206670',
				'estado_cod' => 17,
				'cep' => '64178-000',
			),
			86 => 
			array (
				'id' => 5641,
				'nome' => 'Murici dos Portelas',
				'uf' => 'PI',
				'cep2' => '2206696',
				'estado_cod' => 17,
				'cep' => '64175-000',
			),
			87 => 
			array (
				'id' => 5642,
				'nome' => 'Nazaré do Piauí',
				'uf' => 'PI',
				'cep2' => '2206704',
				'estado_cod' => 17,
				'cep' => '64825-000',
			),
			88 => 
			array (
				'id' => 5643,
				'nome' => 'Nossa Senhora de Nazaré',
				'uf' => 'PI',
				'cep2' => '2206753',
				'estado_cod' => 17,
				'cep' => '64288-000',
			),
			89 => 
			array (
				'id' => 5644,
				'nome' => 'Nossa Senhora dos Remédios',
				'uf' => 'PI',
				'cep2' => '2206803',
				'estado_cod' => 17,
				'cep' => '64140-000',
			),
			90 => 
			array (
				'id' => 5645,
				'nome' => 'Nova Santa Rita',
				'uf' => 'PI',
				'cep2' => '2207959',
				'estado_cod' => 17,
				'cep' => '64764-000',
			),
			91 => 
			array (
				'id' => 5647,
				'nome' => 'Novo Oriente do Piauí',
				'uf' => 'PI',
				'cep2' => '2206902',
				'estado_cod' => 17,
				'cep' => '64530-000',
			),
			92 => 
			array (
				'id' => 5648,
				'nome' => 'Novo Santo Antônio',
				'uf' => 'PI',
				'cep2' => '2206951',
				'estado_cod' => 17,
				'cep' => '64365-000',
			),
			93 => 
			array (
				'id' => 5649,
				'nome' => 'Oeiras',
				'uf' => 'PI',
				'cep2' => '2207009',
				'estado_cod' => 17,
				'cep' => '64500-000',
			),
			94 => 
			array (
				'id' => 5650,
				'nome' => 'Olho D\'Água do Piauí',
				'uf' => 'PI',
				'cep2' => '2207108',
				'estado_cod' => 17,
				'cep' => '64468-000',
			),
			95 => 
			array (
				'id' => 5651,
				'nome' => 'Padre Marcos',
				'uf' => 'PI',
				'cep2' => '2207207',
				'estado_cod' => 17,
				'cep' => '64680-000',
			),
			96 => 
			array (
				'id' => 5652,
				'nome' => 'Paes Landim',
				'uf' => 'PI',
				'cep2' => '2207306',
				'estado_cod' => 17,
				'cep' => '64710-000',
			),
			97 => 
			array (
				'id' => 5653,
				'nome' => 'Pajeú do Piauí',
				'uf' => 'PI',
				'cep2' => '2207355',
				'estado_cod' => 17,
				'cep' => '64898-000',
			),
			98 => 
			array (
				'id' => 5654,
				'nome' => 'Palmeira do Piauí',
				'uf' => 'PI',
				'cep2' => '2207405',
				'estado_cod' => 17,
				'cep' => '64925-000',
			),
			99 => 
			array (
				'id' => 5655,
				'nome' => 'Palmeirais',
				'uf' => 'PI',
				'cep2' => '2207504',
				'estado_cod' => 17,
				'cep' => '64420-000',
			),
			100 => 
			array (
				'id' => 5656,
				'nome' => 'Paquetá',
				'uf' => 'PI',
				'cep2' => '2207553',
				'estado_cod' => 17,
				'cep' => '64618-000',
			),
			101 => 
			array (
				'id' => 5657,
				'nome' => 'Parnaguá',
				'uf' => 'PI',
				'cep2' => '2207603',
				'estado_cod' => 17,
				'cep' => '64970-000',
			),
			102 => 
			array (
				'id' => 5658,
				'nome' => 'Parnaíba',
				'uf' => 'PI',
				'cep2' => '2207702',
				'estado_cod' => 17,
				'cep' => 'LOC',
			),
			103 => 
			array (
				'id' => 5659,
				'nome' => 'Passagem Franca do Piauí',
				'uf' => 'PI',
				'cep2' => '2207751',
				'estado_cod' => 17,
				'cep' => '64395-000',
			),
			104 => 
			array (
				'id' => 5660,
				'nome' => 'Patos do Piauí',
				'uf' => 'PI',
				'cep2' => '2207777',
				'estado_cod' => 17,
				'cep' => '64580-000',
			),
			105 => 
			array (
				'id' => 5661,
				'nome' => 'Paulistana',
				'uf' => 'PI',
				'cep2' => '2207801',
				'estado_cod' => 17,
				'cep' => '64750-000',
			),
			106 => 
			array (
				'id' => 5662,
				'nome' => 'Pavussu',
				'uf' => 'PI',
				'cep2' => '2207850',
				'estado_cod' => 17,
				'cep' => '64838-000',
			),
			107 => 
			array (
				'id' => 5663,
				'nome' => 'Pedro II',
				'uf' => 'PI',
				'cep2' => '2207900',
				'estado_cod' => 17,
				'cep' => '64255-000',
			),
			108 => 
			array (
				'id' => 5664,
				'nome' => 'Pedro Laurentino',
				'uf' => 'PI',
				'cep2' => '2207934',
				'estado_cod' => 17,
				'cep' => '64728-000',
			),
			109 => 
			array (
				'id' => 5665,
				'nome' => 'Picos',
				'uf' => 'PI',
				'cep2' => '2208007',
				'estado_cod' => 17,
				'cep' => '64600-000',
			),
			110 => 
			array (
				'id' => 5666,
				'nome' => 'Pimenteiras',
				'uf' => 'PI',
				'cep2' => '2208106',
				'estado_cod' => 17,
				'cep' => '64320-000',
			),
			111 => 
			array (
				'id' => 5667,
				'nome' => 'Pio IX',
				'uf' => 'PI',
				'cep2' => '2208205',
				'estado_cod' => 17,
				'cep' => '64660-000',
			),
			112 => 
			array (
				'id' => 5668,
				'nome' => 'Piracuruca',
				'uf' => 'PI',
				'cep2' => '2208304',
				'estado_cod' => 17,
				'cep' => '64240-000',
			),
			113 => 
			array (
				'id' => 5669,
				'nome' => 'Piripiri',
				'uf' => 'PI',
				'cep2' => '2208403',
				'estado_cod' => 17,
				'cep' => '64260-000',
			),
			114 => 
			array (
				'id' => 5670,
				'nome' => 'Porto',
				'uf' => 'PI',
				'cep2' => '2208502',
				'estado_cod' => 17,
				'cep' => '64145-000',
			),
			115 => 
			array (
				'id' => 5671,
				'nome' => 'Porto Alegre do Piauí',
				'uf' => 'PI',
				'cep2' => '2208551',
				'estado_cod' => 17,
				'cep' => '64858-000',
			),
			116 => 
			array (
				'id' => 5672,
				'nome' => 'Prata do Piauí',
				'uf' => 'PI',
				'cep2' => '2208601',
				'estado_cod' => 17,
				'cep' => '64370-000',
			),
			117 => 
			array (
				'id' => 5673,
				'nome' => 'Queimada Nova',
				'uf' => 'PI',
				'cep2' => '2208650',
				'estado_cod' => 17,
				'cep' => '64758-000',
			),
			118 => 
			array (
				'id' => 5674,
				'nome' => 'Redenção do Gurguéia',
				'uf' => 'PI',
				'cep2' => '2208700',
				'estado_cod' => 17,
				'cep' => '64915-000',
			),
			119 => 
			array (
				'id' => 5675,
				'nome' => 'Regeneração',
				'uf' => 'PI',
				'cep2' => '2208809',
				'estado_cod' => 17,
				'cep' => '64490-000',
			),
			120 => 
			array (
				'id' => 5676,
				'nome' => 'Riacho Frio',
				'uf' => 'PI',
				'cep2' => '2208858',
				'estado_cod' => 17,
				'cep' => '64975-000',
			),
			121 => 
			array (
				'id' => 5677,
				'nome' => 'Ribeira do Piauí',
				'uf' => 'PI',
				'cep2' => '2208874',
				'estado_cod' => 17,
				'cep' => '64725-000',
			),
			122 => 
			array (
				'id' => 5678,
				'nome' => 'Ribeiro Gonçalves',
				'uf' => 'PI',
				'cep2' => '2208908',
				'estado_cod' => 17,
				'cep' => '64865-000',
			),
			123 => 
			array (
				'id' => 5679,
				'nome' => 'Rio Grande do Piauí',
				'uf' => 'PI',
				'cep2' => '2209005',
				'estado_cod' => 17,
				'cep' => '64835-000',
			),
			124 => 
			array (
				'id' => 5680,
				'nome' => 'Santa Cruz do Piauí',
				'uf' => 'PI',
				'cep2' => '2209104',
				'estado_cod' => 17,
				'cep' => '64545-000',
			),
			125 => 
			array (
				'id' => 5681,
				'nome' => 'Santa Cruz dos Milagres',
				'uf' => 'PI',
				'cep2' => '2209153',
				'estado_cod' => 17,
				'cep' => '64315-000',
			),
			126 => 
			array (
				'id' => 5682,
				'nome' => 'Santa Filomena',
				'uf' => 'PI',
				'cep2' => '2209203',
				'estado_cod' => 17,
				'cep' => '64945-000',
			),
			127 => 
			array (
				'id' => 5683,
				'nome' => 'Santa Luz',
				'uf' => 'PI',
				'cep2' => '2209302',
				'estado_cod' => 17,
				'cep' => '64910-000',
			),
			128 => 
			array (
				'id' => 5684,
				'nome' => 'Santa Rosa do Piauí',
				'uf' => 'PI',
				'cep2' => '2209377',
				'estado_cod' => 17,
				'cep' => '64518-000',
			),
			129 => 
			array (
				'id' => 5685,
				'nome' => 'Santana do Piauí',
				'uf' => 'PI',
				'cep2' => '2209351',
				'estado_cod' => 17,
				'cep' => '64615-000',
			),
			130 => 
			array (
				'id' => 5686,
				'nome' => 'Santo Antônio de Lisboa',
				'uf' => 'PI',
				'cep2' => '2209401',
				'estado_cod' => 17,
				'cep' => '64640-000',
			),
			131 => 
			array (
				'id' => 5687,
				'nome' => 'Santo Antônio dos Milagres',
				'uf' => 'PI',
				'cep2' => '2209450',
				'estado_cod' => 17,
				'cep' => '64438-000',
			),
			132 => 
			array (
				'id' => 5688,
				'nome' => 'Santo Inácio do Piauí',
				'uf' => 'PI',
				'cep2' => '2209500',
				'estado_cod' => 17,
				'cep' => '64560-000',
			),
			133 => 
			array (
				'id' => 5689,
				'nome' => 'São Braz do Piauí',
				'uf' => 'PI',
				'cep2' => '2209559',
				'estado_cod' => 17,
				'cep' => '64783-000',
			),
			134 => 
			array (
				'id' => 5690,
				'nome' => 'São Félix do Piauí',
				'uf' => 'PI',
				'cep2' => '2209609',
				'estado_cod' => 17,
				'cep' => '64375-000',
			),
			135 => 
			array (
				'id' => 5691,
				'nome' => 'São Francisco de Assis do Piauí',
				'uf' => 'PI',
				'cep2' => '',
				'estado_cod' => 17,
				'cep' => '64745-000',
			),
			136 => 
			array (
				'id' => 5692,
				'nome' => 'São Francisco do Piauí',
				'uf' => 'PI',
				'cep2' => '2209708',
				'estado_cod' => 17,
				'cep' => '64550-000',
			),
			137 => 
			array (
				'id' => 5693,
				'nome' => 'São Gonçalo do Gurguéia',
				'uf' => 'PI',
				'cep2' => '2209757',
				'estado_cod' => 17,
				'cep' => '64993-000',
			),
			138 => 
			array (
				'id' => 5694,
				'nome' => 'São Gonçalo do Piauí',
				'uf' => 'PI',
				'cep2' => '2209807',
				'estado_cod' => 17,
				'cep' => '64435-000',
			),
			139 => 
			array (
				'id' => 5695,
				'nome' => 'São João da Canabrava',
				'uf' => 'PI',
				'cep2' => '2209856',
				'estado_cod' => 17,
				'cep' => '64635-000',
			),
			140 => 
			array (
				'id' => 5696,
				'nome' => 'São João da Fronteira',
				'uf' => 'PI',
				'cep2' => '2209872',
				'estado_cod' => 17,
				'cep' => '64243-000',
			),
			141 => 
			array (
				'id' => 5697,
				'nome' => 'São João da Serra',
				'uf' => 'PI',
				'cep2' => '2209906',
				'estado_cod' => 17,
				'cep' => '64350-000',
			),
			142 => 
			array (
				'id' => 5698,
				'nome' => 'São João da Varjota',
				'uf' => 'PI',
				'cep2' => '2209955',
				'estado_cod' => 17,
				'cep' => '64510-000',
			),
			143 => 
			array (
				'id' => 5699,
				'nome' => 'São João do Arraial',
				'uf' => 'PI',
				'cep2' => '2209971',
				'estado_cod' => 17,
				'cep' => '64155-000',
			),
			144 => 
			array (
				'id' => 5700,
				'nome' => 'São João do Piauí',
				'uf' => 'PI',
				'cep2' => '2210003',
				'estado_cod' => 17,
				'cep' => '64760-000',
			),
			145 => 
			array (
				'id' => 5701,
				'nome' => 'São José do Divino',
				'uf' => 'PI',
				'cep2' => '2210052',
				'estado_cod' => 17,
				'cep' => '64245-000',
			),
			146 => 
			array (
				'id' => 5702,
				'nome' => 'São José do Peixe',
				'uf' => 'PI',
				'cep2' => '2210102',
				'estado_cod' => 17,
				'cep' => '64555-000',
			),
			147 => 
			array (
				'id' => 5703,
				'nome' => 'São José do Piauí',
				'uf' => 'PI',
				'cep2' => '2210201',
				'estado_cod' => 17,
				'cep' => '64625-000',
			),
			148 => 
			array (
				'id' => 5704,
				'nome' => 'São Julião',
				'uf' => 'PI',
				'cep2' => '2210300',
				'estado_cod' => 17,
				'cep' => '64670-000',
			),
			149 => 
			array (
				'id' => 5705,
				'nome' => 'São Lourenço do Piauí',
				'uf' => 'PI',
				'cep2' => '2210359',
				'estado_cod' => 17,
				'cep' => '64778-000',
			),
			150 => 
			array (
				'id' => 5706,
				'nome' => 'São Luís do Piauí',
				'uf' => 'PI',
				'cep2' => '2210375',
				'estado_cod' => 17,
				'cep' => '64638-000',
			),
			151 => 
			array (
				'id' => 5707,
				'nome' => 'São Miguel da Baixa Grande',
				'uf' => 'PI',
				'cep2' => '2210383',
				'estado_cod' => 17,
				'cep' => '64378-000',
			),
			152 => 
			array (
				'id' => 5708,
				'nome' => 'São Miguel do Fidalgo',
				'uf' => 'PI',
				'cep2' => '2210391',
				'estado_cod' => 17,
				'cep' => '64558-000',
			),
			153 => 
			array (
				'id' => 5709,
				'nome' => 'São Miguel do Tapuio',
				'uf' => 'PI',
				'cep2' => '2210409',
				'estado_cod' => 17,
				'cep' => '64330-000',
			),
			154 => 
			array (
				'id' => 5710,
				'nome' => 'São Pedro do Piauí',
				'uf' => 'PI',
				'cep2' => '2210508',
				'estado_cod' => 17,
				'cep' => '64430-000',
			),
			155 => 
			array (
				'id' => 5711,
				'nome' => 'São Raimundo Nonato',
				'uf' => 'PI',
				'cep2' => '2210607',
				'estado_cod' => 17,
				'cep' => '64770-000',
			),
			156 => 
			array (
				'id' => 5712,
				'nome' => 'Sebastião Barros',
				'uf' => 'PI',
				'cep2' => '2210623',
				'estado_cod' => 17,
				'cep' => '64985-000',
			),
			157 => 
			array (
				'id' => 5713,
				'nome' => 'Sebastião Leal',
				'uf' => 'PI',
				'cep2' => '2210631',
				'estado_cod' => 17,
				'cep' => '64873-000',
			),
			158 => 
			array (
				'id' => 5714,
				'nome' => 'Sigefredo Pacheco',
				'uf' => 'PI',
				'cep2' => '2210656',
				'estado_cod' => 17,
				'cep' => '64285-000',
			),
			159 => 
			array (
				'id' => 5715,
				'nome' => 'Simões',
				'uf' => 'PI',
				'cep2' => '2210706',
				'estado_cod' => 17,
				'cep' => '64585-000',
			),
			160 => 
			array (
				'id' => 5716,
				'nome' => 'Simplício Mendes',
				'uf' => 'PI',
				'cep2' => '2210805',
				'estado_cod' => 17,
				'cep' => '64700-000',
			),
			161 => 
			array (
				'id' => 5717,
				'nome' => 'Socorro do Piauí',
				'uf' => 'PI',
				'cep2' => '2210904',
				'estado_cod' => 17,
				'cep' => '64720-000',
			),
			162 => 
			array (
				'id' => 5718,
				'nome' => 'Sussuapara',
				'uf' => 'PI',
				'cep2' => '2210938',
				'estado_cod' => 17,
				'cep' => '64610-000',
			),
			163 => 
			array (
				'id' => 5719,
				'nome' => 'Tamboril do Piauí',
				'uf' => 'PI',
				'cep2' => '2210953',
				'estado_cod' => 17,
				'cep' => '64893-000',
			),
			164 => 
			array (
				'id' => 5720,
				'nome' => 'Tanque do Piauí',
				'uf' => 'PI',
				'cep2' => '2210979',
				'estado_cod' => 17,
				'cep' => '64512-000',
			),
			165 => 
			array (
				'id' => 5721,
				'nome' => 'Teresina',
				'uf' => 'PI',
				'cep2' => '2211001',
				'estado_cod' => 17,
				'cep' => 'LOC',
			),
			166 => 
			array (
				'id' => 5722,
				'nome' => 'União',
				'uf' => 'PI',
				'cep2' => '2211100',
				'estado_cod' => 17,
				'cep' => '64120-000',
			),
			167 => 
			array (
				'id' => 5723,
				'nome' => 'Uruçuí',
				'uf' => 'PI',
				'cep2' => '2211209',
				'estado_cod' => 17,
				'cep' => '64860-000',
			),
			168 => 
			array (
				'id' => 5724,
				'nome' => 'Valença do Piauí',
				'uf' => 'PI',
				'cep2' => '2211308',
				'estado_cod' => 17,
				'cep' => '64300-000',
			),
			169 => 
			array (
				'id' => 5725,
				'nome' => 'Várzea Branca',
				'uf' => 'PI',
				'cep2' => '2211357',
				'estado_cod' => 17,
				'cep' => '64773-000',
			),
			170 => 
			array (
				'id' => 5726,
				'nome' => 'Várzea Grande',
				'uf' => 'PI',
				'cep2' => '2211407',
				'estado_cod' => 17,
				'cep' => '64525-000',
			),
			171 => 
			array (
				'id' => 5727,
				'nome' => 'Vera Mendes',
				'uf' => 'PI',
				'cep2' => '2211506',
				'estado_cod' => 17,
				'cep' => '64568-000',
			),
			172 => 
			array (
				'id' => 5728,
				'nome' => 'Vila Nova do Piauí',
				'uf' => 'PI',
				'cep2' => '2211605',
				'estado_cod' => 17,
				'cep' => '64688-000',
			),
			173 => 
			array (
				'id' => 5729,
				'nome' => 'Wall Ferraz',
				'uf' => 'PI',
				'cep2' => '2211704',
				'estado_cod' => 17,
				'cep' => '64548-000',
			),
			174 => 
			array (
				'id' => 5731,
				'nome' => 'Abatiá',
				'uf' => 'PR',
				'cep2' => '4100103',
				'estado_cod' => 18,
				'cep' => '86460-000',
			),
			175 => 
			array (
				'id' => 5735,
				'nome' => 'Adrianópolis',
				'uf' => 'PR',
				'cep2' => '4100202',
				'estado_cod' => 18,
				'cep' => '83490-000',
			),
			176 => 
			array (
				'id' => 5743,
				'nome' => 'Agudos do Sul',
				'uf' => 'PR',
				'cep2' => '4100301',
				'estado_cod' => 18,
				'cep' => '83850-000',
			),
			177 => 
			array (
				'id' => 5746,
				'nome' => 'Almirante Tamandaré',
				'uf' => 'PR',
				'cep2' => '4100400',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			178 => 
			array (
				'id' => 5747,
				'nome' => 'Altamira do Paraná',
				'uf' => 'PR',
				'cep2' => '4100459',
				'estado_cod' => 18,
				'cep' => '85280-000',
			),
			179 => 
			array (
				'id' => 5755,
				'nome' => 'Alto Paraná',
				'uf' => 'PR',
				'cep2' => '4100608',
				'estado_cod' => 18,
				'cep' => '87750-000',
			),
			180 => 
			array (
				'id' => 5756,
				'nome' => 'Alto Piquiri',
				'uf' => 'PR',
				'cep2' => '4100707',
				'estado_cod' => 18,
				'cep' => '87580-000',
			),
			181 => 
			array (
				'id' => 5761,
				'nome' => 'Altônia',
				'uf' => 'PR',
				'cep2' => '4100509',
				'estado_cod' => 18,
				'cep' => '87550-000',
			),
			182 => 
			array (
				'id' => 5763,
				'nome' => 'Alvorada do Sul',
				'uf' => 'PR',
				'cep2' => '4100806',
				'estado_cod' => 18,
				'cep' => '86150-000',
			),
			183 => 
			array (
				'id' => 5764,
				'nome' => 'Amaporã',
				'uf' => 'PR',
				'cep2' => '4100905',
				'estado_cod' => 18,
				'cep' => '87850-000',
			),
			184 => 
			array (
				'id' => 5766,
				'nome' => 'Ampére',
				'uf' => 'PR',
				'cep2' => '4101002',
				'estado_cod' => 18,
				'cep' => '85640-000',
			),
			185 => 
			array (
				'id' => 5767,
				'nome' => 'Anahy',
				'uf' => 'PR',
				'cep2' => '4101051',
				'estado_cod' => 18,
				'cep' => '85425-000',
			),
			186 => 
			array (
				'id' => 5768,
				'nome' => 'Andirá',
				'uf' => 'PR',
				'cep2' => '4101101',
				'estado_cod' => 18,
				'cep' => '86380-000',
			),
			187 => 
			array (
				'id' => 5771,
				'nome' => 'Ângulo',
				'uf' => 'PR',
				'cep2' => '4101150',
				'estado_cod' => 18,
				'cep' => '86755-000',
			),
			188 => 
			array (
				'id' => 5774,
				'nome' => 'Antonina',
				'uf' => 'PR',
				'cep2' => '4101200',
				'estado_cod' => 18,
				'cep' => '83370-000',
			),
			189 => 
			array (
				'id' => 5776,
				'nome' => 'Antônio Olinto',
				'uf' => 'PR',
				'cep2' => '4101309',
				'estado_cod' => 18,
				'cep' => '83980-000',
			),
			190 => 
			array (
				'id' => 5780,
				'nome' => 'Apucarana',
				'uf' => 'PR',
				'cep2' => '4101408',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			191 => 
			array (
				'id' => 5783,
				'nome' => 'Arapongas',
				'uf' => 'PR',
				'cep2' => '4101507',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			192 => 
			array (
				'id' => 5784,
				'nome' => 'Arapoti',
				'uf' => 'PR',
				'cep2' => '4101606',
				'estado_cod' => 18,
				'cep' => '84990-000',
			),
			193 => 
			array (
				'id' => 5785,
				'nome' => 'Arapuã',
				'uf' => 'PR',
				'cep2' => '4101655',
				'estado_cod' => 18,
				'cep' => '86884-000',
			),
			194 => 
			array (
				'id' => 5788,
				'nome' => 'Araruna',
				'uf' => 'PR',
				'cep2' => '4101705',
				'estado_cod' => 18,
				'cep' => '87260-000',
			),
			195 => 
			array (
				'id' => 5789,
				'nome' => 'Araucária',
				'uf' => 'PR',
				'cep2' => '4101804',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			196 => 
			array (
				'id' => 5793,
				'nome' => 'Ariranha do Ivaí',
				'uf' => 'PR',
				'cep2' => '4101853',
				'estado_cod' => 18,
				'cep' => '86880-000',
			),
			197 => 
			array (
				'id' => 5796,
				'nome' => 'Assaí',
				'uf' => 'PR',
				'cep2' => '4101903',
				'estado_cod' => 18,
				'cep' => '86220-000',
			),
			198 => 
			array (
				'id' => 5797,
				'nome' => 'Assis Chateaubriand',
				'uf' => 'PR',
				'cep2' => '4102000',
				'estado_cod' => 18,
				'cep' => '85935-000',
			),
			199 => 
			array (
				'id' => 5798,
				'nome' => 'Astorga',
				'uf' => 'PR',
				'cep2' => '4102109',
				'estado_cod' => 18,
				'cep' => '86730-000',
			),
			200 => 
			array (
				'id' => 5799,
				'nome' => 'Atalaia',
				'uf' => 'PR',
				'cep2' => '4102208',
				'estado_cod' => 18,
				'cep' => '87630-000',
			),
			201 => 
			array (
				'id' => 5804,
				'nome' => 'Balsa Nova',
				'uf' => 'PR',
				'cep2' => '4102307',
				'estado_cod' => 18,
				'cep' => '83650-000',
			),
			202 => 
			array (
				'id' => 5805,
				'nome' => 'Bandeirantes',
				'uf' => 'PR',
				'cep2' => '4102406',
				'estado_cod' => 18,
				'cep' => '86360-000',
			),
			203 => 
			array (
				'id' => 5809,
				'nome' => 'Barbosa Ferraz',
				'uf' => 'PR',
				'cep2' => '4102505',
				'estado_cod' => 18,
				'cep' => '86960-000',
			),
			204 => 
			array (
				'id' => 5812,
				'nome' => 'Barra do Jacaré',
				'uf' => 'PR',
				'cep2' => '4102703',
				'estado_cod' => 18,
				'cep' => '86385-000',
			),
			205 => 
			array (
				'id' => 5819,
				'nome' => 'Barracão',
				'uf' => 'PR',
				'cep2' => '4102604',
				'estado_cod' => 18,
				'cep' => '85700-000',
			),
			206 => 
			array (
				'id' => 5834,
				'nome' => 'Bela Vista da Caroba',
				'uf' => 'PR',
				'cep2' => '',
				'estado_cod' => 18,
				'cep' => '85745-000',
			),
			207 => 
			array (
				'id' => 5836,
				'nome' => 'Bela Vista do Paraíso',
				'uf' => 'PR',
				'cep2' => '4102802',
				'estado_cod' => 18,
				'cep' => '86130-000',
			),
			208 => 
			array (
				'id' => 5842,
				'nome' => 'Bituruna',
				'uf' => 'PR',
				'cep2' => '4102901',
				'estado_cod' => 18,
				'cep' => '84640-000',
			),
			209 => 
			array (
				'id' => 5843,
				'nome' => 'Boa Esperança',
				'uf' => 'PR',
				'cep2' => '4103008',
				'estado_cod' => 18,
				'cep' => '87390-000',
			),
			210 => 
			array (
				'id' => 5845,
				'nome' => 'Boa Esperança do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4103024',
				'estado_cod' => 18,
				'cep' => '85680-000',
			),
			211 => 
			array (
				'id' => 5846,
				'nome' => 'Boa Ventura de São Roque',
				'uf' => 'PR',
				'cep2' => '4103040',
				'estado_cod' => 18,
				'cep' => '85225-000',
			),
			212 => 
			array (
				'id' => 5852,
				'nome' => 'Boa Vista da Aparecida',
				'uf' => 'PR',
				'cep2' => '4103057',
				'estado_cod' => 18,
				'cep' => '85780-000',
			),
			213 => 
			array (
				'id' => 5854,
				'nome' => 'Bocaiúva do Sul',
				'uf' => 'PR',
				'cep2' => '4103107',
				'estado_cod' => 18,
				'cep' => '83450-000',
			),
			214 => 
			array (
				'id' => 5856,
				'nome' => 'Bom Jesus do Sul',
				'uf' => 'PR',
				'cep2' => '4103156',
				'estado_cod' => 18,
				'cep' => '85708-000',
			),
			215 => 
			array (
				'id' => 5862,
				'nome' => 'Bom Sucesso',
				'uf' => 'PR',
				'cep2' => '4103206',
				'estado_cod' => 18,
				'cep' => '86940-000',
			),
			216 => 
			array (
				'id' => 5864,
				'nome' => 'Bom Sucesso do Sul',
				'uf' => 'PR',
				'cep2' => '4103222',
				'estado_cod' => 18,
				'cep' => '85515-000',
			),
			217 => 
			array (
				'id' => 5868,
				'nome' => 'Borrazópolis',
				'uf' => 'PR',
				'cep2' => '4103305',
				'estado_cod' => 18,
				'cep' => '86925-000',
			),
			218 => 
			array (
				'id' => 5871,
				'nome' => 'Braganey',
				'uf' => 'PR',
				'cep2' => '4103354',
				'estado_cod' => 18,
				'cep' => '85430-000',
			),
			219 => 
			array (
				'id' => 5873,
				'nome' => 'Brasilândia do Sul',
				'uf' => 'PR',
				'cep2' => '4103370',
				'estado_cod' => 18,
				'cep' => '87595-000',
			),
			220 => 
			array (
				'id' => 5886,
				'nome' => 'Cafeara',
				'uf' => 'PR',
				'cep2' => '4103404',
				'estado_cod' => 18,
				'cep' => '86640-000',
			),
			221 => 
			array (
				'id' => 5888,
				'nome' => 'Cafelândia',
				'uf' => 'PR',
				'cep2' => '4103453',
				'estado_cod' => 18,
				'cep' => '85415-000',
			),
			222 => 
			array (
				'id' => 5889,
				'nome' => 'Cafezal do Sul',
				'uf' => 'PR',
				'cep2' => '4103479',
				'estado_cod' => 18,
				'cep' => '87565-000',
			),
			223 => 
			array (
				'id' => 5892,
				'nome' => 'Califórnia',
				'uf' => 'PR',
				'cep2' => '4103503',
				'estado_cod' => 18,
				'cep' => '86820-000',
			),
			224 => 
			array (
				'id' => 5894,
				'nome' => 'Cambará',
				'uf' => 'PR',
				'cep2' => '4103602',
				'estado_cod' => 18,
				'cep' => '86390-000',
			),
			225 => 
			array (
				'id' => 5895,
				'nome' => 'Cambé',
				'uf' => 'PR',
				'cep2' => '4103701',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			226 => 
			array (
				'id' => 5897,
				'nome' => 'Cambira',
				'uf' => 'PR',
				'cep2' => '4103800',
				'estado_cod' => 18,
				'cep' => '86890-000',
			),
			227 => 
			array (
				'id' => 5902,
				'nome' => 'Campina da Lagoa',
				'uf' => 'PR',
				'cep2' => '4103909',
				'estado_cod' => 18,
				'cep' => '87345-000',
			),
			228 => 
			array (
				'id' => 5904,
				'nome' => 'Campina do Simão',
				'uf' => 'PR',
				'cep2' => '4103958',
				'estado_cod' => 18,
				'cep' => '85148-000',
			),
			229 => 
			array (
				'id' => 5906,
				'nome' => 'Campina Grande do Sul',
				'uf' => 'PR',
				'cep2' => '4104006',
				'estado_cod' => 18,
				'cep' => '83430-000',
			),
			230 => 
			array (
				'id' => 5908,
				'nome' => 'Campo Bonito',
				'uf' => 'PR',
				'cep2' => '4104055',
				'estado_cod' => 18,
				'cep' => '85450-000',
			),
			231 => 
			array (
				'id' => 5910,
				'nome' => 'Campo do Tenente',
				'uf' => 'PR',
				'cep2' => '4104105',
				'estado_cod' => 18,
				'cep' => '83870-000',
			),
			232 => 
			array (
				'id' => 5911,
				'nome' => 'Campo Largo',
				'uf' => 'PR',
				'cep2' => '4104204',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			233 => 
			array (
				'id' => 5913,
				'nome' => 'Campo Magro',
				'uf' => 'PR',
				'cep2' => '4104253',
				'estado_cod' => 18,
				'cep' => '83535-000',
			),
			234 => 
			array (
				'id' => 5914,
				'nome' => 'Campo Mourão',
				'uf' => 'PR',
				'cep2' => '4104303',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			235 => 
			array (
				'id' => 5915,
				'nome' => 'Cândido de Abreu',
				'uf' => 'PR',
				'cep2' => '4104402',
				'estado_cod' => 18,
				'cep' => '84470-000',
			),
			236 => 
			array (
				'id' => 5916,
				'nome' => 'Candói',
				'uf' => 'PR',
				'cep2' => '4104428',
				'estado_cod' => 18,
				'cep' => '85140-000',
			),
			237 => 
			array (
				'id' => 5918,
				'nome' => 'Cantagalo',
				'uf' => 'PR',
				'cep2' => '4104451',
				'estado_cod' => 18,
				'cep' => '85160-000',
			),
			238 => 
			array (
				'id' => 5920,
				'nome' => 'Capanema',
				'uf' => 'PR',
				'cep2' => '4104501',
				'estado_cod' => 18,
				'cep' => '85760-000',
			),
			239 => 
			array (
				'id' => 5927,
				'nome' => 'Capitão Leônidas Marques',
				'uf' => 'PR',
				'cep2' => '4104600',
				'estado_cod' => 18,
				'cep' => '85790-000',
			),
			240 => 
			array (
				'id' => 5933,
				'nome' => 'Carambeí',
				'uf' => 'PR',
				'cep2' => '4104659',
				'estado_cod' => 18,
				'cep' => '84145-000',
			),
			241 => 
			array (
				'id' => 5938,
				'nome' => 'Carlópolis',
				'uf' => 'PR',
				'cep2' => '4104709',
				'estado_cod' => 18,
				'cep' => '86420-000',
			),
			242 => 
			array (
				'id' => 5941,
				'nome' => 'Cascavel',
				'uf' => 'PR',
				'cep2' => '4104808',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			243 => 
			array (
				'id' => 5942,
				'nome' => 'Castro',
				'uf' => 'PR',
				'cep2' => '4104907',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			244 => 
			array (
				'id' => 5943,
				'nome' => 'Catanduvas',
				'uf' => 'PR',
				'cep2' => '4105003',
				'estado_cod' => 18,
				'cep' => '85470-000',
			),
			245 => 
			array (
				'id' => 5949,
				'nome' => 'Centenário do Sul',
				'uf' => 'PR',
				'cep2' => '4105102',
				'estado_cod' => 18,
				'cep' => '86630-000',
			),
			246 => 
			array (
				'id' => 5955,
				'nome' => 'Cerro Azul',
				'uf' => 'PR',
				'cep2' => '4105201',
				'estado_cod' => 18,
				'cep' => '83570-000',
			),
			247 => 
			array (
				'id' => 5957,
				'nome' => 'Céu Azul',
				'uf' => 'PR',
				'cep2' => '4105300',
				'estado_cod' => 18,
				'cep' => '85840-000',
			),
			248 => 
			array (
				'id' => 5958,
				'nome' => 'Chopinzinho',
				'uf' => 'PR',
				'cep2' => '4105409',
				'estado_cod' => 18,
				'cep' => '85560-000',
			),
			249 => 
			array (
				'id' => 5959,
				'nome' => 'Cianorte',
				'uf' => 'PR',
				'cep2' => '4105508',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			250 => 
			array (
				'id' => 5960,
				'nome' => 'Cidade Gaúcha',
				'uf' => 'PR',
				'cep2' => '4105607',
				'estado_cod' => 18,
				'cep' => '87820-000',
			),
			251 => 
			array (
				'id' => 5962,
				'nome' => 'Clevelândia',
				'uf' => 'PR',
				'cep2' => '4105706',
				'estado_cod' => 18,
				'cep' => '85530-000',
			),
			252 => 
			array (
				'id' => 5964,
				'nome' => 'Colombo',
				'uf' => 'PR',
				'cep2' => '4105805',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			253 => 
			array (
				'id' => 5983,
				'nome' => 'Colorado',
				'uf' => 'PR',
				'cep2' => '4105904',
				'estado_cod' => 18,
				'cep' => '86690-000',
			),
			254 => 
			array (
				'id' => 5990,
				'nome' => 'Congonhinhas',
				'uf' => 'PR',
				'cep2' => '4106001',
				'estado_cod' => 18,
				'cep' => '86320-000',
			),
			255 => 
			array (
				'id' => 5991,
				'nome' => 'Conselheiro Mairinck',
				'uf' => 'PR',
				'cep2' => '4106100',
				'estado_cod' => 18,
				'cep' => '86480-000',
			),
			256 => 
			array (
				'id' => 5993,
				'nome' => 'Contenda',
				'uf' => 'PR',
				'cep2' => '4106209',
				'estado_cod' => 18,
				'cep' => '83730-000',
			),
			257 => 
			array (
				'id' => 5995,
				'nome' => 'Corbélia',
				'uf' => 'PR',
				'cep2' => '4106308',
				'estado_cod' => 18,
				'cep' => '85420-000',
			),
			258 => 
			array (
				'id' => 5996,
				'nome' => 'Cornélio Procópio',
				'uf' => 'PR',
				'cep2' => '4106407',
				'estado_cod' => 18,
				'cep' => '86300-000',
			),
			259 => 
			array (
				'id' => 5997,
				'nome' => 'Coronel Domingos Soares',
				'uf' => 'PR',
				'cep2' => '4106456',
				'estado_cod' => 18,
				'cep' => '85557-000',
			),
			260 => 
			array (
				'id' => 5999,
				'nome' => 'Coronel Vivida',
				'uf' => 'PR',
				'cep2' => '4106506',
				'estado_cod' => 18,
				'cep' => '85550-000',
			),
			261 => 
			array (
				'id' => 6001,
				'nome' => 'Corumbataí do Sul',
				'uf' => 'PR',
				'cep2' => '4106555',
				'estado_cod' => 18,
				'cep' => '86970-000',
			),
			262 => 
			array (
				'id' => 6008,
				'nome' => 'Cruz Machado',
				'uf' => 'PR',
				'cep2' => '4106803',
				'estado_cod' => 18,
				'cep' => '84620-000',
			),
			263 => 
			array (
				'id' => 6009,
				'nome' => 'Cruzeiro do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4106571',
				'estado_cod' => 18,
				'cep' => '85598-000',
			),
			264 => 
			array (
				'id' => 6011,
				'nome' => 'Cruzeiro do Oeste',
				'uf' => 'PR',
				'cep2' => '4106605',
				'estado_cod' => 18,
				'cep' => '87400-000',
			),
			265 => 
			array (
				'id' => 6012,
				'nome' => 'Cruzeiro do Sul',
				'uf' => 'PR',
				'cep2' => '4106704',
				'estado_cod' => 18,
				'cep' => '87650-000',
			),
			266 => 
			array (
				'id' => 6013,
				'nome' => 'Cruzmaltina',
				'uf' => 'PR',
				'cep2' => '4106852',
				'estado_cod' => 18,
				'cep' => '86855-000',
			),
			267 => 
			array (
				'id' => 6015,
				'nome' => 'Curitiba',
				'uf' => 'PR',
				'cep2' => '4106902',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			268 => 
			array (
				'id' => 6016,
				'nome' => 'Curiúva',
				'uf' => 'PR',
				'cep2' => '4107009',
				'estado_cod' => 18,
				'cep' => '84280-000',
			),
			269 => 
			array (
				'id' => 6023,
				'nome' => 'Diamante d\'Oeste',
				'uf' => 'PR',
				'cep2' => '4107157',
				'estado_cod' => 18,
				'cep' => '85896-000',
			),
			270 => 
			array (
				'id' => 6024,
				'nome' => 'Diamante do Norte',
				'uf' => 'PR',
				'cep2' => '4107108',
				'estado_cod' => 18,
				'cep' => '87990-000',
			),
			271 => 
			array (
				'id' => 6025,
				'nome' => 'Diamante do Sul',
				'uf' => 'PR',
				'cep2' => '4107124',
				'estado_cod' => 18,
				'cep' => '85408-000',
			),
			272 => 
			array (
				'id' => 6030,
				'nome' => 'Dois Vizinhos',
				'uf' => 'PR',
				'cep2' => '4107207',
				'estado_cod' => 18,
				'cep' => '85660-000',
			),
			273 => 
			array (
				'id' => 6033,
				'nome' => 'Douradina',
				'uf' => 'PR',
				'cep2' => '4107256',
				'estado_cod' => 18,
				'cep' => '87485-000',
			),
			274 => 
			array (
				'id' => 6035,
				'nome' => 'Doutor Camargo',
				'uf' => 'PR',
				'cep2' => '4107306',
				'estado_cod' => 18,
				'cep' => '87155-000',
			),
			275 => 
			array (
				'id' => 6038,
				'nome' => 'Doutor Ulysses',
				'uf' => 'PR',
				'cep2' => '4128633',
				'estado_cod' => 18,
				'cep' => '83590-000',
			),
			276 => 
			array (
				'id' => 6046,
				'nome' => 'Enéas Marques',
				'uf' => 'PR',
				'cep2' => '4107405',
				'estado_cod' => 18,
				'cep' => '85630-000',
			),
			277 => 
			array (
				'id' => 6047,
				'nome' => 'Engenheiro Beltrão',
				'uf' => 'PR',
				'cep2' => '4107504',
				'estado_cod' => 18,
				'cep' => '87270-000',
			),
			278 => 
			array (
				'id' => 6049,
				'nome' => 'Entre Rios do Oeste',
				'uf' => 'PR',
				'cep2' => '4107538',
				'estado_cod' => 18,
				'cep' => '85988-000',
			),
			279 => 
			array (
				'id' => 6051,
				'nome' => 'Esperança Nova',
				'uf' => 'PR',
				'cep2' => '4107520',
				'estado_cod' => 18,
				'cep' => '87545-000',
			),
			280 => 
			array (
				'id' => 6052,
				'nome' => 'Espigão Alto do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4107546',
				'estado_cod' => 18,
				'cep' => '85465-000',
			),
			281 => 
			array (
				'id' => 6059,
				'nome' => 'Farol',
				'uf' => 'PR',
				'cep2' => '4107553',
				'estado_cod' => 18,
				'cep' => '87325-000',
			),
			282 => 
			array (
				'id' => 6061,
				'nome' => 'Faxinal',
				'uf' => 'PR',
				'cep2' => '4107603',
				'estado_cod' => 18,
				'cep' => '86840-000',
			),
			283 => 
			array (
				'id' => 6068,
				'nome' => 'Fazenda Rio Grande',
				'uf' => 'PR',
				'cep2' => '4107652',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			284 => 
			array (
				'id' => 6072,
				'nome' => 'Fênix',
				'uf' => 'PR',
				'cep2' => '4107702',
				'estado_cod' => 18,
				'cep' => '86950-000',
			),
			285 => 
			array (
				'id' => 6073,
				'nome' => 'Fernandes Pinheiro',
				'uf' => 'PR',
				'cep2' => '4107736',
				'estado_cod' => 18,
				'cep' => '84535-000',
			),
			286 => 
			array (
				'id' => 6077,
				'nome' => 'Figueira',
				'uf' => 'PR',
				'cep2' => '4107751',
				'estado_cod' => 18,
				'cep' => '84285-000',
			),
			287 => 
			array (
				'id' => 6081,
				'nome' => 'Flor da Serra do Sul',
				'uf' => 'PR',
				'cep2' => '4107850',
				'estado_cod' => 18,
				'cep' => '85618-000',
			),
			288 => 
			array (
				'id' => 6082,
				'nome' => 'Floraí',
				'uf' => 'PR',
				'cep2' => '4107801',
				'estado_cod' => 18,
				'cep' => '87185-000',
			),
			289 => 
			array (
				'id' => 6083,
				'nome' => 'Floresta',
				'uf' => 'PR',
				'cep2' => '4107900',
				'estado_cod' => 18,
				'cep' => '87120-000',
			),
			290 => 
			array (
				'id' => 6084,
				'nome' => 'Florestópolis',
				'uf' => 'PR',
				'cep2' => '4108007',
				'estado_cod' => 18,
				'cep' => '86165-000',
			),
			291 => 
			array (
				'id' => 6086,
				'nome' => 'Flórida',
				'uf' => 'PR',
				'cep2' => '4108106',
				'estado_cod' => 18,
				'cep' => '86780-000',
			),
			292 => 
			array (
				'id' => 6090,
				'nome' => 'Formosa do Oeste',
				'uf' => 'PR',
				'cep2' => '4108205',
				'estado_cod' => 18,
				'cep' => '85830-000',
			),
			293 => 
			array (
				'id' => 6091,
				'nome' => 'Foz do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4108304',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			294 => 
			array (
				'id' => 6092,
				'nome' => 'Foz do Jordão',
				'uf' => 'PR',
				'cep2' => '4108452',
				'estado_cod' => 18,
				'cep' => '85145-000',
			),
			295 => 
			array (
				'id' => 6093,
				'nome' => 'Francisco Alves',
				'uf' => 'PR',
				'cep2' => '4108320',
				'estado_cod' => 18,
				'cep' => '87570-000',
			),
			296 => 
			array (
				'id' => 6094,
				'nome' => 'Francisco Beltrão',
				'uf' => 'PR',
				'cep2' => '4108403',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			297 => 
			array (
				'id' => 6104,
				'nome' => 'General Carneiro',
				'uf' => 'PR',
				'cep2' => '4108502',
				'estado_cod' => 18,
				'cep' => '84660-000',
			),
			298 => 
			array (
				'id' => 6107,
				'nome' => 'Godoy Moreira',
				'uf' => 'PR',
				'cep2' => '4108551',
				'estado_cod' => 18,
				'cep' => '86938-000',
			),
			299 => 
			array (
				'id' => 6108,
				'nome' => 'Goioerê',
				'uf' => 'PR',
				'cep2' => '',
				'estado_cod' => 18,
				'cep' => '87360-000',
			),
			300 => 
			array (
				'id' => 6109,
				'nome' => 'Goioxim',
				'uf' => 'PR',
				'cep2' => '4108650',
				'estado_cod' => 18,
				'cep' => '85162-000',
			),
			301 => 
			array (
				'id' => 6113,
				'nome' => 'Grandes Rios',
				'uf' => 'PR',
				'cep2' => '4108700',
				'estado_cod' => 18,
				'cep' => '86845-000',
			),
			302 => 
			array (
				'id' => 6115,
				'nome' => 'Guaíra',
				'uf' => 'PR',
				'cep2' => '4108809',
				'estado_cod' => 18,
				'cep' => '85980-000',
			),
			303 => 
			array (
				'id' => 6116,
				'nome' => 'Guairaçá',
				'uf' => 'PR',
				'cep2' => '4108908',
				'estado_cod' => 18,
				'cep' => '87880-000',
			),
			304 => 
			array (
				'id' => 6120,
				'nome' => 'Guamiranga',
				'uf' => 'PR',
				'cep2' => '4108957',
				'estado_cod' => 18,
				'cep' => '84435-000',
			),
			305 => 
			array (
				'id' => 6122,
				'nome' => 'Guapirama',
				'uf' => 'PR',
				'cep2' => '4109005',
				'estado_cod' => 18,
				'cep' => '86465-000',
			),
			306 => 
			array (
				'id' => 6124,
				'nome' => 'Guaporema',
				'uf' => 'PR',
				'cep2' => '4109104',
				'estado_cod' => 18,
				'cep' => '87810-000',
			),
			307 => 
			array (
				'id' => 6126,
				'nome' => 'Guaraci',
				'uf' => 'PR',
				'cep2' => '4109203',
				'estado_cod' => 18,
				'cep' => '86620-000',
			),
			308 => 
			array (
				'id' => 6130,
				'nome' => 'Guaraniaçu',
				'uf' => 'PR',
				'cep2' => '4109302',
				'estado_cod' => 18,
				'cep' => '85400-000',
			),
			309 => 
			array (
				'id' => 6131,
				'nome' => 'Guarapuava',
				'uf' => 'PR',
				'cep2' => '4109401',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			310 => 
			array (
				'id' => 6133,
				'nome' => 'Guaraqueçaba',
				'uf' => 'PR',
				'cep2' => '4109500',
				'estado_cod' => 18,
				'cep' => '83390-000',
			),
			311 => 
			array (
				'id' => 6135,
				'nome' => 'Guaratuba',
				'uf' => 'PR',
				'cep2' => '4109609',
				'estado_cod' => 18,
				'cep' => '83280-000',
			),
			312 => 
			array (
				'id' => 6145,
				'nome' => 'Honório Serpa',
				'uf' => 'PR',
				'cep2' => '4109658',
				'estado_cod' => 18,
				'cep' => '85548-000',
			),
			313 => 
			array (
				'id' => 6147,
				'nome' => 'Ibaiti',
				'uf' => 'PR',
				'cep2' => '4109708',
				'estado_cod' => 18,
				'cep' => '84900-000',
			),
			314 => 
			array (
				'id' => 6148,
				'nome' => 'Ibema',
				'uf' => 'PR',
				'cep2' => '4109757',
				'estado_cod' => 18,
				'cep' => '85478-000',
			),
			315 => 
			array (
				'id' => 6150,
				'nome' => 'Ibiporã',
				'uf' => 'PR',
				'cep2' => '4109807',
				'estado_cod' => 18,
				'cep' => '86200-000',
			),
			316 => 
			array (
				'id' => 6152,
				'nome' => 'Icaraíma',
				'uf' => 'PR',
				'cep2' => '4109906',
				'estado_cod' => 18,
				'cep' => '87530-000',
			),
			317 => 
			array (
				'id' => 6155,
				'nome' => 'Iguaraçu',
				'uf' => 'PR',
				'cep2' => '4110003',
				'estado_cod' => 18,
				'cep' => '86750-000',
			),
			318 => 
			array (
				'id' => 6157,
				'nome' => 'Iguatu',
				'uf' => 'PR',
				'cep2' => '4110052',
				'estado_cod' => 18,
				'cep' => '85423-000',
			),
			319 => 
			array (
				'id' => 6161,
				'nome' => 'Imbaú',
				'uf' => 'PR',
				'cep2' => '4110078',
				'estado_cod' => 18,
				'cep' => '84250-000',
			),
			320 => 
			array (
				'id' => 6163,
				'nome' => 'Imbituva',
				'uf' => 'PR',
				'cep2' => '4110102',
				'estado_cod' => 18,
				'cep' => '84430-000',
			),
			321 => 
			array (
				'id' => 6164,
				'nome' => 'Inácio Martins',
				'uf' => 'PR',
				'cep2' => '4110201',
				'estado_cod' => 18,
				'cep' => '85155-000',
			),
			322 => 
			array (
				'id' => 6165,
				'nome' => 'Inajá',
				'uf' => 'PR',
				'cep2' => '4110300',
				'estado_cod' => 18,
				'cep' => '87670-000',
			),
			323 => 
			array (
				'id' => 6167,
				'nome' => 'Indianópolis',
				'uf' => 'PR',
				'cep2' => '4110409',
				'estado_cod' => 18,
				'cep' => '87235-000',
			),
			324 => 
			array (
				'id' => 6172,
				'nome' => 'Ipiranga',
				'uf' => 'PR',
				'cep2' => '4110508',
				'estado_cod' => 18,
				'cep' => '84450-000',
			),
			325 => 
			array (
				'id' => 6175,
				'nome' => 'Iporã',
				'uf' => 'PR',
				'cep2' => '4110607',
				'estado_cod' => 18,
				'cep' => '87560-000',
			),
			326 => 
			array (
				'id' => 6176,
				'nome' => 'Iracema do Oeste',
				'uf' => 'PR',
				'cep2' => '4110656',
				'estado_cod' => 18,
				'cep' => '85833-000',
			),
			327 => 
			array (
				'id' => 6178,
				'nome' => 'Irati',
				'uf' => 'PR',
				'cep2' => '4110706',
				'estado_cod' => 18,
				'cep' => '84500-000',
			),
			328 => 
			array (
				'id' => 6180,
				'nome' => 'Iretama',
				'uf' => 'PR',
				'cep2' => '4110805',
				'estado_cod' => 18,
				'cep' => '87280-000',
			),
			329 => 
			array (
				'id' => 6181,
				'nome' => 'Itaguajé',
				'uf' => 'PR',
				'cep2' => '4110904',
				'estado_cod' => 18,
				'cep' => '86670-000',
			),
			330 => 
			array (
				'id' => 6183,
				'nome' => 'Itaipulândia',
				'uf' => 'PR',
				'cep2' => '4110953',
				'estado_cod' => 18,
				'cep' => '85880-000',
			),
			331 => 
			array (
				'id' => 6184,
				'nome' => 'Itambaracá',
				'uf' => 'PR',
				'cep2' => '4111001',
				'estado_cod' => 18,
				'cep' => '86375-000',
			),
			332 => 
			array (
				'id' => 6185,
				'nome' => 'Itambé',
				'uf' => 'PR',
				'cep2' => '4111100',
				'estado_cod' => 18,
				'cep' => '87175-000',
			),
			333 => 
			array (
				'id' => 6189,
				'nome' => 'Itapejara d\'Oeste',
				'uf' => 'PR',
				'cep2' => '4111209',
				'estado_cod' => 18,
				'cep' => '85580-000',
			),
			334 => 
			array (
				'id' => 6190,
				'nome' => 'Itaperuçu',
				'uf' => 'PR',
				'cep2' => '4111258',
				'estado_cod' => 18,
				'cep' => '83560-000',
			),
			335 => 
			array (
				'id' => 6192,
				'nome' => 'Itaúna do Sul',
				'uf' => 'PR',
				'cep2' => '4111308',
				'estado_cod' => 18,
				'cep' => '87980-000',
			),
			336 => 
			array (
				'id' => 6194,
				'nome' => 'Ivaí',
				'uf' => 'PR',
				'cep2' => '4111407',
				'estado_cod' => 18,
				'cep' => '84460-000',
			),
			337 => 
			array (
				'id' => 6196,
				'nome' => 'Ivaiporã',
				'uf' => 'PR',
				'cep2' => '4111506',
				'estado_cod' => 18,
				'cep' => '86870-000',
			),
			338 => 
			array (
				'id' => 6198,
				'nome' => 'Ivaté',
				'uf' => 'PR',
				'cep2' => '4111555',
				'estado_cod' => 18,
				'cep' => '87525-000',
			),
			339 => 
			array (
				'id' => 6199,
				'nome' => 'Ivatuba',
				'uf' => 'PR',
				'cep2' => '4111605',
				'estado_cod' => 18,
				'cep' => '87130-000',
			),
			340 => 
			array (
				'id' => 6201,
				'nome' => 'Jaboti',
				'uf' => 'PR',
				'cep2' => '4111704',
				'estado_cod' => 18,
				'cep' => '84930-000',
			),
			341 => 
			array (
				'id' => 6205,
				'nome' => 'Jacarezinho',
				'uf' => 'PR',
				'cep2' => '4111803',
				'estado_cod' => 18,
				'cep' => '86400-000',
			),
			342 => 
			array (
				'id' => 6211,
				'nome' => 'Jaguapitã',
				'uf' => 'PR',
				'cep2' => '4111902',
				'estado_cod' => 18,
				'cep' => '86610-000',
			),
			343 => 
			array (
				'id' => 6212,
				'nome' => 'Jaguariaíva',
				'uf' => 'PR',
				'cep2' => '4112009',
				'estado_cod' => 18,
				'cep' => '84200-000',
			),
			344 => 
			array (
				'id' => 6213,
				'nome' => 'Jandaia do Sul',
				'uf' => 'PR',
				'cep2' => '4112108',
				'estado_cod' => 18,
				'cep' => '86900-000',
			),
			345 => 
			array (
				'id' => 6216,
				'nome' => 'Janiópolis',
				'uf' => 'PR',
				'cep2' => '4112207',
				'estado_cod' => 18,
				'cep' => '87380-000',
			),
			346 => 
			array (
				'id' => 6217,
				'nome' => 'Japira',
				'uf' => 'PR',
				'cep2' => '4112306',
				'estado_cod' => 18,
				'cep' => '84920-000',
			),
			347 => 
			array (
				'id' => 6218,
				'nome' => 'Japurá',
				'uf' => 'PR',
				'cep2' => '4112405',
				'estado_cod' => 18,
				'cep' => '87225-000',
			),
			348 => 
			array (
				'id' => 6221,
				'nome' => 'Jardim Alegre',
				'uf' => 'PR',
				'cep2' => '4112504',
				'estado_cod' => 18,
				'cep' => '86860-000',
			),
			349 => 
			array (
				'id' => 6222,
				'nome' => 'Jardim Olinda',
				'uf' => 'PR',
				'cep2' => '4112603',
				'estado_cod' => 18,
				'cep' => '87690-000',
			),
			350 => 
			array (
				'id' => 6226,
				'nome' => 'Jataizinho',
				'uf' => 'PR',
				'cep2' => '4112702',
				'estado_cod' => 18,
				'cep' => '86210-000',
			),
			351 => 
			array (
				'id' => 6228,
				'nome' => 'Jesuítas',
				'uf' => 'PR',
				'cep2' => '4112751',
				'estado_cod' => 18,
				'cep' => '85835-000',
			),
			352 => 
			array (
				'id' => 6230,
				'nome' => 'Joaquim Távora',
				'uf' => 'PR',
				'cep2' => '4112801',
				'estado_cod' => 18,
				'cep' => '86455-000',
			),
			353 => 
			array (
				'id' => 6234,
				'nome' => 'Jundiaí do Sul',
				'uf' => 'PR',
				'cep2' => '4112900',
				'estado_cod' => 18,
				'cep' => '86470-000',
			),
			354 => 
			array (
				'id' => 6235,
				'nome' => 'Juranda',
				'uf' => 'PR',
				'cep2' => '4112959',
				'estado_cod' => 18,
				'cep' => '87355-000',
			),
			355 => 
			array (
				'id' => 6236,
				'nome' => 'Jussara',
				'uf' => 'PR',
				'cep2' => '4113007',
				'estado_cod' => 18,
				'cep' => '87230-000',
			),
			356 => 
			array (
				'id' => 6238,
				'nome' => 'Kaloré',
				'uf' => 'PR',
				'cep2' => '4113106',
				'estado_cod' => 18,
				'cep' => '86920-000',
			),
			357 => 
			array (
				'id' => 6254,
				'nome' => 'Lapa',
				'uf' => 'PR',
				'cep2' => '4113205',
				'estado_cod' => 18,
				'cep' => '83750-000',
			),
			358 => 
			array (
				'id' => 6256,
				'nome' => 'Laranjal',
				'uf' => 'PR',
				'cep2' => '4113254',
				'estado_cod' => 18,
				'cep' => '85275-000',
			),
			359 => 
			array (
				'id' => 6257,
				'nome' => 'Laranjeiras do Sul',
				'uf' => 'PR',
				'cep2' => '4113304',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			360 => 
			array (
				'id' => 6261,
				'nome' => 'Leópolis',
				'uf' => 'PR',
				'cep2' => '4113403',
				'estado_cod' => 18,
				'cep' => '86330-000',
			),
			361 => 
			array (
				'id' => 6263,
				'nome' => 'Lidianópolis',
				'uf' => 'PR',
				'cep2' => '4113429',
				'estado_cod' => 18,
				'cep' => '86865-000',
			),
			362 => 
			array (
				'id' => 6264,
				'nome' => 'Lindoeste',
				'uf' => 'PR',
				'cep2' => '4113452',
				'estado_cod' => 18,
				'cep' => '85826-000',
			),
			363 => 
			array (
				'id' => 6266,
				'nome' => 'Loanda',
				'uf' => 'PR',
				'cep2' => '4113502',
				'estado_cod' => 18,
				'cep' => '87900-000',
			),
			364 => 
			array (
				'id' => 6267,
				'nome' => 'Lobato',
				'uf' => 'PR',
				'cep2' => '4113601',
				'estado_cod' => 18,
				'cep' => '86790-000',
			),
			365 => 
			array (
				'id' => 6268,
				'nome' => 'Londrina',
				'uf' => 'PR',
				'cep2' => '4113700',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			366 => 
			array (
				'id' => 6272,
				'nome' => 'Luiziana',
				'uf' => 'PR',
				'cep2' => '4113734',
				'estado_cod' => 18,
				'cep' => '87290-000',
			),
			367 => 
			array (
				'id' => 6273,
				'nome' => 'Lunardelli',
				'uf' => 'PR',
				'cep2' => '4113759',
				'estado_cod' => 18,
				'cep' => '86935-000',
			),
			368 => 
			array (
				'id' => 6274,
				'nome' => 'Lupionópolis',
				'uf' => 'PR',
				'cep2' => '4113809',
				'estado_cod' => 18,
				'cep' => '86635-000',
			),
			369 => 
			array (
				'id' => 6279,
				'nome' => 'Mallet',
				'uf' => 'PR',
				'cep2' => '4113908',
				'estado_cod' => 18,
				'cep' => '84570-000',
			),
			370 => 
			array (
				'id' => 6281,
				'nome' => 'Mamborê',
				'uf' => 'PR',
				'cep2' => '4114005',
				'estado_cod' => 18,
				'cep' => '87340-000',
			),
			371 => 
			array (
				'id' => 6283,
				'nome' => 'Mandaguaçu',
				'uf' => 'PR',
				'cep2' => '4114104',
				'estado_cod' => 18,
				'cep' => '87160-000',
			),
			372 => 
			array (
				'id' => 6284,
				'nome' => 'Mandaguari',
				'uf' => 'PR',
				'cep2' => '4114203',
				'estado_cod' => 18,
				'cep' => '86975-000',
			),
			373 => 
			array (
				'id' => 6286,
				'nome' => 'Mandirituba',
				'uf' => 'PR',
				'cep2' => '4114302',
				'estado_cod' => 18,
				'cep' => '83800-000',
			),
			374 => 
			array (
				'id' => 6287,
				'nome' => 'Manfrinópolis',
				'uf' => 'PR',
				'cep2' => '4114351',
				'estado_cod' => 18,
				'cep' => '85628-000',
			),
			375 => 
			array (
				'id' => 6288,
				'nome' => 'Mangueirinha',
				'uf' => 'PR',
				'cep2' => '4114401',
				'estado_cod' => 18,
				'cep' => '85540-000',
			),
			376 => 
			array (
				'id' => 6289,
				'nome' => 'Manoel Ribas',
				'uf' => 'PR',
				'cep2' => '4114500',
				'estado_cod' => 18,
				'cep' => '85260-000',
			),
			377 => 
			array (
				'id' => 6297,
				'nome' => 'Marechal Cândido Rondon',
				'uf' => 'PR',
				'cep2' => '4114609',
				'estado_cod' => 18,
				'cep' => '85960-000',
			),
			378 => 
			array (
				'id' => 6299,
				'nome' => 'Maria Helena',
				'uf' => 'PR',
				'cep2' => '4114708',
				'estado_cod' => 18,
				'cep' => '87480-000',
			),
			379 => 
			array (
				'id' => 6301,
				'nome' => 'Marialva',
				'uf' => 'PR',
				'cep2' => '4114807',
				'estado_cod' => 18,
				'cep' => '86990-000',
			),
			380 => 
			array (
				'id' => 6303,
				'nome' => 'Marilândia do Sul',
				'uf' => 'PR',
				'cep2' => '4114906',
				'estado_cod' => 18,
				'cep' => '86825-000',
			),
			381 => 
			array (
				'id' => 6304,
				'nome' => 'Marilena',
				'uf' => 'PR',
				'cep2' => '4115002',
				'estado_cod' => 18,
				'cep' => '87960-000',
			),
			382 => 
			array (
				'id' => 6306,
				'nome' => 'Mariluz',
				'uf' => 'PR',
				'cep2' => '4115101',
				'estado_cod' => 18,
				'cep' => '87470-000',
			),
			383 => 
			array (
				'id' => 6308,
				'nome' => 'Maringá',
				'uf' => 'PR',
				'cep2' => '4115200',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			384 => 
			array (
				'id' => 6309,
				'nome' => 'Mariópolis',
				'uf' => 'PR',
				'cep2' => '4115309',
				'estado_cod' => 18,
				'cep' => '85525-000',
			),
			385 => 
			array (
				'id' => 6310,
				'nome' => 'Maripá',
				'uf' => 'PR',
				'cep2' => '4115358',
				'estado_cod' => 18,
				'cep' => '85955-000',
			),
			386 => 
			array (
				'id' => 6314,
				'nome' => 'Marmeleiro',
				'uf' => 'PR',
				'cep2' => '4115408',
				'estado_cod' => 18,
				'cep' => '85615-000',
			),
			387 => 
			array (
				'id' => 6316,
				'nome' => 'Marquinho',
				'uf' => 'PR',
				'cep2' => '4115457',
				'estado_cod' => 18,
				'cep' => '85168-000',
			),
			388 => 
			array (
				'id' => 6319,
				'nome' => 'Marumbi',
				'uf' => 'PR',
				'cep2' => '4115507',
				'estado_cod' => 18,
				'cep' => '86910-000',
			),
			389 => 
			array (
				'id' => 6320,
				'nome' => 'Matelândia',
				'uf' => 'PR',
				'cep2' => '4115606',
				'estado_cod' => 18,
				'cep' => '85887-000',
			),
			390 => 
			array (
				'id' => 6321,
				'nome' => 'Matinhos',
				'uf' => 'PR',
				'cep2' => '4115705',
				'estado_cod' => 18,
				'cep' => '83260-000',
			),
			391 => 
			array (
				'id' => 6324,
				'nome' => 'Mato Rico',
				'uf' => 'PR',
				'cep2' => '4115739',
				'estado_cod' => 18,
				'cep' => '85240-000',
			),
			392 => 
			array (
				'id' => 6325,
				'nome' => 'Mauá da Serra',
				'uf' => 'PR',
				'cep2' => '4115754',
				'estado_cod' => 18,
				'cep' => '86828-000',
			),
			393 => 
			array (
				'id' => 6326,
				'nome' => 'Medianeira',
				'uf' => 'PR',
				'cep2' => '4115804',
				'estado_cod' => 18,
				'cep' => '85884-000',
			),
			394 => 
			array (
				'id' => 6330,
				'nome' => 'Mercedes',
				'uf' => 'PR',
				'cep2' => '4115853',
				'estado_cod' => 18,
				'cep' => '85998-000',
			),
			395 => 
			array (
				'id' => 6331,
				'nome' => 'Mirador',
				'uf' => 'PR',
				'cep2' => '4115903',
				'estado_cod' => 18,
				'cep' => '87840-000',
			),
			396 => 
			array (
				'id' => 6334,
				'nome' => 'Miraselva',
				'uf' => 'PR',
				'cep2' => '4116000',
				'estado_cod' => 18,
				'cep' => '86615-000',
			),
			397 => 
			array (
				'id' => 6335,
				'nome' => 'Missal',
				'uf' => 'PR',
				'cep2' => '4116059',
				'estado_cod' => 18,
				'cep' => '85890-000',
			),
			398 => 
			array (
				'id' => 6338,
				'nome' => 'Moreira Sales',
				'uf' => 'PR',
				'cep2' => '4116109',
				'estado_cod' => 18,
				'cep' => '87370-000',
			),
			399 => 
			array (
				'id' => 6339,
				'nome' => 'Morretes',
				'uf' => 'PR',
				'cep2' => '4116208',
				'estado_cod' => 18,
				'cep' => '83350-000',
			),
			400 => 
			array (
				'id' => 6342,
				'nome' => 'Munhoz de Melo',
				'uf' => 'PR',
				'cep2' => '4116307',
				'estado_cod' => 18,
				'cep' => '86760-000',
			),
			401 => 
			array (
				'id' => 6349,
				'nome' => 'Nossa Senhora das Graças',
				'uf' => 'PR',
				'cep2' => '4116406',
				'estado_cod' => 18,
				'cep' => '86680-000',
			),
			402 => 
			array (
				'id' => 6352,
				'nome' => 'Nova Aliança do Ivaí',
				'uf' => 'PR',
				'cep2' => '4116505',
				'estado_cod' => 18,
				'cep' => '87790-000',
			),
			403 => 
			array (
				'id' => 6354,
				'nome' => 'Nova América da Colina',
				'uf' => 'PR',
				'cep2' => '4116604',
				'estado_cod' => 18,
				'cep' => '86230-000',
			),
			404 => 
			array (
				'id' => 6356,
				'nome' => 'Nova Aurora',
				'uf' => 'PR',
				'cep2' => '4116703',
				'estado_cod' => 18,
				'cep' => '85410-000',
			),
			405 => 
			array (
				'id' => 6361,
				'nome' => 'Nova Cantu',
				'uf' => 'PR',
				'cep2' => '4116802',
				'estado_cod' => 18,
				'cep' => '87330-000',
			),
			406 => 
			array (
				'id' => 6364,
				'nome' => 'Nova Esperança',
				'uf' => 'PR',
				'cep2' => '4116901',
				'estado_cod' => 18,
				'cep' => '87600-000',
			),
			407 => 
			array (
				'id' => 6365,
				'nome' => 'Nova Esperança do Sudoeste',
				'uf' => 'PR',
				'cep2' => '4116950',
				'estado_cod' => 18,
				'cep' => '85635-000',
			),
			408 => 
			array (
				'id' => 6366,
				'nome' => 'Nova Fátima',
				'uf' => 'PR',
				'cep2' => '4117008',
				'estado_cod' => 18,
				'cep' => '86310-000',
			),
			409 => 
			array (
				'id' => 6367,
				'nome' => 'Nova Laranjeiras',
				'uf' => 'PR',
				'cep2' => '4117057',
				'estado_cod' => 18,
				'cep' => '85350-000',
			),
			410 => 
			array (
				'id' => 6368,
				'nome' => 'Nova Londrina',
				'uf' => 'PR',
				'cep2' => '4117107',
				'estado_cod' => 18,
				'cep' => '87970-000',
			),
			411 => 
			array (
				'id' => 6370,
				'nome' => 'Nova Olímpia',
				'uf' => 'PR',
				'cep2' => '4117206',
				'estado_cod' => 18,
				'cep' => '87490-000',
			),
			412 => 
			array (
				'id' => 6371,
				'nome' => 'Nova Prata do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4117255',
				'estado_cod' => 18,
				'cep' => '85685-000',
			),
			413 => 
			array (
				'id' => 6373,
				'nome' => 'Nova Santa Bárbara',
				'uf' => 'PR',
				'cep2' => '4117214',
				'estado_cod' => 18,
				'cep' => '86250-000',
			),
			414 => 
			array (
				'id' => 6374,
				'nome' => 'Nova Santa Rosa',
				'uf' => 'PR',
				'cep2' => '4117222',
				'estado_cod' => 18,
				'cep' => '85930-000',
			),
			415 => 
			array (
				'id' => 6375,
				'nome' => 'Nova Tebas',
				'uf' => 'PR',
				'cep2' => '4117271',
				'estado_cod' => 18,
				'cep' => '85250-000',
			),
			416 => 
			array (
				'id' => 6380,
				'nome' => 'Novo Itacolomi',
				'uf' => 'PR',
				'cep2' => '4117297',
				'estado_cod' => 18,
				'cep' => '86895-000',
			),
			417 => 
			array (
				'id' => 6390,
				'nome' => 'Ortigueira',
				'uf' => 'PR',
				'cep2' => '4117305',
				'estado_cod' => 18,
				'cep' => '84350-000',
			),
			418 => 
			array (
				'id' => 6392,
				'nome' => 'Ourizona',
				'uf' => 'PR',
				'cep2' => '4117404',
				'estado_cod' => 18,
				'cep' => '87170-000',
			),
			419 => 
			array (
				'id' => 6393,
				'nome' => 'Ouro Verde do Oeste',
				'uf' => 'PR',
				'cep2' => '4117453',
				'estado_cod' => 18,
				'cep' => '85933-000',
			),
			420 => 
			array (
				'id' => 6396,
				'nome' => 'Paiçandu',
				'uf' => 'PR',
				'cep2' => '4117503',
				'estado_cod' => 18,
				'cep' => '87140-000',
			),
			421 => 
			array (
				'id' => 6401,
				'nome' => 'Palmas',
				'uf' => 'PR',
				'cep2' => '4117602',
				'estado_cod' => 18,
				'cep' => '85555-000',
			),
			422 => 
			array (
				'id' => 6402,
				'nome' => 'Palmeira',
				'uf' => 'PR',
				'cep2' => '4117701',
				'estado_cod' => 18,
				'cep' => '84130-000',
			),
			423 => 
			array (
				'id' => 6406,
				'nome' => 'Palmital',
				'uf' => 'PR',
				'cep2' => '4117800',
				'estado_cod' => 18,
				'cep' => '85270-000',
			),
			424 => 
			array (
				'id' => 6410,
				'nome' => 'Palotina',
				'uf' => 'PR',
				'cep2' => '4117909',
				'estado_cod' => 18,
				'cep' => '85950-000',
			),
			425 => 
			array (
				'id' => 6414,
				'nome' => 'Paraíso do Norte',
				'uf' => 'PR',
				'cep2' => '4118006',
				'estado_cod' => 18,
				'cep' => '87780-000',
			),
			426 => 
			array (
				'id' => 6416,
				'nome' => 'Paranacity',
				'uf' => 'PR',
				'cep2' => '4118105',
				'estado_cod' => 18,
				'cep' => '87660-000',
			),
			427 => 
			array (
				'id' => 6418,
				'nome' => 'Paranaguá',
				'uf' => 'PR',
				'cep2' => '4118204',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			428 => 
			array (
				'id' => 6419,
				'nome' => 'Paranapoema',
				'uf' => 'PR',
				'cep2' => '4118303',
				'estado_cod' => 18,
				'cep' => '87680-000',
			),
			429 => 
			array (
				'id' => 6420,
				'nome' => 'Paranavaí',
				'uf' => 'PR',
				'cep2' => '4118402',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			430 => 
			array (
				'id' => 6426,
				'nome' => 'Pato Bragado',
				'uf' => 'PR',
				'cep2' => '4118451',
				'estado_cod' => 18,
				'cep' => '85948-000',
			),
			431 => 
			array (
				'id' => 6427,
				'nome' => 'Pato Branco',
				'uf' => 'PR',
				'cep2' => '4118501',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			432 => 
			array (
				'id' => 6430,
				'nome' => 'Paula Freitas',
				'uf' => 'PR',
				'cep2' => '4118600',
				'estado_cod' => 18,
				'cep' => '84630-000',
			),
			433 => 
			array (
				'id' => 6432,
				'nome' => 'Paulo Frontin',
				'uf' => 'PR',
				'cep2' => '4118709',
				'estado_cod' => 18,
				'cep' => '84635-000',
			),
			434 => 
			array (
				'id' => 6433,
				'nome' => 'Peabiru',
				'uf' => 'PR',
				'cep2' => '4118808',
				'estado_cod' => 18,
				'cep' => '87250-000',
			),
			435 => 
			array (
				'id' => 6438,
				'nome' => 'Perobal',
				'uf' => 'PR',
				'cep2' => '4118857',
				'estado_cod' => 18,
				'cep' => '87538-000',
			),
			436 => 
			array (
				'id' => 6439,
				'nome' => 'Pérola',
				'uf' => 'PR',
				'cep2' => '4118907',
				'estado_cod' => 18,
				'cep' => '87540-000',
			),
			437 => 
			array (
				'id' => 6440,
				'nome' => 'Pérola d\'Oeste',
				'uf' => 'PR',
				'cep2' => '4119004',
				'estado_cod' => 18,
				'cep' => '85740-000',
			),
			438 => 
			array (
				'id' => 6443,
				'nome' => 'Piên',
				'uf' => 'PR',
				'cep2' => '4119103',
				'estado_cod' => 18,
				'cep' => '83860-000',
			),
			439 => 
			array (
				'id' => 6445,
				'nome' => 'Pinhais',
				'uf' => 'PR',
				'cep2' => '4119152',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			440 => 
			array (
				'id' => 6446,
				'nome' => 'Pinhal de São Bento',
				'uf' => 'PR',
				'cep2' => '4119251',
				'estado_cod' => 18,
				'cep' => '85727-000',
			),
			441 => 
			array (
				'id' => 6448,
				'nome' => 'Pinhalão',
				'uf' => 'PR',
				'cep2' => '4119202',
				'estado_cod' => 18,
				'cep' => '84925-000',
			),
			442 => 
			array (
				'id' => 6454,
				'nome' => 'Pinhão',
				'uf' => 'PR',
				'cep2' => '4119301',
				'estado_cod' => 18,
				'cep' => '85170-000',
			),
			443 => 
			array (
				'id' => 6458,
				'nome' => 'Piraí do Sul',
				'uf' => 'PR',
				'cep2' => '4119400',
				'estado_cod' => 18,
				'cep' => '84240-000',
			),
			444 => 
			array (
				'id' => 6460,
				'nome' => 'Piraquara',
				'uf' => 'PR',
				'cep2' => '4119509',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			445 => 
			array (
				'id' => 6462,
				'nome' => 'Pitanga',
				'uf' => 'PR',
				'cep2' => '4119608',
				'estado_cod' => 18,
				'cep' => '85200-000',
			),
			446 => 
			array (
				'id' => 6463,
				'nome' => 'Pitangueiras',
				'uf' => 'PR',
				'cep2' => '4119657',
				'estado_cod' => 18,
				'cep' => '86613-000',
			),
			447 => 
			array (
				'id' => 6465,
				'nome' => 'Planaltina do Paraná',
				'uf' => 'PR',
				'cep2' => '4119707',
				'estado_cod' => 18,
				'cep' => '87860-000',
			),
			448 => 
			array (
				'id' => 6466,
				'nome' => 'Planalto',
				'uf' => 'PR',
				'cep2' => '4119806',
				'estado_cod' => 18,
				'cep' => '85750-000',
			),
			449 => 
			array (
				'id' => 6471,
				'nome' => 'Ponta Grossa',
				'uf' => 'PR',
				'cep2' => '4119905',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			450 => 
			array (
				'id' => 6472,
				'nome' => 'Pontal do Paraná',
				'uf' => 'PR',
				'cep2' => '4119954',
				'estado_cod' => 18,
				'cep' => '83255-000',
			),
			451 => 
			array (
				'id' => 6473,
				'nome' => 'Porecatu',
				'uf' => 'PR',
				'cep2' => '4120002',
				'estado_cod' => 18,
				'cep' => '86160-000',
			),
			452 => 
			array (
				'id' => 6476,
				'nome' => 'Porto Amazonas',
				'uf' => 'PR',
				'cep2' => '4120101',
				'estado_cod' => 18,
				'cep' => '84140-000',
			),
			453 => 
			array (
				'id' => 6477,
				'nome' => 'Porto Barreiro',
				'uf' => 'PR',
				'cep2' => '4120150',
				'estado_cod' => 18,
				'cep' => '85345-000',
			),
			454 => 
			array (
				'id' => 6484,
				'nome' => 'Porto Rico',
				'uf' => 'PR',
				'cep2' => '4120200',
				'estado_cod' => 18,
				'cep' => '87950-000',
			),
			455 => 
			array (
				'id' => 6489,
				'nome' => 'Porto Vitória',
				'uf' => 'PR',
				'cep2' => '4120309',
				'estado_cod' => 18,
				'cep' => '84610-000',
			),
			456 => 
			array (
				'id' => 6490,
				'nome' => 'Prado Ferreira',
				'uf' => 'PR',
				'cep2' => '4120333',
				'estado_cod' => 18,
				'cep' => '86618-000',
			),
			457 => 
			array (
				'id' => 6491,
				'nome' => 'Pranchita',
				'uf' => 'PR',
				'cep2' => '4120358',
				'estado_cod' => 18,
				'cep' => '85730-000',
			),
			458 => 
			array (
				'id' => 6494,
				'nome' => 'Presidente Castelo Branco',
				'uf' => 'PR',
				'cep2' => '4120408',
				'estado_cod' => 18,
				'cep' => '87180-000',
			),
			459 => 
			array (
				'id' => 6496,
				'nome' => 'Primeiro de Maio',
				'uf' => 'PR',
				'cep2' => '4120507',
				'estado_cod' => 18,
				'cep' => '86140-000',
			),
			460 => 
			array (
				'id' => 6498,
				'nome' => 'Prudentópolis',
				'uf' => 'PR',
				'cep2' => '4120606',
				'estado_cod' => 18,
				'cep' => '84400-000',
			),
			461 => 
			array (
				'id' => 6500,
				'nome' => 'Quatiguá',
				'uf' => 'PR',
				'cep2' => '4120705',
				'estado_cod' => 18,
				'cep' => '86450-000',
			),
			462 => 
			array (
				'id' => 6501,
				'nome' => 'Quatro Barras',
				'uf' => 'PR',
				'cep2' => '4120804',
				'estado_cod' => 18,
				'cep' => '83420-000',
			),
			463 => 
			array (
				'id' => 6502,
				'nome' => 'Quatro Pontes',
				'uf' => 'PR',
				'cep2' => '4120853',
				'estado_cod' => 18,
				'cep' => '85940-000',
			),
			464 => 
			array (
				'id' => 6504,
				'nome' => 'Quedas do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4120903',
				'estado_cod' => 18,
				'cep' => '85460-000',
			),
			465 => 
			array (
				'id' => 6506,
				'nome' => 'Querência do Norte',
				'uf' => 'PR',
				'cep2' => '4121000',
				'estado_cod' => 18,
				'cep' => '87930-000',
			),
			466 => 
			array (
				'id' => 6507,
				'nome' => 'Quinta do Sol',
				'uf' => 'PR',
				'cep2' => '4121109',
				'estado_cod' => 18,
				'cep' => '87265-000',
			),
			467 => 
			array (
				'id' => 6509,
				'nome' => 'Quitandinha',
				'uf' => 'PR',
				'cep2' => '4121208',
				'estado_cod' => 18,
				'cep' => '83840-000',
			),
			468 => 
			array (
				'id' => 6510,
				'nome' => 'Ramilândia',
				'uf' => 'PR',
				'cep2' => '4121257',
				'estado_cod' => 18,
				'cep' => '85888-000',
			),
			469 => 
			array (
				'id' => 6511,
				'nome' => 'Rancho Alegre',
				'uf' => 'PR',
				'cep2' => '4121307',
				'estado_cod' => 18,
				'cep' => '86290-000',
			),
			470 => 
			array (
				'id' => 6512,
				'nome' => 'Rancho Alegre d\'Oeste',
				'uf' => 'PR',
				'cep2' => '4121356',
				'estado_cod' => 18,
				'cep' => '87395-000',
			),
			471 => 
			array (
				'id' => 6513,
				'nome' => 'Realeza',
				'uf' => 'PR',
				'cep2' => '4121406',
				'estado_cod' => 18,
				'cep' => '85770-000',
			),
			472 => 
			array (
				'id' => 6514,
				'nome' => 'Rebouças',
				'uf' => 'PR',
				'cep2' => '4121505',
				'estado_cod' => 18,
				'cep' => '84550-000',
			),
			473 => 
			array (
				'id' => 6517,
				'nome' => 'Renascença',
				'uf' => 'PR',
				'cep2' => '4121604',
				'estado_cod' => 18,
				'cep' => '85610-000',
			),
			474 => 
			array (
				'id' => 6518,
				'nome' => 'Reserva',
				'uf' => 'PR',
				'cep2' => '4121703',
				'estado_cod' => 18,
				'cep' => '84320-000',
			),
			475 => 
			array (
				'id' => 6519,
				'nome' => 'Reserva do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4121752',
				'estado_cod' => 18,
				'cep' => '85195-000',
			),
			476 => 
			array (
				'id' => 6523,
				'nome' => 'Ribeirão Claro',
				'uf' => 'PR',
				'cep2' => '4121802',
				'estado_cod' => 18,
				'cep' => '86410-000',
			),
			477 => 
			array (
				'id' => 6524,
				'nome' => 'Ribeirão do Pinhal',
				'uf' => 'PR',
				'cep2' => '4121901',
				'estado_cod' => 18,
				'cep' => '86490-000',
			),
			478 => 
			array (
				'id' => 6527,
				'nome' => 'Rio Azul',
				'uf' => 'PR',
				'cep2' => '4122008',
				'estado_cod' => 18,
				'cep' => '84560-000',
			),
			479 => 
			array (
				'id' => 6528,
				'nome' => 'Rio Bom',
				'uf' => 'PR',
				'cep2' => '4122107',
				'estado_cod' => 18,
				'cep' => '86830-000',
			),
			480 => 
			array (
				'id' => 6530,
				'nome' => 'Rio Bonito do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4122156',
				'estado_cod' => 18,
				'cep' => '85340-000',
			),
			481 => 
			array (
				'id' => 6531,
				'nome' => 'Rio Branco do Ivaí',
				'uf' => 'PR',
				'cep2' => '4122172',
				'estado_cod' => 18,
				'cep' => '86848-000',
			),
			482 => 
			array (
				'id' => 6532,
				'nome' => 'Rio Branco do Sul',
				'uf' => 'PR',
				'cep2' => '4122206',
				'estado_cod' => 18,
				'cep' => '83540-000',
			),
			483 => 
			array (
				'id' => 6543,
				'nome' => 'Rio Negro',
				'uf' => 'PR',
				'cep2' => '4122305',
				'estado_cod' => 18,
				'cep' => '83880-000',
			),
			484 => 
			array (
				'id' => 6554,
				'nome' => 'Rolândia',
				'uf' => 'PR',
				'cep2' => '4122404',
				'estado_cod' => 18,
				'cep' => '86600-000',
			),
			485 => 
			array (
				'id' => 6556,
				'nome' => 'Roncador',
				'uf' => 'PR',
				'cep2' => '4122503',
				'estado_cod' => 18,
				'cep' => '87320-000',
			),
			486 => 
			array (
				'id' => 6558,
				'nome' => 'Rondon',
				'uf' => 'PR',
				'cep2' => '4122602',
				'estado_cod' => 18,
				'cep' => '87800-000',
			),
			487 => 
			array (
				'id' => 6559,
				'nome' => 'Rosário do Ivaí',
				'uf' => 'PR',
				'cep2' => '4122651',
				'estado_cod' => 18,
				'cep' => '86850-000',
			),
			488 => 
			array (
				'id' => 6560,
				'nome' => 'Sabáudia',
				'uf' => 'PR',
				'cep2' => '4122701',
				'estado_cod' => 18,
				'cep' => '86720-000',
			),
			489 => 
			array (
				'id' => 6562,
				'nome' => 'Salgado Filho',
				'uf' => 'PR',
				'cep2' => '4122800',
				'estado_cod' => 18,
				'cep' => '85620-000',
			),
			490 => 
			array (
				'id' => 6565,
				'nome' => 'Salto do Itararé',
				'uf' => 'PR',
				'cep2' => '4122909',
				'estado_cod' => 18,
				'cep' => '84945-000',
			),
			491 => 
			array (
				'id' => 6566,
				'nome' => 'Salto do Lontra',
				'uf' => 'PR',
				'cep2' => '4123006',
				'estado_cod' => 18,
				'cep' => '85670-000',
			),
			492 => 
			array (
				'id' => 6569,
				'nome' => 'Santa Amélia',
				'uf' => 'PR',
				'cep2' => '4123105',
				'estado_cod' => 18,
				'cep' => '86370-000',
			),
			493 => 
			array (
				'id' => 6570,
				'nome' => 'Santa Cecília do Pavão',
				'uf' => 'PR',
				'cep2' => '4123204',
				'estado_cod' => 18,
				'cep' => '86225-000',
			),
			494 => 
			array (
				'id' => 6572,
				'nome' => 'Santa Cruz de Monte Castelo',
				'uf' => 'PR',
				'cep2' => '',
				'estado_cod' => 18,
				'cep' => '87920-000',
			),
			495 => 
			array (
				'id' => 6575,
				'nome' => 'Santa Fé',
				'uf' => 'PR',
				'cep2' => '4123402',
				'estado_cod' => 18,
				'cep' => '86770-000',
			),
			496 => 
			array (
				'id' => 6577,
				'nome' => 'Santa Helena',
				'uf' => 'PR',
				'cep2' => '4123501',
				'estado_cod' => 18,
				'cep' => '85892-000',
			),
			497 => 
			array (
				'id' => 6578,
				'nome' => 'Santa Inês',
				'uf' => 'PR',
				'cep2' => '4123600',
				'estado_cod' => 18,
				'cep' => '86660-000',
			),
			498 => 
			array (
				'id' => 6579,
				'nome' => 'Santa Isabel do Ivaí',
				'uf' => 'PR',
				'cep2' => '4123709',
				'estado_cod' => 18,
				'cep' => '87910-000',
			),
			499 => 
			array (
				'id' => 6580,
				'nome' => 'Santa Izabel do Oeste',
				'uf' => 'PR',
				'cep2' => '4123808',
				'estado_cod' => 18,
				'cep' => '85650-000',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 6581,
				'nome' => 'Santa Lúcia',
				'uf' => 'PR',
				'cep2' => '4123824',
				'estado_cod' => 18,
				'cep' => '85795-000',
			),
			1 => 
			array (
				'id' => 6587,
				'nome' => 'Santa Maria do Oeste',
				'uf' => 'PR',
				'cep2' => '4123857',
				'estado_cod' => 18,
				'cep' => '85230-000',
			),
			2 => 
			array (
				'id' => 6589,
				'nome' => 'Santa Mariana',
				'uf' => 'PR',
				'cep2' => '4123907',
				'estado_cod' => 18,
				'cep' => '86350-000',
			),
			3 => 
			array (
				'id' => 6590,
				'nome' => 'Santa Mônica',
				'uf' => 'PR',
				'cep2' => '4123956',
				'estado_cod' => 18,
				'cep' => '87915-000',
			),
			4 => 
			array (
				'id' => 6597,
				'nome' => 'Santa Tereza do Oeste',
				'uf' => 'PR',
				'cep2' => '4124020',
				'estado_cod' => 18,
				'cep' => '85825-000',
			),
			5 => 
			array (
				'id' => 6598,
				'nome' => 'Santa Terezinha de Itaipu',
				'uf' => 'PR',
				'cep2' => '4124053',
				'estado_cod' => 18,
				'cep' => '85875-000',
			),
			6 => 
			array (
				'id' => 6601,
				'nome' => 'Santana do Itararé',
				'uf' => 'PR',
				'cep2' => '4124004',
				'estado_cod' => 18,
				'cep' => '84970-000',
			),
			7 => 
			array (
				'id' => 6604,
				'nome' => 'Santo Antônio da Platina',
				'uf' => 'PR',
				'cep2' => '4124103',
				'estado_cod' => 18,
				'cep' => '86430-000',
			),
			8 => 
			array (
				'id' => 6605,
				'nome' => 'Santo Antônio do Caiuá',
				'uf' => 'PR',
				'cep2' => '4124202',
				'estado_cod' => 18,
				'cep' => '87730-000',
			),
			9 => 
			array (
				'id' => 6608,
				'nome' => 'Santo Antônio do Paraíso',
				'uf' => 'PR',
				'cep2' => '4124301',
				'estado_cod' => 18,
				'cep' => '86315-000',
			),
			10 => 
			array (
				'id' => 6609,
				'nome' => 'Santo Antônio do Sudoeste',
				'uf' => 'PR',
				'cep2' => '4124400',
				'estado_cod' => 18,
				'cep' => '85710-000',
			),
			11 => 
			array (
				'id' => 6610,
				'nome' => 'Santo Inácio',
				'uf' => 'PR',
				'cep2' => '4124509',
				'estado_cod' => 18,
				'cep' => '86650-000',
			),
			12 => 
			array (
				'id' => 6616,
				'nome' => 'São Carlos do Ivaí',
				'uf' => 'PR',
				'cep2' => '4124608',
				'estado_cod' => 18,
				'cep' => '87770-000',
			),
			13 => 
			array (
				'id' => 6632,
				'nome' => 'São Jerônimo da Serra',
				'uf' => 'PR',
				'cep2' => '4124707',
				'estado_cod' => 18,
				'cep' => '86270-000',
			),
			14 => 
			array (
				'id' => 6633,
				'nome' => 'São João',
				'uf' => 'PR',
				'cep2' => '4124806',
				'estado_cod' => 18,
				'cep' => '85570-000',
			),
			15 => 
			array (
				'id' => 6639,
				'nome' => 'São João do Caiuá',
				'uf' => 'PR',
				'cep2' => '4124905',
				'estado_cod' => 18,
				'cep' => '87740-000',
			),
			16 => 
			array (
				'id' => 6640,
				'nome' => 'São João do Ivaí',
				'uf' => 'PR',
				'cep2' => '4125001',
				'estado_cod' => 18,
				'cep' => '86930-000',
			),
			17 => 
			array (
				'id' => 6642,
				'nome' => 'São João do Triunfo',
				'uf' => 'PR',
				'cep2' => '4125100',
				'estado_cod' => 18,
				'cep' => '84150-000',
			),
			18 => 
			array (
				'id' => 6646,
				'nome' => 'São Jorge D\'Oeste',
				'uf' => 'PR',
				'cep2' => '4125209',
				'estado_cod' => 18,
				'cep' => '85575-000',
			),
			19 => 
			array (
				'id' => 6647,
				'nome' => 'São Jorge do Ivaí',
				'uf' => 'PR',
				'cep2' => '4125308',
				'estado_cod' => 18,
				'cep' => '87190-000',
			),
			20 => 
			array (
				'id' => 6648,
				'nome' => 'São Jorge do Patrocínio',
				'uf' => 'PR',
				'cep2' => '4125357',
				'estado_cod' => 18,
				'cep' => '87555-000',
			),
			21 => 
			array (
				'id' => 6651,
				'nome' => 'São José da Boa Vista',
				'uf' => 'PR',
				'cep2' => '4125407',
				'estado_cod' => 18,
				'cep' => '84980-000',
			),
			22 => 
			array (
				'id' => 6652,
				'nome' => 'São José das Palmeiras',
				'uf' => 'PR',
				'cep2' => '4125456',
				'estado_cod' => 18,
				'cep' => '85898-000',
			),
			23 => 
			array (
				'id' => 6656,
				'nome' => 'São José dos Pinhais',
				'uf' => 'PR',
				'cep2' => '4125506',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			24 => 
			array (
				'id' => 6670,
				'nome' => 'São Manoel do Paraná',
				'uf' => 'PR',
				'cep2' => '4125555',
				'estado_cod' => 18,
				'cep' => '87215-000',
			),
			25 => 
			array (
				'id' => 6674,
				'nome' => 'São Mateus do Sul',
				'uf' => 'PR',
				'cep2' => '4125605',
				'estado_cod' => 18,
				'cep' => '83900-000',
			),
			26 => 
			array (
				'id' => 6679,
				'nome' => 'São Miguel do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4125704',
				'estado_cod' => 18,
				'cep' => '85877-000',
			),
			27 => 
			array (
				'id' => 6687,
				'nome' => 'São Pedro do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4125753',
				'estado_cod' => 18,
				'cep' => '85929-000',
			),
			28 => 
			array (
				'id' => 6688,
				'nome' => 'São Pedro do Ivaí',
				'uf' => 'PR',
				'cep2' => '4125803',
				'estado_cod' => 18,
				'cep' => '86945-000',
			),
			29 => 
			array (
				'id' => 6689,
				'nome' => 'São Pedro do Paraná',
				'uf' => 'PR',
				'cep2' => '4125902',
				'estado_cod' => 18,
				'cep' => '87955-000',
			),
			30 => 
			array (
				'id' => 6702,
				'nome' => 'São Sebastião da Amoreira',
				'uf' => 'PR',
				'cep2' => '4126009',
				'estado_cod' => 18,
				'cep' => '86240-000',
			),
			31 => 
			array (
				'id' => 6704,
				'nome' => 'São Tomé',
				'uf' => 'PR',
				'cep2' => '4126108',
				'estado_cod' => 18,
				'cep' => '87220-000',
			),
			32 => 
			array (
				'id' => 6708,
				'nome' => 'Sapopema',
				'uf' => 'PR',
				'cep2' => '4126207',
				'estado_cod' => 18,
				'cep' => '84290-000',
			),
			33 => 
			array (
				'id' => 6709,
				'nome' => 'Sarandi',
				'uf' => 'PR',
				'cep2' => '4126256',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			34 => 
			array (
				'id' => 6711,
				'nome' => 'Saudade do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4126272',
				'estado_cod' => 18,
				'cep' => '85568-000',
			),
			35 => 
			array (
				'id' => 6718,
				'nome' => 'Sengés',
				'uf' => 'PR',
				'cep2' => '4126306',
				'estado_cod' => 18,
				'cep' => '84220-000',
			),
			36 => 
			array (
				'id' => 6722,
				'nome' => 'Serranópolis do Iguaçu',
				'uf' => 'PR',
				'cep2' => '4126355',
				'estado_cod' => 18,
				'cep' => '85885-000',
			),
			37 => 
			array (
				'id' => 6727,
				'nome' => 'Sertaneja',
				'uf' => 'PR',
				'cep2' => '4126405',
				'estado_cod' => 18,
				'cep' => '86340-000',
			),
			38 => 
			array (
				'id' => 6728,
				'nome' => 'Sertanópolis',
				'uf' => 'PR',
				'cep2' => '4126504',
				'estado_cod' => 18,
				'cep' => '86170-000',
			),
			39 => 
			array (
				'id' => 6734,
				'nome' => 'Siqueira Campos',
				'uf' => 'PR',
				'cep2' => '4126603',
				'estado_cod' => 18,
				'cep' => '84940-000',
			),
			40 => 
			array (
				'id' => 6737,
				'nome' => 'Sulina',
				'uf' => 'PR',
				'cep2' => '4126652',
				'estado_cod' => 18,
				'cep' => '85565-000',
			),
			41 => 
			array (
				'id' => 6742,
				'nome' => 'Tamarana',
				'uf' => 'PR',
				'cep2' => '4126678',
				'estado_cod' => 18,
				'cep' => '86125-000',
			),
			42 => 
			array (
				'id' => 6744,
				'nome' => 'Tamboara',
				'uf' => 'PR',
				'cep2' => '4126702',
				'estado_cod' => 18,
				'cep' => '87760-000',
			),
			43 => 
			array (
				'id' => 6746,
				'nome' => 'Tapejara',
				'uf' => 'PR',
				'cep2' => '4126801',
				'estado_cod' => 18,
				'cep' => '87430-000',
			),
			44 => 
			array (
				'id' => 6747,
				'nome' => 'Tapira',
				'uf' => 'PR',
				'cep2' => '4126900',
				'estado_cod' => 18,
				'cep' => '87830-000',
			),
			45 => 
			array (
				'id' => 6753,
				'nome' => 'Teixeira Soares',
				'uf' => 'PR',
				'cep2' => '4127007',
				'estado_cod' => 18,
				'cep' => '84530-000',
			),
			46 => 
			array (
				'id' => 6754,
				'nome' => 'Telêmaco Borba',
				'uf' => 'PR',
				'cep2' => '4127106',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			47 => 
			array (
				'id' => 6758,
				'nome' => 'Terra Boa',
				'uf' => 'PR',
				'cep2' => '4127205',
				'estado_cod' => 18,
				'cep' => '87240-000',
			),
			48 => 
			array (
				'id' => 6760,
				'nome' => 'Terra Rica',
				'uf' => 'PR',
				'cep2' => '4127304',
				'estado_cod' => 18,
				'cep' => '87890-000',
			),
			49 => 
			array (
				'id' => 6761,
				'nome' => 'Terra Roxa',
				'uf' => 'PR',
				'cep2' => '4127403',
				'estado_cod' => 18,
				'cep' => '85990-000',
			),
			50 => 
			array (
				'id' => 6762,
				'nome' => 'Tibagi',
				'uf' => 'PR',
				'cep2' => '4127502',
				'estado_cod' => 18,
				'cep' => '84300-000',
			),
			51 => 
			array (
				'id' => 6763,
				'nome' => 'Tijucas do Sul',
				'uf' => 'PR',
				'cep2' => '4127601',
				'estado_cod' => 18,
				'cep' => '83190-000',
			),
			52 => 
			array (
				'id' => 6770,
				'nome' => 'Toledo',
				'uf' => 'PR',
				'cep2' => '4127700',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			53 => 
			array (
				'id' => 6772,
				'nome' => 'Tomazina',
				'uf' => 'PR',
				'cep2' => '4127809',
				'estado_cod' => 18,
				'cep' => '84935-000',
			),
			54 => 
			array (
				'id' => 6774,
				'nome' => 'Três Barras do Paraná',
				'uf' => 'PR',
				'cep2' => '4127858',
				'estado_cod' => 18,
				'cep' => '85485-000',
			),
			55 => 
			array (
				'id' => 6786,
				'nome' => 'Tunas do Paraná',
				'uf' => 'PR',
				'cep2' => '4127882',
				'estado_cod' => 18,
				'cep' => '83480-000',
			),
			56 => 
			array (
				'id' => 6787,
				'nome' => 'Tuneiras do Oeste',
				'uf' => 'PR',
				'cep2' => '4127908',
				'estado_cod' => 18,
				'cep' => '87450-000',
			),
			57 => 
			array (
				'id' => 6788,
				'nome' => 'Tupãssi',
				'uf' => 'PR',
				'cep2' => '4127957',
				'estado_cod' => 18,
				'cep' => '85945-000',
			),
			58 => 
			array (
				'id' => 6790,
				'nome' => 'Turvo',
				'uf' => 'PR',
				'cep2' => '4127965',
				'estado_cod' => 18,
				'cep' => '85150-000',
			),
			59 => 
			array (
				'id' => 6793,
				'nome' => 'Ubiratã',
				'uf' => 'PR',
				'cep2' => '4128005',
				'estado_cod' => 18,
				'cep' => '85440-000',
			),
			60 => 
			array (
				'id' => 6794,
				'nome' => 'Umuarama',
				'uf' => 'PR',
				'cep2' => '4128104',
				'estado_cod' => 18,
				'cep' => 'LOC',
			),
			61 => 
			array (
				'id' => 6796,
				'nome' => 'União da Vitória',
				'uf' => 'PR',
				'cep2' => '4128203',
				'estado_cod' => 18,
				'cep' => '84600-000',
			),
			62 => 
			array (
				'id' => 6798,
				'nome' => 'Uniflor',
				'uf' => 'PR',
				'cep2' => '4128302',
				'estado_cod' => 18,
				'cep' => '87640-000',
			),
			63 => 
			array (
				'id' => 6799,
				'nome' => 'Uraí',
				'uf' => 'PR',
				'cep2' => '4128401',
				'estado_cod' => 18,
				'cep' => '86280-000',
			),
			64 => 
			array (
				'id' => 6806,
				'nome' => 'Ventania',
				'uf' => 'PR',
				'cep2' => '4128534',
				'estado_cod' => 18,
				'cep' => '84345-000',
			),
			65 => 
			array (
				'id' => 6807,
				'nome' => 'Vera Cruz do Oeste',
				'uf' => 'PR',
				'cep2' => '4128559',
				'estado_cod' => 18,
				'cep' => '85845-000',
			),
			66 => 
			array (
				'id' => 6809,
				'nome' => 'Verê',
				'uf' => 'PR',
				'cep2' => '4128609',
				'estado_cod' => 18,
				'cep' => '85585-000',
			),
			67 => 
			array (
				'id' => 6812,
				'nome' => 'Alto Paraíso',
				'uf' => 'PR',
				'cep2' => '',
				'estado_cod' => 18,
				'cep' => '87528-000',
			),
			68 => 
			array (
				'id' => 6827,
				'nome' => 'Virmond',
				'uf' => 'PR',
				'cep2' => '4128658',
				'estado_cod' => 18,
				'cep' => '85390-000',
			),
			69 => 
			array (
				'id' => 6832,
				'nome' => 'Vitorino',
				'uf' => 'PR',
				'cep2' => '4128708',
				'estado_cod' => 18,
				'cep' => '85520-000',
			),
			70 => 
			array (
				'id' => 6834,
				'nome' => 'Wenceslau Braz',
				'uf' => 'PR',
				'cep2' => '4128500',
				'estado_cod' => 18,
				'cep' => '84950-000',
			),
			71 => 
			array (
				'id' => 6835,
				'nome' => 'Xambrê',
				'uf' => 'PR',
				'cep2' => '4128807',
				'estado_cod' => 18,
				'cep' => '87535-000',
			),
			72 => 
			array (
				'id' => 6838,
				'nome' => 'Quarto Centenário',
				'uf' => 'PR',
				'cep2' => '4120655',
				'estado_cod' => 18,
				'cep' => '87365-000',
			),
			73 => 
			array (
				'id' => 6845,
				'nome' => 'Angra dos Reis',
				'uf' => 'RJ',
				'cep2' => '3300100',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			74 => 
			array (
				'id' => 6847,
				'nome' => 'Aperibé',
				'uf' => 'RJ',
				'cep2' => '3300159',
				'estado_cod' => 19,
				'cep' => '28495-000',
			),
			75 => 
			array (
				'id' => 6848,
				'nome' => 'Araruama',
				'uf' => 'RJ',
				'cep2' => '3300209',
				'estado_cod' => 19,
				'cep' => '28970-000',
			),
			76 => 
			array (
				'id' => 6849,
				'nome' => 'Areal',
				'uf' => 'RJ',
				'cep2' => '3300225',
				'estado_cod' => 19,
				'cep' => '25845-000',
			),
			77 => 
			array (
				'id' => 6850,
				'nome' => 'Armação dos Búzios',
				'uf' => 'RJ',
				'cep2' => '',
				'estado_cod' => 19,
				'cep' => '28950-000',
			),
			78 => 
			array (
				'id' => 6851,
				'nome' => 'Arraial do Cabo',
				'uf' => 'RJ',
				'cep2' => '3300258',
				'estado_cod' => 19,
				'cep' => '28930-000',
			),
			79 => 
			array (
				'id' => 6862,
				'nome' => 'Barra do Piraí',
				'uf' => 'RJ',
				'cep2' => '3300308',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			80 => 
			array (
				'id' => 6863,
				'nome' => 'Barra Mansa',
				'uf' => 'RJ',
				'cep2' => '3300407',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			81 => 
			array (
				'id' => 6865,
				'nome' => 'Belford Roxo',
				'uf' => 'RJ',
				'cep2' => '3300456',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			82 => 
			array (
				'id' => 6870,
				'nome' => 'Bom Jardim',
				'uf' => 'RJ',
				'cep2' => '3300506',
				'estado_cod' => 19,
				'cep' => '28660-000',
			),
			83 => 
			array (
				'id' => 6871,
				'nome' => 'Bom Jesus do Itabapoana',
				'uf' => 'RJ',
				'cep2' => '3300605',
				'estado_cod' => 19,
				'cep' => '28360-000',
			),
			84 => 
			array (
				'id' => 6873,
				'nome' => 'Cabo Frio',
				'uf' => 'RJ',
				'cep2' => '3300704',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			85 => 
			array (
				'id' => 6875,
				'nome' => 'Cachoeiras de Macacu',
				'uf' => 'RJ',
				'cep2' => '3300803',
				'estado_cod' => 19,
				'cep' => '28680-000',
			),
			86 => 
			array (
				'id' => 6879,
				'nome' => 'Cambuci',
				'uf' => 'RJ',
				'cep2' => '3300902',
				'estado_cod' => 19,
				'cep' => '28430-000',
			),
			87 => 
			array (
				'id' => 6881,
				'nome' => 'Campos dos Goytacazes',
				'uf' => 'RJ',
				'cep2' => '3301009',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			88 => 
			array (
				'id' => 6883,
				'nome' => 'Cantagalo',
				'uf' => 'RJ',
				'cep2' => '3301108',
				'estado_cod' => 19,
				'cep' => '28500-000',
			),
			89 => 
			array (
				'id' => 6885,
				'nome' => 'Carapebus',
				'uf' => 'RJ',
				'cep2' => '3300936',
				'estado_cod' => 19,
				'cep' => '27998-000',
			),
			90 => 
			array (
				'id' => 6886,
				'nome' => 'Cardoso Moreira',
				'uf' => 'RJ',
				'cep2' => '3301157',
				'estado_cod' => 19,
				'cep' => '28180-000',
			),
			91 => 
			array (
				'id' => 6887,
				'nome' => 'Carmo',
				'uf' => 'RJ',
				'cep2' => '3301207',
				'estado_cod' => 19,
				'cep' => '28640-000',
			),
			92 => 
			array (
				'id' => 6889,
				'nome' => 'Casimiro de Abreu',
				'uf' => 'RJ',
				'cep2' => '3301306',
				'estado_cod' => 19,
				'cep' => '28860-000',
			),
			93 => 
			array (
				'id' => 6893,
				'nome' => 'Comendador Levy Gasparian',
				'uf' => 'RJ',
				'cep2' => '3300951',
				'estado_cod' => 19,
				'cep' => '25870-000',
			),
			94 => 
			array (
				'id' => 6896,
				'nome' => 'Conceição de Macabu',
				'uf' => 'RJ',
				'cep2' => '3301405',
				'estado_cod' => 19,
				'cep' => '28740-000',
			),
			95 => 
			array (
				'id' => 6900,
				'nome' => 'Cordeiro',
				'uf' => 'RJ',
				'cep2' => '3301504',
				'estado_cod' => 19,
				'cep' => '28540-000',
			),
			96 => 
			array (
				'id' => 6911,
				'nome' => 'Duas Barras',
				'uf' => 'RJ',
				'cep2' => '3301603',
				'estado_cod' => 19,
				'cep' => '28650-000',
			),
			97 => 
			array (
				'id' => 6912,
				'nome' => 'Duque de Caxias',
				'uf' => 'RJ',
				'cep2' => '3301702',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			98 => 
			array (
				'id' => 6914,
				'nome' => 'Engenheiro Paulo de Frontin',
				'uf' => 'RJ',
				'cep2' => '3301801',
				'estado_cod' => 19,
				'cep' => '26650-000',
			),
			99 => 
			array (
				'id' => 6927,
				'nome' => 'Guapimirim',
				'uf' => 'RJ',
				'cep2' => '3301850',
				'estado_cod' => 19,
				'cep' => '25940-000',
			),
			100 => 
			array (
				'id' => 6932,
				'nome' => 'Iguaba Grande',
				'uf' => 'RJ',
				'cep2' => '3301876',
				'estado_cod' => 19,
				'cep' => '28960-000',
			),
			101 => 
			array (
				'id' => 6941,
				'nome' => 'Itaboraí',
				'uf' => 'RJ',
				'cep2' => '3301900',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			102 => 
			array (
				'id' => 6943,
				'nome' => 'Itaguaí',
				'uf' => 'RJ',
				'cep2' => '3302007',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			103 => 
			array (
				'id' => 6947,
				'nome' => 'Italva',
				'uf' => 'RJ',
				'cep2' => '3302056',
				'estado_cod' => 19,
				'cep' => '28250-000',
			),
			104 => 
			array (
				'id' => 6949,
				'nome' => 'Itaocara',
				'uf' => 'RJ',
				'cep2' => '3302106',
				'estado_cod' => 19,
				'cep' => '28570-000',
			),
			105 => 
			array (
				'id' => 6950,
				'nome' => 'Itaperuna',
				'uf' => 'RJ',
				'cep2' => '3302205',
				'estado_cod' => 19,
				'cep' => '28300-000',
			),
			106 => 
			array (
				'id' => 6951,
				'nome' => 'Itatiaia',
				'uf' => 'RJ',
				'cep2' => '3302254',
				'estado_cod' => 19,
				'cep' => '27580-000',
			),
			107 => 
			array (
				'id' => 6955,
				'nome' => 'Japeri',
				'uf' => 'RJ',
				'cep2' => '3302270',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			108 => 
			array (
				'id' => 6957,
				'nome' => 'Laje do Muriaé',
				'uf' => 'RJ',
				'cep2' => '3302304',
				'estado_cod' => 19,
				'cep' => '28350-000',
			),
			109 => 
			array (
				'id' => 6962,
				'nome' => 'Macaé',
				'uf' => 'RJ',
				'cep2' => '3302403',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			110 => 
			array (
				'id' => 6963,
				'nome' => 'Macuco',
				'uf' => 'RJ',
				'cep2' => '3302452',
				'estado_cod' => 19,
				'cep' => '28545-000',
			),
			111 => 
			array (
				'id' => 6964,
				'nome' => 'Magé',
				'uf' => 'RJ',
				'cep2' => '3302502',
				'estado_cod' => 19,
				'cep' => '25900-000',
			),
			112 => 
			array (
				'id' => 6966,
				'nome' => 'Mangaratiba',
				'uf' => 'RJ',
				'cep2' => '3302601',
				'estado_cod' => 19,
				'cep' => '23860-000',
			),
			113 => 
			array (
				'id' => 6971,
				'nome' => 'Maricá',
				'uf' => 'RJ',
				'cep2' => '3302700',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			114 => 
			array (
				'id' => 6972,
				'nome' => 'Mendes',
				'uf' => 'RJ',
				'cep2' => '3302809',
				'estado_cod' => 19,
				'cep' => '26700-000',
			),
			115 => 
			array (
				'id' => 6973,
				'nome' => 'Mesquita',
				'uf' => 'RJ',
				'cep2' => '3302858',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			116 => 
			array (
				'id' => 6974,
				'nome' => 'Miguel Pereira',
				'uf' => 'RJ',
				'cep2' => '3302908',
				'estado_cod' => 19,
				'cep' => '26900-000',
			),
			117 => 
			array (
				'id' => 6975,
				'nome' => 'Miracema',
				'uf' => 'RJ',
				'cep2' => '3303005',
				'estado_cod' => 19,
				'cep' => '28460-000',
			),
			118 => 
			array (
				'id' => 6985,
				'nome' => 'Natividade',
				'uf' => 'RJ',
				'cep2' => '3303104',
				'estado_cod' => 19,
				'cep' => '28380-000',
			),
			119 => 
			array (
				'id' => 6988,
				'nome' => 'Nilópolis',
				'uf' => 'RJ',
				'cep2' => '3303203',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			120 => 
			array (
				'id' => 6989,
				'nome' => 'Niterói',
				'uf' => 'RJ',
				'cep2' => '3303302',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			121 => 
			array (
				'id' => 6993,
				'nome' => 'Nova Friburgo',
				'uf' => 'RJ',
				'cep2' => '3303401',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			122 => 
			array (
				'id' => 6994,
				'nome' => 'Nova Iguaçu',
				'uf' => 'RJ',
				'cep2' => '3303500',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			123 => 
			array (
				'id' => 6999,
				'nome' => 'Paracambi',
				'uf' => 'RJ',
				'cep2' => '3303609',
				'estado_cod' => 19,
				'cep' => '26600-000',
			),
			124 => 
			array (
				'id' => 7000,
				'nome' => 'Paraíba do Sul',
				'uf' => 'RJ',
				'cep2' => '3303708',
				'estado_cod' => 19,
				'cep' => '25850-000',
			),
			125 => 
			array (
				'id' => 7004,
				'nome' => 'Parati',
				'uf' => 'RJ',
				'cep2' => '3303807',
				'estado_cod' => 19,
				'cep' => '23970-000',
			),
			126 => 
			array (
				'id' => 7007,
				'nome' => 'Paty do Alferes',
				'uf' => 'RJ',
				'cep2' => '3303856',
				'estado_cod' => 19,
				'cep' => '26950-000',
			),
			127 => 
			array (
				'id' => 7012,
				'nome' => 'Petrópolis',
				'uf' => 'RJ',
				'cep2' => '3303906',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			128 => 
			array (
				'id' => 7015,
				'nome' => 'Pinheiral',
				'uf' => 'RJ',
				'cep2' => '3303955',
				'estado_cod' => 19,
				'cep' => '27197-000',
			),
			129 => 
			array (
				'id' => 7017,
				'nome' => 'Piraí',
				'uf' => 'RJ',
				'cep2' => '3304003',
				'estado_cod' => 19,
				'cep' => '27175-000',
			),
			130 => 
			array (
				'id' => 7020,
				'nome' => 'Porciúncula',
				'uf' => 'RJ',
				'cep2' => '3304102',
				'estado_cod' => 19,
				'cep' => '28390-000',
			),
			131 => 
			array (
				'id' => 7023,
				'nome' => 'Porto Real',
				'uf' => 'RJ',
				'cep2' => '3304110',
				'estado_cod' => 19,
				'cep' => '27570-000',
			),
			132 => 
			array (
				'id' => 7030,
				'nome' => 'Quatis',
				'uf' => 'RJ',
				'cep2' => '3304128',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			133 => 
			array (
				'id' => 7031,
				'nome' => 'Queimados',
				'uf' => 'RJ',
				'cep2' => '3304144',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			134 => 
			array (
				'id' => 7032,
				'nome' => 'Quissamã',
				'uf' => 'RJ',
				'cep2' => '3304151',
				'estado_cod' => 19,
				'cep' => '28735-000',
			),
			135 => 
			array (
				'id' => 7035,
				'nome' => 'Resende',
				'uf' => 'RJ',
				'cep2' => '3304201',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			136 => 
			array (
				'id' => 7039,
				'nome' => 'Rio Bonito',
				'uf' => 'RJ',
				'cep2' => '3304300',
				'estado_cod' => 19,
				'cep' => '28800-000',
			),
			137 => 
			array (
				'id' => 7040,
				'nome' => 'Rio Claro',
				'uf' => 'RJ',
				'cep2' => '3304409',
				'estado_cod' => 19,
				'cep' => '27460-000',
			),
			138 => 
			array (
				'id' => 7041,
				'nome' => 'Rio das Flores',
				'uf' => 'RJ',
				'cep2' => '3304508',
				'estado_cod' => 19,
				'cep' => '27660-000',
			),
			139 => 
			array (
				'id' => 7042,
				'nome' => 'Rio das Ostras',
				'uf' => 'RJ',
				'cep2' => '3304524',
				'estado_cod' => 19,
				'cep' => '28890-000',
			),
			140 => 
			array (
				'id' => 7043,
				'nome' => 'Rio de Janeiro',
				'uf' => 'RJ',
				'cep2' => '3304557',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			141 => 
			array (
				'id' => 7057,
				'nome' => 'Santa Maria Madalena',
				'uf' => 'RJ',
				'cep2' => '3304607',
				'estado_cod' => 19,
				'cep' => '28770-000',
			),
			142 => 
			array (
				'id' => 7062,
				'nome' => 'Santo Antônio de Pádua',
				'uf' => 'RJ',
				'cep2' => '3304706',
				'estado_cod' => 19,
				'cep' => '28470-000',
			),
			143 => 
			array (
				'id' => 7065,
				'nome' => 'São Fidélis',
				'uf' => 'RJ',
				'cep2' => '3304805',
				'estado_cod' => 19,
				'cep' => '28400-000',
			),
			144 => 
			array (
				'id' => 7066,
				'nome' => 'São Francisco de Itabapoana',
				'uf' => 'RJ',
				'cep2' => '3304755',
				'estado_cod' => 19,
				'cep' => '28230-000',
			),
			145 => 
			array (
				'id' => 7067,
				'nome' => 'São Gonçalo',
				'uf' => 'RJ',
				'cep2' => '3304904',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			146 => 
			array (
				'id' => 7068,
				'nome' => 'São João da Barra',
				'uf' => 'RJ',
				'cep2' => '3305000',
				'estado_cod' => 19,
				'cep' => '28200-000',
			),
			147 => 
			array (
				'id' => 7069,
				'nome' => 'São João de Meriti',
				'uf' => 'RJ',
				'cep2' => '3305109',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			148 => 
			array (
				'id' => 7073,
				'nome' => 'São José de Ubá',
				'uf' => 'RJ',
				'cep2' => '3305133',
				'estado_cod' => 19,
				'cep' => '28455-000',
			),
			149 => 
			array (
				'id' => 7076,
				'nome' => 'São José do Vale do Rio Preto',
				'uf' => 'RJ',
				'cep2' => '3305158',
				'estado_cod' => 19,
				'cep' => '25780-000',
			),
			150 => 
			array (
				'id' => 7078,
				'nome' => 'São Pedro da Aldeia',
				'uf' => 'RJ',
				'cep2' => '3305208',
				'estado_cod' => 19,
				'cep' => '28940-000',
			),
			151 => 
			array (
				'id' => 7080,
				'nome' => 'São Sebastião do Alto',
				'uf' => 'RJ',
				'cep2' => '3305307',
				'estado_cod' => 19,
				'cep' => '28550-000',
			),
			152 => 
			array (
				'id' => 7084,
				'nome' => 'Sapucaia',
				'uf' => 'RJ',
				'cep2' => '3305406',
				'estado_cod' => 19,
				'cep' => '25880-000',
			),
			153 => 
			array (
				'id' => 7085,
				'nome' => 'Saquarema',
				'uf' => 'RJ',
				'cep2' => '3305505',
				'estado_cod' => 19,
				'cep' => '28990-000',
			),
			154 => 
			array (
				'id' => 7088,
				'nome' => 'Seropédica',
				'uf' => 'RJ',
				'cep2' => '3305554',
				'estado_cod' => 19,
				'cep' => '23890-000',
			),
			155 => 
			array (
				'id' => 7091,
				'nome' => 'Silva Jardim',
				'uf' => 'RJ',
				'cep2' => '3305604',
				'estado_cod' => 19,
				'cep' => '28820-000',
			),
			156 => 
			array (
				'id' => 7095,
				'nome' => 'Sumidouro',
				'uf' => 'RJ',
				'cep2' => '3305703',
				'estado_cod' => 19,
				'cep' => '28637-000',
			),
			157 => 
			array (
				'id' => 7099,
				'nome' => 'Tanguá',
				'uf' => 'RJ',
				'cep2' => '3305752',
				'estado_cod' => 19,
				'cep' => '24890-000',
			),
			158 => 
			array (
				'id' => 7102,
				'nome' => 'Teresópolis',
				'uf' => 'RJ',
				'cep2' => '3305802',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			159 => 
			array (
				'id' => 7104,
				'nome' => 'Trajano de Morais',
				'uf' => 'RJ',
				'cep2' => '3305901',
				'estado_cod' => 19,
				'cep' => '28750-000',
			),
			160 => 
			array (
				'id' => 7107,
				'nome' => 'Três Rios',
				'uf' => 'RJ',
				'cep2' => '3306008',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			161 => 
			array (
				'id' => 7110,
				'nome' => 'Valença',
				'uf' => 'RJ',
				'cep2' => '3306107',
				'estado_cod' => 19,
				'cep' => '27600-000',
			),
			162 => 
			array (
				'id' => 7112,
				'nome' => 'Varre-Sai',
				'uf' => 'RJ',
				'cep2' => '3306156',
				'estado_cod' => 19,
				'cep' => '28375-000',
			),
			163 => 
			array (
				'id' => 7113,
				'nome' => 'Vassouras',
				'uf' => 'RJ',
				'cep2' => '3306206',
				'estado_cod' => 19,
				'cep' => '27700-000',
			),
			164 => 
			array (
				'id' => 7121,
				'nome' => 'Volta Redonda',
				'uf' => 'RJ',
				'cep2' => '3306305',
				'estado_cod' => 19,
				'cep' => 'LOC',
			),
			165 => 
			array (
				'id' => 7124,
				'nome' => 'Acari',
				'uf' => 'RN',
				'cep2' => '2400109',
				'estado_cod' => 20,
				'cep' => '59370-000',
			),
			166 => 
			array (
				'id' => 7125,
				'nome' => 'Açu',
				'uf' => 'RN',
				'cep2' => '2400208',
				'estado_cod' => 20,
				'cep' => '59650-000',
			),
			167 => 
			array (
				'id' => 7126,
				'nome' => 'Afonso Bezerra',
				'uf' => 'RN',
				'cep2' => '2400307',
				'estado_cod' => 20,
				'cep' => '59510-000',
			),
			168 => 
			array (
				'id' => 7127,
				'nome' => 'Água Nova',
				'uf' => 'RN',
				'cep2' => '2400406',
				'estado_cod' => 20,
				'cep' => '59995-000',
			),
			169 => 
			array (
				'id' => 7128,
				'nome' => 'Alexandria',
				'uf' => 'RN',
				'cep2' => '2400505',
				'estado_cod' => 20,
				'cep' => '59965-000',
			),
			170 => 
			array (
				'id' => 7129,
				'nome' => 'Almino Afonso',
				'uf' => 'RN',
				'cep2' => '2400604',
				'estado_cod' => 20,
				'cep' => '59760-000',
			),
			171 => 
			array (
				'id' => 7130,
				'nome' => 'Alto do Rodrigues',
				'uf' => 'RN',
				'cep2' => '2400703',
				'estado_cod' => 20,
				'cep' => '59507-000',
			),
			172 => 
			array (
				'id' => 7131,
				'nome' => 'Angicos',
				'uf' => 'RN',
				'cep2' => '2400802',
				'estado_cod' => 20,
				'cep' => '59515-000',
			),
			173 => 
			array (
				'id' => 7132,
				'nome' => 'Antônio Martins',
				'uf' => 'RN',
				'cep2' => '2400901',
				'estado_cod' => 20,
				'cep' => '59870-000',
			),
			174 => 
			array (
				'id' => 7133,
				'nome' => 'Apodi',
				'uf' => 'RN',
				'cep2' => '2401008',
				'estado_cod' => 20,
				'cep' => '59700-000',
			),
			175 => 
			array (
				'id' => 7134,
				'nome' => 'Areia Branca',
				'uf' => 'RN',
				'cep2' => '2401107',
				'estado_cod' => 20,
				'cep' => '59655-000',
			),
			176 => 
			array (
				'id' => 7135,
				'nome' => 'Arez',
				'uf' => 'RN',
				'cep2' => '',
				'estado_cod' => 20,
				'cep' => '59170-000',
			),
			177 => 
			array (
				'id' => 7136,
				'nome' => 'Baía Formosa',
				'uf' => 'RN',
				'cep2' => '2401404',
				'estado_cod' => 20,
				'cep' => '59194-000',
			),
			178 => 
			array (
				'id' => 7138,
				'nome' => 'Baraúna',
				'uf' => 'RN',
				'cep2' => '2401453',
				'estado_cod' => 20,
				'cep' => '59695-000',
			),
			179 => 
			array (
				'id' => 7139,
				'nome' => 'Barcelona',
				'uf' => 'RN',
				'cep2' => '2401503',
				'estado_cod' => 20,
				'cep' => '59410-000',
			),
			180 => 
			array (
				'id' => 7141,
				'nome' => 'Bento Fernandes',
				'uf' => 'RN',
				'cep2' => '2401602',
				'estado_cod' => 20,
				'cep' => '59555-000',
			),
			181 => 
			array (
				'id' => 7142,
				'nome' => 'Boa Saúde',
				'uf' => 'RN',
				'cep2' => '',
				'estado_cod' => 20,
				'cep' => '59260-000',
			),
			182 => 
			array (
				'id' => 7143,
				'nome' => 'Bodó',
				'uf' => 'RN',
				'cep2' => '2401651',
				'estado_cod' => 20,
				'cep' => '59528-000',
			),
			183 => 
			array (
				'id' => 7144,
				'nome' => 'Bom Jesus',
				'uf' => 'RN',
				'cep2' => '2401701',
				'estado_cod' => 20,
				'cep' => '59270-000',
			),
			184 => 
			array (
				'id' => 7145,
				'nome' => 'Brejinho',
				'uf' => 'RN',
				'cep2' => '2401800',
				'estado_cod' => 20,
				'cep' => '59219-000',
			),
			185 => 
			array (
				'id' => 7146,
				'nome' => 'Caiçara do Norte',
				'uf' => 'RN',
				'cep2' => '2401859',
				'estado_cod' => 20,
				'cep' => '59592-000',
			),
			186 => 
			array (
				'id' => 7147,
				'nome' => 'Caiçara do Rio do Vento',
				'uf' => 'RN',
				'cep2' => '2401909',
				'estado_cod' => 20,
				'cep' => '59540-000',
			),
			187 => 
			array (
				'id' => 7148,
				'nome' => 'Caicó',
				'uf' => 'RN',
				'cep2' => '2402006',
				'estado_cod' => 20,
				'cep' => '59300-000',
			),
			188 => 
			array (
				'id' => 7149,
				'nome' => 'Campo Grande',
				'uf' => 'RN',
				'cep2' => '2701506',
				'estado_cod' => 20,
				'cep' => '59680-000',
			),
			189 => 
			array (
				'id' => 7150,
				'nome' => 'Campo Redondo',
				'uf' => 'RN',
				'cep2' => '2402105',
				'estado_cod' => 20,
				'cep' => '59230-000',
			),
			190 => 
			array (
				'id' => 7151,
				'nome' => 'Canguaretama',
				'uf' => 'RN',
				'cep2' => '2402204',
				'estado_cod' => 20,
				'cep' => '59190-000',
			),
			191 => 
			array (
				'id' => 7152,
				'nome' => 'Caraúbas',
				'uf' => 'RN',
				'cep2' => '2402303',
				'estado_cod' => 20,
				'cep' => '59780-000',
			),
			192 => 
			array (
				'id' => 7153,
				'nome' => 'Carnaúba dos Dantas',
				'uf' => 'RN',
				'cep2' => '2402402',
				'estado_cod' => 20,
				'cep' => '59374-000',
			),
			193 => 
			array (
				'id' => 7154,
				'nome' => 'Carnaubais',
				'uf' => 'RN',
				'cep2' => '2402501',
				'estado_cod' => 20,
				'cep' => '59665-000',
			),
			194 => 
			array (
				'id' => 7155,
				'nome' => 'Ceará-Mirim',
				'uf' => 'RN',
				'cep2' => '2402600',
				'estado_cod' => 20,
				'cep' => '59570-000',
			),
			195 => 
			array (
				'id' => 7156,
				'nome' => 'Cerro Corá',
				'uf' => 'RN',
				'cep2' => '2402709',
				'estado_cod' => 20,
				'cep' => '59395-000',
			),
			196 => 
			array (
				'id' => 7157,
				'nome' => 'Coronel Ezequiel',
				'uf' => 'RN',
				'cep2' => '2402808',
				'estado_cod' => 20,
				'cep' => '59220-000',
			),
			197 => 
			array (
				'id' => 7158,
				'nome' => 'Coronel João Pessoa',
				'uf' => 'RN',
				'cep2' => '2402907',
				'estado_cod' => 20,
				'cep' => '59930-000',
			),
			198 => 
			array (
				'id' => 7160,
				'nome' => 'Cruzeta',
				'uf' => 'RN',
				'cep2' => '2403004',
				'estado_cod' => 20,
				'cep' => '59375-000',
			),
			199 => 
			array (
				'id' => 7161,
				'nome' => 'Currais Novos',
				'uf' => 'RN',
				'cep2' => '2403103',
				'estado_cod' => 20,
				'cep' => '59380-000',
			),
			200 => 
			array (
				'id' => 7162,
				'nome' => 'Doutor Severiano',
				'uf' => 'RN',
				'cep2' => '2403202',
				'estado_cod' => 20,
				'cep' => '59910-000',
			),
			201 => 
			array (
				'id' => 7163,
				'nome' => 'Encanto',
				'uf' => 'RN',
				'cep2' => '2403301',
				'estado_cod' => 20,
				'cep' => '59905-000',
			),
			202 => 
			array (
				'id' => 7164,
				'nome' => 'Equador',
				'uf' => 'RN',
				'cep2' => '2403400',
				'estado_cod' => 20,
				'cep' => '59355-000',
			),
			203 => 
			array (
				'id' => 7165,
				'nome' => 'Espírito Santo',
				'uf' => 'RN',
				'cep2' => '2403509',
				'estado_cod' => 20,
				'cep' => '59180-000',
			),
			204 => 
			array (
				'id' => 7166,
				'nome' => 'Paraú',
				'uf' => 'RN',
				'cep2' => '',
				'estado_cod' => 20,
				'cep' => '59660-000',
			),
			205 => 
			array (
				'id' => 7167,
				'nome' => 'Extremoz',
				'uf' => 'RN',
				'cep2' => '2403608',
				'estado_cod' => 20,
				'cep' => '59575-000',
			),
			206 => 
			array (
				'id' => 7168,
				'nome' => 'Felipe Guerra',
				'uf' => 'RN',
				'cep2' => '2403707',
				'estado_cod' => 20,
				'cep' => '59795-000',
			),
			207 => 
			array (
				'id' => 7169,
				'nome' => 'Fernando Pedroza',
				'uf' => 'RN',
				'cep2' => '2403756',
				'estado_cod' => 20,
				'cep' => '59517-000',
			),
			208 => 
			array (
				'id' => 7171,
				'nome' => 'Florânia',
				'uf' => 'RN',
				'cep2' => '2403806',
				'estado_cod' => 20,
				'cep' => '59335-000',
			),
			209 => 
			array (
				'id' => 7172,
				'nome' => 'Francisco Dantas',
				'uf' => 'RN',
				'cep2' => '2403905',
				'estado_cod' => 20,
				'cep' => '59902-000',
			),
			210 => 
			array (
				'id' => 7173,
				'nome' => 'Frutuoso Gomes',
				'uf' => 'RN',
				'cep2' => '2404002',
				'estado_cod' => 20,
				'cep' => '59890-000',
			),
			211 => 
			array (
				'id' => 7174,
				'nome' => 'Galinhos',
				'uf' => 'RN',
				'cep2' => '2404101',
				'estado_cod' => 20,
				'cep' => '59596-000',
			),
			212 => 
			array (
				'id' => 7176,
				'nome' => 'Goianinha',
				'uf' => 'RN',
				'cep2' => '2404200',
				'estado_cod' => 20,
				'cep' => '59173-000',
			),
			213 => 
			array (
				'id' => 7177,
				'nome' => 'Governador Dix-Sept Rosado',
				'uf' => 'RN',
				'cep2' => '2404309',
				'estado_cod' => 20,
				'cep' => '59790-000',
			),
			214 => 
			array (
				'id' => 7178,
				'nome' => 'Grossos',
				'uf' => 'RN',
				'cep2' => '2404408',
				'estado_cod' => 20,
				'cep' => '59675-000',
			),
			215 => 
			array (
				'id' => 7179,
				'nome' => 'Guamaré',
				'uf' => 'RN',
				'cep2' => '2404507',
				'estado_cod' => 20,
				'cep' => '59598-000',
			),
			216 => 
			array (
				'id' => 7180,
				'nome' => 'Ielmo Marinho',
				'uf' => 'RN',
				'cep2' => '2404606',
				'estado_cod' => 20,
				'cep' => '59490-000',
			),
			217 => 
			array (
				'id' => 7182,
				'nome' => 'Ipanguaçu',
				'uf' => 'RN',
				'cep2' => '2404705',
				'estado_cod' => 20,
				'cep' => '59508-000',
			),
			218 => 
			array (
				'id' => 7184,
				'nome' => 'Ipueira',
				'uf' => 'RN',
				'cep2' => '2404804',
				'estado_cod' => 20,
				'cep' => '59315-000',
			),
			219 => 
			array (
				'id' => 7185,
				'nome' => 'Itajá',
				'uf' => 'RN',
				'cep2' => '2404853',
				'estado_cod' => 20,
				'cep' => '59513-000',
			),
			220 => 
			array (
				'id' => 7186,
				'nome' => 'Itaú',
				'uf' => 'RN',
				'cep2' => '2404903',
				'estado_cod' => 20,
				'cep' => '59855-000',
			),
			221 => 
			array (
				'id' => 7187,
				'nome' => 'Jaçanã',
				'uf' => 'RN',
				'cep2' => '2405009',
				'estado_cod' => 20,
				'cep' => '59225-000',
			),
			222 => 
			array (
				'id' => 7188,
				'nome' => 'Jandaíra',
				'uf' => 'RN',
				'cep2' => '2405108',
				'estado_cod' => 20,
				'cep' => '59594-000',
			),
			223 => 
			array (
				'id' => 7189,
				'nome' => 'Janduís',
				'uf' => 'RN',
				'cep2' => '2405207',
				'estado_cod' => 20,
				'cep' => '59690-000',
			),
			224 => 
			array (
				'id' => 7190,
				'nome' => 'Japi',
				'uf' => 'RN',
				'cep2' => '2405405',
				'estado_cod' => 20,
				'cep' => '59213-000',
			),
			225 => 
			array (
				'id' => 7191,
				'nome' => 'Jardim de Angicos',
				'uf' => 'RN',
				'cep2' => '2405504',
				'estado_cod' => 20,
				'cep' => '59544-000',
			),
			226 => 
			array (
				'id' => 7192,
				'nome' => 'Jardim de Piranhas',
				'uf' => 'RN',
				'cep2' => '2405603',
				'estado_cod' => 20,
				'cep' => '59324-000',
			),
			227 => 
			array (
				'id' => 7193,
				'nome' => 'Jardim do Seridó',
				'uf' => 'RN',
				'cep2' => '2405702',
				'estado_cod' => 20,
				'cep' => '59343-000',
			),
			228 => 
			array (
				'id' => 7194,
				'nome' => 'João Câmara',
				'uf' => 'RN',
				'cep2' => '2405801',
				'estado_cod' => 20,
				'cep' => '59550-000',
			),
			229 => 
			array (
				'id' => 7195,
				'nome' => 'João Dias',
				'uf' => 'RN',
				'cep2' => '2405900',
				'estado_cod' => 20,
				'cep' => '59880-000',
			),
			230 => 
			array (
				'id' => 7196,
				'nome' => 'José da Penha',
				'uf' => 'RN',
				'cep2' => '2406007',
				'estado_cod' => 20,
				'cep' => '59980-000',
			),
			231 => 
			array (
				'id' => 7197,
				'nome' => 'Jucurutu',
				'uf' => 'RN',
				'cep2' => '2406106',
				'estado_cod' => 20,
				'cep' => '59330-000',
			),
			232 => 
			array (
				'id' => 7198,
				'nome' => 'Jundiá',
				'uf' => 'RN',
				'cep2' => '2406155',
				'estado_cod' => 20,
				'cep' => '59188-000',
			),
			233 => 
			array (
				'id' => 7199,
				'nome' => 'Lagoa D\'Anta',
				'uf' => 'RN',
				'cep2' => '2406205',
				'estado_cod' => 20,
				'cep' => '59227-000',
			),
			234 => 
			array (
				'id' => 7200,
				'nome' => 'Lagoa de Pedras',
				'uf' => 'RN',
				'cep2' => '2406304',
				'estado_cod' => 20,
				'cep' => '59244-000',
			),
			235 => 
			array (
				'id' => 7201,
				'nome' => 'Lagoa de Velhos',
				'uf' => 'RN',
				'cep2' => '2406403',
				'estado_cod' => 20,
				'cep' => '59430-000',
			),
			236 => 
			array (
				'id' => 7202,
				'nome' => 'Lagoa Nova',
				'uf' => 'RN',
				'cep2' => '2406502',
				'estado_cod' => 20,
				'cep' => '59390-000',
			),
			237 => 
			array (
				'id' => 7203,
				'nome' => 'Lagoa Salgada',
				'uf' => 'RN',
				'cep2' => '2406601',
				'estado_cod' => 20,
				'cep' => '59247-000',
			),
			238 => 
			array (
				'id' => 7204,
				'nome' => 'Lajes',
				'uf' => 'RN',
				'cep2' => '2406700',
				'estado_cod' => 20,
				'cep' => '59535-000',
			),
			239 => 
			array (
				'id' => 7205,
				'nome' => 'Lajes Pintadas',
				'uf' => 'RN',
				'cep2' => '2406809',
				'estado_cod' => 20,
				'cep' => '59235-000',
			),
			240 => 
			array (
				'id' => 7206,
				'nome' => 'Lucrécia',
				'uf' => 'RN',
				'cep2' => '2406908',
				'estado_cod' => 20,
				'cep' => '59805-000',
			),
			241 => 
			array (
				'id' => 7207,
				'nome' => 'Luís Gomes',
				'uf' => 'RN',
				'cep2' => '2407005',
				'estado_cod' => 20,
				'cep' => '59940-000',
			),
			242 => 
			array (
				'id' => 7208,
				'nome' => 'Macaíba',
				'uf' => 'RN',
				'cep2' => '2407104',
				'estado_cod' => 20,
				'cep' => '59280-000',
			),
			243 => 
			array (
				'id' => 7209,
				'nome' => 'Macau',
				'uf' => 'RN',
				'cep2' => '2407203',
				'estado_cod' => 20,
				'cep' => '59500-000',
			),
			244 => 
			array (
				'id' => 7211,
				'nome' => 'Major Sales',
				'uf' => 'RN',
				'cep2' => '2407252',
				'estado_cod' => 20,
				'cep' => '59945-000',
			),
			245 => 
			array (
				'id' => 7212,
				'nome' => 'Marcelino Vieira',
				'uf' => 'RN',
				'cep2' => '2407302',
				'estado_cod' => 20,
				'cep' => '59970-000',
			),
			246 => 
			array (
				'id' => 7213,
				'nome' => 'Martins',
				'uf' => 'RN',
				'cep2' => '2407401',
				'estado_cod' => 20,
				'cep' => '59800-000',
			),
			247 => 
			array (
				'id' => 7215,
				'nome' => 'Maxaranguape',
				'uf' => 'RN',
				'cep2' => '2407500',
				'estado_cod' => 20,
				'cep' => '59580-000',
			),
			248 => 
			array (
				'id' => 7216,
				'nome' => 'Messias Targino',
				'uf' => 'RN',
				'cep2' => '2407609',
				'estado_cod' => 20,
				'cep' => '59775-000',
			),
			249 => 
			array (
				'id' => 7217,
				'nome' => 'Montanhas',
				'uf' => 'RN',
				'cep2' => '2407708',
				'estado_cod' => 20,
				'cep' => '59198-000',
			),
			250 => 
			array (
				'id' => 7218,
				'nome' => 'Monte Alegre',
				'uf' => 'RN',
				'cep2' => '2407807',
				'estado_cod' => 20,
				'cep' => '59182-000',
			),
			251 => 
			array (
				'id' => 7219,
				'nome' => 'Monte das Gameleiras',
				'uf' => 'RN',
				'cep2' => '2407906',
				'estado_cod' => 20,
				'cep' => '59217-000',
			),
			252 => 
			array (
				'id' => 7220,
				'nome' => 'Mossoró',
				'uf' => 'RN',
				'cep2' => '2408003',
				'estado_cod' => 20,
				'cep' => 'LOC',
			),
			253 => 
			array (
				'id' => 7221,
				'nome' => 'Natal',
				'uf' => 'RN',
				'cep2' => '2408102',
				'estado_cod' => 20,
				'cep' => 'LOC',
			),
			254 => 
			array (
				'id' => 7222,
				'nome' => 'Nísia Floresta',
				'uf' => 'RN',
				'cep2' => '2408201',
				'estado_cod' => 20,
				'cep' => '59164-000',
			),
			255 => 
			array (
				'id' => 7223,
				'nome' => 'Nova Cruz',
				'uf' => 'RN',
				'cep2' => '2408300',
				'estado_cod' => 20,
				'cep' => '59215-000',
			),
			256 => 
			array (
				'id' => 7224,
				'nome' => 'Olho-D\'Água do Borges',
				'uf' => 'RN',
				'cep2' => '',
				'estado_cod' => 20,
				'cep' => '59730-000',
			),
			257 => 
			array (
				'id' => 7225,
				'nome' => 'Ouro Branco',
				'uf' => 'RN',
				'cep2' => '2408508',
				'estado_cod' => 20,
				'cep' => '59347-000',
			),
			258 => 
			array (
				'id' => 7226,
				'nome' => 'Paraná',
				'uf' => 'RN',
				'cep2' => '2408607',
				'estado_cod' => 20,
				'cep' => '59950-000',
			),
			259 => 
			array (
				'id' => 7227,
				'nome' => 'Parazinho',
				'uf' => 'RN',
				'cep2' => '2408805',
				'estado_cod' => 20,
				'cep' => '59586-000',
			),
			260 => 
			array (
				'id' => 7228,
				'nome' => 'Parelhas',
				'uf' => 'RN',
				'cep2' => '2408904',
				'estado_cod' => 20,
				'cep' => '59360-000',
			),
			261 => 
			array (
				'id' => 7229,
				'nome' => 'Parnamirim',
				'uf' => 'RN',
				'cep2' => '2403251',
				'estado_cod' => 20,
				'cep' => 'LOC',
			),
			262 => 
			array (
				'id' => 7230,
				'nome' => 'Passa e Fica',
				'uf' => 'RN',
				'cep2' => '2409100',
				'estado_cod' => 20,
				'cep' => '59218-000',
			),
			263 => 
			array (
				'id' => 7231,
				'nome' => 'Passagem',
				'uf' => 'RN',
				'cep2' => '2409209',
				'estado_cod' => 20,
				'cep' => '59259-000',
			),
			264 => 
			array (
				'id' => 7232,
				'nome' => 'Patu',
				'uf' => 'RN',
				'cep2' => '2409308',
				'estado_cod' => 20,
				'cep' => '59770-000',
			),
			265 => 
			array (
				'id' => 7233,
				'nome' => 'Pau dos Ferros',
				'uf' => 'RN',
				'cep2' => '2409407',
				'estado_cod' => 20,
				'cep' => '59900-000',
			),
			266 => 
			array (
				'id' => 7234,
				'nome' => 'Pedra Grande',
				'uf' => 'RN',
				'cep2' => '2409506',
				'estado_cod' => 20,
				'cep' => '59588-000',
			),
			267 => 
			array (
				'id' => 7235,
				'nome' => 'Pedra Preta',
				'uf' => 'RN',
				'cep2' => '2409605',
				'estado_cod' => 20,
				'cep' => '59547-000',
			),
			268 => 
			array (
				'id' => 7236,
				'nome' => 'Pedro Avelino',
				'uf' => 'RN',
				'cep2' => '2409704',
				'estado_cod' => 20,
				'cep' => '59530-000',
			),
			269 => 
			array (
				'id' => 7237,
				'nome' => 'Pedro Velho',
				'uf' => 'RN',
				'cep2' => '2409803',
				'estado_cod' => 20,
				'cep' => '59196-000',
			),
			270 => 
			array (
				'id' => 7238,
				'nome' => 'Pendências',
				'uf' => 'RN',
				'cep2' => '2409902',
				'estado_cod' => 20,
				'cep' => '59504-000',
			),
			271 => 
			array (
				'id' => 7239,
				'nome' => 'Pilões',
				'uf' => 'RN',
				'cep2' => '2410009',
				'estado_cod' => 20,
				'cep' => '59960-000',
			),
			272 => 
			array (
				'id' => 7240,
				'nome' => 'Poço Branco',
				'uf' => 'RN',
				'cep2' => '2410108',
				'estado_cod' => 20,
				'cep' => '59560-000',
			),
			273 => 
			array (
				'id' => 7241,
				'nome' => 'Portalegre',
				'uf' => 'RN',
				'cep2' => '2410207',
				'estado_cod' => 20,
				'cep' => '59810-000',
			),
			274 => 
			array (
				'id' => 7242,
				'nome' => 'Porto do Mangue',
				'uf' => 'RN',
				'cep2' => '2410256',
				'estado_cod' => 20,
				'cep' => '59668-000',
			),
			275 => 
			array (
				'id' => 7243,
				'nome' => 'Pureza',
				'uf' => 'RN',
				'cep2' => '2410405',
				'estado_cod' => 20,
				'cep' => '59582-000',
			),
			276 => 
			array (
				'id' => 7244,
				'nome' => 'Rafael Fernandes',
				'uf' => 'RN',
				'cep2' => '2410504',
				'estado_cod' => 20,
				'cep' => '59990-000',
			),
			277 => 
			array (
				'id' => 7245,
				'nome' => 'Rafael Godeiro',
				'uf' => 'RN',
				'cep2' => '2410603',
				'estado_cod' => 20,
				'cep' => '59740-000',
			),
			278 => 
			array (
				'id' => 7246,
				'nome' => 'Riacho da Cruz',
				'uf' => 'RN',
				'cep2' => '2410702',
				'estado_cod' => 20,
				'cep' => '59820-000',
			),
			279 => 
			array (
				'id' => 7247,
				'nome' => 'Riacho de Santana',
				'uf' => 'RN',
				'cep2' => '2410801',
				'estado_cod' => 20,
				'cep' => '59987-000',
			),
			280 => 
			array (
				'id' => 7248,
				'nome' => 'Riachuelo',
				'uf' => 'RN',
				'cep2' => '2410900',
				'estado_cod' => 20,
				'cep' => '59470-000',
			),
			281 => 
			array (
				'id' => 7249,
				'nome' => 'Rio do Fogo',
				'uf' => 'RN',
				'cep2' => '2408953',
				'estado_cod' => 20,
				'cep' => '59578-000',
			),
			282 => 
			array (
				'id' => 7250,
				'nome' => 'Rodolfo Fernandes',
				'uf' => 'RN',
				'cep2' => '2411007',
				'estado_cod' => 20,
				'cep' => '59830-000',
			),
			283 => 
			array (
				'id' => 7252,
				'nome' => 'Ruy Barbosa',
				'uf' => 'RN',
				'cep2' => '2411106',
				'estado_cod' => 20,
				'cep' => '59420-000',
			),
			284 => 
			array (
				'id' => 7254,
				'nome' => 'Santa Cruz',
				'uf' => 'RN',
				'cep2' => '2411205',
				'estado_cod' => 20,
				'cep' => '59200-000',
			),
			285 => 
			array (
				'id' => 7256,
				'nome' => 'Santa Maria',
				'uf' => 'RN',
				'cep2' => '2409332',
				'estado_cod' => 20,
				'cep' => '59464-000',
			),
			286 => 
			array (
				'id' => 7258,
				'nome' => 'Santana do Matos',
				'uf' => 'RN',
				'cep2' => '2411403',
				'estado_cod' => 20,
				'cep' => '59520-000',
			),
			287 => 
			array (
				'id' => 7259,
				'nome' => 'Santana do Seridó',
				'uf' => 'RN',
				'cep2' => '2411429',
				'estado_cod' => 20,
				'cep' => '59350-000',
			),
			288 => 
			array (
				'id' => 7260,
				'nome' => 'Santo Antônio',
				'uf' => 'RN',
				'cep2' => '2411502',
				'estado_cod' => 20,
				'cep' => '59255-000',
			),
			289 => 
			array (
				'id' => 7262,
				'nome' => 'São Bento do Norte',
				'uf' => 'RN',
				'cep2' => '2411601',
				'estado_cod' => 20,
				'cep' => '59590-000',
			),
			290 => 
			array (
				'id' => 7263,
				'nome' => 'São Bento do Trairi',
				'uf' => 'RN',
				'cep2' => '2411700',
				'estado_cod' => 20,
				'cep' => '59210-000',
			),
			291 => 
			array (
				'id' => 7265,
				'nome' => 'São Fernando',
				'uf' => 'RN',
				'cep2' => '2411809',
				'estado_cod' => 20,
				'cep' => '59327-000',
			),
			292 => 
			array (
				'id' => 7266,
				'nome' => 'São Francisco do Oeste',
				'uf' => 'RN',
				'cep2' => '2411908',
				'estado_cod' => 20,
				'cep' => '59908-000',
			),
			293 => 
			array (
				'id' => 7268,
				'nome' => 'São Gonçalo do Amarante',
				'uf' => 'RN',
				'cep2' => '2412005',
				'estado_cod' => 20,
				'cep' => '59290-000',
			),
			294 => 
			array (
				'id' => 7269,
				'nome' => 'São João do Sabugi',
				'uf' => 'RN',
				'cep2' => '2412104',
				'estado_cod' => 20,
				'cep' => '59310-000',
			),
			295 => 
			array (
				'id' => 7271,
				'nome' => 'São José de Mipibu',
				'uf' => 'RN',
				'cep2' => '2412203',
				'estado_cod' => 20,
				'cep' => '59162-000',
			),
			296 => 
			array (
				'id' => 7272,
				'nome' => 'São José do Campestre',
				'uf' => 'RN',
				'cep2' => '2412302',
				'estado_cod' => 20,
				'cep' => '59275-000',
			),
			297 => 
			array (
				'id' => 7273,
				'nome' => 'São José do Seridó',
				'uf' => 'RN',
				'cep2' => '2412401',
				'estado_cod' => 20,
				'cep' => '59378-000',
			),
			298 => 
			array (
				'id' => 7274,
				'nome' => 'São Miguel',
				'uf' => 'RN',
				'cep2' => '2412500',
				'estado_cod' => 20,
				'cep' => '59920-000',
			),
			299 => 
			array (
				'id' => 7275,
				'nome' => 'São Miguel de Touros',
				'uf' => 'RN',
				'cep2' => '',
				'estado_cod' => 20,
				'cep' => '59585-000',
			),
			300 => 
			array (
				'id' => 7276,
				'nome' => 'São Paulo do Potengi',
				'uf' => 'RN',
				'cep2' => '2412609',
				'estado_cod' => 20,
				'cep' => '59460-000',
			),
			301 => 
			array (
				'id' => 7277,
				'nome' => 'São Pedro',
				'uf' => 'RN',
				'cep2' => '2412708',
				'estado_cod' => 20,
				'cep' => '59480-000',
			),
			302 => 
			array (
				'id' => 7278,
				'nome' => 'São Rafael',
				'uf' => 'RN',
				'cep2' => '2412807',
				'estado_cod' => 20,
				'cep' => '59518-000',
			),
			303 => 
			array (
				'id' => 7279,
				'nome' => 'São Tomé',
				'uf' => 'RN',
				'cep2' => '2412906',
				'estado_cod' => 20,
				'cep' => '59400-000',
			),
			304 => 
			array (
				'id' => 7280,
				'nome' => 'São Vicente',
				'uf' => 'RN',
				'cep2' => '2413003',
				'estado_cod' => 20,
				'cep' => '59340-000',
			),
			305 => 
			array (
				'id' => 7281,
				'nome' => 'Senador Elói de Souza',
				'uf' => 'RN',
				'cep2' => '2413102',
				'estado_cod' => 20,
				'cep' => '59250-000',
			),
			306 => 
			array (
				'id' => 7282,
				'nome' => 'Senador Georgino Avelino',
				'uf' => 'RN',
				'cep2' => '2413201',
				'estado_cod' => 20,
				'cep' => '59168-000',
			),
			307 => 
			array (
				'id' => 7283,
				'nome' => 'Serra Caiada',
				'uf' => 'RN',
				'cep2' => '',
				'estado_cod' => 20,
				'cep' => '59245-000',
			),
			308 => 
			array (
				'id' => 7285,
				'nome' => 'Serra de São Bento',
				'uf' => 'RN',
				'cep2' => '2413300',
				'estado_cod' => 20,
				'cep' => '59214-000',
			),
			309 => 
			array (
				'id' => 7286,
				'nome' => 'Serra do Mel',
				'uf' => 'RN',
				'cep2' => '2413359',
				'estado_cod' => 20,
				'cep' => '59663-000',
			),
			310 => 
			array (
				'id' => 7287,
				'nome' => 'Serra Negra do Norte',
				'uf' => 'RN',
				'cep2' => '2413409',
				'estado_cod' => 20,
				'cep' => '59318-000',
			),
			311 => 
			array (
				'id' => 7288,
				'nome' => 'Serrinha',
				'uf' => 'RN',
				'cep2' => '2413508',
				'estado_cod' => 20,
				'cep' => '59258-000',
			),
			312 => 
			array (
				'id' => 7289,
				'nome' => 'Serrinha dos Pintos',
				'uf' => 'RN',
				'cep2' => '2413557',
				'estado_cod' => 20,
				'cep' => '59808-000',
			),
			313 => 
			array (
				'id' => 7290,
				'nome' => 'Severiano Melo',
				'uf' => 'RN',
				'cep2' => '2413607',
				'estado_cod' => 20,
				'cep' => '59856-000',
			),
			314 => 
			array (
				'id' => 7291,
				'nome' => 'Sítio Novo',
				'uf' => 'RN',
				'cep2' => '2413706',
				'estado_cod' => 20,
				'cep' => '59440-000',
			),
			315 => 
			array (
				'id' => 7292,
				'nome' => 'Taboleiro Grande',
				'uf' => 'RN',
				'cep2' => '2413805',
				'estado_cod' => 20,
				'cep' => '59840-000',
			),
			316 => 
			array (
				'id' => 7293,
				'nome' => 'Taipu',
				'uf' => 'RN',
				'cep2' => '2413904',
				'estado_cod' => 20,
				'cep' => '59565-000',
			),
			317 => 
			array (
				'id' => 7294,
				'nome' => 'Tangará',
				'uf' => 'RN',
				'cep2' => '2414001',
				'estado_cod' => 20,
				'cep' => '59240-000',
			),
			318 => 
			array (
				'id' => 7295,
				'nome' => 'Tenente Ananias',
				'uf' => 'RN',
				'cep2' => '2414100',
				'estado_cod' => 20,
				'cep' => '59955-000',
			),
			319 => 
			array (
				'id' => 7296,
				'nome' => 'Tenente Laurentino Cruz',
				'uf' => 'RN',
				'cep2' => '2414159',
				'estado_cod' => 20,
				'cep' => '59338-000',
			),
			320 => 
			array (
				'id' => 7297,
				'nome' => 'Tibau',
				'uf' => 'RN',
				'cep2' => '2411056',
				'estado_cod' => 20,
				'cep' => '59678-000',
			),
			321 => 
			array (
				'id' => 7298,
				'nome' => 'Tibau do Sul',
				'uf' => 'RN',
				'cep2' => '2414209',
				'estado_cod' => 20,
				'cep' => '59178-000',
			),
			322 => 
			array (
				'id' => 7299,
				'nome' => 'Timbaúba dos Batistas',
				'uf' => 'RN',
				'cep2' => '2414308',
				'estado_cod' => 20,
				'cep' => '59320-000',
			),
			323 => 
			array (
				'id' => 7300,
				'nome' => 'Touros',
				'uf' => 'RN',
				'cep2' => '2414407',
				'estado_cod' => 20,
				'cep' => '59584-000',
			),
			324 => 
			array (
				'id' => 7302,
				'nome' => 'Triunfo Potiguar',
				'uf' => 'RN',
				'cep2' => '2414456',
				'estado_cod' => 20,
				'cep' => '59685-000',
			),
			325 => 
			array (
				'id' => 7303,
				'nome' => 'Umarizal',
				'uf' => 'RN',
				'cep2' => '2414506',
				'estado_cod' => 20,
				'cep' => '59865-000',
			),
			326 => 
			array (
				'id' => 7304,
				'nome' => 'Upanema',
				'uf' => 'RN',
				'cep2' => '2414605',
				'estado_cod' => 20,
				'cep' => '59670-000',
			),
			327 => 
			array (
				'id' => 7305,
				'nome' => 'Várzea',
				'uf' => 'RN',
				'cep2' => '2414704',
				'estado_cod' => 20,
				'cep' => '59185-000',
			),
			328 => 
			array (
				'id' => 7306,
				'nome' => 'Venha-Ver',
				'uf' => 'RN',
				'cep2' => '2414753',
				'estado_cod' => 20,
				'cep' => '59925-000',
			),
			329 => 
			array (
				'id' => 7307,
				'nome' => 'Vera Cruz',
				'uf' => 'RN',
				'cep2' => '2414803',
				'estado_cod' => 20,
				'cep' => '59184-000',
			),
			330 => 
			array (
				'id' => 7308,
				'nome' => 'Viçosa',
				'uf' => 'RN',
				'cep2' => '2414902',
				'estado_cod' => 20,
				'cep' => '59815-000',
			),
			331 => 
			array (
				'id' => 7309,
				'nome' => 'Vila Flor',
				'uf' => 'RN',
				'cep2' => '2415008',
				'estado_cod' => 20,
				'cep' => '59192-000',
			),
			332 => 
			array (
				'id' => 7311,
				'nome' => 'Alto Alegre dos Parecis',
				'uf' => 'RO',
				'cep2' => '1100379',
				'estado_cod' => 21,
				'cep' => '76952-000',
			),
			333 => 
			array (
				'id' => 7312,
				'nome' => 'Alta Floresta D\'Oeste',
				'uf' => 'RO',
				'cep2' => '1100015',
				'estado_cod' => 21,
				'cep' => '76954-000',
			),
			334 => 
			array (
				'id' => 7313,
				'nome' => 'Alto Paraíso',
				'uf' => 'RO',
				'cep2' => '1100403',
				'estado_cod' => 21,
				'cep' => '76862-000',
			),
			335 => 
			array (
				'id' => 7314,
				'nome' => 'Alvorada D\'Oeste',
				'uf' => 'RO',
				'cep2' => '1100346',
				'estado_cod' => 21,
				'cep' => '76930-000',
			),
			336 => 
			array (
				'id' => 7315,
				'nome' => 'Ariquemes',
				'uf' => 'RO',
				'cep2' => '1100023',
				'estado_cod' => 21,
				'cep' => 'LOC',
			),
			337 => 
			array (
				'id' => 7316,
				'nome' => 'Buritis',
				'uf' => 'RO',
				'cep2' => '1100452',
				'estado_cod' => 21,
				'cep' => '76880-000',
			),
			338 => 
			array (
				'id' => 7317,
				'nome' => 'Cabixi',
				'uf' => 'RO',
				'cep2' => '1100031',
				'estado_cod' => 21,
				'cep' => '76994-000',
			),
			339 => 
			array (
				'id' => 7318,
				'nome' => 'Cacaulândia',
				'uf' => 'RO',
				'cep2' => '1100601',
				'estado_cod' => 21,
				'cep' => '76889-000',
			),
			340 => 
			array (
				'id' => 7319,
				'nome' => 'Cacoal',
				'uf' => 'RO',
				'cep2' => '1100049',
				'estado_cod' => 21,
				'cep' => 'LOC',
			),
			341 => 
			array (
				'id' => 7321,
				'nome' => 'Campo Novo de Rondônia',
				'uf' => 'RO',
				'cep2' => '1100700',
				'estado_cod' => 21,
				'cep' => '76887-000',
			),
			342 => 
			array (
				'id' => 7322,
				'nome' => 'Candeias do Jamari',
				'uf' => 'RO',
				'cep2' => '1100809',
				'estado_cod' => 21,
				'cep' => '76860-000',
			),
			343 => 
			array (
				'id' => 7323,
				'nome' => 'Castanheiras',
				'uf' => 'RO',
				'cep2' => '1100908',
				'estado_cod' => 21,
				'cep' => '76948-000',
			),
			344 => 
			array (
				'id' => 7324,
				'nome' => 'Cerejeiras',
				'uf' => 'RO',
				'cep2' => '1100056',
				'estado_cod' => 21,
				'cep' => '76997-000',
			),
			345 => 
			array (
				'id' => 7325,
				'nome' => 'Chupinguaia',
				'uf' => 'RO',
				'cep2' => '1100924',
				'estado_cod' => 21,
				'cep' => '76990-000',
			),
			346 => 
			array (
				'id' => 7326,
				'nome' => 'Colorado do Oeste',
				'uf' => 'RO',
				'cep2' => '1100064',
				'estado_cod' => 21,
				'cep' => '76993-000',
			),
			347 => 
			array (
				'id' => 7327,
				'nome' => 'Corumbiara',
				'uf' => 'RO',
				'cep2' => '1100072',
				'estado_cod' => 21,
				'cep' => '76995-000',
			),
			348 => 
			array (
				'id' => 7328,
				'nome' => 'Costa Marques',
				'uf' => 'RO',
				'cep2' => '1100080',
				'estado_cod' => 21,
				'cep' => '76937-000',
			),
			349 => 
			array (
				'id' => 7329,
				'nome' => 'Cujubim',
				'uf' => 'RO',
				'cep2' => '1100940',
				'estado_cod' => 21,
				'cep' => '76864-000',
			),
			350 => 
			array (
				'id' => 7330,
				'nome' => 'Espigão do Oeste',
				'uf' => 'RO',
				'cep2' => '',
				'estado_cod' => 21,
				'cep' => '76974-000',
			),
			351 => 
			array (
				'id' => 7331,
				'nome' => 'Governador Jorge Teixeira',
				'uf' => 'RO',
				'cep2' => '1101005',
				'estado_cod' => 21,
				'cep' => '76898-000',
			),
			352 => 
			array (
				'id' => 7332,
				'nome' => 'Guajará-Mirim',
				'uf' => 'RO',
				'cep2' => '1100106',
				'estado_cod' => 21,
				'cep' => '76850-000',
			),
			353 => 
			array (
				'id' => 7334,
				'nome' => 'Itapuã do Oeste',
				'uf' => 'RO',
				'cep2' => '1101104',
				'estado_cod' => 21,
				'cep' => '76861-000',
			),
			354 => 
			array (
				'id' => 7335,
				'nome' => 'Jaru',
				'uf' => 'RO',
				'cep2' => '1100114',
				'estado_cod' => 21,
				'cep' => '76890-000',
			),
			355 => 
			array (
				'id' => 7336,
				'nome' => 'Ji-Paraná',
				'uf' => 'RO',
				'cep2' => '1100122',
				'estado_cod' => 21,
				'cep' => 'LOC',
			),
			356 => 
			array (
				'id' => 7337,
				'nome' => 'Machadinho D\'Oeste',
				'uf' => 'RO',
				'cep2' => '1100130',
				'estado_cod' => 21,
				'cep' => '76868-000',
			),
			357 => 
			array (
				'id' => 7339,
				'nome' => 'Ministro Andreazza',
				'uf' => 'RO',
				'cep2' => '1101203',
				'estado_cod' => 21,
				'cep' => '76919-000',
			),
			358 => 
			array (
				'id' => 7340,
				'nome' => 'Mirante da Serra',
				'uf' => 'RO',
				'cep2' => '1101302',
				'estado_cod' => 21,
				'cep' => '76926-000',
			),
			359 => 
			array (
				'id' => 7341,
				'nome' => 'Monte Negro',
				'uf' => 'RO',
				'cep2' => '1101401',
				'estado_cod' => 21,
				'cep' => '76888-000',
			),
			360 => 
			array (
				'id' => 7342,
				'nome' => 'Nova Brasilândia D\'Oeste',
				'uf' => 'RO',
				'cep2' => '1100148',
				'estado_cod' => 21,
				'cep' => '76958-000',
			),
			361 => 
			array (
				'id' => 7343,
				'nome' => 'Nova Mamoré',
				'uf' => 'RO',
				'cep2' => '',
				'estado_cod' => 21,
				'cep' => '76857-000',
			),
			362 => 
			array (
				'id' => 7344,
				'nome' => 'Nova União',
				'uf' => 'RO',
				'cep2' => '1101435',
				'estado_cod' => 21,
				'cep' => '76924-000',
			),
			363 => 
			array (
				'id' => 7346,
				'nome' => 'Novo Horizonte do Oeste',
				'uf' => 'RO',
				'cep2' => '1100502',
				'estado_cod' => 21,
				'cep' => '76956-000',
			),
			364 => 
			array (
				'id' => 7347,
				'nome' => 'Ouro Preto do Oeste',
				'uf' => 'RO',
				'cep2' => '1100155',
				'estado_cod' => 21,
				'cep' => '76920-000',
			),
			365 => 
			array (
				'id' => 7348,
				'nome' => 'Parecis',
				'uf' => 'RO',
				'cep2' => '1101450',
				'estado_cod' => 21,
				'cep' => '76979-000',
			),
			366 => 
			array (
				'id' => 7350,
				'nome' => 'Pimenta Bueno',
				'uf' => 'RO',
				'cep2' => '1100189',
				'estado_cod' => 21,
				'cep' => '76970-000',
			),
			367 => 
			array (
				'id' => 7351,
				'nome' => 'Pimenteiras do Oeste',
				'uf' => 'RO',
				'cep2' => '1101468',
				'estado_cod' => 21,
				'cep' => '76999-000',
			),
			368 => 
			array (
				'id' => 7352,
				'nome' => 'Porto Velho',
				'uf' => 'RO',
				'cep2' => '1100205',
				'estado_cod' => 21,
				'cep' => 'LOC',
			),
			369 => 
			array (
				'id' => 7353,
				'nome' => 'Presidente Médici',
				'uf' => 'RO',
				'cep2' => '1100254',
				'estado_cod' => 21,
				'cep' => '76916-000',
			),
			370 => 
			array (
				'id' => 7354,
				'nome' => 'Primavera de Rondônia',
				'uf' => 'RO',
				'cep2' => '1101476',
				'estado_cod' => 21,
				'cep' => '76976-000',
			),
			371 => 
			array (
				'id' => 7356,
				'nome' => 'Rio Crespo',
				'uf' => 'RO',
				'cep2' => '1100262',
				'estado_cod' => 21,
				'cep' => '76863-000',
			),
			372 => 
			array (
				'id' => 7358,
				'nome' => 'Rolim de Moura',
				'uf' => 'RO',
				'cep2' => '1100288',
				'estado_cod' => 21,
				'cep' => '76940-000',
			),
			373 => 
			array (
				'id' => 7359,
				'nome' => 'Santa Luzia D\'Oeste',
				'uf' => 'RO',
				'cep2' => '1100296',
				'estado_cod' => 21,
				'cep' => '76950-000',
			),
			374 => 
			array (
				'id' => 7360,
				'nome' => 'São Felipe D\'Oeste',
				'uf' => 'RO',
				'cep2' => '1101484',
				'estado_cod' => 21,
				'cep' => '76977-000',
			),
			375 => 
			array (
				'id' => 7361,
				'nome' => 'São Francisco do Guaporé',
				'uf' => 'RO',
				'cep2' => '1101492',
				'estado_cod' => 21,
				'cep' => '76935-000',
			),
			376 => 
			array (
				'id' => 7362,
				'nome' => 'São Miguel do Guaporé',
				'uf' => 'RO',
				'cep2' => '1100320',
				'estado_cod' => 21,
				'cep' => '76932-000',
			),
			377 => 
			array (
				'id' => 7363,
				'nome' => 'Seringueiras',
				'uf' => 'RO',
				'cep2' => '1101500',
				'estado_cod' => 21,
				'cep' => '76934-000',
			),
			378 => 
			array (
				'id' => 7365,
				'nome' => 'Teixeirópolis',
				'uf' => 'RO',
				'cep2' => '1101559',
				'estado_cod' => 21,
				'cep' => '76928-000',
			),
			379 => 
			array (
				'id' => 7366,
				'nome' => 'Theobroma',
				'uf' => 'RO',
				'cep2' => '1101609',
				'estado_cod' => 21,
				'cep' => '76866-000',
			),
			380 => 
			array (
				'id' => 7367,
				'nome' => 'Urupá',
				'uf' => 'RO',
				'cep2' => '1101708',
				'estado_cod' => 21,
				'cep' => '76929-000',
			),
			381 => 
			array (
				'id' => 7368,
				'nome' => 'Vale do Anari',
				'uf' => 'RO',
				'cep2' => '1101757',
				'estado_cod' => 21,
				'cep' => '76867-000',
			),
			382 => 
			array (
				'id' => 7369,
				'nome' => 'Vale do Paraíso',
				'uf' => 'RO',
				'cep2' => '1101807',
				'estado_cod' => 21,
				'cep' => '76923-000',
			),
			383 => 
			array (
				'id' => 7371,
				'nome' => 'Vilhena',
				'uf' => 'RO',
				'cep2' => '1100304',
				'estado_cod' => 21,
				'cep' => '76980-000',
			),
			384 => 
			array (
				'id' => 7373,
				'nome' => 'Alto Alegre',
				'uf' => 'RR',
				'cep2' => '1400050',
				'estado_cod' => 22,
				'cep' => '69350-000',
			),
			385 => 
			array (
				'id' => 7374,
				'nome' => 'Amajari',
				'uf' => 'RR',
				'cep2' => '1400027',
				'estado_cod' => 22,
				'cep' => '69343-000',
			),
			386 => 
			array (
				'id' => 7375,
				'nome' => 'Boa Vista',
				'uf' => 'RR',
				'cep2' => '1400100',
				'estado_cod' => 22,
				'cep' => 'LOC',
			),
			387 => 
			array (
				'id' => 7376,
				'nome' => 'Bonfim',
				'uf' => 'RR',
				'cep2' => '1400159',
				'estado_cod' => 22,
				'cep' => '69380-000',
			),
			388 => 
			array (
				'id' => 7377,
				'nome' => 'Cantá',
				'uf' => 'RR',
				'cep2' => '1400175',
				'estado_cod' => 22,
				'cep' => '69390-000',
			),
			389 => 
			array (
				'id' => 7378,
				'nome' => 'Caracaraí',
				'uf' => 'RR',
				'cep2' => '1400209',
				'estado_cod' => 22,
				'cep' => '69360-000',
			),
			390 => 
			array (
				'id' => 7379,
				'nome' => 'Caroebe',
				'uf' => 'RR',
				'cep2' => '1400233',
				'estado_cod' => 22,
				'cep' => '69378-000',
			),
			391 => 
			array (
				'id' => 7380,
				'nome' => 'Iracema',
				'uf' => 'RR',
				'cep2' => '1400282',
				'estado_cod' => 22,
				'cep' => '69348-000',
			),
			392 => 
			array (
				'id' => 7381,
				'nome' => 'Mucajaí',
				'uf' => 'RR',
				'cep2' => '1400308',
				'estado_cod' => 22,
				'cep' => '69340-000',
			),
			393 => 
			array (
				'id' => 7382,
				'nome' => 'Normandia',
				'uf' => 'RR',
				'cep2' => '1400407',
				'estado_cod' => 22,
				'cep' => '69355-000',
			),
			394 => 
			array (
				'id' => 7383,
				'nome' => 'Pacaraima',
				'uf' => 'RR',
				'cep2' => '1400456',
				'estado_cod' => 22,
				'cep' => '69345-000',
			),
			395 => 
			array (
				'id' => 7384,
				'nome' => 'Rorainópolis',
				'uf' => 'RR',
				'cep2' => '1400472',
				'estado_cod' => 22,
				'cep' => '69373-000',
			),
			396 => 
			array (
				'id' => 7385,
				'nome' => 'São João da Baliza',
				'uf' => 'RR',
				'cep2' => '1400506',
				'estado_cod' => 22,
				'cep' => '69375-000',
			),
			397 => 
			array (
				'id' => 7386,
				'nome' => 'São Luiz',
				'uf' => 'RR',
				'cep2' => '1400605',
				'estado_cod' => 22,
				'cep' => '69370-000',
			),
			398 => 
			array (
				'id' => 7387,
				'nome' => 'Uiramutã',
				'uf' => 'RR',
				'cep2' => '1400704',
				'estado_cod' => 22,
				'cep' => '69358-000',
			),
			399 => 
			array (
				'id' => 7388,
				'nome' => 'Aceguá',
				'uf' => 'RS',
				'cep2' => '4300034',
				'estado_cod' => 23,
				'cep' => '96445-000',
			),
			400 => 
			array (
				'id' => 7390,
				'nome' => 'Água Santa',
				'uf' => 'RS',
				'cep2' => '4300059',
				'estado_cod' => 23,
				'cep' => '99965-000',
			),
			401 => 
			array (
				'id' => 7392,
				'nome' => 'Agudo',
				'uf' => 'RS',
				'cep2' => '4300109',
				'estado_cod' => 23,
				'cep' => '96540-000',
			),
			402 => 
			array (
				'id' => 7393,
				'nome' => 'Ajuricaba',
				'uf' => 'RS',
				'cep2' => '4300208',
				'estado_cod' => 23,
				'cep' => '98750-000',
			),
			403 => 
			array (
				'id' => 7395,
				'nome' => 'Alecrim',
				'uf' => 'RS',
				'cep2' => '4300307',
				'estado_cod' => 23,
				'cep' => '98950-000',
			),
			404 => 
			array (
				'id' => 7396,
				'nome' => 'Alegrete',
				'uf' => 'RS',
				'cep2' => '4300406',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			405 => 
			array (
				'id' => 7397,
				'nome' => 'Alegria',
				'uf' => 'RS',
				'cep2' => '4300455',
				'estado_cod' => 23,
				'cep' => '98905-000',
			),
			406 => 
			array (
				'id' => 7399,
				'nome' => 'Almirante Tamandaré do Sul',
				'uf' => 'RS',
				'cep2' => '4300471',
				'estado_cod' => 23,
				'cep' => '99523-000',
			),
			407 => 
			array (
				'id' => 7400,
				'nome' => 'Alpestre',
				'uf' => 'RS',
				'cep2' => '4300505',
				'estado_cod' => 23,
				'cep' => '98480-000',
			),
			408 => 
			array (
				'id' => 7401,
				'nome' => 'Alto Alegre',
				'uf' => 'RS',
				'cep2' => '4300554',
				'estado_cod' => 23,
				'cep' => '99430-000',
			),
			409 => 
			array (
				'id' => 7404,
				'nome' => 'Alto Feliz',
				'uf' => 'RS',
				'cep2' => '4300570',
				'estado_cod' => 23,
				'cep' => '95773-000',
			),
			410 => 
			array (
				'id' => 7408,
				'nome' => 'Alvorada',
				'uf' => 'RS',
				'cep2' => '4300604',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			411 => 
			array (
				'id' => 7409,
				'nome' => 'Amaral Ferrador',
				'uf' => 'RS',
				'cep2' => '4300638',
				'estado_cod' => 23,
				'cep' => '96635-000',
			),
			412 => 
			array (
				'id' => 7410,
				'nome' => 'Ametista do Sul',
				'uf' => 'RS',
				'cep2' => '4300646',
				'estado_cod' => 23,
				'cep' => '98465-000',
			),
			413 => 
			array (
				'id' => 7411,
				'nome' => 'André da Rocha',
				'uf' => 'RS',
				'cep2' => '4300661',
				'estado_cod' => 23,
				'cep' => '95310-000',
			),
			414 => 
			array (
				'id' => 7412,
				'nome' => 'Anta Gorda',
				'uf' => 'RS',
				'cep2' => '4300703',
				'estado_cod' => 23,
				'cep' => '95980-000',
			),
			415 => 
			array (
				'id' => 7414,
				'nome' => 'Antônio Prado',
				'uf' => 'RS',
				'cep2' => '4300802',
				'estado_cod' => 23,
				'cep' => '95250-000',
			),
			416 => 
			array (
				'id' => 7415,
				'nome' => 'Arambaré',
				'uf' => 'RS',
				'cep2' => '4300851',
				'estado_cod' => 23,
				'cep' => '96178-000',
			),
			417 => 
			array (
				'id' => 7416,
				'nome' => 'Araricá',
				'uf' => 'RS',
				'cep2' => '4300877',
				'estado_cod' => 23,
				'cep' => '93880-000',
			),
			418 => 
			array (
				'id' => 7417,
				'nome' => 'Aratiba',
				'uf' => 'RS',
				'cep2' => '4300901',
				'estado_cod' => 23,
				'cep' => '99770-000',
			),
			419 => 
			array (
				'id' => 7421,
				'nome' => 'Arroio do Meio',
				'uf' => 'RS',
				'cep2' => '4301008',
				'estado_cod' => 23,
				'cep' => '95940-000',
			),
			420 => 
			array (
				'id' => 7422,
				'nome' => 'Arroio do Padre',
				'uf' => 'RS',
				'cep2' => '4301073',
				'estado_cod' => 23,
				'cep' => '96155-000',
			),
			421 => 
			array (
				'id' => 7423,
				'nome' => 'Arroio do Sal',
				'uf' => 'RS',
				'cep2' => '4301057',
				'estado_cod' => 23,
				'cep' => '95585-000',
			),
			422 => 
			array (
				'id' => 7425,
				'nome' => 'Arroio do Tigre',
				'uf' => 'RS',
				'cep2' => '4301206',
				'estado_cod' => 23,
				'cep' => '96950-000',
			),
			423 => 
			array (
				'id' => 7426,
				'nome' => 'Arroio dos Ratos',
				'uf' => 'RS',
				'cep2' => '4301107',
				'estado_cod' => 23,
				'cep' => '96740-000',
			),
			424 => 
			array (
				'id' => 7427,
				'nome' => 'Arroio Grande',
				'uf' => 'RS',
				'cep2' => '4301305',
				'estado_cod' => 23,
				'cep' => '96330-000',
			),
			425 => 
			array (
				'id' => 7431,
				'nome' => 'Arvorezinha',
				'uf' => 'RS',
				'cep2' => '4301404',
				'estado_cod' => 23,
				'cep' => '95995-000',
			),
			426 => 
			array (
				'id' => 7434,
				'nome' => 'Augusto Pestana',
				'uf' => 'RS',
				'cep2' => '4301503',
				'estado_cod' => 23,
				'cep' => '98740-000',
			),
			427 => 
			array (
				'id' => 7435,
				'nome' => 'Áurea',
				'uf' => 'RS',
				'cep2' => '4301552',
				'estado_cod' => 23,
				'cep' => '99835-000',
			),
			428 => 
			array (
				'id' => 7439,
				'nome' => 'Bagé',
				'uf' => 'RS',
				'cep2' => '4301602',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			429 => 
			array (
				'id' => 7441,
				'nome' => 'Balneário Pinhal',
				'uf' => 'RS',
				'cep2' => '4301636',
				'estado_cod' => 23,
				'cep' => '95599-000',
			),
			430 => 
			array (
				'id' => 7443,
				'nome' => 'Barão',
				'uf' => 'RS',
				'cep2' => '4301651',
				'estado_cod' => 23,
				'cep' => '95730-000',
			),
			431 => 
			array (
				'id' => 7444,
				'nome' => 'Barão de Cotegipe',
				'uf' => 'RS',
				'cep2' => '4301701',
				'estado_cod' => 23,
				'cep' => '99740-000',
			),
			432 => 
			array (
				'id' => 7445,
				'nome' => 'Barão do Triunfo',
				'uf' => 'RS',
				'cep2' => '4301750',
				'estado_cod' => 23,
				'cep' => '96735-000',
			),
			433 => 
			array (
				'id' => 7446,
				'nome' => 'Barra do Guarita',
				'uf' => 'RS',
				'cep2' => '4301859',
				'estado_cod' => 23,
				'cep' => '98530-000',
			),
			434 => 
			array (
				'id' => 7448,
				'nome' => 'Barra do Quaraí',
				'uf' => 'RS',
				'cep2' => '4301875',
				'estado_cod' => 23,
				'cep' => '97538-000',
			),
			435 => 
			array (
				'id' => 7449,
				'nome' => 'Barra do Ribeiro',
				'uf' => 'RS',
				'cep2' => '4301909',
				'estado_cod' => 23,
				'cep' => '96790-000',
			),
			436 => 
			array (
				'id' => 7450,
				'nome' => 'Barra do Rio Azul',
				'uf' => 'RS',
				'cep2' => '4301925',
				'estado_cod' => 23,
				'cep' => '99795-000',
			),
			437 => 
			array (
				'id' => 7451,
				'nome' => 'Barra Funda',
				'uf' => 'RS',
				'cep2' => '4301958',
				'estado_cod' => 23,
				'cep' => '99585-000',
			),
			438 => 
			array (
				'id' => 7452,
				'nome' => 'Barracão',
				'uf' => 'RS',
				'cep2' => '4301800',
				'estado_cod' => 23,
				'cep' => '95370-000',
			),
			439 => 
			array (
				'id' => 7458,
				'nome' => 'Barros Cassal',
				'uf' => 'RS',
				'cep2' => '4302006',
				'estado_cod' => 23,
				'cep' => '99360-000',
			),
			440 => 
			array (
				'id' => 7465,
				'nome' => 'Benjamin Constant do Sul',
				'uf' => 'RS',
				'cep2' => '4302055',
				'estado_cod' => 23,
				'cep' => '99650-000',
			),
			441 => 
			array (
				'id' => 7466,
				'nome' => 'Bento Gonçalves',
				'uf' => 'RS',
				'cep2' => '4302105',
				'estado_cod' => 23,
				'cep' => '95700-000',
			),
			442 => 
			array (
				'id' => 7472,
				'nome' => 'Boa Vista das Missões',
				'uf' => 'RS',
				'cep2' => '4302154',
				'estado_cod' => 23,
				'cep' => '98335-000',
			),
			443 => 
			array (
				'id' => 7473,
				'nome' => 'Boa Vista do Buricá',
				'uf' => 'RS',
				'cep2' => '4302204',
				'estado_cod' => 23,
				'cep' => '98918-000',
			),
			444 => 
			array (
				'id' => 7474,
				'nome' => 'Boa Vista do Cadeado',
				'uf' => 'RS',
				'cep2' => '4302220',
				'estado_cod' => 23,
				'cep' => '98118-000',
			),
			445 => 
			array (
				'id' => 7475,
				'nome' => 'Boa Vista do Incra',
				'uf' => 'RS',
				'cep2' => '4302238',
				'estado_cod' => 23,
				'cep' => '98120-000',
			),
			446 => 
			array (
				'id' => 7476,
				'nome' => 'Boa Vista do Sul',
				'uf' => 'RS',
				'cep2' => '4302253',
				'estado_cod' => 23,
				'cep' => '95727-000',
			),
			447 => 
			array (
				'id' => 7481,
				'nome' => 'Bom Jesus',
				'uf' => 'RS',
				'cep2' => '4302303',
				'estado_cod' => 23,
				'cep' => '95290-000',
			),
			448 => 
			array (
				'id' => 7482,
				'nome' => 'Bom Princípio',
				'uf' => 'RS',
				'cep2' => '4302352',
				'estado_cod' => 23,
				'cep' => '95765-000',
			),
			449 => 
			array (
				'id' => 7483,
				'nome' => 'Bom Progresso',
				'uf' => 'RS',
				'cep2' => '4302378',
				'estado_cod' => 23,
				'cep' => '98575-000',
			),
			450 => 
			array (
				'id' => 7486,
				'nome' => 'Bom Retiro do Sul',
				'uf' => 'RS',
				'cep2' => '4302402',
				'estado_cod' => 23,
				'cep' => '95870-000',
			),
			451 => 
			array (
				'id' => 7490,
				'nome' => 'Boqueirão do Leão',
				'uf' => 'RS',
				'cep2' => '4302451',
				'estado_cod' => 23,
				'cep' => '95920-000',
			),
			452 => 
			array (
				'id' => 7492,
				'nome' => 'Bossoroca',
				'uf' => 'RS',
				'cep2' => '4302501',
				'estado_cod' => 23,
				'cep' => '97850-000',
			),
			453 => 
			array (
				'id' => 7494,
				'nome' => 'Braga',
				'uf' => 'RS',
				'cep2' => '4302600',
				'estado_cod' => 23,
				'cep' => '98560-000',
			),
			454 => 
			array (
				'id' => 7495,
				'nome' => 'Brochier',
				'uf' => 'RS',
				'cep2' => '4302659',
				'estado_cod' => 23,
				'cep' => '95790-000',
			),
			455 => 
			array (
				'id' => 7497,
				'nome' => 'Butiá',
				'uf' => 'RS',
				'cep2' => '4302709',
				'estado_cod' => 23,
				'cep' => '96750-000',
			),
			456 => 
			array (
				'id' => 7499,
				'nome' => 'Caçapava do Sul',
				'uf' => 'RS',
				'cep2' => '4302808',
				'estado_cod' => 23,
				'cep' => '96570-000',
			),
			457 => 
			array (
				'id' => 7500,
				'nome' => 'Cacequi',
				'uf' => 'RS',
				'cep2' => '4302907',
				'estado_cod' => 23,
				'cep' => '97450-000',
			),
			458 => 
			array (
				'id' => 7501,
				'nome' => 'Cachoeira do Sul',
				'uf' => 'RS',
				'cep2' => '4303004',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			459 => 
			array (
				'id' => 7502,
				'nome' => 'Cachoeirinha',
				'uf' => 'RS',
				'cep2' => '4303103',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			460 => 
			array (
				'id' => 7503,
				'nome' => 'Cacique Doble',
				'uf' => 'RS',
				'cep2' => '4303202',
				'estado_cod' => 23,
				'cep' => '99860-000',
			),
			461 => 
			array (
				'id' => 7505,
				'nome' => 'Caibaté',
				'uf' => 'RS',
				'cep2' => '4303301',
				'estado_cod' => 23,
				'cep' => '97930-000',
			),
			462 => 
			array (
				'id' => 7506,
				'nome' => 'Caiçara',
				'uf' => 'RS',
				'cep2' => '4303400',
				'estado_cod' => 23,
				'cep' => '98440-000',
			),
			463 => 
			array (
				'id' => 7507,
				'nome' => 'Camaquã',
				'uf' => 'RS',
				'cep2' => '4303509',
				'estado_cod' => 23,
				'cep' => '96180-000',
			),
			464 => 
			array (
				'id' => 7508,
				'nome' => 'Camargo',
				'uf' => 'RS',
				'cep2' => '4303558',
				'estado_cod' => 23,
				'cep' => '99165-000',
			),
			465 => 
			array (
				'id' => 7509,
				'nome' => 'Cambará do Sul',
				'uf' => 'RS',
				'cep2' => '4303608',
				'estado_cod' => 23,
				'cep' => '95480-000',
			),
			466 => 
			array (
				'id' => 7512,
				'nome' => 'Campestre da Serra',
				'uf' => 'RS',
				'cep2' => '4303673',
				'estado_cod' => 23,
				'cep' => '95255-000',
			),
			467 => 
			array (
				'id' => 7513,
				'nome' => 'Campina das Missões',
				'uf' => 'RS',
				'cep2' => '4303707',
				'estado_cod' => 23,
				'cep' => '98975-000',
			),
			468 => 
			array (
				'id' => 7516,
				'nome' => 'Campinas do Sul',
				'uf' => 'RS',
				'cep2' => '4303806',
				'estado_cod' => 23,
				'cep' => '99660-000',
			),
			469 => 
			array (
				'id' => 7517,
				'nome' => 'Campo Bom',
				'uf' => 'RS',
				'cep2' => '4303905',
				'estado_cod' => 23,
				'cep' => '93700-000',
			),
			470 => 
			array (
				'id' => 7520,
				'nome' => 'Campo Novo',
				'uf' => 'RS',
				'cep2' => '4304002',
				'estado_cod' => 23,
				'cep' => '98570-000',
			),
			471 => 
			array (
				'id' => 7525,
				'nome' => 'Campos Borges',
				'uf' => 'RS',
				'cep2' => '4304101',
				'estado_cod' => 23,
				'cep' => '99435-000',
			),
			472 => 
			array (
				'id' => 7526,
				'nome' => 'Candelária',
				'uf' => 'RS',
				'cep2' => '4304200',
				'estado_cod' => 23,
				'cep' => '96930-000',
			),
			473 => 
			array (
				'id' => 7528,
				'nome' => 'Cândido Godói',
				'uf' => 'RS',
				'cep2' => '4304309',
				'estado_cod' => 23,
				'cep' => '98970-000',
			),
			474 => 
			array (
				'id' => 7529,
				'nome' => 'Candiota',
				'uf' => 'RS',
				'cep2' => '4304358',
				'estado_cod' => 23,
				'cep' => '96495-000',
			),
			475 => 
			array (
				'id' => 7530,
				'nome' => 'Canela',
				'uf' => 'RS',
				'cep2' => '4304408',
				'estado_cod' => 23,
				'cep' => '95680-000',
			),
			476 => 
			array (
				'id' => 7531,
				'nome' => 'Canguçu',
				'uf' => 'RS',
				'cep2' => '4304507',
				'estado_cod' => 23,
				'cep' => '96600-000',
			),
			477 => 
			array (
				'id' => 7533,
				'nome' => 'Canoas',
				'uf' => 'RS',
				'cep2' => '4304606',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			478 => 
			array (
				'id' => 7534,
				'nome' => 'Canudos do Vale',
				'uf' => 'RS',
				'cep2' => '4304614',
				'estado_cod' => 23,
				'cep' => '95933-000',
			),
			479 => 
			array (
				'id' => 7536,
				'nome' => 'Capão Bonito do Sul',
				'uf' => 'RS',
				'cep2' => '4304622',
				'estado_cod' => 23,
				'cep' => '95308-000',
			),
			480 => 
			array (
				'id' => 7539,
				'nome' => 'Capão da Canoa',
				'uf' => 'RS',
				'cep2' => '4304630',
				'estado_cod' => 23,
				'cep' => '95555-000',
			),
			481 => 
			array (
				'id' => 7542,
				'nome' => 'Capão do Cipó',
				'uf' => 'RS',
				'cep2' => '4304655',
				'estado_cod' => 23,
				'cep' => '97753-000',
			),
			482 => 
			array (
				'id' => 7543,
				'nome' => 'Capão do Leão',
				'uf' => 'RS',
				'cep2' => '4304663',
				'estado_cod' => 23,
				'cep' => '96160-000',
			),
			483 => 
			array (
				'id' => 7544,
				'nome' => 'Capela de Santana',
				'uf' => 'RS',
				'cep2' => '4304689',
				'estado_cod' => 23,
				'cep' => '95745-000',
			),
			484 => 
			array (
				'id' => 7547,
				'nome' => 'Capitão',
				'uf' => 'RS',
				'cep2' => '4304697',
				'estado_cod' => 23,
				'cep' => '95935-000',
			),
			485 => 
			array (
				'id' => 7548,
				'nome' => 'Capivari do Sul',
				'uf' => 'RS',
				'cep2' => '4304671',
				'estado_cod' => 23,
				'cep' => '95552-000',
			),
			486 => 
			array (
				'id' => 7552,
				'nome' => 'Caraá',
				'uf' => 'RS',
				'cep2' => '4304713',
				'estado_cod' => 23,
				'cep' => '95515-000',
			),
			487 => 
			array (
				'id' => 7554,
				'nome' => 'Carazinho',
				'uf' => 'RS',
				'cep2' => '4304705',
				'estado_cod' => 23,
				'cep' => '99500-000',
			),
			488 => 
			array (
				'id' => 7555,
				'nome' => 'Carlos Barbosa',
				'uf' => 'RS',
				'cep2' => '4304804',
				'estado_cod' => 23,
				'cep' => '95185-000',
			),
			489 => 
			array (
				'id' => 7556,
				'nome' => 'Carlos Gomes',
				'uf' => 'RS',
				'cep2' => '4304853',
				'estado_cod' => 23,
				'cep' => '99825-000',
			),
			490 => 
			array (
				'id' => 7558,
				'nome' => 'Casca',
				'uf' => 'RS',
				'cep2' => '4304903',
				'estado_cod' => 23,
				'cep' => '99260-000',
			),
			491 => 
			array (
				'id' => 7561,
				'nome' => 'Caseiros',
				'uf' => 'RS',
				'cep2' => '4304952',
				'estado_cod' => 23,
				'cep' => '95315-000',
			),
			492 => 
			array (
				'id' => 7563,
				'nome' => 'Catuípe',
				'uf' => 'RS',
				'cep2' => '4305009',
				'estado_cod' => 23,
				'cep' => '98770-000',
			),
			493 => 
			array (
				'id' => 7566,
				'nome' => 'Caxias do Sul',
				'uf' => 'RS',
				'cep2' => '4305108',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			494 => 
			array (
				'id' => 7569,
				'nome' => 'Centenário',
				'uf' => 'RS',
				'cep2' => '4305116',
				'estado_cod' => 23,
				'cep' => '99838-000',
			),
			495 => 
			array (
				'id' => 7572,
				'nome' => 'Cerrito',
				'uf' => 'RS',
				'cep2' => '4305124',
				'estado_cod' => 23,
				'cep' => '96395-000',
			),
			496 => 
			array (
				'id' => 7576,
				'nome' => 'Cerro Branco',
				'uf' => 'RS',
				'cep2' => '4305132',
				'estado_cod' => 23,
				'cep' => '96535-000',
			),
			497 => 
			array (
				'id' => 7580,
				'nome' => 'Cerro Grande',
				'uf' => 'RS',
				'cep2' => '4305157',
				'estado_cod' => 23,
				'cep' => '98340-000',
			),
			498 => 
			array (
				'id' => 7581,
				'nome' => 'Cerro Grande do Sul',
				'uf' => 'RS',
				'cep2' => '4305173',
				'estado_cod' => 23,
				'cep' => '96770-000',
			),
			499 => 
			array (
				'id' => 7582,
				'nome' => 'Cerro Largo',
				'uf' => 'RS',
				'cep2' => '4305207',
				'estado_cod' => 23,
				'cep' => '97900-000',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 7583,
				'nome' => 'Chapada',
				'uf' => 'RS',
				'cep2' => '4305306',
				'estado_cod' => 23,
				'cep' => '99530-000',
			),
			1 => 
			array (
				'id' => 7585,
				'nome' => 'Charqueadas',
				'uf' => 'RS',
				'cep2' => '4305355',
				'estado_cod' => 23,
				'cep' => '96745-000',
			),
			2 => 
			array (
				'id' => 7586,
				'nome' => 'Charrua',
				'uf' => 'RS',
				'cep2' => '4305371',
				'estado_cod' => 23,
				'cep' => '99960-000',
			),
			3 => 
			array (
				'id' => 7587,
				'nome' => 'Chiapetta',
				'uf' => 'RS',
				'cep2' => '4305405',
				'estado_cod' => 23,
				'cep' => '98760-000',
			),
			4 => 
			array (
				'id' => 7591,
				'nome' => 'Chuí',
				'uf' => 'RS',
				'cep2' => '4305439',
				'estado_cod' => 23,
				'cep' => '96255-000',
			),
			5 => 
			array (
				'id' => 7592,
				'nome' => 'Chuvisca',
				'uf' => 'RS',
				'cep2' => '4305447',
				'estado_cod' => 23,
				'cep' => '96193-000',
			),
			6 => 
			array (
				'id' => 7593,
				'nome' => 'Cidreira',
				'uf' => 'RS',
				'cep2' => '4305454',
				'estado_cod' => 23,
				'cep' => '95595-000',
			),
			7 => 
			array (
				'id' => 7595,
				'nome' => 'Ciríaco',
				'uf' => 'RS',
				'cep2' => '4305504',
				'estado_cod' => 23,
				'cep' => '99970-000',
			),
			8 => 
			array (
				'id' => 7599,
				'nome' => 'Colinas',
				'uf' => 'RS',
				'cep2' => '4305587',
				'estado_cod' => 23,
				'cep' => '95895-000',
			),
			9 => 
			array (
				'id' => 7607,
				'nome' => 'Colorado',
				'uf' => 'RS',
				'cep2' => '4305603',
				'estado_cod' => 23,
				'cep' => '99460-000',
			),
			10 => 
			array (
				'id' => 7609,
				'nome' => 'Condor',
				'uf' => 'RS',
				'cep2' => '4305702',
				'estado_cod' => 23,
				'cep' => '98290-000',
			),
			11 => 
			array (
				'id' => 7611,
				'nome' => 'Constantina',
				'uf' => 'RS',
				'cep2' => '4305801',
				'estado_cod' => 23,
				'cep' => '99680-000',
			),
			12 => 
			array (
				'id' => 7612,
				'nome' => 'Coqueiro Baixo',
				'uf' => 'RS',
				'cep2' => '4305835',
				'estado_cod' => 23,
				'cep' => '95955-000',
			),
			13 => 
			array (
				'id' => 7613,
				'nome' => 'Coqueiros do Sul',
				'uf' => 'RS',
				'cep2' => '4305850',
				'estado_cod' => 23,
				'cep' => '99528-000',
			),
			14 => 
			array (
				'id' => 7616,
				'nome' => 'Coronel Barros',
				'uf' => 'RS',
				'cep2' => '4305871',
				'estado_cod' => 23,
				'cep' => '98735-000',
			),
			15 => 
			array (
				'id' => 7617,
				'nome' => 'Coronel Bicaco',
				'uf' => 'RS',
				'cep2' => '4305900',
				'estado_cod' => 23,
				'cep' => '98580-000',
			),
			16 => 
			array (
				'id' => 7619,
				'nome' => 'Coronel Pilar',
				'uf' => 'RS',
				'cep2' => '4305934',
				'estado_cod' => 23,
				'cep' => '95726-000',
			),
			17 => 
			array (
				'id' => 7624,
				'nome' => 'Cotiporã',
				'uf' => 'RS',
				'cep2' => '4305959',
				'estado_cod' => 23,
				'cep' => '95335-000',
			),
			18 => 
			array (
				'id' => 7625,
				'nome' => 'Coxilha',
				'uf' => 'RS',
				'cep2' => '4305975',
				'estado_cod' => 23,
				'cep' => '99145-000',
			),
			19 => 
			array (
				'id' => 7628,
				'nome' => 'Crissiumal',
				'uf' => 'RS',
				'cep2' => '4306007',
				'estado_cod' => 23,
				'cep' => '98640-000',
			),
			20 => 
			array (
				'id' => 7629,
				'nome' => 'Cristal',
				'uf' => 'RS',
				'cep2' => '4306056',
				'estado_cod' => 23,
				'cep' => '96195-000',
			),
			21 => 
			array (
				'id' => 7630,
				'nome' => 'Cristal do Sul',
				'uf' => 'RS',
				'cep2' => '4306072',
				'estado_cod' => 23,
				'cep' => '98368-000',
			),
			22 => 
			array (
				'id' => 7632,
				'nome' => 'Cruz Alta',
				'uf' => 'RS',
				'cep2' => '4306106',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			23 => 
			array (
				'id' => 7633,
				'nome' => 'Cruzaltense',
				'uf' => 'RS',
				'cep2' => '4306130',
				'estado_cod' => 23,
				'cep' => '99665-000',
			),
			24 => 
			array (
				'id' => 7635,
				'nome' => 'Cruzeiro do Sul',
				'uf' => 'RS',
				'cep2' => '4306205',
				'estado_cod' => 23,
				'cep' => '95930-000',
			),
			25 => 
			array (
				'id' => 7642,
				'nome' => 'David Canabarro',
				'uf' => 'RS',
				'cep2' => '4306304',
				'estado_cod' => 23,
				'cep' => '99980-000',
			),
			26 => 
			array (
				'id' => 7646,
				'nome' => 'Derrubadas',
				'uf' => 'RS',
				'cep2' => '4306320',
				'estado_cod' => 23,
				'cep' => '98528-000',
			),
			27 => 
			array (
				'id' => 7647,
				'nome' => 'Dezesseis de Novembro',
				'uf' => 'RS',
				'cep2' => '4306353',
				'estado_cod' => 23,
				'cep' => '97845-000',
			),
			28 => 
			array (
				'id' => 7648,
				'nome' => 'Dilermando de Aguiar',
				'uf' => 'RS',
				'cep2' => '4306379',
				'estado_cod' => 23,
				'cep' => '97180-000',
			),
			29 => 
			array (
				'id' => 7650,
				'nome' => 'Dois Irmãos',
				'uf' => 'RS',
				'cep2' => '4306403',
				'estado_cod' => 23,
				'cep' => '93950-000',
			),
			30 => 
			array (
				'id' => 7651,
				'nome' => 'Dois Irmãos das Missões',
				'uf' => 'RS',
				'cep2' => '4306429',
				'estado_cod' => 23,
				'cep' => '98385-000',
			),
			31 => 
			array (
				'id' => 7652,
				'nome' => 'Dois Lajeados',
				'uf' => 'RS',
				'cep2' => '4306452',
				'estado_cod' => 23,
				'cep' => '99220-000',
			),
			32 => 
			array (
				'id' => 7653,
				'nome' => 'São José do Sul',
				'uf' => 'RS',
				'cep2' => '4318614',
				'estado_cod' => 23,
				'cep' => '95748-000',
			),
			33 => 
			array (
				'id' => 7654,
				'nome' => 'Dom Feliciano',
				'uf' => 'RS',
				'cep2' => '4306502',
				'estado_cod' => 23,
				'cep' => '96190-000',
			),
			34 => 
			array (
				'id' => 7656,
				'nome' => 'Dom Pedrito',
				'uf' => 'RS',
				'cep2' => '4306601',
				'estado_cod' => 23,
				'cep' => '96450-000',
			),
			35 => 
			array (
				'id' => 7657,
				'nome' => 'Dom Pedro de Alcântara',
				'uf' => 'RS',
				'cep2' => '4306551',
				'estado_cod' => 23,
				'cep' => '95568-000',
			),
			36 => 
			array (
				'id' => 7658,
				'nome' => 'Dona Francisca',
				'uf' => 'RS',
				'cep2' => '4306700',
				'estado_cod' => 23,
				'cep' => '97280-000',
			),
			37 => 
			array (
				'id' => 7661,
				'nome' => 'Bozano',
				'uf' => 'RS',
				'cep2' => '4302584',
				'estado_cod' => 23,
				'cep' => '98733-000',
			),
			38 => 
			array (
				'id' => 7663,
				'nome' => 'Doutor Maurício Cardoso',
				'uf' => 'RS',
				'cep2' => '4306734',
				'estado_cod' => 23,
				'cep' => '98925-000',
			),
			39 => 
			array (
				'id' => 7664,
				'nome' => 'Doutor Ricardo',
				'uf' => 'RS',
				'cep2' => '4306759',
				'estado_cod' => 23,
				'cep' => '95967-000',
			),
			40 => 
			array (
				'id' => 7665,
				'nome' => 'Eldorado do Sul',
				'uf' => 'RS',
				'cep2' => '4306767',
				'estado_cod' => 23,
				'cep' => '92990-000',
			),
			41 => 
			array (
				'id' => 7667,
				'nome' => 'Encantado',
				'uf' => 'RS',
				'cep2' => '4306809',
				'estado_cod' => 23,
				'cep' => '95960-000',
			),
			42 => 
			array (
				'id' => 7669,
				'nome' => 'Encruzilhada do Sul',
				'uf' => 'RS',
				'cep2' => '4306908',
				'estado_cod' => 23,
				'cep' => '96610-000',
			),
			43 => 
			array (
				'id' => 7670,
				'nome' => 'Engenho Velho',
				'uf' => 'RS',
				'cep2' => '4306924',
				'estado_cod' => 23,
				'cep' => '99698-000',
			),
			44 => 
			array (
				'id' => 7671,
				'nome' => 'Entre Rios do Sul',
				'uf' => 'RS',
				'cep2' => '4306957',
				'estado_cod' => 23,
				'cep' => '99645-000',
			),
			45 => 
			array (
				'id' => 7672,
				'nome' => 'Entre-Ijuís',
				'uf' => 'RS',
				'cep2' => '',
				'estado_cod' => 23,
				'cep' => '98855-000',
			),
			46 => 
			array (
				'id' => 7674,
				'nome' => 'Erebango',
				'uf' => 'RS',
				'cep2' => '4306973',
				'estado_cod' => 23,
				'cep' => '99920-000',
			),
			47 => 
			array (
				'id' => 7675,
				'nome' => 'Erechim',
				'uf' => 'RS',
				'cep2' => '4307005',
				'estado_cod' => 23,
				'cep' => '99700-000',
			),
			48 => 
			array (
				'id' => 7676,
				'nome' => 'Ernestina',
				'uf' => 'RS',
				'cep2' => '4307054',
				'estado_cod' => 23,
				'cep' => '99140-000',
			),
			49 => 
			array (
				'id' => 7678,
				'nome' => 'Erval Grande',
				'uf' => 'RS',
				'cep2' => '4307203',
				'estado_cod' => 23,
				'cep' => '99750-000',
			),
			50 => 
			array (
				'id' => 7679,
				'nome' => 'Erval Seco',
				'uf' => 'RS',
				'cep2' => '4307302',
				'estado_cod' => 23,
				'cep' => '98390-000',
			),
			51 => 
			array (
				'id' => 7681,
				'nome' => 'Esmeralda',
				'uf' => 'RS',
				'cep2' => '4307401',
				'estado_cod' => 23,
				'cep' => '95380-000',
			),
			52 => 
			array (
				'id' => 7682,
				'nome' => 'Esperança do Sul',
				'uf' => 'RS',
				'cep2' => '4307450',
				'estado_cod' => 23,
				'cep' => '98635-000',
			),
			53 => 
			array (
				'id' => 7687,
				'nome' => 'Espumoso',
				'uf' => 'RS',
				'cep2' => '4307500',
				'estado_cod' => 23,
				'cep' => '99400-000',
			),
			54 => 
			array (
				'id' => 7694,
				'nome' => 'Estação',
				'uf' => 'RS',
				'cep2' => '4307559',
				'estado_cod' => 23,
				'cep' => '99930-000',
			),
			55 => 
			array (
				'id' => 7696,
				'nome' => 'Estância Velha',
				'uf' => 'RS',
				'cep2' => '4307609',
				'estado_cod' => 23,
				'cep' => '93600-000',
			),
			56 => 
			array (
				'id' => 7697,
				'nome' => 'Esteio',
				'uf' => 'RS',
				'cep2' => '4307708',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			57 => 
			array (
				'id' => 7700,
				'nome' => 'Estrela',
				'uf' => 'RS',
				'cep2' => '4307807',
				'estado_cod' => 23,
				'cep' => '95880-000',
			),
			58 => 
			array (
				'id' => 7702,
				'nome' => 'Estrela Velha',
				'uf' => 'RS',
				'cep2' => '4307815',
				'estado_cod' => 23,
				'cep' => '96990-000',
			),
			59 => 
			array (
				'id' => 7703,
				'nome' => 'Eugênio de Castro',
				'uf' => 'RS',
				'cep2' => '4307831',
				'estado_cod' => 23,
				'cep' => '98860-000',
			),
			60 => 
			array (
				'id' => 7705,
				'nome' => 'Fagundes Varela',
				'uf' => 'RS',
				'cep2' => '4307864',
				'estado_cod' => 23,
				'cep' => '95333-000',
			),
			61 => 
			array (
				'id' => 7710,
				'nome' => 'Farroupilha',
				'uf' => 'RS',
				'cep2' => '4307906',
				'estado_cod' => 23,
				'cep' => '95180-000',
			),
			62 => 
			array (
				'id' => 7712,
				'nome' => 'Faxinal do Soturno',
				'uf' => 'RS',
				'cep2' => '4308003',
				'estado_cod' => 23,
				'cep' => '97220-000',
			),
			63 => 
			array (
				'id' => 7713,
				'nome' => 'Faxinalzinho',
				'uf' => 'RS',
				'cep2' => '4308052',
				'estado_cod' => 23,
				'cep' => '99655-000',
			),
			64 => 
			array (
				'id' => 7716,
				'nome' => 'Fazenda Vilanova',
				'uf' => 'RS',
				'cep2' => '4308078',
				'estado_cod' => 23,
				'cep' => '95875-000',
			),
			65 => 
			array (
				'id' => 7717,
				'nome' => 'Feliz',
				'uf' => 'RS',
				'cep2' => '4308102',
				'estado_cod' => 23,
				'cep' => '95770-000',
			),
			66 => 
			array (
				'id' => 7719,
				'nome' => 'Flores da Cunha',
				'uf' => 'RS',
				'cep2' => '4308201',
				'estado_cod' => 23,
				'cep' => '95270-000',
			),
			67 => 
			array (
				'id' => 7722,
				'nome' => 'Floriano Peixoto',
				'uf' => 'RS',
				'cep2' => '4308250',
				'estado_cod' => 23,
				'cep' => '99910-000',
			),
			68 => 
			array (
				'id' => 7724,
				'nome' => 'Fontoura Xavier',
				'uf' => 'RS',
				'cep2' => '4308300',
				'estado_cod' => 23,
				'cep' => '99370-000',
			),
			69 => 
			array (
				'id' => 7725,
				'nome' => 'Formigueiro',
				'uf' => 'RS',
				'cep2' => '4308409',
				'estado_cod' => 23,
				'cep' => '97210-000',
			),
			70 => 
			array (
				'id' => 7728,
				'nome' => 'Forquetinha',
				'uf' => 'RS',
				'cep2' => '4308433',
				'estado_cod' => 23,
				'cep' => '95937-000',
			),
			71 => 
			array (
				'id' => 7729,
				'nome' => 'Fortaleza dos Valos',
				'uf' => 'RS',
				'cep2' => '4308458',
				'estado_cod' => 23,
				'cep' => '98125-000',
			),
			72 => 
			array (
				'id' => 7730,
				'nome' => 'Frederico Westphalen',
				'uf' => 'RS',
				'cep2' => '4308508',
				'estado_cod' => 23,
				'cep' => '98400-000',
			),
			73 => 
			array (
				'id' => 7733,
				'nome' => 'Garibaldi',
				'uf' => 'RS',
				'cep2' => '4308607',
				'estado_cod' => 23,
				'cep' => '95720-000',
			),
			74 => 
			array (
				'id' => 7735,
				'nome' => 'Garruchos',
				'uf' => 'RS',
				'cep2' => '4308656',
				'estado_cod' => 23,
				'cep' => '97690-000',
			),
			75 => 
			array (
				'id' => 7736,
				'nome' => 'Gaurama',
				'uf' => 'RS',
				'cep2' => '4308706',
				'estado_cod' => 23,
				'cep' => '99830-000',
			),
			76 => 
			array (
				'id' => 7737,
				'nome' => 'General Câmara',
				'uf' => 'RS',
				'cep2' => '4308805',
				'estado_cod' => 23,
				'cep' => '95820-000',
			),
			77 => 
			array (
				'id' => 7738,
				'nome' => 'Gentil',
				'uf' => 'RS',
				'cep2' => '4308854',
				'estado_cod' => 23,
				'cep' => '99160-000',
			),
			78 => 
			array (
				'id' => 7739,
				'nome' => 'Getúlio Vargas',
				'uf' => 'RS',
				'cep2' => '4308904',
				'estado_cod' => 23,
				'cep' => '99900-000',
			),
			79 => 
			array (
				'id' => 7740,
				'nome' => 'Giruá',
				'uf' => 'RS',
				'cep2' => '4309001',
				'estado_cod' => 23,
				'cep' => '98870-000',
			),
			80 => 
			array (
				'id' => 7742,
				'nome' => 'Glorinha',
				'uf' => 'RS',
				'cep2' => '4309050',
				'estado_cod' => 23,
				'cep' => '94380-000',
			),
			81 => 
			array (
				'id' => 7744,
				'nome' => 'Gramado',
				'uf' => 'RS',
				'cep2' => '4309100',
				'estado_cod' => 23,
				'cep' => '95670-000',
			),
			82 => 
			array (
				'id' => 7745,
				'nome' => 'Gramado dos Loureiros',
				'uf' => 'RS',
				'cep2' => '4309126',
				'estado_cod' => 23,
				'cep' => '99605-000',
			),
			83 => 
			array (
				'id' => 7747,
				'nome' => 'Gramado Xavier',
				'uf' => 'RS',
				'cep2' => '4309159',
				'estado_cod' => 23,
				'cep' => '96875-000',
			),
			84 => 
			array (
				'id' => 7748,
				'nome' => 'Gravataí',
				'uf' => 'RS',
				'cep2' => '4309209',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			85 => 
			array (
				'id' => 7749,
				'nome' => 'Guabiju',
				'uf' => 'RS',
				'cep2' => '4309258',
				'estado_cod' => 23,
				'cep' => '95355-000',
			),
			86 => 
			array (
				'id' => 7750,
				'nome' => 'Guaíba',
				'uf' => 'RS',
				'cep2' => '4309308',
				'estado_cod' => 23,
				'cep' => '92500-000',
			),
			87 => 
			array (
				'id' => 7752,
				'nome' => 'Guaporé',
				'uf' => 'RS',
				'cep2' => '4309407',
				'estado_cod' => 23,
				'cep' => '99200-000',
			),
			88 => 
			array (
				'id' => 7753,
				'nome' => 'Guarani das Missões',
				'uf' => 'RS',
				'cep2' => '4309506',
				'estado_cod' => 23,
				'cep' => '97950-000',
			),
			89 => 
			array (
				'id' => 7755,
				'nome' => 'Harmonia',
				'uf' => 'RS',
				'cep2' => '4309555',
				'estado_cod' => 23,
				'cep' => '95785-000',
			),
			90 => 
			array (
				'id' => 7756,
				'nome' => 'Herval',
				'uf' => 'RS',
				'cep2' => '4307104',
				'estado_cod' => 23,
				'cep' => '96310-000',
			),
			91 => 
			array (
				'id' => 7757,
				'nome' => 'Herveiras',
				'uf' => 'RS',
				'cep2' => '4309571',
				'estado_cod' => 23,
				'cep' => '96888-000',
			),
			92 => 
			array (
				'id' => 7759,
				'nome' => 'Horizontina',
				'uf' => 'RS',
				'cep2' => '4309605',
				'estado_cod' => 23,
				'cep' => '98920-000',
			),
			93 => 
			array (
				'id' => 7760,
				'nome' => 'Hulha Negra',
				'uf' => 'RS',
				'cep2' => '4309654',
				'estado_cod' => 23,
				'cep' => '96460-000',
			),
			94 => 
			array (
				'id' => 7761,
				'nome' => 'Humaitá',
				'uf' => 'RS',
				'cep2' => '4309704',
				'estado_cod' => 23,
				'cep' => '98670-000',
			),
			95 => 
			array (
				'id' => 7762,
				'nome' => 'Ibarama',
				'uf' => 'RS',
				'cep2' => '4309753',
				'estado_cod' => 23,
				'cep' => '96925-000',
			),
			96 => 
			array (
				'id' => 7764,
				'nome' => 'Ibiaçá',
				'uf' => 'RS',
				'cep2' => '4309803',
				'estado_cod' => 23,
				'cep' => '99940-000',
			),
			97 => 
			array (
				'id' => 7765,
				'nome' => 'Ibiraiaras',
				'uf' => 'RS',
				'cep2' => '4309902',
				'estado_cod' => 23,
				'cep' => '95305-000',
			),
			98 => 
			array (
				'id' => 7766,
				'nome' => 'Ibirapuitã',
				'uf' => 'RS',
				'cep2' => '4309951',
				'estado_cod' => 23,
				'cep' => '99320-000',
			),
			99 => 
			array (
				'id' => 7767,
				'nome' => 'Ibirubá',
				'uf' => 'RS',
				'cep2' => '4310009',
				'estado_cod' => 23,
				'cep' => '98200-000',
			),
			100 => 
			array (
				'id' => 7768,
				'nome' => 'Igrejinha',
				'uf' => 'RS',
				'cep2' => '4310108',
				'estado_cod' => 23,
				'cep' => '95650-000',
			),
			101 => 
			array (
				'id' => 7771,
				'nome' => 'Ijuí',
				'uf' => 'RS',
				'cep2' => '4310207',
				'estado_cod' => 23,
				'cep' => '98700-000',
			),
			102 => 
			array (
				'id' => 7773,
				'nome' => 'Ilópolis',
				'uf' => 'RS',
				'cep2' => '4310306',
				'estado_cod' => 23,
				'cep' => '95990-000',
			),
			103 => 
			array (
				'id' => 7774,
				'nome' => 'Imbé',
				'uf' => 'RS',
				'cep2' => '4310330',
				'estado_cod' => 23,
				'cep' => '95625-000',
			),
			104 => 
			array (
				'id' => 7775,
				'nome' => 'Imigrante',
				'uf' => 'RS',
				'cep2' => '4310363',
				'estado_cod' => 23,
				'cep' => '95885-000',
			),
			105 => 
			array (
				'id' => 7776,
				'nome' => 'Independência',
				'uf' => 'RS',
				'cep2' => '4310405',
				'estado_cod' => 23,
				'cep' => '98915-000',
			),
			106 => 
			array (
				'id' => 7777,
				'nome' => 'Inhacorá',
				'uf' => 'RS',
				'cep2' => '4310413',
				'estado_cod' => 23,
				'cep' => '98765-000',
			),
			107 => 
			array (
				'id' => 7778,
				'nome' => 'Ipê',
				'uf' => 'RS',
				'cep2' => '4310439',
				'estado_cod' => 23,
				'cep' => '95240-000',
			),
			108 => 
			array (
				'id' => 7780,
				'nome' => 'Ipiranga do Sul',
				'uf' => 'RS',
				'cep2' => '4310462',
				'estado_cod' => 23,
				'cep' => '99925-000',
			),
			109 => 
			array (
				'id' => 7782,
				'nome' => 'Iraí',
				'uf' => 'RS',
				'cep2' => '4310504',
				'estado_cod' => 23,
				'cep' => '98460-000',
			),
			110 => 
			array (
				'id' => 7784,
				'nome' => 'Itaara',
				'uf' => 'RS',
				'cep2' => '4310538',
				'estado_cod' => 23,
				'cep' => '97185-000',
			),
			111 => 
			array (
				'id' => 7786,
				'nome' => 'Itacurubi',
				'uf' => 'RS',
				'cep2' => '4310553',
				'estado_cod' => 23,
				'cep' => '97685-000',
			),
			112 => 
			array (
				'id' => 7792,
				'nome' => 'Itapuca',
				'uf' => 'RS',
				'cep2' => '4310579',
				'estado_cod' => 23,
				'cep' => '95997-000',
			),
			113 => 
			array (
				'id' => 7794,
				'nome' => 'Itaqui',
				'uf' => 'RS',
				'cep2' => '4310603',
				'estado_cod' => 23,
				'cep' => '97650-000',
			),
			114 => 
			array (
				'id' => 7795,
				'nome' => 'Itati',
				'uf' => 'RS',
				'cep2' => '4310652',
				'estado_cod' => 23,
				'cep' => '95538-000',
			),
			115 => 
			array (
				'id' => 7796,
				'nome' => 'Itatiba do Sul',
				'uf' => 'RS',
				'cep2' => '4310702',
				'estado_cod' => 23,
				'cep' => '99760-000',
			),
			116 => 
			array (
				'id' => 7800,
				'nome' => 'Ivorá',
				'uf' => 'RS',
				'cep2' => '4310751',
				'estado_cod' => 23,
				'cep' => '98160-000',
			),
			117 => 
			array (
				'id' => 7801,
				'nome' => 'Ivoti',
				'uf' => 'RS',
				'cep2' => '4310801',
				'estado_cod' => 23,
				'cep' => '93900-000',
			),
			118 => 
			array (
				'id' => 7802,
				'nome' => 'Jaboticaba',
				'uf' => 'RS',
				'cep2' => '4310850',
				'estado_cod' => 23,
				'cep' => '98350-000',
			),
			119 => 
			array (
				'id' => 7803,
				'nome' => 'Jacuizinho',
				'uf' => 'RS',
				'cep2' => '4310876',
				'estado_cod' => 23,
				'cep' => '99457-000',
			),
			120 => 
			array (
				'id' => 7804,
				'nome' => 'Jacutinga',
				'uf' => 'RS',
				'cep2' => '4310900',
				'estado_cod' => 23,
				'cep' => '99730-000',
			),
			121 => 
			array (
				'id' => 7805,
				'nome' => 'Jaguarão',
				'uf' => 'RS',
				'cep2' => '4311007',
				'estado_cod' => 23,
				'cep' => '96300-000',
			),
			122 => 
			array (
				'id' => 7807,
				'nome' => 'Jaguari',
				'uf' => 'RS',
				'cep2' => '4311106',
				'estado_cod' => 23,
				'cep' => '97760-000',
			),
			123 => 
			array (
				'id' => 7809,
				'nome' => 'Jaquirana',
				'uf' => 'RS',
				'cep2' => '4311122',
				'estado_cod' => 23,
				'cep' => '95420-000',
			),
			124 => 
			array (
				'id' => 7810,
				'nome' => 'Jari',
				'uf' => 'RS',
				'cep2' => '4311130',
				'estado_cod' => 23,
				'cep' => '98175-000',
			),
			125 => 
			array (
				'id' => 7815,
				'nome' => 'Jóia',
				'uf' => 'RS',
				'cep2' => '4311155',
				'estado_cod' => 23,
				'cep' => '98180-000',
			),
			126 => 
			array (
				'id' => 7818,
				'nome' => 'Júlio de Castilhos',
				'uf' => 'RS',
				'cep2' => '4311205',
				'estado_cod' => 23,
				'cep' => '98130-000',
			),
			127 => 
			array (
				'id' => 7819,
				'nome' => 'Lagoa Bonita do Sul',
				'uf' => 'RS',
				'cep2' => '4311239',
				'estado_cod' => 23,
				'cep' => '96920-000',
			),
			128 => 
			array (
				'id' => 7820,
				'nome' => 'Lagoa dos Três Cantos',
				'uf' => 'RS',
				'cep2' => '4311270',
				'estado_cod' => 23,
				'cep' => '99495-000',
			),
			129 => 
			array (
				'id' => 7821,
				'nome' => 'Lagoa Vermelha',
				'uf' => 'RS',
				'cep2' => '4311304',
				'estado_cod' => 23,
				'cep' => '95300-000',
			),
			130 => 
			array (
				'id' => 7822,
				'nome' => 'Lagoão',
				'uf' => 'RS',
				'cep2' => '4311254',
				'estado_cod' => 23,
				'cep' => '99340-000',
			),
			131 => 
			array (
				'id' => 7823,
				'nome' => 'Lajeado',
				'uf' => 'RS',
				'cep2' => '4311403',
				'estado_cod' => 23,
				'cep' => '95900-000',
			),
			132 => 
			array (
				'id' => 7827,
				'nome' => 'Lajeado do Bugre',
				'uf' => 'RS',
				'cep2' => '4311429',
				'estado_cod' => 23,
				'cep' => '98320-000',
			),
			133 => 
			array (
				'id' => 7833,
				'nome' => 'Lavras do Sul',
				'uf' => 'RS',
				'cep2' => '4311502',
				'estado_cod' => 23,
				'cep' => '97390-000',
			),
			134 => 
			array (
				'id' => 7835,
				'nome' => 'Liberato Salzano',
				'uf' => 'RS',
				'cep2' => '4311601',
				'estado_cod' => 23,
				'cep' => '99690-000',
			),
			135 => 
			array (
				'id' => 7836,
				'nome' => 'Lindolfo Collor',
				'uf' => 'RS',
				'cep2' => '4311627',
				'estado_cod' => 23,
				'cep' => '93940-000',
			),
			136 => 
			array (
				'id' => 7838,
				'nome' => 'Linha Nova',
				'uf' => 'RS',
				'cep2' => '4311643',
				'estado_cod' => 23,
				'cep' => '95768-000',
			),
			137 => 
			array (
				'id' => 7841,
				'nome' => 'Maçambará',
				'uf' => 'RS',
				'cep2' => '',
				'estado_cod' => 23,
				'cep' => '97645-000',
			),
			138 => 
			array (
				'id' => 7842,
				'nome' => 'Machadinho',
				'uf' => 'RS',
				'cep2' => '4311700',
				'estado_cod' => 23,
				'cep' => '99880-000',
			),
			139 => 
			array (
				'id' => 7844,
				'nome' => 'Mampituba',
				'uf' => 'RS',
				'cep2' => '4311734',
				'estado_cod' => 23,
				'cep' => '95572-000',
			),
			140 => 
			array (
				'id' => 7847,
				'nome' => 'Manoel Viana',
				'uf' => 'RS',
				'cep2' => '4311759',
				'estado_cod' => 23,
				'cep' => '97640-000',
			),
			141 => 
			array (
				'id' => 7848,
				'nome' => 'Maquiné',
				'uf' => 'RS',
				'cep2' => '4311775',
				'estado_cod' => 23,
				'cep' => '95530-000',
			),
			142 => 
			array (
				'id' => 7849,
				'nome' => 'Maratá',
				'uf' => 'RS',
				'cep2' => '4311791',
				'estado_cod' => 23,
				'cep' => '95793-000',
			),
			143 => 
			array (
				'id' => 7850,
				'nome' => 'Marau',
				'uf' => 'RS',
				'cep2' => '4311809',
				'estado_cod' => 23,
				'cep' => '99150-000',
			),
			144 => 
			array (
				'id' => 7851,
				'nome' => 'Marcelino Ramos',
				'uf' => 'RS',
				'cep2' => '4311908',
				'estado_cod' => 23,
				'cep' => '99800-000',
			),
			145 => 
			array (
				'id' => 7853,
				'nome' => 'Mariana Pimentel',
				'uf' => 'RS',
				'cep2' => '4311981',
				'estado_cod' => 23,
				'cep' => '92900-000',
			),
			146 => 
			array (
				'id' => 7854,
				'nome' => 'Mariano Moro',
				'uf' => 'RS',
				'cep2' => '4312005',
				'estado_cod' => 23,
				'cep' => '99790-000',
			),
			147 => 
			array (
				'id' => 7857,
				'nome' => 'Marques de Souza',
				'uf' => 'RS',
				'cep2' => '4312054',
				'estado_cod' => 23,
				'cep' => '95923-000',
			),
			148 => 
			array (
				'id' => 7858,
				'nome' => 'Mata',
				'uf' => 'RS',
				'cep2' => '4312104',
				'estado_cod' => 23,
				'cep' => '97410-000',
			),
			149 => 
			array (
				'id' => 7860,
				'nome' => 'Mato Castelhano',
				'uf' => 'RS',
				'cep2' => '4312138',
				'estado_cod' => 23,
				'cep' => '99180-000',
			),
			150 => 
			array (
				'id' => 7862,
				'nome' => 'Mato Leitão',
				'uf' => 'RS',
				'cep2' => '4312153',
				'estado_cod' => 23,
				'cep' => '95835-000',
			),
			151 => 
			array (
				'id' => 7863,
				'nome' => 'Mato Queimado',
				'uf' => 'RS',
				'cep2' => '4312179',
				'estado_cod' => 23,
				'cep' => '97935-000',
			),
			152 => 
			array (
				'id' => 7866,
				'nome' => 'Maximiliano de Almeida',
				'uf' => 'RS',
				'cep2' => '4312203',
				'estado_cod' => 23,
				'cep' => '99890-000',
			),
			153 => 
			array (
				'id' => 7868,
				'nome' => 'Minas do Leão',
				'uf' => 'RS',
				'cep2' => '4312252',
				'estado_cod' => 23,
				'cep' => '96755-000',
			),
			154 => 
			array (
				'id' => 7869,
				'nome' => 'Miraguaí',
				'uf' => 'RS',
				'cep2' => '4312302',
				'estado_cod' => 23,
				'cep' => '98540-000',
			),
			155 => 
			array (
				'id' => 7873,
				'nome' => 'Montauri',
				'uf' => 'RS',
				'cep2' => '4312351',
				'estado_cod' => 23,
				'cep' => '99255-000',
			),
			156 => 
			array (
				'id' => 7876,
				'nome' => 'Monte Alegre dos Campos',
				'uf' => 'RS',
				'cep2' => '4312377',
				'estado_cod' => 23,
				'cep' => '95236-000',
			),
			157 => 
			array (
				'id' => 7878,
				'nome' => 'Monte Belo do Sul',
				'uf' => 'RS',
				'cep2' => '4312385',
				'estado_cod' => 23,
				'cep' => '95718-000',
			),
			158 => 
			array (
				'id' => 7880,
				'nome' => 'Montenegro',
				'uf' => 'RS',
				'cep2' => '4312401',
				'estado_cod' => 23,
				'cep' => '95780-000',
			),
			159 => 
			array (
				'id' => 7881,
				'nome' => 'Mormaço',
				'uf' => 'RS',
				'cep2' => '4312427',
				'estado_cod' => 23,
				'cep' => '99315-000',
			),
			160 => 
			array (
				'id' => 7883,
				'nome' => 'Morrinhos do Sul',
				'uf' => 'RS',
				'cep2' => '4312443',
				'estado_cod' => 23,
				'cep' => '95577-000',
			),
			161 => 
			array (
				'id' => 7886,
				'nome' => 'Morro Redondo',
				'uf' => 'RS',
				'cep2' => '4312450',
				'estado_cod' => 23,
				'cep' => '96150-000',
			),
			162 => 
			array (
				'id' => 7887,
				'nome' => 'Morro Reuter',
				'uf' => 'RS',
				'cep2' => '4312476',
				'estado_cod' => 23,
				'cep' => '93990-000',
			),
			163 => 
			array (
				'id' => 7889,
				'nome' => 'Mostardas',
				'uf' => 'RS',
				'cep2' => '4312500',
				'estado_cod' => 23,
				'cep' => '96270-000',
			),
			164 => 
			array (
				'id' => 7890,
				'nome' => 'Muçum',
				'uf' => 'RS',
				'cep2' => '4312609',
				'estado_cod' => 23,
				'cep' => '95970-000',
			),
			165 => 
			array (
				'id' => 7891,
				'nome' => 'Muitos Capões',
				'uf' => 'RS',
				'cep2' => '4312617',
				'estado_cod' => 23,
				'cep' => '95230-000',
			),
			166 => 
			array (
				'id' => 7892,
				'nome' => 'Muliterno',
				'uf' => 'RS',
				'cep2' => '4312625',
				'estado_cod' => 23,
				'cep' => '99990-000',
			),
			167 => 
			array (
				'id' => 7893,
				'nome' => 'Não-Me-Toque',
				'uf' => 'RS',
				'cep2' => '4312658',
				'estado_cod' => 23,
				'cep' => '99470-000',
			),
			168 => 
			array (
				'id' => 7895,
				'nome' => 'Nicolau Vergueiro',
				'uf' => 'RS',
				'cep2' => '4312674',
				'estado_cod' => 23,
				'cep' => '99175-000',
			),
			169 => 
			array (
				'id' => 7896,
				'nome' => 'Nonoai',
				'uf' => 'RS',
				'cep2' => '4312708',
				'estado_cod' => 23,
				'cep' => '99600-000',
			),
			170 => 
			array (
				'id' => 7899,
				'nome' => 'Nova Alvorada',
				'uf' => 'RS',
				'cep2' => '4312757',
				'estado_cod' => 23,
				'cep' => '95985-000',
			),
			171 => 
			array (
				'id' => 7900,
				'nome' => 'Nova Araçá',
				'uf' => 'RS',
				'cep2' => '4312807',
				'estado_cod' => 23,
				'cep' => '95350-000',
			),
			172 => 
			array (
				'id' => 7901,
				'nome' => 'Nova Bassano',
				'uf' => 'RS',
				'cep2' => '4312906',
				'estado_cod' => 23,
				'cep' => '95340-000',
			),
			173 => 
			array (
				'id' => 7902,
				'nome' => 'Nova Boa Vista',
				'uf' => 'RS',
				'cep2' => '4312955',
				'estado_cod' => 23,
				'cep' => '99580-000',
			),
			174 => 
			array (
				'id' => 7903,
				'nome' => 'Nova Bréscia',
				'uf' => 'RS',
				'cep2' => '4313003',
				'estado_cod' => 23,
				'cep' => '95950-000',
			),
			175 => 
			array (
				'id' => 7904,
				'nome' => 'Nova Candelária',
				'uf' => 'RS',
				'cep2' => '4313011',
				'estado_cod' => 23,
				'cep' => '98919-000',
			),
			176 => 
			array (
				'id' => 7905,
				'nome' => 'Nova Esperança do Sul',
				'uf' => 'RS',
				'cep2' => '4313037',
				'estado_cod' => 23,
				'cep' => '97770-000',
			),
			177 => 
			array (
				'id' => 7906,
				'nome' => 'Nova Hartz',
				'uf' => 'RS',
				'cep2' => '4313060',
				'estado_cod' => 23,
				'cep' => '93890-000',
			),
			178 => 
			array (
				'id' => 7908,
				'nome' => 'Nova Pádua',
				'uf' => 'RS',
				'cep2' => '4313086',
				'estado_cod' => 23,
				'cep' => '95275-000',
			),
			179 => 
			array (
				'id' => 7909,
				'nome' => 'Nova Palma',
				'uf' => 'RS',
				'cep2' => '4313102',
				'estado_cod' => 23,
				'cep' => '97250-000',
			),
			180 => 
			array (
				'id' => 7910,
				'nome' => 'Nova Petrópolis',
				'uf' => 'RS',
				'cep2' => '4313201',
				'estado_cod' => 23,
				'cep' => '95150-000',
			),
			181 => 
			array (
				'id' => 7911,
				'nome' => 'Nova Prata',
				'uf' => 'RS',
				'cep2' => '4313300',
				'estado_cod' => 23,
				'cep' => '95320-000',
			),
			182 => 
			array (
				'id' => 7912,
				'nome' => 'Nova Ramada',
				'uf' => 'RS',
				'cep2' => '4313334',
				'estado_cod' => 23,
				'cep' => '98758-000',
			),
			183 => 
			array (
				'id' => 7913,
				'nome' => 'Nova Roma do Sul',
				'uf' => 'RS',
				'cep2' => '4313359',
				'estado_cod' => 23,
				'cep' => '95260-000',
			),
			184 => 
			array (
				'id' => 7914,
				'nome' => 'Nova Santa Rita',
				'uf' => 'RS',
				'cep2' => '4313375',
				'estado_cod' => 23,
				'cep' => '92480-000',
			),
			185 => 
			array (
				'id' => 7916,
				'nome' => 'Novo Barreiro',
				'uf' => 'RS',
				'cep2' => '4313490',
				'estado_cod' => 23,
				'cep' => '98338-000',
			),
			186 => 
			array (
				'id' => 7917,
				'nome' => 'Novo Cabrais',
				'uf' => 'RS',
				'cep2' => '4313391',
				'estado_cod' => 23,
				'cep' => '96545-000',
			),
			187 => 
			array (
				'id' => 7918,
				'nome' => 'Novo Hamburgo',
				'uf' => 'RS',
				'cep2' => '4313409',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			188 => 
			array (
				'id' => 7920,
				'nome' => 'Novo Machado',
				'uf' => 'RS',
				'cep2' => '4313425',
				'estado_cod' => 23,
				'cep' => '98955-000',
			),
			189 => 
			array (
				'id' => 7922,
				'nome' => 'Novo Tiradentes',
				'uf' => 'RS',
				'cep2' => '4313441',
				'estado_cod' => 23,
				'cep' => '98370-000',
			),
			190 => 
			array (
				'id' => 7925,
				'nome' => 'Osório',
				'uf' => 'RS',
				'cep2' => '4313508',
				'estado_cod' => 23,
				'cep' => '95520-000',
			),
			191 => 
			array (
				'id' => 7932,
				'nome' => 'Paim Filho',
				'uf' => 'RS',
				'cep2' => '4313607',
				'estado_cod' => 23,
				'cep' => '99850-000',
			),
			192 => 
			array (
				'id' => 7933,
				'nome' => 'Palmares do Sul',
				'uf' => 'RS',
				'cep2' => '4313656',
				'estado_cod' => 23,
				'cep' => '95540-000',
			),
			193 => 
			array (
				'id' => 7935,
				'nome' => 'Palmeira das Missões',
				'uf' => 'RS',
				'cep2' => '4313706',
				'estado_cod' => 23,
				'cep' => '98300-000',
			),
			194 => 
			array (
				'id' => 7936,
				'nome' => 'Palmitinho',
				'uf' => 'RS',
				'cep2' => '4313805',
				'estado_cod' => 23,
				'cep' => '98430-000',
			),
			195 => 
			array (
				'id' => 7938,
				'nome' => 'Panambi',
				'uf' => 'RS',
				'cep2' => '4313904',
				'estado_cod' => 23,
				'cep' => '98280-000',
			),
			196 => 
			array (
				'id' => 7939,
				'nome' => 'Pantano Grande',
				'uf' => 'RS',
				'cep2' => '',
				'estado_cod' => 23,
				'cep' => '96690-000',
			),
			197 => 
			array (
				'id' => 7940,
				'nome' => 'Paraí',
				'uf' => 'RS',
				'cep2' => '4314001',
				'estado_cod' => 23,
				'cep' => '95360-000',
			),
			198 => 
			array (
				'id' => 7941,
				'nome' => 'Paraíso do Sul',
				'uf' => 'RS',
				'cep2' => '4314027',
				'estado_cod' => 23,
				'cep' => '96530-000',
			),
			199 => 
			array (
				'id' => 7942,
				'nome' => 'Pareci Novo',
				'uf' => 'RS',
				'cep2' => '4314035',
				'estado_cod' => 23,
				'cep' => '95783-000',
			),
			200 => 
			array (
				'id' => 7943,
				'nome' => 'Parobé',
				'uf' => 'RS',
				'cep2' => '4314050',
				'estado_cod' => 23,
				'cep' => '95630-000',
			),
			201 => 
			array (
				'id' => 7944,
				'nome' => 'Passa Sete',
				'uf' => 'RS',
				'cep2' => '4314068',
				'estado_cod' => 23,
				'cep' => '96908-000',
			),
			202 => 
			array (
				'id' => 7952,
				'nome' => 'Passo do Sobrado',
				'uf' => 'RS',
				'cep2' => '4314076',
				'estado_cod' => 23,
				'cep' => '96685-000',
			),
			203 => 
			array (
				'id' => 7953,
				'nome' => 'Passo Fundo',
				'uf' => 'RS',
				'cep2' => '4314100',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			204 => 
			array (
				'id' => 7956,
				'nome' => 'Paulo Bento',
				'uf' => 'RS',
				'cep2' => '4314134',
				'estado_cod' => 23,
				'cep' => '99718-000',
			),
			205 => 
			array (
				'id' => 7958,
				'nome' => 'Paverama',
				'uf' => 'RS',
				'cep2' => '4314159',
				'estado_cod' => 23,
				'cep' => '95865-000',
			),
			206 => 
			array (
				'id' => 7959,
				'nome' => 'Pedras Altas',
				'uf' => 'RS',
				'cep2' => '4314175',
				'estado_cod' => 23,
				'cep' => '96487-000',
			),
			207 => 
			array (
				'id' => 7962,
				'nome' => 'Pedro Osório',
				'uf' => 'RS',
				'cep2' => '4314209',
				'estado_cod' => 23,
				'cep' => '96360-000',
			),
			208 => 
			array (
				'id' => 7964,
				'nome' => 'Pejuçara',
				'uf' => 'RS',
				'cep2' => '4314308',
				'estado_cod' => 23,
				'cep' => '98270-000',
			),
			209 => 
			array (
				'id' => 7965,
				'nome' => 'Pelotas',
				'uf' => 'RS',
				'cep2' => '4314407',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			210 => 
			array (
				'id' => 7966,
				'nome' => 'Picada Café',
				'uf' => 'RS',
				'cep2' => '4314423',
				'estado_cod' => 23,
				'cep' => '95175-000',
			),
			211 => 
			array (
				'id' => 7967,
				'nome' => 'Pinhal',
				'uf' => 'RS',
				'cep2' => '4314456',
				'estado_cod' => 23,
				'cep' => '98345-000',
			),
			212 => 
			array (
				'id' => 7972,
				'nome' => 'Pinhal da Serra',
				'uf' => 'RS',
				'cep2' => '4314464',
				'estado_cod' => 23,
				'cep' => '95390-000',
			),
			213 => 
			array (
				'id' => 7973,
				'nome' => 'Pinhal Grande',
				'uf' => 'RS',
				'cep2' => '4314472',
				'estado_cod' => 23,
				'cep' => '98150-000',
			),
			214 => 
			array (
				'id' => 7976,
				'nome' => 'Pinheirinho do Vale',
				'uf' => 'RS',
				'cep2' => '4314498',
				'estado_cod' => 23,
				'cep' => '98435-000',
			),
			215 => 
			array (
				'id' => 7977,
				'nome' => 'Pinheiro Machado',
				'uf' => 'RS',
				'cep2' => '4314506',
				'estado_cod' => 23,
				'cep' => '96470-000',
			),
			216 => 
			array (
				'id' => 7982,
				'nome' => 'Pirapó',
				'uf' => 'RS',
				'cep2' => '4314555',
				'estado_cod' => 23,
				'cep' => '97885-000',
			),
			217 => 
			array (
				'id' => 7983,
				'nome' => 'Piratini',
				'uf' => 'RS',
				'cep2' => '4314605',
				'estado_cod' => 23,
				'cep' => '96490-000',
			),
			218 => 
			array (
				'id' => 7985,
				'nome' => 'Planalto',
				'uf' => 'RS',
				'cep2' => '4314704',
				'estado_cod' => 23,
				'cep' => '98470-000',
			),
			219 => 
			array (
				'id' => 7988,
				'nome' => 'Poço das Antas',
				'uf' => 'RS',
				'cep2' => '4314753',
				'estado_cod' => 23,
				'cep' => '95740-000',
			),
			220 => 
			array (
				'id' => 7991,
				'nome' => 'Pontão',
				'uf' => 'RS',
				'cep2' => '4314779',
				'estado_cod' => 23,
				'cep' => '99190-000',
			),
			221 => 
			array (
				'id' => 7992,
				'nome' => 'Ponte Preta',
				'uf' => 'RS',
				'cep2' => '4314787',
				'estado_cod' => 23,
				'cep' => '99735-000',
			),
			222 => 
			array (
				'id' => 7993,
				'nome' => 'Portão',
				'uf' => 'RS',
				'cep2' => '4314803',
				'estado_cod' => 23,
				'cep' => '93180-000',
			),
			223 => 
			array (
				'id' => 7994,
				'nome' => 'Porto Alegre',
				'uf' => 'RS',
				'cep2' => '4314902',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			224 => 
			array (
				'id' => 7996,
				'nome' => 'Porto Lucena',
				'uf' => 'RS',
				'cep2' => '4315008',
				'estado_cod' => 23,
				'cep' => '98980-000',
			),
			225 => 
			array (
				'id' => 7997,
				'nome' => 'Porto Mauá',
				'uf' => 'RS',
				'cep2' => '4315057',
				'estado_cod' => 23,
				'cep' => '98947-000',
			),
			226 => 
			array (
				'id' => 7998,
				'nome' => 'Porto Vera Cruz',
				'uf' => 'RS',
				'cep2' => '4315073',
				'estado_cod' => 23,
				'cep' => '98985-000',
			),
			227 => 
			array (
				'id' => 7999,
				'nome' => 'Porto Xavier',
				'uf' => 'RS',
				'cep2' => '4315107',
				'estado_cod' => 23,
				'cep' => '98995-000',
			),
			228 => 
			array (
				'id' => 8000,
				'nome' => 'Pouso Novo',
				'uf' => 'RS',
				'cep2' => '4315131',
				'estado_cod' => 23,
				'cep' => '95945-000',
			),
			229 => 
			array (
				'id' => 8005,
				'nome' => 'Presidente Lucena',
				'uf' => 'RS',
				'cep2' => '4315149',
				'estado_cod' => 23,
				'cep' => '93945-000',
			),
			230 => 
			array (
				'id' => 8006,
				'nome' => 'Progresso',
				'uf' => 'RS',
				'cep2' => '4315156',
				'estado_cod' => 23,
				'cep' => '95925-000',
			),
			231 => 
			array (
				'id' => 8010,
				'nome' => 'Protásio Alves',
				'uf' => 'RS',
				'cep2' => '4315172',
				'estado_cod' => 23,
				'cep' => '95345-000',
			),
			232 => 
			array (
				'id' => 8012,
				'nome' => 'Putinga',
				'uf' => 'RS',
				'cep2' => '4315206',
				'estado_cod' => 23,
				'cep' => '95975-000',
			),
			233 => 
			array (
				'id' => 8013,
				'nome' => 'Quaraí',
				'uf' => 'RS',
				'cep2' => '4315305',
				'estado_cod' => 23,
				'cep' => '97560-000',
			),
			234 => 
			array (
				'id' => 8015,
				'nome' => 'Quatro Irmãos',
				'uf' => 'RS',
				'cep2' => '4315313',
				'estado_cod' => 23,
				'cep' => '99720-000',
			),
			235 => 
			array (
				'id' => 8016,
				'nome' => 'Quevedos',
				'uf' => 'RS',
				'cep2' => '4315321',
				'estado_cod' => 23,
				'cep' => '98140-000',
			),
			236 => 
			array (
				'id' => 8020,
				'nome' => 'Quinze de Novembro',
				'uf' => 'RS',
				'cep2' => '4315354',
				'estado_cod' => 23,
				'cep' => '98230-000',
			),
			237 => 
			array (
				'id' => 8023,
				'nome' => 'Redentora',
				'uf' => 'RS',
				'cep2' => '4315404',
				'estado_cod' => 23,
				'cep' => '98550-000',
			),
			238 => 
			array (
				'id' => 8025,
				'nome' => 'Relvado',
				'uf' => 'RS',
				'cep2' => '4315453',
				'estado_cod' => 23,
				'cep' => '95965-000',
			),
			239 => 
			array (
				'id' => 8026,
				'nome' => 'Restinga Seca',
				'uf' => 'RS',
				'cep2' => '4315503',
				'estado_cod' => 23,
				'cep' => '97200-000',
			),
			240 => 
			array (
				'id' => 8040,
				'nome' => 'Rio dos Índios',
				'uf' => 'RS',
				'cep2' => '4315552',
				'estado_cod' => 23,
				'cep' => '99610-000',
			),
			241 => 
			array (
				'id' => 8041,
				'nome' => 'Rio Grande',
				'uf' => 'RS',
				'cep2' => '4315602',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			242 => 
			array (
				'id' => 8043,
				'nome' => 'Rio Pardo',
				'uf' => 'RS',
				'cep2' => '4315701',
				'estado_cod' => 23,
				'cep' => '96640-000',
			),
			243 => 
			array (
				'id' => 8047,
				'nome' => 'Riozinho',
				'uf' => 'RS',
				'cep2' => '4315750',
				'estado_cod' => 23,
				'cep' => '95695-000',
			),
			244 => 
			array (
				'id' => 8048,
				'nome' => 'Roca Sales',
				'uf' => 'RS',
				'cep2' => '4315800',
				'estado_cod' => 23,
				'cep' => '95735-000',
			),
			245 => 
			array (
				'id' => 8049,
				'nome' => 'Rodeio Bonito',
				'uf' => 'RS',
				'cep2' => '4315909',
				'estado_cod' => 23,
				'cep' => '98360-000',
			),
			246 => 
			array (
				'id' => 8050,
				'nome' => 'Rolador',
				'uf' => 'RS',
				'cep2' => '4315958',
				'estado_cod' => 23,
				'cep' => '97843-000',
			),
			247 => 
			array (
				'id' => 8051,
				'nome' => 'Rolante',
				'uf' => 'RS',
				'cep2' => '4316006',
				'estado_cod' => 23,
				'cep' => '95690-000',
			),
			248 => 
			array (
				'id' => 8053,
				'nome' => 'Ronda Alta',
				'uf' => 'RS',
				'cep2' => '4316105',
				'estado_cod' => 23,
				'cep' => '99670-000',
			),
			249 => 
			array (
				'id' => 8054,
				'nome' => 'Rondinha',
				'uf' => 'RS',
				'cep2' => '4316204',
				'estado_cod' => 23,
				'cep' => '99590-000',
			),
			250 => 
			array (
				'id' => 8055,
				'nome' => 'Roque Gonzales',
				'uf' => 'RS',
				'cep2' => '4316303',
				'estado_cod' => 23,
				'cep' => '97970-000',
			),
			251 => 
			array (
				'id' => 8057,
				'nome' => 'Rosário do Sul',
				'uf' => 'RS',
				'cep2' => '4316402',
				'estado_cod' => 23,
				'cep' => '97590-000',
			),
			252 => 
			array (
				'id' => 8058,
				'nome' => 'Sagrada Família',
				'uf' => 'RS',
				'cep2' => '4316428',
				'estado_cod' => 23,
				'cep' => '98330-000',
			),
			253 => 
			array (
				'id' => 8060,
				'nome' => 'Saldanha Marinho',
				'uf' => 'RS',
				'cep2' => '4316436',
				'estado_cod' => 23,
				'cep' => '98250-000',
			),
			254 => 
			array (
				'id' => 8064,
				'nome' => 'Salto do Jacuí',
				'uf' => 'RS',
				'cep2' => '4316451',
				'estado_cod' => 23,
				'cep' => '99440-000',
			),
			255 => 
			array (
				'id' => 8065,
				'nome' => 'Salvador das Missões',
				'uf' => 'RS',
				'cep2' => '4316477',
				'estado_cod' => 23,
				'cep' => '97940-000',
			),
			256 => 
			array (
				'id' => 8066,
				'nome' => 'Salvador do Sul',
				'uf' => 'RS',
				'cep2' => '4316501',
				'estado_cod' => 23,
				'cep' => '95750-000',
			),
			257 => 
			array (
				'id' => 8067,
				'nome' => 'Sananduva',
				'uf' => 'RS',
				'cep2' => '4316600',
				'estado_cod' => 23,
				'cep' => '99840-000',
			),
			258 => 
			array (
				'id' => 8071,
				'nome' => 'Santa Bárbara do Sul',
				'uf' => 'RS',
				'cep2' => '4316709',
				'estado_cod' => 23,
				'cep' => '98240-000',
			),
			259 => 
			array (
				'id' => 8073,
				'nome' => 'Santa Cecília do Sul',
				'uf' => 'RS',
				'cep2' => '4316733',
				'estado_cod' => 23,
				'cep' => '99952-000',
			),
			260 => 
			array (
				'id' => 8075,
				'nome' => 'Santa Clara do Sul',
				'uf' => 'RS',
				'cep2' => '4316758',
				'estado_cod' => 23,
				'cep' => '95915-000',
			),
			261 => 
			array (
				'id' => 8079,
				'nome' => 'Santa Cruz do Sul',
				'uf' => 'RS',
				'cep2' => '4316808',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			262 => 
			array (
				'id' => 8087,
				'nome' => 'Santa Maria',
				'uf' => 'RS',
				'cep2' => '4316907',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			263 => 
			array (
				'id' => 8088,
				'nome' => 'Santa Maria do Herval',
				'uf' => 'RS',
				'cep2' => '4316956',
				'estado_cod' => 23,
				'cep' => '93995-000',
			),
			264 => 
			array (
				'id' => 8090,
				'nome' => 'Santa Rosa',
				'uf' => 'RS',
				'cep2' => '4317202',
				'estado_cod' => 23,
				'cep' => '98900-000',
			),
			265 => 
			array (
				'id' => 8094,
				'nome' => 'Santa Tereza',
				'uf' => 'RS',
				'cep2' => '4317251',
				'estado_cod' => 23,
				'cep' => '95715-000',
			),
			266 => 
			array (
				'id' => 8096,
				'nome' => 'Santa Vitória do Palmar',
				'uf' => 'RS',
				'cep2' => '4317301',
				'estado_cod' => 23,
				'cep' => '96230-000',
			),
			267 => 
			array (
				'id' => 8100,
				'nome' => 'Santana da Boa Vista',
				'uf' => 'RS',
				'cep2' => '4317004',
				'estado_cod' => 23,
				'cep' => '96590-000',
			),
			268 => 
			array (
				'id' => 8101,
				'nome' => 'Santana do Livramento',
				'uf' => 'RS',
				'cep2' => '4317103',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			269 => 
			array (
				'id' => 8102,
				'nome' => 'Santiago',
				'uf' => 'RS',
				'cep2' => '4317400',
				'estado_cod' => 23,
				'cep' => '97700-000',
			),
			270 => 
			array (
				'id' => 8104,
				'nome' => 'Santo Ângelo',
				'uf' => 'RS',
				'cep2' => '4317509',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			271 => 
			array (
				'id' => 8108,
				'nome' => 'Santo Antônio da Patrulha',
				'uf' => 'RS',
				'cep2' => '4317608',
				'estado_cod' => 23,
				'cep' => '95500-000',
			),
			272 => 
			array (
				'id' => 8109,
				'nome' => 'Santo Antônio das Missões',
				'uf' => 'RS',
				'cep2' => '4317707',
				'estado_cod' => 23,
				'cep' => '97870-000',
			),
			273 => 
			array (
				'id' => 8112,
				'nome' => 'Santo Antônio do Palma',
				'uf' => 'RS',
				'cep2' => '4317558',
				'estado_cod' => 23,
				'cep' => '99265-000',
			),
			274 => 
			array (
				'id' => 8113,
				'nome' => 'Santo Antônio do Planalto',
				'uf' => 'RS',
				'cep2' => '4317756',
				'estado_cod' => 23,
				'cep' => '99525-000',
			),
			275 => 
			array (
				'id' => 8114,
				'nome' => 'Santo Augusto',
				'uf' => 'RS',
				'cep2' => '4317806',
				'estado_cod' => 23,
				'cep' => '98590-000',
			),
			276 => 
			array (
				'id' => 8115,
				'nome' => 'Santo Cristo',
				'uf' => 'RS',
				'cep2' => '4317905',
				'estado_cod' => 23,
				'cep' => '98960-000',
			),
			277 => 
			array (
				'id' => 8116,
				'nome' => 'Santo Expedito do Sul',
				'uf' => 'RS',
				'cep2' => '4317954',
				'estado_cod' => 23,
				'cep' => '99895-000',
			),
			278 => 
			array (
				'id' => 8120,
				'nome' => 'São Borja',
				'uf' => 'RS',
				'cep2' => '4318002',
				'estado_cod' => 23,
				'cep' => '97670-000',
			),
			279 => 
			array (
				'id' => 8122,
				'nome' => 'São Domingos do Sul',
				'uf' => 'RS',
				'cep2' => '4318051',
				'estado_cod' => 23,
				'cep' => '99270-000',
			),
			280 => 
			array (
				'id' => 8124,
				'nome' => 'São Francisco de Assis',
				'uf' => 'RS',
				'cep2' => '4318101',
				'estado_cod' => 23,
				'cep' => '97610-000',
			),
			281 => 
			array (
				'id' => 8125,
				'nome' => 'São Francisco de Paula',
				'uf' => 'RS',
				'cep2' => '4318200',
				'estado_cod' => 23,
				'cep' => '95400-000',
			),
			282 => 
			array (
				'id' => 8126,
				'nome' => 'São Gabriel',
				'uf' => 'RS',
				'cep2' => '4318309',
				'estado_cod' => 23,
				'cep' => '97300-000',
			),
			283 => 
			array (
				'id' => 8127,
				'nome' => 'São Jerônimo',
				'uf' => 'RS',
				'cep2' => '4318408',
				'estado_cod' => 23,
				'cep' => '96700-000',
			),
			284 => 
			array (
				'id' => 8131,
				'nome' => 'São João da Urtiga',
				'uf' => 'RS',
				'cep2' => '4318424',
				'estado_cod' => 23,
				'cep' => '99855-000',
			),
			285 => 
			array (
				'id' => 8132,
				'nome' => 'São João do Polêsine',
				'uf' => 'RS',
				'cep2' => '4318432',
				'estado_cod' => 23,
				'cep' => '97230-000',
			),
			286 => 
			array (
				'id' => 8133,
				'nome' => 'São Jorge',
				'uf' => 'RS',
				'cep2' => '4318440',
				'estado_cod' => 23,
				'cep' => '95365-000',
			),
			287 => 
			array (
				'id' => 8137,
				'nome' => 'São José das Missões',
				'uf' => 'RS',
				'cep2' => '4318457',
				'estado_cod' => 23,
				'cep' => '98325-000',
			),
			288 => 
			array (
				'id' => 8140,
				'nome' => 'São José do Herval',
				'uf' => 'RS',
				'cep2' => '4318465',
				'estado_cod' => 23,
				'cep' => '99380-000',
			),
			289 => 
			array (
				'id' => 8141,
				'nome' => 'São José do Hortêncio',
				'uf' => 'RS',
				'cep2' => '4318481',
				'estado_cod' => 23,
				'cep' => '95755-000',
			),
			290 => 
			array (
				'id' => 8142,
				'nome' => 'São José do Inhacorá',
				'uf' => 'RS',
				'cep2' => '4318499',
				'estado_cod' => 23,
				'cep' => '98958-000',
			),
			291 => 
			array (
				'id' => 8143,
				'nome' => 'São José do Norte',
				'uf' => 'RS',
				'cep2' => '4318507',
				'estado_cod' => 23,
				'cep' => '96225-000',
			),
			292 => 
			array (
				'id' => 8144,
				'nome' => 'São José do Ouro',
				'uf' => 'RS',
				'cep2' => '4318606',
				'estado_cod' => 23,
				'cep' => '99870-000',
			),
			293 => 
			array (
				'id' => 8145,
				'nome' => 'São José dos Ausentes',
				'uf' => 'RS',
				'cep2' => '4318622',
				'estado_cod' => 23,
				'cep' => '95280-000',
			),
			294 => 
			array (
				'id' => 8146,
				'nome' => 'São Leopoldo',
				'uf' => 'RS',
				'cep2' => '4318705',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			295 => 
			array (
				'id' => 8148,
				'nome' => 'São Lourenço do Sul',
				'uf' => 'RS',
				'cep2' => '4318804',
				'estado_cod' => 23,
				'cep' => '96170-000',
			),
			296 => 
			array (
				'id' => 8151,
				'nome' => 'São Luiz Gonzaga',
				'uf' => 'RS',
				'cep2' => '4318903',
				'estado_cod' => 23,
				'cep' => '97800-000',
			),
			297 => 
			array (
				'id' => 8153,
				'nome' => 'São Marcos',
				'uf' => 'RS',
				'cep2' => '4319000',
				'estado_cod' => 23,
				'cep' => '95190-000',
			),
			298 => 
			array (
				'id' => 8155,
				'nome' => 'São Martinho',
				'uf' => 'RS',
				'cep2' => '4319109',
				'estado_cod' => 23,
				'cep' => '98690-000',
			),
			299 => 
			array (
				'id' => 8156,
				'nome' => 'São Martinho da Serra',
				'uf' => 'RS',
				'cep2' => '4319125',
				'estado_cod' => 23,
				'cep' => '97190-000',
			),
			300 => 
			array (
				'id' => 8158,
				'nome' => 'São Miguel das Missões',
				'uf' => 'RS',
				'cep2' => '4319158',
				'estado_cod' => 23,
				'cep' => '98865-000',
			),
			301 => 
			array (
				'id' => 8159,
				'nome' => 'São Nicolau',
				'uf' => 'RS',
				'cep2' => '4319208',
				'estado_cod' => 23,
				'cep' => '97880-000',
			),
			302 => 
			array (
				'id' => 8161,
				'nome' => 'São Paulo das Missões',
				'uf' => 'RS',
				'cep2' => '4319307',
				'estado_cod' => 23,
				'cep' => '97980-000',
			),
			303 => 
			array (
				'id' => 8163,
				'nome' => 'São Pedro das Missões',
				'uf' => 'RS',
				'cep2' => '4319364',
				'estado_cod' => 23,
				'cep' => '98323-000',
			),
			304 => 
			array (
				'id' => 8164,
				'nome' => 'São Pedro da Serra',
				'uf' => 'RS',
				'cep2' => '4319356',
				'estado_cod' => 23,
				'cep' => '95758-000',
			),
			305 => 
			array (
				'id' => 8165,
				'nome' => 'São Pedro do Butiá',
				'uf' => 'RS',
				'cep2' => '4319372',
				'estado_cod' => 23,
				'cep' => '97920-000',
			),
			306 => 
			array (
				'id' => 8167,
				'nome' => 'São Pedro do Sul',
				'uf' => 'RS',
				'cep2' => '4319406',
				'estado_cod' => 23,
				'cep' => '97400-000',
			),
			307 => 
			array (
				'id' => 8171,
				'nome' => 'São Sebastião do Caí',
				'uf' => 'RS',
				'cep2' => '4319505',
				'estado_cod' => 23,
				'cep' => '95760-000',
			),
			308 => 
			array (
				'id' => 8172,
				'nome' => 'São Sepé',
				'uf' => 'RS',
				'cep2' => '4319604',
				'estado_cod' => 23,
				'cep' => '97340-000',
			),
			309 => 
			array (
				'id' => 8174,
				'nome' => 'São Valentim',
				'uf' => 'RS',
				'cep2' => '4319703',
				'estado_cod' => 23,
				'cep' => '99640-000',
			),
			310 => 
			array (
				'id' => 8176,
				'nome' => 'São Valentim do Sul',
				'uf' => 'RS',
				'cep2' => '4319711',
				'estado_cod' => 23,
				'cep' => '99240-000',
			),
			311 => 
			array (
				'id' => 8177,
				'nome' => 'São Valério do Sul',
				'uf' => 'RS',
				'cep2' => '4319737',
				'estado_cod' => 23,
				'cep' => '98595-000',
			),
			312 => 
			array (
				'id' => 8178,
				'nome' => 'São Vendelino',
				'uf' => 'RS',
				'cep2' => '4319752',
				'estado_cod' => 23,
				'cep' => '95795-000',
			),
			313 => 
			array (
				'id' => 8179,
				'nome' => 'São Vicente do Sul',
				'uf' => 'RS',
				'cep2' => '4319802',
				'estado_cod' => 23,
				'cep' => '97420-000',
			),
			314 => 
			array (
				'id' => 8180,
				'nome' => 'Sapiranga',
				'uf' => 'RS',
				'cep2' => '4319901',
				'estado_cod' => 23,
				'cep' => '93800-000',
			),
			315 => 
			array (
				'id' => 8181,
				'nome' => 'Sapucaia do Sul',
				'uf' => 'RS',
				'cep2' => '4320008',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			316 => 
			array (
				'id' => 8182,
				'nome' => 'Sarandi',
				'uf' => 'RS',
				'cep2' => '4320107',
				'estado_cod' => 23,
				'cep' => '99560-000',
			),
			317 => 
			array (
				'id' => 8184,
				'nome' => 'Seberi',
				'uf' => 'RS',
				'cep2' => '4320206',
				'estado_cod' => 23,
				'cep' => '98380-000',
			),
			318 => 
			array (
				'id' => 8187,
				'nome' => 'Sede Nova',
				'uf' => 'RS',
				'cep2' => '4320230',
				'estado_cod' => 23,
				'cep' => '98675-000',
			),
			319 => 
			array (
				'id' => 8188,
				'nome' => 'Segredo',
				'uf' => 'RS',
				'cep2' => '4320263',
				'estado_cod' => 23,
				'cep' => '96910-000',
			),
			320 => 
			array (
				'id' => 8191,
				'nome' => 'Selbach',
				'uf' => 'RS',
				'cep2' => '4320305',
				'estado_cod' => 23,
				'cep' => '99450-000',
			),
			321 => 
			array (
				'id' => 8192,
				'nome' => 'Senador Salgado Filho',
				'uf' => 'RS',
				'cep2' => '4320321',
				'estado_cod' => 23,
				'cep' => '98895-000',
			),
			322 => 
			array (
				'id' => 8193,
				'nome' => 'Sentinela do Sul',
				'uf' => 'RS',
				'cep2' => '4320354',
				'estado_cod' => 23,
				'cep' => '96765-000',
			),
			323 => 
			array (
				'id' => 8195,
				'nome' => 'Serafina Corrêa',
				'uf' => 'RS',
				'cep2' => '',
				'estado_cod' => 23,
				'cep' => '99250-000',
			),
			324 => 
			array (
				'id' => 8196,
				'nome' => 'Sério',
				'uf' => 'RS',
				'cep2' => '4320453',
				'estado_cod' => 23,
				'cep' => '95918-000',
			),
			325 => 
			array (
				'id' => 8199,
				'nome' => 'Sertão',
				'uf' => 'RS',
				'cep2' => '4320503',
				'estado_cod' => 23,
				'cep' => '99170-000',
			),
			326 => 
			array (
				'id' => 8200,
				'nome' => 'Sertão Santana',
				'uf' => 'RS',
				'cep2' => '4320552',
				'estado_cod' => 23,
				'cep' => '92850-000',
			),
			327 => 
			array (
				'id' => 8202,
				'nome' => 'Sete de Setembro',
				'uf' => 'RS',
				'cep2' => '4320578',
				'estado_cod' => 23,
				'cep' => '97960-000',
			),
			328 => 
			array (
				'id' => 8206,
				'nome' => 'Severiano de Almeida',
				'uf' => 'RS',
				'cep2' => '4320602',
				'estado_cod' => 23,
				'cep' => '99810-000',
			),
			329 => 
			array (
				'id' => 8209,
				'nome' => 'Silveira Martins',
				'uf' => 'RS',
				'cep2' => '4320651',
				'estado_cod' => 23,
				'cep' => '97195-000',
			),
			330 => 
			array (
				'id' => 8210,
				'nome' => 'Sinimbu',
				'uf' => 'RS',
				'cep2' => '4320677',
				'estado_cod' => 23,
				'cep' => '96890-000',
			),
			331 => 
			array (
				'id' => 8213,
				'nome' => 'Sobradinho',
				'uf' => 'RS',
				'cep2' => '4320701',
				'estado_cod' => 23,
				'cep' => '96900-000',
			),
			332 => 
			array (
				'id' => 8214,
				'nome' => 'Soledade',
				'uf' => 'RS',
				'cep2' => '4320800',
				'estado_cod' => 23,
				'cep' => '99300-000',
			),
			333 => 
			array (
				'id' => 8217,
				'nome' => 'Tabaí',
				'uf' => 'RS',
				'cep2' => '4320859',
				'estado_cod' => 23,
				'cep' => '95863-000',
			),
			334 => 
			array (
				'id' => 8223,
				'nome' => 'Tapejara',
				'uf' => 'RS',
				'cep2' => '4320909',
				'estado_cod' => 23,
				'cep' => '99950-000',
			),
			335 => 
			array (
				'id' => 8224,
				'nome' => 'Tapera',
				'uf' => 'RS',
				'cep2' => '4321006',
				'estado_cod' => 23,
				'cep' => '99490-000',
			),
			336 => 
			array (
				'id' => 8226,
				'nome' => 'Tapes',
				'uf' => 'RS',
				'cep2' => '4321105',
				'estado_cod' => 23,
				'cep' => '96760-000',
			),
			337 => 
			array (
				'id' => 8227,
				'nome' => 'Taquara',
				'uf' => 'RS',
				'cep2' => '4321204',
				'estado_cod' => 23,
				'cep' => '95600-000',
			),
			338 => 
			array (
				'id' => 8228,
				'nome' => 'Taquari',
				'uf' => 'RS',
				'cep2' => '4321303',
				'estado_cod' => 23,
				'cep' => '95860-000',
			),
			339 => 
			array (
				'id' => 8230,
				'nome' => 'Taquaruçu do Sul',
				'uf' => 'RS',
				'cep2' => '4321329',
				'estado_cod' => 23,
				'cep' => '98410-000',
			),
			340 => 
			array (
				'id' => 8231,
				'nome' => 'Tavares',
				'uf' => 'RS',
				'cep2' => '4321352',
				'estado_cod' => 23,
				'cep' => '96290-000',
			),
			341 => 
			array (
				'id' => 8232,
				'nome' => 'Tenente Portela',
				'uf' => 'RS',
				'cep2' => '4321402',
				'estado_cod' => 23,
				'cep' => '98500-000',
			),
			342 => 
			array (
				'id' => 8233,
				'nome' => 'Terra de Areia',
				'uf' => 'RS',
				'cep2' => '4321436',
				'estado_cod' => 23,
				'cep' => '95535-000',
			),
			343 => 
			array (
				'id' => 8235,
				'nome' => 'Teutônia',
				'uf' => 'RS',
				'cep2' => '4321451',
				'estado_cod' => 23,
				'cep' => '95890-000',
			),
			344 => 
			array (
				'id' => 8238,
				'nome' => 'Tiradentes do Sul',
				'uf' => 'RS',
				'cep2' => '4321477',
				'estado_cod' => 23,
				'cep' => '98680-000',
			),
			345 => 
			array (
				'id' => 8239,
				'nome' => 'Toropi',
				'uf' => 'RS',
				'cep2' => '4321493',
				'estado_cod' => 23,
				'cep' => '97418-000',
			),
			346 => 
			array (
				'id' => 8242,
				'nome' => 'Torres',
				'uf' => 'RS',
				'cep2' => '4321501',
				'estado_cod' => 23,
				'cep' => '95560-000',
			),
			347 => 
			array (
				'id' => 8245,
				'nome' => 'Tramandaí',
				'uf' => 'RS',
				'cep2' => '4321600',
				'estado_cod' => 23,
				'cep' => '95590-000',
			),
			348 => 
			array (
				'id' => 8246,
				'nome' => 'Travesseiro',
				'uf' => 'RS',
				'cep2' => '4321626',
				'estado_cod' => 23,
				'cep' => '95948-000',
			),
			349 => 
			array (
				'id' => 8248,
				'nome' => 'Três Arroios',
				'uf' => 'RS',
				'cep2' => '4321634',
				'estado_cod' => 23,
				'cep' => '99725-000',
			),
			350 => 
			array (
				'id' => 8250,
				'nome' => 'Três Cachoeiras',
				'uf' => 'RS',
				'cep2' => '4321667',
				'estado_cod' => 23,
				'cep' => '95580-000',
			),
			351 => 
			array (
				'id' => 8251,
				'nome' => 'Três Coroas',
				'uf' => 'RS',
				'cep2' => '4321709',
				'estado_cod' => 23,
				'cep' => '95660-000',
			),
			352 => 
			array (
				'id' => 8252,
				'nome' => 'Três de Maio',
				'uf' => 'RS',
				'cep2' => '4321808',
				'estado_cod' => 23,
				'cep' => '98910-000',
			),
			353 => 
			array (
				'id' => 8253,
				'nome' => 'Três Forquilhas',
				'uf' => 'RS',
				'cep2' => '4321832',
				'estado_cod' => 23,
				'cep' => '95575-000',
			),
			354 => 
			array (
				'id' => 8254,
				'nome' => 'Três Palmeiras',
				'uf' => 'RS',
				'cep2' => '4321857',
				'estado_cod' => 23,
				'cep' => '99675-000',
			),
			355 => 
			array (
				'id' => 8255,
				'nome' => 'Três Passos',
				'uf' => 'RS',
				'cep2' => '4321907',
				'estado_cod' => 23,
				'cep' => '98600-000',
			),
			356 => 
			array (
				'id' => 8257,
				'nome' => 'Trindade do Sul',
				'uf' => 'RS',
				'cep2' => '4321956',
				'estado_cod' => 23,
				'cep' => '99615-000',
			),
			357 => 
			array (
				'id' => 8258,
				'nome' => 'Triunfo',
				'uf' => 'RS',
				'cep2' => '4322004',
				'estado_cod' => 23,
				'cep' => '95840-000',
			),
			358 => 
			array (
				'id' => 8260,
				'nome' => 'Tucunduva',
				'uf' => 'RS',
				'cep2' => '4322103',
				'estado_cod' => 23,
				'cep' => '98930-000',
			),
			359 => 
			array (
				'id' => 8262,
				'nome' => 'Tunas',
				'uf' => 'RS',
				'cep2' => '4322152',
				'estado_cod' => 23,
				'cep' => '99330-000',
			),
			360 => 
			array (
				'id' => 8264,
				'nome' => 'Tupanci do Sul',
				'uf' => 'RS',
				'cep2' => '4322186',
				'estado_cod' => 23,
				'cep' => '99878-000',
			),
			361 => 
			array (
				'id' => 8265,
				'nome' => 'Tupanciretã',
				'uf' => 'RS',
				'cep2' => '4322202',
				'estado_cod' => 23,
				'cep' => '98170-000',
			),
			362 => 
			array (
				'id' => 8267,
				'nome' => 'Tupandi',
				'uf' => 'RS',
				'cep2' => '4322251',
				'estado_cod' => 23,
				'cep' => '95775-000',
			),
			363 => 
			array (
				'id' => 8269,
				'nome' => 'Tuparendi',
				'uf' => 'RS',
				'cep2' => '4322301',
				'estado_cod' => 23,
				'cep' => '98940-000',
			),
			364 => 
			array (
				'id' => 8272,
				'nome' => 'Turuçu',
				'uf' => 'RS',
				'cep2' => '4322327',
				'estado_cod' => 23,
				'cep' => '96148-000',
			),
			365 => 
			array (
				'id' => 8274,
				'nome' => 'Ubiretama',
				'uf' => 'RS',
				'cep2' => '4322343',
				'estado_cod' => 23,
				'cep' => '98898-000',
			),
			366 => 
			array (
				'id' => 8276,
				'nome' => 'União da Serra',
				'uf' => 'RS',
				'cep2' => '4322350',
				'estado_cod' => 23,
				'cep' => '99215-000',
			),
			367 => 
			array (
				'id' => 8277,
				'nome' => 'Unistalda',
				'uf' => 'RS',
				'cep2' => '4322376',
				'estado_cod' => 23,
				'cep' => '97755-000',
			),
			368 => 
			array (
				'id' => 8278,
				'nome' => 'Uruguaiana',
				'uf' => 'RS',
				'cep2' => '4322400',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			369 => 
			array (
				'id' => 8280,
				'nome' => 'Vacaria',
				'uf' => 'RS',
				'cep2' => '4322509',
				'estado_cod' => 23,
				'cep' => '95200-000',
			),
			370 => 
			array (
				'id' => 8283,
				'nome' => 'Vale do Sol',
				'uf' => 'RS',
				'cep2' => '4322533',
				'estado_cod' => 23,
				'cep' => '96878-000',
			),
			371 => 
			array (
				'id' => 8284,
				'nome' => 'Vale Real',
				'uf' => 'RS',
				'cep2' => '4322541',
				'estado_cod' => 23,
				'cep' => '95778-000',
			),
			372 => 
			array (
				'id' => 8286,
				'nome' => 'Vale Verde',
				'uf' => 'RS',
				'cep2' => '4322525',
				'estado_cod' => 23,
				'cep' => '95833-000',
			),
			373 => 
			array (
				'id' => 8287,
				'nome' => 'Vanini',
				'uf' => 'RS',
				'cep2' => '4322558',
				'estado_cod' => 23,
				'cep' => '99290-000',
			),
			374 => 
			array (
				'id' => 8288,
				'nome' => 'Venâncio Aires',
				'uf' => 'RS',
				'cep2' => '4322608',
				'estado_cod' => 23,
				'cep' => '95800-000',
			),
			375 => 
			array (
				'id' => 8289,
				'nome' => 'Vera Cruz',
				'uf' => 'RS',
				'cep2' => '4322707',
				'estado_cod' => 23,
				'cep' => '96880-000',
			),
			376 => 
			array (
				'id' => 8290,
				'nome' => 'Veranópolis',
				'uf' => 'RS',
				'cep2' => '4322806',
				'estado_cod' => 23,
				'cep' => '95330-000',
			),
			377 => 
			array (
				'id' => 8292,
				'nome' => 'Vespasiano Correa',
				'uf' => 'RS',
				'cep2' => '4322855',
				'estado_cod' => 23,
				'cep' => '95972-000',
			),
			378 => 
			array (
				'id' => 8293,
				'nome' => 'Viadutos',
				'uf' => 'RS',
				'cep2' => '4322905',
				'estado_cod' => 23,
				'cep' => '99820-000',
			),
			379 => 
			array (
				'id' => 8294,
				'nome' => 'Viamão',
				'uf' => 'RS',
				'cep2' => '4323002',
				'estado_cod' => 23,
				'cep' => 'LOC',
			),
			380 => 
			array (
				'id' => 8295,
				'nome' => 'Vicente Dutra',
				'uf' => 'RS',
				'cep2' => '4323101',
				'estado_cod' => 23,
				'cep' => '98450-000',
			),
			381 => 
			array (
				'id' => 8296,
				'nome' => 'Victor Graeff',
				'uf' => 'RS',
				'cep2' => '4323200',
				'estado_cod' => 23,
				'cep' => '99350-000',
			),
			382 => 
			array (
				'id' => 8299,
				'nome' => 'Vila Flores',
				'uf' => 'RS',
				'cep2' => '4323309',
				'estado_cod' => 23,
				'cep' => '95334-000',
			),
			383 => 
			array (
				'id' => 8300,
				'nome' => 'Vila Lângaro',
				'uf' => 'RS',
				'cep2' => '4323358',
				'estado_cod' => 23,
				'cep' => '99955-000',
			),
			384 => 
			array (
				'id' => 8302,
				'nome' => 'Vila Maria',
				'uf' => 'RS',
				'cep2' => '4323408',
				'estado_cod' => 23,
				'cep' => '99155-000',
			),
			385 => 
			array (
				'id' => 8303,
				'nome' => 'Vila Nova do Sul',
				'uf' => 'RS',
				'cep2' => '4323457',
				'estado_cod' => 23,
				'cep' => '97385-000',
			),
			386 => 
			array (
				'id' => 8307,
				'nome' => 'Vista Alegre',
				'uf' => 'RS',
				'cep2' => '4323507',
				'estado_cod' => 23,
				'cep' => '98415-000',
			),
			387 => 
			array (
				'id' => 8309,
				'nome' => 'Vista Alegre do Prata',
				'uf' => 'RS',
				'cep2' => '4323606',
				'estado_cod' => 23,
				'cep' => '95325-000',
			),
			388 => 
			array (
				'id' => 8310,
				'nome' => 'Vista Gaúcha',
				'uf' => 'RS',
				'cep2' => '4323705',
				'estado_cod' => 23,
				'cep' => '98535-000',
			),
			389 => 
			array (
				'id' => 8312,
				'nome' => 'Vitória das Missões',
				'uf' => 'RS',
				'cep2' => '4323754',
				'estado_cod' => 23,
				'cep' => '98850-000',
			),
			390 => 
			array (
				'id' => 8317,
				'nome' => 'Xangri-Lá',
				'uf' => 'RS',
				'cep2' => '4323804',
				'estado_cod' => 23,
				'cep' => '95588-000',
			),
			391 => 
			array (
				'id' => 8318,
				'nome' => 'Novo Xingu',
				'uf' => 'RS',
				'cep2' => '4313466',
				'estado_cod' => 23,
				'cep' => '99687-000',
			),
			392 => 
			array (
				'id' => 8319,
				'nome' => 'Abdon Batista',
				'uf' => 'SC',
				'cep2' => '4200051',
				'estado_cod' => 24,
				'cep' => '89636-000',
			),
			393 => 
			array (
				'id' => 8320,
				'nome' => 'Abelardo Luz',
				'uf' => 'SC',
				'cep2' => '4200101',
				'estado_cod' => 24,
				'cep' => '89830-000',
			),
			394 => 
			array (
				'id' => 8321,
				'nome' => 'Agrolândia',
				'uf' => 'SC',
				'cep2' => '4200200',
				'estado_cod' => 24,
				'cep' => '88420-000',
			),
			395 => 
			array (
				'id' => 8322,
				'nome' => 'Agronômica',
				'uf' => 'SC',
				'cep2' => '4200309',
				'estado_cod' => 24,
				'cep' => '89188-000',
			),
			396 => 
			array (
				'id' => 8323,
				'nome' => 'Água Doce',
				'uf' => 'SC',
				'cep2' => '4200408',
				'estado_cod' => 24,
				'cep' => '89654-000',
			),
			397 => 
			array (
				'id' => 8326,
				'nome' => 'Águas de Chapecó',
				'uf' => 'SC',
				'cep2' => '4200507',
				'estado_cod' => 24,
				'cep' => '89883-000',
			),
			398 => 
			array (
				'id' => 8327,
				'nome' => 'Águas Frias',
				'uf' => 'SC',
				'cep2' => '4200556',
				'estado_cod' => 24,
				'cep' => '89843-000',
			),
			399 => 
			array (
				'id' => 8328,
				'nome' => 'Águas Mornas',
				'uf' => 'SC',
				'cep2' => '4200606',
				'estado_cod' => 24,
				'cep' => '88150-000',
			),
			400 => 
			array (
				'id' => 8331,
				'nome' => 'Alfredo Wagner',
				'uf' => 'SC',
				'cep2' => '4200705',
				'estado_cod' => 24,
				'cep' => '88450-000',
			),
			401 => 
			array (
				'id' => 8333,
				'nome' => 'Alto Bela Vista',
				'uf' => 'SC',
				'cep2' => '4200754',
				'estado_cod' => 24,
				'cep' => '89730-000',
			),
			402 => 
			array (
				'id' => 8335,
				'nome' => 'Anchieta',
				'uf' => 'SC',
				'cep2' => '4200804',
				'estado_cod' => 24,
				'cep' => '89970-000',
			),
			403 => 
			array (
				'id' => 8336,
				'nome' => 'Angelina',
				'uf' => 'SC',
				'cep2' => '4200903',
				'estado_cod' => 24,
				'cep' => '88460-000',
			),
			404 => 
			array (
				'id' => 8337,
				'nome' => 'Anita Garibaldi',
				'uf' => 'SC',
				'cep2' => '4201000',
				'estado_cod' => 24,
				'cep' => '88590-000',
			),
			405 => 
			array (
				'id' => 8338,
				'nome' => 'Anitápolis',
				'uf' => 'SC',
				'cep2' => '4201109',
				'estado_cod' => 24,
				'cep' => '88475-000',
			),
			406 => 
			array (
				'id' => 8340,
				'nome' => 'Antônio Carlos',
				'uf' => 'SC',
				'cep2' => '4201208',
				'estado_cod' => 24,
				'cep' => '88180-000',
			),
			407 => 
			array (
				'id' => 8341,
				'nome' => 'Apiúna',
				'uf' => 'SC',
				'cep2' => '4201257',
				'estado_cod' => 24,
				'cep' => '89135-000',
			),
			408 => 
			array (
				'id' => 8342,
				'nome' => 'Arabutã',
				'uf' => 'SC',
				'cep2' => '4201273',
				'estado_cod' => 24,
				'cep' => '89740-000',
			),
			409 => 
			array (
				'id' => 8343,
				'nome' => 'Araquari',
				'uf' => 'SC',
				'cep2' => '4201307',
				'estado_cod' => 24,
				'cep' => '89245-000',
			),
			410 => 
			array (
				'id' => 8344,
				'nome' => 'Araranguá',
				'uf' => 'SC',
				'cep2' => '4201406',
				'estado_cod' => 24,
				'cep' => '88900-000',
			),
			411 => 
			array (
				'id' => 8345,
				'nome' => 'Armazém',
				'uf' => 'SC',
				'cep2' => '4201505',
				'estado_cod' => 24,
				'cep' => '88740-000',
			),
			412 => 
			array (
				'id' => 8347,
				'nome' => 'Arroio Trinta',
				'uf' => 'SC',
				'cep2' => '4201604',
				'estado_cod' => 24,
				'cep' => '89590-000',
			),
			413 => 
			array (
				'id' => 8348,
				'nome' => 'Arvoredo',
				'uf' => 'SC',
				'cep2' => '4201653',
				'estado_cod' => 24,
				'cep' => '89778-000',
			),
			414 => 
			array (
				'id' => 8349,
				'nome' => 'Ascurra',
				'uf' => 'SC',
				'cep2' => '4201703',
				'estado_cod' => 24,
				'cep' => '89138-000',
			),
			415 => 
			array (
				'id' => 8350,
				'nome' => 'Atalanta',
				'uf' => 'SC',
				'cep2' => '4201802',
				'estado_cod' => 24,
				'cep' => '88410-000',
			),
			416 => 
			array (
				'id' => 8352,
				'nome' => 'Aurora',
				'uf' => 'SC',
				'cep2' => '4201901',
				'estado_cod' => 24,
				'cep' => '89186-000',
			),
			417 => 
			array (
				'id' => 8355,
				'nome' => 'Balneário Arroio do Silva',
				'uf' => 'SC',
				'cep2' => '4201950',
				'estado_cod' => 24,
				'cep' => '88914-000',
			),
			418 => 
			array (
				'id' => 8356,
				'nome' => 'Balneário Barra do Sul',
				'uf' => 'SC',
				'cep2' => '4202057',
				'estado_cod' => 24,
				'cep' => '89247-000',
			),
			419 => 
			array (
				'id' => 8357,
				'nome' => 'Balneário Camboriú',
				'uf' => 'SC',
				'cep2' => '4202008',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			420 => 
			array (
				'id' => 8358,
				'nome' => 'Balneário Gaivota',
				'uf' => 'SC',
				'cep2' => '4202073',
				'estado_cod' => 24,
				'cep' => '88955-000',
			),
			421 => 
			array (
				'id' => 8360,
				'nome' => 'Bandeirante',
				'uf' => 'SC',
				'cep2' => '4202081',
				'estado_cod' => 24,
				'cep' => '89905-000',
			),
			422 => 
			array (
				'id' => 8361,
				'nome' => 'Barra Bonita',
				'uf' => 'SC',
				'cep2' => '4202099',
				'estado_cod' => 24,
				'cep' => '89909-000',
			),
			423 => 
			array (
				'id' => 8367,
				'nome' => 'Barra Velha',
				'uf' => 'SC',
				'cep2' => '4202107',
				'estado_cod' => 24,
				'cep' => '88390-000',
			),
			424 => 
			array (
				'id' => 8373,
				'nome' => 'Bela Vista do Toldo',
				'uf' => 'SC',
				'cep2' => '4202131',
				'estado_cod' => 24,
				'cep' => '89478-000',
			),
			425 => 
			array (
				'id' => 8374,
				'nome' => 'Belmonte',
				'uf' => 'SC',
				'cep2' => '4202156',
				'estado_cod' => 24,
				'cep' => '89925-000',
			),
			426 => 
			array (
				'id' => 8375,
				'nome' => 'Benedito Novo',
				'uf' => 'SC',
				'cep2' => '4202206',
				'estado_cod' => 24,
				'cep' => '89124-000',
			),
			427 => 
			array (
				'id' => 8376,
				'nome' => 'Biguaçu',
				'uf' => 'SC',
				'cep2' => '4202305',
				'estado_cod' => 24,
				'cep' => '88160-000',
			),
			428 => 
			array (
				'id' => 8377,
				'nome' => 'Blumenau',
				'uf' => 'SC',
				'cep2' => '4202404',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			429 => 
			array (
				'id' => 8378,
				'nome' => 'Bocaína do Sul',
				'uf' => 'SC',
				'cep2' => '',
				'estado_cod' => 24,
				'cep' => '88538-000',
			),
			430 => 
			array (
				'id' => 8380,
				'nome' => 'Bom Jardim da Serra',
				'uf' => 'SC',
				'cep2' => '4202503',
				'estado_cod' => 24,
				'cep' => '88640-000',
			),
			431 => 
			array (
				'id' => 8381,
				'nome' => 'Bom Jesus',
				'uf' => 'SC',
				'cep2' => '4202537',
				'estado_cod' => 24,
				'cep' => '89824-000',
			),
			432 => 
			array (
				'id' => 8382,
				'nome' => 'Bom Jesus do Oeste',
				'uf' => 'SC',
				'cep2' => '4202578',
				'estado_cod' => 24,
				'cep' => '89873-000',
			),
			433 => 
			array (
				'id' => 8383,
				'nome' => 'Bom Retiro',
				'uf' => 'SC',
				'cep2' => '4202602',
				'estado_cod' => 24,
				'cep' => '88680-000',
			),
			434 => 
			array (
				'id' => 8385,
				'nome' => 'Bombinhas',
				'uf' => 'SC',
				'cep2' => '4202453',
				'estado_cod' => 24,
				'cep' => '88215-000',
			),
			435 => 
			array (
				'id' => 8386,
				'nome' => 'Botuverá',
				'uf' => 'SC',
				'cep2' => '4202701',
				'estado_cod' => 24,
				'cep' => '88370-000',
			),
			436 => 
			array (
				'id' => 8387,
				'nome' => 'Braço do Norte',
				'uf' => 'SC',
				'cep2' => '4202800',
				'estado_cod' => 24,
				'cep' => '88750-000',
			),
			437 => 
			array (
				'id' => 8388,
				'nome' => 'Braço do Trombudo',
				'uf' => 'SC',
				'cep2' => '4202859',
				'estado_cod' => 24,
				'cep' => '89178-000',
			),
			438 => 
			array (
				'id' => 8389,
				'nome' => 'Brunópolis',
				'uf' => 'SC',
				'cep2' => '4202875',
				'estado_cod' => 24,
				'cep' => '89634-000',
			),
			439 => 
			array (
				'id' => 8390,
				'nome' => 'Brusque',
				'uf' => 'SC',
				'cep2' => '4202909',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			440 => 
			array (
				'id' => 8391,
				'nome' => 'Caçador',
				'uf' => 'SC',
				'cep2' => '4203006',
				'estado_cod' => 24,
				'cep' => '89500-000',
			),
			441 => 
			array (
				'id' => 8394,
				'nome' => 'Caibi',
				'uf' => 'SC',
				'cep2' => '4203105',
				'estado_cod' => 24,
				'cep' => '89888-000',
			),
			442 => 
			array (
				'id' => 8395,
				'nome' => 'Calmon',
				'uf' => 'SC',
				'cep2' => '4203154',
				'estado_cod' => 24,
				'cep' => '89430-000',
			),
			443 => 
			array (
				'id' => 8396,
				'nome' => 'Camboriú',
				'uf' => 'SC',
				'cep2' => '4203204',
				'estado_cod' => 24,
				'cep' => '88340-000',
			),
			444 => 
			array (
				'id' => 8400,
				'nome' => 'Campo Alegre',
				'uf' => 'SC',
				'cep2' => '4203303',
				'estado_cod' => 24,
				'cep' => '89294-000',
			),
			445 => 
			array (
				'id' => 8401,
				'nome' => 'Campo Belo do Sul',
				'uf' => 'SC',
				'cep2' => '4203402',
				'estado_cod' => 24,
				'cep' => '88580-000',
			),
			446 => 
			array (
				'id' => 8402,
				'nome' => 'Campo Erê',
				'uf' => 'SC',
				'cep2' => '4203501',
				'estado_cod' => 24,
				'cep' => '89980-000',
			),
			447 => 
			array (
				'id' => 8403,
				'nome' => 'Campos Novos',
				'uf' => 'SC',
				'cep2' => '4203600',
				'estado_cod' => 24,
				'cep' => '89620-000',
			),
			448 => 
			array (
				'id' => 8405,
				'nome' => 'Canelinha',
				'uf' => 'SC',
				'cep2' => '4203709',
				'estado_cod' => 24,
				'cep' => '88230-000',
			),
			449 => 
			array (
				'id' => 8407,
				'nome' => 'Canoinhas',
				'uf' => 'SC',
				'cep2' => '4203808',
				'estado_cod' => 24,
				'cep' => '89460-000',
			),
			450 => 
			array (
				'id' => 8408,
				'nome' => 'Capão Alto',
				'uf' => 'SC',
				'cep2' => '4203253',
				'estado_cod' => 24,
				'cep' => '88548-000',
			),
			451 => 
			array (
				'id' => 8409,
				'nome' => 'Capinzal',
				'uf' => 'SC',
				'cep2' => '4203907',
				'estado_cod' => 24,
				'cep' => '89665-000',
			),
			452 => 
			array (
				'id' => 8410,
				'nome' => 'Capivari de Baixo',
				'uf' => 'SC',
				'cep2' => '4203956',
				'estado_cod' => 24,
				'cep' => '88745-000',
			),
			453 => 
			array (
				'id' => 8412,
				'nome' => 'Catanduvas',
				'uf' => 'SC',
				'cep2' => '4204004',
				'estado_cod' => 24,
				'cep' => '89670-000',
			),
			454 => 
			array (
				'id' => 8414,
				'nome' => 'Caxambu do Sul',
				'uf' => 'SC',
				'cep2' => '4204103',
				'estado_cod' => 24,
				'cep' => '89880-000',
			),
			455 => 
			array (
				'id' => 8416,
				'nome' => 'Celso Ramos',
				'uf' => 'SC',
				'cep2' => '4204152',
				'estado_cod' => 24,
				'cep' => '88598-000',
			),
			456 => 
			array (
				'id' => 8417,
				'nome' => 'Cerro Negro',
				'uf' => 'SC',
				'cep2' => '4204178',
				'estado_cod' => 24,
				'cep' => '88585-000',
			),
			457 => 
			array (
				'id' => 8418,
				'nome' => 'Chapadão do Lageado',
				'uf' => 'SC',
				'cep2' => '4204194',
				'estado_cod' => 24,
				'cep' => '88407-000',
			),
			458 => 
			array (
				'id' => 8419,
				'nome' => 'Chapecó',
				'uf' => 'SC',
				'cep2' => '4204202',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			459 => 
			array (
				'id' => 8421,
				'nome' => 'Cocal do Sul',
				'uf' => 'SC',
				'cep2' => '4204251',
				'estado_cod' => 24,
				'cep' => '88845-000',
			),
			460 => 
			array (
				'id' => 8424,
				'nome' => 'Concórdia',
				'uf' => 'SC',
				'cep2' => '4204301',
				'estado_cod' => 24,
				'cep' => '89700-000',
			),
			461 => 
			array (
				'id' => 8425,
				'nome' => 'Cordilheira Alta',
				'uf' => 'SC',
				'cep2' => '4204350',
				'estado_cod' => 24,
				'cep' => '89819-000',
			),
			462 => 
			array (
				'id' => 8426,
				'nome' => 'Coronel Freitas',
				'uf' => 'SC',
				'cep2' => '4204400',
				'estado_cod' => 24,
				'cep' => '89840-000',
			),
			463 => 
			array (
				'id' => 8427,
				'nome' => 'Coronel Martins',
				'uf' => 'SC',
				'cep2' => '4204459',
				'estado_cod' => 24,
				'cep' => '89837-000',
			),
			464 => 
			array (
				'id' => 8428,
				'nome' => 'Correia Pinto',
				'uf' => 'SC',
				'cep2' => '4204558',
				'estado_cod' => 24,
				'cep' => '88535-000',
			),
			465 => 
			array (
				'id' => 8429,
				'nome' => 'Corupá',
				'uf' => 'SC',
				'cep2' => '4204509',
				'estado_cod' => 24,
				'cep' => '89278-000',
			),
			466 => 
			array (
				'id' => 8430,
				'nome' => 'Criciúma',
				'uf' => 'SC',
				'cep2' => '4204608',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			467 => 
			array (
				'id' => 8431,
				'nome' => 'Cunha Porã',
				'uf' => 'SC',
				'cep2' => '4204707',
				'estado_cod' => 24,
				'cep' => '89890-000',
			),
			468 => 
			array (
				'id' => 8432,
				'nome' => 'Cunhataí',
				'uf' => 'SC',
				'cep2' => '4204756',
				'estado_cod' => 24,
				'cep' => '89886-000',
			),
			469 => 
			array (
				'id' => 8433,
				'nome' => 'Curitibanos',
				'uf' => 'SC',
				'cep2' => '4204806',
				'estado_cod' => 24,
				'cep' => '89520-000',
			),
			470 => 
			array (
				'id' => 8436,
				'nome' => 'Descanso',
				'uf' => 'SC',
				'cep2' => '4204905',
				'estado_cod' => 24,
				'cep' => '89910-000',
			),
			471 => 
			array (
				'id' => 8437,
				'nome' => 'Dionísio Cerqueira',
				'uf' => 'SC',
				'cep2' => '4205001',
				'estado_cod' => 24,
				'cep' => '89950-000',
			),
			472 => 
			array (
				'id' => 8438,
				'nome' => 'Dona Emma',
				'uf' => 'SC',
				'cep2' => '4205100',
				'estado_cod' => 24,
				'cep' => '89155-000',
			),
			473 => 
			array (
				'id' => 8439,
				'nome' => 'Doutor Pedrinho',
				'uf' => 'SC',
				'cep2' => '4205159',
				'estado_cod' => 24,
				'cep' => '89126-000',
			),
			474 => 
			array (
				'id' => 8442,
				'nome' => 'Entre Rios',
				'uf' => 'SC',
				'cep2' => '4205175',
				'estado_cod' => 24,
				'cep' => '89862-000',
			),
			475 => 
			array (
				'id' => 8443,
				'nome' => 'Ermo',
				'uf' => 'SC',
				'cep2' => '4205191',
				'estado_cod' => 24,
				'cep' => '88935-000',
			),
			476 => 
			array (
				'id' => 8444,
				'nome' => 'Erval Velho',
				'uf' => 'SC',
				'cep2' => '4205209',
				'estado_cod' => 24,
				'cep' => '89613-000',
			),
			477 => 
			array (
				'id' => 8447,
				'nome' => 'Faxinal dos Guedes',
				'uf' => 'SC',
				'cep2' => '4205308',
				'estado_cod' => 24,
				'cep' => '89694-000',
			),
			478 => 
			array (
				'id' => 8451,
				'nome' => 'Flor do Sertão',
				'uf' => 'SC',
				'cep2' => '4205357',
				'estado_cod' => 24,
				'cep' => '89878-000',
			),
			479 => 
			array (
				'id' => 8452,
				'nome' => 'Florianópolis',
				'uf' => 'SC',
				'cep2' => '4205407',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			480 => 
			array (
				'id' => 8453,
				'nome' => 'Formosa do Sul',
				'uf' => 'SC',
				'cep2' => '4205431',
				'estado_cod' => 24,
				'cep' => '89859-000',
			),
			481 => 
			array (
				'id' => 8454,
				'nome' => 'Forquilhinha',
				'uf' => 'SC',
				'cep2' => '4205456',
				'estado_cod' => 24,
				'cep' => '88850-000',
			),
			482 => 
			array (
				'id' => 8456,
				'nome' => 'Fraiburgo',
				'uf' => 'SC',
				'cep2' => '4205506',
				'estado_cod' => 24,
				'cep' => '89580-000',
			),
			483 => 
			array (
				'id' => 8458,
				'nome' => 'Frei Rogério',
				'uf' => 'SC',
				'cep2' => '4205555',
				'estado_cod' => 24,
				'cep' => '89530-000',
			),
			484 => 
			array (
				'id' => 8459,
				'nome' => 'Galvão',
				'uf' => 'SC',
				'cep2' => '4205605',
				'estado_cod' => 24,
				'cep' => '89838-000',
			),
			485 => 
			array (
				'id' => 8461,
				'nome' => 'Garopaba',
				'uf' => 'SC',
				'cep2' => '4205704',
				'estado_cod' => 24,
				'cep' => '88495-000',
			),
			486 => 
			array (
				'id' => 8462,
				'nome' => 'Garuva',
				'uf' => 'SC',
				'cep2' => '4205803',
				'estado_cod' => 24,
				'cep' => '89248-000',
			),
			487 => 
			array (
				'id' => 8463,
				'nome' => 'Gaspar',
				'uf' => 'SC',
				'cep2' => '4205902',
				'estado_cod' => 24,
				'cep' => '89110-000',
			),
			488 => 
			array (
				'id' => 8465,
				'nome' => 'Governador Celso Ramos',
				'uf' => 'SC',
				'cep2' => '4206009',
				'estado_cod' => 24,
				'cep' => '88190-000',
			),
			489 => 
			array (
				'id' => 8466,
				'nome' => 'Grão Pará',
				'uf' => 'SC',
				'cep2' => '4206108',
				'estado_cod' => 24,
				'cep' => '88890-000',
			),
			490 => 
			array (
				'id' => 8468,
				'nome' => 'Gravatal',
				'uf' => 'SC',
				'cep2' => '4206207',
				'estado_cod' => 24,
				'cep' => '88735-000',
			),
			491 => 
			array (
				'id' => 8469,
				'nome' => 'Guabiruba',
				'uf' => 'SC',
				'cep2' => '4206306',
				'estado_cod' => 24,
				'cep' => '88360-000',
			),
			492 => 
			array (
				'id' => 8471,
				'nome' => 'Guaraciaba',
				'uf' => 'SC',
				'cep2' => '4206405',
				'estado_cod' => 24,
				'cep' => '89920-000',
			),
			493 => 
			array (
				'id' => 8472,
				'nome' => 'Guaramirim',
				'uf' => 'SC',
				'cep2' => '4206504',
				'estado_cod' => 24,
				'cep' => '89270-000',
			),
			494 => 
			array (
				'id' => 8473,
				'nome' => 'Guarujá do Sul',
				'uf' => 'SC',
				'cep2' => '4206603',
				'estado_cod' => 24,
				'cep' => '89940-000',
			),
			495 => 
			array (
				'id' => 8475,
				'nome' => 'Guatambú',
				'uf' => 'SC',
				'cep2' => '',
				'estado_cod' => 24,
				'cep' => '89817-000',
			),
			496 => 
			array (
				'id' => 8478,
				'nome' => 'Herval D\'Oeste',
				'uf' => 'SC',
				'cep2' => '4206702',
				'estado_cod' => 24,
				'cep' => '89610-000',
			),
			497 => 
			array (
				'id' => 8479,
				'nome' => 'Ibiam',
				'uf' => 'SC',
				'cep2' => '4206751',
				'estado_cod' => 24,
				'cep' => '89652-000',
			),
			498 => 
			array (
				'id' => 8480,
				'nome' => 'Ibicaré',
				'uf' => 'SC',
				'cep2' => '4206801',
				'estado_cod' => 24,
				'cep' => '89640-000',
			),
			499 => 
			array (
				'id' => 8482,
				'nome' => 'Ibirama',
				'uf' => 'SC',
				'cep2' => '4206900',
				'estado_cod' => 24,
				'cep' => '89140-000',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 8483,
				'nome' => 'Içara',
				'uf' => 'SC',
				'cep2' => '4207007',
				'estado_cod' => 24,
				'cep' => '88820-000',
			),
			1 => 
			array (
				'id' => 8484,
				'nome' => 'Ilhota',
				'uf' => 'SC',
				'cep2' => '4207106',
				'estado_cod' => 24,
				'cep' => '88320-000',
			),
			2 => 
			array (
				'id' => 8485,
				'nome' => 'Imaruí',
				'uf' => 'SC',
				'cep2' => '4207205',
				'estado_cod' => 24,
				'cep' => '88770-000',
			),
			3 => 
			array (
				'id' => 8486,
				'nome' => 'Imbituba',
				'uf' => 'SC',
				'cep2' => '4207304',
				'estado_cod' => 24,
				'cep' => '88780-000',
			),
			4 => 
			array (
				'id' => 8487,
				'nome' => 'Imbuia',
				'uf' => 'SC',
				'cep2' => '4207403',
				'estado_cod' => 24,
				'cep' => '88440-000',
			),
			5 => 
			array (
				'id' => 8488,
				'nome' => 'Indaial',
				'uf' => 'SC',
				'cep2' => '4207502',
				'estado_cod' => 24,
				'cep' => '89130-000',
			),
			6 => 
			array (
				'id' => 8492,
				'nome' => 'Iomerê',
				'uf' => 'SC',
				'cep2' => '4207577',
				'estado_cod' => 24,
				'cep' => '89558-000',
			),
			7 => 
			array (
				'id' => 8493,
				'nome' => 'Ipira',
				'uf' => 'SC',
				'cep2' => '4207601',
				'estado_cod' => 24,
				'cep' => '89669-000',
			),
			8 => 
			array (
				'id' => 8495,
				'nome' => 'Iporã do Oeste',
				'uf' => 'SC',
				'cep2' => '4207650',
				'estado_cod' => 24,
				'cep' => '89899-000',
			),
			9 => 
			array (
				'id' => 8496,
				'nome' => 'Ipuaçu',
				'uf' => 'SC',
				'cep2' => '4207684',
				'estado_cod' => 24,
				'cep' => '89832-000',
			),
			10 => 
			array (
				'id' => 8497,
				'nome' => 'Ipumirim',
				'uf' => 'SC',
				'cep2' => '4207700',
				'estado_cod' => 24,
				'cep' => '89790-000',
			),
			11 => 
			array (
				'id' => 8498,
				'nome' => 'Iraceminha',
				'uf' => 'SC',
				'cep2' => '4207759',
				'estado_cod' => 24,
				'cep' => '89891-000',
			),
			12 => 
			array (
				'id' => 8500,
				'nome' => 'Irani',
				'uf' => 'SC',
				'cep2' => '4207809',
				'estado_cod' => 24,
				'cep' => '89680-000',
			),
			13 => 
			array (
				'id' => 8502,
				'nome' => 'Irati',
				'uf' => 'SC',
				'cep2' => '4207858',
				'estado_cod' => 24,
				'cep' => '89856-000',
			),
			14 => 
			array (
				'id' => 8503,
				'nome' => 'Irineópolis',
				'uf' => 'SC',
				'cep2' => '4207908',
				'estado_cod' => 24,
				'cep' => '89440-000',
			),
			15 => 
			array (
				'id' => 8504,
				'nome' => 'Itá',
				'uf' => 'SC',
				'cep2' => '4208005',
				'estado_cod' => 24,
				'cep' => '89760-000',
			),
			16 => 
			array (
				'id' => 8506,
				'nome' => 'Itaiópolis',
				'uf' => 'SC',
				'cep2' => '4208104',
				'estado_cod' => 24,
				'cep' => '89340-000',
			),
			17 => 
			array (
				'id' => 8507,
				'nome' => 'Itajaí',
				'uf' => 'SC',
				'cep2' => '4208203',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			18 => 
			array (
				'id' => 8509,
				'nome' => 'Itapema',
				'uf' => 'SC',
				'cep2' => '4208302',
				'estado_cod' => 24,
				'cep' => '88220-000',
			),
			19 => 
			array (
				'id' => 8510,
				'nome' => 'Itapiranga',
				'uf' => 'SC',
				'cep2' => '4208401',
				'estado_cod' => 24,
				'cep' => '89896-000',
			),
			20 => 
			array (
				'id' => 8511,
				'nome' => 'Itapoá',
				'uf' => 'SC',
				'cep2' => '4208450',
				'estado_cod' => 24,
				'cep' => '89249-000',
			),
			21 => 
			array (
				'id' => 8514,
				'nome' => 'Ituporanga',
				'uf' => 'SC',
				'cep2' => '4208500',
				'estado_cod' => 24,
				'cep' => '88400-000',
			),
			22 => 
			array (
				'id' => 8515,
				'nome' => 'Jaborá',
				'uf' => 'SC',
				'cep2' => '4208609',
				'estado_cod' => 24,
				'cep' => '89677-000',
			),
			23 => 
			array (
				'id' => 8516,
				'nome' => 'Jacinto Machado',
				'uf' => 'SC',
				'cep2' => '4208708',
				'estado_cod' => 24,
				'cep' => '88950-000',
			),
			24 => 
			array (
				'id' => 8517,
				'nome' => 'Jaguaruna',
				'uf' => 'SC',
				'cep2' => '4208807',
				'estado_cod' => 24,
				'cep' => '88715-000',
			),
			25 => 
			array (
				'id' => 8518,
				'nome' => 'Jaraguá do Sul',
				'uf' => 'SC',
				'cep2' => '4208906',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			26 => 
			array (
				'id' => 8519,
				'nome' => 'Jardinópolis',
				'uf' => 'SC',
				'cep2' => '4208955',
				'estado_cod' => 24,
				'cep' => '89848-000',
			),
			27 => 
			array (
				'id' => 8520,
				'nome' => 'Joaçaba',
				'uf' => 'SC',
				'cep2' => '4209003',
				'estado_cod' => 24,
				'cep' => '89600-000',
			),
			28 => 
			array (
				'id' => 8521,
				'nome' => 'Joinville',
				'uf' => 'SC',
				'cep2' => '4209102',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			29 => 
			array (
				'id' => 8522,
				'nome' => 'José Boiteux',
				'uf' => 'SC',
				'cep2' => '4209151',
				'estado_cod' => 24,
				'cep' => '89145-000',
			),
			30 => 
			array (
				'id' => 8523,
				'nome' => 'Jupiá',
				'uf' => 'SC',
				'cep2' => '4209177',
				'estado_cod' => 24,
				'cep' => '89839-000',
			),
			31 => 
			array (
				'id' => 8524,
				'nome' => 'Lacerdópolis',
				'uf' => 'SC',
				'cep2' => '4209201',
				'estado_cod' => 24,
				'cep' => '89660-000',
			),
			32 => 
			array (
				'id' => 8525,
				'nome' => 'Lages',
				'uf' => 'SC',
				'cep2' => '4209300',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			33 => 
			array (
				'id' => 8528,
				'nome' => 'Laguna',
				'uf' => 'SC',
				'cep2' => '4209409',
				'estado_cod' => 24,
				'cep' => '88790-000',
			),
			34 => 
			array (
				'id' => 8529,
				'nome' => 'Lajeado Grande',
				'uf' => 'SC',
				'cep2' => '4209458',
				'estado_cod' => 24,
				'cep' => '89828-000',
			),
			35 => 
			array (
				'id' => 8530,
				'nome' => 'Laurentino',
				'uf' => 'SC',
				'cep2' => '4209508',
				'estado_cod' => 24,
				'cep' => '89170-000',
			),
			36 => 
			array (
				'id' => 8531,
				'nome' => 'Lauro Müller',
				'uf' => 'SC',
				'cep2' => '4209607',
				'estado_cod' => 24,
				'cep' => '88880-000',
			),
			37 => 
			array (
				'id' => 8533,
				'nome' => 'Lebon Régis',
				'uf' => 'SC',
				'cep2' => '4209706',
				'estado_cod' => 24,
				'cep' => '89515-000',
			),
			38 => 
			array (
				'id' => 8534,
				'nome' => 'Leoberto Leal',
				'uf' => 'SC',
				'cep2' => '4209805',
				'estado_cod' => 24,
				'cep' => '88445-000',
			),
			39 => 
			array (
				'id' => 8535,
				'nome' => 'Lindóia do Sul',
				'uf' => 'SC',
				'cep2' => '4209854',
				'estado_cod' => 24,
				'cep' => '89735-000',
			),
			40 => 
			array (
				'id' => 8537,
				'nome' => 'Lontras',
				'uf' => 'SC',
				'cep2' => '4209904',
				'estado_cod' => 24,
				'cep' => '89182-000',
			),
			41 => 
			array (
				'id' => 8539,
				'nome' => 'Luiz Alves',
				'uf' => 'SC',
				'cep2' => '4210001',
				'estado_cod' => 24,
				'cep' => '89115-000',
			),
			42 => 
			array (
				'id' => 8540,
				'nome' => 'Luzerna',
				'uf' => 'SC',
				'cep2' => '4210035',
				'estado_cod' => 24,
				'cep' => '89609-000',
			),
			43 => 
			array (
				'id' => 8542,
				'nome' => 'Macieira',
				'uf' => 'SC',
				'cep2' => '4210050',
				'estado_cod' => 24,
				'cep' => '89518-000',
			),
			44 => 
			array (
				'id' => 8543,
				'nome' => 'Mafra',
				'uf' => 'SC',
				'cep2' => '4210100',
				'estado_cod' => 24,
				'cep' => '89300-000',
			),
			45 => 
			array (
				'id' => 8544,
				'nome' => 'Major Gercino',
				'uf' => 'SC',
				'cep2' => '4210209',
				'estado_cod' => 24,
				'cep' => '88260-000',
			),
			46 => 
			array (
				'id' => 8545,
				'nome' => 'Major Vieira',
				'uf' => 'SC',
				'cep2' => '4210308',
				'estado_cod' => 24,
				'cep' => '89480-000',
			),
			47 => 
			array (
				'id' => 8546,
				'nome' => 'Maracajá',
				'uf' => 'SC',
				'cep2' => '4210407',
				'estado_cod' => 24,
				'cep' => '88915-000',
			),
			48 => 
			array (
				'id' => 8549,
				'nome' => 'Maravilha',
				'uf' => 'SC',
				'cep2' => '4210506',
				'estado_cod' => 24,
				'cep' => '89874-000',
			),
			49 => 
			array (
				'id' => 8552,
				'nome' => 'Marema',
				'uf' => 'SC',
				'cep2' => '4210555',
				'estado_cod' => 24,
				'cep' => '89860-000',
			),
			50 => 
			array (
				'id' => 8556,
				'nome' => 'Massaranduba',
				'uf' => 'SC',
				'cep2' => '4210605',
				'estado_cod' => 24,
				'cep' => '89108-000',
			),
			51 => 
			array (
				'id' => 8557,
				'nome' => 'Matos Costa',
				'uf' => 'SC',
				'cep2' => '4210704',
				'estado_cod' => 24,
				'cep' => '89420-000',
			),
			52 => 
			array (
				'id' => 8558,
				'nome' => 'Meleiro',
				'uf' => 'SC',
				'cep2' => '4210803',
				'estado_cod' => 24,
				'cep' => '88920-000',
			),
			53 => 
			array (
				'id' => 8561,
				'nome' => 'Mirim Doce',
				'uf' => 'SC',
				'cep2' => '4210852',
				'estado_cod' => 24,
				'cep' => '89194-000',
			),
			54 => 
			array (
				'id' => 8562,
				'nome' => 'Modelo',
				'uf' => 'SC',
				'cep2' => '4210902',
				'estado_cod' => 24,
				'cep' => '89872-000',
			),
			55 => 
			array (
				'id' => 8563,
				'nome' => 'Mondaí',
				'uf' => 'SC',
				'cep2' => '4211009',
				'estado_cod' => 24,
				'cep' => '89893-000',
			),
			56 => 
			array (
				'id' => 8565,
				'nome' => 'Monte Carlo',
				'uf' => 'SC',
				'cep2' => '4211058',
				'estado_cod' => 24,
				'cep' => '89618-000',
			),
			57 => 
			array (
				'id' => 8566,
				'nome' => 'Monte Castelo',
				'uf' => 'SC',
				'cep2' => '4211108',
				'estado_cod' => 24,
				'cep' => '89380-000',
			),
			58 => 
			array (
				'id' => 8568,
				'nome' => 'Morro da Fumaça',
				'uf' => 'SC',
				'cep2' => '4211207',
				'estado_cod' => 24,
				'cep' => '88830-000',
			),
			59 => 
			array (
				'id' => 8570,
				'nome' => 'Morro Grande',
				'uf' => 'SC',
				'cep2' => '4211256',
				'estado_cod' => 24,
				'cep' => '88925-000',
			),
			60 => 
			array (
				'id' => 8571,
				'nome' => 'Navegantes',
				'uf' => 'SC',
				'cep2' => '4211306',
				'estado_cod' => 24,
				'cep' => '88375-000',
			),
			61 => 
			array (
				'id' => 8574,
				'nome' => 'Nova Erechim',
				'uf' => 'SC',
				'cep2' => '4211405',
				'estado_cod' => 24,
				'cep' => '89865-000',
			),
			62 => 
			array (
				'id' => 8576,
				'nome' => 'Nova Itaberaba',
				'uf' => 'SC',
				'cep2' => '4211454',
				'estado_cod' => 24,
				'cep' => '89818-000',
			),
			63 => 
			array (
				'id' => 8579,
				'nome' => 'Nova Trento',
				'uf' => 'SC',
				'cep2' => '4211504',
				'estado_cod' => 24,
				'cep' => '88270-000',
			),
			64 => 
			array (
				'id' => 8580,
				'nome' => 'Nova Veneza',
				'uf' => 'SC',
				'cep2' => '4211603',
				'estado_cod' => 24,
				'cep' => '88865-000',
			),
			65 => 
			array (
				'id' => 8581,
				'nome' => 'Novo Horizonte',
				'uf' => 'SC',
				'cep2' => '4211652',
				'estado_cod' => 24,
				'cep' => '89998-000',
			),
			66 => 
			array (
				'id' => 8582,
				'nome' => 'Orleans',
				'uf' => 'SC',
				'cep2' => '4211702',
				'estado_cod' => 24,
				'cep' => '88870-000',
			),
			67 => 
			array (
				'id' => 8583,
				'nome' => 'Otacílio Costa',
				'uf' => 'SC',
				'cep2' => '4211751',
				'estado_cod' => 24,
				'cep' => '88540-000',
			),
			68 => 
			array (
				'id' => 8584,
				'nome' => 'Ouro',
				'uf' => 'SC',
				'cep2' => '4211801',
				'estado_cod' => 24,
				'cep' => '89663-000',
			),
			69 => 
			array (
				'id' => 8585,
				'nome' => 'Ouro Verde',
				'uf' => 'SC',
				'cep2' => '4211850',
				'estado_cod' => 24,
				'cep' => '89834-000',
			),
			70 => 
			array (
				'id' => 8587,
				'nome' => 'Paial',
				'uf' => 'SC',
				'cep2' => '4211876',
				'estado_cod' => 24,
				'cep' => '89765-000',
			),
			71 => 
			array (
				'id' => 8588,
				'nome' => 'Painel',
				'uf' => 'SC',
				'cep2' => '4211892',
				'estado_cod' => 24,
				'cep' => '88543-000',
			),
			72 => 
			array (
				'id' => 8589,
				'nome' => 'Palhoça',
				'uf' => 'SC',
				'cep2' => '4211900',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			73 => 
			array (
				'id' => 8590,
				'nome' => 'Palma Sola',
				'uf' => 'SC',
				'cep2' => '4212007',
				'estado_cod' => 24,
				'cep' => '89985-000',
			),
			74 => 
			array (
				'id' => 8591,
				'nome' => 'Palmeira',
				'uf' => 'SC',
				'cep2' => '4212056',
				'estado_cod' => 24,
				'cep' => '88545-000',
			),
			75 => 
			array (
				'id' => 8592,
				'nome' => 'Palmitos',
				'uf' => 'SC',
				'cep2' => '4212106',
				'estado_cod' => 24,
				'cep' => '89887-000',
			),
			76 => 
			array (
				'id' => 8594,
				'nome' => 'Papanduva',
				'uf' => 'SC',
				'cep2' => '4212205',
				'estado_cod' => 24,
				'cep' => '89370-000',
			),
			77 => 
			array (
				'id' => 8595,
				'nome' => 'Paraíso',
				'uf' => 'SC',
				'cep2' => '4212239',
				'estado_cod' => 24,
				'cep' => '89906-000',
			),
			78 => 
			array (
				'id' => 8596,
				'nome' => 'Passo de Torres',
				'uf' => 'SC',
				'cep2' => '4212254',
				'estado_cod' => 24,
				'cep' => '88980-000',
			),
			79 => 
			array (
				'id' => 8598,
				'nome' => 'Passos Maia',
				'uf' => 'SC',
				'cep2' => '4212270',
				'estado_cod' => 24,
				'cep' => '89687-000',
			),
			80 => 
			array (
				'id' => 8600,
				'nome' => 'Paulo Lopes',
				'uf' => 'SC',
				'cep2' => '4212304',
				'estado_cod' => 24,
				'cep' => '88490-000',
			),
			81 => 
			array (
				'id' => 8601,
				'nome' => 'Pedras Grandes',
				'uf' => 'SC',
				'cep2' => '4212403',
				'estado_cod' => 24,
				'cep' => '88720-000',
			),
			82 => 
			array (
				'id' => 8602,
				'nome' => 'Penha',
				'uf' => 'SC',
				'cep2' => '4212502',
				'estado_cod' => 24,
				'cep' => '88385-000',
			),
			83 => 
			array (
				'id' => 8604,
				'nome' => 'Peritiba',
				'uf' => 'SC',
				'cep2' => '4212601',
				'estado_cod' => 24,
				'cep' => '89750-000',
			),
			84 => 
			array (
				'id' => 8606,
				'nome' => 'Petrolândia',
				'uf' => 'SC',
				'cep2' => '4212700',
				'estado_cod' => 24,
				'cep' => '88430-000',
			),
			85 => 
			array (
				'id' => 8607,
				'nome' => 'Balneário Piçarras',
				'uf' => 'SC',
				'cep2' => '',
				'estado_cod' => 24,
				'cep' => '88380-000',
			),
			86 => 
			array (
				'id' => 8609,
				'nome' => 'Pinhalzinho',
				'uf' => 'SC',
				'cep2' => '4212908',
				'estado_cod' => 24,
				'cep' => '89870-000',
			),
			87 => 
			array (
				'id' => 8611,
				'nome' => 'Pinheiro Preto',
				'uf' => 'SC',
				'cep2' => '4213005',
				'estado_cod' => 24,
				'cep' => '89570-000',
			),
			88 => 
			array (
				'id' => 8614,
				'nome' => 'Piratuba',
				'uf' => 'SC',
				'cep2' => '4213104',
				'estado_cod' => 24,
				'cep' => '89667-000',
			),
			89 => 
			array (
				'id' => 8616,
				'nome' => 'Planalto Alegre',
				'uf' => 'SC',
				'cep2' => '4213153',
				'estado_cod' => 24,
				'cep' => '89882-000',
			),
			90 => 
			array (
				'id' => 8618,
				'nome' => 'Pomerode',
				'uf' => 'SC',
				'cep2' => '4213203',
				'estado_cod' => 24,
				'cep' => '89107-000',
			),
			91 => 
			array (
				'id' => 8619,
				'nome' => 'Ponte Alta',
				'uf' => 'SC',
				'cep2' => '4213302',
				'estado_cod' => 24,
				'cep' => '88550-000',
			),
			92 => 
			array (
				'id' => 8620,
				'nome' => 'Ponte Alta do Norte',
				'uf' => 'SC',
				'cep2' => '4213351',
				'estado_cod' => 24,
				'cep' => '89535-000',
			),
			93 => 
			array (
				'id' => 8621,
				'nome' => 'Ponte Serrada',
				'uf' => 'SC',
				'cep2' => '4213401',
				'estado_cod' => 24,
				'cep' => '89683-000',
			),
			94 => 
			array (
				'id' => 8622,
				'nome' => 'Porto Belo',
				'uf' => 'SC',
				'cep2' => '4213500',
				'estado_cod' => 24,
				'cep' => '88210-000',
			),
			95 => 
			array (
				'id' => 8623,
				'nome' => 'Porto União',
				'uf' => 'SC',
				'cep2' => '4213609',
				'estado_cod' => 24,
				'cep' => '89400-000',
			),
			96 => 
			array (
				'id' => 8624,
				'nome' => 'Pouso Redondo',
				'uf' => 'SC',
				'cep2' => '4213708',
				'estado_cod' => 24,
				'cep' => '89172-000',
			),
			97 => 
			array (
				'id' => 8625,
				'nome' => 'Praia Grande',
				'uf' => 'SC',
				'cep2' => '4213807',
				'estado_cod' => 24,
				'cep' => '88990-000',
			),
			98 => 
			array (
				'id' => 8627,
				'nome' => 'Presidente Castelo Branco',
				'uf' => 'SC',
				'cep2' => '4213906',
				'estado_cod' => 24,
				'cep' => '89745-000',
			),
			99 => 
			array (
				'id' => 8628,
				'nome' => 'Presidente Getúlio',
				'uf' => 'SC',
				'cep2' => '4214003',
				'estado_cod' => 24,
				'cep' => '89150-000',
			),
			100 => 
			array (
				'id' => 8631,
				'nome' => 'Presidente Nereu',
				'uf' => 'SC',
				'cep2' => '4214102',
				'estado_cod' => 24,
				'cep' => '89184-000',
			),
			101 => 
			array (
				'id' => 8632,
				'nome' => 'Princesa',
				'uf' => 'SC',
				'cep2' => '4214151',
				'estado_cod' => 24,
				'cep' => '89935-000',
			),
			102 => 
			array (
				'id' => 8634,
				'nome' => 'Quilombo',
				'uf' => 'SC',
				'cep2' => '4214201',
				'estado_cod' => 24,
				'cep' => '89850-000',
			),
			103 => 
			array (
				'id' => 8636,
				'nome' => 'Rancho Queimado',
				'uf' => 'SC',
				'cep2' => '4214300',
				'estado_cod' => 24,
				'cep' => '88470-000',
			),
			104 => 
			array (
				'id' => 8646,
				'nome' => 'Rio das Antas',
				'uf' => 'SC',
				'cep2' => '4214409',
				'estado_cod' => 24,
				'cep' => '89550-000',
			),
			105 => 
			array (
				'id' => 8648,
				'nome' => 'Rio do Campo',
				'uf' => 'SC',
				'cep2' => '4214508',
				'estado_cod' => 24,
				'cep' => '89198-000',
			),
			106 => 
			array (
				'id' => 8649,
				'nome' => 'Rio do Oeste',
				'uf' => 'SC',
				'cep2' => '4214607',
				'estado_cod' => 24,
				'cep' => '89180-000',
			),
			107 => 
			array (
				'id' => 8650,
				'nome' => 'Rio do Sul',
				'uf' => 'SC',
				'cep2' => '4214805',
				'estado_cod' => 24,
				'cep' => '89160-000',
			),
			108 => 
			array (
				'id' => 8652,
				'nome' => 'Rio dos Cedros',
				'uf' => 'SC',
				'cep2' => '4214706',
				'estado_cod' => 24,
				'cep' => '89121-000',
			),
			109 => 
			array (
				'id' => 8653,
				'nome' => 'Rio Fortuna',
				'uf' => 'SC',
				'cep2' => '4214904',
				'estado_cod' => 24,
				'cep' => '88760-000',
			),
			110 => 
			array (
				'id' => 8655,
				'nome' => 'Rio Negrinho',
				'uf' => 'SC',
				'cep2' => '4215000',
				'estado_cod' => 24,
				'cep' => '89295-000',
			),
			111 => 
			array (
				'id' => 8657,
				'nome' => 'Rio Rufino',
				'uf' => 'SC',
				'cep2' => '4215059',
				'estado_cod' => 24,
				'cep' => '88658-000',
			),
			112 => 
			array (
				'id' => 8658,
				'nome' => 'Riqueza',
				'uf' => 'SC',
				'cep2' => '4215075',
				'estado_cod' => 24,
				'cep' => '89895-000',
			),
			113 => 
			array (
				'id' => 8659,
				'nome' => 'Rodeio',
				'uf' => 'SC',
				'cep2' => '4215109',
				'estado_cod' => 24,
				'cep' => '89136-000',
			),
			114 => 
			array (
				'id' => 8660,
				'nome' => 'Romelândia',
				'uf' => 'SC',
				'cep2' => '4215208',
				'estado_cod' => 24,
				'cep' => '89908-000',
			),
			115 => 
			array (
				'id' => 8662,
				'nome' => 'Salete',
				'uf' => 'SC',
				'cep2' => '4215307',
				'estado_cod' => 24,
				'cep' => '89196-000',
			),
			116 => 
			array (
				'id' => 8663,
				'nome' => 'Saltinho',
				'uf' => 'SC',
				'cep2' => '4215356',
				'estado_cod' => 24,
				'cep' => '89981-000',
			),
			117 => 
			array (
				'id' => 8664,
				'nome' => 'Salto Veloso',
				'uf' => 'SC',
				'cep2' => '4215406',
				'estado_cod' => 24,
				'cep' => '89595-000',
			),
			118 => 
			array (
				'id' => 8666,
				'nome' => 'Sangão',
				'uf' => 'SC',
				'cep2' => '4215455',
				'estado_cod' => 24,
				'cep' => '88717-000',
			),
			119 => 
			array (
				'id' => 8667,
				'nome' => 'Santa Cecília',
				'uf' => 'SC',
				'cep2' => '4215505',
				'estado_cod' => 24,
				'cep' => '89540-000',
			),
			120 => 
			array (
				'id' => 8669,
				'nome' => 'Santa Helena',
				'uf' => 'SC',
				'cep2' => '4215554',
				'estado_cod' => 24,
				'cep' => '89915-000',
			),
			121 => 
			array (
				'id' => 8675,
				'nome' => 'Santa Rosa de Lima',
				'uf' => 'SC',
				'cep2' => '4215604',
				'estado_cod' => 24,
				'cep' => '88763-000',
			),
			122 => 
			array (
				'id' => 8676,
				'nome' => 'Santa Rosa do Sul',
				'uf' => 'SC',
				'cep2' => '4215653',
				'estado_cod' => 24,
				'cep' => '88965-000',
			),
			123 => 
			array (
				'id' => 8677,
				'nome' => 'Santa Terezinha',
				'uf' => 'SC',
				'cep2' => '4215679',
				'estado_cod' => 24,
				'cep' => '89199-000',
			),
			124 => 
			array (
				'id' => 8678,
				'nome' => 'Santa Terezinha do Progresso',
				'uf' => 'SC',
				'cep2' => '4215687',
				'estado_cod' => 24,
				'cep' => '89983-000',
			),
			125 => 
			array (
				'id' => 8680,
				'nome' => 'Santiago do Sul',
				'uf' => 'SC',
				'cep2' => '4215695',
				'estado_cod' => 24,
				'cep' => '89854-000',
			),
			126 => 
			array (
				'id' => 8681,
				'nome' => 'Santo Amaro da Imperatriz',
				'uf' => 'SC',
				'cep2' => '4215703',
				'estado_cod' => 24,
				'cep' => '88140-000',
			),
			127 => 
			array (
				'id' => 8684,
				'nome' => 'São Bento do Sul',
				'uf' => 'SC',
				'cep2' => '4215802',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			128 => 
			array (
				'id' => 8685,
				'nome' => 'São Bernardino',
				'uf' => 'SC',
				'cep2' => '4215752',
				'estado_cod' => 24,
				'cep' => '89982-000',
			),
			129 => 
			array (
				'id' => 8686,
				'nome' => 'São Bonifácio',
				'uf' => 'SC',
				'cep2' => '4215901',
				'estado_cod' => 24,
				'cep' => '88485-000',
			),
			130 => 
			array (
				'id' => 8687,
				'nome' => 'São Carlos',
				'uf' => 'SC',
				'cep2' => '4216008',
				'estado_cod' => 24,
				'cep' => '89885-000',
			),
			131 => 
			array (
				'id' => 8689,
				'nome' => 'São Cristóvão do Sul',
				'uf' => 'SC',
				'cep2' => '4216057',
				'estado_cod' => 24,
				'cep' => '89533-000',
			),
			132 => 
			array (
				'id' => 8691,
				'nome' => 'São Domingos',
				'uf' => 'SC',
				'cep2' => '4216107',
				'estado_cod' => 24,
				'cep' => '89835-000',
			),
			133 => 
			array (
				'id' => 8692,
				'nome' => 'São Francisco do Sul',
				'uf' => 'SC',
				'cep2' => '4216206',
				'estado_cod' => 24,
				'cep' => '89240-000',
			),
			134 => 
			array (
				'id' => 8694,
				'nome' => 'São João Batista',
				'uf' => 'SC',
				'cep2' => '4216305',
				'estado_cod' => 24,
				'cep' => '88240-000',
			),
			135 => 
			array (
				'id' => 8695,
				'nome' => 'São João do Itaperiú',
				'uf' => 'SC',
				'cep2' => '4216354',
				'estado_cod' => 24,
				'cep' => '88395-000',
			),
			136 => 
			array (
				'id' => 8696,
				'nome' => 'São João do Oeste',
				'uf' => 'SC',
				'cep2' => '4216255',
				'estado_cod' => 24,
				'cep' => '89897-000',
			),
			137 => 
			array (
				'id' => 8698,
				'nome' => 'São João do Sul',
				'uf' => 'SC',
				'cep2' => '4216404',
				'estado_cod' => 24,
				'cep' => '88970-000',
			),
			138 => 
			array (
				'id' => 8699,
				'nome' => 'São Joaquim',
				'uf' => 'SC',
				'cep2' => '4216503',
				'estado_cod' => 24,
				'cep' => '88600-000',
			),
			139 => 
			array (
				'id' => 8700,
				'nome' => 'São José',
				'uf' => 'SC',
				'cep2' => '4216602',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			140 => 
			array (
				'id' => 8701,
				'nome' => 'São José do Cedro',
				'uf' => 'SC',
				'cep2' => '4216701',
				'estado_cod' => 24,
				'cep' => '89930-000',
			),
			141 => 
			array (
				'id' => 8702,
				'nome' => 'São José do Cerrito',
				'uf' => 'SC',
				'cep2' => '4216800',
				'estado_cod' => 24,
				'cep' => '88570-000',
			),
			142 => 
			array (
				'id' => 8705,
				'nome' => 'São Lourenço do Oeste',
				'uf' => 'SC',
				'cep2' => '4216909',
				'estado_cod' => 24,
				'cep' => '89990-000',
			),
			143 => 
			array (
				'id' => 8706,
				'nome' => 'São Ludgero',
				'uf' => 'SC',
				'cep2' => '4217006',
				'estado_cod' => 24,
				'cep' => '88730-000',
			),
			144 => 
			array (
				'id' => 8707,
				'nome' => 'São Martinho',
				'uf' => 'SC',
				'cep2' => '4217105',
				'estado_cod' => 24,
				'cep' => '88765-000',
			),
			145 => 
			array (
				'id' => 8708,
				'nome' => 'São Miguel do Oeste',
				'uf' => 'SC',
				'cep2' => '',
				'estado_cod' => 24,
				'cep' => '89900-000',
			),
			146 => 
			array (
				'id' => 8709,
				'nome' => 'São Miguel da Boa Vista',
				'uf' => 'SC',
				'cep2' => '4217154',
				'estado_cod' => 24,
				'cep' => '89879-000',
			),
			147 => 
			array (
				'id' => 8711,
				'nome' => 'São Pedro de Alcântara',
				'uf' => 'SC',
				'cep2' => '4217253',
				'estado_cod' => 24,
				'cep' => '88125-000',
			),
			148 => 
			array (
				'id' => 8717,
				'nome' => 'Saudades',
				'uf' => 'SC',
				'cep2' => '4217303',
				'estado_cod' => 24,
				'cep' => '89868-000',
			),
			149 => 
			array (
				'id' => 8718,
				'nome' => 'Schroeder',
				'uf' => 'SC',
				'cep2' => '4217402',
				'estado_cod' => 24,
				'cep' => '89275-000',
			),
			150 => 
			array (
				'id' => 8719,
				'nome' => 'Seara',
				'uf' => 'SC',
				'cep2' => '4217501',
				'estado_cod' => 24,
				'cep' => '89770-000',
			),
			151 => 
			array (
				'id' => 8721,
				'nome' => 'Serra Alta',
				'uf' => 'SC',
				'cep2' => '4217550',
				'estado_cod' => 24,
				'cep' => '89871-000',
			),
			152 => 
			array (
				'id' => 8723,
				'nome' => 'Siderópolis',
				'uf' => 'SC',
				'cep2' => '4217600',
				'estado_cod' => 24,
				'cep' => '88860-000',
			),
			153 => 
			array (
				'id' => 8724,
				'nome' => 'Sombrio',
				'uf' => 'SC',
				'cep2' => '4217709',
				'estado_cod' => 24,
				'cep' => '88960-000',
			),
			154 => 
			array (
				'id' => 8726,
				'nome' => 'Sul Brasil',
				'uf' => 'SC',
				'cep2' => '4217758',
				'estado_cod' => 24,
				'cep' => '89855-000',
			),
			155 => 
			array (
				'id' => 8727,
				'nome' => 'Taió',
				'uf' => 'SC',
				'cep2' => '4217808',
				'estado_cod' => 24,
				'cep' => '89190-000',
			),
			156 => 
			array (
				'id' => 8728,
				'nome' => 'Tangará',
				'uf' => 'SC',
				'cep2' => '4217907',
				'estado_cod' => 24,
				'cep' => '89642-000',
			),
			157 => 
			array (
				'id' => 8732,
				'nome' => 'Tigrinhos',
				'uf' => 'SC',
				'cep2' => '4217956',
				'estado_cod' => 24,
				'cep' => '89875-000',
			),
			158 => 
			array (
				'id' => 8733,
				'nome' => 'Tijucas',
				'uf' => 'SC',
				'cep2' => '4218004',
				'estado_cod' => 24,
				'cep' => '88200-000',
			),
			159 => 
			array (
				'id' => 8734,
				'nome' => 'Timbé do Sul',
				'uf' => 'SC',
				'cep2' => '4218103',
				'estado_cod' => 24,
				'cep' => '88940-000',
			),
			160 => 
			array (
				'id' => 8735,
				'nome' => 'Timbó',
				'uf' => 'SC',
				'cep2' => '4218202',
				'estado_cod' => 24,
				'cep' => '89120-000',
			),
			161 => 
			array (
				'id' => 8736,
				'nome' => 'Timbó Grande',
				'uf' => 'SC',
				'cep2' => '4218251',
				'estado_cod' => 24,
				'cep' => '89545-000',
			),
			162 => 
			array (
				'id' => 8737,
				'nome' => 'Três Barras',
				'uf' => 'SC',
				'cep2' => '4218301',
				'estado_cod' => 24,
				'cep' => '89490-000',
			),
			163 => 
			array (
				'id' => 8738,
				'nome' => 'Treviso',
				'uf' => 'SC',
				'cep2' => '4218350',
				'estado_cod' => 24,
				'cep' => '88862-000',
			),
			164 => 
			array (
				'id' => 8739,
				'nome' => 'Treze de Maio',
				'uf' => 'SC',
				'cep2' => '4218400',
				'estado_cod' => 24,
				'cep' => '88710-000',
			),
			165 => 
			array (
				'id' => 8740,
				'nome' => 'Treze Tílias',
				'uf' => 'SC',
				'cep2' => '4218509',
				'estado_cod' => 24,
				'cep' => '89650-000',
			),
			166 => 
			array (
				'id' => 8741,
				'nome' => 'Trombudo Central',
				'uf' => 'SC',
				'cep2' => '4218608',
				'estado_cod' => 24,
				'cep' => '89176-000',
			),
			167 => 
			array (
				'id' => 8742,
				'nome' => 'Tubarão',
				'uf' => 'SC',
				'cep2' => '4218707',
				'estado_cod' => 24,
				'cep' => 'LOC',
			),
			168 => 
			array (
				'id' => 8743,
				'nome' => 'Tunápolis',
				'uf' => 'SC',
				'cep2' => '4218756',
				'estado_cod' => 24,
				'cep' => '89898-000',
			),
			169 => 
			array (
				'id' => 8745,
				'nome' => 'Turvo',
				'uf' => 'SC',
				'cep2' => '4218806',
				'estado_cod' => 24,
				'cep' => '88930-000',
			),
			170 => 
			array (
				'id' => 8746,
				'nome' => 'União do Oeste',
				'uf' => 'SC',
				'cep2' => '4218855',
				'estado_cod' => 24,
				'cep' => '89845-000',
			),
			171 => 
			array (
				'id' => 8747,
				'nome' => 'Urubici',
				'uf' => 'SC',
				'cep2' => '4218905',
				'estado_cod' => 24,
				'cep' => '88650-000',
			),
			172 => 
			array (
				'id' => 8749,
				'nome' => 'Urupema',
				'uf' => 'SC',
				'cep2' => '4218954',
				'estado_cod' => 24,
				'cep' => '88625-000',
			),
			173 => 
			array (
				'id' => 8750,
				'nome' => 'Urussanga',
				'uf' => 'SC',
				'cep2' => '4219002',
				'estado_cod' => 24,
				'cep' => '88840-000',
			),
			174 => 
			array (
				'id' => 8751,
				'nome' => 'Vargeão',
				'uf' => 'SC',
				'cep2' => '4219101',
				'estado_cod' => 24,
				'cep' => '89690-000',
			),
			175 => 
			array (
				'id' => 8752,
				'nome' => 'Vargem',
				'uf' => 'SC',
				'cep2' => '4219150',
				'estado_cod' => 24,
				'cep' => '89638-000',
			),
			176 => 
			array (
				'id' => 8753,
				'nome' => 'Vargem Bonita',
				'uf' => 'SC',
				'cep2' => '4219176',
				'estado_cod' => 24,
				'cep' => '89675-000',
			),
			177 => 
			array (
				'id' => 8755,
				'nome' => 'Vidal Ramos',
				'uf' => 'SC',
				'cep2' => '4219200',
				'estado_cod' => 24,
				'cep' => '88443-000',
			),
			178 => 
			array (
				'id' => 8756,
				'nome' => 'Videira',
				'uf' => 'SC',
				'cep2' => '4219309',
				'estado_cod' => 24,
				'cep' => '89560-000',
			),
			179 => 
			array (
				'id' => 8761,
				'nome' => 'Vítor Meireles',
				'uf' => 'SC',
				'cep2' => '4219358',
				'estado_cod' => 24,
				'cep' => '89148-000',
			),
			180 => 
			array (
				'id' => 8762,
				'nome' => 'Witmarsum',
				'uf' => 'SC',
				'cep2' => '4219408',
				'estado_cod' => 24,
				'cep' => '89157-000',
			),
			181 => 
			array (
				'id' => 8763,
				'nome' => 'Xanxerê',
				'uf' => 'SC',
				'cep2' => '4219507',
				'estado_cod' => 24,
				'cep' => '89820-000',
			),
			182 => 
			array (
				'id' => 8764,
				'nome' => 'Xavantina',
				'uf' => 'SC',
				'cep2' => '4219606',
				'estado_cod' => 24,
				'cep' => '89780-000',
			),
			183 => 
			array (
				'id' => 8765,
				'nome' => 'Xaxim',
				'uf' => 'SC',
				'cep2' => '4219705',
				'estado_cod' => 24,
				'cep' => '89825-000',
			),
			184 => 
			array (
				'id' => 8766,
				'nome' => 'Zortéa',
				'uf' => 'SC',
				'cep2' => '4219853',
				'estado_cod' => 24,
				'cep' => '89633-000',
			),
			185 => 
			array (
				'id' => 8768,
				'nome' => 'Amparo de São Francisco',
				'uf' => 'SE',
				'cep2' => '2800100',
				'estado_cod' => 25,
				'cep' => '49920-000',
			),
			186 => 
			array (
				'id' => 8769,
				'nome' => 'Aquidabã',
				'uf' => 'SE',
				'cep2' => '2800209',
				'estado_cod' => 25,
				'cep' => '49790-000',
			),
			187 => 
			array (
				'id' => 8770,
				'nome' => 'Aracaju',
				'uf' => 'SE',
				'cep2' => '2800308',
				'estado_cod' => 25,
				'cep' => 'LOC',
			),
			188 => 
			array (
				'id' => 8771,
				'nome' => 'Arauá',
				'uf' => 'SE',
				'cep2' => '2800407',
				'estado_cod' => 25,
				'cep' => '49220-000',
			),
			189 => 
			array (
				'id' => 8772,
				'nome' => 'Areia Branca',
				'uf' => 'SE',
				'cep2' => '2800506',
				'estado_cod' => 25,
				'cep' => '49580-000',
			),
			190 => 
			array (
				'id' => 8774,
				'nome' => 'Barra dos Coqueiros',
				'uf' => 'SE',
				'cep2' => '2800605',
				'estado_cod' => 25,
				'cep' => '49140-000',
			),
			191 => 
			array (
				'id' => 8776,
				'nome' => 'Boquim',
				'uf' => 'SE',
				'cep2' => '2800670',
				'estado_cod' => 25,
				'cep' => '49360-000',
			),
			192 => 
			array (
				'id' => 8777,
				'nome' => 'Brejo Grande',
				'uf' => 'SE',
				'cep2' => '2800704',
				'estado_cod' => 25,
				'cep' => '49995-000',
			),
			193 => 
			array (
				'id' => 8778,
				'nome' => 'Campo do Brito',
				'uf' => 'SE',
				'cep2' => '2801009',
				'estado_cod' => 25,
				'cep' => '49520-000',
			),
			194 => 
			array (
				'id' => 8779,
				'nome' => 'Canhoba',
				'uf' => 'SE',
				'cep2' => '2801108',
				'estado_cod' => 25,
				'cep' => '49880-000',
			),
			195 => 
			array (
				'id' => 8780,
				'nome' => 'Canindé de São Francisco',
				'uf' => 'SE',
				'cep2' => '2801207',
				'estado_cod' => 25,
				'cep' => '49820-000',
			),
			196 => 
			array (
				'id' => 8781,
				'nome' => 'Capela',
				'uf' => 'SE',
				'cep2' => '2801306',
				'estado_cod' => 25,
				'cep' => '49700-000',
			),
			197 => 
			array (
				'id' => 8782,
				'nome' => 'Carira',
				'uf' => 'SE',
				'cep2' => '2801405',
				'estado_cod' => 25,
				'cep' => '49550-000',
			),
			198 => 
			array (
				'id' => 8783,
				'nome' => 'Carmópolis',
				'uf' => 'SE',
				'cep2' => '2801504',
				'estado_cod' => 25,
				'cep' => '49740-000',
			),
			199 => 
			array (
				'id' => 8784,
				'nome' => 'Cedro de São João',
				'uf' => 'SE',
				'cep2' => '2801603',
				'estado_cod' => 25,
				'cep' => '49930-000',
			),
			200 => 
			array (
				'id' => 8785,
				'nome' => 'Cristinápolis',
				'uf' => 'SE',
				'cep2' => '2801702',
				'estado_cod' => 25,
				'cep' => '49270-000',
			),
			201 => 
			array (
				'id' => 8786,
				'nome' => 'Cumbe',
				'uf' => 'SE',
				'cep2' => '2801900',
				'estado_cod' => 25,
				'cep' => '49660-000',
			),
			202 => 
			array (
				'id' => 8787,
				'nome' => 'Divina Pastora',
				'uf' => 'SE',
				'cep2' => '2802007',
				'estado_cod' => 25,
				'cep' => '49650-000',
			),
			203 => 
			array (
				'id' => 8788,
				'nome' => 'Estância',
				'uf' => 'SE',
				'cep2' => '2802106',
				'estado_cod' => 25,
				'cep' => '49200-000',
			),
			204 => 
			array (
				'id' => 8789,
				'nome' => 'Feira Nova',
				'uf' => 'SE',
				'cep2' => '2802205',
				'estado_cod' => 25,
				'cep' => '49670-000',
			),
			205 => 
			array (
				'id' => 8790,
				'nome' => 'Frei Paulo',
				'uf' => 'SE',
				'cep2' => '2802304',
				'estado_cod' => 25,
				'cep' => '49514-000',
			),
			206 => 
			array (
				'id' => 8791,
				'nome' => 'Gararu',
				'uf' => 'SE',
				'cep2' => '2802403',
				'estado_cod' => 25,
				'cep' => '49830-000',
			),
			207 => 
			array (
				'id' => 8792,
				'nome' => 'General Maynard',
				'uf' => 'SE',
				'cep2' => '2802502',
				'estado_cod' => 25,
				'cep' => '49750-000',
			),
			208 => 
			array (
				'id' => 8793,
				'nome' => 'Graccho Cardoso',
				'uf' => 'SE',
				'cep2' => '2802601',
				'estado_cod' => 25,
				'cep' => '49860-000',
			),
			209 => 
			array (
				'id' => 8794,
				'nome' => 'Ilha das Flores',
				'uf' => 'SE',
				'cep2' => '2802700',
				'estado_cod' => 25,
				'cep' => '49990-000',
			),
			210 => 
			array (
				'id' => 8795,
				'nome' => 'Indiaroba',
				'uf' => 'SE',
				'cep2' => '2802809',
				'estado_cod' => 25,
				'cep' => '49250-000',
			),
			211 => 
			array (
				'id' => 8796,
				'nome' => 'Itabaiana',
				'uf' => 'SE',
				'cep2' => '2802908',
				'estado_cod' => 25,
				'cep' => '49500-000',
			),
			212 => 
			array (
				'id' => 8797,
				'nome' => 'Itabaianinha',
				'uf' => 'SE',
				'cep2' => '2803005',
				'estado_cod' => 25,
				'cep' => '49290-000',
			),
			213 => 
			array (
				'id' => 8798,
				'nome' => 'Itabi',
				'uf' => 'SE',
				'cep2' => '2803104',
				'estado_cod' => 25,
				'cep' => '49870-000',
			),
			214 => 
			array (
				'id' => 8799,
				'nome' => 'Itaporanga D\'Ajuda',
				'uf' => 'SE',
				'cep2' => '2803203',
				'estado_cod' => 25,
				'cep' => '49120-000',
			),
			215 => 
			array (
				'id' => 8800,
				'nome' => 'Japaratuba',
				'uf' => 'SE',
				'cep2' => '2803302',
				'estado_cod' => 25,
				'cep' => '49960-000',
			),
			216 => 
			array (
				'id' => 8801,
				'nome' => 'Japoatã',
				'uf' => 'SE',
				'cep2' => '2803401',
				'estado_cod' => 25,
				'cep' => '49950-000',
			),
			217 => 
			array (
				'id' => 8802,
				'nome' => 'Lagarto',
				'uf' => 'SE',
				'cep2' => '2803500',
				'estado_cod' => 25,
				'cep' => '49400-000',
			),
			218 => 
			array (
				'id' => 8804,
				'nome' => 'Laranjeiras',
				'uf' => 'SE',
				'cep2' => '2803609',
				'estado_cod' => 25,
				'cep' => '49170-000',
			),
			219 => 
			array (
				'id' => 8805,
				'nome' => 'Macambira',
				'uf' => 'SE',
				'cep2' => '2803708',
				'estado_cod' => 25,
				'cep' => '49565-000',
			),
			220 => 
			array (
				'id' => 8806,
				'nome' => 'Malhada dos Bois',
				'uf' => 'SE',
				'cep2' => '2803807',
				'estado_cod' => 25,
				'cep' => '49940-000',
			),
			221 => 
			array (
				'id' => 8807,
				'nome' => 'Malhador',
				'uf' => 'SE',
				'cep2' => '2803906',
				'estado_cod' => 25,
				'cep' => '49570-000',
			),
			222 => 
			array (
				'id' => 8808,
				'nome' => 'Maruim',
				'uf' => 'SE',
				'cep2' => '2804003',
				'estado_cod' => 25,
				'cep' => '49770-000',
			),
			223 => 
			array (
				'id' => 8810,
				'nome' => 'Moita Bonita',
				'uf' => 'SE',
				'cep2' => '2804102',
				'estado_cod' => 25,
				'cep' => '49560-000',
			),
			224 => 
			array (
				'id' => 8811,
				'nome' => 'Monte Alegre de Sergipe',
				'uf' => 'SE',
				'cep2' => '2804201',
				'estado_cod' => 25,
				'cep' => '49690-000',
			),
			225 => 
			array (
				'id' => 8813,
				'nome' => 'Muribeca',
				'uf' => 'SE',
				'cep2' => '2804300',
				'estado_cod' => 25,
				'cep' => '49780-000',
			),
			226 => 
			array (
				'id' => 8814,
				'nome' => 'Neópolis',
				'uf' => 'SE',
				'cep2' => '2804409',
				'estado_cod' => 25,
				'cep' => '49980-000',
			),
			227 => 
			array (
				'id' => 8815,
				'nome' => 'Nossa Senhora Aparecida',
				'uf' => 'SE',
				'cep2' => '2804458',
				'estado_cod' => 25,
				'cep' => '49540-000',
			),
			228 => 
			array (
				'id' => 8816,
				'nome' => 'Nossa Senhora da Glória',
				'uf' => 'SE',
				'cep2' => '2804508',
				'estado_cod' => 25,
				'cep' => '49680-000',
			),
			229 => 
			array (
				'id' => 8817,
				'nome' => 'Nossa Senhora das Dores',
				'uf' => 'SE',
				'cep2' => '2804607',
				'estado_cod' => 25,
				'cep' => '49600-000',
			),
			230 => 
			array (
				'id' => 8818,
				'nome' => 'Nossa Senhora de Lourdes',
				'uf' => 'SE',
				'cep2' => '2804706',
				'estado_cod' => 25,
				'cep' => '49890-000',
			),
			231 => 
			array (
				'id' => 8819,
				'nome' => 'Nossa Senhora do Socorro',
				'uf' => 'SE',
				'cep2' => '2804805',
				'estado_cod' => 25,
				'cep' => '49160-000',
			),
			232 => 
			array (
				'id' => 8820,
				'nome' => 'Pacatuba',
				'uf' => 'SE',
				'cep2' => '2804904',
				'estado_cod' => 25,
				'cep' => '49970-000',
			),
			233 => 
			array (
				'id' => 8822,
				'nome' => 'Pedra Mole',
				'uf' => 'SE',
				'cep2' => '2805000',
				'estado_cod' => 25,
				'cep' => '49512-000',
			),
			234 => 
			array (
				'id' => 8824,
				'nome' => 'Pedrinhas',
				'uf' => 'SE',
				'cep2' => '2805109',
				'estado_cod' => 25,
				'cep' => '49350-000',
			),
			235 => 
			array (
				'id' => 8825,
				'nome' => 'Pinhão',
				'uf' => 'SE',
				'cep2' => '2805208',
				'estado_cod' => 25,
				'cep' => '49517-000',
			),
			236 => 
			array (
				'id' => 8826,
				'nome' => 'Pirambu',
				'uf' => 'SE',
				'cep2' => '2805307',
				'estado_cod' => 25,
				'cep' => '49190-000',
			),
			237 => 
			array (
				'id' => 8827,
				'nome' => 'Poço Redondo',
				'uf' => 'SE',
				'cep2' => '2805406',
				'estado_cod' => 25,
				'cep' => '49810-000',
			),
			238 => 
			array (
				'id' => 8828,
				'nome' => 'Poço Verde',
				'uf' => 'SE',
				'cep2' => '2805505',
				'estado_cod' => 25,
				'cep' => '49490-000',
			),
			239 => 
			array (
				'id' => 8829,
				'nome' => 'Porto da Folha',
				'uf' => 'SE',
				'cep2' => '2805604',
				'estado_cod' => 25,
				'cep' => '49800-000',
			),
			240 => 
			array (
				'id' => 8830,
				'nome' => 'Propriá',
				'uf' => 'SE',
				'cep2' => '2805703',
				'estado_cod' => 25,
				'cep' => '49900-000',
			),
			241 => 
			array (
				'id' => 8831,
				'nome' => 'Riachão do Dantas',
				'uf' => 'SE',
				'cep2' => '2805802',
				'estado_cod' => 25,
				'cep' => '49320-000',
			),
			242 => 
			array (
				'id' => 8832,
				'nome' => 'Riachuelo',
				'uf' => 'SE',
				'cep2' => '2805901',
				'estado_cod' => 25,
				'cep' => '49130-000',
			),
			243 => 
			array (
				'id' => 8833,
				'nome' => 'Ribeirópolis',
				'uf' => 'SE',
				'cep2' => '2806008',
				'estado_cod' => 25,
				'cep' => '49530-000',
			),
			244 => 
			array (
				'id' => 8834,
				'nome' => 'Rosário do Catete',
				'uf' => 'SE',
				'cep2' => '2806107',
				'estado_cod' => 25,
				'cep' => '49760-000',
			),
			245 => 
			array (
				'id' => 8835,
				'nome' => 'Salgado',
				'uf' => 'SE',
				'cep2' => '2806206',
				'estado_cod' => 25,
				'cep' => '49390-000',
			),
			246 => 
			array (
				'id' => 8837,
				'nome' => 'Santa Luzia do Itanhy',
				'uf' => 'SE',
				'cep2' => '2806305',
				'estado_cod' => 25,
				'cep' => '49230-000',
			),
			247 => 
			array (
				'id' => 8838,
				'nome' => 'Santa Rosa de Lima',
				'uf' => 'SE',
				'cep2' => '2806503',
				'estado_cod' => 25,
				'cep' => '49640-000',
			),
			248 => 
			array (
				'id' => 8839,
				'nome' => 'Santana do São Francisco',
				'uf' => 'SE',
				'cep2' => '2806404',
				'estado_cod' => 25,
				'cep' => '49985-000',
			),
			249 => 
			array (
				'id' => 8840,
				'nome' => 'Santo Amaro das Brotas',
				'uf' => 'SE',
				'cep2' => '2806602',
				'estado_cod' => 25,
				'cep' => '49180-000',
			),
			250 => 
			array (
				'id' => 8841,
				'nome' => 'São Cristóvão',
				'uf' => 'SE',
				'cep2' => '2806701',
				'estado_cod' => 25,
				'cep' => '49100-000',
			),
			251 => 
			array (
				'id' => 8842,
				'nome' => 'São Domingos',
				'uf' => 'SE',
				'cep2' => '2806800',
				'estado_cod' => 25,
				'cep' => '49525-000',
			),
			252 => 
			array (
				'id' => 8843,
				'nome' => 'São Francisco',
				'uf' => 'SE',
				'cep2' => '2806909',
				'estado_cod' => 25,
				'cep' => '49945-000',
			),
			253 => 
			array (
				'id' => 8846,
				'nome' => 'São Miguel do Aleixo',
				'uf' => 'SE',
				'cep2' => '2807006',
				'estado_cod' => 25,
				'cep' => '49535-000',
			),
			254 => 
			array (
				'id' => 8847,
				'nome' => 'Simão Dias',
				'uf' => 'SE',
				'cep2' => '2807105',
				'estado_cod' => 25,
				'cep' => '49480-000',
			),
			255 => 
			array (
				'id' => 8848,
				'nome' => 'Siriri',
				'uf' => 'SE',
				'cep2' => '2807204',
				'estado_cod' => 25,
				'cep' => '49630-000',
			),
			256 => 
			array (
				'id' => 8849,
				'nome' => 'Telha',
				'uf' => 'SE',
				'cep2' => '2807303',
				'estado_cod' => 25,
				'cep' => '49910-000',
			),
			257 => 
			array (
				'id' => 8850,
				'nome' => 'Tobias Barreto',
				'uf' => 'SE',
				'cep2' => '2807402',
				'estado_cod' => 25,
				'cep' => '49300-000',
			),
			258 => 
			array (
				'id' => 8851,
				'nome' => 'Tomar do Geru',
				'uf' => 'SE',
				'cep2' => '2807501',
				'estado_cod' => 25,
				'cep' => '49280-000',
			),
			259 => 
			array (
				'id' => 8852,
				'nome' => 'Umbaúba',
				'uf' => 'SE',
				'cep2' => '2807600',
				'estado_cod' => 25,
				'cep' => '49260-000',
			),
			260 => 
			array (
				'id' => 8853,
				'nome' => 'Adamantina',
				'uf' => 'SP',
				'cep2' => '3500105',
				'estado_cod' => 26,
				'cep' => '17800-000',
			),
			261 => 
			array (
				'id' => 8854,
				'nome' => 'Adolfo',
				'uf' => 'SP',
				'cep2' => '3500204',
				'estado_cod' => 26,
				'cep' => '15230-000',
			),
			262 => 
			array (
				'id' => 8857,
				'nome' => 'Aguaí',
				'uf' => 'SP',
				'cep2' => '3500303',
				'estado_cod' => 26,
				'cep' => '13860-000',
			),
			263 => 
			array (
				'id' => 8858,
				'nome' => 'Águas da Prata',
				'uf' => 'SP',
				'cep2' => '3500402',
				'estado_cod' => 26,
				'cep' => '13890-000',
			),
			264 => 
			array (
				'id' => 8859,
				'nome' => 'Águas de Lindóia',
				'uf' => 'SP',
				'cep2' => '3500501',
				'estado_cod' => 26,
				'cep' => '13940-000',
			),
			265 => 
			array (
				'id' => 8860,
				'nome' => 'Águas de Santa Bárbara',
				'uf' => 'SP',
				'cep2' => '3500550',
				'estado_cod' => 26,
				'cep' => '18770-000',
			),
			266 => 
			array (
				'id' => 8861,
				'nome' => 'Águas de São Pedro',
				'uf' => 'SP',
				'cep2' => '3500600',
				'estado_cod' => 26,
				'cep' => '13525-000',
			),
			267 => 
			array (
				'id' => 8862,
				'nome' => 'Agudos',
				'uf' => 'SP',
				'cep2' => '3500709',
				'estado_cod' => 26,
				'cep' => '17120-000',
			),
			268 => 
			array (
				'id' => 8865,
				'nome' => 'Alambari',
				'uf' => 'SP',
				'cep2' => '3500758',
				'estado_cod' => 26,
				'cep' => '18220-000',
			),
			269 => 
			array (
				'id' => 8870,
				'nome' => 'Alfredo Marcondes',
				'uf' => 'SP',
				'cep2' => '3500808',
				'estado_cod' => 26,
				'cep' => '19180-000',
			),
			270 => 
			array (
				'id' => 8871,
				'nome' => 'Altair',
				'uf' => 'SP',
				'cep2' => '3500907',
				'estado_cod' => 26,
				'cep' => '15430-000',
			),
			271 => 
			array (
				'id' => 8872,
				'nome' => 'Altinópolis',
				'uf' => 'SP',
				'cep2' => '3501004',
				'estado_cod' => 26,
				'cep' => '14350-000',
			),
			272 => 
			array (
				'id' => 8873,
				'nome' => 'Alto Alegre',
				'uf' => 'SP',
				'cep2' => '3501103',
				'estado_cod' => 26,
				'cep' => '16310-000',
			),
			273 => 
			array (
				'id' => 8875,
				'nome' => 'Alumínio',
				'uf' => 'SP',
				'cep2' => '3501152',
				'estado_cod' => 26,
				'cep' => '18125-000',
			),
			274 => 
			array (
				'id' => 8876,
				'nome' => 'Álvares Florence',
				'uf' => 'SP',
				'cep2' => '3501202',
				'estado_cod' => 26,
				'cep' => '15540-000',
			),
			275 => 
			array (
				'id' => 8877,
				'nome' => 'Álvares Machado',
				'uf' => 'SP',
				'cep2' => '3501301',
				'estado_cod' => 26,
				'cep' => '19160-000',
			),
			276 => 
			array (
				'id' => 8878,
				'nome' => 'Álvaro de Carvalho',
				'uf' => 'SP',
				'cep2' => '3501400',
				'estado_cod' => 26,
				'cep' => '17410-000',
			),
			277 => 
			array (
				'id' => 8879,
				'nome' => 'Alvinlândia',
				'uf' => 'SP',
				'cep2' => '3501509',
				'estado_cod' => 26,
				'cep' => '17430-000',
			),
			278 => 
			array (
				'id' => 8883,
				'nome' => 'Americana',
				'uf' => 'SP',
				'cep2' => '3501608',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			279 => 
			array (
				'id' => 8884,
				'nome' => 'Américo Brasiliense',
				'uf' => 'SP',
				'cep2' => '3501707',
				'estado_cod' => 26,
				'cep' => '14820-000',
			),
			280 => 
			array (
				'id' => 8885,
				'nome' => 'Américo de Campos',
				'uf' => 'SP',
				'cep2' => '3501806',
				'estado_cod' => 26,
				'cep' => '15550-000',
			),
			281 => 
			array (
				'id' => 8886,
				'nome' => 'Amparo',
				'uf' => 'SP',
				'cep2' => '3501905',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			282 => 
			array (
				'id' => 8888,
				'nome' => 'Analândia',
				'uf' => 'SP',
				'cep2' => '3502002',
				'estado_cod' => 26,
				'cep' => '13550-000',
			),
			283 => 
			array (
				'id' => 8891,
				'nome' => 'Andradina',
				'uf' => 'SP',
				'cep2' => '3502101',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			284 => 
			array (
				'id' => 8892,
				'nome' => 'Angatuba',
				'uf' => 'SP',
				'cep2' => '3502200',
				'estado_cod' => 26,
				'cep' => '18240-000',
			),
			285 => 
			array (
				'id' => 8893,
				'nome' => 'Anhembi',
				'uf' => 'SP',
				'cep2' => '3502309',
				'estado_cod' => 26,
				'cep' => '18620-000',
			),
			286 => 
			array (
				'id' => 8894,
				'nome' => 'Anhumas',
				'uf' => 'SP',
				'cep2' => '3502408',
				'estado_cod' => 26,
				'cep' => '19580-000',
			),
			287 => 
			array (
				'id' => 8896,
				'nome' => 'Aparecida',
				'uf' => 'SP',
				'cep2' => '3502507',
				'estado_cod' => 26,
				'cep' => '12570-000',
			),
			288 => 
			array (
				'id' => 8897,
				'nome' => 'Aparecida D\'Oeste',
				'uf' => 'SP',
				'cep2' => '3502606',
				'estado_cod' => 26,
				'cep' => '15735-000',
			),
			289 => 
			array (
				'id' => 8901,
				'nome' => 'Apiaí',
				'uf' => 'SP',
				'cep2' => '3502705',
				'estado_cod' => 26,
				'cep' => '18320-000',
			),
			290 => 
			array (
				'id' => 8906,
				'nome' => 'Araçariguama',
				'uf' => 'SP',
				'cep2' => '3502754',
				'estado_cod' => 26,
				'cep' => '18147-000',
			),
			291 => 
			array (
				'id' => 8907,
				'nome' => 'Araçatuba',
				'uf' => 'SP',
				'cep2' => '3502804',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			292 => 
			array (
				'id' => 8908,
				'nome' => 'Araçoiaba da Serra',
				'uf' => 'SP',
				'cep2' => '3502903',
				'estado_cod' => 26,
				'cep' => '18190-000',
			),
			293 => 
			array (
				'id' => 8909,
				'nome' => 'Aramina',
				'uf' => 'SP',
				'cep2' => '3503000',
				'estado_cod' => 26,
				'cep' => '14550-000',
			),
			294 => 
			array (
				'id' => 8910,
				'nome' => 'Arandu',
				'uf' => 'SP',
				'cep2' => '3503109',
				'estado_cod' => 26,
				'cep' => '18710-000',
			),
			295 => 
			array (
				'id' => 8911,
				'nome' => 'Arapeí',
				'uf' => 'SP',
				'cep2' => '3503158',
				'estado_cod' => 26,
				'cep' => '12870-000',
			),
			296 => 
			array (
				'id' => 8912,
				'nome' => 'Araraquara',
				'uf' => 'SP',
				'cep2' => '3503208',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			297 => 
			array (
				'id' => 8913,
				'nome' => 'Araras',
				'uf' => 'SP',
				'cep2' => '3503307',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			298 => 
			array (
				'id' => 8916,
				'nome' => 'Arco-Íris',
				'uf' => 'SP',
				'cep2' => '3503356',
				'estado_cod' => 26,
				'cep' => '17630-000',
			),
			299 => 
			array (
				'id' => 8917,
				'nome' => 'Arealva',
				'uf' => 'SP',
				'cep2' => '3503406',
				'estado_cod' => 26,
				'cep' => '17160-000',
			),
			300 => 
			array (
				'id' => 8918,
				'nome' => 'Areias',
				'uf' => 'SP',
				'cep2' => '3503505',
				'estado_cod' => 26,
				'cep' => '12820-000',
			),
			301 => 
			array (
				'id' => 8919,
				'nome' => 'Areiópolis',
				'uf' => 'SP',
				'cep2' => '3503604',
				'estado_cod' => 26,
				'cep' => '18670-000',
			),
			302 => 
			array (
				'id' => 8920,
				'nome' => 'Ariranha',
				'uf' => 'SP',
				'cep2' => '3503703',
				'estado_cod' => 26,
				'cep' => '15960-000',
			),
			303 => 
			array (
				'id' => 8923,
				'nome' => 'Artur Nogueira',
				'uf' => 'SP',
				'cep2' => '3503802',
				'estado_cod' => 26,
				'cep' => '13160-000',
			),
			304 => 
			array (
				'id' => 8924,
				'nome' => 'Arujá',
				'uf' => 'SP',
				'cep2' => '3503901',
				'estado_cod' => 26,
				'cep' => '07400-000',
			),
			305 => 
			array (
				'id' => 8925,
				'nome' => 'Aspásia',
				'uf' => 'SP',
				'cep2' => '3503950',
				'estado_cod' => 26,
				'cep' => '15763-000',
			),
			306 => 
			array (
				'id' => 8926,
				'nome' => 'Assis',
				'uf' => 'SP',
				'cep2' => '3504008',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			307 => 
			array (
				'id' => 8928,
				'nome' => 'Atibaia',
				'uf' => 'SP',
				'cep2' => '3504107',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			308 => 
			array (
				'id' => 8930,
				'nome' => 'Auriflama',
				'uf' => 'SP',
				'cep2' => '3504206',
				'estado_cod' => 26,
				'cep' => '15350-000',
			),
			309 => 
			array (
				'id' => 8931,
				'nome' => 'Avaí',
				'uf' => 'SP',
				'cep2' => '3504305',
				'estado_cod' => 26,
				'cep' => '16680-000',
			),
			310 => 
			array (
				'id' => 8932,
				'nome' => 'Avanhandava',
				'uf' => 'SP',
				'cep2' => '3504404',
				'estado_cod' => 26,
				'cep' => '16360-000',
			),
			311 => 
			array (
				'id' => 8933,
				'nome' => 'Avaré',
				'uf' => 'SP',
				'cep2' => '3504503',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			312 => 
			array (
				'id' => 8937,
				'nome' => 'Bady Bassitt',
				'uf' => 'SP',
				'cep2' => '3504602',
				'estado_cod' => 26,
				'cep' => '15115-000',
			),
			313 => 
			array (
				'id' => 8940,
				'nome' => 'Balbinos',
				'uf' => 'SP',
				'cep2' => '3504701',
				'estado_cod' => 26,
				'cep' => '16640-000',
			),
			314 => 
			array (
				'id' => 8941,
				'nome' => 'Bálsamo',
				'uf' => 'SP',
				'cep2' => '3504800',
				'estado_cod' => 26,
				'cep' => '15140-000',
			),
			315 => 
			array (
				'id' => 8942,
				'nome' => 'Bananal',
				'uf' => 'SP',
				'cep2' => '3504909',
				'estado_cod' => 26,
				'cep' => '12850-000',
			),
			316 => 
			array (
				'id' => 8945,
				'nome' => 'Barão de Antonina',
				'uf' => 'SP',
				'cep2' => '3505005',
				'estado_cod' => 26,
				'cep' => '18490-000',
			),
			317 => 
			array (
				'id' => 8947,
				'nome' => 'Barbosa',
				'uf' => 'SP',
				'cep2' => '3505104',
				'estado_cod' => 26,
				'cep' => '16350-000',
			),
			318 => 
			array (
				'id' => 8948,
				'nome' => 'Bariri',
				'uf' => 'SP',
				'cep2' => '3505203',
				'estado_cod' => 26,
				'cep' => '17250-000',
			),
			319 => 
			array (
				'id' => 8949,
				'nome' => 'Barra Bonita',
				'uf' => 'SP',
				'cep2' => '3505302',
				'estado_cod' => 26,
				'cep' => '17340-000',
			),
			320 => 
			array (
				'id' => 8950,
				'nome' => 'Barra do Chapéu',
				'uf' => 'SP',
				'cep2' => '3505351',
				'estado_cod' => 26,
				'cep' => '18325-000',
			),
			321 => 
			array (
				'id' => 8951,
				'nome' => 'Barra do Turvo',
				'uf' => 'SP',
				'cep2' => '3505401',
				'estado_cod' => 26,
				'cep' => '11955-000',
			),
			322 => 
			array (
				'id' => 8954,
				'nome' => 'Barretos',
				'uf' => 'SP',
				'cep2' => '3505500',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			323 => 
			array (
				'id' => 8955,
				'nome' => 'Barrinha',
				'uf' => 'SP',
				'cep2' => '3505609',
				'estado_cod' => 26,
				'cep' => '14860-000',
			),
			324 => 
			array (
				'id' => 8956,
				'nome' => 'Barueri',
				'uf' => 'SP',
				'cep2' => '3505708',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			325 => 
			array (
				'id' => 8957,
				'nome' => 'Bastos',
				'uf' => 'SP',
				'cep2' => '3505807',
				'estado_cod' => 26,
				'cep' => '17690-000',
			),
			326 => 
			array (
				'id' => 8958,
				'nome' => 'Batatais',
				'uf' => 'SP',
				'cep2' => '3505906',
				'estado_cod' => 26,
				'cep' => '14300-000',
			),
			327 => 
			array (
				'id' => 8961,
				'nome' => 'Bauru',
				'uf' => 'SP',
				'cep2' => '3506003',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			328 => 
			array (
				'id' => 8962,
				'nome' => 'Bebedouro',
				'uf' => 'SP',
				'cep2' => '3506102',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			329 => 
			array (
				'id' => 8965,
				'nome' => 'Bento de Abreu',
				'uf' => 'SP',
				'cep2' => '3506201',
				'estado_cod' => 26,
				'cep' => '16790-000',
			),
			330 => 
			array (
				'id' => 8966,
				'nome' => 'Bernardino de Campos',
				'uf' => 'SP',
				'cep2' => '3506300',
				'estado_cod' => 26,
				'cep' => '18960-000',
			),
			331 => 
			array (
				'id' => 8967,
				'nome' => 'Bertioga',
				'uf' => 'SP',
				'cep2' => '3506359',
				'estado_cod' => 26,
				'cep' => '11250-000',
			),
			332 => 
			array (
				'id' => 8968,
				'nome' => 'Bilac',
				'uf' => 'SP',
				'cep2' => '3506409',
				'estado_cod' => 26,
				'cep' => '16210-000',
			),
			333 => 
			array (
				'id' => 8969,
				'nome' => 'Birigüi',
				'uf' => 'SP',
				'cep2' => '3506508',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			334 => 
			array (
				'id' => 8970,
				'nome' => 'Biritiba-Mirim',
				'uf' => 'SP',
				'cep2' => '3506607',
				'estado_cod' => 26,
				'cep' => '08940-000',
			),
			335 => 
			array (
				'id' => 8972,
				'nome' => 'Boa Esperança do Sul',
				'uf' => 'SP',
				'cep2' => '3506706',
				'estado_cod' => 26,
				'cep' => '14930-000',
			),
			336 => 
			array (
				'id' => 8975,
				'nome' => 'Bocaina',
				'uf' => 'SP',
				'cep2' => '3506805',
				'estado_cod' => 26,
				'cep' => '17240-000',
			),
			337 => 
			array (
				'id' => 8976,
				'nome' => 'Bofete',
				'uf' => 'SP',
				'cep2' => '3506904',
				'estado_cod' => 26,
				'cep' => '18590-000',
			),
			338 => 
			array (
				'id' => 8977,
				'nome' => 'Boituva',
				'uf' => 'SP',
				'cep2' => '3507001',
				'estado_cod' => 26,
				'cep' => '18550-000',
			),
			339 => 
			array (
				'id' => 8979,
				'nome' => 'Bom Jesus dos Perdões',
				'uf' => 'SP',
				'cep2' => '3507100',
				'estado_cod' => 26,
				'cep' => '12955-000',
			),
			340 => 
			array (
				'id' => 8981,
				'nome' => 'Bom Sucesso de Itararé',
				'uf' => 'SP',
				'cep2' => '3507159',
				'estado_cod' => 26,
				'cep' => '18475-000',
			),
			341 => 
			array (
				'id' => 8983,
				'nome' => 'Borá',
				'uf' => 'SP',
				'cep2' => '3507209',
				'estado_cod' => 26,
				'cep' => '19740-000',
			),
			342 => 
			array (
				'id' => 8984,
				'nome' => 'Boracéia',
				'uf' => 'SP',
				'cep2' => '3507308',
				'estado_cod' => 26,
				'cep' => '17270-000',
			),
			343 => 
			array (
				'id' => 8985,
				'nome' => 'Borborema',
				'uf' => 'SP',
				'cep2' => '3507407',
				'estado_cod' => 26,
				'cep' => '14955-000',
			),
			344 => 
			array (
				'id' => 8986,
				'nome' => 'Borebi',
				'uf' => 'SP',
				'cep2' => '3507456',
				'estado_cod' => 26,
				'cep' => '18675-000',
			),
			345 => 
			array (
				'id' => 8989,
				'nome' => 'Botucatu',
				'uf' => 'SP',
				'cep2' => '3507506',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			346 => 
			array (
				'id' => 8992,
				'nome' => 'Bragança Paulista',
				'uf' => 'SP',
				'cep2' => '3507605',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			347 => 
			array (
				'id' => 8995,
				'nome' => 'Braúna',
				'uf' => 'SP',
				'cep2' => '3507704',
				'estado_cod' => 26,
				'cep' => '16290-000',
			),
			348 => 
			array (
				'id' => 8996,
				'nome' => 'Brejo Alegre',
				'uf' => 'SP',
				'cep2' => '3507753',
				'estado_cod' => 26,
				'cep' => '16265-000',
			),
			349 => 
			array (
				'id' => 8997,
				'nome' => 'Brodowski',
				'uf' => 'SP',
				'cep2' => '3507803',
				'estado_cod' => 26,
				'cep' => '14340-000',
			),
			350 => 
			array (
				'id' => 8998,
				'nome' => 'Brotas',
				'uf' => 'SP',
				'cep2' => '3507902',
				'estado_cod' => 26,
				'cep' => '17380-000',
			),
			351 => 
			array (
				'id' => 9000,
				'nome' => 'Buri',
				'uf' => 'SP',
				'cep2' => '3508009',
				'estado_cod' => 26,
				'cep' => '18290-000',
			),
			352 => 
			array (
				'id' => 9001,
				'nome' => 'Buritama',
				'uf' => 'SP',
				'cep2' => '3508108',
				'estado_cod' => 26,
				'cep' => '15290-000',
			),
			353 => 
			array (
				'id' => 9002,
				'nome' => 'Buritizal',
				'uf' => 'SP',
				'cep2' => '3508207',
				'estado_cod' => 26,
				'cep' => '14570-000',
			),
			354 => 
			array (
				'id' => 9003,
				'nome' => 'Cabrália Paulista',
				'uf' => 'SP',
				'cep2' => '3508306',
				'estado_cod' => 26,
				'cep' => '17480-000',
			),
			355 => 
			array (
				'id' => 9004,
				'nome' => 'Cabreúva',
				'uf' => 'SP',
				'cep2' => '3508405',
				'estado_cod' => 26,
				'cep' => '13315-000',
			),
			356 => 
			array (
				'id' => 9005,
				'nome' => 'Caçapava',
				'uf' => 'SP',
				'cep2' => '3508504',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			357 => 
			array (
				'id' => 9007,
				'nome' => 'Cachoeira Paulista',
				'uf' => 'SP',
				'cep2' => '3508603',
				'estado_cod' => 26,
				'cep' => '12630-000',
			),
			358 => 
			array (
				'id' => 9008,
				'nome' => 'Caconde',
				'uf' => 'SP',
				'cep2' => '3508702',
				'estado_cod' => 26,
				'cep' => '13770-000',
			),
			359 => 
			array (
				'id' => 9009,
				'nome' => 'Cafelândia',
				'uf' => 'SP',
				'cep2' => '3508801',
				'estado_cod' => 26,
				'cep' => '16500-000',
			),
			360 => 
			array (
				'id' => 9011,
				'nome' => 'Caiabu',
				'uf' => 'SP',
				'cep2' => '3508900',
				'estado_cod' => 26,
				'cep' => '19530-000',
			),
			361 => 
			array (
				'id' => 9013,
				'nome' => 'Caieiras',
				'uf' => 'SP',
				'cep2' => '3509007',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			362 => 
			array (
				'id' => 9014,
				'nome' => 'Caiuá',
				'uf' => 'SP',
				'cep2' => '3509106',
				'estado_cod' => 26,
				'cep' => '19450-000',
			),
			363 => 
			array (
				'id' => 9015,
				'nome' => 'Cajamar',
				'uf' => 'SP',
				'cep2' => '3509205',
				'estado_cod' => 26,
				'cep' => '07750-000',
			),
			364 => 
			array (
				'id' => 9016,
				'nome' => 'Cajati',
				'uf' => 'SP',
				'cep2' => '3509254',
				'estado_cod' => 26,
				'cep' => '11950-000',
			),
			365 => 
			array (
				'id' => 9017,
				'nome' => 'Cajobi',
				'uf' => 'SP',
				'cep2' => '3509304',
				'estado_cod' => 26,
				'cep' => '15410-000',
			),
			366 => 
			array (
				'id' => 9018,
				'nome' => 'Cajuru',
				'uf' => 'SP',
				'cep2' => '3509403',
				'estado_cod' => 26,
				'cep' => '14240-000',
			),
			367 => 
			array (
				'id' => 9023,
				'nome' => 'Campina do Monte Alegre',
				'uf' => 'SP',
				'cep2' => '3509452',
				'estado_cod' => 26,
				'cep' => '18245-000',
			),
			368 => 
			array (
				'id' => 9025,
				'nome' => 'Campinas',
				'uf' => 'SP',
				'cep2' => '3509502',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			369 => 
			array (
				'id' => 9026,
				'nome' => 'Campo Limpo Paulista',
				'uf' => 'SP',
				'cep2' => '3509601',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			370 => 
			array (
				'id' => 9028,
				'nome' => 'Campos do Jordão',
				'uf' => 'SP',
				'cep2' => '3509700',
				'estado_cod' => 26,
				'cep' => '12460-000',
			),
			371 => 
			array (
				'id' => 9029,
				'nome' => 'Campos Novos Paulista',
				'uf' => 'SP',
				'cep2' => '3509809',
				'estado_cod' => 26,
				'cep' => '19960-000',
			),
			372 => 
			array (
				'id' => 9030,
				'nome' => 'Cananéia',
				'uf' => 'SP',
				'cep2' => '3509908',
				'estado_cod' => 26,
				'cep' => '11990-000',
			),
			373 => 
			array (
				'id' => 9031,
				'nome' => 'Canas',
				'uf' => 'SP',
				'cep2' => '3509957',
				'estado_cod' => 26,
				'cep' => '12615-000',
			),
			374 => 
			array (
				'id' => 9033,
				'nome' => 'Cândido Mota',
				'uf' => 'SP',
				'cep2' => '3510005',
				'estado_cod' => 26,
				'cep' => '19880-000',
			),
			375 => 
			array (
				'id' => 9034,
				'nome' => 'Cândido Rodrigues',
				'uf' => 'SP',
				'cep2' => '3510104',
				'estado_cod' => 26,
				'cep' => '15930-000',
			),
			376 => 
			array (
				'id' => 9036,
				'nome' => 'Canitar',
				'uf' => 'SP',
				'cep2' => '3510153',
				'estado_cod' => 26,
				'cep' => '18990-000',
			),
			377 => 
			array (
				'id' => 9037,
				'nome' => 'Capão Bonito',
				'uf' => 'SP',
				'cep2' => '3510203',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			378 => 
			array (
				'id' => 9038,
				'nome' => 'Capela do Alto',
				'uf' => 'SP',
				'cep2' => '3510302',
				'estado_cod' => 26,
				'cep' => '18195-000',
			),
			379 => 
			array (
				'id' => 9039,
				'nome' => 'Capivari',
				'uf' => 'SP',
				'cep2' => '3510401',
				'estado_cod' => 26,
				'cep' => '13360-000',
			),
			380 => 
			array (
				'id' => 9043,
				'nome' => 'Caraguatatuba',
				'uf' => 'SP',
				'cep2' => '3510500',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			381 => 
			array (
				'id' => 9044,
				'nome' => 'Carapicuíba',
				'uf' => 'SP',
				'cep2' => '3510609',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			382 => 
			array (
				'id' => 9046,
				'nome' => 'Cardoso',
				'uf' => 'SP',
				'cep2' => '3510708',
				'estado_cod' => 26,
				'cep' => '15570-000',
			),
			383 => 
			array (
				'id' => 9049,
				'nome' => 'Casa Branca',
				'uf' => 'SP',
				'cep2' => '3510807',
				'estado_cod' => 26,
				'cep' => '13700-000',
			),
			384 => 
			array (
				'id' => 9050,
				'nome' => 'Cássia dos Coqueiros',
				'uf' => 'SP',
				'cep2' => '3510906',
				'estado_cod' => 26,
				'cep' => '14260-000',
			),
			385 => 
			array (
				'id' => 9051,
				'nome' => 'Castilho',
				'uf' => 'SP',
				'cep2' => '3511003',
				'estado_cod' => 26,
				'cep' => '16920-000',
			),
			386 => 
			array (
				'id' => 9052,
				'nome' => 'Catanduva',
				'uf' => 'SP',
				'cep2' => '3511102',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			387 => 
			array (
				'id' => 9053,
				'nome' => 'Catiguá',
				'uf' => 'SP',
				'cep2' => '3511201',
				'estado_cod' => 26,
				'cep' => '15870-000',
			),
			388 => 
			array (
				'id' => 9056,
				'nome' => 'Cedral',
				'uf' => 'SP',
				'cep2' => '3511300',
				'estado_cod' => 26,
				'cep' => '15895-000',
			),
			389 => 
			array (
				'id' => 9057,
				'nome' => 'Cerqueira César',
				'uf' => 'SP',
				'cep2' => '3511409',
				'estado_cod' => 26,
				'cep' => '18760-000',
			),
			390 => 
			array (
				'id' => 9058,
				'nome' => 'Cerquilho',
				'uf' => 'SP',
				'cep2' => '3511508',
				'estado_cod' => 26,
				'cep' => '18520-000',
			),
			391 => 
			array (
				'id' => 9059,
				'nome' => 'Cesário Lange',
				'uf' => 'SP',
				'cep2' => '3511607',
				'estado_cod' => 26,
				'cep' => '18285-000',
			),
			392 => 
			array (
				'id' => 9061,
				'nome' => 'Charqueada',
				'uf' => 'SP',
				'cep2' => '3511706',
				'estado_cod' => 26,
				'cep' => '13515-000',
			),
			393 => 
			array (
				'id' => 9062,
				'nome' => 'Chavantes',
				'uf' => 'SP',
				'cep2' => '3557204',
				'estado_cod' => 26,
				'cep' => '18970-000',
			),
			394 => 
			array (
				'id' => 9065,
				'nome' => 'Clementina',
				'uf' => 'SP',
				'cep2' => '3511904',
				'estado_cod' => 26,
				'cep' => '16250-000',
			),
			395 => 
			array (
				'id' => 9067,
				'nome' => 'Colina',
				'uf' => 'SP',
				'cep2' => '3512001',
				'estado_cod' => 26,
				'cep' => '14770-000',
			),
			396 => 
			array (
				'id' => 9068,
				'nome' => 'Colômbia',
				'uf' => 'SP',
				'cep2' => '3512100',
				'estado_cod' => 26,
				'cep' => '14795-000',
			),
			397 => 
			array (
				'id' => 9070,
				'nome' => 'Conchal',
				'uf' => 'SP',
				'cep2' => '3512209',
				'estado_cod' => 26,
				'cep' => '13835-000',
			),
			398 => 
			array (
				'id' => 9071,
				'nome' => 'Conchas',
				'uf' => 'SP',
				'cep2' => '3512308',
				'estado_cod' => 26,
				'cep' => '18570-000',
			),
			399 => 
			array (
				'id' => 9072,
				'nome' => 'Cordeirópolis',
				'uf' => 'SP',
				'cep2' => '3512407',
				'estado_cod' => 26,
				'cep' => '13490-000',
			),
			400 => 
			array (
				'id' => 9073,
				'nome' => 'Coroados',
				'uf' => 'SP',
				'cep2' => '3512506',
				'estado_cod' => 26,
				'cep' => '16260-000',
			),
			401 => 
			array (
				'id' => 9075,
				'nome' => 'Coronel Macedo',
				'uf' => 'SP',
				'cep2' => '3512605',
				'estado_cod' => 26,
				'cep' => '18745-000',
			),
			402 => 
			array (
				'id' => 9078,
				'nome' => 'Corumbataí',
				'uf' => 'SP',
				'cep2' => '3512704',
				'estado_cod' => 26,
				'cep' => '13540-000',
			),
			403 => 
			array (
				'id' => 9079,
				'nome' => 'Cosmópolis',
				'uf' => 'SP',
				'cep2' => '3512803',
				'estado_cod' => 26,
				'cep' => '13150-000',
			),
			404 => 
			array (
				'id' => 9080,
				'nome' => 'Cosmorama',
				'uf' => 'SP',
				'cep2' => '3512902',
				'estado_cod' => 26,
				'cep' => '15530-000',
			),
			405 => 
			array (
				'id' => 9082,
				'nome' => 'Cotia',
				'uf' => 'SP',
				'cep2' => '3513009',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			406 => 
			array (
				'id' => 9083,
				'nome' => 'Cravinhos',
				'uf' => 'SP',
				'cep2' => '3513108',
				'estado_cod' => 26,
				'cep' => '14140-000',
			),
			407 => 
			array (
				'id' => 9084,
				'nome' => 'Cristais Paulista',
				'uf' => 'SP',
				'cep2' => '3513207',
				'estado_cod' => 26,
				'cep' => '14460-000',
			),
			408 => 
			array (
				'id' => 9086,
				'nome' => 'Cruzália',
				'uf' => 'SP',
				'cep2' => '3513306',
				'estado_cod' => 26,
				'cep' => '19860-000',
			),
			409 => 
			array (
				'id' => 9087,
				'nome' => 'Cruzeiro',
				'uf' => 'SP',
				'cep2' => '3513405',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			410 => 
			array (
				'id' => 9088,
				'nome' => 'Cubatão',
				'uf' => 'SP',
				'cep2' => '3513504',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			411 => 
			array (
				'id' => 9090,
				'nome' => 'Cunha',
				'uf' => 'SP',
				'cep2' => '3513603',
				'estado_cod' => 26,
				'cep' => '12530-000',
			),
			412 => 
			array (
				'id' => 9093,
				'nome' => 'Descalvado',
				'uf' => 'SP',
				'cep2' => '3513702',
				'estado_cod' => 26,
				'cep' => '13690-000',
			),
			413 => 
			array (
				'id' => 9094,
				'nome' => 'Diadema',
				'uf' => 'SP',
				'cep2' => '3513801',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			414 => 
			array (
				'id' => 9095,
				'nome' => 'Dirce Reis',
				'uf' => 'SP',
				'cep2' => '3513850',
				'estado_cod' => 26,
				'cep' => '15715-000',
			),
			415 => 
			array (
				'id' => 9097,
				'nome' => 'Divinolândia',
				'uf' => 'SP',
				'cep2' => '3513900',
				'estado_cod' => 26,
				'cep' => '13780-000',
			),
			416 => 
			array (
				'id' => 9098,
				'nome' => 'Dobrada',
				'uf' => 'SP',
				'cep2' => '3514007',
				'estado_cod' => 26,
				'cep' => '15980-000',
			),
			417 => 
			array (
				'id' => 9099,
				'nome' => 'Dois Córregos',
				'uf' => 'SP',
				'cep2' => '3514106',
				'estado_cod' => 26,
				'cep' => '17300-000',
			),
			418 => 
			array (
				'id' => 9100,
				'nome' => 'Dolcinópolis',
				'uf' => 'SP',
				'cep2' => '3514205',
				'estado_cod' => 26,
				'cep' => '15740-000',
			),
			419 => 
			array (
				'id' => 9102,
				'nome' => 'Dourado',
				'uf' => 'SP',
				'cep2' => '3514304',
				'estado_cod' => 26,
				'cep' => '13590-000',
			),
			420 => 
			array (
				'id' => 9103,
				'nome' => 'Dracena',
				'uf' => 'SP',
				'cep2' => '3514403',
				'estado_cod' => 26,
				'cep' => '17900-000',
			),
			421 => 
			array (
				'id' => 9104,
				'nome' => 'Duartina',
				'uf' => 'SP',
				'cep2' => '3514502',
				'estado_cod' => 26,
				'cep' => '17470-000',
			),
			422 => 
			array (
				'id' => 9105,
				'nome' => 'Dumont',
				'uf' => 'SP',
				'cep2' => '3514601',
				'estado_cod' => 26,
				'cep' => '14120-000',
			),
			423 => 
			array (
				'id' => 9107,
				'nome' => 'Echaporã',
				'uf' => 'SP',
				'cep2' => '3514700',
				'estado_cod' => 26,
				'cep' => '19830-000',
			),
			424 => 
			array (
				'id' => 9108,
				'nome' => 'Eldorado',
				'uf' => 'SP',
				'cep2' => '3514809',
				'estado_cod' => 26,
				'cep' => '11960-000',
			),
			425 => 
			array (
				'id' => 9110,
				'nome' => 'Elias Fausto',
				'uf' => 'SP',
				'cep2' => '3514908',
				'estado_cod' => 26,
				'cep' => '13350-000',
			),
			426 => 
			array (
				'id' => 9111,
				'nome' => 'Elisiário',
				'uf' => 'SP',
				'cep2' => '3514924',
				'estado_cod' => 26,
				'cep' => '15823-000',
			),
			427 => 
			array (
				'id' => 9112,
				'nome' => 'Embaúba',
				'uf' => 'SP',
				'cep2' => '3514957',
				'estado_cod' => 26,
				'cep' => '15425-000',
			),
			428 => 
			array (
				'id' => 9113,
				'nome' => 'Embu das Artes',
				'uf' => 'SP',
				'cep2' => '',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			429 => 
			array (
				'id' => 9114,
				'nome' => 'Embu-Guaçu',
				'uf' => 'SP',
				'cep2' => '3515103',
				'estado_cod' => 26,
				'cep' => '06900-000',
			),
			430 => 
			array (
				'id' => 9115,
				'nome' => 'Emilianópolis',
				'uf' => 'SP',
				'cep2' => '3515129',
				'estado_cod' => 26,
				'cep' => '19350-000',
			),
			431 => 
			array (
				'id' => 9118,
				'nome' => 'Engenheiro Coelho',
				'uf' => 'SP',
				'cep2' => '3515152',
				'estado_cod' => 26,
				'cep' => '13165-000',
			),
			432 => 
			array (
				'id' => 9124,
				'nome' => 'Espírito Santo do Pinhal',
				'uf' => 'SP',
				'cep2' => '3515186',
				'estado_cod' => 26,
				'cep' => '13990-000',
			),
			433 => 
			array (
				'id' => 9125,
				'nome' => 'Espírito Santo do Turvo',
				'uf' => 'SP',
				'cep2' => '3515194',
				'estado_cod' => 26,
				'cep' => '18935-000',
			),
			434 => 
			array (
				'id' => 9126,
				'nome' => 'Estiva Gerbi',
				'uf' => 'SP',
				'cep2' => '3557303',
				'estado_cod' => 26,
				'cep' => '13857-000',
			),
			435 => 
			array (
				'id' => 9127,
				'nome' => 'Estrela D\'Oeste',
				'uf' => 'SP',
				'cep2' => '3515202',
				'estado_cod' => 26,
				'cep' => '15650-000',
			),
			436 => 
			array (
				'id' => 9128,
				'nome' => 'Estrela do Norte',
				'uf' => 'SP',
				'cep2' => '3515301',
				'estado_cod' => 26,
				'cep' => '19230-000',
			),
			437 => 
			array (
				'id' => 9129,
				'nome' => 'Euclides da Cunha Paulista',
				'uf' => 'SP',
				'cep2' => '3515350',
				'estado_cod' => 26,
				'cep' => '19275-000',
			),
			438 => 
			array (
				'id' => 9131,
				'nome' => 'Fartura',
				'uf' => 'SP',
				'cep2' => '3515400',
				'estado_cod' => 26,
				'cep' => '18870-000',
			),
			439 => 
			array (
				'id' => 9135,
				'nome' => 'Fernando Prestes',
				'uf' => 'SP',
				'cep2' => '3515608',
				'estado_cod' => 26,
				'cep' => '15940-000',
			),
			440 => 
			array (
				'id' => 9136,
				'nome' => 'Fernandópolis',
				'uf' => 'SP',
				'cep2' => '3515509',
				'estado_cod' => 26,
				'cep' => '15600-000',
			),
			441 => 
			array (
				'id' => 9137,
				'nome' => 'Fernão',
				'uf' => 'SP',
				'cep2' => '3515657',
				'estado_cod' => 26,
				'cep' => '17455-000',
			),
			442 => 
			array (
				'id' => 9138,
				'nome' => 'Ferraz de Vasconcelos',
				'uf' => 'SP',
				'cep2' => '3515707',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			443 => 
			array (
				'id' => 9139,
				'nome' => 'Flora Rica',
				'uf' => 'SP',
				'cep2' => '3515806',
				'estado_cod' => 26,
				'cep' => '17870-000',
			),
			444 => 
			array (
				'id' => 9140,
				'nome' => 'Floreal',
				'uf' => 'SP',
				'cep2' => '3515905',
				'estado_cod' => 26,
				'cep' => '15320-000',
			),
			445 => 
			array (
				'id' => 9142,
				'nome' => 'Flórida Paulista',
				'uf' => 'SP',
				'cep2' => '3516002',
				'estado_cod' => 26,
				'cep' => '17830-000',
			),
			446 => 
			array (
				'id' => 9143,
				'nome' => 'Florínia',
				'uf' => 'SP',
				'cep2' => '',
				'estado_cod' => 26,
				'cep' => '19870-000',
			),
			447 => 
			array (
				'id' => 9144,
				'nome' => 'Franca',
				'uf' => 'SP',
				'cep2' => '3516200',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			448 => 
			array (
				'id' => 9145,
				'nome' => 'Francisco Morato',
				'uf' => 'SP',
				'cep2' => '3516309',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			449 => 
			array (
				'id' => 9146,
				'nome' => 'Franco da Rocha',
				'uf' => 'SP',
				'cep2' => '3516408',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			450 => 
			array (
				'id' => 9148,
				'nome' => 'Gabriel Monteiro',
				'uf' => 'SP',
				'cep2' => '3516507',
				'estado_cod' => 26,
				'cep' => '16220-000',
			),
			451 => 
			array (
				'id' => 9149,
				'nome' => 'Gália',
				'uf' => 'SP',
				'cep2' => '3516606',
				'estado_cod' => 26,
				'cep' => '17450-000',
			),
			452 => 
			array (
				'id' => 9150,
				'nome' => 'Garça',
				'uf' => 'SP',
				'cep2' => '3516705',
				'estado_cod' => 26,
				'cep' => '17400-000',
			),
			453 => 
			array (
				'id' => 9152,
				'nome' => 'Gastão Vidigal',
				'uf' => 'SP',
				'cep2' => '3516804',
				'estado_cod' => 26,
				'cep' => '15330-000',
			),
			454 => 
			array (
				'id' => 9153,
				'nome' => 'Gavião Peixoto',
				'uf' => 'SP',
				'cep2' => '3516853',
				'estado_cod' => 26,
				'cep' => '14813-000',
			),
			455 => 
			array (
				'id' => 9154,
				'nome' => 'General Salgado',
				'uf' => 'SP',
				'cep2' => '3516903',
				'estado_cod' => 26,
				'cep' => '15300-000',
			),
			456 => 
			array (
				'id' => 9155,
				'nome' => 'Getulina',
				'uf' => 'SP',
				'cep2' => '3517000',
				'estado_cod' => 26,
				'cep' => '16450-000',
			),
			457 => 
			array (
				'id' => 9156,
				'nome' => 'Glicério',
				'uf' => 'SP',
				'cep2' => '3517109',
				'estado_cod' => 26,
				'cep' => '16270-000',
			),
			458 => 
			array (
				'id' => 9160,
				'nome' => 'Guaiçara',
				'uf' => 'SP',
				'cep2' => '3517208',
				'estado_cod' => 26,
				'cep' => '16430-000',
			),
			459 => 
			array (
				'id' => 9161,
				'nome' => 'Guaimbê',
				'uf' => 'SP',
				'cep2' => '3517307',
				'estado_cod' => 26,
				'cep' => '16480-000',
			),
			460 => 
			array (
				'id' => 9162,
				'nome' => 'Guaíra',
				'uf' => 'SP',
				'cep2' => '3517406',
				'estado_cod' => 26,
				'cep' => '14790-000',
			),
			461 => 
			array (
				'id' => 9164,
				'nome' => 'Guapiaçu',
				'uf' => 'SP',
				'cep2' => '3517505',
				'estado_cod' => 26,
				'cep' => '15110-000',
			),
			462 => 
			array (
				'id' => 9165,
				'nome' => 'Guapiara',
				'uf' => 'SP',
				'cep2' => '3517604',
				'estado_cod' => 26,
				'cep' => '18310-000',
			),
			463 => 
			array (
				'id' => 9167,
				'nome' => 'Guará',
				'uf' => 'SP',
				'cep2' => '3517703',
				'estado_cod' => 26,
				'cep' => '14580-000',
			),
			464 => 
			array (
				'id' => 9168,
				'nome' => 'Guaraçaí',
				'uf' => 'SP',
				'cep2' => '3517802',
				'estado_cod' => 26,
				'cep' => '16980-000',
			),
			465 => 
			array (
				'id' => 9169,
				'nome' => 'Guaraci',
				'uf' => 'SP',
				'cep2' => '3517901',
				'estado_cod' => 26,
				'cep' => '15420-000',
			),
			466 => 
			array (
				'id' => 9171,
				'nome' => 'Guarani D\'Oeste',
				'uf' => 'SP',
				'cep2' => '3518008',
				'estado_cod' => 26,
				'cep' => '15680-000',
			),
			467 => 
			array (
				'id' => 9172,
				'nome' => 'Guarantã',
				'uf' => 'SP',
				'cep2' => '3518107',
				'estado_cod' => 26,
				'cep' => '16570-000',
			),
			468 => 
			array (
				'id' => 9175,
				'nome' => 'Guararapes',
				'uf' => 'SP',
				'cep2' => '3518206',
				'estado_cod' => 26,
				'cep' => '16700-000',
			),
			469 => 
			array (
				'id' => 9176,
				'nome' => 'Guararema',
				'uf' => 'SP',
				'cep2' => '3518305',
				'estado_cod' => 26,
				'cep' => '08900-000',
			),
			470 => 
			array (
				'id' => 9177,
				'nome' => 'Guaratinguetá',
				'uf' => 'SP',
				'cep2' => '3518404',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			471 => 
			array (
				'id' => 9178,
				'nome' => 'Guareí',
				'uf' => 'SP',
				'cep2' => '3518503',
				'estado_cod' => 26,
				'cep' => '18250-000',
			),
			472 => 
			array (
				'id' => 9179,
				'nome' => 'Guariba',
				'uf' => 'SP',
				'cep2' => '3518602',
				'estado_cod' => 26,
				'cep' => '14840-000',
			),
			473 => 
			array (
				'id' => 9182,
				'nome' => 'Guarujá',
				'uf' => 'SP',
				'cep2' => '3518701',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			474 => 
			array (
				'id' => 9183,
				'nome' => 'Guarulhos',
				'uf' => 'SP',
				'cep2' => '3518800',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			475 => 
			array (
				'id' => 9184,
				'nome' => 'Guatapará',
				'uf' => 'SP',
				'cep2' => '3518859',
				'estado_cod' => 26,
				'cep' => '14115-000',
			),
			476 => 
			array (
				'id' => 9185,
				'nome' => 'Guzolândia',
				'uf' => 'SP',
				'cep2' => '3518909',
				'estado_cod' => 26,
				'cep' => '15355-000',
			),
			477 => 
			array (
				'id' => 9186,
				'nome' => 'Herculândia',
				'uf' => 'SP',
				'cep2' => '3519006',
				'estado_cod' => 26,
				'cep' => '17650-000',
			),
			478 => 
			array (
				'id' => 9187,
				'nome' => 'Holambra',
				'uf' => 'SP',
				'cep2' => '3519055',
				'estado_cod' => 26,
				'cep' => '13825-000',
			),
			479 => 
			array (
				'id' => 9189,
				'nome' => 'Hortolândia',
				'uf' => 'SP',
				'cep2' => '3519071',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			480 => 
			array (
				'id' => 9190,
				'nome' => 'Iacanga',
				'uf' => 'SP',
				'cep2' => '3519105',
				'estado_cod' => 26,
				'cep' => '17180-000',
			),
			481 => 
			array (
				'id' => 9191,
				'nome' => 'Iacri',
				'uf' => 'SP',
				'cep2' => '3519204',
				'estado_cod' => 26,
				'cep' => '17680-000',
			),
			482 => 
			array (
				'id' => 9192,
				'nome' => 'Iaras',
				'uf' => 'SP',
				'cep2' => '3519253',
				'estado_cod' => 26,
				'cep' => '18775-000',
			),
			483 => 
			array (
				'id' => 9193,
				'nome' => 'Ibaté',
				'uf' => 'SP',
				'cep2' => '3519303',
				'estado_cod' => 26,
				'cep' => '14815-000',
			),
			484 => 
			array (
				'id' => 9195,
				'nome' => 'Ibirá',
				'uf' => 'SP',
				'cep2' => '3519402',
				'estado_cod' => 26,
				'cep' => '15860-000',
			),
			485 => 
			array (
				'id' => 9196,
				'nome' => 'Ibirarema',
				'uf' => 'SP',
				'cep2' => '3519501',
				'estado_cod' => 26,
				'cep' => '19940-000',
			),
			486 => 
			array (
				'id' => 9197,
				'nome' => 'Ibitinga',
				'uf' => 'SP',
				'cep2' => '3519600',
				'estado_cod' => 26,
				'cep' => '14940-000',
			),
			487 => 
			array (
				'id' => 9201,
				'nome' => 'Ibiúna',
				'uf' => 'SP',
				'cep2' => '3519709',
				'estado_cod' => 26,
				'cep' => '18150-000',
			),
			488 => 
			array (
				'id' => 9202,
				'nome' => 'Icém',
				'uf' => 'SP',
				'cep2' => '3519808',
				'estado_cod' => 26,
				'cep' => '15460-000',
			),
			489 => 
			array (
				'id' => 9204,
				'nome' => 'Iepê',
				'uf' => 'SP',
				'cep2' => '3519907',
				'estado_cod' => 26,
				'cep' => '19640-000',
			),
			490 => 
			array (
				'id' => 9206,
				'nome' => 'Igaraçu do Tietê',
				'uf' => 'SP',
				'cep2' => '3520004',
				'estado_cod' => 26,
				'cep' => '17350-000',
			),
			491 => 
			array (
				'id' => 9208,
				'nome' => 'Igarapava',
				'uf' => 'SP',
				'cep2' => '3520103',
				'estado_cod' => 26,
				'cep' => '14540-000',
			),
			492 => 
			array (
				'id' => 9209,
				'nome' => 'Igaratá',
				'uf' => 'SP',
				'cep2' => '3520202',
				'estado_cod' => 26,
				'cep' => '12350-000',
			),
			493 => 
			array (
				'id' => 9210,
				'nome' => 'Iguape',
				'uf' => 'SP',
				'cep2' => '3520301',
				'estado_cod' => 26,
				'cep' => '11920-000',
			),
			494 => 
			array (
				'id' => 9211,
				'nome' => 'Ilha Comprida',
				'uf' => 'SP',
				'cep2' => '3520426',
				'estado_cod' => 26,
				'cep' => '11925-000',
			),
			495 => 
			array (
				'id' => 9213,
				'nome' => 'Ilha Solteira',
				'uf' => 'SP',
				'cep2' => '3520442',
				'estado_cod' => 26,
				'cep' => '15385-000',
			),
			496 => 
			array (
				'id' => 9214,
				'nome' => 'Ilhabela',
				'uf' => 'SP',
				'cep2' => '3520400',
				'estado_cod' => 26,
				'cep' => '11630-000',
			),
			497 => 
			array (
				'id' => 9216,
				'nome' => 'Indaiatuba',
				'uf' => 'SP',
				'cep2' => '3520509',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			498 => 
			array (
				'id' => 9217,
				'nome' => 'Indiana',
				'uf' => 'SP',
				'cep2' => '3520608',
				'estado_cod' => 26,
				'cep' => '19560-000',
			),
			499 => 
			array (
				'id' => 9218,
				'nome' => 'Indiaporã',
				'uf' => 'SP',
				'cep2' => '3520707',
				'estado_cod' => 26,
				'cep' => '15690-000',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 9220,
				'nome' => 'Inúbia Paulista',
				'uf' => 'SP',
				'cep2' => '3520806',
				'estado_cod' => 26,
				'cep' => '17760-000',
			),
			1 => 
			array (
				'id' => 9221,
				'nome' => 'Ipaussu',
				'uf' => 'SP',
				'cep2' => '3520905',
				'estado_cod' => 26,
				'cep' => '18950-000',
			),
			2 => 
			array (
				'id' => 9222,
				'nome' => 'Iperó',
				'uf' => 'SP',
				'cep2' => '3521002',
				'estado_cod' => 26,
				'cep' => '18560-000',
			),
			3 => 
			array (
				'id' => 9223,
				'nome' => 'Ipeúna',
				'uf' => 'SP',
				'cep2' => '3521101',
				'estado_cod' => 26,
				'cep' => '13537-000',
			),
			4 => 
			array (
				'id' => 9224,
				'nome' => 'Ipiguá',
				'uf' => 'SP',
				'cep2' => '3521150',
				'estado_cod' => 26,
				'cep' => '15108-000',
			),
			5 => 
			array (
				'id' => 9225,
				'nome' => 'Iporanga',
				'uf' => 'SP',
				'cep2' => '3521200',
				'estado_cod' => 26,
				'cep' => '18330-000',
			),
			6 => 
			array (
				'id' => 9226,
				'nome' => 'Ipuã',
				'uf' => 'SP',
				'cep2' => '3521309',
				'estado_cod' => 26,
				'cep' => '14610-000',
			),
			7 => 
			array (
				'id' => 9227,
				'nome' => 'Iracemápolis',
				'uf' => 'SP',
				'cep2' => '3521408',
				'estado_cod' => 26,
				'cep' => '13495-000',
			),
			8 => 
			array (
				'id' => 9229,
				'nome' => 'Irapuã',
				'uf' => 'SP',
				'cep2' => '3521507',
				'estado_cod' => 26,
				'cep' => '14990-000',
			),
			9 => 
			array (
				'id' => 9230,
				'nome' => 'Irapuru',
				'uf' => 'SP',
				'cep2' => '3521606',
				'estado_cod' => 26,
				'cep' => '17880-000',
			),
			10 => 
			array (
				'id' => 9231,
				'nome' => 'Itaberá',
				'uf' => 'SP',
				'cep2' => '3521705',
				'estado_cod' => 26,
				'cep' => '18440-000',
			),
			11 => 
			array (
				'id' => 9233,
				'nome' => 'Itaí',
				'uf' => 'SP',
				'cep2' => '3521804',
				'estado_cod' => 26,
				'cep' => '18730-000',
			),
			12 => 
			array (
				'id' => 9235,
				'nome' => 'Itajobi',
				'uf' => 'SP',
				'cep2' => '3521903',
				'estado_cod' => 26,
				'cep' => '15840-000',
			),
			13 => 
			array (
				'id' => 9236,
				'nome' => 'Itaju',
				'uf' => 'SP',
				'cep2' => '3522000',
				'estado_cod' => 26,
				'cep' => '17260-000',
			),
			14 => 
			array (
				'id' => 9237,
				'nome' => 'Itanhaém',
				'uf' => 'SP',
				'cep2' => '3522109',
				'estado_cod' => 26,
				'cep' => '11740-000',
			),
			15 => 
			array (
				'id' => 9238,
				'nome' => 'Itaóca',
				'uf' => 'SP',
				'cep2' => '',
				'estado_cod' => 26,
				'cep' => '18360-000',
			),
			16 => 
			array (
				'id' => 9239,
				'nome' => 'Itapecerica da Serra',
				'uf' => 'SP',
				'cep2' => '3522208',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			17 => 
			array (
				'id' => 9240,
				'nome' => 'Itapetininga',
				'uf' => 'SP',
				'cep2' => '3522307',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			18 => 
			array (
				'id' => 9242,
				'nome' => 'Itapeva',
				'uf' => 'SP',
				'cep2' => '3522406',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			19 => 
			array (
				'id' => 9243,
				'nome' => 'Itapevi',
				'uf' => 'SP',
				'cep2' => '3522505',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			20 => 
			array (
				'id' => 9244,
				'nome' => 'Itapira',
				'uf' => 'SP',
				'cep2' => '3522604',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			21 => 
			array (
				'id' => 9245,
				'nome' => 'Itapirapuã Paulista',
				'uf' => 'SP',
				'cep2' => '3522653',
				'estado_cod' => 26,
				'cep' => '18385-000',
			),
			22 => 
			array (
				'id' => 9246,
				'nome' => 'Itápolis',
				'uf' => 'SP',
				'cep2' => '3522703',
				'estado_cod' => 26,
				'cep' => '14900-000',
			),
			23 => 
			array (
				'id' => 9247,
				'nome' => 'Itaporanga',
				'uf' => 'SP',
				'cep2' => '3522802',
				'estado_cod' => 26,
				'cep' => '18480-000',
			),
			24 => 
			array (
				'id' => 9248,
				'nome' => 'Itapuí',
				'uf' => 'SP',
				'cep2' => '3522901',
				'estado_cod' => 26,
				'cep' => '17230-000',
			),
			25 => 
			array (
				'id' => 9249,
				'nome' => 'Itapura',
				'uf' => 'SP',
				'cep2' => '3523008',
				'estado_cod' => 26,
				'cep' => '15390-000',
			),
			26 => 
			array (
				'id' => 9250,
				'nome' => 'Itaquaquecetuba',
				'uf' => 'SP',
				'cep2' => '3523107',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			27 => 
			array (
				'id' => 9252,
				'nome' => 'Itararé',
				'uf' => 'SP',
				'cep2' => '3523206',
				'estado_cod' => 26,
				'cep' => '18460-000',
			),
			28 => 
			array (
				'id' => 9253,
				'nome' => 'Itariri',
				'uf' => 'SP',
				'cep2' => '3523305',
				'estado_cod' => 26,
				'cep' => '11760-000',
			),
			29 => 
			array (
				'id' => 9254,
				'nome' => 'Itatiba',
				'uf' => 'SP',
				'cep2' => '3523404',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			30 => 
			array (
				'id' => 9255,
				'nome' => 'Itatinga',
				'uf' => 'SP',
				'cep2' => '3523503',
				'estado_cod' => 26,
				'cep' => '18690-000',
			),
			31 => 
			array (
				'id' => 9256,
				'nome' => 'Itirapina',
				'uf' => 'SP',
				'cep2' => '3523602',
				'estado_cod' => 26,
				'cep' => '13530-000',
			),
			32 => 
			array (
				'id' => 9257,
				'nome' => 'Itirapuã',
				'uf' => 'SP',
				'cep2' => '3523701',
				'estado_cod' => 26,
				'cep' => '14420-000',
			),
			33 => 
			array (
				'id' => 9258,
				'nome' => 'Itobi',
				'uf' => 'SP',
				'cep2' => '3523800',
				'estado_cod' => 26,
				'cep' => '13715-000',
			),
			34 => 
			array (
				'id' => 9260,
				'nome' => 'Itu',
				'uf' => 'SP',
				'cep2' => '3523909',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			35 => 
			array (
				'id' => 9261,
				'nome' => 'Itupeva',
				'uf' => 'SP',
				'cep2' => '3524006',
				'estado_cod' => 26,
				'cep' => '13295-000',
			),
			36 => 
			array (
				'id' => 9262,
				'nome' => 'Ituverava',
				'uf' => 'SP',
				'cep2' => '3524105',
				'estado_cod' => 26,
				'cep' => '14500-000',
			),
			37 => 
			array (
				'id' => 9264,
				'nome' => 'Jaborandi',
				'uf' => 'SP',
				'cep2' => '3524204',
				'estado_cod' => 26,
				'cep' => '14775-000',
			),
			38 => 
			array (
				'id' => 9265,
				'nome' => 'Jaboticabal',
				'uf' => 'SP',
				'cep2' => '3524303',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			39 => 
			array (
				'id' => 9267,
				'nome' => 'Jacareí',
				'uf' => 'SP',
				'cep2' => '3524402',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			40 => 
			array (
				'id' => 9268,
				'nome' => 'Jaci',
				'uf' => 'SP',
				'cep2' => '3524501',
				'estado_cod' => 26,
				'cep' => '15155-000',
			),
			41 => 
			array (
				'id' => 9271,
				'nome' => 'Jacupiranga',
				'uf' => 'SP',
				'cep2' => '3524600',
				'estado_cod' => 26,
				'cep' => '11940-000',
			),
			42 => 
			array (
				'id' => 9273,
				'nome' => 'Jaguariúna',
				'uf' => 'SP',
				'cep2' => '3524709',
				'estado_cod' => 26,
				'cep' => '13820-000',
			),
			43 => 
			array (
				'id' => 9274,
				'nome' => 'Jales',
				'uf' => 'SP',
				'cep2' => '3524808',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			44 => 
			array (
				'id' => 9276,
				'nome' => 'Jambeiro',
				'uf' => 'SP',
				'cep2' => '3524907',
				'estado_cod' => 26,
				'cep' => '12270-000',
			),
			45 => 
			array (
				'id' => 9277,
				'nome' => 'Jandira',
				'uf' => 'SP',
				'cep2' => '3525003',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			46 => 
			array (
				'id' => 9282,
				'nome' => 'Jardinópolis',
				'uf' => 'SP',
				'cep2' => '3525102',
				'estado_cod' => 26,
				'cep' => '14680-000',
			),
			47 => 
			array (
				'id' => 9283,
				'nome' => 'Jarinu',
				'uf' => 'SP',
				'cep2' => '3525201',
				'estado_cod' => 26,
				'cep' => '13240-000',
			),
			48 => 
			array (
				'id' => 9285,
				'nome' => 'Jaú',
				'uf' => 'SP',
				'cep2' => '3525300',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			49 => 
			array (
				'id' => 9286,
				'nome' => 'Jeriquara',
				'uf' => 'SP',
				'cep2' => '3525409',
				'estado_cod' => 26,
				'cep' => '14450-000',
			),
			50 => 
			array (
				'id' => 9287,
				'nome' => 'Joanópolis',
				'uf' => 'SP',
				'cep2' => '3525508',
				'estado_cod' => 26,
				'cep' => '12980-000',
			),
			51 => 
			array (
				'id' => 9288,
				'nome' => 'João Ramalho',
				'uf' => 'SP',
				'cep2' => '3525607',
				'estado_cod' => 26,
				'cep' => '19680-000',
			),
			52 => 
			array (
				'id' => 9291,
				'nome' => 'José Bonifácio',
				'uf' => 'SP',
				'cep2' => '3525706',
				'estado_cod' => 26,
				'cep' => '15200-000',
			),
			53 => 
			array (
				'id' => 9293,
				'nome' => 'Júlio Mesquita',
				'uf' => 'SP',
				'cep2' => '3525805',
				'estado_cod' => 26,
				'cep' => '17550-000',
			),
			54 => 
			array (
				'id' => 9294,
				'nome' => 'Jumirim',
				'uf' => 'SP',
				'cep2' => '3525854',
				'estado_cod' => 26,
				'cep' => '18535-000',
			),
			55 => 
			array (
				'id' => 9295,
				'nome' => 'Jundiaí',
				'uf' => 'SP',
				'cep2' => '3525904',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			56 => 
			array (
				'id' => 9298,
				'nome' => 'Junqueirópolis',
				'uf' => 'SP',
				'cep2' => '3526001',
				'estado_cod' => 26,
				'cep' => '17890-000',
			),
			57 => 
			array (
				'id' => 9299,
				'nome' => 'Juquiá',
				'uf' => 'SP',
				'cep2' => '3526100',
				'estado_cod' => 26,
				'cep' => '11800-000',
			),
			58 => 
			array (
				'id' => 9301,
				'nome' => 'Juquitiba',
				'uf' => 'SP',
				'cep2' => '3526209',
				'estado_cod' => 26,
				'cep' => '06950-000',
			),
			59 => 
			array (
				'id' => 9309,
				'nome' => 'Lagoinha',
				'uf' => 'SP',
				'cep2' => '3526308',
				'estado_cod' => 26,
				'cep' => '12130-000',
			),
			60 => 
			array (
				'id' => 9310,
				'nome' => 'Laranjal Paulista',
				'uf' => 'SP',
				'cep2' => '3526407',
				'estado_cod' => 26,
				'cep' => '18500-000',
			),
			61 => 
			array (
				'id' => 9313,
				'nome' => 'Lavínia',
				'uf' => 'SP',
				'cep2' => '3526506',
				'estado_cod' => 26,
				'cep' => '16850-000',
			),
			62 => 
			array (
				'id' => 9314,
				'nome' => 'Lavrinhas',
				'uf' => 'SP',
				'cep2' => '3526605',
				'estado_cod' => 26,
				'cep' => '12760-000',
			),
			63 => 
			array (
				'id' => 9315,
				'nome' => 'Leme',
				'uf' => 'SP',
				'cep2' => '3526704',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			64 => 
			array (
				'id' => 9316,
				'nome' => 'Lençóis Paulista',
				'uf' => 'SP',
				'cep2' => '3526803',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			65 => 
			array (
				'id' => 9317,
				'nome' => 'Limeira',
				'uf' => 'SP',
				'cep2' => '3526902',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			66 => 
			array (
				'id' => 9318,
				'nome' => 'Lindóia',
				'uf' => 'SP',
				'cep2' => '3527009',
				'estado_cod' => 26,
				'cep' => '13950-000',
			),
			67 => 
			array (
				'id' => 9319,
				'nome' => 'Lins',
				'uf' => 'SP',
				'cep2' => '3527108',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			68 => 
			array (
				'id' => 9321,
				'nome' => 'Lorena',
				'uf' => 'SP',
				'cep2' => '3527207',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			69 => 
			array (
				'id' => 9322,
				'nome' => 'Lourdes',
				'uf' => 'SP',
				'cep2' => '3527256',
				'estado_cod' => 26,
				'cep' => '15285-000',
			),
			70 => 
			array (
				'id' => 9323,
				'nome' => 'Louveira',
				'uf' => 'SP',
				'cep2' => '3527306',
				'estado_cod' => 26,
				'cep' => '13290-000',
			),
			71 => 
			array (
				'id' => 9324,
				'nome' => 'Lucélia',
				'uf' => 'SP',
				'cep2' => '3527405',
				'estado_cod' => 26,
				'cep' => '17780-000',
			),
			72 => 
			array (
				'id' => 9325,
				'nome' => 'Lucianópolis',
				'uf' => 'SP',
				'cep2' => '3527504',
				'estado_cod' => 26,
				'cep' => '17475-000',
			),
			73 => 
			array (
				'id' => 9326,
				'nome' => 'Luís Antônio',
				'uf' => 'SP',
				'cep2' => '3527603',
				'estado_cod' => 26,
				'cep' => '14210-000',
			),
			74 => 
			array (
				'id' => 9327,
				'nome' => 'Luiziânia',
				'uf' => 'SP',
				'cep2' => '3527702',
				'estado_cod' => 26,
				'cep' => '16340-000',
			),
			75 => 
			array (
				'id' => 9328,
				'nome' => 'Lupércio',
				'uf' => 'SP',
				'cep2' => '3527801',
				'estado_cod' => 26,
				'cep' => '17420-000',
			),
			76 => 
			array (
				'id' => 9330,
				'nome' => 'Lutécia',
				'uf' => 'SP',
				'cep2' => '3527900',
				'estado_cod' => 26,
				'cep' => '19750-000',
			),
			77 => 
			array (
				'id' => 9331,
				'nome' => 'Macatuba',
				'uf' => 'SP',
				'cep2' => '3528007',
				'estado_cod' => 26,
				'cep' => '17290-000',
			),
			78 => 
			array (
				'id' => 9332,
				'nome' => 'Macaubal',
				'uf' => 'SP',
				'cep2' => '3528106',
				'estado_cod' => 26,
				'cep' => '15270-000',
			),
			79 => 
			array (
				'id' => 9333,
				'nome' => 'Macedônia',
				'uf' => 'SP',
				'cep2' => '3528205',
				'estado_cod' => 26,
				'cep' => '15620-000',
			),
			80 => 
			array (
				'id' => 9335,
				'nome' => 'Mágda',
				'uf' => 'SP',
				'cep2' => '',
				'estado_cod' => 26,
				'cep' => '15310-000',
			),
			81 => 
			array (
				'id' => 9337,
				'nome' => 'Mairinque',
				'uf' => 'SP',
				'cep2' => '3528403',
				'estado_cod' => 26,
				'cep' => '18120-000',
			),
			82 => 
			array (
				'id' => 9338,
				'nome' => 'Mairiporã',
				'uf' => 'SP',
				'cep2' => '3528502',
				'estado_cod' => 26,
				'cep' => '07600-000',
			),
			83 => 
			array (
				'id' => 9340,
				'nome' => 'Manduri',
				'uf' => 'SP',
				'cep2' => '3528601',
				'estado_cod' => 26,
				'cep' => '18780-000',
			),
			84 => 
			array (
				'id' => 9342,
				'nome' => 'Marabá Paulista',
				'uf' => 'SP',
				'cep2' => '3528700',
				'estado_cod' => 26,
				'cep' => '19430-000',
			),
			85 => 
			array (
				'id' => 9343,
				'nome' => 'Maracaí',
				'uf' => 'SP',
				'cep2' => '3528809',
				'estado_cod' => 26,
				'cep' => '19840-000',
			),
			86 => 
			array (
				'id' => 9344,
				'nome' => 'Marapoama',
				'uf' => 'SP',
				'cep2' => '3528858',
				'estado_cod' => 26,
				'cep' => '15845-000',
			),
			87 => 
			array (
				'id' => 9347,
				'nome' => 'Mariápolis',
				'uf' => 'SP',
				'cep2' => '3528908',
				'estado_cod' => 26,
				'cep' => '17810-000',
			),
			88 => 
			array (
				'id' => 9348,
				'nome' => 'Marília',
				'uf' => 'SP',
				'cep2' => '3529005',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			89 => 
			array (
				'id' => 9349,
				'nome' => 'Marinópolis',
				'uf' => 'SP',
				'cep2' => '3529104',
				'estado_cod' => 26,
				'cep' => '15730-000',
			),
			90 => 
			array (
				'id' => 9353,
				'nome' => 'Martinópolis',
				'uf' => 'SP',
				'cep2' => '3529203',
				'estado_cod' => 26,
				'cep' => '19500-000',
			),
			91 => 
			array (
				'id' => 9354,
				'nome' => 'Matão',
				'uf' => 'SP',
				'cep2' => '3529302',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			92 => 
			array (
				'id' => 9355,
				'nome' => 'Mauá',
				'uf' => 'SP',
				'cep2' => '3529401',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			93 => 
			array (
				'id' => 9356,
				'nome' => 'Mendonça',
				'uf' => 'SP',
				'cep2' => '3529500',
				'estado_cod' => 26,
				'cep' => '15220-000',
			),
			94 => 
			array (
				'id' => 9357,
				'nome' => 'Meridiano',
				'uf' => 'SP',
				'cep2' => '3529609',
				'estado_cod' => 26,
				'cep' => '15625-000',
			),
			95 => 
			array (
				'id' => 9358,
				'nome' => 'Mesópolis',
				'uf' => 'SP',
				'cep2' => '3529658',
				'estado_cod' => 26,
				'cep' => '15748-000',
			),
			96 => 
			array (
				'id' => 9359,
				'nome' => 'Miguelópolis',
				'uf' => 'SP',
				'cep2' => '3529708',
				'estado_cod' => 26,
				'cep' => '14530-000',
			),
			97 => 
			array (
				'id' => 9360,
				'nome' => 'Mineiros do Tietê',
				'uf' => 'SP',
				'cep2' => '3529807',
				'estado_cod' => 26,
				'cep' => '17320-000',
			),
			98 => 
			array (
				'id' => 9361,
				'nome' => 'Mira Estrela',
				'uf' => 'SP',
				'cep2' => '3530003',
				'estado_cod' => 26,
				'cep' => '15580-000',
			),
			99 => 
			array (
				'id' => 9362,
				'nome' => 'Miracatu',
				'uf' => 'SP',
				'cep2' => '3529906',
				'estado_cod' => 26,
				'cep' => '11850-000',
			),
			100 => 
			array (
				'id' => 9364,
				'nome' => 'Mirandópolis',
				'uf' => 'SP',
				'cep2' => '3530102',
				'estado_cod' => 26,
				'cep' => '16800-000',
			),
			101 => 
			array (
				'id' => 9365,
				'nome' => 'Mirante do Paranapanema',
				'uf' => 'SP',
				'cep2' => '3530201',
				'estado_cod' => 26,
				'cep' => '19260-000',
			),
			102 => 
			array (
				'id' => 9366,
				'nome' => 'Mirassol',
				'uf' => 'SP',
				'cep2' => '3530300',
				'estado_cod' => 26,
				'cep' => '15130-000',
			),
			103 => 
			array (
				'id' => 9367,
				'nome' => 'Mirassolândia',
				'uf' => 'SP',
				'cep2' => '3530409',
				'estado_cod' => 26,
				'cep' => '15145-000',
			),
			104 => 
			array (
				'id' => 9368,
				'nome' => 'Mococa',
				'uf' => 'SP',
				'cep2' => '3530508',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			105 => 
			array (
				'id' => 9369,
				'nome' => 'Mogi das Cruzes',
				'uf' => 'SP',
				'cep2' => '3530607',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			106 => 
			array (
				'id' => 9370,
				'nome' => 'Mogi Guaçu',
				'uf' => 'SP',
				'cep2' => '',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			107 => 
			array (
				'id' => 9371,
				'nome' => 'Mogi Mirim',
				'uf' => 'SP',
				'cep2' => '',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			108 => 
			array (
				'id' => 9372,
				'nome' => 'Mombuca',
				'uf' => 'SP',
				'cep2' => '3530904',
				'estado_cod' => 26,
				'cep' => '13380-000',
			),
			109 => 
			array (
				'id' => 9373,
				'nome' => 'Monções',
				'uf' => 'SP',
				'cep2' => '3531001',
				'estado_cod' => 26,
				'cep' => '15275-000',
			),
			110 => 
			array (
				'id' => 9374,
				'nome' => 'Mongaguá',
				'uf' => 'SP',
				'cep2' => '3531100',
				'estado_cod' => 26,
				'cep' => '11730-000',
			),
			111 => 
			array (
				'id' => 9376,
				'nome' => 'Monte Alegre do Sul',
				'uf' => 'SP',
				'cep2' => '3531209',
				'estado_cod' => 26,
				'cep' => '13910-000',
			),
			112 => 
			array (
				'id' => 9377,
				'nome' => 'Monte Alto',
				'uf' => 'SP',
				'cep2' => '3531308',
				'estado_cod' => 26,
				'cep' => '15910-000',
			),
			113 => 
			array (
				'id' => 9378,
				'nome' => 'Monte Aprazível',
				'uf' => 'SP',
				'cep2' => '3531407',
				'estado_cod' => 26,
				'cep' => '15150-000',
			),
			114 => 
			array (
				'id' => 9379,
				'nome' => 'Monte Azul Paulista',
				'uf' => 'SP',
				'cep2' => '3531506',
				'estado_cod' => 26,
				'cep' => '14730-000',
			),
			115 => 
			array (
				'id' => 9381,
				'nome' => 'Monte Castelo',
				'uf' => 'SP',
				'cep2' => '3531605',
				'estado_cod' => 26,
				'cep' => '17960-000',
			),
			116 => 
			array (
				'id' => 9382,
				'nome' => 'Monte Mor',
				'uf' => 'SP',
				'cep2' => '3531803',
				'estado_cod' => 26,
				'cep' => '13190-000',
			),
			117 => 
			array (
				'id' => 9384,
				'nome' => 'Monteiro Lobato',
				'uf' => 'SP',
				'cep2' => '3531704',
				'estado_cod' => 26,
				'cep' => '12250-000',
			),
			118 => 
			array (
				'id' => 9386,
				'nome' => 'Morro Agudo',
				'uf' => 'SP',
				'cep2' => '3531902',
				'estado_cod' => 26,
				'cep' => '14640-000',
			),
			119 => 
			array (
				'id' => 9388,
				'nome' => 'Morungaba',
				'uf' => 'SP',
				'cep2' => '3532009',
				'estado_cod' => 26,
				'cep' => '13260-000',
			),
			120 => 
			array (
				'id' => 9390,
				'nome' => 'Motuca',
				'uf' => 'SP',
				'cep2' => '3532058',
				'estado_cod' => 26,
				'cep' => '14835-000',
			),
			121 => 
			array (
				'id' => 9392,
				'nome' => 'Murutinga do Sul',
				'uf' => 'SP',
				'cep2' => '3532108',
				'estado_cod' => 26,
				'cep' => '16950-000',
			),
			122 => 
			array (
				'id' => 9393,
				'nome' => 'Nantes',
				'uf' => 'SP',
				'cep2' => '3532157',
				'estado_cod' => 26,
				'cep' => '19645-000',
			),
			123 => 
			array (
				'id' => 9394,
				'nome' => 'Narandiba',
				'uf' => 'SP',
				'cep2' => '3532207',
				'estado_cod' => 26,
				'cep' => '19220-000',
			),
			124 => 
			array (
				'id' => 9395,
				'nome' => 'Natividade da Serra',
				'uf' => 'SP',
				'cep2' => '3532306',
				'estado_cod' => 26,
				'cep' => '12180-000',
			),
			125 => 
			array (
				'id' => 9396,
				'nome' => 'Nazaré Paulista',
				'uf' => 'SP',
				'cep2' => '3532405',
				'estado_cod' => 26,
				'cep' => '12960-000',
			),
			126 => 
			array (
				'id' => 9397,
				'nome' => 'Neves Paulista',
				'uf' => 'SP',
				'cep2' => '3532504',
				'estado_cod' => 26,
				'cep' => '15120-000',
			),
			127 => 
			array (
				'id' => 9398,
				'nome' => 'Nhandeara',
				'uf' => 'SP',
				'cep2' => '3532603',
				'estado_cod' => 26,
				'cep' => '15190-000',
			),
			128 => 
			array (
				'id' => 9399,
				'nome' => 'Nipoã',
				'uf' => 'SP',
				'cep2' => '3532702',
				'estado_cod' => 26,
				'cep' => '15240-000',
			),
			129 => 
			array (
				'id' => 9403,
				'nome' => 'Nova Aliança',
				'uf' => 'SP',
				'cep2' => '3532801',
				'estado_cod' => 26,
				'cep' => '15210-000',
			),
			130 => 
			array (
				'id' => 9406,
				'nome' => 'Nova Campina',
				'uf' => 'SP',
				'cep2' => '3532827',
				'estado_cod' => 26,
				'cep' => '18435-000',
			),
			131 => 
			array (
				'id' => 9407,
				'nome' => 'Nova Canaã Paulista',
				'uf' => 'SP',
				'cep2' => '3532843',
				'estado_cod' => 26,
				'cep' => '15773-000',
			),
			132 => 
			array (
				'id' => 9408,
				'nome' => 'Nova Castilho',
				'uf' => 'SP',
				'cep2' => '3532868',
				'estado_cod' => 26,
				'cep' => '15313-000',
			),
			133 => 
			array (
				'id' => 9409,
				'nome' => 'Nova Europa',
				'uf' => 'SP',
				'cep2' => '3532900',
				'estado_cod' => 26,
				'cep' => '14920-000',
			),
			134 => 
			array (
				'id' => 9410,
				'nome' => 'Nova Granada',
				'uf' => 'SP',
				'cep2' => '3533007',
				'estado_cod' => 26,
				'cep' => '15440-000',
			),
			135 => 
			array (
				'id' => 9411,
				'nome' => 'Nova Guataporanga',
				'uf' => 'SP',
				'cep2' => '3533106',
				'estado_cod' => 26,
				'cep' => '17950-000',
			),
			136 => 
			array (
				'id' => 9412,
				'nome' => 'Nova Independência',
				'uf' => 'SP',
				'cep2' => '3533205',
				'estado_cod' => 26,
				'cep' => '16940-000',
			),
			137 => 
			array (
				'id' => 9414,
				'nome' => 'Nova Luzitânia',
				'uf' => 'SP',
				'cep2' => '3533304',
				'estado_cod' => 26,
				'cep' => '15340-000',
			),
			138 => 
			array (
				'id' => 9415,
				'nome' => 'Nova Odessa',
				'uf' => 'SP',
				'cep2' => '3533403',
				'estado_cod' => 26,
				'cep' => '13460-000',
			),
			139 => 
			array (
				'id' => 9418,
				'nome' => 'Novais',
				'uf' => 'SP',
				'cep2' => '3533254',
				'estado_cod' => 26,
				'cep' => '15885-000',
			),
			140 => 
			array (
				'id' => 9420,
				'nome' => 'Novo Horizonte',
				'uf' => 'SP',
				'cep2' => '3533502',
				'estado_cod' => 26,
				'cep' => '14960-000',
			),
			141 => 
			array (
				'id' => 9421,
				'nome' => 'Nuporanga',
				'uf' => 'SP',
				'cep2' => '3533601',
				'estado_cod' => 26,
				'cep' => '14670-000',
			),
			142 => 
			array (
				'id' => 9423,
				'nome' => 'Ocauçu',
				'uf' => 'SP',
				'cep2' => '3533700',
				'estado_cod' => 26,
				'cep' => '17540-000',
			),
			143 => 
			array (
				'id' => 9424,
				'nome' => 'Óleo',
				'uf' => 'SP',
				'cep2' => '3533809',
				'estado_cod' => 26,
				'cep' => '18790-000',
			),
			144 => 
			array (
				'id' => 9425,
				'nome' => 'Olímpia',
				'uf' => 'SP',
				'cep2' => '3533908',
				'estado_cod' => 26,
				'cep' => '15400-000',
			),
			145 => 
			array (
				'id' => 9428,
				'nome' => 'Onda Verde',
				'uf' => 'SP',
				'cep2' => '3534005',
				'estado_cod' => 26,
				'cep' => '15450-000',
			),
			146 => 
			array (
				'id' => 9429,
				'nome' => 'Oriente',
				'uf' => 'SP',
				'cep2' => '3534104',
				'estado_cod' => 26,
				'cep' => '17570-000',
			),
			147 => 
			array (
				'id' => 9430,
				'nome' => 'Orindiúva',
				'uf' => 'SP',
				'cep2' => '3534203',
				'estado_cod' => 26,
				'cep' => '15480-000',
			),
			148 => 
			array (
				'id' => 9431,
				'nome' => 'Orlândia',
				'uf' => 'SP',
				'cep2' => '3534302',
				'estado_cod' => 26,
				'cep' => '14620-000',
			),
			149 => 
			array (
				'id' => 9432,
				'nome' => 'Osasco',
				'uf' => 'SP',
				'cep2' => '3534401',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			150 => 
			array (
				'id' => 9433,
				'nome' => 'Oscar Bressane',
				'uf' => 'SP',
				'cep2' => '3534500',
				'estado_cod' => 26,
				'cep' => '19770-000',
			),
			151 => 
			array (
				'id' => 9434,
				'nome' => 'Osvaldo Cruz',
				'uf' => 'SP',
				'cep2' => '3534609',
				'estado_cod' => 26,
				'cep' => '17700-000',
			),
			152 => 
			array (
				'id' => 9435,
				'nome' => 'Ourinhos',
				'uf' => 'SP',
				'cep2' => '3534708',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			153 => 
			array (
				'id' => 9437,
				'nome' => 'Ouro Verde',
				'uf' => 'SP',
				'cep2' => '3534807',
				'estado_cod' => 26,
				'cep' => '17920-000',
			),
			154 => 
			array (
				'id' => 9438,
				'nome' => 'Ouroeste',
				'uf' => 'SP',
				'cep2' => '3534757',
				'estado_cod' => 26,
				'cep' => '15685-000',
			),
			155 => 
			array (
				'id' => 9439,
				'nome' => 'Pacaembu',
				'uf' => 'SP',
				'cep2' => '3534906',
				'estado_cod' => 26,
				'cep' => '17860-000',
			),
			156 => 
			array (
				'id' => 9441,
				'nome' => 'Palestina',
				'uf' => 'SP',
				'cep2' => '3535002',
				'estado_cod' => 26,
				'cep' => '15470-000',
			),
			157 => 
			array (
				'id' => 9442,
				'nome' => 'Palmares Paulista',
				'uf' => 'SP',
				'cep2' => '3535101',
				'estado_cod' => 26,
				'cep' => '15828-000',
			),
			158 => 
			array (
				'id' => 9443,
				'nome' => 'Palmeira D\'Oeste',
				'uf' => 'SP',
				'cep2' => '3535200',
				'estado_cod' => 26,
				'cep' => '15720-000',
			),
			159 => 
			array (
				'id' => 9445,
				'nome' => 'Palmital',
				'uf' => 'SP',
				'cep2' => '3535309',
				'estado_cod' => 26,
				'cep' => '19970-000',
			),
			160 => 
			array (
				'id' => 9446,
				'nome' => 'Panorama',
				'uf' => 'SP',
				'cep2' => '3535408',
				'estado_cod' => 26,
				'cep' => '17980-000',
			),
			161 => 
			array (
				'id' => 9447,
				'nome' => 'Paraguaçu Paulista',
				'uf' => 'SP',
				'cep2' => '3535507',
				'estado_cod' => 26,
				'cep' => '19700-000',
			),
			162 => 
			array (
				'id' => 9448,
				'nome' => 'Paraibuna',
				'uf' => 'SP',
				'cep2' => '3535606',
				'estado_cod' => 26,
				'cep' => '12260-000',
			),
			163 => 
			array (
				'id' => 9449,
				'nome' => 'Paraíso',
				'uf' => 'SP',
				'cep2' => '3535705',
				'estado_cod' => 26,
				'cep' => '15825-000',
			),
			164 => 
			array (
				'id' => 9452,
				'nome' => 'Paranapanema',
				'uf' => 'SP',
				'cep2' => '3535804',
				'estado_cod' => 26,
				'cep' => '18720-000',
			),
			165 => 
			array (
				'id' => 9454,
				'nome' => 'Paranapuã',
				'uf' => 'SP',
				'cep2' => '3535903',
				'estado_cod' => 26,
				'cep' => '15745-000',
			),
			166 => 
			array (
				'id' => 9455,
				'nome' => 'Parapuã',
				'uf' => 'SP',
				'cep2' => '3536000',
				'estado_cod' => 26,
				'cep' => '17730-000',
			),
			167 => 
			array (
				'id' => 9456,
				'nome' => 'Pardinho',
				'uf' => 'SP',
				'cep2' => '3536109',
				'estado_cod' => 26,
				'cep' => '18640-000',
			),
			168 => 
			array (
				'id' => 9457,
				'nome' => 'Pariquera-Açu',
				'uf' => 'SP',
				'cep2' => '3536208',
				'estado_cod' => 26,
				'cep' => '11930-000',
			),
			169 => 
			array (
				'id' => 9458,
				'nome' => 'Parisi',
				'uf' => 'SP',
				'cep2' => '3536257',
				'estado_cod' => 26,
				'cep' => '15525-000',
			),
			170 => 
			array (
				'id' => 9462,
				'nome' => 'Patrocínio Paulista',
				'uf' => 'SP',
				'cep2' => '3536307',
				'estado_cod' => 26,
				'cep' => '14415-000',
			),
			171 => 
			array (
				'id' => 9463,
				'nome' => 'Paulicéia',
				'uf' => 'SP',
				'cep2' => '3536406',
				'estado_cod' => 26,
				'cep' => '17990-000',
			),
			172 => 
			array (
				'id' => 9464,
				'nome' => 'Paulínia',
				'uf' => 'SP',
				'cep2' => '3536505',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			173 => 
			array (
				'id' => 9465,
				'nome' => 'Paulistânia',
				'uf' => 'SP',
				'cep2' => '3536570',
				'estado_cod' => 26,
				'cep' => '17150-000',
			),
			174 => 
			array (
				'id' => 9466,
				'nome' => 'Paulo de Faria',
				'uf' => 'SP',
				'cep2' => '3536604',
				'estado_cod' => 26,
				'cep' => '15490-000',
			),
			175 => 
			array (
				'id' => 9468,
				'nome' => 'Pederneiras',
				'uf' => 'SP',
				'cep2' => '3536703',
				'estado_cod' => 26,
				'cep' => '17280-000',
			),
			176 => 
			array (
				'id' => 9469,
				'nome' => 'Pedra Bela',
				'uf' => 'SP',
				'cep2' => '3536802',
				'estado_cod' => 26,
				'cep' => '12990-000',
			),
			177 => 
			array (
				'id' => 9471,
				'nome' => 'Pedranópolis',
				'uf' => 'SP',
				'cep2' => '3536901',
				'estado_cod' => 26,
				'cep' => '15630-000',
			),
			178 => 
			array (
				'id' => 9472,
				'nome' => 'Pedregulho',
				'uf' => 'SP',
				'cep2' => '3537008',
				'estado_cod' => 26,
				'cep' => '14470-000',
			),
			179 => 
			array (
				'id' => 9473,
				'nome' => 'Pedreira',
				'uf' => 'SP',
				'cep2' => '3537107',
				'estado_cod' => 26,
				'cep' => '13920-000',
			),
			180 => 
			array (
				'id' => 9474,
				'nome' => 'Pedrinhas Paulista',
				'uf' => 'SP',
				'cep2' => '3537156',
				'estado_cod' => 26,
				'cep' => '19865-000',
			),
			181 => 
			array (
				'id' => 9476,
				'nome' => 'Pedro de Toledo',
				'uf' => 'SP',
				'cep2' => '3537206',
				'estado_cod' => 26,
				'cep' => '11790-000',
			),
			182 => 
			array (
				'id' => 9477,
				'nome' => 'Penápolis',
				'uf' => 'SP',
				'cep2' => '3537305',
				'estado_cod' => 26,
				'cep' => '16300-000',
			),
			183 => 
			array (
				'id' => 9478,
				'nome' => 'Pereira Barreto',
				'uf' => 'SP',
				'cep2' => '3537404',
				'estado_cod' => 26,
				'cep' => '15370-000',
			),
			184 => 
			array (
				'id' => 9479,
				'nome' => 'Pereiras',
				'uf' => 'SP',
				'cep2' => '3537503',
				'estado_cod' => 26,
				'cep' => '18580-000',
			),
			185 => 
			array (
				'id' => 9480,
				'nome' => 'Peruíbe',
				'uf' => 'SP',
				'cep2' => '3537602',
				'estado_cod' => 26,
				'cep' => '11750-000',
			),
			186 => 
			array (
				'id' => 9481,
				'nome' => 'Piacatu',
				'uf' => 'SP',
				'cep2' => '3537701',
				'estado_cod' => 26,
				'cep' => '16230-000',
			),
			187 => 
			array (
				'id' => 9483,
				'nome' => 'Piedade',
				'uf' => 'SP',
				'cep2' => '3537800',
				'estado_cod' => 26,
				'cep' => '18170-000',
			),
			188 => 
			array (
				'id' => 9484,
				'nome' => 'Pilar do Sul',
				'uf' => 'SP',
				'cep2' => '3537909',
				'estado_cod' => 26,
				'cep' => '18185-000',
			),
			189 => 
			array (
				'id' => 9485,
				'nome' => 'Pindamonhangaba',
				'uf' => 'SP',
				'cep2' => '3538006',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			190 => 
			array (
				'id' => 9486,
				'nome' => 'Pindorama',
				'uf' => 'SP',
				'cep2' => '3538105',
				'estado_cod' => 26,
				'cep' => '15830-000',
			),
			191 => 
			array (
				'id' => 9487,
				'nome' => 'Pinhalzinho',
				'uf' => 'SP',
				'cep2' => '3538204',
				'estado_cod' => 26,
				'cep' => '12995-000',
			),
			192 => 
			array (
				'id' => 9490,
				'nome' => 'Piquerobi',
				'uf' => 'SP',
				'cep2' => '3538303',
				'estado_cod' => 26,
				'cep' => '19410-000',
			),
			193 => 
			array (
				'id' => 9491,
				'nome' => 'Piquete',
				'uf' => 'SP',
				'cep2' => '3538501',
				'estado_cod' => 26,
				'cep' => '12620-000',
			),
			194 => 
			array (
				'id' => 9492,
				'nome' => 'Piracaia',
				'uf' => 'SP',
				'cep2' => '3538600',
				'estado_cod' => 26,
				'cep' => '12970-000',
			),
			195 => 
			array (
				'id' => 9493,
				'nome' => 'Piracicaba',
				'uf' => 'SP',
				'cep2' => '3538709',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			196 => 
			array (
				'id' => 9494,
				'nome' => 'Piraju',
				'uf' => 'SP',
				'cep2' => '3538808',
				'estado_cod' => 26,
				'cep' => '18800-000',
			),
			197 => 
			array (
				'id' => 9495,
				'nome' => 'Pirajuí',
				'uf' => 'SP',
				'cep2' => '3538907',
				'estado_cod' => 26,
				'cep' => '16600-000',
			),
			198 => 
			array (
				'id' => 9497,
				'nome' => 'Pirangi',
				'uf' => 'SP',
				'cep2' => '3539004',
				'estado_cod' => 26,
				'cep' => '15820-000',
			),
			199 => 
			array (
				'id' => 9499,
				'nome' => 'Pirapora do Bom Jesus',
				'uf' => 'SP',
				'cep2' => '3539103',
				'estado_cod' => 26,
				'cep' => '06550-000',
			),
			200 => 
			array (
				'id' => 9500,
				'nome' => 'Pirapozinho',
				'uf' => 'SP',
				'cep2' => '3539202',
				'estado_cod' => 26,
				'cep' => '19200-000',
			),
			201 => 
			array (
				'id' => 9501,
				'nome' => 'Pirassununga',
				'uf' => 'SP',
				'cep2' => '3539301',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			202 => 
			array (
				'id' => 9502,
				'nome' => 'Piratininga',
				'uf' => 'SP',
				'cep2' => '3539400',
				'estado_cod' => 26,
				'cep' => '17490-000',
			),
			203 => 
			array (
				'id' => 9503,
				'nome' => 'Pitangueiras',
				'uf' => 'SP',
				'cep2' => '3539509',
				'estado_cod' => 26,
				'cep' => '14750-000',
			),
			204 => 
			array (
				'id' => 9504,
				'nome' => 'Planalto',
				'uf' => 'SP',
				'cep2' => '3539608',
				'estado_cod' => 26,
				'cep' => '15260-000',
			),
			205 => 
			array (
				'id' => 9507,
				'nome' => 'Platina',
				'uf' => 'SP',
				'cep2' => '3539707',
				'estado_cod' => 26,
				'cep' => '19990-000',
			),
			206 => 
			array (
				'id' => 9508,
				'nome' => 'Poá',
				'uf' => 'SP',
				'cep2' => '3539806',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			207 => 
			array (
				'id' => 9509,
				'nome' => 'Poloni',
				'uf' => 'SP',
				'cep2' => '3539905',
				'estado_cod' => 26,
				'cep' => '15160-000',
			),
			208 => 
			array (
				'id' => 9511,
				'nome' => 'Pompéia',
				'uf' => 'SP',
				'cep2' => '3540002',
				'estado_cod' => 26,
				'cep' => '17580-000',
			),
			209 => 
			array (
				'id' => 9512,
				'nome' => 'Pongaí',
				'uf' => 'SP',
				'cep2' => '3540101',
				'estado_cod' => 26,
				'cep' => '16660-000',
			),
			210 => 
			array (
				'id' => 9513,
				'nome' => 'Pontal',
				'uf' => 'SP',
				'cep2' => '3540200',
				'estado_cod' => 26,
				'cep' => '14180-000',
			),
			211 => 
			array (
				'id' => 9514,
				'nome' => 'Pontalinda',
				'uf' => 'SP',
				'cep2' => '3540259',
				'estado_cod' => 26,
				'cep' => '15718-000',
			),
			212 => 
			array (
				'id' => 9515,
				'nome' => 'Pontes Gestal',
				'uf' => 'SP',
				'cep2' => '3540309',
				'estado_cod' => 26,
				'cep' => '15560-000',
			),
			213 => 
			array (
				'id' => 9516,
				'nome' => 'Populina',
				'uf' => 'SP',
				'cep2' => '3540408',
				'estado_cod' => 26,
				'cep' => '15670-000',
			),
			214 => 
			array (
				'id' => 9517,
				'nome' => 'Porangaba',
				'uf' => 'SP',
				'cep2' => '3540507',
				'estado_cod' => 26,
				'cep' => '18260-000',
			),
			215 => 
			array (
				'id' => 9518,
				'nome' => 'Porto Feliz',
				'uf' => 'SP',
				'cep2' => '3540606',
				'estado_cod' => 26,
				'cep' => '18540-000',
			),
			216 => 
			array (
				'id' => 9519,
				'nome' => 'Porto Ferreira',
				'uf' => 'SP',
				'cep2' => '3540705',
				'estado_cod' => 26,
				'cep' => '13660-000',
			),
			217 => 
			array (
				'id' => 9521,
				'nome' => 'Potim',
				'uf' => 'SP',
				'cep2' => '3540754',
				'estado_cod' => 26,
				'cep' => '12525-000',
			),
			218 => 
			array (
				'id' => 9522,
				'nome' => 'Potirendaba',
				'uf' => 'SP',
				'cep2' => '3540804',
				'estado_cod' => 26,
				'cep' => '15105-000',
			),
			219 => 
			array (
				'id' => 9524,
				'nome' => 'Pracinha',
				'uf' => 'SP',
				'cep2' => '3540853',
				'estado_cod' => 26,
				'cep' => '17790-000',
			),
			220 => 
			array (
				'id' => 9526,
				'nome' => 'Pradópolis',
				'uf' => 'SP',
				'cep2' => '3540903',
				'estado_cod' => 26,
				'cep' => '14850-000',
			),
			221 => 
			array (
				'id' => 9527,
				'nome' => 'Praia Grande',
				'uf' => 'SP',
				'cep2' => '3541000',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			222 => 
			array (
				'id' => 9528,
				'nome' => 'Pratânia',
				'uf' => 'SP',
				'cep2' => '3541059',
				'estado_cod' => 26,
				'cep' => '18660-000',
			),
			223 => 
			array (
				'id' => 9529,
				'nome' => 'Presidente Alves',
				'uf' => 'SP',
				'cep2' => '3541109',
				'estado_cod' => 26,
				'cep' => '16670-000',
			),
			224 => 
			array (
				'id' => 9530,
				'nome' => 'Presidente Bernardes',
				'uf' => 'SP',
				'cep2' => '3541208',
				'estado_cod' => 26,
				'cep' => '19300-000',
			),
			225 => 
			array (
				'id' => 9531,
				'nome' => 'Presidente Epitácio',
				'uf' => 'SP',
				'cep2' => '3541307',
				'estado_cod' => 26,
				'cep' => '19470-000',
			),
			226 => 
			array (
				'id' => 9532,
				'nome' => 'Presidente Prudente',
				'uf' => 'SP',
				'cep2' => '3541406',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			227 => 
			array (
				'id' => 9533,
				'nome' => 'Presidente Venceslau',
				'uf' => 'SP',
				'cep2' => '3541505',
				'estado_cod' => 26,
				'cep' => '19400-000',
			),
			228 => 
			array (
				'id' => 9535,
				'nome' => 'Promissão',
				'uf' => 'SP',
				'cep2' => '3541604',
				'estado_cod' => 26,
				'cep' => '16370-000',
			),
			229 => 
			array (
				'id' => 9537,
				'nome' => 'Quadra',
				'uf' => 'SP',
				'cep2' => '3541653',
				'estado_cod' => 26,
				'cep' => '18255-000',
			),
			230 => 
			array (
				'id' => 9538,
				'nome' => 'Quatá',
				'uf' => 'SP',
				'cep2' => '3541703',
				'estado_cod' => 26,
				'cep' => '19780-000',
			),
			231 => 
			array (
				'id' => 9539,
				'nome' => 'Queiroz',
				'uf' => 'SP',
				'cep2' => '3541802',
				'estado_cod' => 26,
				'cep' => '17590-000',
			),
			232 => 
			array (
				'id' => 9540,
				'nome' => 'Queluz',
				'uf' => 'SP',
				'cep2' => '3541901',
				'estado_cod' => 26,
				'cep' => '12800-000',
			),
			233 => 
			array (
				'id' => 9541,
				'nome' => 'Quintana',
				'uf' => 'SP',
				'cep2' => '3542008',
				'estado_cod' => 26,
				'cep' => '17670-000',
			),
			234 => 
			array (
				'id' => 9543,
				'nome' => 'Rafard',
				'uf' => 'SP',
				'cep2' => '3542107',
				'estado_cod' => 26,
				'cep' => '13370-000',
			),
			235 => 
			array (
				'id' => 9544,
				'nome' => 'Rancharia',
				'uf' => 'SP',
				'cep2' => '3542206',
				'estado_cod' => 26,
				'cep' => '19600-000',
			),
			236 => 
			array (
				'id' => 9546,
				'nome' => 'Redenção da Serra',
				'uf' => 'SP',
				'cep2' => '3542305',
				'estado_cod' => 26,
				'cep' => '12170-000',
			),
			237 => 
			array (
				'id' => 9547,
				'nome' => 'Regente Feijó',
				'uf' => 'SP',
				'cep2' => '3542404',
				'estado_cod' => 26,
				'cep' => '19570-000',
			),
			238 => 
			array (
				'id' => 9548,
				'nome' => 'Reginópolis',
				'uf' => 'SP',
				'cep2' => '3542503',
				'estado_cod' => 26,
				'cep' => '17190-000',
			),
			239 => 
			array (
				'id' => 9549,
				'nome' => 'Registro',
				'uf' => 'SP',
				'cep2' => '3542602',
				'estado_cod' => 26,
				'cep' => '11900-000',
			),
			240 => 
			array (
				'id' => 9550,
				'nome' => 'Restinga',
				'uf' => 'SP',
				'cep2' => '3542701',
				'estado_cod' => 26,
				'cep' => '14430-000',
			),
			241 => 
			array (
				'id' => 9552,
				'nome' => 'Ribeira',
				'uf' => 'SP',
				'cep2' => '3542800',
				'estado_cod' => 26,
				'cep' => '18380-000',
			),
			242 => 
			array (
				'id' => 9553,
				'nome' => 'Ribeirão Bonito',
				'uf' => 'SP',
				'cep2' => '3542909',
				'estado_cod' => 26,
				'cep' => '13580-000',
			),
			243 => 
			array (
				'id' => 9554,
				'nome' => 'Ribeirão Branco',
				'uf' => 'SP',
				'cep2' => '3543006',
				'estado_cod' => 26,
				'cep' => '18430-000',
			),
			244 => 
			array (
				'id' => 9555,
				'nome' => 'Ribeirão Corrente',
				'uf' => 'SP',
				'cep2' => '3543105',
				'estado_cod' => 26,
				'cep' => '14445-000',
			),
			245 => 
			array (
				'id' => 9556,
				'nome' => 'Ribeirão do Sul',
				'uf' => 'SP',
				'cep2' => '3543204',
				'estado_cod' => 26,
				'cep' => '19930-000',
			),
			246 => 
			array (
				'id' => 9557,
				'nome' => 'Ribeirão dos Índios',
				'uf' => 'SP',
				'cep2' => '3543238',
				'estado_cod' => 26,
				'cep' => '19380-000',
			),
			247 => 
			array (
				'id' => 9558,
				'nome' => 'Ribeirão Grande',
				'uf' => 'SP',
				'cep2' => '3543253',
				'estado_cod' => 26,
				'cep' => '18315-000',
			),
			248 => 
			array (
				'id' => 9559,
				'nome' => 'Ribeirão Pires',
				'uf' => 'SP',
				'cep2' => '3543303',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			249 => 
			array (
				'id' => 9560,
				'nome' => 'Ribeirão Preto',
				'uf' => 'SP',
				'cep2' => '3543402',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			250 => 
			array (
				'id' => 9563,
				'nome' => 'Rifaina',
				'uf' => 'SP',
				'cep2' => '3543600',
				'estado_cod' => 26,
				'cep' => '14490-000',
			),
			251 => 
			array (
				'id' => 9564,
				'nome' => 'Rincão',
				'uf' => 'SP',
				'cep2' => '3543709',
				'estado_cod' => 26,
				'cep' => '14830-000',
			),
			252 => 
			array (
				'id' => 9565,
				'nome' => 'Rinópolis',
				'uf' => 'SP',
				'cep2' => '3543808',
				'estado_cod' => 26,
				'cep' => '17740-000',
			),
			253 => 
			array (
				'id' => 9566,
				'nome' => 'Rio Claro',
				'uf' => 'SP',
				'cep2' => '3543907',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			254 => 
			array (
				'id' => 9567,
				'nome' => 'Rio das Pedras',
				'uf' => 'SP',
				'cep2' => '3544004',
				'estado_cod' => 26,
				'cep' => '13390-000',
			),
			255 => 
			array (
				'id' => 9568,
				'nome' => 'Rio Grande da Serra',
				'uf' => 'SP',
				'cep2' => '3544103',
				'estado_cod' => 26,
				'cep' => '09450-000',
			),
			256 => 
			array (
				'id' => 9569,
				'nome' => 'Riolândia',
				'uf' => 'SP',
				'cep2' => '3544202',
				'estado_cod' => 26,
				'cep' => '15495-000',
			),
			257 => 
			array (
				'id' => 9570,
				'nome' => 'Riversul',
				'uf' => 'SP',
				'cep2' => '3543501',
				'estado_cod' => 26,
				'cep' => '18470-000',
			),
			258 => 
			array (
				'id' => 9573,
				'nome' => 'Rosana',
				'uf' => 'SP',
				'cep2' => '3544251',
				'estado_cod' => 26,
				'cep' => '19273-000',
			),
			259 => 
			array (
				'id' => 9574,
				'nome' => 'Roseira',
				'uf' => 'SP',
				'cep2' => '3544301',
				'estado_cod' => 26,
				'cep' => '12580-000',
			),
			260 => 
			array (
				'id' => 9575,
				'nome' => 'Rubiácea',
				'uf' => 'SP',
				'cep2' => '3544400',
				'estado_cod' => 26,
				'cep' => '16750-000',
			),
			261 => 
			array (
				'id' => 9577,
				'nome' => 'Rubinéia',
				'uf' => 'SP',
				'cep2' => '3544509',
				'estado_cod' => 26,
				'cep' => '15790-000',
			),
			262 => 
			array (
				'id' => 9580,
				'nome' => 'Sabino',
				'uf' => 'SP',
				'cep2' => '3544608',
				'estado_cod' => 26,
				'cep' => '16440-000',
			),
			263 => 
			array (
				'id' => 9581,
				'nome' => 'Sagres',
				'uf' => 'SP',
				'cep2' => '3544707',
				'estado_cod' => 26,
				'cep' => '17710-000',
			),
			264 => 
			array (
				'id' => 9582,
				'nome' => 'Sales',
				'uf' => 'SP',
				'cep2' => '3544806',
				'estado_cod' => 26,
				'cep' => '14980-000',
			),
			265 => 
			array (
				'id' => 9583,
				'nome' => 'Sales Oliveira',
				'uf' => 'SP',
				'cep2' => '3544905',
				'estado_cod' => 26,
				'cep' => '14660-000',
			),
			266 => 
			array (
				'id' => 9584,
				'nome' => 'Salesópolis',
				'uf' => 'SP',
				'cep2' => '3545001',
				'estado_cod' => 26,
				'cep' => '08970-000',
			),
			267 => 
			array (
				'id' => 9585,
				'nome' => 'Salmourão',
				'uf' => 'SP',
				'cep2' => '3545100',
				'estado_cod' => 26,
				'cep' => '17720-000',
			),
			268 => 
			array (
				'id' => 9586,
				'nome' => 'Saltinho',
				'uf' => 'SP',
				'cep2' => '3545159',
				'estado_cod' => 26,
				'cep' => '13440-000',
			),
			269 => 
			array (
				'id' => 9587,
				'nome' => 'Salto',
				'uf' => 'SP',
				'cep2' => '3545209',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			270 => 
			array (
				'id' => 9588,
				'nome' => 'Salto de Pirapora',
				'uf' => 'SP',
				'cep2' => '3545308',
				'estado_cod' => 26,
				'cep' => '18160-000',
			),
			271 => 
			array (
				'id' => 9590,
				'nome' => 'Salto Grande',
				'uf' => 'SP',
				'cep2' => '3545407',
				'estado_cod' => 26,
				'cep' => '19920-000',
			),
			272 => 
			array (
				'id' => 9591,
				'nome' => 'Sandovalina',
				'uf' => 'SP',
				'cep2' => '3545506',
				'estado_cod' => 26,
				'cep' => '19250-000',
			),
			273 => 
			array (
				'id' => 9592,
				'nome' => 'Santa Adélia',
				'uf' => 'SP',
				'cep2' => '3545605',
				'estado_cod' => 26,
				'cep' => '15950-000',
			),
			274 => 
			array (
				'id' => 9593,
				'nome' => 'Santa Albertina',
				'uf' => 'SP',
				'cep2' => '3545704',
				'estado_cod' => 26,
				'cep' => '15750-000',
			),
			275 => 
			array (
				'id' => 9595,
				'nome' => 'Santa Bárbara D\'Oeste',
				'uf' => 'SP',
				'cep2' => '3545803',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			276 => 
			array (
				'id' => 9596,
				'nome' => 'Santa Branca',
				'uf' => 'SP',
				'cep2' => '3546009',
				'estado_cod' => 26,
				'cep' => '12380-000',
			),
			277 => 
			array (
				'id' => 9597,
				'nome' => 'Santa Clara D\'Oeste',
				'uf' => 'SP',
				'cep2' => '3546108',
				'estado_cod' => 26,
				'cep' => '15785-000',
			),
			278 => 
			array (
				'id' => 9598,
				'nome' => 'Santa Cruz da Conceição',
				'uf' => 'SP',
				'cep2' => '3546207',
				'estado_cod' => 26,
				'cep' => '13625-000',
			),
			279 => 
			array (
				'id' => 9599,
				'nome' => 'Santa Cruz da Esperança',
				'uf' => 'SP',
				'cep2' => '3546256',
				'estado_cod' => 26,
				'cep' => '14250-000',
			),
			280 => 
			array (
				'id' => 9601,
				'nome' => 'Santa Cruz das Palmeiras',
				'uf' => 'SP',
				'cep2' => '3546306',
				'estado_cod' => 26,
				'cep' => '13650-000',
			),
			281 => 
			array (
				'id' => 9602,
				'nome' => 'Santa Cruz do Rio Pardo',
				'uf' => 'SP',
				'cep2' => '3546405',
				'estado_cod' => 26,
				'cep' => '18900-000',
			),
			282 => 
			array (
				'id' => 9604,
				'nome' => 'Santa Ernestina',
				'uf' => 'SP',
				'cep2' => '3546504',
				'estado_cod' => 26,
				'cep' => '15970-000',
			),
			283 => 
			array (
				'id' => 9606,
				'nome' => 'Santa Fé do Sul',
				'uf' => 'SP',
				'cep2' => '3546603',
				'estado_cod' => 26,
				'cep' => '15775-000',
			),
			284 => 
			array (
				'id' => 9607,
				'nome' => 'Santa Gertrudes',
				'uf' => 'SP',
				'cep2' => '3546702',
				'estado_cod' => 26,
				'cep' => '13510-000',
			),
			285 => 
			array (
				'id' => 9608,
				'nome' => 'Santa Isabel',
				'uf' => 'SP',
				'cep2' => '3546801',
				'estado_cod' => 26,
				'cep' => '07500-000',
			),
			286 => 
			array (
				'id' => 9610,
				'nome' => 'Santa Lúcia',
				'uf' => 'SP',
				'cep2' => '3546900',
				'estado_cod' => 26,
				'cep' => '14825-000',
			),
			287 => 
			array (
				'id' => 9612,
				'nome' => 'Santa Maria da Serra',
				'uf' => 'SP',
				'cep2' => '3547007',
				'estado_cod' => 26,
				'cep' => '17370-000',
			),
			288 => 
			array (
				'id' => 9614,
				'nome' => 'Santa Mercedes',
				'uf' => 'SP',
				'cep2' => '3547106',
				'estado_cod' => 26,
				'cep' => '17940-000',
			),
			289 => 
			array (
				'id' => 9615,
				'nome' => 'Santa Rita D\'Oeste',
				'uf' => 'SP',
				'cep2' => '3547403',
				'estado_cod' => 26,
				'cep' => '15780-000',
			),
			290 => 
			array (
				'id' => 9616,
				'nome' => 'Santa Rita do Passa Quatro',
				'uf' => 'SP',
				'cep2' => '3547502',
				'estado_cod' => 26,
				'cep' => '13670-000',
			),
			291 => 
			array (
				'id' => 9618,
				'nome' => 'Santa Rosa de Viterbo',
				'uf' => 'SP',
				'cep2' => '3547601',
				'estado_cod' => 26,
				'cep' => '14270-000',
			),
			292 => 
			array (
				'id' => 9619,
				'nome' => 'Santa Salete',
				'uf' => 'SP',
				'cep2' => '3547650',
				'estado_cod' => 26,
				'cep' => '15768-000',
			),
			293 => 
			array (
				'id' => 9621,
				'nome' => 'Santana da Ponte Pensa',
				'uf' => 'SP',
				'cep2' => '3547205',
				'estado_cod' => 26,
				'cep' => '15765-000',
			),
			294 => 
			array (
				'id' => 9622,
				'nome' => 'Santana de Parnaíba',
				'uf' => 'SP',
				'cep2' => '3547304',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			295 => 
			array (
				'id' => 9624,
				'nome' => 'Santo Anastácio',
				'uf' => 'SP',
				'cep2' => '3547700',
				'estado_cod' => 26,
				'cep' => '19360-000',
			),
			296 => 
			array (
				'id' => 9625,
				'nome' => 'Santo André',
				'uf' => 'SP',
				'cep2' => '3547809',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			297 => 
			array (
				'id' => 9626,
				'nome' => 'Santo Antônio da Alegria',
				'uf' => 'SP',
				'cep2' => '3547908',
				'estado_cod' => 26,
				'cep' => '14390-000',
			),
			298 => 
			array (
				'id' => 9628,
				'nome' => 'Santo Antônio de Posse',
				'uf' => 'SP',
				'cep2' => '3548005',
				'estado_cod' => 26,
				'cep' => '13830-000',
			),
			299 => 
			array (
				'id' => 9629,
				'nome' => 'Santo Antônio do Aracanguá',
				'uf' => 'SP',
				'cep2' => '3548054',
				'estado_cod' => 26,
				'cep' => '16130-000',
			),
			300 => 
			array (
				'id' => 9630,
				'nome' => 'Santo Antônio do Jardim',
				'uf' => 'SP',
				'cep2' => '3548104',
				'estado_cod' => 26,
				'cep' => '13995-000',
			),
			301 => 
			array (
				'id' => 9632,
				'nome' => 'Santo Antônio do Pinhal',
				'uf' => 'SP',
				'cep2' => '3548203',
				'estado_cod' => 26,
				'cep' => '12450-000',
			),
			302 => 
			array (
				'id' => 9634,
				'nome' => 'Santo Expedito',
				'uf' => 'SP',
				'cep2' => '3548302',
				'estado_cod' => 26,
				'cep' => '19190-000',
			),
			303 => 
			array (
				'id' => 9635,
				'nome' => 'Santópolis do Aguapeí',
				'uf' => 'SP',
				'cep2' => '3548401',
				'estado_cod' => 26,
				'cep' => '16240-000',
			),
			304 => 
			array (
				'id' => 9636,
				'nome' => 'Santos',
				'uf' => 'SP',
				'cep2' => '3548500',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			305 => 
			array (
				'id' => 9639,
				'nome' => 'São Bento do Sapucaí',
				'uf' => 'SP',
				'cep2' => '3548609',
				'estado_cod' => 26,
				'cep' => '12490-000',
			),
			306 => 
			array (
				'id' => 9640,
				'nome' => 'São Bernardo do Campo',
				'uf' => 'SP',
				'cep2' => '3548708',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			307 => 
			array (
				'id' => 9642,
				'nome' => 'São Caetano do Sul',
				'uf' => 'SP',
				'cep2' => '3548807',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			308 => 
			array (
				'id' => 9643,
				'nome' => 'São Carlos',
				'uf' => 'SP',
				'cep2' => '3548906',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			309 => 
			array (
				'id' => 9644,
				'nome' => 'São Francisco',
				'uf' => 'SP',
				'cep2' => '3549003',
				'estado_cod' => 26,
				'cep' => '15710-000',
			),
			310 => 
			array (
				'id' => 9647,
				'nome' => 'São João da Boa Vista',
				'uf' => 'SP',
				'cep2' => '3549102',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			311 => 
			array (
				'id' => 9648,
				'nome' => 'São João das Duas Pontes',
				'uf' => 'SP',
				'cep2' => '3549201',
				'estado_cod' => 26,
				'cep' => '15640-000',
			),
			312 => 
			array (
				'id' => 9649,
				'nome' => 'São João de Iracema',
				'uf' => 'SP',
				'cep2' => '3549250',
				'estado_cod' => 26,
				'cep' => '15315-000',
			),
			313 => 
			array (
				'id' => 9652,
				'nome' => 'São João do Pau d\'Alho',
				'uf' => 'SP',
				'cep2' => '3549300',
				'estado_cod' => 26,
				'cep' => '17970-000',
			),
			314 => 
			array (
				'id' => 9654,
				'nome' => 'São Joaquim da Barra',
				'uf' => 'SP',
				'cep2' => '3549409',
				'estado_cod' => 26,
				'cep' => '14600-000',
			),
			315 => 
			array (
				'id' => 9655,
				'nome' => 'São José da Bela Vista',
				'uf' => 'SP',
				'cep2' => '3549508',
				'estado_cod' => 26,
				'cep' => '14440-000',
			),
			316 => 
			array (
				'id' => 9657,
				'nome' => 'São José do Barreiro',
				'uf' => 'SP',
				'cep2' => '3549607',
				'estado_cod' => 26,
				'cep' => '12830-000',
			),
			317 => 
			array (
				'id' => 9658,
				'nome' => 'São José do Rio Pardo',
				'uf' => 'SP',
				'cep2' => '3549706',
				'estado_cod' => 26,
				'cep' => '13720-000',
			),
			318 => 
			array (
				'id' => 9659,
				'nome' => 'São José do Rio Preto',
				'uf' => 'SP',
				'cep2' => '3549805',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			319 => 
			array (
				'id' => 9660,
				'nome' => 'São José dos Campos',
				'uf' => 'SP',
				'cep2' => '3549904',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			320 => 
			array (
				'id' => 9661,
				'nome' => 'São Lourenço da Serra',
				'uf' => 'SP',
				'cep2' => '3549953',
				'estado_cod' => 26,
				'cep' => '06890-000',
			),
			321 => 
			array (
				'id' => 9663,
				'nome' => 'São Luiz do Paraitinga',
				'uf' => 'SP',
				'cep2' => '',
				'estado_cod' => 26,
				'cep' => '12140-000',
			),
			322 => 
			array (
				'id' => 9665,
				'nome' => 'São Manuel',
				'uf' => 'SP',
				'cep2' => '3550100',
				'estado_cod' => 26,
				'cep' => '18650-000',
			),
			323 => 
			array (
				'id' => 9667,
				'nome' => 'São Miguel Arcanjo',
				'uf' => 'SP',
				'cep2' => '3550209',
				'estado_cod' => 26,
				'cep' => '18230-000',
			),
			324 => 
			array (
				'id' => 9668,
				'nome' => 'São Paulo',
				'uf' => 'SP',
				'cep2' => '3550308',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			325 => 
			array (
				'id' => 9669,
				'nome' => 'São Pedro',
				'uf' => 'SP',
				'cep2' => '3550407',
				'estado_cod' => 26,
				'cep' => '13520-000',
			),
			326 => 
			array (
				'id' => 9670,
				'nome' => 'São Pedro do Turvo',
				'uf' => 'SP',
				'cep2' => '3550506',
				'estado_cod' => 26,
				'cep' => '18940-000',
			),
			327 => 
			array (
				'id' => 9671,
				'nome' => 'São Roque',
				'uf' => 'SP',
				'cep2' => '3550605',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			328 => 
			array (
				'id' => 9673,
				'nome' => 'São Sebastião',
				'uf' => 'SP',
				'cep2' => '3550704',
				'estado_cod' => 26,
				'cep' => '11600-000',
			),
			329 => 
			array (
				'id' => 9674,
				'nome' => 'São Sebastião da Grama',
				'uf' => 'SP',
				'cep2' => '3550803',
				'estado_cod' => 26,
				'cep' => '13790-000',
			),
			330 => 
			array (
				'id' => 9677,
				'nome' => 'São Simão',
				'uf' => 'SP',
				'cep2' => '3550902',
				'estado_cod' => 26,
				'cep' => '14200-000',
			),
			331 => 
			array (
				'id' => 9678,
				'nome' => 'São Vicente',
				'uf' => 'SP',
				'cep2' => '3551009',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			332 => 
			array (
				'id' => 9680,
				'nome' => 'Sarapuí',
				'uf' => 'SP',
				'cep2' => '3551108',
				'estado_cod' => 26,
				'cep' => '18225-000',
			),
			333 => 
			array (
				'id' => 9681,
				'nome' => 'Sarutaiá',
				'uf' => 'SP',
				'cep2' => '3551207',
				'estado_cod' => 26,
				'cep' => '18840-000',
			),
			334 => 
			array (
				'id' => 9682,
				'nome' => 'Sebastianópolis do Sul',
				'uf' => 'SP',
				'cep2' => '3551306',
				'estado_cod' => 26,
				'cep' => '15180-000',
			),
			335 => 
			array (
				'id' => 9683,
				'nome' => 'Serra Azul',
				'uf' => 'SP',
				'cep2' => '3551405',
				'estado_cod' => 26,
				'cep' => '14230-000',
			),
			336 => 
			array (
				'id' => 9684,
				'nome' => 'Serra Negra',
				'uf' => 'SP',
				'cep2' => '3551603',
				'estado_cod' => 26,
				'cep' => '13930-000',
			),
			337 => 
			array (
				'id' => 9685,
				'nome' => 'Serrana',
				'uf' => 'SP',
				'cep2' => '3551504',
				'estado_cod' => 26,
				'cep' => '14150-000',
			),
			338 => 
			array (
				'id' => 9686,
				'nome' => 'Sertãozinho',
				'uf' => 'SP',
				'cep2' => '3551702',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			339 => 
			array (
				'id' => 9687,
				'nome' => 'Sete Barras',
				'uf' => 'SP',
				'cep2' => '3551801',
				'estado_cod' => 26,
				'cep' => '11910-000',
			),
			340 => 
			array (
				'id' => 9688,
				'nome' => 'Severínia',
				'uf' => 'SP',
				'cep2' => '3551900',
				'estado_cod' => 26,
				'cep' => '14735-000',
			),
			341 => 
			array (
				'id' => 9690,
				'nome' => 'Silveiras',
				'uf' => 'SP',
				'cep2' => '3552007',
				'estado_cod' => 26,
				'cep' => '12690-000',
			),
			342 => 
			array (
				'id' => 9693,
				'nome' => 'Socorro',
				'uf' => 'SP',
				'cep2' => '3552106',
				'estado_cod' => 26,
				'cep' => '13960-000',
			),
			343 => 
			array (
				'id' => 9696,
				'nome' => 'Sorocaba',
				'uf' => 'SP',
				'cep2' => '3552205',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			344 => 
			array (
				'id' => 9698,
				'nome' => 'Sud Mennucci',
				'uf' => 'SP',
				'cep2' => '3552304',
				'estado_cod' => 26,
				'cep' => '15360-000',
			),
			345 => 
			array (
				'id' => 9700,
				'nome' => 'Sumaré',
				'uf' => 'SP',
				'cep2' => '3552403',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			346 => 
			array (
				'id' => 9702,
				'nome' => 'Suzanápolis',
				'uf' => 'SP',
				'cep2' => '',
				'estado_cod' => 26,
				'cep' => '15380-000',
			),
			347 => 
			array (
				'id' => 9703,
				'nome' => 'Suzano',
				'uf' => 'SP',
				'cep2' => '3552502',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			348 => 
			array (
				'id' => 9705,
				'nome' => 'Tabapuã',
				'uf' => 'SP',
				'cep2' => '3552601',
				'estado_cod' => 26,
				'cep' => '15880-000',
			),
			349 => 
			array (
				'id' => 9706,
				'nome' => 'Tabatinga',
				'uf' => 'SP',
				'cep2' => '3552700',
				'estado_cod' => 26,
				'cep' => '14910-000',
			),
			350 => 
			array (
				'id' => 9707,
				'nome' => 'Taboão da Serra',
				'uf' => 'SP',
				'cep2' => '3552809',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			351 => 
			array (
				'id' => 9708,
				'nome' => 'Taciba',
				'uf' => 'SP',
				'cep2' => '3552908',
				'estado_cod' => 26,
				'cep' => '19590-000',
			),
			352 => 
			array (
				'id' => 9709,
				'nome' => 'Taguaí',
				'uf' => 'SP',
				'cep2' => '3553005',
				'estado_cod' => 26,
				'cep' => '18890-000',
			),
			353 => 
			array (
				'id' => 9710,
				'nome' => 'Taiaçu',
				'uf' => 'SP',
				'cep2' => '3553104',
				'estado_cod' => 26,
				'cep' => '14725-000',
			),
			354 => 
			array (
				'id' => 9712,
				'nome' => 'Taiúva',
				'uf' => 'SP',
				'cep2' => '3553203',
				'estado_cod' => 26,
				'cep' => '14720-000',
			),
			355 => 
			array (
				'id' => 9714,
				'nome' => 'Tambaú',
				'uf' => 'SP',
				'cep2' => '3553302',
				'estado_cod' => 26,
				'cep' => '13710-000',
			),
			356 => 
			array (
				'id' => 9715,
				'nome' => 'Tanabi',
				'uf' => 'SP',
				'cep2' => '3553401',
				'estado_cod' => 26,
				'cep' => '15170-000',
			),
			357 => 
			array (
				'id' => 9717,
				'nome' => 'Tapiraí',
				'uf' => 'SP',
				'cep2' => '3553500',
				'estado_cod' => 26,
				'cep' => '18180-000',
			),
			358 => 
			array (
				'id' => 9718,
				'nome' => 'Tapiratiba',
				'uf' => 'SP',
				'cep2' => '3553609',
				'estado_cod' => 26,
				'cep' => '13760-000',
			),
			359 => 
			array (
				'id' => 9719,
				'nome' => 'Taquaral',
				'uf' => 'SP',
				'cep2' => '3553658',
				'estado_cod' => 26,
				'cep' => '14765-000',
			),
			360 => 
			array (
				'id' => 9720,
				'nome' => 'Taquaritinga',
				'uf' => 'SP',
				'cep2' => '3553708',
				'estado_cod' => 26,
				'cep' => '15900-000',
			),
			361 => 
			array (
				'id' => 9721,
				'nome' => 'Taquarituba',
				'uf' => 'SP',
				'cep2' => '3553807',
				'estado_cod' => 26,
				'cep' => '18740-000',
			),
			362 => 
			array (
				'id' => 9722,
				'nome' => 'Taquarivaí',
				'uf' => 'SP',
				'cep2' => '3553856',
				'estado_cod' => 26,
				'cep' => '18425-000',
			),
			363 => 
			array (
				'id' => 9723,
				'nome' => 'Tarabai',
				'uf' => 'SP',
				'cep2' => '3553906',
				'estado_cod' => 26,
				'cep' => '19210-000',
			),
			364 => 
			array (
				'id' => 9724,
				'nome' => 'Tarumã',
				'uf' => 'SP',
				'cep2' => '3553955',
				'estado_cod' => 26,
				'cep' => '19820-000',
			),
			365 => 
			array (
				'id' => 9725,
				'nome' => 'Tatuí',
				'uf' => 'SP',
				'cep2' => '3554003',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			366 => 
			array (
				'id' => 9726,
				'nome' => 'Taubaté',
				'uf' => 'SP',
				'cep2' => '3554102',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			367 => 
			array (
				'id' => 9728,
				'nome' => 'Tejupá',
				'uf' => 'SP',
				'cep2' => '3554201',
				'estado_cod' => 26,
				'cep' => '18830-000',
			),
			368 => 
			array (
				'id' => 9729,
				'nome' => 'Teodoro Sampaio',
				'uf' => 'SP',
				'cep2' => '3554300',
				'estado_cod' => 26,
				'cep' => '19280-000',
			),
			369 => 
			array (
				'id' => 9732,
				'nome' => 'Terra Roxa',
				'uf' => 'SP',
				'cep2' => '3554409',
				'estado_cod' => 26,
				'cep' => '14745-000',
			),
			370 => 
			array (
				'id' => 9735,
				'nome' => 'Tietê',
				'uf' => 'SP',
				'cep2' => '3554508',
				'estado_cod' => 26,
				'cep' => '18530-000',
			),
			371 => 
			array (
				'id' => 9736,
				'nome' => 'Timburi',
				'uf' => 'SP',
				'cep2' => '3554607',
				'estado_cod' => 26,
				'cep' => '18860-000',
			),
			372 => 
			array (
				'id' => 9738,
				'nome' => 'Torre de Pedra',
				'uf' => 'SP',
				'cep2' => '3554656',
				'estado_cod' => 26,
				'cep' => '18265-000',
			),
			373 => 
			array (
				'id' => 9739,
				'nome' => 'Torrinha',
				'uf' => 'SP',
				'cep2' => '3554706',
				'estado_cod' => 26,
				'cep' => '17360-000',
			),
			374 => 
			array (
				'id' => 9740,
				'nome' => 'Trabiju',
				'uf' => 'SP',
				'cep2' => '3554755',
				'estado_cod' => 26,
				'cep' => '14935-000',
			),
			375 => 
			array (
				'id' => 9741,
				'nome' => 'Tremembé',
				'uf' => 'SP',
				'cep2' => '3554805',
				'estado_cod' => 26,
				'cep' => '12120-000',
			),
			376 => 
			array (
				'id' => 9743,
				'nome' => 'Três Fronteiras',
				'uf' => 'SP',
				'cep2' => '3554904',
				'estado_cod' => 26,
				'cep' => '15770-000',
			),
			377 => 
			array (
				'id' => 9746,
				'nome' => 'Tuiuti',
				'uf' => 'SP',
				'cep2' => '3554953',
				'estado_cod' => 26,
				'cep' => '12930-000',
			),
			378 => 
			array (
				'id' => 9748,
				'nome' => 'Tupã',
				'uf' => 'SP',
				'cep2' => '3555000',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			379 => 
			array (
				'id' => 9750,
				'nome' => 'Tupi Paulista',
				'uf' => 'SP',
				'cep2' => '3555109',
				'estado_cod' => 26,
				'cep' => '17930-000',
			),
			380 => 
			array (
				'id' => 9752,
				'nome' => 'Turiúba',
				'uf' => 'SP',
				'cep2' => '3555208',
				'estado_cod' => 26,
				'cep' => '15280-000',
			),
			381 => 
			array (
				'id' => 9753,
				'nome' => 'Turmalina',
				'uf' => 'SP',
				'cep2' => '3555307',
				'estado_cod' => 26,
				'cep' => '15755-000',
			),
			382 => 
			array (
				'id' => 9755,
				'nome' => 'Ubarana',
				'uf' => 'SP',
				'cep2' => '3555356',
				'estado_cod' => 26,
				'cep' => '15225-000',
			),
			383 => 
			array (
				'id' => 9756,
				'nome' => 'Ubatuba',
				'uf' => 'SP',
				'cep2' => '3555406',
				'estado_cod' => 26,
				'cep' => '11680-000',
			),
			384 => 
			array (
				'id' => 9757,
				'nome' => 'Ubirajara',
				'uf' => 'SP',
				'cep2' => '3555505',
				'estado_cod' => 26,
				'cep' => '17440-000',
			),
			385 => 
			array (
				'id' => 9758,
				'nome' => 'Uchoa',
				'uf' => 'SP',
				'cep2' => '3555604',
				'estado_cod' => 26,
				'cep' => '15890-000',
			),
			386 => 
			array (
				'id' => 9759,
				'nome' => 'União Paulista',
				'uf' => 'SP',
				'cep2' => '3555703',
				'estado_cod' => 26,
				'cep' => '15250-000',
			),
			387 => 
			array (
				'id' => 9761,
				'nome' => 'Urânia',
				'uf' => 'SP',
				'cep2' => '3555802',
				'estado_cod' => 26,
				'cep' => '15760-000',
			),
			388 => 
			array (
				'id' => 9762,
				'nome' => 'Uru',
				'uf' => 'SP',
				'cep2' => '3555901',
				'estado_cod' => 26,
				'cep' => '16650-000',
			),
			389 => 
			array (
				'id' => 9763,
				'nome' => 'Urupês',
				'uf' => 'SP',
				'cep2' => '3556008',
				'estado_cod' => 26,
				'cep' => '15850-000',
			),
			390 => 
			array (
				'id' => 9767,
				'nome' => 'Valentim Gentil',
				'uf' => 'SP',
				'cep2' => '3556107',
				'estado_cod' => 26,
				'cep' => '15520-000',
			),
			391 => 
			array (
				'id' => 9768,
				'nome' => 'Valinhos',
				'uf' => 'SP',
				'cep2' => '3556206',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			392 => 
			array (
				'id' => 9769,
				'nome' => 'Valparaíso',
				'uf' => 'SP',
				'cep2' => '3556305',
				'estado_cod' => 26,
				'cep' => '16880-000',
			),
			393 => 
			array (
				'id' => 9771,
				'nome' => 'Vargem',
				'uf' => 'SP',
				'cep2' => '3556354',
				'estado_cod' => 26,
				'cep' => '12935-000',
			),
			394 => 
			array (
				'id' => 9772,
				'nome' => 'Vargem Grande do Sul',
				'uf' => 'SP',
				'cep2' => '3556404',
				'estado_cod' => 26,
				'cep' => '13880-000',
			),
			395 => 
			array (
				'id' => 9773,
				'nome' => 'Vargem Grande Paulista',
				'uf' => 'SP',
				'cep2' => '3556453',
				'estado_cod' => 26,
				'cep' => '06730-000',
			),
			396 => 
			array (
				'id' => 9775,
				'nome' => 'Várzea Paulista',
				'uf' => 'SP',
				'cep2' => '3556503',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			397 => 
			array (
				'id' => 9777,
				'nome' => 'Vera Cruz',
				'uf' => 'SP',
				'cep2' => '3556602',
				'estado_cod' => 26,
				'cep' => '17560-000',
			),
			398 => 
			array (
				'id' => 9783,
				'nome' => 'Vinhedo',
				'uf' => 'SP',
				'cep2' => '3556701',
				'estado_cod' => 26,
				'cep' => '13280-000',
			),
			399 => 
			array (
				'id' => 9784,
				'nome' => 'Viradouro',
				'uf' => 'SP',
				'cep2' => '3556800',
				'estado_cod' => 26,
				'cep' => '14740-000',
			),
			400 => 
			array (
				'id' => 9785,
				'nome' => 'Vista Alegre do Alto',
				'uf' => 'SP',
				'cep2' => '3556909',
				'estado_cod' => 26,
				'cep' => '15920-000',
			),
			401 => 
			array (
				'id' => 9786,
				'nome' => 'Vitória Brasil',
				'uf' => 'SP',
				'cep2' => '3556958',
				'estado_cod' => 26,
				'cep' => '15713-000',
			),
			402 => 
			array (
				'id' => 9788,
				'nome' => 'Votorantim',
				'uf' => 'SP',
				'cep2' => '3557006',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			403 => 
			array (
				'id' => 9789,
				'nome' => 'Votuporanga',
				'uf' => 'SP',
				'cep2' => '3557105',
				'estado_cod' => 26,
				'cep' => 'LOC',
			),
			404 => 
			array (
				'id' => 9790,
				'nome' => 'Zacarias',
				'uf' => 'SP',
				'cep2' => '3557154',
				'estado_cod' => 26,
				'cep' => '15265-000',
			),
			405 => 
			array (
				'id' => 9791,
				'nome' => 'Abreulândia',
				'uf' => 'TO',
				'cep2' => '1700251',
				'estado_cod' => 27,
				'cep' => '77693-000',
			),
			406 => 
			array (
				'id' => 9792,
				'nome' => 'Aguiarnópolis',
				'uf' => 'TO',
				'cep2' => '1700301',
				'estado_cod' => 27,
				'cep' => '77908-000',
			),
			407 => 
			array (
				'id' => 9793,
				'nome' => 'Aliança do Tocantins',
				'uf' => 'TO',
				'cep2' => '1700350',
				'estado_cod' => 27,
				'cep' => '77455-000',
			),
			408 => 
			array (
				'id' => 9794,
				'nome' => 'Almas',
				'uf' => 'TO',
				'cep2' => '1700400',
				'estado_cod' => 27,
				'cep' => '77310-000',
			),
			409 => 
			array (
				'id' => 9795,
				'nome' => 'Alvorada',
				'uf' => 'TO',
				'cep2' => '1700707',
				'estado_cod' => 27,
				'cep' => '77480-000',
			),
			410 => 
			array (
				'id' => 9797,
				'nome' => 'Ananás',
				'uf' => 'TO',
				'cep2' => '1701002',
				'estado_cod' => 27,
				'cep' => '77890-000',
			),
			411 => 
			array (
				'id' => 9798,
				'nome' => 'Angico',
				'uf' => 'TO',
				'cep2' => '1701051',
				'estado_cod' => 27,
				'cep' => '77905-000',
			),
			412 => 
			array (
				'id' => 9799,
				'nome' => 'Aparecida do Rio Negro',
				'uf' => 'TO',
				'cep2' => '1701101',
				'estado_cod' => 27,
				'cep' => '77620-000',
			),
			413 => 
			array (
				'id' => 9802,
				'nome' => 'Aragominas',
				'uf' => 'TO',
				'cep2' => '1701309',
				'estado_cod' => 27,
				'cep' => '77845-000',
			),
			414 => 
			array (
				'id' => 9803,
				'nome' => 'Araguacema',
				'uf' => 'TO',
				'cep2' => '1701903',
				'estado_cod' => 27,
				'cep' => '77690-000',
			),
			415 => 
			array (
				'id' => 9804,
				'nome' => 'Araguaçu',
				'uf' => 'TO',
				'cep2' => '1702000',
				'estado_cod' => 27,
				'cep' => '77475-000',
			),
			416 => 
			array (
				'id' => 9805,
				'nome' => 'Araguaína',
				'uf' => 'TO',
				'cep2' => '1702109',
				'estado_cod' => 27,
				'cep' => 'LOC',
			),
			417 => 
			array (
				'id' => 9806,
				'nome' => 'Araguanã',
				'uf' => 'TO',
				'cep2' => '1702158',
				'estado_cod' => 27,
				'cep' => '77855-000',
			),
			418 => 
			array (
				'id' => 9807,
				'nome' => 'Araguatins',
				'uf' => 'TO',
				'cep2' => '1702208',
				'estado_cod' => 27,
				'cep' => '77950-000',
			),
			419 => 
			array (
				'id' => 9808,
				'nome' => 'Arapoema',
				'uf' => 'TO',
				'cep2' => '1702307',
				'estado_cod' => 27,
				'cep' => '77780-000',
			),
			420 => 
			array (
				'id' => 9809,
				'nome' => 'Arraias',
				'uf' => 'TO',
				'cep2' => '1702406',
				'estado_cod' => 27,
				'cep' => '77330-000',
			),
			421 => 
			array (
				'id' => 9810,
				'nome' => 'Augustinópolis',
				'uf' => 'TO',
				'cep2' => '1702554',
				'estado_cod' => 27,
				'cep' => '77960-000',
			),
			422 => 
			array (
				'id' => 9811,
				'nome' => 'Aurora do Tocantins',
				'uf' => 'TO',
				'cep2' => '1702703',
				'estado_cod' => 27,
				'cep' => '77325-000',
			),
			423 => 
			array (
				'id' => 9812,
				'nome' => 'Axixá do Tocantins',
				'uf' => 'TO',
				'cep2' => '1702901',
				'estado_cod' => 27,
				'cep' => '77930-000',
			),
			424 => 
			array (
				'id' => 9813,
				'nome' => 'Babaçulândia',
				'uf' => 'TO',
				'cep2' => '1703008',
				'estado_cod' => 27,
				'cep' => '77870-000',
			),
			425 => 
			array (
				'id' => 9814,
				'nome' => 'Bandeirantes do Tocantins',
				'uf' => 'TO',
				'cep2' => '1703057',
				'estado_cod' => 27,
				'cep' => '77783-000',
			),
			426 => 
			array (
				'id' => 9816,
				'nome' => 'Barra do Ouro',
				'uf' => 'TO',
				'cep2' => '1703073',
				'estado_cod' => 27,
				'cep' => '77765-000',
			),
			427 => 
			array (
				'id' => 9817,
				'nome' => 'Barrolândia',
				'uf' => 'TO',
				'cep2' => '1703107',
				'estado_cod' => 27,
				'cep' => '77665-000',
			),
			428 => 
			array (
				'id' => 9818,
				'nome' => 'Bernardo Sayão',
				'uf' => 'TO',
				'cep2' => '1703206',
				'estado_cod' => 27,
				'cep' => '77755-000',
			),
			429 => 
			array (
				'id' => 9819,
				'nome' => 'Bom Jesus do Tocantins',
				'uf' => 'TO',
				'cep2' => '1703305',
				'estado_cod' => 27,
				'cep' => '77714-000',
			),
			430 => 
			array (
				'id' => 9821,
				'nome' => 'Brasilândia do Tocantins',
				'uf' => 'TO',
				'cep2' => '1703602',
				'estado_cod' => 27,
				'cep' => '77735-000',
			),
			431 => 
			array (
				'id' => 9822,
				'nome' => 'Brejinho de Nazaré',
				'uf' => 'TO',
				'cep2' => '1703701',
				'estado_cod' => 27,
				'cep' => '77560-000',
			),
			432 => 
			array (
				'id' => 9823,
				'nome' => 'Buriti do Tocantins',
				'uf' => 'TO',
				'cep2' => '1703800',
				'estado_cod' => 27,
				'cep' => '77995-000',
			),
			433 => 
			array (
				'id' => 9824,
				'nome' => 'Cachoeirinha',
				'uf' => 'TO',
				'cep2' => '1703826',
				'estado_cod' => 27,
				'cep' => '77915-000',
			),
			434 => 
			array (
				'id' => 9825,
				'nome' => 'Campos Lindos',
				'uf' => 'TO',
				'cep2' => '1703842',
				'estado_cod' => 27,
				'cep' => '77777-000',
			),
			435 => 
			array (
				'id' => 9827,
				'nome' => 'Cariri do Tocantins',
				'uf' => 'TO',
				'cep2' => '1703867',
				'estado_cod' => 27,
				'cep' => '77453-000',
			),
			436 => 
			array (
				'id' => 9828,
				'nome' => 'Carmolândia',
				'uf' => 'TO',
				'cep2' => '1703883',
				'estado_cod' => 27,
				'cep' => '77840-000',
			),
			437 => 
			array (
				'id' => 9829,
				'nome' => 'Carrasco Bonito',
				'uf' => 'TO',
				'cep2' => '1703891',
				'estado_cod' => 27,
				'cep' => '77985-000',
			),
			438 => 
			array (
				'id' => 9831,
				'nome' => 'Caseara',
				'uf' => 'TO',
				'cep2' => '1703909',
				'estado_cod' => 27,
				'cep' => '77680-000',
			),
			439 => 
			array (
				'id' => 9832,
				'nome' => 'Centenário',
				'uf' => 'TO',
				'cep2' => '1704105',
				'estado_cod' => 27,
				'cep' => '77723-000',
			),
			440 => 
			array (
				'id' => 9833,
				'nome' => 'Chapada de Areia',
				'uf' => 'TO',
				'cep2' => '1704600',
				'estado_cod' => 27,
				'cep' => '77575-000',
			),
			441 => 
			array (
				'id' => 9834,
				'nome' => 'Chapada da Natividade',
				'uf' => 'TO',
				'cep2' => '1705102',
				'estado_cod' => 27,
				'cep' => '77378-000',
			),
			442 => 
			array (
				'id' => 9837,
				'nome' => 'Colinas do Tocantins',
				'uf' => 'TO',
				'cep2' => '1705508',
				'estado_cod' => 27,
				'cep' => '77760-000',
			),
			443 => 
			array (
				'id' => 9838,
				'nome' => 'Colméia',
				'uf' => 'TO',
				'cep2' => '1716703',
				'estado_cod' => 27,
				'cep' => '77725-000',
			),
			444 => 
			array (
				'id' => 9839,
				'nome' => 'Combinado',
				'uf' => 'TO',
				'cep2' => '1705557',
				'estado_cod' => 27,
				'cep' => '77350-000',
			),
			445 => 
			array (
				'id' => 9840,
				'nome' => 'Conceição do Tocantins',
				'uf' => 'TO',
				'cep2' => '1705607',
				'estado_cod' => 27,
				'cep' => '77305-000',
			),
			446 => 
			array (
				'id' => 9842,
				'nome' => 'Couto de Magalhães',
				'uf' => 'TO',
				'cep2' => '1706001',
				'estado_cod' => 27,
				'cep' => '77750-000',
			),
			447 => 
			array (
				'id' => 9844,
				'nome' => 'Cristalândia',
				'uf' => 'TO',
				'cep2' => '1706100',
				'estado_cod' => 27,
				'cep' => '77490-000',
			),
			448 => 
			array (
				'id' => 9846,
				'nome' => 'Crixás do Tocantins',
				'uf' => 'TO',
				'cep2' => '1706258',
				'estado_cod' => 27,
				'cep' => '77463-000',
			),
			449 => 
			array (
				'id' => 9847,
				'nome' => 'Darcinópolis',
				'uf' => 'TO',
				'cep2' => '1706506',
				'estado_cod' => 27,
				'cep' => '77910-000',
			),
			450 => 
			array (
				'id' => 9848,
				'nome' => 'Dianópolis',
				'uf' => 'TO',
				'cep2' => '1707009',
				'estado_cod' => 27,
				'cep' => '77300-000',
			),
			451 => 
			array (
				'id' => 9849,
				'nome' => 'Divinópolis do Tocantins',
				'uf' => 'TO',
				'cep2' => '1707108',
				'estado_cod' => 27,
				'cep' => '77670-000',
			),
			452 => 
			array (
				'id' => 9850,
				'nome' => 'Dois Irmãos do Tocantins',
				'uf' => 'TO',
				'cep2' => '1707207',
				'estado_cod' => 27,
				'cep' => '77685-000',
			),
			453 => 
			array (
				'id' => 9851,
				'nome' => 'Duerê',
				'uf' => 'TO',
				'cep2' => '',
				'estado_cod' => 27,
				'cep' => '77485-000',
			),
			454 => 
			array (
				'id' => 9853,
				'nome' => 'Esperantina',
				'uf' => 'TO',
				'cep2' => '1707405',
				'estado_cod' => 27,
				'cep' => '77993-000',
			),
			455 => 
			array (
				'id' => 9854,
				'nome' => 'Fátima',
				'uf' => 'TO',
				'cep2' => '1707553',
				'estado_cod' => 27,
				'cep' => '77555-000',
			),
			456 => 
			array (
				'id' => 9855,
				'nome' => 'Figueirópolis',
				'uf' => 'TO',
				'cep2' => '1707652',
				'estado_cod' => 27,
				'cep' => '77465-000',
			),
			457 => 
			array (
				'id' => 9856,
				'nome' => 'Filadélfia',
				'uf' => 'TO',
				'cep2' => '1707702',
				'estado_cod' => 27,
				'cep' => '77795-000',
			),
			458 => 
			array (
				'id' => 9857,
				'nome' => 'Formoso do Araguaia',
				'uf' => 'TO',
				'cep2' => '1708205',
				'estado_cod' => 27,
				'cep' => '77470-000',
			),
			459 => 
			array (
				'id' => 9858,
				'nome' => 'Fortaleza do Tabocão',
				'uf' => 'TO',
				'cep2' => '1708254',
				'estado_cod' => 27,
				'cep' => '77708-000',
			),
			460 => 
			array (
				'id' => 9859,
				'nome' => 'Goianorte',
				'uf' => 'TO',
				'cep2' => '1708304',
				'estado_cod' => 27,
				'cep' => '77695-000',
			),
			461 => 
			array (
				'id' => 9860,
				'nome' => 'Goiatins',
				'uf' => 'TO',
				'cep2' => '1709005',
				'estado_cod' => 27,
				'cep' => '77770-000',
			),
			462 => 
			array (
				'id' => 9861,
				'nome' => 'Guaraí',
				'uf' => 'TO',
				'cep2' => '1709302',
				'estado_cod' => 27,
				'cep' => '77700-000',
			),
			463 => 
			array (
				'id' => 9862,
				'nome' => 'Gurupi',
				'uf' => 'TO',
				'cep2' => '1709500',
				'estado_cod' => 27,
				'cep' => 'LOC',
			),
			464 => 
			array (
				'id' => 9864,
				'nome' => 'Ipueiras',
				'uf' => 'TO',
				'cep2' => '1709807',
				'estado_cod' => 27,
				'cep' => '77553-000',
			),
			465 => 
			array (
				'id' => 9865,
				'nome' => 'Itacajá',
				'uf' => 'TO',
				'cep2' => '1710508',
				'estado_cod' => 27,
				'cep' => '77720-000',
			),
			466 => 
			array (
				'id' => 9866,
				'nome' => 'Itaguatins',
				'uf' => 'TO',
				'cep2' => '1710706',
				'estado_cod' => 27,
				'cep' => '77920-000',
			),
			467 => 
			array (
				'id' => 9867,
				'nome' => 'Itapiratins',
				'uf' => 'TO',
				'cep2' => '1710904',
				'estado_cod' => 27,
				'cep' => '77718-000',
			),
			468 => 
			array (
				'id' => 9868,
				'nome' => 'Itaporã do Tocantins',
				'uf' => 'TO',
				'cep2' => '1711100',
				'estado_cod' => 27,
				'cep' => '77740-000',
			),
			469 => 
			array (
				'id' => 9869,
				'nome' => 'Jaú do Tocantins',
				'uf' => 'TO',
				'cep2' => '1711506',
				'estado_cod' => 27,
				'cep' => '77450-000',
			),
			470 => 
			array (
				'id' => 9870,
				'nome' => 'Juarina',
				'uf' => 'TO',
				'cep2' => '1711803',
				'estado_cod' => 27,
				'cep' => '77753-000',
			),
			471 => 
			array (
				'id' => 9872,
				'nome' => 'Lagoa da Confusão',
				'uf' => 'TO',
				'cep2' => '1711902',
				'estado_cod' => 27,
				'cep' => '77493-000',
			),
			472 => 
			array (
				'id' => 9873,
				'nome' => 'Lagoa do Tocantins',
				'uf' => 'TO',
				'cep2' => '1711951',
				'estado_cod' => 27,
				'cep' => '77613-000',
			),
			473 => 
			array (
				'id' => 9874,
				'nome' => 'Lajeado',
				'uf' => 'TO',
				'cep2' => '1712009',
				'estado_cod' => 27,
				'cep' => '77645-000',
			),
			474 => 
			array (
				'id' => 9875,
				'nome' => 'Lavandeira',
				'uf' => 'TO',
				'cep2' => '1712157',
				'estado_cod' => 27,
				'cep' => '77328-000',
			),
			475 => 
			array (
				'id' => 9876,
				'nome' => 'Lizarda',
				'uf' => 'TO',
				'cep2' => '1712405',
				'estado_cod' => 27,
				'cep' => '77630-000',
			),
			476 => 
			array (
				'id' => 9877,
				'nome' => 'Luzinópolis',
				'uf' => 'TO',
				'cep2' => '1712454',
				'estado_cod' => 27,
				'cep' => '77903-000',
			),
			477 => 
			array (
				'id' => 9878,
				'nome' => 'Marianópolis do Tocantins',
				'uf' => 'TO',
				'cep2' => '1712504',
				'estado_cod' => 27,
				'cep' => '77675-000',
			),
			478 => 
			array (
				'id' => 9879,
				'nome' => 'Mateiros',
				'uf' => 'TO',
				'cep2' => '1712702',
				'estado_cod' => 27,
				'cep' => '77593-000',
			),
			479 => 
			array (
				'id' => 9880,
				'nome' => 'Maurilândia do Tocantins',
				'uf' => 'TO',
				'cep2' => '1712801',
				'estado_cod' => 27,
				'cep' => '77918-000',
			),
			480 => 
			array (
				'id' => 9881,
				'nome' => 'Miracema do Tocantins',
				'uf' => 'TO',
				'cep2' => '1713205',
				'estado_cod' => 27,
				'cep' => '77650-000',
			),
			481 => 
			array (
				'id' => 9883,
				'nome' => 'Miranorte',
				'uf' => 'TO',
				'cep2' => '1713304',
				'estado_cod' => 27,
				'cep' => '77660-000',
			),
			482 => 
			array (
				'id' => 9884,
				'nome' => 'Monte do Carmo',
				'uf' => 'TO',
				'cep2' => '1713601',
				'estado_cod' => 27,
				'cep' => '77585-000',
			),
			483 => 
			array (
				'id' => 9886,
				'nome' => 'Monte Santo do Tocantins',
				'uf' => 'TO',
				'cep2' => '1713700',
				'estado_cod' => 27,
				'cep' => '77673-000',
			),
			484 => 
			array (
				'id' => 9887,
				'nome' => 'Palmeiras do Tocantins',
				'uf' => 'TO',
				'cep2' => '1713809',
				'estado_cod' => 27,
				'cep' => '77913-000',
			),
			485 => 
			array (
				'id' => 9888,
				'nome' => 'Muricilândia',
				'uf' => 'TO',
				'cep2' => '1713957',
				'estado_cod' => 27,
				'cep' => '77850-000',
			),
			486 => 
			array (
				'id' => 9890,
				'nome' => 'Natividade',
				'uf' => 'TO',
				'cep2' => '1714203',
				'estado_cod' => 27,
				'cep' => '77370-000',
			),
			487 => 
			array (
				'id' => 9891,
				'nome' => 'Nazaré',
				'uf' => 'TO',
				'cep2' => '1714302',
				'estado_cod' => 27,
				'cep' => '77895-000',
			),
			488 => 
			array (
				'id' => 9892,
				'nome' => 'Nova Olinda',
				'uf' => 'TO',
				'cep2' => '1714880',
				'estado_cod' => 27,
				'cep' => '77790-000',
			),
			489 => 
			array (
				'id' => 9893,
				'nome' => 'Nova Rosalândia',
				'uf' => 'TO',
				'cep2' => '1715002',
				'estado_cod' => 27,
				'cep' => '77495-000',
			),
			490 => 
			array (
				'id' => 9894,
				'nome' => 'Novo Acordo',
				'uf' => 'TO',
				'cep2' => '1715101',
				'estado_cod' => 27,
				'cep' => '77610-000',
			),
			491 => 
			array (
				'id' => 9895,
				'nome' => 'Novo Alegre',
				'uf' => 'TO',
				'cep2' => '1715150',
				'estado_cod' => 27,
				'cep' => '77353-000',
			),
			492 => 
			array (
				'id' => 9897,
				'nome' => 'Novo Jardim',
				'uf' => 'TO',
				'cep2' => '1715259',
				'estado_cod' => 27,
				'cep' => '77318-000',
			),
			493 => 
			array (
				'id' => 9898,
				'nome' => 'Oliveira de Fátima',
				'uf' => 'TO',
				'cep2' => '1715507',
				'estado_cod' => 27,
				'cep' => '77558-000',
			),
			494 => 
			array (
				'id' => 9899,
				'nome' => 'Palmas',
				'uf' => 'TO',
				'cep2' => '1721000',
				'estado_cod' => 27,
				'cep' => 'LOC',
			),
			495 => 
			array (
				'id' => 9900,
				'nome' => 'Palmeirante',
				'uf' => 'TO',
				'cep2' => '1715705',
				'estado_cod' => 27,
				'cep' => '77798-000',
			),
			496 => 
			array (
				'id' => 9901,
				'nome' => 'Palmeirópolis',
				'uf' => 'TO',
				'cep2' => '1715754',
				'estado_cod' => 27,
				'cep' => '77365-000',
			),
			497 => 
			array (
				'id' => 9902,
				'nome' => 'Paraíso do Tocantins',
				'uf' => 'TO',
				'cep2' => '1716109',
				'estado_cod' => 27,
				'cep' => '77600-000',
			),
			498 => 
			array (
				'id' => 9903,
				'nome' => 'Paranã',
				'uf' => 'TO',
				'cep2' => '1716208',
				'estado_cod' => 27,
				'cep' => '77360-000',
			),
			499 => 
			array (
				'id' => 9904,
				'nome' => 'Pau D\'Arco',
				'uf' => 'TO',
				'cep2' => '1716307',
				'estado_cod' => 27,
				'cep' => '77785-000',
			),
		));
		\DB::table('cidades')->insert(array (
			0 => 
			array (
				'id' => 9906,
				'nome' => 'Pedro Afonso',
				'uf' => 'TO',
				'cep2' => '1716505',
				'estado_cod' => 27,
				'cep' => '77710-000',
			),
			1 => 
			array (
				'id' => 9908,
				'nome' => 'Peixe',
				'uf' => 'TO',
				'cep2' => '1716604',
				'estado_cod' => 27,
				'cep' => '77460-000',
			),
			2 => 
			array (
				'id' => 9910,
				'nome' => 'Pequizeiro',
				'uf' => 'TO',
				'cep2' => '1716653',
				'estado_cod' => 27,
				'cep' => '77730-000',
			),
			3 => 
			array (
				'id' => 9912,
				'nome' => 'Pindorama do Tocantins',
				'uf' => 'TO',
				'cep2' => '1717008',
				'estado_cod' => 27,
				'cep' => '77380-000',
			),
			4 => 
			array (
				'id' => 9913,
				'nome' => 'Piraquê',
				'uf' => 'TO',
				'cep2' => '1717206',
				'estado_cod' => 27,
				'cep' => '77888-000',
			),
			5 => 
			array (
				'id' => 9914,
				'nome' => 'Pium',
				'uf' => 'TO',
				'cep2' => '1717503',
				'estado_cod' => 27,
				'cep' => '77570-000',
			),
			6 => 
			array (
				'id' => 9915,
				'nome' => 'Ponte Alta do Bom Jesus',
				'uf' => 'TO',
				'cep2' => '1717800',
				'estado_cod' => 27,
				'cep' => '77315-000',
			),
			7 => 
			array (
				'id' => 9916,
				'nome' => 'Ponte Alta do Tocantins',
				'uf' => 'TO',
				'cep2' => '1717909',
				'estado_cod' => 27,
				'cep' => '77590-000',
			),
			8 => 
			array (
				'id' => 9919,
				'nome' => 'Porto Alegre do Tocantins',
				'uf' => 'TO',
				'cep2' => '1718006',
				'estado_cod' => 27,
				'cep' => '77395-000',
			),
			9 => 
			array (
				'id' => 9921,
				'nome' => 'Porto Nacional',
				'uf' => 'TO',
				'cep2' => '1718204',
				'estado_cod' => 27,
				'cep' => '77500-000',
			),
			10 => 
			array (
				'id' => 9922,
				'nome' => 'Praia Norte',
				'uf' => 'TO',
				'cep2' => '1718303',
				'estado_cod' => 27,
				'cep' => '77970-000',
			),
			11 => 
			array (
				'id' => 9923,
				'nome' => 'Presidente Kennedy',
				'uf' => 'TO',
				'cep2' => '1718402',
				'estado_cod' => 27,
				'cep' => '77745-000',
			),
			12 => 
			array (
				'id' => 9925,
				'nome' => 'Pugmil',
				'uf' => 'TO',
				'cep2' => '1718451',
				'estado_cod' => 27,
				'cep' => '77603-000',
			),
			13 => 
			array (
				'id' => 9926,
				'nome' => 'Recursolândia',
				'uf' => 'TO',
				'cep2' => '1718501',
				'estado_cod' => 27,
				'cep' => '77733-000',
			),
			14 => 
			array (
				'id' => 9927,
				'nome' => 'Riachinho',
				'uf' => 'TO',
				'cep2' => '1718550',
				'estado_cod' => 27,
				'cep' => '77893-000',
			),
			15 => 
			array (
				'id' => 9928,
				'nome' => 'Rio da Conceição',
				'uf' => 'TO',
				'cep2' => '1718659',
				'estado_cod' => 27,
				'cep' => '77303-000',
			),
			16 => 
			array (
				'id' => 9929,
				'nome' => 'Rio dos Bois',
				'uf' => 'TO',
				'cep2' => '1718709',
				'estado_cod' => 27,
				'cep' => '77655-000',
			),
			17 => 
			array (
				'id' => 9930,
				'nome' => 'Rio Sono',
				'uf' => 'TO',
				'cep2' => '1718758',
				'estado_cod' => 27,
				'cep' => '77635-000',
			),
			18 => 
			array (
				'id' => 9931,
				'nome' => 'Sampaio',
				'uf' => 'TO',
				'cep2' => '1718808',
				'estado_cod' => 27,
				'cep' => '77980-000',
			),
			19 => 
			array (
				'id' => 9932,
				'nome' => 'Sandolândia',
				'uf' => 'TO',
				'cep2' => '1718840',
				'estado_cod' => 27,
				'cep' => '77478-000',
			),
			20 => 
			array (
				'id' => 9933,
				'nome' => 'Santa Fé do Araguaia',
				'uf' => 'TO',
				'cep2' => '1718865',
				'estado_cod' => 27,
				'cep' => '77848-000',
			),
			21 => 
			array (
				'id' => 9934,
				'nome' => 'Santa Maria do Tocantins',
				'uf' => 'TO',
				'cep2' => '1718881',
				'estado_cod' => 27,
				'cep' => '77716-000',
			),
			22 => 
			array (
				'id' => 9935,
				'nome' => 'Santa Rita do Tocantins',
				'uf' => 'TO',
				'cep2' => '1718899',
				'estado_cod' => 27,
				'cep' => '77565-000',
			),
			23 => 
			array (
				'id' => 9936,
				'nome' => 'Santa Rosa do Tocantins',
				'uf' => 'TO',
				'cep2' => '1718907',
				'estado_cod' => 27,
				'cep' => '77375-000',
			),
			24 => 
			array (
				'id' => 9937,
				'nome' => 'Santa Tereza do Tocantins',
				'uf' => 'TO',
				'cep2' => '1719004',
				'estado_cod' => 27,
				'cep' => '77615-000',
			),
			25 => 
			array (
				'id' => 9938,
				'nome' => 'Santa Terezinha do Tocantins',
				'uf' => 'TO',
				'cep2' => '1720002',
				'estado_cod' => 27,
				'cep' => '77885-000',
			),
			26 => 
			array (
				'id' => 9939,
				'nome' => 'São Bento do Tocantins',
				'uf' => 'TO',
				'cep2' => '1720101',
				'estado_cod' => 27,
				'cep' => '77958-000',
			),
			27 => 
			array (
				'id' => 9940,
				'nome' => 'São Félix do Tocantins',
				'uf' => 'TO',
				'cep2' => '1720150',
				'estado_cod' => 27,
				'cep' => '77605-000',
			),
			28 => 
			array (
				'id' => 9941,
				'nome' => 'São Miguel do Tocantins',
				'uf' => 'TO',
				'cep2' => '1720200',
				'estado_cod' => 27,
				'cep' => '77925-000',
			),
			29 => 
			array (
				'id' => 9942,
				'nome' => 'São Salvador do Tocantins',
				'uf' => 'TO',
				'cep2' => '1720259',
				'estado_cod' => 27,
				'cep' => '77368-000',
			),
			30 => 
			array (
				'id' => 9943,
				'nome' => 'São Sebastião do Tocantins',
				'uf' => 'TO',
				'cep2' => '1720309',
				'estado_cod' => 27,
				'cep' => '77990-000',
			),
			31 => 
			array (
				'id' => 9944,
				'nome' => 'São Valério da Natividade',
				'uf' => 'TO',
				'cep2' => '1720499',
				'estado_cod' => 27,
				'cep' => '77390-000',
			),
			32 => 
			array (
				'id' => 9945,
				'nome' => 'Silvanópolis',
				'uf' => 'TO',
				'cep2' => '1720655',
				'estado_cod' => 27,
				'cep' => '77580-000',
			),
			33 => 
			array (
				'id' => 9946,
				'nome' => 'Sítio Novo do Tocantins',
				'uf' => 'TO',
				'cep2' => '1720804',
				'estado_cod' => 27,
				'cep' => '77940-000',
			),
			34 => 
			array (
				'id' => 9947,
				'nome' => 'Sucupira',
				'uf' => 'TO',
				'cep2' => '1720853',
				'estado_cod' => 27,
				'cep' => '77458-000',
			),
			35 => 
			array (
				'id' => 9948,
				'nome' => 'Taguatinga',
				'uf' => 'TO',
				'cep2' => '1720903',
				'estado_cod' => 27,
				'cep' => '77320-000',
			),
			36 => 
			array (
				'id' => 9949,
				'nome' => 'Taipas do Tocantins',
				'uf' => 'TO',
				'cep2' => '1720937',
				'estado_cod' => 27,
				'cep' => '77308-000',
			),
			37 => 
			array (
				'id' => 9950,
				'nome' => 'Talismã',
				'uf' => 'TO',
				'cep2' => '1720978',
				'estado_cod' => 27,
				'cep' => '77483-000',
			),
			38 => 
			array (
				'id' => 9954,
				'nome' => 'Tocantínia',
				'uf' => 'TO',
				'cep2' => '1721109',
				'estado_cod' => 27,
				'cep' => '77640-000',
			),
			39 => 
			array (
				'id' => 9955,
				'nome' => 'Tocantinópolis',
				'uf' => 'TO',
				'cep2' => '1721208',
				'estado_cod' => 27,
				'cep' => '77900-000',
			),
			40 => 
			array (
				'id' => 9956,
				'nome' => 'Tupirama',
				'uf' => 'TO',
				'cep2' => '1721257',
				'estado_cod' => 27,
				'cep' => '77704-000',
			),
			41 => 
			array (
				'id' => 9958,
				'nome' => 'Tupiratins',
				'uf' => 'TO',
				'cep2' => '1721307',
				'estado_cod' => 27,
				'cep' => '77743-000',
			),
			42 => 
			array (
				'id' => 9960,
				'nome' => 'Wanderlândia',
				'uf' => 'TO',
				'cep2' => '1722081',
				'estado_cod' => 27,
				'cep' => '77860-000',
			),
			43 => 
			array (
				'id' => 9961,
				'nome' => 'Xambioá',
				'uf' => 'TO',
				'cep2' => '1722107',
				'estado_cod' => 27,
				'cep' => '77880-000',
			),
			44 => 
			array (
				'id' => 10089,
				'nome' => 'Jequiá da Praia',
				'uf' => 'AL',
				'cep2' => '2703759',
				'estado_cod' => 2,
				'cep' => '57244-000',
			),
			45 => 
			array (
				'id' => 10090,
				'nome' => 'Ipiranga de Goiás',
				'uf' => 'GO',
				'cep2' => '5210158',
				'estado_cod' => 9,
				'cep' => '76304-000',
			),
			46 => 
			array (
				'id' => 10091,
				'nome' => 'Conquista D\'Oeste',
				'uf' => 'MT',
				'cep2' => '5103361',
				'estado_cod' => 13,
				'cep' => '78254-000',
			),
			47 => 
			array (
				'id' => 10092,
				'nome' => 'Colniza',
				'uf' => 'MT',
				'cep2' => '5103254',
				'estado_cod' => 13,
				'cep' => '78335-000',
			),
			48 => 
			array (
				'id' => 10093,
				'nome' => 'Rondolândia',
				'uf' => 'MT',
				'cep2' => '5107578',
				'estado_cod' => 13,
				'cep' => '78338-000',
			),
			49 => 
			array (
				'id' => 10094,
				'nome' => 'Santa Rita do Trivelato',
				'uf' => 'MT',
				'cep2' => '5107768',
				'estado_cod' => 13,
				'cep' => '78453-000',
			),
			50 => 
			array (
				'id' => 10095,
				'nome' => 'Nova Santa Helena',
				'uf' => 'MT',
				'cep2' => '5106190',
				'estado_cod' => 13,
				'cep' => '78548-000',
			),
			51 => 
			array (
				'id' => 10096,
				'nome' => 'Santo Antônio do Leste',
				'uf' => 'MT',
				'cep2' => '5107792',
				'estado_cod' => 13,
				'cep' => '78628-000',
			),
			52 => 
			array (
				'id' => 10097,
				'nome' => 'Nova Nazaré',
				'uf' => 'MT',
				'cep2' => '5106174',
				'estado_cod' => 13,
				'cep' => '78638-000',
			),
			53 => 
			array (
				'id' => 10098,
				'nome' => 'Santa Cruz do Xingu',
				'uf' => 'MT',
				'cep2' => '5107743',
				'estado_cod' => 13,
				'cep' => '78664-000',
			),
			54 => 
			array (
				'id' => 10099,
				'nome' => 'Bom Jesus do Araguaia',
				'uf' => 'MT',
				'cep2' => '5101852',
				'estado_cod' => 13,
				'cep' => '78678-000',
			),
			55 => 
			array (
				'id' => 10100,
				'nome' => 'Pau D\'Arco do Piauí',
				'uf' => 'PI',
				'cep2' => '2207793',
				'estado_cod' => 17,
				'cep' => '64295-000',
			),
			56 => 
			array (
				'id' => 10101,
				'nome' => 'Westfalia',
				'uf' => 'RS',
				'cep2' => '4323770',
				'estado_cod' => 23,
				'cep' => '95893-000',
			),
			57 => 
			array (
				'id' => 10102,
				'nome' => 'Santa Margarida do Sul',
				'uf' => 'RS',
				'cep2' => '4316972',
				'estado_cod' => 23,
				'cep' => '97335-000',
			),
			58 => 
			array (
				'id' => 10103,
				'nome' => 'Tio Hugo',
				'uf' => 'RS',
				'cep2' => '4321469',
				'estado_cod' => 23,
				'cep' => '99345-000',
			),
			59 => 
			array (
				'id' => 10144,
				'nome' => 'Rialma',
				'uf' => 'GO',
				'cep2' => '5218607',
				'estado_cod' => 9,
				'cep' => '76310-000',
			),
			60 => 
			array (
				'id' => 11019,
				'nome' => 'Aroeiras do Itaim',
				'uf' => 'PI',
				'cep2' => '2200954',
				'estado_cod' => 17,
				'cep' => '64612-000',
			),
			61 => 
			array (
				'id' => 11020,
				'nome' => 'Ipiranga do Norte',
				'uf' => 'MT',
				'cep2' => '5104526',
				'estado_cod' => 13,
				'cep' => '78578-000',
			),
			62 => 
			array (
				'id' => 11021,
				'nome' => 'Itanhangá',
				'uf' => 'MT',
				'cep2' => '5104542',
				'estado_cod' => 13,
				'cep' => '78579-000',
			),
			63 => 
			array (
				'id' => 11323,
				'nome' => 'Serra do Navio',
				'uf' => 'AP',
				'cep2' => '1600055',
				'estado_cod' => 4,
				'cep' => '68948-000',
			),
			64 => 
			array (
				'id' => 11896,
				'nome' => 'Nazária',
				'uf' => 'PI',
				'cep2' => '',
				'estado_cod' => 17,
				'cep' => '64415-000',
			),
		));
	}

}
