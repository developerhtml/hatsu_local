<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClientes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes', function(Blueprint $table)
		{
			$table->increments('id_cliente');

			$table->string('tipo', 1);
			$table->string('cpf', 14);
			$table->string('cnpj', 19);
			$table->string('nome', 200);
			$table->string('endereco', 200);
			$table->integer('numero');
			$table->string('complemento', 20);
			$table->string('bairro', 30);
			$table->string('cidade', 50);
			$table->string('uf', 2);
			$table->string('cep', 20);
			$table->string('telefone', 14);
			$table->string('celular', 14);
			$table->string('email', 50);

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clientes');
	}

}
