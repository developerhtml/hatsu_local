<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
				
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('tipo', 1);
			$table->string('nome', 255);
			$table->string('email', 255);
			$table->string('telefone', 40);
			$table->string('celular', 40);
			$table->string('username', 255);
			$table->string('password', 200);
			$table->string('remember_token', 100);
			 
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
