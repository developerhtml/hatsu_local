@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">          
            
            <div class="widget ">
              
              <div class="widget-header">
                <i class="icon-group"></i>
                <h3>Editando Cliente {{ $cliente->nome }} </h3>
            </div> <!-- /widget-header -->
          
          <div class="widget-content">
            
            <div class="tabbable">

         <form id="edit-profile" class="form-horizontal" action="{{ URL::to('cliente/update/'.$cliente->id_cliente) }}" method="POST" name="form-clientes">

         <fieldset>

          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3>Dados Pessoais</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div id='calendar' style='padding-top:15px;'>            

                <div class="control-group">  
 
                    <div class="control-group">                     
                      <label class="control-label" for="cpf">CPF</label>
                      <div class="controls">
                        <input type="text" class="span4" id="cpf" @if($errors->has('cpf')>0) style='border-color:#ff0000;' @endif name="cpf" @if((Input::old('cpf'))) value="{{ Input::old('cpf') }}" @else value="{{ $cliente->cpf }}" @endif ">

                        @if($errors->has('cpf')>0) <p class='error'> {{ $errors->first('cpf') }} </p> @endif

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="name">Nome Completo</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('name')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" @if((Input::old('nome'))) value="{{ Input::old('nome') }}" @else value="{{ $cliente->nome }}" @endif >
                        @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
                    </div>

                    <div class="control-group">                     
                      <label class="control-label" for="email">Email</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" @if((Input::old('email'))) value="{{ Input::old('email') }}" @else value="{{ $cliente->email }}" @endif >
                        @if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="telefone">Telefone</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('telefone')>0) style='border-color:#ff0000;' @endif id="telefone" name="telefone" @if((Input::old('telefone'))) value="{{ Input::old('telefone') }}" @else value="{{ $cliente->telefone }}" @endif >
                        @if($errors->has('telefone')>0) <p class='error'> {{ $errors->first('telefone') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="celular">Celular</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('celular')>0) style='border-color:#ff0000;' @endif id="celular" name="celular" @if((Input::old('celular'))) value="{{ Input::old('celular') }}" @else value="{{ $cliente->celular }}" @endif >
                        @if($errors->has('celular')>0) <p class='error'> {{ $errors->first('celular') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="sexo">Sexo</label>
                      <div class="controls">

                        <select name="sexo" class="span4" @if($errors->has('sexo')>0) style='border-color:#ff0000;' @endif>
                        <option value="">- Selecione -</option>
                        <option @if(Input::has('sexo') && Input::old('sexo') == "Masculino") {{ 'selected="selected"' }} @elseif ($cliente->sexo == "Masculino") {{ 'selected="selected"' }} @endif value="Masculino">Masculino</option>
                        <option @if(Input::has('sexo') && Input::old('sexo') == "Feminino") {{ 'selected="selected"' }} @elseif ($cliente->sexo == "Feminino") {{ 'selected="selected"' }} @endif value="Feminino">Feminino</option>
                        </select>
                        @if($errors->has('sexo')>0) <p class='error'> {{ $errors->first('sexo') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->                      


              </div>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->

            
        <!-- /widget -->
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-home"></i>
              <h3>Endereço</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div id='calendar' style='padding-top:15px;'>   

                    <div class="control-group">                     
                      <label class="control-label" for="cep">CEP</label>
                      <div class="controls">
                        <input type="text" onblur="return ajaxEndereco(this);" class="span4" @if($errors->has('cep')>0) style='border-color:#ff0000;' @endif id="cep" name="cep" @if((Input::old('cep'))) value="{{ Input::old('cep') }}" @else value="{{ $cliente->cep }}" @endif >
                        @if($errors->has('cep')>0) <p class='error'> {{ $errors->first('cep') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->  

                    <div class="control-group">                     
                      <label class="control-label" for="estado">Estado</label>
                      <div class="controls">
                        <select class="span4" id="estado" name="estado" @if($errors->has('estado')>0) style='border-color:#ff0000;' @endif>
                          <option value="">- Selecione -</option>
                          @foreach(Estado::all() as $estado)
                              <option @if((Input::old('estado')) && Input::old('estado') == $estado->id) {{ 'selected="selected"' }} @elseif($cliente->uf == $estado->id) {{ 'selected="selected"' }} @endif value="{{ $estado->id }}">{{ $estado->nome }}</option>
                          @endforeach
                        </select>
                        @if($errors->has('estado')>0) <p class='error'> {{ $errors->first('estado') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="cidade">Cidade</label>
                      <div class="controls">
                        <select class="span4" id="cidade" name="cidade" @if($errors->has('cidade')>0) style='border-color:#ff0000;' @endif>
                          @if (Input::has('cidade')) 
                            @foreach(Cidade::where('uf','=',Cidade::find(Input::get('cidade'))->uf)->get() as $cidade)
                              <option value='{{ $cidade->id }}' @if($cidade->id == Input::get('cidade')) selected='selected' @endif >{{ $cidade->nome }}</option>
                            @endforeach
                          @elseif (!empty($cliente->cidade))
                            @foreach(Cidade::where('uf','=',Cidade::find($cliente->cidade)->uf)->get() as $cidade)
                              <option value='{{ $cidade->id }}' @if($cidade->id == $cliente->cidade) selected='selected' @endif >{{ $cidade->nome }}</option>
                            @endforeach
                          @else
                            <option value="">- Selecione -</option>
                          @endif
                        </select>
                        @if($errors->has('cidade')>0) <p class='error'> {{ $errors->first('cidade') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->  

                    <div class="control-group">                     
                      <label class="control-label" for="bairro">Bairro</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('bairro')>0) style='border-color:#ff0000;' @endif id="bairro" name="bairro" @if((Input::old('bairro'))) value="{{ Input::old('bairro') }}" @else value="{{ $cliente->bairro }}" @endif >
                        @if($errors->has('bairro')>0) <p class='error'> {{ $errors->first('bairro') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->       

                    <div class="control-group">                     
                      <label class="control-label" for="endereco">Endereço</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('endereco')>0) style='border-color:#ff0000;' @endif id="endereco" name="endereco" @if((Input::old('endereco'))) value="{{ Input::old('endereco') }}" @else value="{{ $cliente->endereco }}" @endif >
                        @if($errors->has('endereco')>0) <p class='error'> {{ $errors->first('endereco') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="numero">Número</label>
                      <div class="controls">
                        <input type="text" onkeyup="checkInput(this)" maxlength="6" class="span4" @if($errors->has('numero')>0) style='border-color:#ff0000;' @endif id="numero" name="numero" @if((Input::old('numero'))) value="{{ Input::old('numero') }}" @else value="{{ $cliente->numero }}" @endif >
                        @if($errors->has('numero')>0) <p class='error'> {{ $errors->first('numero') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="complemento">Complemento</label>
                      <div class="controls">
                        <input type="text" class="span4" id="complemento" name="complemento" @if((Input::old('complemento'))) value="{{ Input::old('complemento') }}" @else value="{{ $cliente->complemento }}" @endif >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

              </div>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->


                    <div class="form-actions">
                      
                      <button type="submit" class="btn btn-primary">Editar</button> 
                      
                      <a href="{{ URL::to('cliente') }}"><button class="btn" type="button">Retornar</button></a>

                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>
              
            </div>
            
          </div> <!-- /widget-content -->
            
        </div> <!-- /widget -->
            
        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
@stop