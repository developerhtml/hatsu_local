@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">       

         <form id="edit-profile" class="form-horizontal hidden-print" action="{{ URL::to('cliente/search/') }}" method="POST" name="form-busca-clientes">
          <fieldset>

                <div class="control-group" style="text-align:right;">                     
                  <div class="controls">
                     <div class="input-append">
                        <input class="span2 m-wrap" id="appendedInputButton"  @if (Session::has('erro_busca')) placeholder="{{ Session::get('erro_busca') }}" @else placeholder="Procure pelo nome do cliente" @endif type="text" name="busca" style="width:300px;">
                        
                      </div>

                       <select name="orderby" size="1">

                       <option value="">- Ordenar por -</option>
                       <option value="nome" @if(isset($_POST['orderby']) &&  $_POST['orderby'] == "nome") {{ 'selected="selected"' }} @endif >Nome do cliente</option>
                       <option value="cidade" @if(isset($_POST['orderby']) && $_POST['orderby'] == "cidade") {{ 'selected="selected"' }} @endif>Cidade</option>

                     </select>

                     <select name="paginate" size="1">

                       <option value="">- Resultados por página -</option>
                       <option value="10" @if(isset($_POST['paginate']) &&  $_POST['paginate'] == "10") {{ 'selected="selected"' }} @endif>10</option>
                       <option value="20" @if(isset($_POST['paginate']) &&  $_POST['paginate'] == "20") {{ 'selected="selected"' }} @endif>20</option>
                       <option value="30" @if(isset($_POST['paginate']) &&  $_POST['paginate'] == "30") {{ 'selected="selected"' }} @endif>30</option>
                       <option value="50" @if(isset($_POST['paginate']) &&  $_POST['paginate'] == "50") {{ 'selected="selected"' }} @endif>50</option>
                       <option value="100" @if(isset($_POST['paginate']) &&  ($_POST['paginate'] == "100" || $_POST['paginate'] == "")) {{ 'selected="selected"' }} @endif>100 (padrão)</option>

                     </select>

                    <button class="btn btn-success hidden-print" type="submit">Filtrar</button>

                    </div>  <!-- /controls -->      
                 </div> <!-- /control-group -->

                 

         </fieldset>
        </form>
            
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header hidden-print"> <i class="icon-group"></i>
              <h3 class="hidden-print">Gerenciamento de Clientes</h3>
            </div>
            <!-- /widget-header -->

            <!-- exibindo message da session -->
            @if (Session::has('message'))
              <div class="alert alert-info" style="margin-top:10px;background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;">{{ Session::get('message') }}</div>
            @endif

            @if (count($clientes)<=0)

              <div class="widget-content">
            
              <div class="alert alert-info" style="margin:10px;background:#fff59b;color:#000;border:1px solid #ccc;text-align:center;font-size:14px;">Sua busca não retornou nenhum resultado</div>

              </div>
              <!-- /widget-content --> 
            </div>
            <!-- /widget --> 

            @else 

            {{ $clientes->links(); }}

            <div class="widget-content">

              <table class="table table-striped table-bordered">
                <thead>
                  <tr>

                    <th> 

                    @if ($sortby == 'nome' && $order == 'asc') {{
                        link_to_action('ClienteController@getIndex','Nome',
                            array(
                                'sortby' => 'nome',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('ClienteController@getIndex','Nome',
                            array(
                                'sortby' => 'nome',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif

                    </th>
                    <th> Contato  </th>

                    <th>                     
                    @if ($sortby == 'bairro' && $order == 'asc') {{
                        link_to_action('ClienteController@getIndex','Bairro',
                            array(
                                'sortby' => 'bairro',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('ClienteController@getIndex','Bairro',
                            array(
                                'sortby' => 'bairro',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif  
                    </th>

                    <th> 
                    @if ($sortby == 'cidade' && $order == 'asc') {{
                        link_to_action('ClienteController@getIndex','Cidade/UF',
                            array(
                                'sortby' => 'cidade',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('ClienteController@getIndex','Cidade/UF',
                            array(
                                'sortby' => 'cidade',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif
                     </th>
                    <th class="td-actions hidden-print"> Ações </th>
                  </tr>
                </thead>

               <tbody>
                  @foreach ($clientes as $cliente)
              <tr>
                
                <td>{{ $cliente->nome }} </td>

                <td>{{ $cliente->telefone }}</td>

                <td>{{ $cliente->bairro }}</td>

                <td>

                {{ Cidade::find($cliente->cidade)->nome ." / ". Cidade::find($cliente->cidade)->uf  }}

                </td>
                
                <td class="td-actions hidden-print">

                <a href="{{ URL::to('cliente/edit/' . $cliente->id_cliente) }}" class="btn btn-small btn-warning">

                <i class="btn-icon-only icon-pencil"></i></a>

                <a href="{{ URL::to('cliente/destroy/' . $cliente->id_cliente) }}" class="btn btn-danger btn-small">

                <i class="btn-icon-only icon-remove"> </i></a></td>

            </tr>
            
        @endforeach

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 

        @endif

        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
@stop