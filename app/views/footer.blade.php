<!-- NEWS LETTER AND SOCIAL START -->
    <section class="news-container">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-5">
                	<div class="news-letter-box">
                    	<form class="form-inline" role="form" action="{{ URL::to('newsletter/store') }}" method="post">
                        	<label>Receba nossa newsletter</label>
                            <div class="news-email-box">
                                <input type="email" name="email" placeholder="Insira seu e-mail" required>
                                <button type="submit" >enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-7">
                	<div class="social-icon-box">
                    	<ul>
                        	<li><a href="http://facebook.com/hatsubrasil"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="http://twitter.com/hatsubr"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="http://instagram.com/hatsubr"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="http://plus.google.com/+HatsuBrasil/"><i class="fa fa-google-plus"></i></a></li>
                            <li>Conheça também: <span><a href="http://blog.hatsu.com.br/" target=”_blank”><i class="blog_icon"></i></a></span></li>
                            <li><a href="http://wiki.hatsu.com.br/" target=”_blank”><i class="wikipedia_icon"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>	
<!-- NEW LETTER AND SOCIAL END -->

<!-----------FOOTER START--------------------->
<section class="footer-box">
       <div class="container">
            <div class="row">
                    <div class="col-sm-9">
                        <div class="col-sm-12 pdl0">
                            <div class="col-xs-5 mobile-wd100">
                                <div class="small-list">
                                    <h5>LINHAS</h5>	
                                    <ul class="tow-row-list">
                                        <li><a href="{{ URL::to('projection')}}" title="Conheça a Linha Projection">Projection</a></li>
                                        <li><a href="{{ URL::to('ultra')}}" title="Conheça a Linha Ultra">Ultra</a></li>
                                        <li><a href="#">Titanium</a></li>
                                        <li><a href="{{ URL::to('raid')}}" title="Conheça a Linha Raid">Raid</a></li>    
                                        <li><a href="{{ URL::to('kizo')}}" title="Conheça a Linha Kizo">Kizo</a></li>
                                    </ul>
                                <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-xs-7 mobile-wd100">
                                <div class="small-list links-uteis">
                                    <h5>LINKS ÚTEIS</h5>	
                                <ul>
                                <li><a href="{{ URL::to('lentes')}}" title="Informações sobre Lentes">Lentes</a></li>
                                <li><a href="{{ URL::to('experimente')}}" title="Conheça o serviço de Experimente Hatsu">Experimente Hatsu</a></li>
                                <li><a href="{{ URL::to('guiadecompra')}}">Guia de Compras</a></li>
                                <li><a href="{{ URL::to('duvidas')}}" title="Tire suas dúvidas sobre a Hatsu">Dúvidas?</a></li> 
                                <li><a href="{{ URL::to('garantia')}}">Garantia</a></li>   
                                <li><a href="{{ URL::to('contato')}}" title="Entre em contato conosco">Contato</a></li>
                                <!--<li><a href="#">Lab Hatsu</a></li> -->
                                <li><a href="{{ URL::to('viracopos')}}" tittle ="Pontos de Venda">Pontos de Venda</a></li>
                                <li><a href="http://blog.hatsu.com.br" target=”_blank”>Blog</a></li>
                                <li><a href="http://wiki.hatsu.com.br" target=”_blank”>Wiki | Hatsu</a></li>
                                </ul>
                                <div class="clearfix"></div>
                             </div>
                            </div>
               <div class="clearfix"></div>
                            <hr class="footer-line">
                            <div class="col-xs-5 mobile-wd100">
                                <div class="formus">
                                    <h5>SITE PROTEGIDO</h5>
                            <p>
								<img src="{{URL::asset('img/site_blindado_icon.png') }}">
							<span><img src="{{URL::asset('img/ssl_icon.png') }}"></span>
							</p>
                                </div>
                            </div>
                            <div class="col-xs-7 mobile-wd100">
                                <div class="formus">
                                    <h5>FORMAS DE PAGAMENTO</h5>
                                <img src="{{URL::asset('img/paypal_payment_icon.png') }}">                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			<!-----------LEFT FOOTER END--------------------->	
                <div class="col-sm-3 news">
                	<div class="col-xs-6 col-sm-12 mobile-wd100">
                        <h5>ATENDIMENTO </h5>
                        <p class="news-text">
                        <i class="fa fa-home"></i> Rua Capitão Francisco de Paula, 61<br>
                        Cambuí - Campinas/SP<br>
                        <i class="fa fa-envelope-o"></i> contato@hatsu.com.br<br>
                        <i class="fa fa-phone"></i> (19) 3252-3760<br>
                        <i class="whatsup-icon"></i> (19) 98331-1070</p>
                        <p class="news-text atend-para"><strong>Horário de atendimento:</strong><br>
                        Segunda a sexta-feira das 9:00 às 18:00</p>
                    </div>
                    <div class="col-xs-6 col-sm-12 mobile-wd100">
                        <h5>ASSESSORIA DE IMPRENSA</h5>
                        <p class="news-text">
                            Pace Comunicação<br>
                            <i class="fa fa-phone"></i> (11) 3704-7372<br>                                <i class="fa fa-envelope-o"></i> rp@hatsu.com.br
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                	<p class="copy-right">Hatsu - Óculos de Alta Tecnologia</p>
                </div>
			</div>
        </div>
    </section>
    <!-----------FOOTER END--------------------->
	