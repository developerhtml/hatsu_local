@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Pedidos | Minha Conta | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<!-- My Account -->

  <div class="container">
    <div class="row">

      <div class="col-md-3 col-sm-3 ">

        <!-- Sidebar navigation -->
          <nav>
            <ul id="navi" style="text-transform:uppercase;">
              <li><a href="{{ URL::to('minhaconta') }}" title="Página Inicial">Minha Conta</a></li>
              <li><a href="{{ URL::to('meusdados') }}" title="Altere seus dados cadastrais">Alterar Dados</a></li>
              <li><a href="{{ URL::to('pedidos') }}" title="Visualize seus pedidos">Meus Pedidos</a></li>
              <li><a href="{{ URL::to('receitas') }}" title="Adicione suas prescrições médicas">Prescrições Médicas</a></li>
              <li><a href="{{ URL::to('logout') }}" title="Sair de sua conta">Sair</a></li>
            </ul>
          </nav>

      </div>

<!-- Main content -->
      <div class="col-md-9 col-sm-9">
        <!-- title -->
			@foreach($dados as $item)

			<h5 class="title">DETALHE DO PEDIDO {{ $item->cod_pedido }}</h5>

            <table class="table table-striped tcart">
              <thead>
                <tr>
                  <th>DATA</th>
                  <th>CÓDIGO</th>
                  <th>VALOR</th>
                  <th>STATUS</th>
                </tr>
              </thead>
              <tbody>

              <tr>
              <td>{{ Helper::formataData($item->data_pedido,1) }}</td>
              <td>{{ $item->cod_pedido }}</td>
              <td>R$ {{ number_format($item->valor_total,2,",",".") }}</td>
              <td>{{ $item->status }}</td>

              </tr>
              </tbody>

              </table>

              <br /><br />

              {{ Helper::VisualizarPedido($item->id,2)  }}

              <tr><td>TOTAL</td><td><b>R$  {{ number_format($item->valor_total,2,",",".") }} </b></td></tr>

              </table>

              <br />

              @endforeach

      </div>                                                                    

    </div>
  </div>

  </div>
</div>



@stop