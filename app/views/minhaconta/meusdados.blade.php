@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Meus Dados | Minha Conta | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<!-- My Account -->

  <div class="container">
    <div class="row">

      <div class="col-md-3 col-sm-3">

        <!-- Sidebar navigation -->
          <nav>
            <ul id="navi" style="text-transform:uppercase;">
              <li><a href="{{ URL::to('minhaconta') }}" title="Página Inicial">Minha Conta</a></li>
              <li><a href="{{ URL::to('meusdados') }}" title="Altere seus dados cadastrais">Alterar Dados</a></li>
              <li><a href="{{ URL::to('pedidos') }}" title="Visualize seus pedidos">Meus Pedidos</a></li>
              <li><a href="{{ URL::to('receitas') }}" title="Adicione suas prescrições médicas">Prescrições Médicas</a></li>
              <li><a href="{{ URL::to('logout') }}" title="Sair de sua conta">Sair</a></li>
            </ul>
          </nav>

      </div>

<!-- Main content -->
      <div class="col-md-9 col-sm-9">

          <h5 class="title">DADOS PESSOAIS</h5>

      <?php $dados_cliente = Cliente::find(Auth::id()); ?>

      Olá {{ $dados_cliente->nome }}! <br />

    @if (Session::has('message'))

        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2><br />
        
      @endif
      
           <div class="form form-small">

          <form class="form-horizontal" method="post" action="{{ URL::to('cliente/update') }}">
              <!-- Name -->
              <div class="form-group">
              @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif
                <label class="control-label col-md-3" for="name1">Nome completo</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="name1" @if($errors->has('nome')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" @if(Input::old('nome')) value="{{ Input::old('nome') }}" @elseif(Auth::check() && !empty($dados_cliente->nome)) value="{{ $dados_cliente->nome }}" @endif placeholder="Informe seu nome completo">
                </div>
              </div>   

            <div class="form-group">
              @if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
                <label class="control-label col-md-3" for="email">E-mail</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="email" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" @if(Input::old('email')) value="{{ Input::old('email') }}" @elseif(Auth::check() && !empty($dados_cliente->email)) value="{{ $dados_cliente->email }}" @endif placeholder="Informe seu e-mail">
                </div>
              </div>   

            <div class="form-group">
              @if($errors->has('telefone')>0) <p class='error'> {{ $errors->first('telefone') }} </p> @endif
                <label class="control-label col-md-3" for="telefone">Telefone</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="telefone" @if($errors->has('telefone')>0) style='border-color:#ff0000;' @endif id="telefone" name="telefone" @if(Input::old('telefone')) value="{{ Input::old('telefone') }}" @elseif(Auth::check() && !empty($dados_cliente->telefone)) value="{{ $dados_cliente->telefone }}" @endif placeholder="Informe seu telefone">
                </div>
              </div>     
              
              <div class="form-group">
                <label class="control-label col-md-3" for="celular">Celular</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="celular" @if($errors->has('celular')>0) style='border-color:#ff0000;' @endif id="celular" name="celular" @if(Input::old('celular')) value="{{ Input::old('celular') }}" @elseif(Auth::check() && !empty($dados_cliente->celular)) value="{{ $dados_cliente->celular }}" @endif placeholder="Informe seu celular">
                </div>
              </div>   

              <div class="form-group">
              @if($errors->has('cpf')>0) <p class='error'> {{ $errors->first('cpf') }} </p> @endif
                <label class="control-label col-md-3" for="cpf">CPF</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="cpf" @if($errors->has('cpf')>0) style='border-color:#ff0000;' @endif id="cpf" name="cpf" @if(Input::old('cpf')) value="{{ Input::old('cpf') }}" @elseif(Auth::check() && !empty($dados_cliente->cpf)) value="{{ $dados_cliente->cpf }}" @endif placeholder="Informe seu CPF">
                </div>
              </div>   

              <div class="form-group">
              @if($errors->has('sexo')>0) <p class='error'> {{ $errors->first('sexo') }} </p> @endif
                <label class="control-label col-md-3" for="sexo">Sexo</label>
                <div class="col-md-6">
                  <select class="form-control" style="min-width:155px;" @if($errors->has('sexo')>0) style='border-color:#ff0000;' @endif id="sexo" name="sexo">
                    <option value="">- Selecione -</option>
                    <option value="Masculino" @if(Input::old('sexo') && Input::old('sexo') == "Masculino") selected="selected" @elseif(Auth::check() && !empty($dados_cliente->sexo) && $dados_cliente->sexo == 'Masculino') selected='selected' @endif >Masculino</option>
                    <option value="Feminino" @if(Input::old('sexo') && Input::old('sexo') == "Feminino")  selected="selected" @elseif(Auth::check() && !empty($dados_cliente->sexo) && $dados_cliente->sexo == 'Feminino') selected='selected'  @endif >Feminino</option>
                  </select>
                </div>
              </div>   

      
      <h5 class="title">DADOS DE ENTREGA</h5>

      
              <div class="form-group">
              @if($errors->has('cep')>0) <p class='error'> {{ $errors->first('cep') }} </p> @endif
                <label class="control-label col-md-3" for="cep">CEP</label>
                <div class="col-md-6">
                  <input type="text" onblur="return ajaxEndereco(this);" class="form-control" @if($errors->has('cep')>0) style='border-color:#ff0000;' @endif id="cep" name="cep" @if(Input::old('cep')) value="{{ Input::old('cep') }}" @elseif(Auth::check() && !empty($dados_cliente->cep)) value="{{ $dados_cliente->cep }}" @endif>
                </div>
              </div>  

            <div class="form-group">
              @if($errors->has('estado')>0) <p class='error'> {{ $errors->first('estado') }} </p> @endif
              <label class="control-label col-md-3" for="estado">Estado</label>
                <div class="col-md-6">

                <select class="form-control" id="estado" name="estado" @if($errors->has('estado')>0) style='border-color:#ff0000;' @endif>
                  <option value="">- Selecione -</option>
                  @foreach(Estado::all() as $estado)
                  <option @if(Input::old('estado') && Input::old('estado') == $estado->id) {{ 'selected="selected"' }} @elseif(Auth::check() && $dados_cliente->uf == $estado->id) {{ 'selected="selected"' }} @endif value="{{ $estado->id }}">{{ $estado->nome }}</option>
                  @endforeach
                  </select>

               </div>
            </div>

            <div class="form-group">
              @if($errors->has('cidade')>0) <p class='error'> {{ $errors->first('cidade') }} </p> @endif
              <label class="control-label col-md-3" for="cidade">Cidade</label>
                <div class="col-md-6">
                <select class="form-control" id="cidade" name="cidade" @if($errors->has('cidade')>0) style='border-color:#ff0000;' @endif>
                @if (Input::old('cidade')) 
                  @foreach(Cidade::where('uf','=',Cidade::find(Input::old('cidade'))->uf)->get() as $cidade)
                    <option value='{{ $cidade->id }}' @if($cidade->id == Input::old('cidade')) selected='selected' @endif >{{ $cidade->nome }}</option>
                  @endforeach
                @elseif (!empty($dados_cliente->cidade))
                  @foreach(Cidade::where('uf','=',Cidade::find($dados_cliente->cidade)->uf)->get() as $cidade)
                    <option value='{{ $cidade->id }}' @if($cidade->id == $dados_cliente->cidade) selected='selected' @endif >{{ $cidade->nome }}</option>
                  @endforeach
                @else
                  <option value="">- Selecione -</option>
                @endif
                </select>
               </div>
            </div>            
      
            <div class="form-group">
              @if($errors->has('bairro')>0) <p class='error'> {{ $errors->first('bairro') }} </p> @endif
                <label class="control-label col-md-3" for="bairro">Bairro</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="bairro" @if($errors->has('bairro')>0) style='border-color:#ff0000;' @endif id="bairro" name="bairro" @if(Input::old('bairro')) value="{{ Input::old('bairro') }}" @elseif(Auth::check() && !empty($dados_cliente->bairro)) value="{{ $dados_cliente->bairro }}" @endif placeholder="Informe seu bairro">
                </div>
              </div> 

            <div class="form-group">
              @if($errors->has('endereco')>0) <p class='error'> {{ $errors->first('endereco') }} </p> @endif
                <label class="control-label col-md-3" for="endereco">Endereço</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="endereco" @if($errors->has('endereco')>0) style='border-color:#ff0000;' @endif id="endereco" name="endereco" @if(Input::old('endereco')) value="{{ Input::old('endereco') }}" @elseif(Auth::check() && !empty($dados_cliente->endereco)) value="{{ $dados_cliente->endereco }}" @endif placeholder="Informe seu endereço">
                </div>
              </div> 

            <div class="form-group">
              @if($errors->has('numero')>0) <p class='error'> {{ $errors->first('numero') }} </p> @endif
                <label class="control-label col-md-3" for="numero">Número</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="numero" @if($errors->has('numero')>0) style='border-color:#ff0000;' @endif id="numero" name="numero" @if(Input::old('numero')) value="{{ Input::old('numero') }}" @elseif(Auth::check() && !empty($dados_cliente->numero)) value="{{ $dados_cliente->numero }}" @endif placeholder="Informe o número">
                </div>
              </div> 

              <div class="form-group">
                <label class="control-label col-md-3" for="complemento">Complemento</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="complemento" @if($errors->has('complemento')>0) style='border-color:#ff0000;' @endif id="complemento" name="complemento" @if(Input::old('complemento')) value="{{ Input::old('complemento') }}" @elseif(Auth::check() && !empty($dados_cliente->complemento)) value="{{ $dados_cliente->complemento }}" @endif>
                </div>
              </div> 
      
            <div class="form-group">
             <!-- Buttons -->
              <div class="col-md-6 col-md-offset-2">
                <input type="submit" class="botao-comprar" style="width:280px;" value="ATUALIZAR DADOS">
              </div>

            </div>

            </form>
            
            </div>

            </div>


      </div>                                      

  </div>
</div>


@stop