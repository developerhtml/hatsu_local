@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Pagamento | Minha Conta | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<!-- My Account -->

  <div class="container">
    <div class="row">

      <div class="col-md-3 col-sm-3 ">

        <!-- Sidebar navigation -->
          <nav>
            <ul id="navi" style="text-transform:uppercase;">
              <li><a href="{{ URL::to('minhaconta') }}" title="Página Inicial">Minha Conta</a></li>
              <li><a href="{{ URL::to('meusdados') }}" title="Altere seus dados cadastrais">Alterar Dados</a></li>
              <li><a href="{{ URL::to('pedidos') }}" title="Visualize seus pedidos">Meus Pedidos</a></li>
              <li><a href="{{ URL::to('receitas') }}" title="Adicione suas prescrições médicas">Prescrições Médicas</a></li>
              <li><a href="{{ URL::to('logout') }}" title="Sair de sua conta">Sair</a></li>
            </ul>
          </nav>

      </div>

<!-- Main content -->
      <div class="col-md-9 col-sm-9">
        <!-- title -->

         @if (Session::has('boleto'))
        
          <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;"><a href="{{ Session::get('boleto') }}" style="color:#fff;" target="_blank">CLIQUE AQUI PARA IMPRIMIR SEU BOLETO</a></h2><br />

          @endif


			@foreach($dados as $item)

			<h5 class="title">DETALHE DO PEDIDO {{ $item->cod_pedido }}</h5>

            <table class="table table-striped tcart">
              <thead>
                <tr>
                  <th>DATA</th>
                  <th>CÓDIGO</th>
                  <th>VALOR</th>
                  <th>STATUS</th>
                </tr>
              </thead>
              <tbody>

              <tr>
              <td>{{ Helper::formataData($item->data_pedido,1) }}</td>
              <td>{{ $item->cod_pedido }}</td>
              <td>R$ {{ number_format($item->valor_total,2,",",".") }}</td>
              <td>{{ $item->status }}</td>

              </tr>
              </tbody>

              </table>

              <br /><br />

              {{ Helper::VisualizarPedido($item->id,1)  }}

              @endforeach


              <h4>EFETUAR NOVO PAGAMENTO</h4>

    <div class="form form-small">

    <form class="form-horizontal" action="{{ URL::to('pagamento/store') }}" method="post" id="payment_form">

      <input type="hidden" name="id_pedido" value="{{ $item->id }}">

      @foreach($pagamento as $pag)

      <input type="hidden" name="id_pagamento" value="{{ $pag->id }}">

      @endforeach

      @if($errors->has('forma_pagamento')>0) <p class='error'> {{ $errors->first('forma_pagamento') }} </p> @endif


      <input type="radio" id="forma_pagamento_boleto" name="forma_pagamento" value="boleto" style="width:15px;height:15px;">
      <label class="label-checkout" id="forma_pagamento_boleto" for="forma_pagamento_boleto" style="font-size:16px;font-weight:normal;">Boleto</label>
      
      &nbsp;&nbsp;

      <input type="radio" id="forma_pagamento_cartao" name="forma_pagamento" value="cartao" style="width:15px;height:15px;">
      <label class="label-checkout" id="forma_pagamento_cartao" for="forma_pagamento_cartao" style="font-size:16px;font-weight:normal;">Cartão de crédito</label>

      <input type="hidden" value="{{ $item->valor_total }}" name="amount" id="amount">

      <br />

      

      @if (Session::has('message'))

      <p class="error">Sua transação foi recusada. Tente novamente ou utilize outro cartão!</p>

      @endif

        <div id="checkout-cartao" style="display:none;margin-top:10px;">

          @if($errors->has('bandeira')>0) <p class='error'> {{ $errors->first('bandeira') }} </p> @endif

          <input type="radio" name="bandeira" id="bandeira_visa" value="visa" @if(Input::old('bandeira') == "visa") checked="checked" @endif ><label for="bandeira_visa"><img src="{{ URL::asset('img/payment/n_visa.png') }}" /></label>&nbsp;&nbsp;
          <input type="radio" name="bandeira" id="bandeira_master" value="mastercard" @if(Input::old('bandeira') == "mastercard") checked='checked' @endif><label for="bandeira_master"><img src="{{ URL::asset('img/payment/n_master.png') }}" alt="Bandeira Mastercard" /></label>&nbsp;&nbsp;
          <input type="radio" name="bandeira" id="bandeira_elo" value="elo" @if(Input::old('bandeira') == "mastercard") checked='checked' @endif><label for="bandeira_elo"><img src="{{ URL::asset('img/payment/n_elo.png') }}" alt="Bandeira Elo" /></label>&nbsp;&nbsp;
          <input type="radio" name="bandeira" id="bandeira_diners" value="diners" @if(Input::old('bandeira') == "mastercard") checked='checked' @endif><label for="bandeira_diners"><img src="{{ URL::asset('img/payment/n_diners.png') }}" alt="Bandeira Diners Club" /></label><br />

            <div class="form-group">
            @if($errors->has('instalments')>0) <p class='error'> {{ $errors->first('instalments') }} </p> @endif
              <label class="control-label col-md-3" for="instalments">Parcelas:</label>
              <div class="col-md-6">
              <select id="instalments" name="instalments" class="form-control">
              @foreach (range(1,10) as $value)
                <option value='{{ $value }}'>{{ $value .' x R$ '. number_format($item->valor_total/$value,2,",",".")  }}</option>
              @endforeach
              </select>
              </div>
            </div>   

            <div class="form-group">
            @if($errors->has('instalments')>0) <p class='error'> {{ $errors->first('instalments') }} </p> @endif
              <label class="control-label col-md-3" for="card_number">Número do Cartão:</label>
              <div class="col-md-6">
              <input type="text" id="card_number" name="card_number" class="form-control" />
              </div>
            </div>   

            <div class="form-group">
            @if($errors->has('card_holder_name')>0) <p class='error'> {{ $errors->first('card_holder_name') }} </p> @endif
              <label class="control-label col-md-3" for="card_holder_name">Nome <span style="font-size:9px">(como escrito no cartão)</span>:</label>
              <div class="col-md-6">
              <input type="text" id="card_holder_name" name="card_holder_name" class="form-control" />
              </div>
            </div>              

            <div class="form-group">
            @if($errors->has('card_expiration_month')>0) <p class='error'> {{ $errors->first('card_expiration_month') }} </p> @endif
              <label class="control-label col-md-3" for="card_expiration_month">Mês de Expiração:</label>
              <div class="col-md-6">
              <input type="text" id="card_expiration_month" name="card_expiration_month" class="form-control" />
              </div>
            </div>   

            <div class="form-group">
            @if($errors->has('card_expiration_year')>0) <p class='error'> {{ $errors->first('card_expiration_year') }} </p> @endif
              <label class="control-label col-md-3" for="card_expiration_year">Ano de Expiração:</label>
              <div class="col-md-6">
              <input type="text" id="card_expiration_year" name="card_expiration_year" class="form-control" />
              </div>
            </div>   

            <div class="form-group">
            @if($errors->has('card_cvv')>0) <p class='error'> {{ $errors->first('card_cvv') }} </p> @endif
              <label class="control-label col-md-3" for="card_cvv">Código de segurança:</label>
              <div class="col-md-6">
              <input type="text" id="card_cvv" name="card_cvv" class="form-control" />
              </div>
            </div>   


            </div>
            
              <input type="submit" class="button-comprar" style="width:280px;" value="EFETUAR PAGAMENTO">

              </form>

      </div>

      </div>                                                                    

    </div>
  </div>

  </div>
</div>



@stop