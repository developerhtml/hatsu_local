@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Prescrições Médicas | Minha Conta | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<!-- My Account -->

  <div class="container">
    <div class="row">

      <div class="col-md-3 col-sm-3 hidden-xs">

        <!-- Sidebar navigation -->
          <nav>
            <ul id="navi" style="text-transform:uppercase;">
              <li><a href="{{ URL::to('minhaconta') }}" title="Página Inicial">Minha Conta</a></li>
              <li><a href="{{ URL::to('meusdados') }}" title="Altere seus dados cadastrais">Alterar Dados</a></li>
              <li><a href="{{ URL::to('pedidos') }}" title="Visualize seus pedidos">Meus Pedidos</a></li>
              <li><a href="{{ URL::to('receitas') }}" title="Adicione suas prescrições médicas">Prescrições Médicas</a></li>
              <li><a href="{{ URL::to('logout') }}" title="Sair de sua conta">Sair</a></li>
            </ul>
          </nav>

      </div>

<!-- Main content -->
      <div class="col-md-9 col-sm-9">

          <h5 class="title">GERENCIANDO PRESCRIÇÕES MÉDICAS</h5>

      <?php $dados_cliente = Cliente::find(Auth::id()); ?>

      @if (Session::has('message'))

        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2><br />
        
      @endif

      @if($total>0)

        <table class="table table-striped tcart">
              <thead>
                <tr>
                  <th>Data</th>
                  <th>Nome</th>
                  <th>Pedido</th>
                  <th>Visualizar</th>
                </tr>
              </thead>
              <tbody>

              @foreach($dados as $receita)

                <tr>
                  <td>{{ Helper::formataData($receita->data,1) }}</td>
                  <td>{{ $receita->nome }}</td>
                  <td>{{ $receita->cod_pedido }}</td>
                  <td><a href="{{ URL::asset("$receita->file") }}" target="_blank">ABRIR ARQUIVO</a></td>
                </tr>

                @endforeach

              </tbody>
            </table>
      @endif


      <h5 class="title">ENVIAR PRESCRIÇÃO MÉDICA</h5>

           <div class="form form-small">

          <form class="form-horizontal" method="post" action="{{ URL::to('receitas/store') }}" enctype="multipart/form-data">

              <div class="form-group">
              @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif
                <label class="control-label col-md-3" for="name1">Identificação da Receita</label>
                <div class="col-md-6">
                  <input type="nome" class="form-control" id="name1" @if($errors->has('nome')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" @if(Input::old('nome')) value="{{ Input::old('nome') }}" @endif >
                </div>
              </div>   

              <div class="form-group">
              @if($errors->has('receita')>0) <p class='error'> {{ $errors->first('receita') }} </p> @endif
                <label class="control-label col-md-3" for="receita">Selecione o arquivo (JPG ou PDF)</label>
                <div class="col-md-6">
                  <input type="file" class="form-control" id="receita" @if($errors->has('receita')>0) style='border-color:#ff0000;' @endif  name="receita">
                </div>
              </div>       

              @if($total_pedidos>0)

              <div class="form-group">
                <label class="control-label col-md-3" for="id_pedido">Relacionar ao pedido: </label>
                <div class="col-md-6">
                <select name="id_pedido" class="form-control">
                  <option value="">- Selecione -</option>
              @foreach($pedidos as $item)
                  <option value="{{ $item->id }}">CÓD: {{ $item->cod_pedido }}</option>
              @endforeach
                </select>
                </div>
              </div>

              @endif
                                           


            <div class="form-group">
             <!-- Buttons -->
              <div class="col-md-6 col-md-offset-2">
                <button type="submit" class="button-comprar" style="width:280px;">ENVIAR</button>
              </div>

            </div>

            </form>
            
            </div>

      </div>                                      

      </div>                                      

  </div>
</div>


@stop