@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Criar Conta Hatsu | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">Criar conta hatsu</span>

</div>

</div>

<!-- Page content starts -->

<div class="content contact-two">

  <div class="container resume">

    <div class="row"> 
      
      <div class="col-md-12" style="text-align:right;">

      <h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;">CRIAR CONTA HATSU</h1>

      </div>

      <br style="clear:both;" />

      @if (Session::has('message'))
        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2>
      @endif

      <p style="text-align:center;">

      <div class="col-md-12">
     <!-- Contact form -->
        <h4 class="title" style="text-align:center;">PREENCHA OS DADOS ABAIXO</h4>

        <div class="form" style="text-align:center;">
          <!-- Contact form (not working)-->
          <form class="form-horizontal" action="{{ URL::to('cliente/store') }}" method="post">
              
              @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif

              <!-- Name -->
              <div class="form-group">
                <label class="control-label col-md-2" for="name1">Nome</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('nome')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" value="{{ Input::old('nome') }}" placeholder="Informe seu nome completo">
                </div>
              </div>

              @if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
              <!-- Email -->
              <div class="form-group">
                <label class="control-label col-md-2" for="email1">E-mail</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" value="{{ Input::old('email') }}" placeholder="Informe seu e-mail">
                </div>
              </div>

              @if($errors->has('telefone')>0) <p class='error'> {{ $errors->first('telefone') }} </p> @endif
              <!-- Email -->
              <div class="form-group">
                <label class="control-label col-md-2" for="telefone">Contato</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('telefone')>0) style='border-color:#ff0000;' @endif id="telefone" name="telefone" value="{{ Input::old('telefone') }}" placeholder="Telefone ou Celular">
                </div>
              </div>

              @if($errors->has('estado')>0) <p class='error'> {{ $errors->first('estado') }} </p> @endif

                <div class="form-group">
                <label class="control-label col-md-2">Estado</label>
                <div class="col-md-9">                               
                  <select class="form-control" id="estado" name="estado" @if($errors->has('estado')>0) style='border-color:#ff0000;' @endif>
                  <option value="">- Selecione -</option>
                    @foreach(Estado::all() as $estado)
                        <option @if(Input::old('estado') == $estado->id) {{ 'selected="selected"' }} @endif value="{{ $estado->id }}">{{ $estado->nome }}</option>
                    @endforeach
                  </select>
                </div>
                </div>  

                @if($errors->has('cidade')>0) <p class='error'> {{ $errors->first('cidade') }} </p> @endif    

                <div class="form-group">
                <label class="control-label col-md-2">Cidade</label>
                <div class="col-md-9">                               
                  <select class="form-control" id="cidade" name="cidade" @if($errors->has('cidade')>0) style='border-color:#ff0000;' @endif>
                    @if (Input::old('cidade')) 
                        @foreach(Cidade::where('uf','=',Cidade::find(Input::old('cidade'))->uf)->get() as $cidade)
                          <option value='{{ $cidade->id }}' @if($cidade->id == Input::old('cidade')) selected='selected' @endif >{{ $cidade->nome }}</option>
                        @endforeach
                      @else
                        <option value="">- Selecione -</option>
                      @endif
                    </select>
                  </div>
                </div>  

			@if($errors->has('password')>0) <p class='error'> {{ $errors->first('password') }} </p> @endif
              <div class="form-group">
                <label class="control-label col-md-2" for="password">Crie sua senha</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" @if($errors->has('password')>0) style='border-color:#ff0000;' @endif id="password" name="password">
                </div>
              </div>

			@if($errors->has('c_password')>0) <p class='error'> {{ $errors->first('c_password') }} </p> @endif
              <div class="form-group">
                <label class="control-label col-md-2" for="c_password">Confirme sua senha</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" @if($errors->has('c_password')>0) style='border-color:#ff0000;' @endif id="c_password" name="c_password">
                </div>
              </div>

              <!-- Buttons -->
              <div class="form-group">
                 <!-- Buttons -->
         <div class="col-md-9 col-md-offset-2">
          <button type="submit" class="btn btn-success" style="background-color:#77A302;text-transform:uppercase;width:200px;">CRIAR CONTA</button>
         </div>
              </div>
          </form>
        </div>

</div>

</p>


  </div>
</div>

</div>

</div>

<!-- Page content ends -->


@stop