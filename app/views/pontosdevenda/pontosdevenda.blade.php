@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Pontos de Vendas | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')
	
    <!----------- TOP PAGE NAV START -------------->
	<section class="top-page-nav">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
					<p><a href="{{ URL::to('/') }}" class="inativo" style="color: #77a302">INÍCIO</a>&nbsp;|&nbsp;<span class="ativo">Pontos de Vendas</span></p>
				</div>
            </div>
        </div>
    </section>
    <!----------- TOP PAGE NAV END --------------->
	
    <!-----------BANNER START-------------->
    <section class="banner-section">
    	<div class="pdv-banner">
        	<div class="container">
            	<div class="row">
                    <div class="col-sm-5">
                        <br>
                        <br>
                    	<div class="pdv-bnner-content">
                    		<h3>QUIOSQUE EM VIRACOPOS</h3>
                            <p>Para aqueles que querem conferir pessoalmente a qualidade dos produtos Hatsu, abrimos um quiosque no Aeroporto Internacional de Viracopos, que recebe 800 mil pessoas mensalmente.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-----------BANNER END-------------->
    <!-----------CONTENT AREA START-------------->
    <br>
    <div class="container">
    	<div class="row">
        	<div class="col-sm-12">
            	<div class="pdv">
                	<h4>CHEGAMOS EM VIRACOPOS</h4>
                    <p>Como primeiro Ponto de Venda escolhemos o Aeroporto Internacional de Viracopos pois grande parte do nosso público viaja constantemente, seja a trabalho ou a lazer, por isso, queríamos estar mais próximos de vocês.</p>
                    <p>Conseguiremos levar a experimentação de todos nossos produtos para que você possa sentir e confirir a qualidade do design, tecnologia e dos materiais utilizados em nossos óculos.</p>
                    <p>Consultores com informações sobre as armações e lentes irão ajudar você a escolher os modelos mais adequados para seu rosto, hábito e necessidade</p>
                    
                </div>
            </div>
        </div>
    </div>
    <!-----------CONTENT AREA END--------------->

    <!-----------VCP SECTION START-------------->

       <section class="vcp-section">
        <div class="container">
            <div class="col-sm-12" >
            <div class="row">
            <div class="map-box">
                <div class="col-sm-5" align= "right";>
                    <div class="map">
                        <br>
                        <img src="img/vcp.png" alt="" title="map"/>
                    </div>
                </div>
                <div class="col-sm-7" style="padding-left: 0px;">
                    <br>
                    <div class="quero map-para experiment-map-contant1" >
                        <p style="font-family:'roboto-regular'; font-size: 16px; text-align: left;magin-left:18px;">AEROPORTO DE VIRACOPOS (VCP) - CAMPINAS - SP</p>
                        <div class="requ-para caso experiment-map-contant1" >
                            
                            <p style="font-family:'roboto-light'; font-size: 14px; text-align: left;">Local: Estamos no Terminal de Passageiros - térreo, próximo ao balcão de informações</p>
                            <p style="font-family:'roboto-light'; font-size: 14px; text-align: left;">Hórário de funcionamento: Segunda a Sábado das 5:00 as 22:30</p>
                            <p style="font-family:'roboto-light'; font-size: 14px; text-align: left;">Contato: <a href="mailto:viracopos@hatsu.com.br" style="color: #77a302">viracopos@hatsu.com.br</a></p>
                            <p style="font-family:'roboto-light'; font-size: 14px; text-align: left;">Ficaremos felizes em recebê-lo.</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
    </div>  
    </section>
<!--
    <section class="vcp-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="vcp-content">
                        <h3>Aeroporto de Viracopos (VCP) - Campinas - SP</h3>
                        <ul>
                            hue hue BR BR
                        </ul>
                    </div>
                    <img src="{{ URL::asset('img/vcp.png') }}" alt="Viracopos" class="vcp-imagem" align="left" />
                </div>
                
            </div>
        </div>	
    </section>

-->
    <!-----------WORLD SECTION END-------------->
    <br>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="experimente-pdv">
                    <h4>EXPERIMENTE HATSU</h4>
                    <p>Caso queira conhecer nosso óculos, mas não temos nenhum ponto de vendas prómixo à você, requisite o serviço Experimente Hatsu. Experimente Hatsu é um serviço gratuito oferecido pela Hatsu, onde um consultor leva nossos óculos até você e te ajuda a escolher o modelo de óculos que mais combina com seu rosto e necessidades diárias</p>
                    
                </div>
            </div>
            <div class="quem"> <a href="http://www.hatsu.com.br/experimente"><button>Experimente Hatsu</button></a></div>
        </div>
    </div>

        <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="pdv">
                    <h4>SUGIRA UM LOCAL</h4>
                    <p>Gostaria que estivéssemos mais próximos de você? Sugira um local! Envie sua sugestão de local para <a href="vendas@hatsu.com.br" style="color: #77a302">vendas@hatsu.com.br</a> e estudaremos a possibilidade de realizá-lo. </p>
                </div>
            </div>
        </div>
    </div>

   


</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div><!-- fecha div col 12 -->


@stop