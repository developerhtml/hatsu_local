@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Linha Ultra | Hatsu | Óculos de Alta Tecnologia</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

	<div class="container-fluid">

		<div class="row">

			<div class="col-md-12" id="linha-ultra" style="background-color:#000000">

			<div class="col-md-6">

			&nbsp;

			</div>

			<div class="col-md-4" id="info-ultra" style="color:#fff">

			<p class='ultra-titulo-hatsu'>ULTRA</p>

			<br />

			<p class="ultra-principal">

			A linha Ultra não deforma quando submetida a torções ou quedas, pois possui armação de uma resina extremamente flexível, leve e de altíssima estabilidade dimensional. Ideal para desastrados e para acompanhar o cotidiano corrido. O design conta com caimento perfeito e conforto excepcional.

			</p>

			</div>

			<div class="col-md-2" style="backgroun-color:#FAF2E7">

			&nbsp;

			</div>

		</div>

	</div>

	</div>

<!-- 	<div class="container-fluid" style="background-color:#fff;">

		<div class="row" id="linha-ultra">

			<div class="col-md-2">

			&nbsp;

			</div>

			<div class="col-md-3" id="titulo-ultra">

			<p style="color:#000;padding:20px;padding-right:20px;">ULTRA</p>

			</div>

			<div class="col-md-7" id="info-ultra">

			<p class="ultra-texto">

			

			</p>

			</div>

	</div>

	</div>	 -->

	<br style="clear:both;" />

	<div class="container">

		<h2 style="border-bottom:1px solid #000;text-align:center;">ESCOLHA O SEU</h2>
		<br />

		{{ Helper::LinhaProdutos(3) }}

	</div>

	<br style="clear:both;" />

		<div class="container-fluid" id="ultra-estabilidade">

			<div class="col-md-2">
				&nbsp;
			</div>

			<div class="col-md-4" style="text-align:center;vertical-align:middle;height:150px;color:#000;padding-top:150px;">
				<p class="titulo-ultra">ESTABILIDADE DIMENSIONAL</p>
				<p class="subtitulo-ultra">Armações tortas? Nunca mais!</p>
				<p class="texto-ultra" style="width:300px">
				Diferentemente de metal comum ou acetato, as armações não perdem sua forma quando submetidas a torções, stress ou compressão.
				</p>
			  </div>

			  <div class="col-md-4" style="text-align:center;vertical-align:middle;height:375px;">			  	
			  	<!--
				<video name="media" autoplay loop>
				  <source src="{{ URL::asset('img/videos/estabilidade_ultra.ogv') }}" type="video/ogv">
				  <source src="{{ URL::asset('img/videos/estabilidade_ultra.webm') }}" type="video/webm">
				  <source src="{{ URL::asset('img/videos/estabilidade_ultra.mp4') }}" type="video/mp4">
				</video>
				-->				
				<img src="{{ URL::asset('img/linha-ultra/estabilidade_ultra_hipotese.png') }}" alt="Estabilidade Dimensional" />								
				<img src="{{ URL::asset('img/linha-ultra/estabilidade.jpg') }}" class="estabilidade img-responsive" style="display:none;">
			  </div>

			<div class="col-md-1">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="resistencia-ultra" style="height:275px;background:url('{{ URL::asset('img/linha-ultra/bgfire.png') }}') repeat top center;padding-top:50px;">

			<div class="col-md-2">

			&nbsp;

			</div>

			  <div class="col-md-3" style="text-align:center;vertical-align:middle;">

			  <img src="{{ URL::asset('img/linha-ultra/fire.png') }}" alt="Resistência Térmica"   />

			</div>

			  <div class="col-md-5" style="text-align:center;vertical-align:middle;color:#fff;padding-top:35px;">

				<p class="titulo-ultra">RESISTÊNCIA TÉRMINA</p>

				<p class="subtitulo-ultra">Rigidez além de 200 graus Celsius.</p>

				<p class="texto-ultra">É comum armações de acetato perderem sua rigidez. Com temperatura de mais de 200 graus, a armação do Ultra suporta altas temperaturas sem perder suas propriedades ou forma.</p>

			  </div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="ultra-flexibilidade">

			<div class="col-md-1">
				&nbsp;
			</div>

			 <div class="col-md-4" style="text-align:center;vertical-align:middle;height:150px;color:#000;padding-top:200px;">
				<p class="titulo-ultra">FLEXIBILIDADE</p>
				<p class="subtitulo-ultra">Adeus armações quebradas.</p>
				<p class="texto-ultra">Conforto e a ergonomia que se adapta a cada formato de rosto é graças a flexibilidade atingida com o uso de material altamente inovador, inédito no Brasil. Além de não quebrar facilmente como armações de acetato.</p>
			 </div>

			 <div class="col-md-5" style="text-align:center;vertical-align:middle;height:150px;">
				<!--
				<video name="media" autoplay loop>
					<source src="{{ URL::asset('img/videos/flexibilidade_ultra.ogv') }}" type="video/ogv">
					<source src="{{ URL::asset('img/videos/flexibilidade_ultra.webm') }}" type="video/webm">
				  	<source src="{{ URL::asset('img/videos/flexibilidade_ultra.mp4') }}" type="video/mp4">
				</video>
				-->				
				<img src="{{ URL::asset('img/linha-ultra/flexibilidade_ultra.jpg') }}" alt="Flexibilidade">
				<img src="{{ URL::asset('img/linha-ultra/flexibilidade.png') }}" class="flexibilidade img-responsive" style="display:none;">
			 </div>

			<div class="col-md-1">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="info-conforto">

			<div class="col-md-2">

			&nbsp;

			</div>

			  <div class="col-md-8" style="text-align:center;vertical-align:middle;height:150px;">

			  <p class="conforto">"Design. Conforto. Resistência"</p>

			  </div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>
		
		<div class="container-fluid" id="ultra-leveza" style="background-color:#FFF;height:450px;">

			<div class="col-md-2">
				&nbsp;
			</div>

			<div class="col-md-4" style="text-align:center;vertical-align:middle;">
				<img src="{{ URL::asset('img/linha-ultra/dentedelion.png') }}" alt="Leveza - 6 gramas de ergonomia" class="leveza" />
			</div>

			  <div class="col-md-4 lev" style="text-align:center;vertical-align:middle;color:#000;margin-top:30px;">

				<p class="titulo-ultra">LEVEZA</p>

				<p class="subtitulo-ultra">6 gramas de ergonomia.</p>

				<p class="texto-ultra">A da armação traz conforto extremo, uma vez que não pesa no rosto não forçando assim nariz e orelha. Sendo ideal pra quem já usa óculos, e perfeito para recém adeptos.</p>

			  </div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<!--
		<div class="container-fluid" style="height:auto;padding:0;margin:0;" id="ultra-trivex">

			<div class="col-md-2">
				&nbsp;
			</div>

			<div class="col-md-4" style="height:574px;text-align:right;vertical-align:middle;">
			
				<video name="media" autoplay loop>
					<source src="{{ URL::asset('img/videos/trivex.ogv') }}" type="video/ogv">
				  	<source src="{{ URL::asset('img/videos/trivex.webm') }}" type="video/webm">
				  	<source src="{{ URL::asset('img/videos/trivex.mp4') }}" type="video/mp4">
				</video>
				
				<img src="{{ URL::asset('img/linha-ultra/trivex_hipotese.jpg') }}" alt="Lentes Trivex">
				<img src="{{ URL::asset('img/linha-ultra/trivex.jpg') }}" class="trivex img-responsive" style="display:none;">

			</div>

			  <div class="col-md-4" style="height:575px;text-align:center;vertical-align:middle;color:#fff;background:#7e3516;padding-top:220px">

				<p class="titulo-ultra">LENTES TRIVEX</p>

				<p class="subtitulo-ultra">Antirreflexo, antirrisco, antiquebra. Grátis!</p>

				<p class="texto-ultra">Oferecidas gratuitamente, as lentes Trivex® com antirreflexo, com proteção UVA e UVB, antirrisco e praticamente inquebráveis.</p>

			  </div>

			<div class="col-md-2" style="background-color:#7e3516;padding:0;margin:0;height:575px;">

			&nbsp;

			</div>
			-->
		</div>

</div>

@stop