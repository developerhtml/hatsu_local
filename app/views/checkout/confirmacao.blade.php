<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">

    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/hatsu.css') }}" rel="stylesheet"> 
    <link rel="shortcut icon" href="{{ URL::asset('img/favicon/favicon.png') }}">

    <style type="text/css">

    p { font-size:14px; }

    </style>

</head>

<body>

<div class="container" style="padding:10px;width:600px;">

  <div class="row">

  <header style="padding:0;">
    <div class="container">
      <div class="row">
        <div class="col-md-3" style="padding-left:0;padding:0;">
          <!-- Logo. Use class "color" to add color to the text. -->
          <div class="logo">
            <h1><a href="http://www.hatsu.com.br"><img src="http://hatsu.com.br/public/img/logo-hatsu.png" class="img-responsive" /></a></h1>
          </div>
        </div>
      </div>
    </div>
  </header>


  <?php 

  $id_cliente = 1; 
  $id_pedido = 2; 
  $id_pagamento = 1; 

  $cliente =  Cliente::find($id_cliente);

  $pedido =  Pedido::find($id_pedido);

  $itens_pedido = PedidoItem::where('id_pedido', '=', $id_pedido)->get();

  $pagamento =  Pagamento::find($id_pagamento);

  ?>

  <h1>Olá {{ $cliente->nome }}. Seja bem vindo a Hatsu!</h1>

  <br />

  <p>Acesse sua conta Hatsu a qualquer momento clicando no botão de <a href="http://hatsu.com.br/login">LOGIN</a>. Utilize seu e-mail e senha para alterar seus dados, gerenciar seus pedidos e enviar suas prescrições médicas.</p>

  <br />

  <p>Dúvidas podem ser previamente retiradas na página de <a href="http://hatsu.com.br/duvidas">Dúvidas</a>. 

  <br />
  <br />

  <p>Caso necessite entrar em contato conosco envie um email para <b><a href="atendimento@hatsu.com.br">atendimento@hatsu.com.br</a></b> ou fique à vontade para nos ligar no telefone <b>(19) 3252-3760</b></p>

  <br />
  <br />

  <p>

  Curta nossa página no Facebook: <a href="http://www.facebook.com/HatsuBrasil">FB.com/HatsuBrasil</a>
  <br />

  Siga nosso twitter: <a href="http://www.twitter.com/hatsubr">@HatsuBR</a>
  </p>

  </div>

</div>

</body>

</html>
