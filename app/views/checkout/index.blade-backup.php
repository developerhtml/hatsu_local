@extends('template')

@section('seo')

<!-- Title here -->
<title>Checkout | Óculos de grau, armações e lentes | Hatsu | Óculos de Alta Tecnologia</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil. Produtos de primeira linha com pagamento em até 10 vezes sem juros.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importados, óculos miopia, óculos de alta tecnologia, óculos femininos, óculos masculinos, óculos inquebrável, lentes bifocais, lentes,raid, projection, titanium, óculos titânio">
<meta name="author" content="Hatsu - 2014 - Todos os direitos reservados">
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="2 days" />
<link rel="canonical" href="http://www.hatsu.com.br" />
<link rel="index" title="Óculos de grau, armações e lentes | Hatsu" href="http://www.hatsu.com.br" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">INÍCIO</a>&nbsp;|&nbsp;<a href="{{ URL::to('/produtos') }}" class="inativo">Produtos</a>&nbsp;|&nbsp;<a href="{{ URL::to('/carrinho') }}" class="inativo">Carrinho</a>&nbsp;|&nbsp;<span class="ativo">Checkout</span>

</div>

</div>

<!-- Page content starts -->

<div class="content">

	<div class="container">

		<div class="row"> 

			<div class="col-md-12" style="text-align:right;">

			<h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;">CHECKOUT</h1>

			</div>

		</div>

		<form class="form-horizontal" action="{{ URL::to('checkout/store') }}" method="post" id="payment_form">

		<div class="row" id="checkout-cadastro"> 


	  @if (Session::has('message'))

        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2><br />
        
      @endif

			@if(!Auth::check())

			<p class="checkout-alert">Já possui conta na <strong>HATSU</strong>? Acesse-a clicando <a href="#login" role="button" data-toggle="modal" style="color:#000;text-decoration:underline;">AQUI</a></p>

			@else 

			<?php $dados_cliente = Cliente::find(Auth::id()); ?>

			<p class="checkou-alert">Olá {{ $dados_cliente->nome }}! </p><br />

			@endif

			@if($errors->has('codigo')>0) <p class='error' style='text-align:center;font-size:14px;'> {{ $errors->first('codigo') }} </p> @endif	

			@if (Session::has('naodeu'))

        	<p class="error" style="text-align:center;font-size:16px;">{{ Session::get('naodeu') }}</p>
        
      		@endif

      		@if(Session::has('voucher')) 

			<p class="checkout-alert" style="color:green;">CÓDIGO DE DESCONTO APLICADO COM SUCESSO!</p>

			@elseif(Session::has('voucherclube')) 

			<p class="checkout-alert" style="color:green;">CÓDIGO DE DESCONTO APLICADO COM SUCESSO!</p>

			@else

			<p class="checkout-alert" style="color:green;"><a href="#voucher" role="button" data-toggle="modal" style="color:green;text-decoration:underline;">Você possui um <strong>VOUCHER</strong> de desconto? CLIQUE AQUI</a></p>

			@endif

			<br />

			<div class="col-md-4" style="border-right:1px solid #000;">

			<h4 class="step">1. DADOS PESSOAIS</h4>

			<div id="div-checkout">

			@if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif
			<label for="nome" class="label-checkout">Nome Completo</label>
			<input type="text" class="form-checkout" @if($errors->has('nome')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" @if(Input::old('nome')) value="{{ Input::old('nome') }}" @elseif(Auth::check() && !empty($dados_cliente->nome)) value="{{ $dados_cliente->nome }}" @endif placeholder="Informe seu nome completo">
			</div>

			<div id="div-checkout">
			@if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
			<label for="email" class="label-checkout">Email</label>
			<input type="text" class="form-checkout" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" @if(Input::old('email')) value="{{ Input::old('email') }}" @elseif(Auth::check() && !empty($dados_cliente->email)) value="{{ $dados_cliente->email }}" @endif placeholder="Preencha seu e-mail">
			</div>

			<div id="div-checkout">
			@if($errors->has('telefone')>0) <p class='error'> {{ $errors->first('telefone') }} </p> @endif
			<label for="telefone" class="label-checkout">Telefone</label>
			<input type="text" class="form-checkout" @if($errors->has('telefone')>0) style='border-color:#ff0000;' @endif id="telefone" name="telefone" @if(Input::old('telefone')) value="{{ Input::old('telefone') }}" @elseif(Auth::check() && !empty($dados_cliente->telefone)) value="{{ $dados_cliente->telefone }}" @endif placeholder="Telefone para contato">
			</div>

			<div id="div-checkout">
			<label for="celular" class="label-checkout">Celular</label>
			<input type="text" class="form-checkout" @if($errors->has('celular')>0) style='border-color:#ff0000;' @endif id="celular" name="celular" @if(Input::old('celular')) value="{{ Input::old('celular') }}" @elseif(Auth::check() && !empty($dados_cliente->celular)) value="{{ $dados_cliente->celular }}" @endif placeholder="Celular para contato">
			</div>

			@if(!Auth::check()) 

				<div id="div-checkout">
				@if($errors->has('password')>0) <p class='error'> {{ $errors->first('password') }} </p> @endif
				<label for="senhaa" class="label-checkout">Crie sua senha</label>
				<input type="password" class="form-checkout" @if($errors->has('password')>0) style='border-color:#ff0000;' @endif id="password" name="password">
				</div>

				<div id="div-checkout">
				@if($errors->has('c_password')>0) <p class='error'> {{ $errors->first('c_password') }} </p> @endif
				<label for="confirma" class="label-checkout" style="font-size:11px !important;">Confirme a Senha</label>
				<input type="password" class="form-checkout" @if($errors->has('c_password')>0) style='border-color:#ff0000;' @endif id="c_password" name="c_password">
				</div>

			@endif

			<div id="div-checkout">
			@if($errors->has('cpf')>0) <p class='error'> {{ $errors->first('cpf') }} </p> @endif
			<label for="cpf" class="label-checkout">CPF</label>
			<input type="text" class="form-checkout" @if($errors->has('cpf')>0) style='border-color:#ff0000;' @endif id="cpf" name="cpf"  @if(Input::old('cpf')) value="{{ Input::old('cpf') }}" @elseif(Auth::check() && !empty($dados_cliente->cpf)) value="{{ $dados_cliente->cpf }}"  @endif placeholder="Informe seu CPF">
			</div>

			<div id="div-checkout">
			@if($errors->has('sexo')>0) <p class='error'> {{ $errors->first('sexo') }} </p> @endif
			<label for="sexo" class="label-checkout">Sexo</label>
				<select class="form-checkout" style="min-width:155px;" @if($errors->has('sexo')>0) style='border-color:#ff0000;' @endif id="sexo" name="sexo">
					<option value="">- Selecione -</option>
					<option value="Masculino" @if(Input::old('sexo') && Input::old('sexo') == "Masculino") selected="selected" @elseif(Auth::check() && !empty($dados_cliente->sexo) && $dados_cliente->sexo == 'Masculino') selected='selected' @endif >Masculino</option>
					<option value="Feminino" @if(Input::old('sexo') && Input::old('sexo') == "Feminino")  selected="selected" @elseif(Auth::check() && !empty($dados_cliente->sexo) && $dados_cliente->sexo == 'Feminino') selected='selected'  @endif >Feminino</option>
				</select>
			</div>

			<br style="clear:both;" />

			<h4 class="step">2. DADOS DE ENTREGA</h4>

			<div id="div-checkout">
				@if($errors->has('cep')>0) <p class='error'> {{ $errors->first('cep') }} </p> @endif
				<label class="label-checkout" for="cep">CEP</label>
				<input type="text" onblur="return ajaxEndereco(this);" class="form-checkout" @if($errors->has('cep')>0) style='border-color:#ff0000;' @endif id="cep" name="cep" @if(Input::old('cep')) value="{{ Input::old('cep') }}" @elseif(Auth::check() && !empty($dados_cliente->cep)) value="{{ $dados_cliente->cep }}" @endif>
			</div>

			<div id="div-checkout">
			@if($errors->has('estado')>0) <p class='error'> {{ $errors->first('estado') }} </p> @endif
				<label class="label-checkout" for="estado">Estado</label>
				<select class="form-checkout" id="estado" name="estado" @if($errors->has('estado')>0) style='border-color:#ff0000;' @endif>
				<option value="">- Selecione -</option>
				@foreach(Estado::all() as $estado)
				<option @if(Input::old('estado') && Input::old('estado') == $estado->id) {{ 'selected="selected"' }} @elseif(Auth::check() && $dados_cliente->uf == $estado->id) {{ 'selected="selected"' }} @endif value="{{ $estado->id }}">{{ $estado->nome }}</option>
				@endforeach
				</select>
			</div>
			
			<div id="div-checkout">
			@if($errors->has('cidade')>0) <p class='error'> {{ $errors->first('cidade') }} </p> @endif
			<label class="label-checkout" for="cidade">Cidade</label>
			<select class="form-checkout" id="cidade" name="cidade" @if($errors->has('cidade')>0) style='border-color:#ff0000;' @endif>
			@if (Input::old('cidade')) 
				@foreach(Cidade::where('uf','=',Cidade::find(Input::old('cidade'))->uf)->get() as $cidade)
					<option value='{{ $cidade->id }}' @if($cidade->id == Input::old('cidade')) selected='selected' @endif >{{ $cidade->nome }}</option>
				@endforeach
			@elseif (!empty($dados_cliente->cidade))
				@foreach(Cidade::where('uf','=',Cidade::find($dados_cliente->cidade)->uf)->get() as $cidade)
					<option value='{{ $cidade->id }}' @if($cidade->id == $dados_cliente->cidade) selected='selected' @endif >{{ $cidade->nome }}</option>
				@endforeach
			@else
				<option value="">- Selecione -</option>
			@endif
			</select>
			</div>

			<div id="div-checkout">
			@if($errors->has('bairro')>0) <p class='error'> {{ $errors->first('bairro') }} </p> @endif
			<label class="label-checkout" for="bairro">Bairro</label>
			<input type="text" class="form-checkout" @if($errors->has('bairro')>0) style='border-color:#ff0000;' @endif id="bairro" name="bairro" @if(Input::old('bairro')) value="{{ Input::old('bairro') }}" @elseif(Auth::check() && !empty($dados_cliente->bairro)) value="{{ $dados_cliente->bairro }}" @endif >
			</div>

			<div id="div-checkout">
			@if($errors->has('endereco')>0) <p class='error'> {{ $errors->first('endereco') }} </p> @endif
			<label class="label-checkout" for="endereco">Endereço</label>
			<input type="text" class="form-checkout" @if($errors->has('endereco')>0) style='border-color:#ff0000;' @endif id="endereco" name="endereco" @if(Input::old('endereco')) value="{{ Input::old('endereco') }}" @elseif(Auth::check() && !empty($dados_cliente->endereco)) value="{{ $dados_cliente->endereco }}" @endif >
			</div>

			<div id="div-checkout">
			@if($errors->has('numero')>0) <p class='error'> {{ $errors->first('numero') }} </p> @endif
			<label class="label-checkout" for="numero">Número</label>
			<input type="text" onkeyup="checkInput(this)" maxlength="6" class="form-checkout" @if($errors->has('numero')>0) style='border-color:#ff0000;' @endif id="numero" name="numero" @if(Input::old('numero')) value="{{ Input::old('numero') }}" @elseif(Auth::check() && !empty($dados_cliente->numero)) value="{{ $dados_cliente->numero }}" @endif>
			</div>

			<div id="div-checkout">
			<label class="label-checkout" for="complemento">Complemento</label>
			<input type="text" class="form-checkout" id="complemento" name="complemento" @if(Input::old('complemento')) value="{{ Input::old('complemento') }}" @elseif(Auth::check() && !empty($dados_cliente->complemento)) value="{{ $dados_cliente->complemento }}" @endif >
			</div>

			</div>

			<div class="col-md-4" style="border-right:1px solid #000;">

			<h4>3. FRETE E PRAZO</h4>

			<div id="aviso-frete">

			<span>Frete GRÁTIS</span> para todo Brasil<br />
			<!-- <span>Frete GRÁTIS</span> para os estados de SP, MG, RJ, PR e SC<br /> -->
			<b>Entrega</b> 5 dias úteis para lentes sem grau<br />
			10 dias úteis para lentes com grau

			</div>

			<br />

			<h4>4. PAGAMENTO</h4>

			<?php  $totalPedido = Helper::CalculaTotal()  ?>

			<p><b>TOTAL DO PEDIDO:</b> R$ {{ number_format($totalPedido,2,",",".") }}</p>

			@if($totalPedido <= 0 && Session::has('voucherclube'))

			<input type="hidden" id="forma_pagamento_boleto" name="forma_pagamento" value="boleto">

			<input type="hidden" value="{{ $totalPedido }}" name="amount" id="amount">

			<p>Não será necessário gerar boleto ou pagamento com cartão.</p>

			<br />

			<p>Preencha seus dados cadastrais e de entrega corretamente e clique no botão "FINALIZAR COMPRA"</p>

			@else 

			<br />

			@if($errors->has('forma_pagamento')>0) <p class='error'> {{ $errors->first('forma_pagamento') }} </p> @endif

			<input type="radio" id="forma_pagamento_boleto" name="forma_pagamento" value="boleto">
			<label class="label-checkout" id="forma_pagamento_boleto" for="forma_pagamento_boleto" style="font-size:16px;text-transform:uppercase;font-weight:normal;">Boleto</label>
			
			&nbsp;&nbsp;

			<input type="radio" id="forma_pagamento_cartao" name="forma_pagamento" value="cartao" style="width:12px;height:12px;" checked="checked">
			<label class="label-checkout" id="forma_pagamento_cartao" for="forma_pagamento_cartao" style="font-size:16px;text-transform:uppercase;font-weight:normal;">Cartão de crédito</label>

			<input type="hidden" value="{{ $totalPedido }}" name="amount" id="amount">

			<br />

			<div id="checkout-boleto" style="display:none;margin-top:10px;">

			<br />

			<p><b>INSTRUÇÕES PARA PAGAMENTO COM BOLETO</b></p>

			<br />

			<p>

			<span style="font-size:15px;color:#ff0000;"> O link para o boleto bancário <b>será gerado na próxima página</b>, após o checkout</span><br /><br />

			- Para esta opção, só serão aceitos pagamentos à vista.<br /><br />

			- Adicione 2 a 3 dias úteis ao prazo de entrega (devido à compensação do pagamento com o banco)<br /><br />

			</p>

			</div>

			@if(isset($erro_pagamento))

			<!-- Google Code for Compra Conversion Page -->
			<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 977863673;
			var google_conversion_language = "en";
			var google_conversion_format = "1";
			var google_conversion_color = "ffffff";
			var google_conversion_label = "I_y_CO-HqAoQ-Yek0gM";
			var google_conversion_value = 1.00;
			var google_remarketing_only = false;
			/* ]]> */
			</script>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
			</script>
			<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/977863673/?value=1.00&amp;label=I_y_CO-HqAoQ-Yek0gM&amp;guid=ON&amp;script=0"/>
			</div>
			</noscript>


			<p class="error">Sua transação foi recusada. Tente novamente ou utilize outro cartão!</p>

			@endif

				<div id="checkout-cartao" style="display:block;margin-top:10px;">

					@if($errors->has('bandeira')>0) <p class='error'> {{ $errors->first('bandeira') }} </p> @endif

					<input type="radio" name="bandeira" id="bandeira_visa" value="visa" @if(Input::old('bandeira') == "visa") checked="checked" @endif ><label for="bandeira_visa"><img src="{{ URL::asset('img/payment/n_visa.png') }}" /></label>&nbsp;&nbsp;
					<input type="radio" name="bandeira" id="bandeira_master" value="mastercard" @if(Input::old('bandeira') == "mastercard") checked='checked' @endif><label for="bandeira_master"><img src="{{ URL::asset('img/payment/n_master.png') }}" alt="Bandeira Mastercard" /></label>&nbsp;&nbsp;
					<input type="radio" name="bandeira" id="bandeira_elo" value="elo" @if(Input::old('bandeira') == "mastercard") checked='checked' @endif><label for="bandeira_elo"><img src="{{ URL::asset('img/payment/n_elo.png') }}" alt="Bandeira Elo" /></label>&nbsp;&nbsp;
					<input type="radio" name="bandeira" id="bandeira_diners" value="diners" @if(Input::old('bandeira') == "mastercard") checked='checked' @endif><label for="bandeira_diners"><img src="{{ URL::asset('img/payment/n_diners.png') }}" alt="Bandeira Diners Club" /></label><br />

					<div id="div-checkout">
					@if($errors->has('instalments')>0) <p class='error'> {{ $errors->first('instalments') }} </p> @endif
					<label for="instalments" class="label-checkout">Parcelas:</label>
					<select id="instalments" name="instalments" class="form-checkout" style="height:20px;">
					@foreach (range(1,10) as $value)
						<option value='{{ $value }}'>{{ $value .' x R$ '. number_format($totalPedido/$value,2,",",".")  }}</option>
					@endforeach
					</select>
					</div>

					<div id="div-checkout">
					@if($errors->has('card_number')>0) <p class='error'> {{ $errors->first('card_number') }} </p> @endif
					<label for="card_number" class="label-checkout">Número do Cartão:</label>
		 			<input type="text" id="card_number" name="card_number" class="form-checkout" />
		 			</div>

					<div id="div-checkout">
					@if($errors->has('card_holder_name')>0) <p class='error'> {{ $errors->first('card_holder_name') }} </p> @endif
					<label for="card_holder_name" class="label-checkout">Titular do Cartão:</label>
					<input type="text" id="card_holder_name" name="card_holder_name" class="form-checkout" style="width:280px" />
					</div>
		            
		            <br /><br />

					<div id="div-checkout">
					@if($errors->has('card_expiration_month')>0) <p class='error'> {{ $errors->first('card_expiration_month') }} </p> @endif
					<label for="card_expiration_month" class="label-checkout">Mês de Validade:</label>
					<input type="text" id="card_expiration_month" name="card_expiration_month" class="form-checkout" style="width:50px;" />
					</div>

					<div id="div-checkout">
					@if($errors->has('card_expiration_year')>0) <p class='error'> {{ $errors->first('card_expiration_year') }} </p> @endif
					<label for="card_expiration_year" class="label-checkout">Ano de Validade:</label>
					<input type="text" id="card_expiration_year" name="card_expiration_year" class="form-checkout"  style="width:50px;" />
					</div>					


					<div id="div-checkout">
					@if($errors->has('card_cvv')>0) <p class='error'> {{ $errors->first('card_cvv') }} </p> @endif
					<label for="card_cvv" class="label-checkout">Código de segurança:</label>
					<input type="text" id="card_cvv" name="card_cvv" class="form-checkout" style="width:50px;" />
					</div>		

	            </div>

				@endif
				
			</div>

			<div class="col-md-4">

			<h4>5. REVISÃO</h4>

			<p>Após finalizar a compra, envia cópia de sua receita para <a href="mailto:receita@hatsu.com.br" title="Envie sua receita">receita@hatsu.com.br</a></p>

			@if(isset($id_pedido))

			<input type="hidden" name="id_pedido" value="{{ $id_pedido }}">

			@endif

			@if(isset($id_pagamento))

			<input type="hidden" name="id_pagamento" value="{{ $id_pagamento }}">

			@endif

			<div id="revisao">

			{{ Helper::RevisaoPedido() }}

			</div>

			<br />
							
			<p style="text-align:center;">
			
			<a href="{{ URL::to('produtos') }}" class="botao-experimente" style="width:280px;font-size:13px;">CONTINUAR COMPRANDO</a>

			<a href="{{ URL::to('carrinho') }}" class="botao-experimente" style="width:280px;font-size:13px;margin:5px 0px 5px 0px">MODIFICAR PEDIDO</a>

			<input type="submit" class="botao-comprar" style="width:280px;" value="FINALIZAR COMPRA">

			</form>
			
			<br>
			
			<!-- Script do Site Blindado
			<div id="aw_malware">
		    	<param id="aw_malware_preload" value="true" />
		    </div>
			<script type="text/javascript" src="//selo.siteblindado.com/aw_malware.js"></script>
			-->
			
			<img src="{{ URL::asset('img/ssl.png') }}" alt="Site Blindado" />
			
			</p>

			</div>

		</div>

	</div>

</div>



@stop