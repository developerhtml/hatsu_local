@extends('template')

@section('seo')

{{ Helper::limpaCarrinho() }}

<!-- Tags de SEO personalizadas -->
<title>Pedido Concluído | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<!-- Page content starts -->

<div class="content">

  <div class="container">

    <div class="row"> 

      <div class="col-md-12" style="text-align:left;border-bottom:1px solid #000;">

      <h1 style="font-size:30px;font-weight:normal;color:#000;">OBRIGADO POR COMPRAR NA HATSU</h1>

      </div>

    </div>

  <div class="row" style="border-bottom:1px solid #000;padding-top:10px;padding-bottom:10px;"> 

    <div class="col-md-12">

      <div class="col-md-4">

            <!-- Google Code for Compra Conversion Page -->
      <script type="text/javascript">
      /* <![CDATA[ */
      var google_conversion_id = 977863673;
      var google_conversion_language = "en";
      var google_conversion_format = "1";
      var google_conversion_color = "ffffff";
      var google_conversion_label = "I_y_CO-HqAoQ-Yek0gM";
      var google_conversion_value = 1.00;
      var google_remarketing_only = false;
      /* ]]> */
      </script>
      <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
      </script>
      <noscript>
      <div style="display:inline;">
      <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/977863673/?value=1.00&amp;label=I_y_CO-HqAoQ-Yek0gM&amp;guid=ON&amp;script=0"/>
      </div>
      </noscript>
      

       <p class="numero_pedido">O número do seu pedido é<br />
       <span>{{ $cod }}</span>
       </p>

      </div>

      <div class="col-md-6" style="font-size:16px;">

      <p><b>SEU PEDIDO FOI REALIZADO COM SUCESSO</b></p>
      <p>Em breve você receberá um e-mail de confirmação com todas as informações de sua compra.</p>

      </div>

      <div class="col-md-2">

          @if(isset($boleto))

          <a href="{{ $boleto }}" class="button-comum" style="font-size:14px;">IMPRIMIR BOLETO</a>

          @endif

      </div>

    </div>

  </div>

    <br style="clear:both;"><br style="clear:both;">

    <div class="row">

        <div class="col-md-12">

          <div class="col-md-4 confirmacao">

          <img src="{{ URL::asset('img/minha-conta.png') }}" /><br />

          <p>MINHA CONTA</p>

          <br />

          Para confeccionarmos corretamente suas lentes precisamos de sua prescrição médica. Por esse motivo solicitamos que nos envia sua receita por e-mail.

          <br />

          <a href="{{ URL::to('minhaconta') }}" style="color:#77A302;text-decoration:underline;font-weight:bold;">SAIBA MAIS</a>

          </div>

          <div class="col-md-4 confirmacao">

          <img src="{{ URL::asset('img/blog.png') }}" /><br />

          <p>BLOG</p>

          <br />

          Em nosso blog publicamos textos produzidos por nós, abordando temas variados como curiosidades, recomendações e histórias relacionadas à visão.<br />

          <a href="http://blog.hatsu.com.br" target="_blank" style="color:#77A302;text-decoration:underline;font-weight:bold;">SAIBA MAIS</a>

          </div>

          <div class="col-md-4 confirmacao">

            <img src="{{ URL::asset('img/contato.png') }}" /><br />

            <p>CONTATO</p>

            <br />

            Acesse nossa página de contato e nos dê sua opinião sobre a Hatsu.<br />

            <a href="{{ URL::to('contato') }}" style="color:#77A302;text-decoration:underline;font-weight:bold;">ACESSAR</a>

        </div>

</div>

    </div>

  </div>

</div>

<!-- Page content ends -->


@stop