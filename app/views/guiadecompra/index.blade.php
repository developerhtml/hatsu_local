@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Guia de Compra | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>

<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">INÍCIO</a>&nbsp;|&nbsp;<span class="ativo">guia de compra</span>

</div>

</div>

	<div class="container" style="padding:0;" id="guia">

      @if (Session::has('message'))
      	<br />
        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2>
        <br />
      @endif


		<div id="banner-guia">

		<img src="{{ URL::asset('img/guia/banner.png') }}" class="img-responsive" />

		</div>

		<form method="POST" id="guia-de-compra">

		<div id="masculino-feminino">

			<div class="col-md-6" id="souhomem">
				<input type="radio" name="sexo" id="label-masculino" value="masculino">
				<label for="label-masculino" class="show-souhomem" id="exibe-masculino">
				<img src="{{ URL::asset('img/guia/souhomem.jpg') }}" class="img-responsive normal" />
				<img src="{{ URL::asset('img/guia/souhomem-over.jpg') }}" class="img-responsive over" />
				</label>
			</div>

			<div class="col-md-6 show-soumulher" id="soumulher">
				<input name="sexo" type="radio" id="label-feminino" value="feminino">
				<label for="label-feminino" class="show-soumulher" id="exibe-feminino">
				<img src="{{ URL::asset('img/guia/soumulher.jpg') }}" class="img-responsive normal" />
				<img src="{{ URL::asset('img/guia/soumulher-over.jpg') }}" class="img-responsive over" />
				</label>
			</div>

		</div>

		<div id="rostos-masculino">

		<p class="titulo" id="exibe-rosto-masculino">Selecione abaixo o formato de rosto que mais se parece com o seu.</p>

			<input type="radio" name="masculino-rosto" value="redondo" id="masculino-rosto-redondo-radio">
			<label for="masculino-rosto-redondo-radio" id="exibe-perfil-masculino-1" class="select-masculino-rosto-redondo">
			<div class="col-md-3" id="masculino-rosto-redondo">

				<img src="{{ URL::asset('img/guia/homem-redondo.png') }}" class="normal" title="Rosto Redondo" alt="Rosto redondo masculino" />
				<img src="{{ URL::asset('img/guia/oculos-redondo.png') }}" class="over" title="Rosto Redondo" alt="Rosto redondo masculino" />
				<p class="subtitulo-rosto">Redondo</p>
				<p class="descricao-rosto">Pessoas com rosto sem ângulos definidos, com largura e comprimento em proporções semelhantes e as linhas da mandíbula arredondadas.</p>
				<p class="descricao-rosto-over">Óculos com armações quadradas ou retangulares são mais adequados a esse tipo de rosto. Linhas retas ajudam a afinar e definir os ângulos deste tipo de rosto.</p>
				<p class="selecione-rosto">SELECIONE</p>

			</div>
			</label>

			<input type="radio" name="masculino-rosto" value="quadrado" id="masculino-rosto-quadrado-radio">
			<label for="masculino-rosto-quadrado-radio" id="exibe-perfil-masculino-2" class="select-masculino-rosto-quadrado">
			<div class="col-md-3" id="masculino-rosto-quadrado">

				<img src="{{ URL::asset('img/guia/homem-quadrado.png') }}" class="normal" title="Rosto quadrado" alt="Rosto quadrado masculino" />
				<img src="{{ URL::asset('img/guia/oculos-quadrado.png') }}" class="over" title="Rosto quadrado" alt="Rosto quadrado masculino" />
				<p class="subtitulo-rosto">Quadrado</p>
				<p class="descricao-rosto">Rosto com um amplo maxilar e queixo curto, com largura e comprimento com proporções semelhantes.</p>
				<p class="descricao-rosto-over">Aros totalmente redondos ou arredondados nas laterais suavizam as linhas do rosto.</p>
				<p class="selecione-rosto">SELECIONE</p>

			</div>
			</label>

			<input type="radio" name="masculino-rosto" value="triangular" id="masculino-rosto-triangular-radio">
			<label for="masculino-rosto-triangular-radio" id="exibe-perfil-masculino-3" class="select-masculino-rosto-triangular">
			<div class="col-md-3" id="masculino-rosto-triangular">

				<img src="{{ URL::asset('img/guia/homem-triangular.png') }}" class="normal" title="Rosto triangular" alt="Rosto triangular masculino" />
				<img src="{{ URL::asset('img/guia/oculos-triangular.png') }}" class="over" title="Rosto triangular" alt="Rosto triangular masculino" />
				<p class="subtitulo-rosto">Triangular</p>
				<p class="descricao-rosto">Pessoas com o rosto comprido, com uma ampla linha do maxilar e queixo definido.</p>
				<p class="descricao-rosto-over">Para equilibrar com o queixo mais fino, opte por armações redondas e também as ovaladas.</p>
				<p class="selecione-rosto">SELECIONE</p>

			</div>
			</label>

			<input type="radio" name="masculino-rosto" value="oval" id="masculino-rosto-oval-radio">
			<label for="masculino-rosto-oval-radio" id="exibe-perfil-masculino-4" class="select-masculino-rosto-oval">
			<div class="col-md-3" id="masculino-rosto-oval">

				<img src="{{ URL::asset('img/guia/homem-oval.png') }}" class="normal" title="Rosto oval" alt="Rosto oval masculino" />
				<img src="{{ URL::asset('img/guia/oculos-oval.png') }}" class="over" title="Rosto oval" alt="Rosto oval masculino" />
				<p class="subtitulo-rosto">Oval</p>
				<p class="descricao-rosto">Rosto do tipo oval tem como característica ser levemente mais largo nas maças do rosto. Em geral pessoas com o rosto comprido e as linhas da mandíbula arredondadas.</p>
				<p class="descricao-rosto-over">Por ser um formato de rosto equilibrado, as pessoas com esse tipo de rosto se adequam bem a qualquer formato de óculos.</p>
				<p class="selecione-rosto">SELECIONE</p>

			</div>
			</label>

		</div>

		<div id="rostos-feminino">

			<p class="titulo" id="exibe-rosto-feminino">Selecione abaixo o formato de rosto que mais se parece com o seu.</p>

			<input type="radio" name="feminino-rosto" value="redondo" id="feminino-rosto-redondo-radio">
			<label for="feminino-rosto-redondo-radio" id="exibe-perfil-feminino-1" class="select-feminino-rosto-redondo">
			<div class="col-md-3" id="feminino-rosto-redondo">

				<img src="{{ URL::asset('img/guia/mulher-redondo.png') }}" class="normal" title="Rosto Redondo" alt="Rosto redondo feminino" />
				<img src="{{ URL::asset('img/guia/oculos-redondo.png') }}" class="over" title="Rosto Redondo" alt="Rosto redondo feminino" />
				<p class="subtitulo-rosto">Redondo</p>
				<p class="descricao-rosto">Mulheres com rosto sem ângulos definidos, com largura e comprimento em proporções semelhantes e as linhas da mandíbula arredondadas.</p>
				<p class="descricao-rosto-over">Óculos com armações quadradas ou retangulares são mais adequados a esse tipo de rosto. Linhas retas ajudam a afinar e definir os ângulos deste tipo de rosto.</p>
				<p class="selecione-rosto">SELECIONE</p>

			</div>
			</label>

			<input type="radio" name="feminino-rosto" value="quadrado" id="feminino-rosto-quadrado-radio">
			<label for="feminino-rosto-quadrado-radio" id="exibe-perfil-feminino-2" class="select-feminino-rosto-quadrado">
			<div class="col-md-3" id="feminino-rosto-quadrado">

				<img src="{{ URL::asset('img/guia/mulher-quadrado.png') }}" class="normal" title="Rosto quadrado" alt="Rosto quadrado feminino" />
				<img src="{{ URL::asset('img/guia/oculos-quadrado.png') }}" class="over" title="Rosto quadrado" alt="Rosto quadrado feminino" />
				<p class="subtitulo-rosto">Quadrado</p>
				<p class="descricao-rosto">Rosto com um amplo maxilar e queixo curto, com largura e comprimento com proporções semelhantes.</p>
				<p class="descricao-rosto-over">Aros totalmente redondos ou arredondados nas laterais suavizam as linhas do rosto.</p>
				<p class="selecione-rosto">SELECIONE</p>

			</div>
			</label>

			<input type="radio" name="feminino-rosto" value="triangular" id="feminino-rosto-triangular-radio">
			<label for="feminino-rosto-triangular-radio" id="exibe-perfil-feminino-3" class="select-feminino-rosto-triangular">
			<div class="col-md-3" id="feminino-rosto-triangular">

				<img src="{{ URL::asset('img/guia/mulher-triangular.png') }}" class="normal" title="Rosto triangular" alt="Rosto triangular feminino" />
				<img src="{{ URL::asset('img/guia/oculos-triangular.png') }}" class="over" title="Rosto triangular" alt="Rosto triangular feminino" />
				<p class="subtitulo-rosto">Triangular</p>
				<p class="descricao-rosto">Mulheres com o rosto comprido, com uma ampla linha do maxilar e queixo definido.</p>
				<p class="descricao-rosto-over">Para equilibrar com o queixo mais fino, opte por armações redondas e também as ovaladas.</p>
				<p class="selecione-rosto">SELECIONE</p>

			</div>
			</label>

			<input type="radio" name="feminino-rosto" value="oval" id="feminino-rosto-oval-radio">
			<label for="feminino-rosto-oval-radio" id="exibe-perfil-feminino-4" class="select-feminino-rosto-oval">
			<div class="col-md-3" id="feminino-rosto-oval">

				<img src="{{ URL::asset('img/guia/mulher-oval.png') }}" class="normal" title="Rosto oval" alt="Rosto oval feminino" />
				<img src="{{ URL::asset('img/guia/oculos-oval.png') }}" class="over" title="Rosto oval" alt="Rosto oval feminino" />
				<p class="subtitulo-rosto">Oval</p>
				<p class="descricao-rosto">Rosto do tipo oval tem como característica ser levemente mais largo nas maças do rosto. Em geral pessoas com o rosto comprido e as linhas da mandíbula arredondadas.</p>
				<p class="descricao-rosto-over">Por ser um formato de rosto equilibrado, as pessoas com esse tipo de rosto se adequam bem a qualquer formato de óculos.</p>
				<p class="selecione-rosto">SELECIONE</p>

			</div>
			</label>

		</div>

		<br style="clear:both;display:none;" class="brbr" />
		<br style="clear:both;display:none;" class="brbr" />
		<br style="clear:both;display:none;" class="brbr" />

		<div id="white-block-masculino">

			<div id="perfil-masculino">

				<p class="titulo" id="exibe-qualidades">Escolha quais perfis abaixo melhor se adequam à seu estilo</p>

				<div class="col-md-6" id="perfil-masculino-esportista">

					<input type="radio" value="esportista" name="perfil-masculino" id="masculino-esportista">
					<label for="masculino-esportista" id="perfil-masculino-1" class="select-perfil-masculino-esportista">
					<img src="{{ URL::asset('img/guia/perfil-homem-esportista.png') }}" class="img-responsive normal" title="Homem - Perfil Esportista" alt="Homem - Perfil Esportista" />
					<img src="{{ URL::asset('img/guia/perfil-homem-esportista-over.png') }}" class="img-responsive over" title="Homem - Perfil Esportista" alt="Homem - Perfil Esportista" />
					<p class="subtitulo-perfil">ESPORTISTA</p>
					</label>

				</div>

				<div class="col-md-6" id="perfil-masculino-sofisticado">

					<input type="radio" value="sofisticado" name="perfil-masculino" id="masculino-sofisticado">
					<label for="masculino-sofisticado" id="perfil-masculino-2" class="select-perfil-masculino-sofisticado">
					<img src="{{ URL::asset('img/guia/perfil-homem-sofisticado.png') }}" class="img-responsive normal" title="Homem - Perfil Sofisticado" alt="Homem - Perfil Sofisticado" />
					<img src="{{ URL::asset('img/guia/perfil-homem-sofisticado-over.png') }}" class="img-responsive over" title="Homem - Perfil Sofisticado" alt="Homem - Perfil Sofisticado" />
					<p class="subtitulo-perfil">SOFISTICADO</p>
					</label>

				</div>

				<div class="col-md-6" id="perfil-masculino-cosmopolita">

					<input type="radio" value="cosmopolita" name="perfil-masculino" id="masculino-cosmopolita">
					<label for="masculino-cosmopolita" id="perfil-masculino-3" class="select-perfil-masculino-cosmopolita">
					<img src="{{ URL::asset('img/guia/perfil-homem-cosmopolita.png') }}" class="img-responsive normal" title="Homem - Perfil Cosmopolita" alt="Homem - Perfil Cosmopolita" />
					<img src="{{ URL::asset('img/guia/perfil-homem-cosmopolita-over.png') }}" class="img-responsive over" title="Homem - Perfil Cosmopolita" alt="Homem - Perfil Cosmopolita" />
					<p class="subtitulo-perfil">COSMOPOLITA</p>
					</label>

				</div>

				<div class="col-md-6" id="perfil-masculino-kids">

					<input type="radio" value="kids" name="perfil-masculino" id="masculino-kids">
					<label for="masculino-kids" id="perfil-masculino-4" class="select-perfil-masculino-kids">
					<img src="{{ URL::asset('img/guia/perfil-homem-kids.png') }}" class="img-responsive normal" title="Homem - Perfil Kids" alt="Homem - Perfil Kids" />
					<img src="{{ URL::asset('img/guia/perfil-homem-kids-over.png') }}" class="img-responsive over" title="Homem - Perfil Kids" alt="Homem - Perfil Kids" />
					<p class="subtitulo-perfil">INFANTO/JUVENIL</p>
					</label>

				</div>

			</div>

		</div>			

		<div id="white-block-feminino">

			<div id="perfil-feminino">

				<p class="titulo" id="exibe-qualidades-2">Escolha quais perfis abaixo melhor se adequam à seu estilo</p>

				<div class="col-md-6" id="perfil-feminino-esportista">

					<input type="radio" value="esportista" name="perfil-feminino" id="feminino-esportista">
					<label for="feminino-esportista" id="perfil-feminino-1" class="select-perfil-feminino-esportista">
					<img src="{{ URL::asset('img/guia/perfil-mulher-esportista.png') }}" class="img-responsive normal" title="mulher - Perfil Esportista" alt="mulher - Perfil Esportista" />
					<img src="{{ URL::asset('img/guia/perfil-mulher-esportista-over.png') }}" class="img-responsive over" title="mulher - Perfil Esportista" alt="mulher - Perfil Esportista" />
					<p class="subtitulo-perfil">ESPORTISTA</p>
					</label>

				</div>

				<div class="col-md-6" id="perfil-feminino-sofisticado">

					<input type="radio" value="sofisticado" name="perfil-feminino" id="feminino-sofisticado">
					<label for="feminino-sofisticado" id="perfil-feminino-2" class="select-perfil-feminino-sofisticado">
					<img src="{{ URL::asset('img/guia/perfil-mulher-sofisticada.png') }}" class="img-responsive normal" title="mulher - Perfil Sofisticado" alt="mulher - Perfil Sofisticado" />
					<img src="{{ URL::asset('img/guia/perfil-mulher-sofisticada-over.png') }}" class="img-responsive over" title="mulher - Perfil Sofisticado" alt="mulher - Perfil Sofisticado" />
					<p class="subtitulo-perfil">SOFISTICADO</p>
					</label>

				</div>

				<div class="col-md-6" id="perfil-feminino-cosmopolita">

					<input type="radio" value="cosmopolita" name="perfil-feminino" id="feminino-cosmopolita">
					<label for="feminino-cosmopolita" id="perfil-feminino-3" class="select-perfil-feminino-cosmopolita">
					<img src="{{ URL::asset('img/guia/perfil-mulher-cosmopolita.png') }}" class="img-responsive normal" title="mulher - Perfil Cosmopolita" alt="mulher - Perfil Cosmopolita" />
					<img src="{{ URL::asset('img/guia/perfil-mulher-cosmopolita-over.png') }}" class="img-responsive over" title="mulher - Perfil Cosmopolita" alt="mulher - Perfil Cosmopolita" />
					<p class="subtitulo-perfil">COSMOPOLITA</p>
					</label>

				</div>

				<div class="col-md-6" id="perfil-feminino-kids">

					<input type="radio" value="kids" name="perfil-feminino" id="feminino-kids">
					<label for="feminino-kids" id="perfil-feminino-4" class="select-perfil-feminino-kids">
					<img src="{{ URL::asset('img/guia/perfil-mulher-kids.png') }}" class="img-responsive normal" title="mulher - Perfil Kids" alt="mulher - Perfil Kids" />
					<img src="{{ URL::asset('img/guia/perfil-mulher-kids-over.png') }}" class="img-responsive over" title="mulher - Perfil Kids" alt="mulher - Perfil Kids" />
					<p class="subtitulo-perfil">INFANTO/JUVENIL</p>
					</label>

				</div>

			</div>

		</div>			

		<br style="clear:both;display:none;" class="brbr" />
		<br style="clear:both;display:none;" class="brbr" />

		<div id="white-block-qualidades">

			<div id="guia-qualidades">

				<p class="titulo" style="line-height:22px;">Cada material possui propriedades indispensáveis para ditar qual óculos mais adequa a seu perfil e necessidade.</p>

				<br />

				<p class="titulo">Selecione abaixo os atributos que você busca em um óculos.</p>

				<ul id="lista-qualidades-1">

					<input type="checkbox" value="conforto" name="qualidades[]" id="qualidades-1">
					<label for="qualidades-1">
					<li class="show-item-qualidade" id="item-1">
					<img src="{{ URL::asset('img/guia/conforto.jpg') }}" class="img-responsive normal" />
					<img src="{{ URL::asset('img/guia/conforto-over.jpg') }}" class="img-responsive over click" />
					</li>
					</label>

					<input type="checkbox" value="durabilidade" name="qualidades[]" id="qualidades-2">
					<label for="qualidades-2">
					<li class="show-item-qualidade" id="item-2">
					<img src="{{ URL::asset('img/guia/durabilidade.jpg') }}" class="img-responsive normal" />
					<img src="{{ URL::asset('img/guia/durabilidade-over.jpg') }}" class="img-responsive over click" />
					</li>
					</label>

					<input type="checkbox" value="flexibilidade" name="qualidades[]" id="qualidades-3">
					<label for="qualidades-3">
					<li class="show-item-qualidade" id="item-3">
					<img src="{{ URL::asset('img/guia/flexibilidade.jpg') }}" class="img-responsive normal" />
					<img src="{{ URL::asset('img/guia/flexibilidade-over.jpg') }}" class="img-responsive over click" />
					</li>
					</label>

					<input type="checkbox" value="leveza" name="qualidades[]" id="qualidades-4">
					<label for="qualidades-4">
					<li class="show-item-qualidade" id="item-4">
					<img src="{{ URL::asset('img/guia/leveza.jpg') }}" class="img-responsive normal" />
					<img src="{{ URL::asset('img/guia/leveza-over.jpg') }}" class="img-responsive over click" />
					</li>
					</label>

					<input type="checkbox" value="resistencia" name="qualidades[]" id="qualidades-5">
					<label for="qualidades-5">
					<li class="show-item-qualidade" id="item-5">
					<img src="{{ URL::asset('img/guia/resistencia.jpg') }}" class="img-responsive normal" />
					<img src="{{ URL::asset('img/guia/resistencia-over.jpg') }}" class="img-responsive over click" />
					</li>
					</label>

				</ul>

			</div>

		</div>

		<br style="clear:both;display:none;" class="brbr" />
		<br style="clear:both;display:none;" class="brbr" />

		<div id="botao-final" class="col-md-12">

		<input type="submit" class="botao-encontre" id="botaozin" style="border:none;" value="ENCONTRE MEUS ÓCULOS" >

		<br />

		<div class="form_result"> 
		</div>


		</div>

		</form>

		

</div>

<!-- Page content ends -->


@stop