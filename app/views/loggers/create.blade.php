@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">          
            
            <div class="widget ">
              
              <div class="widget-header">
                <i class="icon-copy"></i>
                <h3>Configurar LOGs</h3>
            </div> <!-- /widget-header -->
          
          <div class="widget-content">
            
            <div class="tabbable">

         <form id="edit-profile" class="form-horizontal" action="{{ URL::to('logger/store') }}" method="POST" name="form-clientes">

         <fieldset>

          <div class="widget widget-nopad">
            <div class="widget-content">
              <div id='calendar' style='padding-top:15px;'>            


                    <div class="control-group">                     
                      <label class="control-label" for="tempo_expiracao">Tempo de Expiração (em dias)</label>
                      <div class="controls">
                        <input type="text" class="span2" placeholder="Informe o valor em dias!" @if($errors->has('tempo_expiracao')>0) style='border-color:#ff0000;' @endif id="tempo_expiracao" name="tempo_expiracao"  @if((Input::old('tempo_expiracao'))) value="{{ Input::old('tempo_expiracao') }}" @else value="{{ $logger->tempo_expiracao }}" @endif >
                        @if($errors->has('tempo_expiracao')>0) <p class='error'> {{ $errors->first('tempo_expiracao') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

              </div>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary">Atualizar</button> 
                      <!-- <button class="btn">Cancelar</button> -->
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>
              
            </div>
            
          </div> <!-- /widget-content -->
            
        </div> <!-- /widget -->
            
        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
@stop