@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">          
            
            <div class="widget ">
              
              <div class="widget-header">
                <i class="icon-tasks"></i>
                <h3>Editando Grupo {{ $grupo->descricao }} </h3>
            </div> <!-- /widget-header -->
          
          <div class="widget-content">
            
            <div class="tabbable">

         <form id="edit-profile" class="form-horizontal" action="{{ URL::to('grupo/update/'.$grupo->id_grupo ) }}" method="POST" name="form-grupos">

         <fieldset>

          <div class="widget widget-nopad">
            <div class="widget-content">
              <div id='calendar' style='padding-top:15px;'>            

                    <div class="control-group">                     
                      <label class="control-label" for="descricao">Descrição</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('descricao')>0) style='border-color:#ff0000;' @endif id="descricao" name="descricao" @if(Input::old('descricao')) value="{{ Input::old('descricao') }}" @else value="{{ $grupo->descricao }}" @endif >
                        @if($errors->has('descricao')>0) <p class='error'> {{ $errors->first('descricao') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      
                      <label class="control-label" @if($errors->has('tipo')>0) style='color:#ff0000' @endif >Tipo:</label>

                      <div class="controls">
                      
                      <label class="checkbox inline"><input type="checkbox" name="pagamento" value="1" @if(Input::old('pagamento') == "1") {{ 'checked="checked"' }} @elseif ($grupo->pagamento == "1") {{ 'checked="checked"' }} @endif> Pagamento</label>

                      <label class="checkbox inline"><input type="checkbox" name="recebimento" value="1" @if(Input::old('recebimento') == "1") {{ 'checked="checked"' }} @elseif ($grupo->recebimento == "1") {{ 'checked="checked"' }} @endif> Recebimento</label>
                        
                        @if($errors->has('tipo')>0) <p class='error'> {{ $errors->first('tipo') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

              </div>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->

                    <div class="form-actions">
                      
                      <button type="submit" class="btn btn-primary">Editar</button> 
                      
                      <a href="{{ URL::to('grupo') }}"><button class="btn" type="button">Retornar</button></a>

                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>
              
            </div>
            
          </div> <!-- /widget-content -->
            
        </div> <!-- /widget -->
            
        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
@stop