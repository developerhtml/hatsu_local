@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">   

          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header hidden-print"> <i class="icon-copy"></i>
              <h3 class="hidden-print">Relatório de Logs  <span style="margin-left:20px;font-size:12px;font-weight:bold"><a href="{{ URL::to('xls') }}" target="_blank">(exportar CSV)</a></span></h3>
            </div>
            <!-- /widget-header -->

            <!-- exibindo message da session -->
            @if (Session::has('message'))
              
              <div class="alert alert-info" style="margin-top:10px;background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;">{{ Session::get('message') }}</div>

            @endif

            @if (count($loggers)<=0)

              <div class="widget-content">
            
              <div class="alert alert-info" style="margin:10px;background:#fff59b;color:#000;border:1px solid #ccc;text-align:center;font-size:14px;">Sua busca não retornou nenhum resultado</div>

              </div>
              <!-- /widget-content --> 
            </div>
            <!-- /widget --> 

            @else             

            <div class="widget-content">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>                     
                    @if ($sortby == 'nome_tabela' && $order == 'asc') {{
                        link_to_action('LoggerController@getIndex','Nome da Tabela',
                            array(
                                'sortby' => 'nome_tabela',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('LoggerController@getIndex','Nome da Tabela',
                            array(
                                'sortby' => 'nome_tabela',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif  
                    </th>
                    <th>                     
                    @if ($sortby == 'nome_tabela' && $order == 'asc') {{
                        link_to_action('LoggerController@getIndex','Identificador Único',
                            array(
                                'sortby' => 'registro',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('LoggerController@getIndex','Identificador Único',
                            array(
                                'sortby' => 'registro',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif  
                    </th>
                    <th>                     
                    @if ($sortby == 'tipo_alteracao' && $order == 'asc') {{
                        link_to_action('LoggerController@getIndex','Ação',
                            array(
                                'sortby' => 'tipo_alteracao',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('LoggerController@getIndex','Ação',
                            array(
                                'sortby' => 'tipo_alteracao',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif  
                    </th>
                    <th>                     
                    @if ($sortby == 'id_usuario' && $order == 'asc') {{
                        link_to_action('LoggerController@getIndex','Usuário',
                            array(
                                'sortby' => 'id_usuario',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('LoggerController@getIndex','Usuário',
                            array(
                                'sortby' => 'id_usuario',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif  
                    </th>
                    <th>                     
                    @if ($sortby == 'data' && $order == 'asc') {{
                        link_to_action('LoggerController@getIndex','Data',
                            array(
                                'sortby' => 'data',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('LoggerController@getIndex','Data',
                            array(
                                'sortby' => 'data',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif  
                    </th>

                  </tr>
                </thead>

               <tbody>
                  @foreach ($loggers as $logger)
              <tr>
                
                <td>{{ strtoupper($logger->nome_tabela) }}</td>

                <td>

                {{ Helper::exibeNome($logger->nome_tabela,$logger->registro); }}

                </td>

                <td>{{ strtoupper($logger->tipo_alteracao) }}</td>

                <td>{{ User::find($logger->id_usuario)->nome }}</td>

                <td>{{ Helper::formataData($logger->data,1) }}</td>

            </tr>
        @endforeach

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 

          @endif

        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
@stop