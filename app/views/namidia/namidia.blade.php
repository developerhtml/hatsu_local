@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Hatsu na Midia | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')
	
    <!----------- TOP PAGE NAV START --------------->
	<section class="top-page-nav">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
					<p><a href="{{ URL::to('/') }}" class="inativo">INÍCIO</a>&nbsp;|&nbsp;<span class="ativo">EXPERIMENTE HATSU</span></p>
				</div>
            </div>
        </div>
    </section>
    <!----------- TOP PAGE NAV END --------------->

    <!-----------CONTENT AREA START--------------->
    <div class="container">
    	<div class="row">
        	<div class="col-sm-12">
            	<div class="namidia-heading-box">
                	<h2>hatsu na mídia</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla laoreet ante at egestas. Proin mollis commodo arcu quis semper.</p>
                </div>
            </div>
            
            <div class="col-sm-12">
            	<div class="namidia-list-box">
                	<ul>
                    	<li class="namidia-box1">
                        	<div class="dinheiro">
                                <a href="#">
                                    <img src="{{ URL::asset('img/namidia/1.jpg') }}" alt="">
                                </a>
                            </div>
                        </li>
                        <li class="namidia-box2">
                        	<div class="gray-bg">
                                <div class="vidade-logo">
                                    <a href="#">
                                        <img src="{{ URL::asset('img/namidia/2.jpg') }}" alt="">
                                    </a>
                                </div>
                                <div class="vidade-contant">
                                    <a href="#">
                                        <p>“vi pessoalmente e fiquei bem impressionado com a qualidade e resistência” </p>
                                    </a>
                                </div>
                            </div>
                        </li>
                        
                        <li class="namidia-box3">
                        	<div class="gray-bg">
                                <div class="petiscos-box">
                                	<a href="#"><img src="{{ URL::asset('img/namidia/3.jpg') }}" alt=""></a>
                                    <p>“nova marca pretende revolucionar o mercado de óculos no países”</p>
                                </div>
                            </div>
                            
                        </li>
                        
                        <li class="namidia-box1">
                        	<div class="gray-bg">
                                <div class="cm-moda">
                                    <a href="#">
                                        <img src="{{ URL::asset('img/namidia/4.png') }}" alt="">
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="namidia-box1">
                        	<div class="jounge">
                                <a href="#">
                                    <img src="{{ URL::asset('img/namidia/5.jpg') }}" alt="">
                                </a>
                            </div>
                        </li>
                        
                        <li class="namidia-box1">
                        	<div class="yahoo">
                                <a href="#">
                                    <img src="{{ URL::asset('img/namidia/6.png') }}" alt="">
                                </a>
                            </div>
                        </li>
                        <li class="namidia-box1">
                        	<div class="gray-bg">
                            	<div class="suacorrida">
                                    <a href="#">
                                        <img src="{{ URL::asset('img/namidia/7.jpg') }}" alt="">
                                    </a>
                                </div>
                            </div>
                        </li>
                        
                        <li class="namidia-box2">
                        	<div class="gray-bg">
                                <div class="mph-logo">
                                    <a href="#">
                                        <img src="{{ URL::asset('img/namidia/8.png') }}" alt="">
                                    </a>
                                </div>
                                <div class="mph-contant">
                                    <a href="#">
                                        <p>“marca brasileira nasce trazendo uma novidade em inovação, design e alta tecnologia”</p>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="namidia-box1">
                        	<div class="modait">
                                <a href="#">
                                    <img src="{{ URL::asset('img/namidia/9.jpg') }}" alt="">
                                </a>
                            </div>
                        </li>
                        
                        <li class="namidia-box1">
                        	<div class="mdermulher">
                                <a href="#">
                                    <img src="{{ URL::asset('img/namidia/10.png') }}" alt="">
                                </a>
                            </div>
                        </li>
                        <li class="namidia-box1">
                        	<div class="fator">
                                <a href="#">
                                    <img src="{{ URL::asset('img/namidia/11.jpg') }}" alt="">
                                </a>
                            </div>
                        </li>
                        <li class="namidia-box3">
                        	<div class="dormir">
                                <a href="#">
                                    <img src="{{ URL::asset('img/namidia/12.png') }}" alt="">
                                </a>
                            </div>
                        </li>
                        
                        <li class="namidia-box1">
                        	<div class="guia">
                                <a href="#">
                                    <img src="{{ URL::asset('img/namidia/13.png') }}" alt="">
                                </a>
                            </div>
                        </li>
                        <li class="namidia-box1">
                        	<div class="gray-bg">
                            	<div class="meterna">
                                    <a href="#">
                                        <img src="{{ URL::asset('img/namidia/14.jpg') }}" alt="">
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-----------CONTENT AREA END--------------->
    


</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div><!-- fecha div col 12 -->


@stop