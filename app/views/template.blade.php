<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">

 @section('seo')

  @show

 @section('head')

    <!-- Bootstrap CSS -->
    <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/guia.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/standalone.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/scrollable-horizontal.css') }}" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="https://pagar.me/assets/pagarme.js"></script>
  <script src="{{ URL::asset('js/all.js') }}"></script>
  <script src="{{ URL::asset('js/jquery.maskedinput.js') }}"></script>
  <script src="{{ URL::asset('/js/mootools-core-1.4.5-nocompat.js') }}"></script>

<!-- Bootstrap JS -->
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    
<script type="text/javascript">

  function ajaxEndereco(cep){

     /* Configura a requisição AJAX */

     $.ajax({
          url : 'http://cep.republicavirtual.com.br/web_cep.php?cep='+$(cep).val()+'&formato=jsonp',
          dataType: 'json', /* Tipo de transmissão */
         
          success: function(data){ // se deu certo

              if(data.resultado == 1){

                $('#msg_erro_cep').hide(); // esconde div de erro no cep

                $(cep).css({border:'1px solid #00ff00'}); // css do cep encontrado na borda
                
                  $('#endereco').val(data.tipo_logradouro+" "+data.logradouro); // monta valor do endereço com logradouro
                  $('#bairro').val(data.bairro); // monta valor do bairro
                  
                 $.ajax({

                url : "{{ url('getcidade')}}",
                type : 'POST', /* Tipo da requisição */ 
                data:  { cidade: data.cidade, estado: data.uf }, /* dado que será enviado via POST */
                dataType: 'json', /* Tipo de transmissão */

                success: function(cidade){

                     jQuery('#estado option[value='+cidade.estado[0].id+']').attr('selected',true).change(); 
                     jQuery('#cidade option[value='+cidade.cidade[0].id+']').attr('selected', true)

                      $('#numero').focus(); // focus no campo do numero
                }

            });  

              }else{

                $('#msg_erro_cep').show();
                $(cep).css({border:'1px solid #ff0000'});

              }
          }
     });  
  }

// AJAX CIDADE E ESTADOS VIA CEP
jQuery(document).ready(function($){
    $('#estado').change(function(){
      jQuery.ajax({
      url: "{{ url('api/dropdown') }}",
      async: false,
      data: { option: this.value },
      success: function(data){
                var model = $('#cidade');
                model.empty();
                $.each(data, function(index, element) {
                    model.append("<option value='"+ element.id +"'>" + element.nome + "</option>");
                }); // foreach
            }  // function data
        }); //jquery ajax
    }); // estado on change
}); // jquery document ready

  </script>

<script type="text/javascript">

$(document).ready(function() { // o jQuery precisa estar carregado para obter os dados do form...
    // insira sua encryption_key, disponível no dashboard
    PagarMe.encryption_key = "ek_live_yXHnthpssq3cNKVZdg7ZyMHqjLHrZc";

    var form = $("#payment_form");

    form.submit(function(event) { // quando o form for enviado...
        // inicializa um objeto de cartão de crédito e completa
        // com os dados do form
        var creditCard = new PagarMe.creditCard();
  creditCard.cardHolderName = $("#payment_form #card_holder_name").val();
  creditCard.cardExpirationMonth = $("#payment_form #card_expiration_month").val();
  creditCard.cardExpirationYear = $("#payment_form #card_expiration_year").val();
  creditCard.cardNumber = $("#payment_form #card_number").val();
  creditCard.cardCVV = $("#payment_form #card_cvv").val();

        // pega os erros de validação nos campos do form
        // var fieldErrors = creditCard.fieldErrors();

  //Verifica se há erros
    //     var hasErrors = false;
    //     for(var field in fieldErrors) { hasErrors = true; break; }

    // if(hasErrors) {
    //   alert(fieldErrors);
    // } else {
            // se não há erros, gera o card_hash...
              creditCard.generateHash(function(cardHash) {
      // ...coloca-o no form...
                form.append($('<input type="hidden" name="card_hash">').val(cardHash));
      // ...remove os campos com os dados de cartão do form...
      PagarMe.removeCardFieldsFromForm(form);
      // e envia o form
            form.get(0).submit();
          });
    // }

        return false;
    });
});

</script>


    
    <!-- MENU CSS -->
    <link href="{{ URL::asset('css/menu.css') }}" rel="stylesheet">

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" href="{{ URL::asset('css/settings.css') }}" media="screen" />
    
    <!-- Flex slider -->
    <link href="{{ URL::asset('css/flexslider.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/owl.carousel.css') }}" rel="stylesheet"> 
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">   
    
    <!-- Custom CSS -->
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">

    <!-- Stylesheet for Color -->
    <link href="{{ URL::asset('css/hatsu.css') }}" rel="stylesheet"> 
    
    <!--css do menu -->
    <link href="{{ URL::asset('css/menu.css') }}" rel="stylesheet"> 

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ URL::asset('img/favicon/favicon.png') }}">

@show

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47664900-1', 'hatsu.com.br');
  ga('send', 'pageview');

</script>



</head>

<body>


    <!-- STYLIST MODAL STARTS -->
    <div id="stylist" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 style="font-family:roboto-regular;font-size:16px;font-weight:normal;">Preencha seus dados de contato</h4>
          </div>
          <div class="modal-body">
            <div class="form">
              <form class="form-horizontal" action="{{ URL::to('guiadecompra/store') }}" method="POST">   
                <div class="form-group">
                <label class="control-label col-md-2" for="nome">*Nome</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" id="nome" name="nome" placeholder="Campo obrigatório">
                </div>
                </div>

                <div class="form-group">
                <label class="control-label col-md-2" for="email">*E-mail</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" id="email" name="email" placeholder="Campo obrigatório">
                </div>
                </div>

                <div class="form-group">
                <label class="control-label col-md-2" for="social">*Rede social</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" id="social" name="social" placeholder="Informe o link de seu perfil na rede social de sua preferência">
                </div>
                </div>

                <p><input type="checkbox" checked="checked">Eu concordo em ter meu perfil na rede social informada visualizado pelo Personal Stylist</p>

                <br />

                <input type="hidden" value="{{ URL::current() }}" name="url_redirect">
                
                <div class="form-group">
                <div class="col-md-7 col-md-offset-3">
                <button type="submit" class="btn btn-default">SOLICITAR PERSONAL STYLIST</button>
                </div>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
    <!--/ Login modal ends -->


    <!-- voucher Modal starts -->
    <div id="voucher" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4>INFORME O CÓDIGO</h4>
          </div>
          <div class="modal-body">
            <div class="form">
              <form class="form-horizontal" action="{{ URL::to('voucher/store') }}" method="POST">   
                <div class="form-group">
                @if($errors->has('codigo')>0) <p class='error' style='text-align:center;font-size:14px;'> {{ $errors->first('codigo') }} </p> @endif  
                <label class="control-label col-md-3" for="codigo">Código:</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" id="codigo" name="codigo">
                </div>
                </div>
                <input type="hidden" value="{{ URL::current() }}" name="url_redirect">
                
                <div class="form-group">
                <div class="col-md-7 col-md-offset-3">
                <button type="submit" class="btn btn-default">APLICAR DESCONTO</button>
                </div>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
    <!--/ voucher modal ends -->


    <!-- AJUDA MODAL STARTS -->
<div id="ajuda" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:990px;">
    <div class="modal-content">

<div id="processo" class="modal-container-leo">

  <h2>PROCESSO DE COMPRA</h2>

  <div class="row-leo">

    <div class="col-md-3" style="border-right:0;">
    <h3>1º PASSO</h3>

    <p><span class="sub">Escolha sua armação</span><br />
    Ao encontrar a armação que mais combina com suas necessidades e estilo, clique em “Escolher” para comprá-la online ou selecione “Experimente em casa” para requisitar o serviço (<a href="{{ URL::to('experimente')}}" target="_blank">Saiba mais</a>)
    </p>

    <br />

    <img src="{{ URL::asset('img/processo/passo-1.png') }}" title="Escolha sua armação" alt="Escolha sua armação" class="img-responsive" />

    </div>

    <div class="col-md-3" style="border-right:0;">
    <h3>2º PASSO</h3>

    <p><span class="sub">Selecione suas lentes</span><br />
    <p>Escolha qual lente cobre suas necessidades:</p><br />

    <img src="{{ URL::asset('img/processo/passo-2.png') }}" title="Escolha sua armação" alt="Escolha sua armação" class="img-responsive" />

    <br />

    <ul>

    <li>Lentes “Sem Grau” caso você queira usar nossos óculos como acessório com lentes sem correções, ou caso queira fazer as lentes em outra óptica;</li>
    <li>Lentes “Visão Simples” para correção de miopia ou hipermetropia, e astigmatismo ou presbiopia (vista cansada);</li>
    <li>Lentes “Multifocais” para correção simultânea de miopia e presbiopia, ou miopia e presbiopia mais astigmatismo, ou lentes planas (sem grau para longe) e presbiopia.</li>

    </ul>

    </div>

    <div class="col-md-3">
    <h3>3º PASSO</h3>

    <p><span class="sub">Escolha o tipo de sua lente</span><br />
    Certos graus de lentes necessitam materiais especiais para que as lentes não fiquem tão grossas.</p><br />

    <img src="{{ URL::asset('img/processo/passo-3.png') }}" title="Escolha sua armação" alt="Escolha sua armação" class="img-responsive" />

    <br />

    <ul>

    <li>Lentes “normais” são para graus não tão acentuados;</li>
    <li>Já as lentes “Finas resistentes” são para graus mais avançados como de 2 a 4 para ametropias como miopia e hipermetropia;</li>
    <li>As lentes “Finíssimas” são feitas de material que permite graus bem acentuados, como 10 graus de miopia, continuem com lentes finas.</li>

    </ul>
    <br />
    <p style="color:#77a302;">Todas lentes possuem antirreflexo, proteção UVA e UVB e proteção antirrisco inclusa.</p>
    </div>

    <div class="col-md-3" style="border-left:0;">
    
    <h3>4º PASSO</h3>

    <p><span class="sub">Envie sua receita</span><br />

    Depois de efetuar a compra, tire uma foto ou escaneie sua receita oftalmológica e nós envie através do email receita@hatsu.com.br ou envie na seção “Minha conta” para que possamos fabricar suas lentes com perfeição</p>

    <br />

    <img src="{{ URL::asset('img/processo/receita.png') }}" title="Escolha sua armação" alt="Escolha sua armação" class="img-responsive" />

    </div>

  </div> 

  <div class="row-leo" style="margin-top:5px;text-align:center;">

  <div class="col-md-12" style="background:#f1f1f1;">

  <p style="font-size:16px;padding:5px;">Continua com dúvidas? Acesse nossa página de <a href="{{ URL::to('duvidas') }}" style="color:#77a302">dúvidas</a> para obter ajuda</p>

  </div>

  </div>

</div>
    </div>
  </div>
</div>
    <!--/ Login modal ends -->

    <!-- ESGOTADO MODAL STARTS -->
    <div id="avise" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 style="font-family:roboto-regular;font-size:16px;font-weight:normal;">Assim que nosso produto voltar ao estoque, você será informado.</h4>
          </div>
          <div class="modal-body">
            <div class="form">
              <form class="form-horizontal" action="{{ URL::to('produtos/store') }}" method="POST">   
                <div class="form-group">
                <label class="control-label col-md-3" for="nome">*Nome</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" id="nome" name="nome" placeholder="Campo obrigatório">
                </div>
                </div>

                <div class="form-group">
                <label class="control-label col-md-3" for="email">*E-mail</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" id="email" name="email" placeholder="Campo obrigatório">
                </div>
                </div>

                <input type="hidden" value="{{ URL::current() }}" name="url_redirect">
                
                <div class="form-group">
                <div class="col-md-7 col-md-offset-3">
                <button type="submit" class="btn btn-default">ENVIAR</button>
                </div>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
    <!--/ Login modal ends -->

    <!-- Login Modal starts -->
    <div id="login" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4>Acessar Minha Conta</h4>
          </div>
          <div class="modal-body">
            <div class="form">
              <form class="form-horizontal" action="{{ URL::to('cliente/login') }}" method="POST">   
                <div class="form-group">
                <label class="control-label col-md-3" for="username">E-mail</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" id="email" name="email">
                </div>
                </div>
                <div class="form-group">
                <label class="control-label col-md-3" for="email">Senha</label>
                <div class="col-md-7">
                  <input type="password" class="form-control" id="password" name="password">
                </div>
                </div>
                <input type="hidden" value="{{ URL::current() }}" name="url_redirect">
                
                <div class="form-group">
                <div class="col-md-7 col-md-offset-3">
                <button type="submit" class="btn btn-default">ENTRAR</button>
                </div>
                </div>
              </form>
            </div> 
          </div>
          <div class="modal-footer">
            <p>Não é registrado? <a href="{{ URL::to('/cliente') }}">Clique aqui</a></p>
          </div>
        </div>
      </div>
    </div>
    <!--/ Login modal ends -->

@include('novo-menu')

@show

@yield('body')

    <!-- Javascript files -->
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script src="{{ URL::asset('js/jquery.themepunch.plugins.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.themepunch.revolution.min.js') }}"></script> 
    <script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script> 
    <script src="{{ URL::asset('js/filter.js') }}"></script> 
    <!-- Flex slider -->
    <script src="{{ URL::asset('js/jquery.flexslider-min.js') }}"></script> 
    <!-- Respond JS for IE8 -->
    <script src="{{ URL::asset('js/respond.min.js') }}"></script>
    <!-- HTML5 Support for IE -->
    <script src="{{ URL::asset('js/html5shiv.js') }}"></script>
    <!-- Custom JS -->
    <script src="{{ URL::asset('js/custom.js') }}"></script>
    <!-- menu -->
    
    <script>
      // Revolution Slider
      var revapi;
      jQuery(document).ready(function() {
           revapi = jQuery('.tp-banner').revolution(
          {
            delay: 6000,
            startwidth: 1170,
            startheight: 500,
            hideThumbs: 200,
            shadow: 0,
            navigationType: "none",
            hideThumbsOnMobile: "on",
            hideArrowsOnMobile: "on",
            hideThumbsUnderResoluition: 0,
            touchenabled: "on",
            fullWidth: "off"
          });
      }); 
    </script>

@yield('footer')

@include('footer')

    <!-- Scroll to top -->
    <span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span> 

<!-- Código do Google para tag de remarketing -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 977863673;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;background:#000;">
<img height="1" width="1" style="border-style:none;background:#000;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/977863673/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>

</html>
