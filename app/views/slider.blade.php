<!-- Slider starts -->
<div class="tp-banner-container">
<div class="tp-banner">
	<ul>	<!-- SLIDE  -->	

		<li data-transition="parallaxtoright" data-slotamount="4" data-masterspeed="700" style="background-color:#fedbc7;" >
			<!-- MAIN IMAGE -->
			<!-- <img src="{{ URL::asset('img/home/slider/slide-back.jpg') }}"  alt="" data-duration="2000" />	 -->
			<!-- LAYER NR. 4 -->
			<div class="tp-caption"
				data-x="-135" data-hoffset="0"
				data-y="0"
				data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
				data-speed="600"
				data-start="1300"
				data-easing="Power4.easeOut"
				data-endspeed="300"
				data-endeasing="Power1.easeIn"
				data-captionhidden="on"
				style="z-index: 2"><a href="{{ URL::to('kizo') }}"><img src="img/home/slider/new_kizo.jpg" alt="Conheça a Linha Kizo" /></a>
			</div>
		</li>

		<li data-transition="parallaxtoright" data-slotamount="4" data-masterspeed="700" style="background-color:#06080d;" >
			<!-- MAIN IMAGE -->
			<!-- <img src="{{ URL::asset('img/home/slider/slide-back.jpg') }}"  alt="" data-duration="2000" />	 -->
			<!-- LAYER NR. 4 -->
			<div class="tp-caption"
				data-x="-135" data-hoffset="0"
				data-y="0"
				data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
				data-speed="600"
				data-start="1300"
				data-easing="Power4.easeOut"
				data-endspeed="300"
				data-endeasing="Power1.easeIn"
				data-captionhidden="on"
				style="z-index: 2"><a href="{{ URL::to('experimente') }}"><img src="img/home/slider/Banner-Experimente.jpg" alt="Experimente Hatsu" /></a>
			</div>
		</li>

		<li data-transition="parallaxtoright" data-slotamount="4" data-masterspeed="700" style="background-color:#0a0b10;" >
			<!-- MAIN IMAGE -->
			<!-- <img src="{{ URL::asset('img/home/slider/slide-back.jpg') }}"  alt="" data-duration="2000" />	 -->
			<!-- LAYER NR. 4 -->
			<div class="tp-caption"
				data-x="-135" data-hoffset="0"
				data-y="0"
				data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
				data-speed="600"
				data-start="1300"
				data-easing="Power4.easeOut"
				data-endspeed="300"
				data-endeasing="Power1.easeIn"
				data-captionhidden="on"
				style="z-index: 2"><a href="{{ URL::to('projection') }}"><img src="img/home/slider/new_projection.jpg" alt="Conheça a Linha Projection" /></a>
			</div>

	</ul>
</div>
</div>