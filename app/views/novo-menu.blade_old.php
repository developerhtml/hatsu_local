<div class="navbar bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button> 
    </div>

    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">

      <ul class="nav navbar-nav">

        <div class="logo" style="float:left;margin-right:60px;">
            <a href="{{ URL::to('/') }}"><img src="{{ URL::asset('img/logo-hatsu.png') }}" style="width:97px;height:49px;" /></a>
        </div>
        
        <li><a href="#" id="exibe-menu" class="show-menu" title="Conheça nossa linha de produtos">PRODUTOS <img src="{{ URL::asset('./img/menu/blue_arrow_down.gif')}}" style="vertical-align:middle;border:0;padding-top:2px;" class="arrow-down-produtos" /><img src="{{ URL::asset('./img/menu/blue_arrow_up.gif')}}" style="vertical-align:middle;border:0;padding-bottom:4px;display:none;" class="arrow-up-produtos" /></a></li>
        <li><a href="#" id="exibe-sobre" class="show-sobre" title="Conheça a Hatsu">A HATSU <img src="{{ URL::asset('./img/menu/blue_arrow_down.gif')}}" style="vertical-align:middle;border:0;padding-top:2px;" class="arrow-down-sobre" /><img src="{{ URL::asset('./img/menu/blue_arrow_up.gif')}}" style="vertical-align:middle;border:0;padding-bottom:4px;display:none;" class="arrow-up-sobre" /></a></li>
        <li class="mobile-sobre" style="display:none;"><a href="{{ URL::to('sobre') }}" title="Conheça a Hatsu">SOBRE A HATSU</a></li>
        <li class="mobile-sobre" style="display:none;"><a href="{{ URL::to('guiadecompra') }}" title="Encontre o Hatsu ideal para você">GUIA DE COMPRA</a></li>
        <li class="mobile-sobre" style="display:none;"><a href="{{ URL::to('duvidas') }}" title="Tire as principais dúvidas sobre a Hatsu">DÚVIDAS</a></li>
        <li class="mobile-produtos" style="display:none;"><a href="{{ URL::to('produtos') }}" title="Conheça nossaEM  linha de produtos">PRODUTOS</a></li>
        <li><a href="{{ URL::to('experimente') }}" title="Conheça o serviço Experimente Hatsu">EXPERIMENTE HATSU</a></li>
        <li style="border-right:1px solid #ccc;padding-right:10px;"><a href="{{ URL::to('contato') }}" title="Entre em contato conosco">CONTATO</a></li>

    
        @if(Auth::check())

        <li style="padding-left:10px;padding-right:10px;"><a href="{{ URL::to('cliente')}}" title="Clique para acessar sua conta">MINHA CONTA</a>

        @else 

        <li style="padding-left:10px;"><a href="#login" role="button" data-toggle="modal" style="color:#000;">LOGIN</a></li>
        <li style="padding-right:10px;"><a href="{{ URL::to('cliente') }}" title="Entre em contato conosco">REGISTRAR</a></li>

        @endif

        {{ Helper::topCarrinho() }}

      </ul>
    </nav>

  </div>
</div>

{{ Helper:: novoMenu() }}

<div id="menu-sobre">

    <div id="item-menu-sobre">

        <a href="{{ URL::to('sobre')}}" data-toggle="tooltip" title="Conheça a Hatsu">

            <img src="{{ URL::asset('./img/menu/sobreahatsu.png')}}" style="margin-bottom:10px;" />

            <p class="menu-sobre">SOBRE A HATSU</p>

        </a>

    </div>

    <div id="item-menu-sobre">

        <a href="{{ URL::to('duvidas')}}" data-toggle="tooltip" title="Tire suas dúvidas">

            <img src="{{ URL::asset('./img/menu/duvidas.png')}}" style="margin-bottom:10px;" />

            <p class="menu-sobre">DÚVIDAS</p>

        </a>

    </div>

    <div id="item-menu-sobre">

        <a href="{{ URL::to('guiadecompra')}}" data-toggle="tooltip" title="Encontre o Hatsu ideal para você!">

            <img src="{{ URL::asset('./img/menu/guia.png')}}" style="margin-bottom:10px;" />

            <p class="menu-sobre">GUIA DE COMPRA</p>

        </a>

    </div>

    <div id="item-menu-sobre">

        <a href="{{ URL::to('garantia')}}" data-toggle="tooltip" title="Conheça as garantias Hatsu">

            <img src="{{ URL::asset('./img/menu/garantia.png')}}" style="margin-bottom:10px;" />

            <p class="menu-sobre">GARANTIA</p>

        </a>

    </div>

    <div id="item-menu-sobre" style="height:108px;">

        <a href="http://blog.hatsu.com.br/" data-toggle="tooltip" title="Visite nosso blog" target="_blank">

            <img src="{{ URL::asset('./img/menu/blog.png')}}" style="margin-bottom:10px;" />

            <p class="menu-sobre">BLOG</p>

        </a>

    </div>        

    <div id="item-menu-sobre" style="height:108px;">

        <a href="http://wiki.hatsu.com.br/" data-toggle="tooltip" title="Visite nossa Wiki" target="_blank">

            <img src="{{ URL::asset('./img/menu/wiki.png')}}" style="margin-bottom:10px;" />

            <p class="menu-sobre">WIKI</p>

        </a>

    </div> 

</div>
