@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">          
            
            <div class="widget ">
              
              <div class="widget-header">
                <i class="icon-group"></i>
                <h3>Editando Cliente {{ $cliente->nome }} </h3>
            </div> <!-- /widget-header -->
          
          <div class="widget-content">
            
            <div class="tabbable">

         <form id="edit-profile" class="form-horizontal" action="{{ URL::to('cliente/update/'.$cliente->id_cliente) }}" method="POST" name="form-clientes">

         <fieldset>

          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3>Dados Pessoais</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div id='calendar' style='padding-top:15px;'>            

                <div class="control-group">  
 
                      <label class="control-label" @if($errors->has('tipo')>0) style='color:#ff0000' @endif >Tipo:</label>

                      <div class="controls">
                      <label class="radio inline">
                        <input type="radio" name="tipo" onchange="document.getElementById('div1').style.display='block';document.getElementById('div2').style.display='none'" class="radio1" value="P" @if(Input::old('tipo') == "P") {{ 'checked="checked"' }} @elseif($cliente->tipo == "P") {{ 'checked="checked"' }} @endif > Pessoa Física
                      </label>
                      
                      <label class="radio inline">
                        <input type="radio" name="tipo" onchange=" document.getElementById('div2').style.display='block';document.getElementById('div1').style.display='none'" class="radio2" value="J" @if(Input::old('tipo') == "J") {{ 'checked="checked"' }} @elseif($cliente->tipo == "J") {{ 'checked="checked"' }} @endif > Pessoa Jurídica
                      </label>

                      @if($errors->has('tipo')>0) <p class="error"> Selecione uma das opções acima  </p> @endif

                    </div>  <!-- /controls -->      
                    </div> <!-- /control-group -->

                    <div id="div1" @if(Input::old('tipo') && Input::old('tipo') == "P") style='display:block' @elseif($cliente->tipo == "P") style='display:block' @endif >

                    <div class="control-group">                     
                      <label class="control-label" for="cpf">CPF</label>
                      <div class="controls">
                        <input type="text" class="span4" id="cpf" @if($errors->has('cpf')>0) style='border-color:#ff0000;' @endif name="cpf" @if((Input::old('cpf'))) value="{{ Input::old('cpf') }}" @else value="{{ $cliente->cpf }}" @endif ">

                        @if($errors->has('cpf')>0) <p class='error'> {{ $errors->first('cpf') }} </p> @endif

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="name">Nome Completo</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('name')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" @if((Input::old('nome'))) value="{{ Input::old('nome') }}" @else value="{{ $cliente->nome }}" @endif >
                        @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
                    </div>


                    <div id="div2" @if((Input::old('tipo')) && Input::old('tipo') == "J")) style='display:block' @elseif($cliente->tipo == "J") style='display:block' @endif>

                    <div class="control-group">                     
                      <label class="control-label" for="cnpj">CNPJ</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('cnpj')>0) style='border-color:#ff0000;' @endif id="cnpj" name="cnpj" @if((Input::old('cnpj'))) value="{{ Input::old('cnpj') }}" @else value="{{ $cliente->cnpj }}" @endif >
                        @if($errors->has('cnpj')>0) <p class='error'> {{ $errors->first('cnpj') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="name">Razão Social</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('razao_social')>0) style='border-color:#ff0000;' @endif id="razao_social" name="razao_social" @if((Input::old('razao_social'))) value="{{ Input::old('razao_social') }}" @else value="{{ $cliente->razao_social }}" @endif >
                        @if($errors->has('razao_social')>0) <p class='error'> {{ $errors->first('razao_social') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    </div>

                    <div class="control-group">                     
                      <label class="control-label" for="email">Email</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" @if((Input::old('email'))) value="{{ Input::old('email') }}" @else value="{{ $cliente->email }}" @endif >
                        @if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="telefone">Telefone</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('telefone')>0) style='border-color:#ff0000;' @endif id="telefone" name="telefone" @if((Input::old('telefone'))) value="{{ Input::old('telefone') }}" @else value="{{ $cliente->telefone }}" @endif >
                        @if($errors->has('telefone')>0) <p class='error'> {{ $errors->first('telefone') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="celular">Celular</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('celular')>0) style='border-color:#ff0000;' @endif id="celular" name="celular" @if((Input::old('celular'))) value="{{ Input::old('celular') }}" @else value="{{ $cliente->celular }}" @endif >
                        @if($errors->has('celular')>0) <p class='error'> {{ $errors->first('celular') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


              </div>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->

            
        <!-- /widget -->
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-home"></i>
              <h3>Endereço</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div id='calendar' style='padding-top:15px;'>   

                    <div class="control-group">                     
                      <label class="control-label" for="cep">CEP</label>
                      <div class="controls">
                        <input type="text" onblur="return ajaxEndereco(this);" class="span4" @if($errors->has('cep')>0) style='border-color:#ff0000;' @endif id="cep" name="cep" @if((Input::old('cep'))) value="{{ Input::old('cep') }}" @else value="{{ $cliente->cep }}" @endif >
                        @if($errors->has('cep')>0) <p class='error'> {{ $errors->first('cep') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->  

                    <div class="control-group">                     
                      <label class="control-label" for="estado">Estado</label>
                      <div class="controls">
                        <select class="span4" id="estado" name="estado" @if($errors->has('estado')>0) style='border-color:#ff0000;' @endif>
                          <option value="">- Selecione -</option>
                          @foreach(Estado::all() as $estado)
                              <option @if((Input::old('estado')) && Input::old('estado') == $estado->id) {{ 'selected="selected"' }} @elseif($cliente->uf == $estado->id) {{ 'selected="selected"' }} @endif value="{{ $estado->id }}">{{ $estado->nome }}</option>
                          @endforeach
                        </select>
                        @if($errors->has('estado')>0) <p class='error'> {{ $errors->first('estado') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="cidade">Cidade</label>
                      <div class="controls">
                        <select class="span4" id="cidade" name="cidade" @if($errors->has('cidade')>0) style='border-color:#ff0000;' @endif>
                          @if (Input::has('cidade')) 
                            @foreach(Cidade::where('uf','=',Cidade::find(Input::get('cidade'))->uf)->get() as $cidade)
                              <option value='{{ $cidade->id }}' @if($cidade->id == Input::get('cidade')) selected='selected' @endif >{{ $cidade->nome }}</option>
                            @endforeach
                          @elseif (!empty($cliente->cidade))
                            @foreach(Cidade::where('uf','=',Cidade::find($cliente->cidade)->uf)->get() as $cidade)
                              <option value='{{ $cidade->id }}' @if($cidade->id == $cliente->cidade) selected='selected' @endif >{{ $cidade->nome }}</option>
                            @endforeach
                          @else
                            <option value="">- Selecione -</option>
                          @endif
                        </select>
                        @if($errors->has('cidade')>0) <p class='error'> {{ $errors->first('cidade') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->  

                    <div class="control-group">                     
                      <label class="control-label" for="bairro">Bairro</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('bairro')>0) style='border-color:#ff0000;' @endif id="bairro" name="bairro" @if((Input::old('bairro'))) value="{{ Input::old('bairro') }}" @else value="{{ $cliente->bairro }}" @endif >
                        @if($errors->has('bairro')>0) <p class='error'> {{ $errors->first('bairro') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->       

                    <div class="control-group">                     
                      <label class="control-label" for="endereco">Endereço</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('endereco')>0) style='border-color:#ff0000;' @endif id="endereco" name="endereco" @if((Input::old('endereco'))) value="{{ Input::old('endereco') }}" @else value="{{ $cliente->endereco }}" @endif >
                        @if($errors->has('endereco')>0) <p class='error'> {{ $errors->first('endereco') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="numero">Número</label>
                      <div class="controls">
                        <input type="text" onkeyup="checkInput(this)" maxlength="6" class="span4" @if($errors->has('numero')>0) style='border-color:#ff0000;' @endif id="numero" name="numero" @if((Input::old('numero'))) value="{{ Input::old('numero') }}" @else value="{{ $cliente->numero }}" @endif >
                        @if($errors->has('numero')>0) <p class='error'> {{ $errors->first('numero') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="complemento">Complemento</label>
                      <div class="controls">
                        <input type="text" class="span4" id="complemento" name="complemento" @if((Input::old('complemento'))) value="{{ Input::old('complemento') }}" @else value="{{ $cliente->complemento }}" @endif >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

              </div>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->


                    <div class="form-actions">
                      
                      <button type="submit" class="btn btn-primary">Editar</button> 
                      
                      <a href="{{ URL::to('cliente') }}"><button class="btn" type="button">Retornar</button></a>

                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>
              
            </div>
            
          </div> <!-- /widget-content -->
            
        </div> <!-- /widget -->
            
        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
@stop