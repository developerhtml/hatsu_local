@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">          
            
            <div class="widget ">
              
              <div class="widget-header">
                <i class="icon-group"></i>
                <h3>Editando {{ $produto->nome }} </h3>
            </div> <!-- /widget-header -->
          
          <div class="widget-content">
            
            <div class="tabbable">

           <form id="edit-profile" class="form-horizontal" action="{{ URL::to('projection/update/'.$produto->id_cliente) }}" method="POST" name="form-clientes">

           <fieldset>

                    <div class="control-group">                     
                      <label class="control-label" for="name">Nome</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('name')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" @if((Input::old('nome'))) value="{{ Input::old('nome') }}" @else value="{{ $produto->nome }}" @endif >
                        @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="descricao_menu">Texto do Menu</label>
                      <div class="controls">
                        <textarea class="span4" @if($errors->has('descricao_menu')>0) style='border-color:#ff0000;' @endif id="descricao_menu" name="descricao" @if((Input::old('descricao'))) {{ Input::old('descricao_menu') }} @else {{ $produto->descricao_menu }} @endif ></textarea>
                        @if($errors->has('descricao_menu')>0) <p class='error'> {{ $errors->first('descricao_menu') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="name">Descrição</label>
                      <div class="controls">
                        <textarea class="span4" @if($errors->has('name')>0) style='border-color:#ff0000;' @endif id="descricao" name="descricao" @if((Input::old('descricao'))) {{ Input::old('descricao') }} @else {{ $produto->descricao }} @endif ></textarea>
                        @if($errors->has('descricao')>0) <p class='error'> {{ $errors->first('descricao') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                  <div class="control-group">                     
                      <label class="control-label" for="name">Meta Title</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('name')>0) style='border-color:#ff0000;' @endif id="meta_title" name="meta_title" @if((Input::old('meta_title'))) value="{{ Input::old('meta_title') }}" @else value="{{ $produto->meta_title }}" @endif >
                        @if($errors->has('meta_title')>0) <p class='error'> {{ $errors->first('meta_title') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="name">Meta Description</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('name')>0) style='border-color:#ff0000;' @endif id="meta_description" name="meta_description" @if((Input::old('meta_description'))) value="{{ Input::old('meta_description') }}" @else value="{{ $produto->meta_description }}" @endif >
                        @if($errors->has('meta_description')>0) <p class='error'> {{ $errors->first('meta_description') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                     <div class="control-group">                     
                      <label class="control-label" for="name">Meta Keywords</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('name')>0) style='border-color:#ff0000;' @endif id="meta_keywords" name="meta_keywords" @if((Input::old('meta_keywords'))) value="{{ Input::old('meta_keywords') }}" @else value="{{ $produto->meta_keywords }}" @endif >
                        @if($errors->has('meta_keywords')>0) <p class='error'> {{ $errors->first('meta_keywords') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                  <div class="form-actions">
                    
                    <button type="submit" class="btn btn-primary">Editar</button> 
                    
                  </div> <!-- /form-actions -->
                </fieldset>
              </form>
              </div>
              
          </div>  
            
        </div> <!-- /widget-content -->
            
      </div> <!-- /widget -->
            
      </div> <!-- /span8 -->
          
      </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
@stop