@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Produtos | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">produtos</span>

</div>

</div>

<!-- Page content starts -->

<div class="content">

	<div class="container">

		<div class="row"> 

			<div class="col-md-12" style="text-align:right;">

			<h1 style="font-size:62px;font-weight:normal;color:#000;">PRODUTOS</h1>

			</div>

		</div>

		@if (Session::has('message'))

        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2><br /><Br />
        
      @endif


		{{ Helper::Produtos() }}

	</div>

</div>

<!-- Page content ends -->


@stop