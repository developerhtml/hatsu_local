@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Showroom | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">showroom</span>

</div>

</div>

<!-- Page content starts -->

<div class="content contact-two">

  <div class="container resume">

    <div class="row"> 
      
      <div class="col-md-12" style="text-align:right;">

      <h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;">SHOWROOM</h1>

      </div>


    <div class="content resume">
      <div class="container">
      <div class="row">
        <div class="col-md-12">

          <!-- <div class="bor"></div> -->
          <br style="clear:both;" />
          <div class="clearfix"></div>

          <div class="rblock">
            <div class="row">
             <div class="col-md-2" style="text-align:center;">
              <!-- title -->
              <h4 style="text-transform:uppercase;text-align:center;">O que é?</h4>                           
             </div>
             <div class="col-md-10">
              <div class="rinfo">
                 <br /><p> Para aqueles que gostam de conferir pessoalmente a qualidade dos produtos Hatsu, possuímos um serviço de showroom, onde expomos nossos produtos para que você experimente-os e confira a qualidade do design e dos materiais utilizados em nossos óculos.
Nos showrooms, temos consultores com informações sobre os óculos, tipos de lentes e quais óculos e lentes são mais adequados para seu rosto e hábitos.
Caso queira conhecer nossos óculos mas não temos nenhum showroom próximo à você, requisite o serviço de <a href="{{ URL::to('experimente') }}">Experimente em casa</a>. É grátis!
</p>
              </div>
             </div>
            </div>
           </div>

        <!--   <div class="rblock">
            <div class="row">
             <div class="col-md-2" style="text-align:center;">
              <h4 style="text-transform:uppercase;text-align:center;">onde estamos atualmente</h4>                           
             </div>
             <div class="col-md-10">
              <div class="rinfo">
                 <br /><p>Para aqueles que gostam de conferir pessoalmente a qualidade dos produtos Hatsu, possuímos um serviço de showroom, onde expomos nossos produtos para que você experimente-os e confira a qualidade do design e dos materiais utilizados em nossos óculos.<br />
Nos showrooms, temos consultores com informações sobre os óculos, tipos de lentes e quais óculos e lentes são mais adequados para seu rosto e hábitos.<br />
Caso queira conhecer nossos óculos mas não estamos com [??] nenhum showroom próximo à você, requisite o serviço de <a href="{{ URL::to('experimente') }}">Experimente em casa</a>. É grátis!
</p>
              </div>
             </div>
            </div>
           </div> -->

                     <div class="rblock">
            <div class="row">
             <div class="col-md-2" style="text-align:center;">
              <!-- title -->
              <h4 style="text-transform:uppercase;text-align:center;">SUGIRA UM LOCAL</h4>                           
             </div>
             <div class="col-md-10">
              <div class="rinfo">



      <p><b>Gostaria que estivéssemos mais próximo de você? Sugira um local!</b><br />
Envie sua sugestão de local e estudaremos a possiblidade de realizá-lo.
</p><br />

                 @if (Session::has('message'))
        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2>
      @endif

      <p style="text-align:center;">

        <div class="form" style="text-align:center;">
          <!-- Contact form (not working)-->
          <form class="form-horizontal" action="{{ URL::to('showroom/store') }}" method="post">
              
              @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif

              <!-- Name -->
              <div class="form-group">
                <label class="control-label col-md-2" for="name1">Nome</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('nome')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" value="{{ Input::old('nome') }}" placeholder="Informe seu nome completo">
                </div>
              </div>

              @if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
              <!-- Email -->
              <div class="form-group">
                <label class="control-label col-md-2" for="email1">E-mail</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" value="{{ Input::old('email') }}" placeholder="Informe seu e-mail">
                </div>
              </div>

              @if($errors->has('sugestao')>0) <p class='error'> {{ $errors->first('sugestao') }} </p> @endif

              <!-- Comment -->
              <div class="form-group">
                <label class="control-label col-md-2" for="sugestao">Sugestão</label>
                <div class="col-md-9">
                  <textarea class="form-control" id="sugestao" name="sugestao" rows="3" placeholder="Informe seu estado, cidade e bairro. Nosso atendimento entrará em contato o mais breve possível." @if($errors->has('sugestao')>0) style='border-color:#ff0000;' @endif>{{ Input::old('sugestao') }}</textarea>
                </div>
              </div>
              <!-- Buttons -->
              <div class="form-group">
                 <!-- Buttons -->
         <div class="col-md-9 col-md-offset-2">
          <button type="submit" class="btn btn-success" style="background-color:#77A302;text-transform:uppercase;width:200px;">Enviar</button>
         </div>
          
          </div>

          </form>

              </div>

              </p>

             </div>
      </div>
    </div>

</div>

</div>

</div>



</div>

</div>

</div>

</div>

</div>

</div>
<!-- Page content ends -->


@stop