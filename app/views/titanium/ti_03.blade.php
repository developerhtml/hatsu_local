@extends('template')

@section('seo')

@foreach($dados as $produto)

<!-- Tags de SEO personalizadas -->
<title>{{ $produto->modelo}} - {{ $produto->cor }} | Linha Titanium | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')


<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">INÍCIO</a>&nbsp;|&nbsp;<a href="{{ URL::to('/produtos') }}" class="inativo">produtos</a>&nbsp;|&nbsp;<a href="{{ URL::to('/titanium') }}" class="inativo">titanium</a>&nbsp;|&nbsp;<span class="ativo">TI_03 - {{ $produto->cor }}</span>

</div>

</div>

<!-- Page content starts -->

<div class="content">

	<div class="container" style="padding:0;">

		<h2 style="font-family:grotesque-ultralight;font-size:32px;">TITANIUM</h2>

		<div class="col-md-9" style="padding:0;">
              
              	{{ Helper::Galeria($produto->sku) }}
              	
             

			<div class="col-md-12" style="padding:2px">

				<h4 id="h4">CORES</h4>

				{{ Helper:: ExibeCores(5,$produto->id,$produto->modelo) }}

			</div>

			

			<div class="col-md-12" style="padding:2px;margin-top:40px;" id="outros_modelos">

				<h4 id="h4">OUTROS MODELOS</h4>

				{{ Helper:: ExibeModelos(5,$produto->modelo) }}

			</div>


		<br style="clear:both;" />
		<br style="clear:both;" />

		

		<h4 id="h4">VISÃO 180</h4>

		<div id="faces" style="padding:0">

			<div id="face-area" style="padding:0">

				<div id="image-all" style="display:block;">
				<img src="{{ URL::asset('img/visao180/'.strtolower($produto->modelo).'-'.str_replace(" e ","_",strtolower($produto->cor)).'_03.jpg') }}" class="img-responsive" />
				</div>

				<div id="image-1" style="display:none;">
				<img src="{{ URL::asset('img/visao180/'.strtolower($produto->modelo).'-'.str_replace(" e ","_",strtolower($produto->cor)).'_01.jpg') }}" class="img-responsive" />
				</div>

				<div id="image-2" style="display:none;">
				<img src="{{ URL::asset('img/visao180/'.strtolower($produto->modelo).'-'.str_replace(" e ","_",strtolower($produto->cor)).'_02.jpg') }}" class="img-responsive" />
				</div>

				<div id="image-3" style="display:none;">
				<img src="{{ URL::asset('img/visao180/'.strtolower($produto->modelo).'-'.str_replace(" e ","_",strtolower($produto->cor)).'_03.jpg') }}" class="img-responsive" />
				</div>

				<div id="image-4" style="display:none;">
				<img src="{{ URL::asset('img/visao180/'.strtolower($produto->modelo).'-'.str_replace(" e ","_",strtolower($produto->cor)).'_04.jpg') }}" class="img-responsive" />
				</div>

				<div id="image-5" style="display:none;">
				<img src="{{ URL::asset('img/visao180/'.strtolower($produto->modelo).'-'.str_replace(" e ","_",strtolower($produto->cor)).'_05.jpg') }}" class="img-responsive" />
				</div>

				<div id="the_faces_overlay">
					<div class="the_faces" data-number="1"></div>
					<div class="the_faces" data-number="2"></div>
					<div class="the_faces" data-number="3"></div>
					<div class="the_faces" data-number="4"></div>
					<div class="the_faces" data-number="5"></div>
				</div>  
			</div>  

			<!-- </div> -->

		</div>  

	</div> <!-- fecha col 9 galeria de imagens -->



			<span id="aba_modelo">MODELO</span>
			<span id="aba_adulto">ADULTO</span>
			<!-- <span id="aba_sport">SPORT</span> -->
			<!-- <span id="aba_kids">KIDS</span> -->

			<div class="col-md-3" id="compra-produto">

				<p>

				<span id="escolha"></span>

				<br />

				<h3>{{ $produto->modelo }} - {{ $produto->cor }} </h3>
				 
				</p>

				<p style='text-align:center;padding:5px;'>Cortada a partir de uma chapa única de titânio, a linha Titanium possui resistência e leveza incomparável com as armações de acetato. O polimento gradual da armação garante leveza para as hastes e resistência para a armação e lentes.</p>

				<br />

				<p style="text-align:center;border-bottom:1px solid #ccc;padding:2px;">

				<span class="parcelas" style="color:#ff0000;">

				ESGOTADO

				</span>
				
				</p>
				<!-- 
				<p style="text-align:center;border-bottom:1px solid #ccc;padding:2px;">

				<span class="parcelas">

				<span style="font-size:20px;">10 x</span> 

				R$ {{ number_format($produto->preco/10,2,",",".") }}

				</span>

				</p>

				<p style="text-align:center;padding:2px;">

				<span class="preco">à vista R$</span>
				
				<span class="preco">{{ number_format($produto->preco,2,",",".") }}</span>
				
				</p> -->

				<form action="{{ URL::secure('checkout') }}" method="post">

				<input type="hidden" name="preco" value="{{ $produto->preco }}"><br />
				<input type="hidden" name="sku" value="{{ $produto->sku }}">
				<input type="hidden" name="linha" value="titanium">
				<input type="hidden" name="modelo" value="{{ $produto->modelo }}">
				<input type="hidden" name="thumb" value="{{ $produto->thumb }}">
				<input type="hidden" name="cor" value="{{ $produto->cor }}">
				<input type="hidden" name="url" value="{{ $produto->url }}">

				
@if (Session::has('sucesso'))

				<p style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('sucesso') }}</p>

				@endif

				@if (Session::has('erro'))

				<p style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</p>

				@endif

				<div id="exibe-escolher">

				<a class="botao-comprar" style="background:#1f1f1f;" href="#avise" role="button" data-toggle="modal" id="exibe-avise">AVISE-ME QUANDO CHEGAR</a>

				</div>

				@if($errors->has('lente')>0) <p class="error"> Selecione uma das opções abaixo  </p> @endif

				<div id="exibe-lente">

					<h4 class="step">1. SELECIONE A LENTE</h4>
					<input type="radio" name="lente" value="semgrau" id="lente1">
					<span class="texto-radio"><label for="lente1">SEM GRAU</label></span>
					&nbsp;<a href="#" data-toggle="tooltip" title="Caso você queira usar nossos óculos como acessório com lentes sem correções, ou caso queira fazer as lentes em outra óptica" class="compra-tooltip">?</a>

					<br />

					<input type="radio" name="lente" value="simples" id="lente2">
					<span class="texto-radio"><label for="lente2">VISÃO SIMPLES</label></span>
					&nbsp;<a href="#" data-toggle="tooltip" title="Para correção de miopia, hipermetropia, astigmatismo ou presbiopia (vista cansada), as lentes de visão simples corrigem miopia ou miopia e astigmatismo, hipermetropia ou hipermetropia e astigmatismo, presbiopia ou presbiopia e astigmatismo." class="compra-tooltip">?</a>
					<br />
					<span class="mini-texto">Antirreflexo/risco; proteção UVA/UVB</span>
					<br />

					<input type="radio" name="lente" value="multifocal" id="lente3">
					<span class="texto-radio"><label for="lente3">MULTIFOCAL <span style="font-size:11px;font-weight:normal;">a partir de R$ 550</span></label></span>
					&nbsp;<a href="#" data-toggle="tooltip" title="Para correção simultânea de miopia e presbiopia, ou miopia e presbiopia mais astigmatismo, ou lentes planas (sem grau para longe) e presbiopia." class="compra-tooltip">?</a>
					<br />
					<span class="mini-texto">Antirreflexo/risco; proteção UVA/UVB</span>

				</div>

				<div id="escolha-lente-semgrau">
				<h4 class="step">1. LENTE: Sem grau</h4>
				</div>

				<div id="escolha-lente-simples">
				<h4 class="step">1. LENTE: Visão simples</h4>
				</div>

				<div id="escolha-lente-multifocal">
				<h4 class="step">1. LENTE: Multifocal</h4>
				</div>

				<span id="editar-lente">editar lente</span>

				<div id="escolha-tipo-normais">
				<h4 class="step">2. ESPESSURA: Normal</h4>
				</div>

				<div id="escolha-tipo-finas">
				<h4 class="step">2. ESPESSURA: Fina resistente</h4>
				</div>

				<div id="escolha-tipo-finissimas">
				<h4 class="step">2. ESPESSURA: Finíssima</h4>
				</div>

				<div id="escolha-tipo-naosei">
				<h4 class="step">2. ESPESSURA: Não sei</h4>
				</div>

				<div id="escolha-tipo-plana">
				<h4 class="step">2. TIPO: Plana</h4>
				</div>

				<div id="escolha-tipo-demonstrativa">
				<h4 class="step">2. TIPO: Demonstrativa</h4>
				</div>

				<div id="exibe-tipo-simples">

					<h4 class="step">2. TIPO DE LENTE</h4>

					@if($errors->has('tipo')>0) <p class="error"> Selecione uma das opções abaixo  </p> @endif

					<input type="radio" name="tipo" value="normais" id="tipo1">
					<span class="texto-radio"><label for="tipo1">NORMAIS<span style="font-size:11px;font-weight:normal;">&nbsp;Lente Grátis!</span></label></span>
					&nbsp;<a href="#" data-toggle="tooltip" title="Para graus dióptricos pequenos, as lentes normais são suficientes para construção dos óculos" class="compra-tooltip">?</a>
					
					<div id="texto-lente-compra">

					<p>Material: CR-39</p>
					<p>Miopia: de 0 a -2 graus</p>
					<p>Hipermetropia: de 0 a +2 graus</p>
					<p>Astigmatismo: de 0 a -2 graus</p>

					</div>

					<input type="radio" name="tipo" value="finas" id="tipo2">
					<span class="texto-radio"><label for="tipo2">FINAS RESISTENTES <span style="font-size:11px;font-weight:normal;">R$ 100,00</span></label></span>
					&nbsp;<a href="#" data-toggle="tooltip" title="Para graus dióptricos mais elevados, as lentes finas possibilitam construção de lentes mais leves e finas" class="compra-tooltip">?</a>
					<br />
					
					<div id="texto-lente-compra">
					<p>Material: Trivex</p>
					<p>Miopia: de -2 a -4 graus</p>
					<p>Hipermetropia: de +2 a +4 graus</p>
					<p>Astigmatismo: de 0 a -4 graus</p>

					</div>


					<input type="radio" name="tipo" value="finissimas" id="tipo3">
					<span class="texto-radio"><label for="tipo3">FINÍSSIMAS <span style="font-size:11px;font-weight:normal;">R$ 200,00</span></label></span>
					&nbsp;<a href="#" data-toggle="tooltip" title="Caso o grau dióptrico seja muito elevado, as lentes finíssimas são essenciais para não termos lentes muito grossas e pesadas." class="compra-tooltip">?</a>
					
					<div id="texto-lente-compra">
					<p>Material: 1.61</p>
					<p>Miopia: de -4 a -10 graus</p>
					<p>Astigmatismo: de 0 a -4 graus</p>
					</div>


					<input type="radio" name="tipo" value="naosei" id="tipo4">
					<span class="texto-radio"><label for="tipo4">NÃO SEI</label></span>
					&nbsp;<a href="#" data-toggle="tooltip" title="Caso você não saiba qual lente mais adequada para seu grau, confeccionaremos sua armação com lentes normais." class="compra-tooltip">?</a>
					<br />

				</div>

				<div id="exibe-tipo-semgrau">

					<h4 class="step">2. TIPO DE LENTE</h4>

					@if($errors->has('tipo')>0) <p class="error"> Selecione uma das opções abaixo  </p> @endif

					<input type="radio" name="tipo" value="plana" id="tipo5">
					<span class="texto-radio"><label for="tipo5">PLANA</label></span>
					&nbsp;<a href="#" data-toggle="tooltip" title="Para uso de óculos como acessório, sem correção nas lentes" class="compra-tooltip">?</a>

					<br />

					<input type="radio" name="tipo" value="demonstrativa" id="tipo6">
					<span class="texto-radio"><label for="tipo6">DEMONSTRATIVA</label></span>
					&nbsp;<a href="#" data-toggle="tooltip" title="Se preferir confeccionar suas lentes em outra óptica, as lentes demonstrativas são lentes que já vêm com a armação, para serem substituídas posteriormente" class="compra-tooltip">?</a>

					<br />

				</div>			

				<span id="editar-tipo-semgrau">editar tipo</span>

				<span id="editar-tipo-simples">editar tipo</span>

				<div id="exibe-selecionar" style="margin-top:5px;">
				<a href="#" class="botao-comprar">SELECIONAR</a>
				</div>

				<div id="exibe-comprar">
				<br />

				<p style="text-align:center;margin-left:10px;">
				Se seus óculos forem <b>VISÃO SIMPLES</b>, você deve enviar uma cópia de sua receita para <span style="text-decoration:underline;">receita@hatsu.com.br</span>
				</p>
				
				<br />
				<input type="submit" class="botao-comprar" value="COMPRAR">
				</div>

				</form>

				<div id="exibe-experimente">
					
					<div id="exibe-texto-multifocal" style="padding:5px;text-align:justify;">Para confecção de lentes multifocais necessitamos fazer medições em seu rosto, para isso necessitamos que um consultor vá até você realizar as medições. Esse serviço é <b>gratuito</b> e pode ser requisitado abaixo.<br />
					<br />Caso queira comprar a armação e fazer as lentes multifocais em outra óptica, selecione a opção <b>“Lentes: Sem grau”</b>.
					</div>

				<a href="{{ URL::to('experimente') }}" class="botao-experimente">EXPERIMENTAR</a>

				</div>

			</div> <!-- fecha div col 3 de produto -->

	<br style="clear:both;" />
	<br style="clear:both;" />

	<h4 id="h4">CONSTRUÇÃO DE QUALIDADE</h4>
	
	</div>

<div class="container-fluid" style="background:#F9FAF2;">

<div class="container">

<img src="{{ URL::asset('img/produtos/detalhes/'.strtolower($produto->modelo).'_'.str_replace(" e ","_",strtolower($produto->cor).'.jpg')) }}" class="img-responsive" alt="Construção de Qualidade - Titanium"/>

</div>

</div>

		<br style="clear:both;">
		<!-- Page content starts -->

<div class="container">

<h4 id="h4">CARACTERÍSTICAS</h4>

		<div class="row">

			<div class="col-md-4">
				<img src="{{ URL::asset('img/produtos/titanium_chapaunica.jpg') }}" style='border:1px solid #ccc;'>
			</div>

			<div class="col-md-8" style="padding-top:10px;">

			<p style="font-size:24px;font-family:grotesque-ultralight;margin-bottom:10px;"><b>CHAPA ÚNICA</b></p>
			<p style="font-size:18px;font-family:roboto-regular;">TITÂNIO HOMOGÊNEO POR TODA ARMAÇÃO</p><br />

				<p>O corte de uma chapa única de titânio proporciona material homogêneo por toda a armação. Com isso, a densidade do material é constante por toda armação, garantindo assim ergonomia e resistência uniforme.</p>
			</div>
		</div>

<br style="clear:both;" />

		<div class="row">

			<div class="col-md-4">
				
				<img src="{{ URL::asset('img/produtos/titanium_polimentogradual.jpg') }}" style='border:1px solid #ccc;'>

			</div>

			<div class="col-md-8" style="padding-top:10px;">

			<p style="font-size:24px;font-family:grotesque-ultralight;margin-bottom:10px;"><b>POLIMENTO GRADUAL</b></p>
			<p style="font-size:18px;font-family:roboto-regular;">RESISTÊNCIA E LEVEZA NA MEDIDA CERTA</p><br />

			<p>Alterando a quantidade de polimento da chapa de titânio, a armação apresenta uma variação gradual de espessura, deixando assim as hastes mais finas para maior ergonomia e a armação que envolve as lentes com maior espessura, logo maior resistência contra quedas sem pesar em nariz e orelha.</p>

			</div>

		</div>

		<br style="clear:both;" />

		<div class="row">

			<div class="col-md-4">
				
				<img src="{{ URL::asset('img/produtos/titanium_resistente.jpg') }}" style="border:1px solid #ccc;">

			</div>


			<div class="col-md-8" style="padding-top:10px;">

			<p style="font-size:24px;font-family:grotesque-ultralight;margin-bottom:10px;"><b>DESEMPENHO MÁXIMO</b></p>
			<p style="font-size:18px;font-family:roboto-regular;">MAIOR RESISTÊNCIA A TEMPERATURA E QUEDAS</p><br />

			<p>As propriedades do Titânio oferecem aos óculos desempenho que óculos de matérias como acetato não possuem.
			A rigidez do Titânio somada a espessura da armação, garantem que quedas e altas temperaturas não deformem sua estrutura, como em armações de acetato ou até mesmo armações tubulares de titânio, que são muito finas para aproveitarem das propriedades do Titânio.</p>

			</div>

		</div>

		<br style="clear:both;" />

		<div class="row">

			<div class="col-md-4">
				
				<img src="{{ URL::asset('img/produtos/titanium_plaquetas.jpg') }}" style="border:1px solid #ccc;">

			</div>


			<div class="col-md-8" style="padding-top:10px;">

			<p style="font-size:24px;font-family:grotesque-ultralight;margin-bottom:10px;"><b>SILICONE DE GRAU MÉDICO </b></p>
			<p style="font-size:18px;font-family:roboto-regular;">ÓTIMO CAIMENTO, CONFORTO E ADERÊNCIA</p><br />

			<p>Para maior conforto ao contato com nariz e orelha, as plaquetas e o revestimento das hastes são feitos de silicone de grau médico, altamente biocompatível, de aderência perfeita à pele e longa vida útil, oferecendo conforto e durabilidade.</p>

			</div>

		</div>

	<br style="clear:both;" />


			<ul class="nav nav-tabs" role="tablist">

			<li class="active" style="font-family:grotesque-regular;font-weight:bold;font-size:16px;"><a href="#tab1" data-toggle="tab">BENEFÍCIOS HATSU</a></li>

			<li><a href="#tab2" data-toggle="tab" style="font-family:grotesque-regular;font-weight:bold;font-size:16px;">ESPECIFICAÇÕES</a></li>

			<li><a href="#tab3" data-toggle="tab" style="font-family:grotesque-regular;font-weight:bold;font-size:16px;">GARANTIA</a></li>

			<li><a href="#tab4" data-toggle="tab" style="font-family:grotesque-regular;font-weight:bold;font-size:16px;">PERGUNTAS FREQUENTES</a></li>

			</ul>

			<div class="tab-content">

			<div class="tab-pane active" id="tab1" style="border:1px solid #ccc;margin-top:-5px;">

			<br style="clear:both;" />

				<div class="row" style="margin-bottom:10px;">

					<div class="col-md-3" style="text-align:center;">
						
						<img src="{{ URL::asset('img/produtos/satisfacao.png') }}" style="min-width:140px;height:120px;">

					</div>

					<div class="col-md-9" style="padding-top:10px;">

					<p style="font-size:20px;font-family:grotesque-ultralight;text-transform:uppercase;margin-bottom:10px;"><b>SATISFAÇÃO TOTAL. Troca ou devolução assegurada</b></p>

					<p>Em caso de não adaptação de armação ou lentes, ou simplesmente por achar que a armação não ficou legal em você, asseguramos troca de modelo ou devolução total de seu dinheiro (conforme os <a href="{{ URL::to('garantia') }}" target="_blank">Termos de Garantia</a>).</p>

					</div>

				</div>

				<div class="row" style="margin-bottom:10px;">

					<div class="col-md-3" style="text-align:center;">
						
						<img src="{{ URL::asset('img/produtos/garantia.png') }}" style="min-width:140px;height:120px;">

					</div>


					<div class="col-md-9" style="padding-top:10px;">

					<p style="font-size:20px;font-family:grotesque-ultralight;text-transform:uppercase;margin-bottom:10px;"><b>1 ano de garantia para armações e lentes</b></p>

					<p>Confiando em nossos materiais e produtos, oferecemos 1 ano de garantia para armações e lentes em caso de defeito de fabricação ou de montagem de peça.</p>

					</div>

				</div>


				<div class="row" style="margin-bottom:10px;">

					<div class="col-md-3" style="text-align:center;">
						
						<img src="{{ URL::asset('img/produtos/testado.png') }}" style="min-width:140px;height:120px;">

					</div>


					<div class="col-md-9" style="padding-top:10px;">

					<p style="font-size:20px;font-family:grotesque-ultralight;text-transform:uppercase;margin-bottom:10px;"><b>Triplamente testado por empresas de inspeção diferentes</b></p>

					<p>Visando a qualidade de nossos produtos contamos com 3 diferentes empresas de inspeção de qualidade. Duas localizadas no Japão e na Coréia e uma no Brasil.</p>

					</div>

				</div>


				<div class="row" style="margin-bottom:10px;">

					<div class="col-md-3" style="text-align:center;">
						
						<img src="{{ URL::asset('img/produtos/parcelamento.png') }}" style="min-width:140px;height:120px;">

					</div>

					<div class="col-md-9" style="padding-top:10px;">

					<p style="font-size:20px;font-family:grotesque-ultralight;text-transform:uppercase;margin-bottom:10px;"><b>10 vezes sem juros</b></p>

					<p>Para maior comodidade oferecemos parcelamento de até 10 vezes sem juros para cartões Visa e MasterCard.</p>

					</div>

				</div>


			</div>

			<div class="tab-pane" id="tab2" style="border:1px solid #ccc;margin-top:-5px;padding:20px;">
			
				<div class="row">

					<div class="col-md-12">

					<p><b>Material</b> 

					<br /><br />- Ultem®<br /><br />
					
					<b>Cores</b> 

					<br /><br />
					- Preto e Amarelo<br />
					- Preto e Laranja<br />
					- Preto e Ouro<br />
					- Preto e Verde<br />
					- Cinza e Vermelho<br />

					<br /><br />
					<b>Tamanho</b>

					<table style="border-collapse:collapse;width:500px;text-align:center;">
					<tr>
					<td colspan="4" style="border:1px solid #000;">
					<b>Modelo TI_01 - Preto e Ouro</b>
					</td>
					</tr>
					<tr>
					<td style="border:1px solid #000;">Largura da haste</td>
					<td style="border:1px solid #000;">Ponte</td>
					<td style="border:1px solid #000;">Largura das lentes</td>
					<td style="border:1px solid #000;">Altura das Lentes</td>
					</tr>
					<tr>
					<td style="border:1px solid #000;">140mm</td>
					<td style="border:1px solid #000;">18mm</td>
					<td style="border:1px solid #000;">52mm</td>
					<td style="border:1px solid #000;">31mm</td>
					</tr>
					</table>

					<br />


					<table style="border-collapse:collapse;width:500px;text-align:center;">
					<tr>
					<td colspan="4" style="border:1px solid #000;">
					<b>Modelo TI_02 - Preto e Amarelo</b>
					</td>
					</tr>
					<tr>
					<td style="border:1px solid #000;">Largura da haste</td>
					<td style="border:1px solid #000;">Ponte</td>
					<td style="border:1px solid #000;">Largura das lentes</td>
					<td style="border:1px solid #000;">Altura das Lentes</td>
					</tr>
					<tr>
					<td style="border:1px solid #000;">140mm</td>
					<td style="border:1px solid #000;">17mm</td>
					<td style="border:1px solid #000;">55mm</td>
					<td style="border:1px solid #000;">44m</td>
					</tr>
					</table>

					<br />


					<table style="border-collapse:collapse;width:500px;text-align:center;">
					<tr>
					<td colspan="4" style="border:1px solid #000;">
					<b>Modelo TI_03 - Preto e Verde</b>
					</td>
					</tr>
					<tr>
					<td style="border:1px solid #000;">Largura da haste</td>
					<td style="border:1px solid #000;">Ponte</td>
					<td style="border:1px solid #000;">Largura das lentes</td>
					<td style="border:1px solid #000;">Altura das Lentes</td>
					</tr>
					<tr>
					<td style="border:1px solid #000;">140mm</td>
					<td style="border:1px solid #000;">16mm</td>
					<td style="border:1px solid #000;">52mm</td>
					<td style="border:1px solid #000;">47mm</td>
					</tr>
					</table>

					<br />


					<table style="border-collapse:collapse;width:500px;text-align:center;">
					<tr>
					<td colspan="4" style="border:1px solid #000;">
					<b>Modelo TI_04 - Preto e Laranja</b>
					</td>
					</tr>
					<tr>
					<td style="border:1px solid #000;">Largura da haste</td>
					<td style="border:1px solid #000;">Ponte</td>
					<td style="border:1px solid #000;">Largura das lentes</td>
					<td style="border:1px solid #000;">Altura das Lentes</td>
					</tr>
					<tr>
					<td style="border:1px solid #000;">145mm</td>
					<td style="border:1px solid #000;">17mm</td>
					<td style="border:1px solid #000;">54mm</td>
					<td style="border:1px solid #000;">37mm</td>
					</tr>
					</table>

					<br />


					<table style="border-collapse:collapse;width:500px;text-align:center;">
					<tr>
					<td colspan="4" style="border:1px solid #000;">
					<b>Modelo TI_05 - Cinza e Vermelho</b>
					</td>
					</tr>
					<tr>
					<td style="border:1px solid #000;">Largura da haste</td>
					<td style="border:1px solid #000;">Ponte</td>
					<td style="border:1px solid #000;">Largura das lentes</td>
					<td style="border:1px solid #000;">Altura das Lentes</td>
					</tr>
					<tr>
					<td style="border:1px solid #000;">145mm</td>
					<td style="border:1px solid #000;">18mm</td>
					<td style="border:1px solid #000;">55mm</td>
					<td style="border:1px solid #000;">40mm</td>
					</tr>
					</table>															
					</p>

					</div>

				</div>

			</div>

			<div class="tab-pane" id="tab3" style="border:1px solid #ccc;margin-top:-5px;padding:20px;">

				<div class="row">

					<div class="col-md-12">

						<p>Confiando em nossos produtos e prezando por sua satisfação, oferecemos garantia que oferece o máximo de suporte a você e seus óculos. Caso a garantia necessite ser acionada, oferecemos trocas gratuitas ou devoluções com reembolso total, com envio pago por nós. A garantia é requisitada através do email <a href="mailto:atendimento@hatsu.com.br">atendimento@hatsu.com.br</a></p>

						<br />

						<ul>
						
							<li>
							Caso você não se adapte as nossas armações ou lentes e quer devolvê-las, entre em contato conosco, em até 30 dias depois do recebimento dos óculos.
							</li>
							
							<li>
							Caso você não se adapte as nossas armações ou lentes e quer trocá-las, entre em contato conosco, em até 90 dias depois do recebimento dos óculos.
							</li>
							
							<li>
							Mesmo com nosso criterioso processo de avaliação individual de cada armação e lente, caso sua armação ou lente possuem algum problema de fabricação ou algum defeito após utilização, ofereceremos garantia de 1 ano a partir da data de entrega.
							</li>

						</ul>

					</div>

				</div>


			</div>

			<div class="tab-pane" id="tab4" style="border:1px solid #ccc;margin-top:-5px;padding:20px;">

				<div class="row">

					<div class="col-md-12">

						<div class="support container" style="margin-top:20px;width:100%;background:#fff;">

                    <!-- Lists -->
                    <ul id="slist">

                        <li style="margin-bottom:10px;">
                           <a href="#" style="text-transform:uppercase;">Eu recebo os óculos no momento da compra? </a>

                           
                           <p>Não. Precisamos de alguns dias para confeccionar as lentes indicadas pelo seu oftalmologista. Você apresenta a prescrição para o consultor, ele tira uma cópia, envia para o laboratório e em poucos dias você recebe em casa os óculos com as lentes. É muito importante apresentar a prescrição ao consultor. Dessa forma as lentes são confeccionadas corretamente e não prejudicam a saúde dos seus olhos.

                         
                        </li>

                        <li style="margin-bottom:10px;">
                           <a href="#" style="text-transform:uppercase;">Por que devo enviar minha receita médica para comprar meus óculos? </a>

                        
                          <p> A Hatsu se preocupa com a saúde dos seus olhos. Por isso, queremos garantir que as lentes enviadas sejam adequadas para você. A única maneira de montar os óculos corretamente é tendo a prescrição do seu médico em mãos. Por esse motivo solicitamos o envio de uma foto ou imagem escaneada do receituário.

                        </li>

                        <li style="margin-bottom:10px;">

                           <a href="#" style="text-transform:uppercase;">Como funciona o serviço “Experimente Hatsu”? </a>

                          
                           <p>Experimente Hatsu é um serviço gratuito oferecido pela Hatsu onde um consultor leva nossos óculos até você e te ajuda a escolher o modelo de óculos que mais combina com seu rosto e necessidades diárias. O consultor realiza também medidas do seu rosto para confeccionar as lentes de forma mais adequada à seu rosto e armação.
                          


                        </li>

                        <li style="margin-bottom:10px;">

                           <a href="#" style="text-transform:uppercase;">É possível adquirir somente a armação, sem as lentes?</a>

                                                     
                          <p>Sim, é possível. Basta na seção “Comprar” selecionar “Lentes de Grau: Sem Grau” e depois escolher em “Tipo de lente” entre “Demonstrativas” (para substituir por outra lente) ou “Planas” (Se você quer usar os óculos apenas como acessório).</a>
                          

                        </li>


                        <li style="margin-bottom:10px;">
                           <a href="#" style="text-transform:uppercase;">Troca e devolução</a>

                           <p>Em caso de não adaptação de armação ou lentes, ou simplesmente por achar que a armação não ficou legal em você, asseguramos troca de modelo ou devolução total de seu dinheiro.
						As trocas e devoluções são realizadas através do envio, pago por nós, do produto de volta para nós. As trocas são realizadas o mais rápido possível e enviaremos para o endereço desejado assim que o novo produto estiver disponível.

						Em caso de solicitação de devolução, o reembolso será feito da mesma maneira que o pagamento foi realizado, ou seja, caso o pagamento tenha sido feito através do cartão de créditos, nós estornaremos o valor em até duas fátuas. Por sua vez, se o pagamento foi realizado via boleto bancário, o reembolso será realizado em até 5 dias uteis em sua conta corrente.
						

                        </li>


                   </ul>

               </div>

					</div>

				</div>


			</div>

		</div>

	</div>

</div>
<!-- Page content ends -->

@endforeach

@stop