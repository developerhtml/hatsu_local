    <!-- Custom CSS -->
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
        <!-- Bootstrap CSS -->
    <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">

<div id="processo" class="container">

	<h2>PROCESSO DE COMPRA</h2>

	<div class="row">

		<div class="col-md-3" style="border-right:0;">
		<h3>1º PASSO</h3>

		<p><span class="sub">Escolha sua armação</span><br />
		Ao encontrar a armação que mais combina com suas necessidades e estilo, clique em “Escolher” para comprá-la online ou selecione “Experimente em casa” para requisitar o serviço (<a href="{{ URL::to('experimente')}}" target="_blank">Saiba mais</a>)
		</p>

		<br />

		<img src="{{ URL::asset('img/processo/passo-1.png') }}" title="Escolha sua armação" alt="Escolha sua armação" class="img-responsive" />

		</div>

		<div class="col-md-3" style="border-right:0;">
		<h3>2º PASSO</h3>

		<p><span class="sub">Selecione suas lentes</span><br />
		<p>Escolha qual lente cobre suas necessidades:</p><br />

		<img src="{{ URL::asset('img/processo/passo-2.png') }}" title="Escolha sua armação" alt="Escolha sua armação" class="img-responsive" />

		<br />

		<ul>

		<li>Lentes “Sem Grau” caso você queira usar nossos óculos como acessório com lentes sem correções, ou caso queira fazer as lentes em outra óptica;</li>
		<li>Lentes “Visão Simples” para correção de miopia ou hipermetropia, e astigmatismo ou presbiopia (vista cansada);</li>
		<li>Lentes “Multifocais” para correção simultânea de miopia e presbiopia, ou miopia e presbiopia mais astigmatismo, ou lentes planas (sem grau para longe) e presbiopia.</li>

		</ul>

		</div>

		<div class="col-md-3">
		<h3>3º PASSO</h3>

		<p><span class="sub">Escolha o tipo de sua lente</span><br />
		Certos graus de lentes necessitam materiais especiais para que as lentes não fiquem tão grossas.</p><br />

		<img src="{{ URL::asset('img/processo/passo-3.png') }}" title="Escolha sua armação" alt="Escolha sua armação" class="img-responsive" />

		<br />

		<ul>

		<li>Lentes “normais” são para graus não tão acentuados;</li>
		<li>Já as lentes “Finas resistentes” são para graus mais avançados como de 2 a 4 para ametropias como miopia e hipermetropia;</li>
		<li>As lentes “Finíssimas” são feitas de material que permite graus bem acentuados, como 10 graus de miopia, continuem com lentes finas.</li>

		</ul>

		<p style="color:#77a302;">Todas lentes possuem antirreflexo, proteção UVA e UVB e proteção antirrisco inclusa.</p>
		</div>

		<div class="col-md-3" style="border-left:0;">
		
		<h3>4º PASSO</h3>

		<p><span class="sub">Envie sua receita</span><br />

		Depois de efetuar a compra, tire uma foto ou escaneie sua receita oftalmológica e nós envie através do email receita@hatsu.com.br ou envie na seção “Minha conta” para que possamos fabricar suas lentes com perfeição</p>

		<img src="{{ URL::asset('img/processo/receita.png') }}" title="Escolha sua armação" alt="Escolha sua armação" class="img-responsive" />

		</div>

	</div>

	<div class="row" style="margin-top:5px;text-align:center;">

	<div class="col-md-12" style="background:#f1f1f1;">

	<p style="font-size:16px;">Continua com dúvidas? Acesse nossa página de <a href="{{ URL::to('duvidas') }}" style="color:#77a302">dúvidas</a> para obter ajuda</p>

	</div>

	</div>

</div>