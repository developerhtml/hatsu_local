@extends('template')

@section('seo')

<!-- Title here -->
<title>Linha Titanium | Óculos de Titânio | Óculos de grau| Hatsu | Óculos de Alta Tecnologia</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil. Produtos de primeira linha com pagamento em até 10 vezes sem juros.">
<meta name="keywords" content="titânio, hatsu, óculos de grau, óculos de graus, comprar óculos, óculos importados, óculos miopia, óculos de alta tecnologia, óculos femininos, óculos masculinos, óculos inquebrável, lentes bifocais, lentes,raid, projection, titanium, óculos titânio">
<meta name="author" content="Hatsu - 2014 - Todos os direitos reservados">
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="2 days" />
<link rel="canonical" href="http://www.hatsu.com.br" />
<link rel="index" title="Óculos de grau, armações e lentes | Hatsu" href="http://www.hatsu.com.br" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

	<div class="container-fluid" style="background-color:#EDEDED;">

		<div class="row">

			<div class="col-md-12" id="linha-titanium">

			<div class="col-md-7">

			&nbsp;

			</div>

			<div class="col-md-3" id="info-titanium">

			<p class='titanium-titulo-hatsu'>TITANIUM</p>

			<br />

			<p class="titanium-texto">

			A linha Titanium conta com armação feita 100% de titânio, cortada a partir de uma chapa única. A armação é polida em diferentes intensidades, gerando diferentes espessuras, para oferecer resistência onde necessário e ainda assim não pesar no nariz e orelha. O material e o acabamento das bordas da armação garantem que quedas não amassem nem lasquem os óculos.

			</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

	</div>

	</div>	

	<br style="clear:both;" />

	<div class="container">

		<h2 style="border-bottom:1px solid #000;text-align:center;">ESCOLHA O SEU</h2>
					<br />


			{{ Helper::LinhaProdutos(5) }}

			<br />
			<br />

	                 </div>
	</div>




	</div>



	<br style="clear:both;" />

		<div class="container-fluid" style="background-color:#e4edcc;padding-top:20px;padding-bottom:20px;">

			<div class="col-md-2">

			&nbsp;

			</div>

			  <div class="col-md-3" style="text-align:center;vertical-align:middle;">

			  <img src="{{ URL::asset('img/linha-titanium/tesoura.png') }}" alt="Corte de uma chapa única"   />

			  </div>

			  <div class="col-md-5" style="text-align:center;vertical-align:middle;">

			  <p class="titanium-titulo">CORTE DE UMA CHAPA ÚNICA</p>

			  <br />

			  <p class="titanium-texto">O design final da armação é alcançado através do corte de uma chapa única de titânio e do polimento que afina gradualmente a armação para maior ergonomia sem deixar para trás a resistência e proteção das lentes.</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" style="background-color:#fff;padding-top:20px;padding-bottom:20px;">

			<div class="col-md-2">

			&nbsp;

			</div>

			  <div class="col-md-5" style="text-align:center;vertical-align:middle;">

			  <p class="titanium-titulo">PROCESSO DE PINTURA</p>

			  <br />

			  <p class="titanium-texto">O aspecto fosco da pintura é adquirido pelo processo de fixação da tinta no titânio, aplicada de maneira homogênea e simultânea em toda armação. No topo da armação, o detalhe em cor diferente possui tinta e processo de aplicação diferente, trazendo tridimensionalidade a armação.</p>

			  </div>

			  <div class="col-md-3" style="text-align:center;vertical-align:middle;">

			  <img src="{{ URL::asset('img/linha-titanium/spray.png') }}" alt="Processo de pintura"   />

			  </div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>



		<div class="container-fluid" style="background-color:#e4edcc;padding-top:20px;padding-bottom:20px;">

			<div class="col-md-2">

			&nbsp;

			</div>

			  <div class="col-md-3" style="text-align:center;vertical-align:middle;">

			  <img src="{{ URL::asset('img/linha-titanium/halter.png') }}" alt="Resistência incomparável"   />

			  </div>

			  <div class="col-md-5" style="text-align:center;vertical-align:middle;">

			  <p class="titanium-titulo">RESISTÊNCIA INCOMPARÁVEL</p>

			  <br />

			  <p class="titanium-texto">As propriedades do titânio somadas ao design de aro inteiro com curvaturas estratégicas garantem resistência que aros de acetato jamais atingirão. Na espessura e leveza fica evidente a diferença do uso de materiais de melhor desempenho e acabamento.</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>	


		</div>

@stop