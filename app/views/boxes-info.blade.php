<div class="container-fluid" style="text-align:center;">

	<h1 class="titulo">CONHEÇA</h1>
	<p class="subtitulo">Fazemos mais que oferecer óculos incríveis</p>

	<div class="col-md-1">

	&nbsp;

	</div>
			<div class="row" id="overandover">

				<div class="col-md-3" style="text-align:center;">

	               <a href="http://blog.hatsu.com.br/" class="col-1-3" target="_blank" >
	                    <div class="img-frame" style="text-align:center;">
	                        <div class="hover">
	                            <span class="botao-bloco">ACESSAR BLOG</span>
	                        </div>
	                        <img alt="Blog Hatsu" src="{{ URL::asset('img/home/blog.jpg') }}" style="max-width:280px" />
	                    </div>
	                </a>

	                <h3 style="color:#78A600;">BLOG</h3>

	                <p>Em nosso blog publicamos textos autorais, abordando temas relacionados à visão</p>


				  </div>

				<div class="col-md-4" style="text-align:center;">

	               <a href="{{ URL::to('sobre')}}" class="col-1-3">
	                    <div class="img-frame" style="text-align:center;">
	                        <div class="hover">
	                            <span class="botao-bloco">CONHEÇA A HATSU</span>
	                        </div>
	                        <img src="{{ URL::asset('img/home/sobre.jpg') }}" style="max-width:280px;" alt="Óculos de Grau Hatsu" />
	                    </div>
	                </a>

	                <h3 style="color:#78A600;">SOBRE A HATSU</h3>

	                <p>Óculos produzidos no Japão, com design inovador e materiais de alta tecnologia, inéditos no Brasil</p>

				  </div>

				<div class="col-md-3" style="text-align:center;">

	               <a href="{{ URL::to('lentes')}}" class="col-1-3">
	                    <div class="img-frame" style="text-align:center;">
	                        <div class="hover">
	                            <span class="botao-bloco">VER PÁGINA DE LENTES</span>
	                        </div>
	                        <img src="{{ URL::asset('img/home/lentes.jpg') }}" style="max-width:280px;" alt="Óculos de Grau com Armação e Lentes de qualidade" />
	                    </div>
	                </a>

					<h3 style="color:#78A600;">LENTES</h3>

	                <p>Para trazer maior conhecimento sobre as lentes oftálmicas, criamos uma página que contém informações variadas sobre lentes.</p>
				  </div>

				  <div class="col-md-1">

	&nbsp;

	</div>

			</div>


		  </div>
