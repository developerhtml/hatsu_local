		<!-- Promo box starts -->
		  <div class="container">
			<div class="row">

			  <div class="col-md-4" style="text-align:center;">
				<a href="{{ URL::to('experimente')}}">
				<img src="{{ URL::asset('img/home/experimente.jpg') }}" alt="Experimente em casa" />
				</a>
			  </div>


			  <!-- Blue color promo box -->
			  <div class="col-md-4" style="text-align:center;">
				<!-- bcolor =  Blue color -->
				<br />
				<iframe src="//player.vimeo.com/video/100104825?title=0&amp;byline=0&amp;portrait=0&amp;color=77a302&amp;autoplay=1&amp;loop=1" width="275" height="155" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			  </div> 

			  <!-- Green color promo box -->
			  <div class="col-md-4" style="text-align:center;">
				<!-- <div class="pbox" style="border:none;"> -->
				<a href="{{ URL::to('produtos')}}">
				<img src="{{ URL::asset('img/home/lentes_gratuitas.png') }}" alt="Lentes" />
				</a>
				<!-- </div> -->
			  </div>

			</div>
		  </div>
		<!--/ Promo box ends -->