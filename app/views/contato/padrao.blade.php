<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">

    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/hatsu.css') }}" rel="stylesheet"> 
    <link rel="shortcut icon" href="{{ URL::asset('img/favicon/favicon.png') }}">

    <style type="text/css">

    p { font-size:14px; }

    </style>

</head>

<body>

<div class="container" style="padding:10px;width:600px;">

	<div class="row">

	<header style="padding:0;">
		<div class="container">
			<div class="row">
				<div class="col-md-3" style="padding-left:0;padding:0;">
					<!-- Logo. Use class "color" to add color to the text. -->
					<div class="logo">
						<h1><a href="http://www.hatsu.com.br"><img src="http://hatsu.com.br/public/img/logo-hatsu.png" class="img-responsive" /></a></h1>
					</div>
				</div>
			</div>
		</div>
	</header>


	<?php 

	$contato =  Contato::find($id_contato);

	?>

	<p>Foi enviado um novo e-mail através do formulário de contato. Acesse <a href="http://admin.hatsu.com.br">o painel administrativo</a> para visualizar maiores detalhes.</p>

	<p>Nome: {{ $contato->nome }} </p>

	<p>E-mail: {{ $contato->email }} </p>

	<p>Telefone: {{ $contato->telefone }} </p>

	<br />

	</div>

</div>

</body>

</html>
