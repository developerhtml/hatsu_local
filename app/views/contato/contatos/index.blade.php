@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">       

          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header hidden-print"> <i class="icon-pushpin"></i>
              <h3 class="hidden-print">Gerenciamento de Contatos</h3>
            </div>
            <!-- /widget-header -->

            <!-- exibindo message da session -->
            @if (Session::has('message'))
              <div class="alert alert-info" style="margin-top:10px;background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;">{{ Session::get('message') }}</div>
            @endif

            @if (count($contatos)<=0)

              <div class="widget-content">
            
              <div class="alert alert-info" style="margin:10px;background:#fff59b;color:#000;border:1px solid #ccc;text-align:center;font-size:14px;">Sua busca não retornou nenhum resultado</div>

              </div>
              <!-- /widget-content --> 
            </div>
            <!-- /widget --> 

            @else 

            {{ $contatos->links(); }}

            <div class="widget-content">

              <table class="table table-striped table-bordered">
                <thead>
                  <tr>

                    <th> 

                    @if ($sortby == 'nome' && $order == 'asc') {{
                        link_to_action('ContatoController@getIndex','Nome',
                            array(
                                'sortby' => 'nome',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('ContatoController@getIndex','Nome',
                            array(
                                'sortby' => 'nome',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif

                    </th>

                    <th> 

                    @if ($sortby == 'email' && $order == 'asc') {{
                        link_to_action('ContatoController@getIndex','Email',
                            array(
                                'sortby' => 'email',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('ContatoController@getIndex','Email',
                            array(
                                'sortby' => 'email',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif

                    </th>

                    <th> Contato </th>

                    <th> 
                    @if ($sortby == 'cidade' && $order == 'asc') {{
                        link_to_action('ContatoController@getIndex','Cidade/UF',
                            array(
                                'sortby' => 'cidade',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('ContatoController@getIndex','Cidade/UF',
                            array(
                                'sortby' => 'cidade',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif
                     </th>

                     <th> Mensagem </th>

                    <th class="td-actions hidden-print"> Ações </th>
                  </tr>
                </thead>

               <tbody>
                  @foreach ($contatos as $contato)
              <tr>
                
                <td>{{ $contato->nome }} </td>

                <td>{{ $contato->email }} </td>

                <td>{{ $contato->telefone }}</td>

                <td>

                {{ Cidade::find($contato->cidade)->nome ." / ". Cidade::find($contato->cidade)->uf  }}

                </td>

                <td>{{ $contato->mensagem }}</td>
                
                <td class="td-actions hidden-print">

                <!-- <a href="{{ URL::to('contato/edit/' . $contato->id) }}" class="btn btn-small btn-warning"> -->

                <!-- <i class="btn-icon-only icon-pencil"></i></a> -->

                <a href="{{ URL::to('contato/destroy/' . $contato->id) }}" class="btn btn-danger btn-small">

                <i class="btn-icon-only icon-remove"> </i></a></td>

            </tr>
            
        @endforeach

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 

        @endif

        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
@stop