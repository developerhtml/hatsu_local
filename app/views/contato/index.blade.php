@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Contato | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">contato</span>

</div>

</div>

<!-- Page content starts -->

<div class="content contact-two">

  <div class="container" style="padding:0;">

    <div class="row"> 
      
      <div class="col-md-12" style="text-align:center;">

      <h1 style="font-size:32px;font-weight:normal;color:#2z2z2z;padding:20;">CONTATO</h1>
      
      </div>

      <div class="col-md-12" style="text-align:center;font-size:16px;">

      <p>Quer retirar alguma dúvida ou saber o status de sua compra? Entre em contato conosco.</p>
      <br>
      </div>

      <br style="clear:both;" />
      <br style="clear:both;" />

      @if (Session::has('message'))

        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2>
        
      @endif

      <p style="text-align:center;">

      <div class="col-md-8">
     <!-- Contact form -->
        <h4 class="title" style="text-align:center;">FORMULÁRIO</h4>

        <div class="form" style="text-align:center;">
          <!-- Contact form (not working)-->
          <form class="form-horizontal" action="{{ URL::to('contato/store') }}" method="post">
              
              @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif

              <!-- Name -->
              <div class="form-group">
                <label class="control-label col-md-2" for="name1">Nome</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('nome')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" value="{{ Input::old('nome') }}" placeholder="Informe seu nome completo">
                </div>
              </div>

              @if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
              <!-- Email -->
              <div class="form-group">
                <label class="control-label col-md-2" for="email1">E-mail</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" value="{{ Input::old('email') }}" placeholder="Informe seu e-mail">
                </div>
              </div>

              <!-- Email -->
              <div class="form-group">
                <label class="control-label col-md-2" for="telefone">Telefone</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('telefone')>0) style='border-color:#ff0000;' @endif id="celular" name="telefone" value="{{ Input::old('telefone') }}" placeholder="Telefone ou Celular">
                </div>
              </div>

              @if($errors->has('estado')>0) <p class='error'> {{ $errors->first('estado') }} </p> @endif

                <div class="form-group">
                <label class="control-label col-md-2">Estado</label>
                <div class="col-md-9">                               
                  <select class="form-control" id="estado" name="estado" @if($errors->has('estado')>0) style='border-color:#ff0000;' @endif>
                  <option value="">- Selecione -</option>
                    @foreach(Estado::all() as $estado)
                        <option @if(Input::old('estado') == $estado->id) {{ 'selected="selected"' }} @endif value="{{ $estado->id }}">{{ $estado->nome }}</option>
                    @endforeach
                  </select>
                </div>
                </div>  

                @if($errors->has('cidade')>0) <p class='error'> {{ $errors->first('cidade') }} </p> @endif    

                <div class="form-group">
                <label class="control-label col-md-2">Cidade</label>
                <div class="col-md-9">                               
                  <select class="form-control" id="cidade" name="cidade" @if($errors->has('cidade')>0) style='border-color:#ff0000;' @endif>
                    @if (Input::old('cidade')) 
                        @foreach(Cidade::where('uf','=',Cidade::find(Input::old('cidade'))->uf)->get() as $cidade)
                          <option value='{{ $cidade->id }}' @if($cidade->id == Input::old('cidade')) selected='selected' @endif >{{ $cidade->nome }}</option>
                        @endforeach
                      @else
                        <option value="">- Selecione -</option>
                      @endif
                    </select>
                  </div>
                </div>  

              @if($errors->has('mensagem')>0) <p class='error'> {{ $errors->first('mensagem') }} </p> @endif

              <!-- Comment -->
              <div class="form-group">
                <label class="control-label col-md-2" for="comment">Mensagem</label>
                <div class="col-md-9">
                  <textarea class="form-control" id="comment" name="mensagem" rows="3" placeholder="No que podemos te ajudar? Nosso atendimento entrará em contato o mais breve possível." @if($errors->has('mensagem')>0) style='border-color:#ff0000;' @endif>{{ Input::old('mensagem') }}</textarea>
                </div>
              </div>
              <!-- Buttons -->
              <div class="form-group">
                 <!-- Buttons -->
				 <div class="col-md-9 col-md-offset-4">
					<input type="submit" class="botao-comprar" value="ENVIAR">
				 </div>
              </div>
          </form>
        </div>

</div>
<div class="col-md-4" style="text-align:center;background:#f8f8f8;">
        <!-- Address section -->
        <br>
           <h4 class="title">ATENDIMENTO</h4>
           <div class="address" style="padding:0;margin:0;">
               <address>
                  <!-- Company name -->
                  <!--<strong>Hatsu - Óculos de Alta Tecnologia</strong><br><br/>-->
                  <abbr title="Telefone para contato">Telefone</abbr>: (19) 3252-3760
               </address>

               <address>
	      	  <abbr title="Atendimento via WhatsApp">WhatsApp</abbr>: (19) 98331-1070
               </address>
               
	           <b>Horário de atendimento</b>:<br> Segunda a sexta-feira das 9:00 às 18:00<br></br>
                
               <address>
                  <a href="mailto:contato@hatsu.com.br" title="Envie-nos um e-mail" style="color:#77a302;">contato@hatsu.com.br</a>
               </address> 
           </div>
</div>
        <hr />        
<br />
        <div class="col-md-4" style="text-align:center;background:#f8f8f8;">
     <!-- Contact form -->
        <h4 class="title">MÍDIAS SOCIAIS</h4>
                       <!-- Social media icons -->
               <div class="social">
                    <a href="http://www.facebook.com/HatsuBrasil" target="_blank" title="Curta nosso Facebook"><i class="fa fa-facebook facebook"></i></a>
                    <a href="http://twitter.com/HatsuBR" target="_blank" title="Siga nosso Twitter"><i class="fa fa-twitter twitter"></i></a>
                    <a href="http://instagram.com/HatsuBr" target="_blank" title="Siga nosso Instagram"><i class="fa fa-instagram instagram"></i></a>
                    <a href="https://www.linkedin.com/company/hatsu" target="_blank" title="Siga nosso Linkedin"><i class="fa fa-linkedin-square" style="background:#000;"></i></a>
                    <a href="https://plus.google.com/+HatsuBrasil/" target="_blank" title="Siga nosso Google+"><i class="fa fa-google-plus google-plus"></i></a>
                    <a href="http://vimeo.com/hatsu" target="_blank" title="Acesse nosso Vimeo"><i class="fa fa-vimeo-square" style="background:#000;"></i></a>
              </div>
              <br>
    </div>

    </p>

  </div>
</div>

</div>

</div>

<!-- Page content ends -->


@stop