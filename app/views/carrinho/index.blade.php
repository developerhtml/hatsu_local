@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Carrinho de Compras | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">Carrinho</span>

</div>

</div>

<!-- Page content starts -->

<div class="content">

<form class="form-horizontal" action="{{ URL::to('carrinho/store') }}" method="post" id="carrinho">

	<div class="container">

<div class="col-md-12" id="carrinho">

<div class="col-md-3 header">ITEM</div>

<div class="col-md-3 header">LENTE</div>

<div class="col-md-3 header">REMOVER</div>

<div class="col-md-3 header">TOTAL</div>

</div>

<?php $i = 1; ?>

@foreach($produtos as $indice => $item)

<input type="hidden" name="indice[{{ $indice }}]" value="{{ $indice }}">
<input type="hidden" name="preco[{{ $indice }}]" value="{{ $item["preco"] }}">
<input type="hidden" name="thumb[{{ $indice }}]" value="{{ $item["thumb"] }}">
<input type="hidden" name="sku[{{ $indice }}]" value="{{ $item["sku"] }}">
<input type="hidden" name="modelo[{{ $indice }}]" value="{{ substr($item["sku"],0,5) }}">
<input type="hidden" name="cor[{{ $indice }}]" value="{{ $item["cor"] }}">

<div class="col-md-12" id="item-carrinho">

<div class="col-md-3">

<img src="{{ URL::asset('img/'.$item["thumb"]) }}" style="max-width:190px" /> <br />

@if(substr($item["sku"],0,2) == "PR")

PROJECTION | {{ substr($item["sku"],0,5) }} ({{ $item["cor"] }})

<input type="hidden" name="linha[{{ $indice }}]" value="projection">

@elseif(substr($item["sku"],0,2) == "RA")

RAID | {{ substr($item["sku"],0,5) }} ({{ $item["cor"] }})

<input type="hidden" name="linha[{{ $indice }}]" value="raid">

@elseif(substr($item["sku"],0,2) == "UT")

ULTRA | {{ substr($item["sku"],0,5) }} ({{ $item["cor"] }})

<input type="hidden" name="linha[{{ $indice }}]" value="ultra">

@elseif(substr($item["sku"],0,2) == "KI")

KIZO | {{ substr($item["sku"],0,5) }} ({{ $item["cor"] }})

<input type="hidden" name="linha[{{ $indice }}]" value="kizo">

@elseif(substr($item["sku"],0,2) == "TI")

TITANIUM | {{ substr($item["sku"],0,5) }} ({{ $item["cor"] }})

<input type="hidden" name="linha[{{ $indice }}]" value="titanium">

@endif

<br />

</div>

<div class="col-md-3">

<br />
<input type="radio" name="lente[{{ $indice }}]" @if($item["lente"] == "semgrau") checked='checked' @endif value="semgrau" id="car-exibe-semgrau-{{ $i }}">
<span class="texto-radio"><label for="car-exibe-semgrau-{{ $i }}">SEM GRAU</label></span>
<br />

<input type="radio" name="lente[{{ $indice }}]" @if($item["lente"] == "simples") checked='checked' @endif value="simples" id="car-exibe-simples-{{ $i }}">
<span class="texto-radio"><label for="car-exibe-simples-{{ $i }}">VISÃO SIMPLES</label></span>

</div>

<div class="col-md-3">
<br /><br />
<a href="{{ URL::to('carrinho/remover/'.$indice) }}">REMOVER</a>

</div>


<div class="col-md-3">
<br /><br />
<p style="font-size:20px;font-weight:bold;">

R$ {{ number_format($item["subtotal"],2,",",".") }}

</p>

</div>

</div>

<div id="car-detalhe-semgrau-{{ $i }}" @if($item["lente"] == "semgrau") style='display:block' @elseif($item["lente"] == "simples") style='display:none;' @endif>

	<div class="col-md-12" id="carrinho-semgrau">

		<div class="col-md-6">

			<input type="radio" name="tipo[{{ $indice }}]" @if($item["tipo"] == "plana") checked="checked" @endif value="plana" id="tipo1-{{ $i }}">
			<span class="texto-radio"><label for="tipo1-{{ $i }}">PLANA</label></span>

			<br />

			<div id="texto-lente" style="text-align:left;">

			<p>Para uso de óculos como acessório, sem correção nas lentes</p>

			</div>

		</div>

		<div class="col-md-6">

			<input type="radio" name="tipo[{{ $indice }}]" @if($item["tipo"] == "demonstrativa") checked="checked" @endif value="demonstrativa" id="tipo2-{{ $i }}">
			<span class="texto-radio"><label for="tipo2-{{ $i }}">DEMONSTRATIVA</label></span>

			<div id="texto-lente" style="text-align:left;">

			<p>Se preferir confeccionar suas lentes em outra óptica, as lentes demonstrativas são lentes que já vêm com a armação, para serem substituídas posteriormente</p>

			</div>

			</div>

		</div>

</div>

<div id="car-detalhe-simples-{{ $i }}"  @if($item["lente"] == "semgrau") style='display:none' @elseif($item["lente"] == "simples") style='display:block;' @endif>

	<div class="col-md-12" id="carrinho-simples">

	<div class="col-md-3">

	<input type="radio" name="tipo[{{ $indice }}]" @if($item["tipo"] == "normais") checked="checked" @endif value="normais" id="tipo3-{{ $i }}">
	<span class="texto-radio"><label for="tipo3-{{ $i }}">NORMAIS</label></span>
	<br />

	<div id="texto-lente" style="text-align:left;">

	<p>Miopia: de 0 a -2 graus</p>
	<p>Hipermetropia: de 0 a +2 graus</p>
	<p>Astigmatismo: de 0 a -2 graus</p>

	</div>

	</div>

	<div class="col-md-3">

	<input type="radio" name="tipo[{{ $indice }}]" @if($item["tipo"] == "finas") checked="checked" @endif value="finas" id="tipo4-{{ $i }}">
	<span class="texto-radio"><label for="tipo4-{{ $i }}">FINAS</label></span>
	<div id="texto-lente" style="text-align:left;">

	<p>Miopia: de -2 a -4 graus</p>
	<p>Hipermetropia: de +2 a +4 graus</p>
	<p>Astigmatismo: de 0 a -4 graus</p>

	</div>

	</div>

	<div class="col-md-3">

	<input type="radio" name="tipo[{{ $indice }}]" @if($item["tipo"] == "finissimas") checked="checked" @endif  value="finissimas" id="tipo5-{{ $i }}">
	<span class="texto-radio"><label for="tipo5-{{ $i }}">FINÍSSIMAS</label></span>
	<div id="texto-lente" style="text-align:left !important;">

	<p>Miopia: de -4 a -10 graus</p>
	<p>Astigmatismo: de 0 a -4 graus</p>

	</div>

	</div>

	<div class="col-md-3">

	<input type="radio" name="tipo[{{ $indice }}]" @if($item["tipo"] == "naosei") checked="checked" @endif  value="naosei" id="tipo6-{{ $i }}">
	<span class="texto-radio"><label for="tipo6-{{ $i }}">NÃO SEI</label></span>

	</div>

	</div>

</div>

<?php $i++; ?>

@endforeach


<div class="col-md-12" style="text-align:right;margin-top:10px;">

<input type="submit" class="botao-comprar" value="IR PARA CHECKOUT" style="float:right;margin-left:20px;">

<a href="{{ URL::to('produtos')}}" class="botao-experimente" style="float:right;">CONTINUAR COMPRANDO</a>


</div>

</div>

</form>


</div>


@stop