@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Carrinho de Compras | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<!-- Page content starts -->

<div class="content">

	<div class="container">

		<form class="form-horizontal" action="{{ URL::to('carrinho/store') }}" method="post" id="carrinho">

<!-- 

1- VERIFICAR SE EXISTE A SESSION

2- FOREACHAR NOS PRODUTOS

3- MONTAR FORMULÁRIO

4- SELECINAR AS LENTES DA SESSAO

5- CONTROLLER PARA ATUALIZAR O CARRINHO

 -->

<table class="col-md-12" id="carrinho">

<tr>

<td class="col-md-3 header">ITEM</td>

<td class="col-md-3 header">LENTE</td>

<td class="col-md-3 header">REMOVER</td>

<td class="col-md-3 header">TOTAL</td>

</tr>

@foreach($produtos as $item)

<?php $sku = $item["sku"]; ?>

<input type="hidden" name="preco[{{ $item["sku"] }}]" value="{{ $item["preco"] }}">
<input type="hidden" name="thumb[{{ $item["sku"] }}]" value="{{ $item["thumb"] }}">
<input type="hidden" name="sku[{{ $item["sku"] }}]" value="{{ $item["sku"] }}">
<input type="hidden" name="modelo[{{ $item["sku"] }}]" value="{{ substr($item["sku"],0,5) }}">
<input type="hidden" name="cor[{{ $item["sku"] }}]" value="{{ $item["cor"] }}">

<tr style="border-top:1px solid #000;border-bottom:1px solid #000;">

<td class="col-md-3 normal">

@if(substr($item["sku"],0,2) == "PR")

<img src="{{ URL::asset('img/'.$item["thumb"]) }}" /> <br />

PROJECTION | {{ substr($item["sku"],0,5) }} ({{ $item["cor"] }})

<input type="hidden" name="linha[{{ $item["sku"] }}]" value="projection">

@elseif(substr($item["sku"],0,2) == "RA")

<img src="{{ URL::asset('img/'.$item["thumb"]) }}" /> <br />

RAID | {{ substr($item["sku"],0,5) }} ({{ $item["cor"] }})

<input type="hidden" name="linha[{{ $item["sku"] }}]" value="raid">

@elseif(substr($item["sku"],0,2) == "UT")

<img src="{{ URL::asset('img/'.$item["thumb"]) }}" /> <br />

ULTRA | {{ substr($item["sku"],0,5) }} ({{ $item["cor"] }})

<input type="hidden" name="linha[{{ $item["sku"] }}]" value="ultra">

@elseif(substr($item["sku"],0,2) == "KI")

<img src="{{ URL::asset('img/'.$item["thumb"]) }}" /> <br />

KIZO | {{ substr($item["sku"],0,5) }} ({{ $item["cor"] }})

<input type="hidden" name="linha[{{ $item["sku"] }}]" value="kizo">

@elseif(substr($item["sku"],0,2) == "TI")

<img src="{{ URL::asset('img/'.$item["thumb"]) }}" /> <br />

TITANIUM | {{ substr($item["sku"],0,5) }} ({{ $item["cor"] }})

<input type="hidden" name="linha[{{ $item["sku"] }}]" value="titanium">

@endif

<br />

</td>

<td class="normal col-md-3">
<input type="radio" name="lente[{{ $item["sku"] }}]" @if($item["lente"] == "semgrau") checked='checked' @endif value="semgrau" id="car-exibe-semgrau">
<span class="texto-radio"><label for="car-exibe-semgrau">SEM GRAU</label></span>
<br />

<input type="radio" name="lente[{{ $item["sku"] }}]" @if($item["lente"] == "simples") checked='checked' @endif value="simples" id="car-exibe-simples">
<span class="texto-radio"><label for="car-exibe-simples">VISÃO SIMPLES</label></span>
<div id="texto-lente">Lente: CR-39; antirreflexo/risco;<br />proteção UVA/UVB</div>

</td>

<td class="normal col-md-3">

<a href="{{ URL::to('carrinho/remover/'.$sku) }}">REMOVER</a>

</td>


<td class="normal col-md-3">
	
R$ {{ number_format($item["subtotal"],2,",",".") }}

</td>


</tr>

</table>

<div id="car-detalhe-semgrau" @if($item["lente"] == "semgrau") style='display:block' @elseif($item["lente"] == "simples") style='display:none;' @endif>

<table class="col-md-12" id="carrinho">
<tr style="border:none;">
<td class="col-md-12 header" colspan="2" style="text-align:left;">TIPOS DE LENTES</td>
<!-- <td>R$ {{ number_format($item["valor_lente"],2,",",".") }}</td> -->
</tr>

<td class="normal col-md-6">
<input type="radio" name="tipo[{{ $item["sku"] }}]" @if($item["tipo"] == "normais") checked="checked" @endif value="normais" id="tipo1">
<span class="texto-radio"><label for="tipo1">PLANA</label></span>
<br />

<div id="texto-lente" style="text-align:left;">

<p>Para uso de óculos como acessório, sem correção nas lentes</p>

</div>

</td>

<td class="normal col-md-6">

<input type="radio" name="tipo[{{ $item["sku"] }}]" @if($item["tipo"] == "finas") checked="checked" @endif value="finas" id="tipo2">
<span class="texto-radio"><label for="tipo2">DEMONSTRATIVA</label></span>

<div id="texto-lente" style="text-align:left;">

<p>Se preferir confeccionar suas lentes em outra óptica, as lentes demonstrativas são lentes que já vêm com a armação, para serem substituídas posteriormente</p>

</div>

</td>

</tr>

</table>

</div>

<br />

<div id="car-detalhe-simples" @if($item["lente"] == "semgrau") style='display:none' @elseif($item["lente"] == "simples") style='display:block;' @endif>

<table class="col-md-12" id="carrinho">
<tr style="border:none;">
<td class="col-md-12 header" colspan="4" style="text-align:left;">TIPOS DE LENTES</td>
</tr>

<td class="normal col-md-3">
<input type="radio" name="tipo[{{ $item["sku"] }}]" @if($item["tipo"] == "normais") checked="checked" @endif value="normais" id="tipo1">
<span class="texto-radio"><label for="tipo1">NORMAIS</label></span>
<br />

<div id="texto-lente" style="text-align:left;">

<p>Miopia: de 0 a -2 graus</p>
<p>Hipermetropia: de 0 a +2 graus</p>
<p>Astigmatismo: de 0 a -2 graus</p>

</div>

</td>

<td class="normal col-md-3">

<input type="radio" name="tipo[{{ $item["sku"] }}]" @if($item["tipo"] == "finas") checked="checked" @endif value="finas" id="tipo2">
<span class="texto-radio"><label for="tipo2">FINAS</label></span>
<div id="texto-lente" style="text-align:left;">

<p>Miopia: de -2 a -4 graus</p>
<p>Hipermetropia: de +2 a +4 graus</p>
<p>Astigmatismo: de 0 a -4 graus</p>

</div>

</td>

<td class="normal col-md-3">

<input type="radio" name="tipo[{{ $item["sku"] }}]" @if($item["tipo"] == "finissimas") checked="checked" @endif  value="finissimas" id="tipo3">
<span class="texto-radio"><label for="tipo3">FINÍSSIMAS</label></span>
<div id="texto-lente" style="text-align:left !important;">

<p>Miopia: de -4 a -10 graus</p>
<p>Astigmatismo: de 0 a -4 graus</p>

</div>

</td>

<td class="normal col-md-3">

<input type="radio" name="tipo[{{ $item["sku"] }}]" @if($item["tipo"] == "naosei") checked="checked" @endif  value="naosei" id="tipo4">
<span class="texto-radio"><label for="tipo4">NÃO SEI</label></span>

</td>

</tr>

</table>

</div>

@endforeach

</table>

<div class="col-md-12" style="text-align:right;margin-top:10px;">

<input type="submit" class="botao-comprar" value="IR PARA CHECKOUT" style="float:right;">

<a href="{{ URL::to('produtos')}}" class="botao-experimente" style="float:right;">CONTINUAR COMPRANDO</a>


</div>

</div>

</form>


</div>


@stop