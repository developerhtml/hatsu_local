@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">       

         <form id="edit-profile" class="form-horizontal" action="{{ URL::to('usuario/search/') }}" method="POST" name="form-busca">
          <fieldset>

                <div class="control-group" style="text-align:right;">                     
                  <div class="controls">
                     <div class="input-append">
                        <input class="span2 m-wrap" id="appendedInputButton"  @if (Session::has('erro_busca')) placeholder="{{ Session::get('erro_busca') }}" @else placeholder="Digite o que você procura" @endif type="text" name="busca" style="width:300px;">
                        <button class="btn btn-success" type="submit">Buscar</button>
                      </div>
                    </div>  <!-- /controls -->      
                 </div> <!-- /control-group -->
         </fieldset>
        </form>

            
          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-cogs"></i>
              <h3>Gerenciamento de Usuários</h3>
            </div>
            <!-- /widget-header -->

            <!-- exibindo message da session -->
            @if (Session::has('message'))
              <div class="alert alert-info" style="margin-top:10px;background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;">{{ Session::get('message') }}</div>
            @endif

            @if (count($usuarios)<=0)

              <div class="widget-content">
            
              <div class="alert alert-info" style="margin:10px;background:#fff59b;color:#000;border:1px solid #ccc;text-align:center;font-size:14px;">Sua busca não retornou nenhum resultado</div>

              </div>
              <!-- /widget-content --> 
            </div>
            <!-- /widget --> 

            @else 

            <div class="widget-content">

              <table class="table table-striped table-bordered">
                <thead>
                  <tr>

                    <th>                     
                    @if ($sortby == 'tipo' && $order == 'asc') {{
                        link_to_action('UserController@getIndex','Tipo',
                            array(
                                'sortby' => 'tipo',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('UserController@getIndex','Tipo',
                            array(
                                'sortby' => 'tipo',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif  
                    </th>

                    <th> 

                    @if ($sortby == 'nome' && $order == 'asc') {{
                        link_to_action('UserController@getIndex','Nome',
                            array(
                                'sortby' => 'nome',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('UserController@getIndex','Nome',
                            array(
                                'sortby' => 'nome',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif

                    </th>

                    <th> 

                    @if ($sortby == 'email' && $order == 'asc') {{
                        link_to_action('UserController@getIndex','E-mail',
                            array(
                                'sortby' => 'email',
                                'order' => 'desc'
                            )
                        )
                    }}
                    @else {{
                        link_to_action('UserController@getIndex','E-mail',
                            array(
                                'sortby' => 'email',
                                'order' => 'asc'
                            )
                        )
                    }}
                    @endif

                    </th>

                    <th class="td-actions"> Ações </th>

                  </tr>
                </thead>

               <tbody>
                  @foreach ($usuarios as $usuario)
              <tr>
                
                <td>@if($usuario->tipo == "1") {{ "Administrador " }} @elseif($usuario->tipo == "2") {{ "Padrão" }} @endif</td>

                <td>{{ $usuario->nome }}</td>

                <td>{{ $usuario->email }}</td>

                <td class="td-actions">

                <a href="{{ URL::to('usuario/edit/' . $usuario->id) }}" class="btn btn-small btn-warning">

                <i class="btn-icon-only icon-pencil"></i></a>

                <a href="{{ URL::to('usuario/destroy/' . $usuario->id) }}" class="btn btn-danger btn-small">

                <i class="btn-icon-only icon-remove"> </i></a></td>

            </tr>
            
        @endforeach

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 

        @endif

        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
@stop