@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">          
            
            <div class="widget ">
              
              <div class="widget-header">
                <i class="icon-cogs"></i>
                <h3>Editando Usuário {{ $usuario->nome }}</h3>
            </div> <!-- /widget-header -->
          
          <div class="widget-content">
            
            <div class="tabbable">

         <form id="edit-profile" class="form-horizontal" action="{{ URL::to('usuario/update/'.$usuario->id) }}" method="POST" name="form-usuarios">

         <fieldset>

          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-signin"></i>
              <h3>Dados de Acesso</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div id='calendar' style='padding-top:15px;'>   

                  <div class="control-group">           
                      <label class="control-label" @if($errors->has('tipo')>0) style='color:#ff0000' @endif >Tipo:</label>

                      <div class="controls">
                      <label class="radio inline">
                        <input type="radio" name="tipo" class="radio1" value="1" @if(Input::old('tipo') == "1") {{ 'checked="checked"' }} @elseif($usuario->tipo == "1") {{ 'checked="checked"' }} @endif > Administrador
                      </label>
                      
                      <label class="radio inline">
                        <input type="radio" name="tipo" class="radio1" value="2" @if(Input::old('tipo') == "2") {{ 'checked="checked"' }} @elseif($usuario->tipo == "2") {{ 'checked="checked"' }} @endif > Usuário Padrão
                      </label>

                      @if($errors->has('tipo')>0) <p class="error"> Selecione uma das opções acima  </p> @endif

                    </div>  <!-- /controls -->      
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="username">Nome de Usuário</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('username')>0) style='border-color:#ff0000;' @endif id="username" name="username" @if(Input::old('username')) value="{{ Input::old('username') }}" @else value="{{ $usuario->username }}" @endif >
                        @if($errors->has('username')>0) <p class='error'> {{ $errors->first('username') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group" style="margin-left:30px;">                     
                        <a href="#" id="alterar_senha" name="alterar_senha">Clique aqui caso queira alterar sua senha </a>
                    </div> <!-- /control-group -->

                  <div id="div1" @if(!empty(Input::old('password'))) style='display:block' @endif>

                    <div class="control-group">                     
                      <label class="control-label" for="password">Nova Senha</label>
                      <div class="controls">
                        <input type="password" class="span4" @if($errors->has('password')>0) style='border-color:#ff0000;' @endif @if($errors->has('c_password')>0) style='border-color:#ff0000;' @endif id="password" name="password">
                        @if($errors->has('password')>0) <p class='error'> {{ $errors->first('password') }} </p> @endif
                        @if($errors->has('c_password')>0) <p class='error'> {{ $errors->first('c_password') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="c_password">Confirme sua Nova Senha</label>
                      <div class="controls">
                        <input type="password" class="span4" @if($errors->has('c_password')>0) style='border-color:#ff0000;' @endif id="c_password" name="c_password">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
                    </div>

              </div>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->

       <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-user"></i>
              <h3>Dados de Pessoais</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div id='calendar' style='padding-top:15px;'>            

                <div class="control-group">  
 
                    <div class="control-group">                     
                      <label class="control-label" for="name">Nome Completo</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('nome')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" @if(Input::old('nome')) value="{{ Input::old('nome') }}" @else value="{{ $usuario->nome }}" @endif >
                        @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="email">Email</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" @if(Input::old('email')) value="{{ Input::old('email') }}" @else value="{{ $usuario->email }}" @endif >
                        @if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="telefone">Telefone</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('telefone')>0) style='border-color:#ff0000;' @endif id="telefone" name="telefone" @if(Input::old('telefone')) value="{{ Input::old('telefone') }}" @else value="{{ $usuario->telefone }}" @endif >
                        @if($errors->has('telefone')>0) <p class='error'> {{ $errors->first('telefone') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="celular">Celular</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('celular')>0) style='border-color:#ff0000;' @endif id="celular" name="celular" @if(Input::old('celular')) value="{{ Input::old('celular') }}" @else value="{{ $usuario->celular }}" @endif >
                        @if($errors->has('celular')>0) <p class='error'> {{ $errors->first('celular') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

              </div>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->


                  <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Editar</button> 
                      
                      <a href="{{ URL::to('usuario') }}"><button class="btn" type="button">Retornar</button></a>

                  </div> <!-- /form-actions -->
                </fieldset>
              </form>
              </div>
              
            </div>
            
          </div> <!-- /widget-content -->
            
        </div> <!-- /widget -->
            
        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->

<script>
$('#alterar_senha').toggle(function() {
  $('#div1').show()
  }, function() {
  $('#div1').hide()
});
</script>

@stop