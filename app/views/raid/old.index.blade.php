@extends('template')

@section('seo')

<!-- Title here -->
<title>Linha Raid | Óculos Flexível | Óculos de grau| Hatsu | Óculos de Alta Tecnologia</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil. Produtos de primeira linha com pagamento em até 10 vezes sem juros.">
<meta name="keywords" content="óculos flexível, hatsu, óculos de grau, óculos de graus, comprar óculos, óculos importados, óculos miopia, óculos de alta tecnologia, óculos femininos, óculos masculinos, óculos inquebrável, lentes bifocais, lentes,raid, projection, titanium, óculos titânio">
<meta name="author" content="Hatsu - 2014 - Todos os direitos reservados">
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="2 days" />
<link rel="canonical" href="http://www.hatsu.com.br" />
<link rel="index" title="Óculos de grau, armações e lentes | Hatsu" href="http://www.hatsu.com.br" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<!-- Page content starts -->

<div class="content">

	<div class="container">


		<div class="row">

			<div class="col-md-12 responsive" id="linha-raid">

			<h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;">RAID</h1>

			</div>

		<div class="row">

			<div class="col-md-12">

              A linha Raid é moderna e arrojada. As armações de Polifenilsulfona (PPSU) são leves e flexíveis. Pesando apenas 10 gramas e com design ergonômico, as armações Raid são ideais para prática de esportes ou no uso cotidiano. Apesar da leveza e flexibilidade, as propriedades da Polifenilsulfona garantem resistência e durabilidade, para você esquecer que usa óculos.
				 
			</div>

		</div>

		<br />

			<h2 style="border-bottom:1px solid #000;">MODELOS</h2>

			{{ Helper::LinhaProdutos(2) }}

			<p>&nbsp;</p>

			<h2 style="border-bottom:1px solid #000;">ATRIBUTOS</h2>

			<div class="content features-two">
			  <div class="container">
			    <div class="row">
					<div class="col-md-4">
						<img src="{{ URL::asset('img/linha-raid/10-gramas.jpg') }}" />
						<br />
						<h4>ARMAÇÃO DE 10 GRAMAS</h4>
						<p>Feitas de Polifenilsulfona (PPSU), a armação da linha Raid pesa apenas 10 gramas, não forçando nariz e orelha durante o dia a dia. Com ponteiras de elastômero, a aderência ao rosto é perfeita.
						</p>
						</div>  

			  			<div class="col-md-4">
						<img src="{{ URL::asset('img/linha-raid/queda.png') }}" />
						<br />
						<h4>RESISTENTE A IMPACTOS</h4>
						<p>As propriedades do polímero de alto desempenho, Polifenilsulfona, garantem à armação resistência a impactos, torções e até produtos químicos.

						</p>
						</div>  

						<div class="col-md-4">
						<img src="{{ URL::asset('img/linha-raid/flexivel.png') }}" />
						<br />
						<h4>FLEXIBILIDADE AO EXTREMO</h4>
						<p>Com a alta resistência a torções, a armação possui a capacidade de ser extremamente flexível e adaptativa ao seu rosto, sem deformar sua forma original quando submetida à stress.
						</p>
						</div>  

			    </div>
			  </div>
			</div>

		</div>

		</div>

		</div>

	</div>


<!-- Page content ends -->


@stop