@extends('template')

@section('seo')

<!-- Title here -->
<title>Linha Raid | Hatsu | Óculos de Alta Tecnologia</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil. Produtos de primeira linha com pagamento em até 10 vezes sem juros.">
<meta name="keywords" content="titânio, hatsu, óculos de grau, óculos de graus, comprar óculos, óculos importados, óculos miopia, óculos de alta tecnologia, óculos femininos, óculos masculinos, óculos inquebrável, lentes bifocais, lentes,raid, projection, titanium, óculos titânio">
<meta name="author" content="Hatsu - 2014 - Todos os direitos reservados">
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="2 days" />
<link rel="canonical" href="http://www.hatsu.com.br" />
<link rel="index" title="Óculos de grau, armações e lentes | Hatsu" href="http://www.hatsu.com.br" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

	<div class="container-fluid" style="background-color:#EDEDED;">

		<div class="row">

			<div class="col-md-12" id="linha-raid">

			<div class="col-md-7">

			&nbsp;

			</div>

			<div class="col-md-3" id="info-raid">

			<p class='titulo-raid'>RAID</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

	</div>

	</div>	

		<div class="container-fluid" id="tres_textos_raid">

			<div class="col-md-1">

			&nbsp;

			</div>

			  <div class="col-md-3" style="text-align:center;margin-top:-10px">

			  <p class="raid-titulo-box"><span>LENTES CR-39</span><br />GRATUITAS</p>

			</div>

			  <div class="col-md-4" style="text-align:center;">

			  <p class="raid-titulo-box">EXTREMAMENTE<br /><span>FLEXÍVEL</span></p>

			</div>

			  <div class="col-md-3" style="text-align:center;">

			  <p class="raid-titulo-box">APENAS<br /><span>10 GRAMAS</span></p>

			</div>

			<div class="col-md-1">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="tecnologia-raid">

			<div class="col-md-2 titulo_tecnologia_raid">

			TECNOLOGIA

			</div>

			<div class="col-md-7" style="text-align:center;">

			<p class="raid_texto_tecnologia">

			<span>PROPRIEDADES INIGUALÁVEIS</span><br /><br />

			Usando material aeronáutico, a linha Raid pesa apenas 10 gramas, além de ser extremamente flexível

			</p>

			</div>			


			<div class="col-md-3">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="bike">

			<div class="col-md-2">

			&nbsp;

			</div>

			<div class="col-md-4" style="text-align:center;vertical-align:middle;">

				<div class="esportistas">

				<p class="titulo_esportistas">IDEAL PARA ESPORTISTAS</p>

				<br />				

				<p class="itens_esportistas">

				<span>• Flexibilidade ao extremo</span>

				<br /><br />

				Com alta resistência a torções, a armação possui a capacidade de ser extremamente flexível e adaptativa ao seu rosto

				</p>

				<br />

				<p class="itens_esportistas">

				<span>• Armação de 10 gramas</span>

				<br /><br />

				Pesando apenas 10 gramas, a armação da linha Raid não pesa no nariz e orelha.

				</p>

				<br />

				<p class="itens_esportistas">

				<span>• Resistente a impactos</span>

				<br /><br />

				As propriedades da Polifenilsulfona garantem à armação resistência a impactos, torções e até produtos químicos

				</p>


			</div>


			</div>

			<div class="col-md-6">

			&nbsp;

			</div>

		</div>

		<br style="clear:both;">


		<div class="container-fluid" id="titulo_h1">

			<div class="col-md-1">

			&nbsp;

			</div>

			  <div class="col-md-9">

			  <p>ATRIBUTOS</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="projection-materiais">

			<div class="col-md-2">

			&nbsp;

			</div>


			<div class="col-md-8" style="text-align:center;">

			<p class="texto_materiais">

			<span>MATERIAIS SOFISTICADOS</span><br /><br />

			As propriedades do PPSU permitem cortes angulados e afiados ao mesmo tempo, trazendo a ergonomia necessária para amantes de atividades físicas.

			</p>

			</div>			

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<br style="clear:both;" />
		<br style="clear:both;" />

		<div class="container-fluid" id="atributos">

			&nbsp;

		</div>

		<div class="container-fluid" id="atributos_mobile" style="display:none;">

			<div class="col-md-4">

			<img src="{{ URL::asset('img/linha-raid/ponteira.png') }}">

<br />
			<br />

			<p>

			<span><b style="font-family:roboto-regular;font-size:18px;">Elastômetro</b></span><br />

			Com ponteira de elastômetro a aderência torna-se mais confiáveç e mais agradável
			</p>

			<br />

			</div>

			<div class="col-md-4">

			<img src="{{ URL::asset('img/linha-raid/aluminio.png') }}">

<br />
			<br />

			<p>

			<span><b style="font-family:roboto-regular;font-size:18px;">Camada de pó de alumínio</b></span><br />

			Com ponteira de elastômetro a aderência torna-se mais confiáveç e mais agradável
			</p>

			<br />

			</div>

			<div class="col-md-4">

			<img src="{{ URL::asset('img/linha-raid/ppsu.png') }}">

			<br />
			<br />

			<p>

			<span><b style="font-family:roboto-regular;font-size:18px;">PPSU (Polifenilsulfona)</b></span><br />
			O polímero de alto desempenho, o PPSU, proporciona flexibilidade e leveza incomparável.
			</p>

			<br />

			</div>

		</div>

			<!-- Experimente -->
			<div class="container-fluid" style="background-color:#000539;">
			<div class="col-md-2" style="text-align:center;"></div>
			<div class="col-md-5" style="text-align:center;">

			<p class="texto_materiais" style="color:#fff;">

			<br style="line-height:35px;font-size:35px" />

			<span><b>Experimente Hatsu:</b></span> Um consultor especializado vai gratuitamente até você, em data e local de sua preferência.

			</p><br /><br />

			</div>	

			<div class="col-md-3" style="text-align:center;">	
	        <div class="quero-experiment">
	        	<br style="line-height:35px;font-size:35px;" />
        		<button><a href="experimente" style="color:#fff;">Experimente</a></button><br /><br />

    		</div></div>
    		</div><BR />
			<!-- fim do Experimente -->
		<br style="clear:both;" />		

		<div class="container-fluid" id="titulo_h1">

			<div class="col-md-1"> &nbsp; </div>

			<div class="col-md-9">

			  <p>ESCOLHA O SEU</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container">

			<br />


			{{ Helper::LinhaProdutos(2) }}

			<br />
			<br />

		</div>

		<div class="container-fluid" id="titulo_h1" style="background-color:#f7fbfb;">

			<div class="col-md-1">

			&nbsp;

			</div>

			  <div class="col-md-9">

			  <p>EMBALAGEM</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="embalagem-raid">

		<div class="col-md-3">

			&nbsp;

			</div>

			<div class="col-md-6" style="text-align:center;">

			<p class="texto_embalagem-raid">

			Cuidadosamente desenvolvidas, a embalagem da linha Raid garante proteção, portabilidade e beleza

			</p>

			</div>			


			<div class="col-md-3">

			&nbsp;

			</div>

		</div>		

</div>

@stop