@extends('template')

@section('seo')

<!-- Title here -->
<title>Garantia, Trocas e Devoluções | Óculos de grau | Hatsu | Óculos de Alta Tecnologia</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil. Produtos de primeira linha com pagamento em até 10 vezes sem juros.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, comprar óculos, óculos importados, óculos miopia, óculos de alta tecnologia, óculos femininos, óculos masculinos, óculos inquebrável, lentes bifocais, lentes,raid, projection, titanium, óculos titânio">
<meta name="author" content="Hatsu - 2014 - Todos os direitos reservados">
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="2 days" />
<link rel="canonical" href="http://www.hatsu.com.br" />
<link rel="index" title="Óculos de grau, armações e lentes | Hatsu" href="http://www.hatsu.com.br" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">garantia</span>

</div>

</div>

<!-- Page content starts -->


<div class="container-fluid" >
<!--style="background-color:#EDEDED;"-->
		<div class="row">

			<div class="col-md-12" id="garantia-cover">

			<div class="col-md-7">

			&nbsp;

			</div>

			<div class="col-md-3" id="info-projection">

			<p class="titulo-projection">Garantia</p>
			<p class="titulo-projection-description">Prezamos pela satisfação dos nossos clientes e atestamos a qualidade dos nossos produtos. </p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

	</div>
	</div>

<div class="bages">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<img src='img/bages/satisfacao.png'>
				<h3>Satisfação total.<br/> Troca ou devolução assegurada.</h3>
				<p>Em caso de não adaptação de armação ou lentes, ou simplesmente por achar que a armação não ficou legal em você, asseguramos troca de modelo ou devolução total de seu dinheiro.</p>
			</div>
			<div class="col-md-4">
				<img src='img/bages/garantia-bage.png'>
					<h3>1 ano de garantia<br/> para armações e lentes</h3>
					<p>Confiando em nossos materiais e produtos, oferecemos 1 ano de garantia para armações e lentes em caso de defeito de fabricação ou queda de desempenho com o uso.</p>
			</div>
			<div class="col-md-4">
				<img src='img/bages/testado.png'>
					<h3>Triplamente testado por empresas de inspeção diferentes</h3>
					<p>Visando a qualidade de nossos produtos contamos com 3 diferentes empresas de inspeção de qualidade de nossos óculos. Duas localizadas no Japão e na Coréia e uma no Brasil.</p>
			</div>
		</div>
	</div>
<a href="https://hatsu.com.br/produtos" class="btn-conheca">conheça nossos óculos</a>
</div>

<div class=" contact-two wblock">
  <div class="container resume">
<!--

      <div class="col-md-12" style="text-align:right;">

      <h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;">GARANTIA</h1>

      </div>
-->
<div class="row">

        <div class="rblock">
          
            <div class="col-md-12" >
             
              <h4 >GARANTIA</h4>                           
           
            <div class="rinfo">
               
               <p>Confiando em nossos produtos e prezando por sua satisfação, oferecemos garantia que oferece o máximo de suporte a você e seus óculos. Caso a garantia necessite ser acionada, oferecemos trocas gratuitas ou devoluções com reembolso total, com envio pago por nós. A garantia é requisitada através do email <a href="mailto:atendimento@hatsu.com.br">atendimento@hatsu.com.br</a></p>

              <ul>

              <li><span>Caso você não se adapte as nossas armações ou lentes e quer devolvê-las, entre em contato conosco, em até 30 dias depois do recebimento dos óculos.</span></li>
              <li><span>Caso você não se adapte as nossas armações ou lentes e quer trocá-las, entre em contato conosco, em até 90 dias depois do recebimento dos óculos.</span></li>
              <li><span>Mesmo com nosso criterioso processo de avaliação individual de cada armação e lente, caso sua armação ou lente possuem algum problema de fabricação ou algum defeito após utilização, ofereceremos garantia de 1 ano a partir da data de entrega.</span></li>

              </ul>

            </div>

          </div>
          </div>
          </div>


      </div>
      </div>

      <div class="clearfix"></div>

<div class="container ">
     <div class="row">
      <div class="rblock" style="margin-bottom:30px;">
      
          <div class="col-md-12" style="padding-top:50px;">

            <h4 >TROCAS E DEVOLUÇÕES</h4>  
          
            <div class="rinfo">

            <p>
            Caso você não se adapte as nossas armações ou lentes, ou detectou algum problema de fabricação, ou seu médico errou em sua prescrição, oferecemos troca gratuitas ou devolução com reembolso total de armações e/ou lentes.</p>
           <p> As trocas e devoluções são realizadas através do envio, pago por nós, do produto de volta para nós. As trocas são realizadas o mais rápido possível e enviaremos para o endereço desejado assim que o novo produto estiver disponível.</p>
           <p> Em caso de solicitação de devolução, o reembolso será feito da mesma maneira que o pagamento foi realizado, ou seja, caso o pagamento tenha sido feito através do cartão de créditos, nós estornaremos o valor em até duas faturas. Por sua vez, se o pagamento foi realizado via boleto bancário, o reembolso será realizado em até 5 dias uteis em sua conta corrente.

            </p>

          </div>

        </div>

      <div class="clearfix"></div> 

      </div> <!-- fecha div col 12 --> 

</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div><!-- fecha div col 12 -->


@stop