<!-- Navigation -->
<div class="navbar bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
      <ul class="nav navbar-nav" style="font-size:15px;">
        <li><a href="{{ URL::to('sobre') }}" title="Conheça a Hatsu">SOBRE A HATSU</a></li>
        <li><a href="{{ URL::to('produtos') }}" title="Conheça nossa linha de produtos">PRODUTOS</a></li>
        <li><a href="{{ URL::to('duvidas') }}" title="Tire suas dúvidas sobre a Hatsu">DÚVIDAS</a></li>
        <li><a href="{{ URL::to('contato') }}" title="Entre em contato conosco">CONTATO</a></li>
        <li><a href="{{ URL::to('experimente') }}" title="Conheça o serviço Experimente em Casa">EXPERIMENTE EM CASA</a></li>
        <!-- <li><a href="{{ URL::to('showroom') }}">SHOWROOM</a></li> -->
        <li><a href="{{ URL::to('lentes') }}" title="Conheça nossas lentes">LENTES</a></li>
        <li><a href="http://blog.hatsu.com.br" target="_blank" title="Acesse nosso blog">BLOG</a></li>
        <!-- <li><a href="http://wiki.hatsu.com.br" target="_blank">WIKI</a></li> -->
        <li id="telefone-menu"><a href="#" style="cursor:normal;">(19) 3252-3760</a></li>
      </ul>
      <!-- TELEFONE FIXO -->
    <span id="telefone-topo">(19) 3252-3760</span>
    </nav>
  </div>
</div>
<!--/ Navigation End