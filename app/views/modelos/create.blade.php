@extends('template')

@section('body')

<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
           
          <div class="span12">          
            
            <div class="widget ">
              
              <div class="widget-header">
                <i class="icon-group"></i>
                <h3>Cadastrar Novo Modelo</h3>
            </div> <!-- /widget-header -->
          
          <div class="widget-content">
            
            <div class="tabbable">

         <form id="edit-profile" class="form-horizontal" action="{{ URL::to('modelo/store') }}" method="POST" name="form-clientes">

         <fieldset>

          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3>Preencha os Dados Abaixo</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div id='calendar' style='padding-top:15px;'>            

                    <div class="control-group">                     
                      <label class="control-label" for="id_produto">Linha</label>
                      <div class="controls">
                        <select name="id_produto" class="span4" @if($errors->has('id_produto')>0) style='border-color:#ff0000;' @endif>
                        <option value="">- Selecione -</option>
                        <option @if(Input::old('id_produto') == "1") {{ 'selected="selected"' }} @endif value="1">Projection</option>
                        <option @if(Input::old('id_produto') == "0") {{ 'selected="selected"' }} @endif value="2">Raid</option>
                        </select>
                        @if($errors->has('id_produto')>0) <p class='error'> {{ $errors->first('id_produto') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->  

                    <div class="control-group">                     
                      <label class="control-label" for="modelo">Modelo</label>
                      <div class="controls">
                        <input type="text" class="span4" @if($errors->has('modelo')>0) style='border-color:#ff0000;' @endif id="modelo" name="modelo" value="{{ Input::old('modelo') }}">
                        @if($errors->has('modelo')>0) <p class='error'> {{ $errors->first('modelo') }} </p> @endif
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->       

              </div>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary">Cadastrar</button> 
                      <!-- <button class="btn">Cancelar</button> -->
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>
              
            </div>
            
          </div> <!-- /widget-content -->
            
        </div> <!-- /widget -->
            
        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    

@stop