@extends('template')

@section('seo')

<!-- Title here -->
<title>Óculos de grau, armações e lentes | Hatsu | Óculos de Alta Tecnologia</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil. Produtos de primeira linha com pagamento em até 10 vezes sem juros.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importados, óculos miopia, óculos de alta tecnologia, óculos femininos, óculos masculinos, óculos inquebrável, lentes bifocais, lentes,raid, projection, titanium, óculos titânio">
<meta name="author" content="Hatsu - 2014 - Todos os direitos reservados">
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="2 days" />
<link rel="canonical" href="http://www.hatsu.com.br" />
<link rel="index" title="Óculos de grau, armações e lentes | Hatsu" href="http://www.hatsu.com.br" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<!-- Page content starts -->

</div>

<!-- <div class="content"> -->

@include('slider')

		<div class="container-fluid" id="tres_textos" style="background:#292929;">

			<div class="col-md-1">

			&nbsp;

			</div>

				<div class="col-md-3" style="text-align:center;margin-top:-10px">

			  <p class="projection-titulo-box"><span>TROCAS OU DEVOLUÇÕES</span><br />ASSEGURADAS</p>

			</div>


			  <div class="col-md-4" style="text-align:center;margin-top:-10px">

			  <p class="projection-titulo-box"><span>TRATAMENTO ESPECIAL</span><br />NAS LENTES</p>

			</div>



			  <div class="col-md-3" style="text-align:center;margin-top:-10px">

			  <p class="projection-titulo-box"><span>GARANTIA DE 1 ANO</span><br />NOS ÓCULOS</p>

			</div>

			<div class="col-md-1">

			&nbsp;

			</div>

		</div>

<!--<div class="container-fluid" id="tres_textos_home" style="background:#292929;height:auto;color:#fff;line-height:30px;padding-top:20px;padding-bottom:20px;">

	<div class="col-md-1">

	&nbsp;

	</div>

	  <div class="col-md-3" style="text-align:right;">

	  <p class="home-titulo-box" style="font-size:25px;line-height:30px;font-family:grotesque-regular;"><span style="font-size:24px;font-family:grotesque-ultralight;">LENTES COM</span><br /><b>TRATAMENTO ESPECIAL</b></p>

	</div>

	  <div class="col-md-4" style="text-align:center;">

	  <p class="home-titulo-box" style="font-size:26px;line-height:30px;padding-top:3px;font-family:grotesque-regular;"><b>TROCA OU DEVOLUÇÃO</b><br /><span style="font-size:24px;font-family:grotesque-ultralight;">ASSEGURADA</span></p>

	</div>

	  <div class="col-md-3" style="text-align:left;">

	  <p class="home-titulo-box" style="font-size:26px;line-height:30px;font-family:grotesque-regular;"><span style="font-size:17px;font-family:grotesque-ultralight;">ARMAÇÕES E LENTES COM</span><br /><b>GARANTIA DE 1 ANO</b></p>

	</div>

	<div class="col-md-1">

	&nbsp;

	</div>

</div> -->

<br style="clear:both;" />

<div class="container">

<div class="col-md-4" id="home-1">

<a href="{{ URL::to('guiadecompra') }}" title="Encontre o Hatsu ideal para você!"><img src="{{ URL::asset('img/home/guiadecompra-.png') }}" alt="Guia de Compra" class="img-responsive normal"   /></a>
<a href="{{ URL::to('guiadecompra') }}" title="Encontre o Hatsu ideal para você!"><img src="{{ URL::asset('img/home/guiadecompra-mouseover.png') }}" alt="Guia de Compra" class="img-responsive over"  /></a>

</div>


<div class="col-md-4" id="home-2">

<a href="{{ URL::to('experimente') }}" title="Conheça o serviço 'Experimente em Casa'"><img src="{{ URL::asset('img/home/experimente-.png') }}" alt="Experimente em Casa" class="img-responsive normal"   /></a>
<a href="{{ URL::to('experimente') }}" title="Conheça o serviço 'Experimente em Casa'"><img src="{{ URL::asset('img/home/experimente-mouseover.png') }}" alt="Experimente em Casa" class="img-responsive over"  /></a>

</div>

<div class="col-md-4" id="home-2">

<a href="{{ URL::to('pontosdevenda') }}" title="Conheça nossos Pontos de Venda"><img src="{{ URL::asset('img/home/pdv.png') }}" alt="Pontos de Venda" class="img-responsive normal"   /></a>
<a href="{{ URL::to('pontosdevenda') }}" title="Conheça nossos Pontos de Venda"><img src="{{ URL::asset('img/home/pdv-mouseover.png') }}" alt="Pontos de Venda" class="img-responsive over"  /></a>

</div>

</div>

<br style="clear:both;" />
<br style="clear:both;" />

{{ Helper::Destaques(25) }}

<br style="clear:both;" />
<br style="clear:both;" />



@stop