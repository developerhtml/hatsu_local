<html lang="en">
	<head>
		<meta charset="utf-8">
		<!-- Title here -->
		<title>Icaro Technologies | Hatsu | Óculos de Alta Tecnologia</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Hatsu, Óculos de Alta Tecnologia">
		<meta name="keywords" content="óculos alta tecnologia, óculos de alta tecnologia, óculos importados, óculos de grau, óculos de qualidade">
		<meta name="author" content="Hatsu">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Bootstrap CSS -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">

    <script src="{{ URL::asset('js/cidades-estados-1.2-utf8.js') }}"></script>

    <script type="text/javascript">

    function validaCampos(){

        var nome = document.cadastro.nome;
        var email = document.cadastro.email;
        var estado = document.cadastro.uf;
        var cidade = document.cadastro.city;
        //validaÃ§Ã£o do nomes
        if(nome.value ==""){
          alert("Informe seu nome!");
          nome.focus();
          return false;
        }
        //validaÃ§Ã£o para que o e-mail
        if(email.value == ""){
          alert("Informe seu e-mail corretamente");
          email.focus();
          return false;
        }

        if(estado.value ==""){
          alert("Selecione seu estado!");
          estado.focus();
          return false;
        }
      if(cidade.value ==""){
        alert("Selecione sua cidade!");
        cidade.focus();
        return false;
      }

      }
      </script>

      <script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript">
          window.onload = function() {
            new dgCidadesEstados({
              estado: document.getElementById('uf'),
              cidade: document.getElementById('city')
            });
          }
    </script>

      <style type="text/css">

      label.control-label { font-weight: bold; color:#fff; }

      h5.title { font-family: 'grotesque-regular'}

      p {line-height: 18px; color:#000;}

      div.texto {
        background: #f1f1f1;
        font-family: 'grotesque-regular';
        font-weight: bold;
        padding-top: 10px;
        font-size: 20px;
      }

      .btn-default { min-width: 140px; 
        background: green; 
        color: #fff; 
        font-family: 'grotesque-regular'; 
        font-size: 30px;
      border: none;

    }

      </style>

	</head>
	
	<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=188415054684695&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	
<div class="col-md-12" style="min-height:800px;background:#fbfbfb url('./img/background.jpg') no-repeat center top">

<div class="col-md-5" style="margin-top:80px;">
<!-- Contact form -->

  <div class="col-md-4" style="text-align:left;"><img src="{{ URL::asset('img/logo-hatsu.png') }}"></div>

  <div class="col-md-8" style="text-align:right;"><img src="{{ URL::asset('img/icaro.png') }}"></div>

  <br style='clear:both;' />

  <br style='clear:both;' />

  <h5 class="title" style="border:none;font-size:26px;">DIA DA ACUIDADE VISUAL</h5>

  <p style="line-spacing:0;">No dia <span style="color:#779329;font-size:20px;">10/07</span>, a Hatsu proporcionará a vocês da Icaro Technologies um dia para cuidar da acuidade visual de seus olhos. Levaremos um optometrista que irá realizar consultas gratuitas para quem quiser checar sua visão.</p>

  <br />

  <h5 class="title" style="border:none;font-size:26px;">QUEM SOMOS</h5>

  <p style="line-spacing:0;">Criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil, a Hatsu proporciona óculos produzidos no Japão, a partir de projetos inovadores e avançados processos de fabricação. Queremos oferecer produtos que se justifiquem por seus materiais e tecnologias.


  <br /><br />

  <div style="background: #000;
    filter:alpha(opacity=60);
    -moz-opacity:0.6;
    opacity: 0.6;
    padding:10px;margin-top:10px;text-align:center;">

    <p style="color:#fff;text-align:center;margin:10px">Caso tenha interesse em realizar a consulta no dia 10/07 e para facilitar seu atendimento, acesse o formulário abaixo e preencha os campos necessários.</p>

    <br />

    <a href="https://docs.google.com/forms/d/19RnvVTzI2aggPJTLWqxqZbFhNW3SB61OS7eDwIkg3YA/viewform" target="_blank" style="width:300px;background:green;color:#fff;padding:10px;font-size:20px;text-align:center;">ACESSAR FORMULÁRIO</a>

    <br /><br />

  </div>
  <hr />        
      <div class="center" style="background:#f1f1f1;padding:5px;">

         <div class="col-md-6 texto">Conheça a Hatsu no Facebook</div>

         <div class="fb-like-box" data-href="https://www.facebook.com/HatsuBrasil" data-width="100" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false"></div>

         &nbsp;
      </div>
</div>

</div>

		<!-- Javascript files -->
		<!-- HTML5 Support for IE -->
    <script src="{{ URL::asset('js/respond.min.js') }}"></script> 
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script> 
    <script src="{{ URL::asset('js/html5shiv.js') }}"></script> 
	</body>	
</html>