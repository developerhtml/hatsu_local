<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Title here -->
    <title>Dia dos Pais | Hatsu | Óculos de Alta Tecnologia</title>
    <!-- Description, Keywords and Author -->
    <meta name="description" content="Hatsu, Óculos de Alta Tecnologia">
    <meta name="keywords" content="óculos alta tecnologia, óculos de alta tecnologia, óculos importados, óculos de grau, óculos de qualidade">
    <meta name="author" content="Hatsu">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">   
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ URL::asset('js/valida.js') }}"></script>
    <script type="text/javascript">

    function validaCampos(){

        var nome = document.cadastro.nome;
        var email = document.cadastro.email;
        //validaÃ§Ã£o do nomes
        if(nome.value ==""){
          alert("Informe seu nome!");
          nome.focus();
          return false;
        }
        //validaÃ§Ã£o para que o e-mail
        if(email.value == ""){
          alert("Informe seu e-mail corretamente");
          email.focus();
          return false;
        }

      }
      </script>

      <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>

      <style type="text/css">

      @media (max-width: 950px){

        div#fundo {
          background: #fff;
          filter:alpha(opacity=90);
          -moz-opacity:0.9;
          opacity: 0.9;
          min-height:70px;
          margin-bottom: -26px;
        }

        p.title {
          margin-left:30px;
          margin-top:5px;
        }
      }

      label.control-label { font-family:"roboto-light"; font-size:14px; color:#000; }


      p {
        line-height: 18px; color:#000;
      }

      div.texto {
        background: #f1f1f1;
        font-family: 'grotesque-ultralight';
        font-weight: bold;
        padding-top: 10px;
        font-size: 20px;
      }

      form {
        font-family:'roboto-light';
        font-size:14px;
      }

      .btn-default { 

        min-width: 140px; 
        color: #fff; 
        font-family: 'roboto-regular'; 
        font-size: 14px;
      border: none;

    }

      </style>

  </head>
  
  <body style="background:#EAE3D4 url({{ URL::asset('./img/landing/background.jpg')}}) top right no-repeat">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=188415054684695&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  
<div class="col-md-12" style="min-height:800px;">

  <div class="col-md-5" style="margin-top:70px;">

    <div id="fundo">

      <p class="title" style="border:none;padding-top:25px;font-family:grotesque-regular;font-size:28px;height:60px;color:#000;float:left;border-right:2px solid #000;margin-right:20px;">DIA DOS PAIS&nbsp;&nbsp;</p>

      <img src="./img/logo-hatsu.png" class="width:117px;height:60px;" style="float:left;">

      &nbsp;
    </div> 

    <br style="clear:both;" />


      <div style="background: #fff;
      filter:alpha(opacity=90);
      -moz-opacity:0.9;
      opacity: 0.9;
      padding:20px;margin-top:5px;">

      <h5 class="title" style="color:#77A101;border:none;font-size:24px;font-family:grotesque-ultralight;">GANHE R$ 150 DE DESCONTO</h5>

      <p style="font-family:roboto-light;font-size:14px;margin-bottom:10px;">Preencha seu nome e email abaixo e receba um voucher com R$150 de desconto para utilizar na compra de um Projection ou um Raid.</p>
      <!-- Contact form (not workig)-->
      <form onsubmit="return validaCampos();" class="form-horizontal" name="cadastro" id="cadastro" action="enviacontato.php" enctype="multipart/form-data" method="post" >

          <!-- Name -->
          <div class="form-group">
            <label class="control-label col-md-2" for="nome">Nome</label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="nome" name="nome">
            </div>
          </div>
          <!-- Email -->
          <div class="form-group">
            <label class="control-label col-md-2" for="email1">Email</label>
            <div class="col-md-9">
              <input type="text" class="form-control col-md-3" id="email1" name="email"placeholder="Não enviamos SPAM">
            </div>
          </div>

          <!-- Buttons -->
          <div class="form-group" style="text-align:right;">
             <!-- Buttons -->
          <div class="col-md-9 col-md-offset-2">
          <button type="submit" class="btn btn-default" name="submit" style="font-family:roboto-regular;font-size:14px;background:#77A302;font-size:20px;">ENVIAR</button>
          </div>

        </div>
        <br style="clear:both;" />

        <p style="text-align:left;font-family:roboto-light;font-size:10px;color:#848484;">*válidos somente para as Linhas Projection e Raid</p>

        
      </form>
    </div>

      <div style="background: #fff;
      filter:alpha(opacity=90);
      -moz-opacity:0.9;
      opacity: 0.9;
      padding:20px;margin-top:5px;">


           <p style="font-family:roboto-regular;font-size:18px;">SOBRE NÓS</p>

           <p style="font-family:roboto-light;font-size:14px;clear:both;">Óculos produzidos no Japão e na Coréia, com design inovador e materiais de alta tecnologia, inéditos no Brasil.</p>


      </div>

      <div style="background: #fff;
      filter:alpha(opacity=90);
      -moz-opacity:0.9;
      opacity: 0.9;
      padding:20px;margin-top:5px;">

           <div class="col-md-6 texto" style="background:none;">Conheça a Hatsu no Facebook</div>

           <div class="fb-like-box" data-href="https://www.facebook.com/HatsuBrasil" data-width="100" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false"></div>

           &nbsp;
        </div>
  </div>

</div>

    <!-- Javascript files -->
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <!-- Respond JS for IE8 -->
    <script src="{{ URL::asset('js/respond.min.js') }}"></script>
    <!-- HTML5 Support for IE -->
    <script src="{{ URL::asset('js/html5shiv.js') }}"></script>
  </body> 
</html>