<html lang="en">
	<head>
		<meta charset="utf-8">
		<!-- Title here -->
		<title>Dia dos Pais | Hatsu | Óculos de Alta Tecnologia</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Hatsu, Óculos de Alta Tecnologia">
		<meta name="keywords" content="óculos alta tecnologia, óculos de alta tecnologia, óculos importados, óculos de grau, óculos de qualidade">
		<meta name="author" content="Hatsu">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">   
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ URL::asset('js/valida.js') }}"></script>
    <script type="text/javascript">

    function validaCampos(){

        var nome = document.cadastro.nome;
        var email = document.cadastro.email;
        //validaÃ§Ã£o do nomes
        if(nome.value ==""){
          alert("Informe seu nome!");
          nome.focus();
          return false;
        }
        //validaÃ§Ã£o para que o e-mail
        if(email.value == ""){
          alert("Informe seu e-mail corretamente");
          email.focus();
          return false;
        }

      }
      </script>

      <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>

      <style type="text/css">

      @media (max-width: 950px){

        div#fundo {
          background: #fff;
          filter:alpha(opacity=90);
          -moz-opacity:0.9;
          opacity: 0.9;
          min-height:70px;
          margin-bottom: -26px;
        }

        p.title {
          margin-left:30px;
          margin-top:5px;
        }
      }

      label.control-label { font-family:"roboto-light"; font-size:14px; color:#000; }


      p {
        line-height: 18px; color:#000;
      }

      div.texto {
        background: #f1f1f1;
        font-family: 'grotesque-ultralight';
        font-weight: bold;
        padding-top: 10px;
        font-size: 20px;
      }

      form {
        font-family:'roboto-light';
        font-size:14px;
      }

      .btn-default { 

        min-width: 140px; 
        color: #fff; 
        font-family: 'roboto-regular'; 
        font-size: 14px;
      border: none;

    }

      </style>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47664900-1', 'hatsu.com.br');
  ga('send', 'pageview');

</script>


	</head>
	
	<body style="background:#EAE3D4 url({{ URL::asset('./img/landing/background.jpg')}}) top right no-repeat">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=188415054684695&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	
<div class="col-md-12" style="min-height:800px;">

  <div class="col-md-5" style="margin-top:70px;">

    <div id="fundo">

      <p class="title" style="border:none;padding-top:25px;font-family:grotesque-regular;font-size:28px;height:60px;color:#000;float:left;border-right:2px solid #000;margin-right:10px;">DIA DOS PAIS&nbsp;&nbsp;</p>

      <img src="./img/logo-hatsu.png" class="width:117px;height:60px;" style="float:left;">

      &nbsp;
    </div> 

    <br style="clear:both;" />


      <div style="background: #fff;
      filter:alpha(opacity=90);
      -moz-opacity:0.9;
      opacity: 0.9;
      padding:20px;margin-top:5px;">

      @if (Session::has('message'))

      <h5 class="title" style="color:#77A101;border:none;font-size:24px;font-family:grotesque-ultralight;">{{ Session::get('message') }}</h5>
        
      @else

      <h5 class="title" style="color:#77A101;border:none;font-size:24px;font-family:grotesque-ultralight;">GANHE R$ 150 DE DESCONTO</h5>

      @endif
      

    @if (Session::has('message'))

      <br />

      <p class="menu-linha" style="line-height:26px;text-align:center;">ACESSE <a href="http://hatsu.com.br" target="_blank" style="color:#77A101">NOSSO SITE</a> E CONHEÇA NOSSOS PRODUTOS</p><br />

      <div style="text-align:center;">

      <div id="item-menu" style="width:300px;text-align:center;">

      <a href="http://hatsu.com.br/projection">

      <p class="menu-linha">PROJECTION</p>

      <p class="menu-descricao">Aro de β-Titânio e dobradiças sem parafusos</p>

      <img src="http://hatsu.com.br/public/img/produtos/PR_02_gunmetal/PR-02-Gunmetal_Thumb.jpg" />

      </a>

      </div>

      <br /><br />


      <div id="item-menu" style="width:300px;text-align:center;">

      <a href="http://hatsu.com.br/raid">

      <p class="menu-linha">RAID</p>

      <p class="menu-descricao">Flexibilidade e resistência em uma armação de 10 gramas</p>

      <img src="http://hatsu.com.br/img/produtos/RA_03_azul/RA-03-Azul_Thumb.jpg" />

      </a>

      </div>

      &nbsp;

      </div>

      @else


      <p style="font-family:roboto-light;font-size:14px;margin-bottom:10px;">Preencha seu nome e email abaixo e receba um voucher com R$150 de desconto para utilizar na compra de um Projection ou um Raid.</p>
      <!-- Contact form (not workig)-->
      <form onsubmit="return validaCampos();" class="form-horizontal" name="cadastro" id="cadastro" action="{{ URL::to('diadospais/store')}}"  method="post" >
        <br>
        <p target="_blank" style="width:250px;background:#D55163;color:#fff;padding:10px;font-size:20px;text-align:center;">PROMOÇÃO FINALIZADA</p>
        


          <!-- Name 
           <div class="form-group">
            <label class="control-label col-md-2" for="nome">Nome</label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="nome" name="nome">
            </div>
          </div>
          -->
          <!-- Email
          <div class="form-group">
            <label class="control-label col-md-2" for="email1">Email</label>
            <div class="col-md-9">
              <input type="text" class="form-control col-md-3" id="email1" name="email"placeholder="Não enviamos SPAM">
            </div>
          </div>
          -->
          <br>
         
          <p>Mantenha-se atualizado na linha de produtos da Hatsu e em nosso blog</p>
              <!--<form class="form-inline" role="form" action="{{ URL::to('http://www.hatsu.com.br/newsletter/store') }}" method="post">
                <div class="form-group">
                  <br>
                  <div class="col-md-12">

                 <input type="email" class="form-control" name="email" placeholder="Informe seu e-mail">
                 
                </div>
                </div>
                               
              </form>
            -->
            <br>
             <a href="http://hatsu.com.br/newsletter" target="_blank" style="width:250px;background:#77a302;color:#fff;padding:5px;font-size:20px;text-align:center;">NEWSLETTER</a>
              

          
          <!-- Buttons 
          <div class="form-group" style="text-align:right;"> 
             <!-- Buttons 
          <div class="col-md-9 col-md-offset-2">

          <button type="submit" class="btn btn-default" name="submit" style="font-family:roboto-regular;font-size:14px;background:#D55163;font-size:20px;">PROMOÇÃO FINALIZADA</button>
          
          </div>

        </div>
        -->
        <br style="clear:both;" />

      </form>

      @endif
      
    </div>

      <div style="background: #fff;
      filter:alpha(opacity=90);
      -moz-opacity:0.9;
      opacity: 0.9;
      padding:20px;margin-top:5px;">


           <p style="font-family:roboto-regular;font-size:18px;"><a href="http://hatsu.com.br" target="_blank" style="text-decoration:none;color:#000">SOBRE NÓS</a></p>

           <br />

           <p style="font-family:roboto-light;font-size:14px;clear:both;"><a href="http://hatsu.com.br" target="_blank" style="text-decoration:none;color:#000">Óculos produzidos no Japão e na Coréia, com design inovador e materiais de alta tecnologia, inéditos no Brasil.</a></p>

           <br />

           <p style="font-family:roboto-light;font-size:14px;clear:both;"><a href="http://hatsu.com.br" target="_blank" style="text-decoration:none;color:#000;">Acesse nosso site: www.hatsu.com.br</a></p>


      </div>

      <div style="background: #fff;
      filter:alpha(opacity=90);
      -moz-opacity:0.9;
      opacity: 0.9;
      padding:20px;margin-top:5px;">

           <div class="col-md-6 texto" style="background:none;">Conheça a Hatsu no Facebook</div>

           <div class="fb-like-box" data-href="https://www.facebook.com/HatsuBrasil" data-width="100" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false"></div>

           &nbsp;
        </div>
  </div>

</div>

		<!-- Javascript files -->
		<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
		<!-- Respond JS for IE8 -->
		<script src="{{ URL::asset('js/respond.min.js') }}"></script>
		<!-- HTML5 Support for IE -->
		<script src="{{ URL::asset('js/html5shiv.js') }}"></script>
	</body>	
</html>