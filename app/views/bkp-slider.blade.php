<!-- Slider starts -->
<div class="tp-banner-container">
<div class="tp-banner">
	<ul>	<!-- SLIDE  -->	

		<li data-transition="parallaxtoright" data-slotamount="4" data-masterspeed="700" >
			<!-- MAIN IMAGE -->
			<img src="{{ URL::asset('img/home/slider/slide-back.jpg') }}"  alt="" data-duration="2000" />	
			<!-- LAYER NR. 4 -->
			<div class="tp-caption"
				data-x="100" data-hoffset="0"
				data-y="0"
				data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
				data-speed="600"
				data-start="1300"
				data-easing="Power4.easeOut"
				data-endspeed="300"
				data-endeasing="Power1.easeIn"
				data-captionhidden="on"
				style="z-index: 2"><a href="{{ URL::to('kizo') }}"><img src="img/home/slider/banner_kizo.jpg" alt="Conheça a Linha Hatsu | Kizo" /></a>
			</div>
		</li>

			<li data-transition="parallaxtoright" data-slotamount="4" data-masterspeed="700" >
			<!-- MAIN IMAGE -->
			<img src="{{ URL::asset('img/home/slider/slide-back.jpg') }}"  alt="" data-duration="2000" />	
			<!-- LAYER NR. 4 -->
			<div class="tp-caption"
				data-x="100" data-hoffset="0"
				data-y="0"
				data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
				data-speed="600"
				data-start="1300"
				data-easing="Power4.easeOut"
				data-endspeed="300"
				data-endeasing="Power1.easeIn"
				data-captionhidden="on"
				style="z-index: 2"><a href="{{ URL::to('ultra') }}"><img src="img/home/slider/banner_ultra.jpg" alt="Conheça a Linha Hatsu | Ultra" /></a>
			</div>
		</li>

	<li data-transition="parallaxtoright" data-slotamount="4" data-masterspeed="700" >
			<!-- MAIN IMAGE -->
			<img src="{{ URL::asset('img/home/slider/slide-back.jpg') }}"  alt="" data-duration="2000" />	
			<!-- LAYER NR. 4 -->
			<div class="tp-caption"
				data-x="100" data-hoffset="0"
				data-y="0"
				data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
				data-speed="600"
				data-start="1300"
				data-easing="Power4.easeOut"
				data-endspeed="300"
				data-endeasing="Power1.easeIn"
				data-captionhidden="on"
				style="z-index: 2"><a href="{{ URL::to('projection') }}"><img src="img/home/slider/banner_projection.jpg" alt="Conheça a Linha Hatsu | Projection" /></a>
			</div>
		</li>

		<li data-transition="parallaxtoright" data-slotamount="4" data-masterspeed="700" >
			<!-- MAIN IMAGE -->
			<img src="img/home/slider/slide-back.jpg"  alt="" data-duration="2000" />	
			<!-- LAYER NR. 4 -->
			<div class="tp-caption"
				data-x="100" data-hoffset="0"
				data-y="0"
				data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
				data-speed="600"
				data-start="1300"
				data-easing="Power1.easeOut"
				data-endspeed="300"
				data-endeasing="Power1.easeIn"
				data-captionhidden="on"
				style="z-index: 2"><a href="{{ URL::to('produtos') }}"><img class="img-responsive" src="img/home/slider/banner_diferencial.jpg" alt="Hatsu - Diferenciais" /></a>
			</div>
		</li>	

		<li data-transition="parallaxtoright" data-slotamount="4" data-masterspeed="700" >
			<!-- MAIN IMAGE -->
			<img src="{{ URL::asset('img/home/slider/slide-back.jpg') }}"  alt="" data-duration="2000" />	
			<!-- LAYER NR. 4 -->
			<div class="tp-caption"
				data-x="100" data-hoffset="0"
				data-y="0"
				data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
				data-speed="600"
				data-start="1300"
				data-easing="Power4.easeOut"
				data-endspeed="300"
				data-endeasing="Power1.easeIn"
				data-captionhidden="on"
				style="z-index: 2"><a href="{{ URL::to('titanium') }}"><img class="img-responsive" src="img/home/slider/banner_titanium.jpg" alt="Conheça a Linha Hatsu | Titanium" /></a>
			</div>
		</li>
		<li data-transition="parallaxtoright" data-slotamount="4" data-masterspeed="700" >
			<!-- MAIN IMAGE -->
			<img src="img/home/slider/slide-back.jpg"  alt="" data-duration="2000" />	
			<!-- LAYER NR. 4 -->
			<div class="tp-caption"
				data-x="100" data-hoffset="0"
				data-y="0"
				data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
				data-speed="600"
				data-start="1300"
				data-easing="Power4.easeOut"
				data-endspeed="300"
				data-endeasing="Power1.easeIn"
				data-captionhidden="on"
				style="z-index: 2"><a href="{{ URL::to('raid') }}"><img class="img-responsive" src="img/home/slider/novo_raid.jpg" alt="Conheça a Linha Hatsu | Raid" /></a>
			</div>
		</li>

	</ul>
</div>
</div>