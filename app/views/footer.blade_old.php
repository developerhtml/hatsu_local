<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-3">

							<h5>FORMAS DE PAGAMENTO</h5>
							<!-- <hr /> -->
							<div class="payment-icons">
								<img src="{{ URL::asset('img/payment/n_boleto.png') }}" alt="Pagamento via Boleto" />
								<img src="{{ URL::asset('img/payment/n_visa.png') }}" alt="Pagamento via Visa" />
								<img src="{{ URL::asset('img/payment/n_master.png') }}" alt="Pagamento via Mastercard" />
								<img src="{{ URL::asset('img/payment/n_elo.png') }}" alt="Pagamento via Elo" />
								<img src="{{ URL::asset('img/payment/n_diners.png') }}" alt="Pagamento via Diners Club" />
							</div>

							<br />
							
							<h5>SITE PROTEGIDO</h5>
							<!-- <hr /> -->
							
							<!--							
							<hr style="visibility:hidden;" />

							<div id="aw_malware">
		                        <param id="aw_malware_preload" value="true" />
		                    </div>
		                    <script type="text/javascript" src="//selo.siteblindado.com/aw_malware.js"></script> 
							-->
							<img src="{{ URL::asset('img/blindado.png') }}" alt="Site Blindado" />
							
							<!-- <hr style="visibility:hidden;" /> -->

							<img src="{{ URL::asset('img/ssl.png') }}" alt="Conexão Segura" />

						</div>

					<div class="col-md-2">
						<div class="widget">
							<h5>LINHAS</h5>
							<!-- <hr /> -->
							<ul>
								<li><a href="{{ URL::to('projection')}}" title="Conheça a Linha Projection">Projection</a></li>
								<li><a href="{{ URL::to('raid')}}" title="Conheça a Linha Raid">Raid</a></li>
								<li><a href="{{ URL::to('ultra')}}" title="Conheça a Linha Ultra">Ultra</a></li>
								<li><a href="{{ URL::to('kizo')}}" title="Conheça a Linha Kizo">Kizo</a></li>
								<li><a href="{{ URL::to('titanium')}}" title="Conheça a Linha Titanium">Titanium</a></li>
							</ul>

							<!-- <br /> -->

							
						</div>
					</div>

					<div class="col-md-3">
						<div class="widget">
							<h5>LINKS ÚTEIS</h5>
							<!-- <hr /> -->
							<ul><li><a href="{{ URL::to('lentes')}}" title="Informações sobre Lentes">Lentes</a></li>
								<li><a href="{{ URL::to('experimente')}}" title="Conheça o serviço de Experimente Hatsu">Experimente Hatsu</a></li>
								<li><a href="{{ URL::to('showroom')}}" title="Solicite um showroom em seu estabelecimento">Showroom</a></li>
								<li><a href="{{ URL::to('garantia') }}" title="Política de Trocas e Devoluções">Garantia</a></li>
								<li><a href="{{ URL::to('duvidas')}}" title="Tire suas dúvidas sobre a Hatsu">Dúvidas</a></li>
								<li><a href="http://blog.hatsu.com.br" target="_blank" title="Acesse nosso Blog">Blog</a></li>
								<li><a href="http://wiki.hatsu.com.br" target="_blank" title="Acesse nossa Wiki">Wiki</a></li>
								<li><a href="{{ URL::to('contato')}}" title="Entre em contato conosco">Contato</a></li>
						</div>
					</div>
					<div class="col-md-4">
						<div class="widget">

						<h5>NEWSLETTER</h5>
							<p>Informe seu e-mail e matenha-se atualizado na linha de produtos da Hatsu e em nosso blog</p>
							<form class="form-inline" role="form" action="{{ URL::to('newsletter/store') }}" method="post">
								<div class="form-group">
									<input type="email" class="form-control" name="email" placeholder="Informe seu e-mail">
								</div>
								<button type="submit" class="btn btn-default">Cadastrar</button>
							</form> 
							<br>

							<h5>ATENDIMENTO / CONTATO</h5>
							<!-- <hr /> -->
							<i class="fa fa-home"></i> &nbsp; Rua Capitão Francisco de Paula, 61 <br />Cambuí - Campinas/SP
							<!-- <hr /> -->
							<br>
							<i class="fa fa-phone"></i> &nbsp; +55 19 3252-3760
							<!-- <hr /> --><br />
							<i class="fa fa-envelope-o"></i> &nbsp; <a href="mailto:contato@hatsu.com.br">contato@hatsu.com.br</a>
							<br><br>
							
							<h5><a href="#" data-toggle="tooltip" data-html="true" title="<p style='font-family:roboto-light;padding:5px;font-size:12px;background:#fff;color:#000;'>Pace Comunicação<br />(11) 3704-7372<br /><a href='mailto:rp@hatsu.com.br' style='color:#000'>rp@hatsu.com.br</a></p>">ASSESSORIA DE IMPRENSA</a></h5>
							<br>
							<!-- <hr /> -->
							<h5>REDES SOCIAIS</h5>
							<div class="social">
								<a href="http://www.facebook.com/HatsuBrasil" target="_blank" title="Curta nosso Facebook"><i class="fa fa-facebook"></i></a>
			                    <a href="http://twitter.com/HatsuBR" target="_blank" title="Siga nosso Twitter"><i class="fa fa-twitter twitter"></i></a>
			                    <!-- <a href="https://www.linkedin.com/company/hatsu" target="_blank" title="Siga nosso Linkedin"><i style="background:#000;" class="fa fa-linkedin linkedin"></i></a> -->
			                    <a href="https://plus.google.com/+HatsuBrasil/" target="_blank" title="Siga nosso Google+"><i class="fa fa-google-plus google-plus"></i></a>
			                    <a href="http://instagram.com/HatsuBr" target="_blank" title="Siga nosso Instagram"><i class="fa fa-instagram instagram"></i></a>
			                    <!-- <a href="http://vimeo.com/hatsu" target="_blank" title="Acesse nosso Vimeo"><i class="fa fa-vimeo-square" style="background:#000;"></i></a> -->
							</div>
						</div>
					</div>
				</div>
				<!-- <hr /> -->
				<!-- Copyright info -->
				<p class="copy"><a href="http://www.hatsu.com.br" title="Hatsu">Hatsu</a> - Óculos de Alta Tecnologia</p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</footer> 	
<!--/ Footer ends