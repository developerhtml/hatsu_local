@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>404 Página não encontrada | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')
	
    <!----------- PAGE 404 SECTION START --------------->
    <section class="page-404-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-404-content">
                    	<h1 style="font-family:grotesque-medium;font-size:32px">404 página não encontrada</h1>
                        <img src="img/goggle_404.png" class="goggle-404" alt="">
                        <h3 style="font-family:grotesque-ultralight">Opa! Não estávamos de olho nessa página</h3>
						<p sytle="font-family:robotolight;font-size:14px;">Não encontramos a página que você procurava. Fique à vontade para nos informar desse erro</p>
                        <div class="page-404-contect">
                        	<p><a href="mailto:atendimento@hatsu.com.br">atendimento@hatsu.com.br</a><span>(19) 3252-3760</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!----------- PAGE 404 SECTION END --------------->

    <!-----------PRODUCTS AREA START--------------->
    <section class="page-404-products">
    
    	{{ Helper::MeraFunc(6) }}
        
    </section>
    <!-----------PRODUCTS AREA END--------------->
    


</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div><!-- fecha div col 12 -->


@stop