@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Linha Kizo | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

	<div class="container-fluid" style="background-color:#fff;">

		<div class="row" id="linha-kizo">

			<div class="col-md-2">

			&nbsp;

			</div>

			<div class="col-md-3" id="titulo-kizo">

			<p style="color:#FF7C00;padding:20px;">KIZO</p>

			</div>

			<div class="col-md-7" id="info-kizo">

			<p class="kizo-texto">

			Não deformando quando submetida a torções ou quedas, a linha Kizo foi criada para acompanhar o dia a dia agitado das crianças, contando com material extremamente flexível e leve, inédito no Brasil.<br /><br />Veja abaixo as propriedades do Kizo e surpreenda-se.


			</p>

			</div>

	</div>

	</div>	

	<br style="clear:both;" />

	<div class="container">

		<h2 style="border-bottom:1px solid #000;text-align:center;">ESCOLHA O SEU</h2>
		<br />

		{{ Helper::LinhaProdutos(4) }}

	</div>

	<br style="clear:both;" />

		<div class="container-fluid" id="kizo-leveza">

			<div class="col-md-2">

			&nbsp;

			</div>

			  <div class="col-md-5" style="text-align:center;vertical-align:middle;color:#FAC900;margin-top:30px;">

				<p class="titulo-kizo">LEVEZA</p>

				<p class="subtitulo-kizo">6 gramas de ergonomia.</p>

				<p class="texto-kizo">A leveza da armação traz conforto extremo, uma vez que não pesa no rosto da criança não forçando assim nariz e orelha. Sendo ideal pra quem já usa óculos, e perfeito para recém adeptos.</p>

			  </div>

			  <div class="col-md-3" style="text-align:center;vertical-align:middle;">

			  <img src="{{ URL::asset('img/linha-kizo/pena.png') }}" alt="Leveza - 6 gramas de ergonomia"   />

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="info-ultem">

			<div class="col-md-2">

			&nbsp;

			</div>

			  <div class="col-md-8" style="text-align:center;vertical-align:middle;height:150px;">

			  <p class="ultem">"Armações de tecnologia usada pela NASA, inédita no Brasil"</p>

			  </div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>



		<div class="container-fluid" id="kizo-criancas">

			<div class="col-md-3">
			<!--
			&nbsp;
			-->
			</div>

			  <div class="col-md-6">

			  <img src="{{ URL::asset('img/linha-kizo/criancas.jpg') }}" alt="Ultem" class="img-responsive"   />

			</div>

			<div class="col-md-3">
			<!--
			&nbsp;
			-->
			</div>

		</div>

		<div class="container-fluid" id="kizo-flexibilidade">

			<div class="col-md-1">
				&nbsp;
			</div>

			  <div class="col-md-5" style="text-align:center;vertical-align:middle;height:150px;">
			  	<!--
				<video name="media" autoplay loop>
					<source src="{{ URL::asset('img/videos/flexibilidade.ogv') }}" type="video/ogv">
					<source src="{{ URL::asset('img/videos/flexibilidade.webm') }}" type="video/webm">
				  	<source src="{{ URL::asset('img/videos/flexibilidade.mp4') }}" type="video/mp4">
				</video>
				-->
				<img src="{{ URL::asset('img/linha-kizo/flexibilidade_kizo_final.png') }}" alt="Flexibilidade">
				<img src="{{ URL::asset('img/linha-kizo/flexibilidade.jpg') }}" class="flexibilidade img-responsive" style="display:none;">

			  </div>

			  <div class="col-md-4" style="text-align:center;vertical-align:middle;height:150px;color:#fff;padding-top:200px;">
				<p class="titulo-kizo">FLEXIBILIDADE</p>
				<p class="subtitulo-kizo">Armação quebrada? Nunca mais.</p>
				<p class="texto-kizo">A flexibilidade atingida com o uso de resina termoplástica extremamente flexível na armação garante que os óculos não quebrem ao caírem do rosto da criança e ainda ofereçam conforto e ergonomia adaptando-se a cada formato de rosto.</p>
				<p style="text-align:right;" id="casal-kizo"><img src="{{ URL::asset('img/linha-kizo/casal.png') }}" alt="Kizo - Crianças"  /></p>
			  </div>

			<div class="col-md-1">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="kizo-estabilidade" style="background:#fff">

			<div class="col-md-2">

			<img src="{{ URL::asset('img/linha-kizo/menina.png') }}" alt="Kizo - Crianças" style="margin-top:110px;" class="kizo-menina" />

			</div>			

			<div class="col-md-3" style="text-align:center;vertical-align:middle;height:150px;color:#013EBD;padding-top:100px;">

				<p class="titulo-kizo">ESTABILIDADE DIMENSIONAL</p>

				<p class="subtitulo-kizo">Adeus óculos tortos.</p>

				<p class="texto-kizo">

				Não perdendo a forma quando derrubadas ou torcidas, o Kizo permanece intacto até o limite da resistência, diferentemente de metal comum ou acetato que possuem pouca rigidez espacial.

				</p>

			  </div>

			  <div class="col-md-5" style="text-align:center;vertical-align:middle;height:37px;">
			  	<!--
				<video name="media" autoplay loop>
				  <source src="{{ URL::asset('img/videos/estabilidade.ogv') }}" type="video/ogv">
				  <source src="{{ URL::asset('img/videos/estabilidade.webm') }}" type="video/webm">
				  <source src="{{ URL::asset('img/videos/estabilidade.mp4') }}" type="video/mp4">
				</video>
				-->
				<img src="{{ URL::asset('img/linha-kizo/estabilidade.jpg') }}" alt="Estabilidade Dimensional" />								
				<img src="{{ URL::asset('img/linha-kizo/estabilidade.jpg') }}" class="estabilidade img-responsive" style="display:none;">

			  </div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id='resistencia-kizo' style="background-color:#7B44C4;height:275px;padding-top:50px;">

			<div class="col-md-2">

			&nbsp;

			</div>

			  <div class="col-md-3" style="text-align:center;vertical-align:middle;">

			  <img src="{{ URL::asset('img/linha-kizo/fogo.png') }}" alt="Resistência Térmica"   />

			</div>

			  <div class="col-md-5" style="text-align:center;vertical-align:middle;color:#FF7EEA;margin-top:15px;">

				<p class="titulo-kizo">RESISTÊNCIA TÉRMINA</p>

				<p class="subtitulo-kizo">Rigidez além de 200 graus.</p>

				<p class="texto-kizo">É comum armações de acetato perderem sua rigidez, quando por exemplo serem esquecidas em um carro em pleno verão carioca. Com temperatura de fusão acima de 200 graus Celsius, a armação do Kizo suporta altas temperaturas sem perder suas propriedades ou forma.</p>

			  </div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" style="height:auto;padding:0;margin:0;" id="kizo-trivex">

			<div class="col-md-2">
				&nbsp;
			</div>

			<div class="col-md-4" style="height:575px;text-align:right;vertical-align:middle;">
				<!--
				<video name="media" autoplay loop>
					<source src="{{ URL::asset('img/videos/trivex.ogv') }}" type="video/ogv">
					<source src="{{ URL::asset('img/videos/trivex.webm') }}" type="video/webm">
					<source src="{{ URL::asset('img/videos/trivex.mp4') }}" type="video/mp4">
				</video>
				-->
				<img src="{{ URL::asset('img/linha-kizo/trivex_hipotese.jpg') }}" alt="Lentes Trivex">
				<img src="{{ URL::asset('img/linha-kizo/trivex.jpg') }}" class="trivex img-responsive" style="display:none;">

			</div>

			  <div class="col-md-4" style="height:575px;text-align:center;vertical-align:middle;color:#FFF;background:#FF7EEA url('{{ URL::asset('img/linha-kizo/pendurado.png') }}') no-repeat top left;padding-top:290px">

				<p class="titulo-kizo">LENTES TRIVEX</p>

				<p class="subtitulo-kizo">Antirrisco, antiquebra. Grátis!</p>

				<p class="texto-kizo">Oferecidas gratuitamente as lentes Trivex® - praticamente inquebráveis - com antirrisco.</p>

			  </div>

			<div class="col-md-2" style="padding:0;margin:0;background-color:#FF7EEA;height:575px;">

			&nbsp;

			</div>

		</div>

</div>

@stop