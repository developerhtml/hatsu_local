@extends('template')

@section('seo')

<!-- SEO here -->
<title>Dúvidas (FAQ) | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">dúvidas</span>

</div>

</div>

  <div class="container" style="">

      <h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;text-align:right;padding-top:10px;">DÚVIDAS</h1>

      <div class="col-md-12" style="text-align:left;background:#f1f1f1;min-height:100px;padding-top:10px;">

      <p>As mais frequentes dúvidas estão aqui listadas por categorias</p><br />

      <p style="display:inline;line-height:22px;"><span style="font-size:18px;">PESQUISE:</span>&nbsp;&nbsp;

      <div id="form" style="display:inline;margin-top:-5px;"></div>

      </p>

      </div>

      <br style="clear:both;" />

      <div class="support container" style="margin-top:20px;width:100%;background:#fff;">

                    <!-- Lists -->
                    <ul id="slist">

                        <li style="margin-bottom:10px;">
                           <a href="#" style="text-transform:uppercase;">Experimente Hatsu

                           <span style="display:none;">
                           experimente casa lentes oftamologista consultor campinas região visitas hatsu lojas físicas online ópticas óticas 
                           internet consultores formulario equipe prescrição distância pupilar
                           </span>

                           </a>

                           <p><b><span id="question">Como funciona o serviço “Experimente Hatsu”?</span></b><br />
                           <span id="answer">Para sua comodidade, temos uma equipe treinada de consultores de vendas que levam os modelos até a sua casa e te ajudam a escolher o modelo de óculos que mais combina com seu rosto e necessidades. O consultor realiza também a medida da distância pupilar para adequar melhor as lentes a seus olhos (veja seção “Prescrição médica” > “O que é Distância Pupilar”). A visita é feita em horário agendado por você.</span><br /><br />

                           <b>Como eu agendo uma visita?</b><br />
                           Clique em “Experimente Hatsu” para preencher o formulário com seu nome, e-mail e telefone. Nós entraremos em contato com você para marcar um horário.<br /><br />

                           <b>Eu recebo os óculos no momento da compra?</b><br />
                           Não. Precisamos de alguns dias para confeccionar as lentes indicadas pelo seu oftalmologista. Você apresenta a prescrição para o consultor, ele tira uma cópia, envia para o laboratório e em poucos dias você recebe em casa os óculos com as lentes. É muito importante apresentar a prescrição ao consultor. Dessa forma as lentes são confeccionadas corretamente e não prejudicam a saúde dos seus olhos.<br /><br />

                           <b>É possível comprar os óculos Hatsu em ópticas e lojas físicas?</b><br />
                           Não. Nossos óculos são vendidos exclusivamente através de nossos consultores ou diretamente pela internet, através da nossa loja virtual. Para conhecer nossos óculos pessoalmente e comprá-los, basta solicitar uma visita em “Experimente Hatsu”.<br /><br />

                           <b>Quais são as cidades atendidas pelos consultores Hatsu?</b><br />
                           Visitamos os clientes da Região Metropolitana de Campinas. Em breve atenderemos outras cidades.</p>

                        </li>

                        <li style="margin-bottom:10px;">
                           <a href="#" style="text-transform:uppercase;">Prescrição médica

                           <span style="display:none;">
                           prescrição médica enviar receita médica óculos saúde garantir lentes enviadas adequadas corretamente imagem escaneada do receituário
                           distância pupilar dp pupila centro óptico lente alinhado desconforto confeccionar perfeição
                           consultor vendas lentes multifocais
                           oftálmicas refração óptica miopia hipermetropia astigmatismo alinhamento altura montagem
                           </span>

                           </a>

                           <p><b>Por que devo enviar minha receita médica para comprar meus óculos?</b><br />
                           A Hatsu se preocupa com a saúde dos seus olhos. Por isso, queremos garantir que as lentes enviadas sejam adequadas para você. A única maneira de montar o óculos corretamente é tendo a prescrição do seu médico em mãos. Por esse motivo solicitamos o envio de uma foto ou imagem escaneada do receituário.<br /><br />

                           <b>O que é Distância Pupilar (DP) e por que ela é importante?</b><br />
                           A distância pupilar é a medida entre uma pupila e outra. Essa medida é importante para que o centro óptico de cada lente fique alinhado com sua pupila, evitando desconforto ao usar os óculos.<br /><br />

                           <b>Minha receita não indica a distância pupilar. O que devo fazer?</b><br />
                           Se seu médico não indicou sua DP na prescrição, não se preocupe. Basta nos enviar uma foto sua, de rosto, segurando um cartão de crédito próximo à boca, com a tarja negra para frente. Com essa fotografia, somos capazes de calcular sua distância pupilar e confeccionar seus óculos com perfeição.<br /><br />

                           <b>Por que preciso solicitar um consultor de vendas para comprar óculos com Lentes Multifocais?</b><br />
                           Todas as lentes oftálmicas, ou seja, de óculos, possuem um centro óptico. Trata-se de um ponto específico na lente onde o fenômeno da refração óptica não ocorre. Para confeccionarmos lentes de Visão Simples (miopia/hipermetropia/astigmatismo), precisamos seguir apenas o alinhamento horizontal, que é feito pela distância pupilar.<br /><br />
                           Já para lentes multifocais, precisamos alinhar a lente, não só horizontalmente, mas também verticalmente com seu eixo visual, o que chamamos de altura de montagem. Essa medida muda não só de pessoa para pessoa mas também de óculos para óculos. Só conseguimos aferir essa medida vertical colocando os óculos no rosto de cada um e fazendo as medições. Não há como medir sua altura de montagem sem que você esteja com os óculos no rosto e com sua cabeça posicionada em uma posição especificamente correta para a aferição. Por isso a necessidade de um consultor.
                           </p>

                        </li>

                        <li style="margin-bottom:10px;">

                           <a href="#" style="text-transform:uppercase;">Lentes

                           <span style="display:none;">
                           tipos de lentes disponibilizadas hatsu correção ametropias: miopia, hipermetropia astigmatismo lentes visão simples multifocais correção da presbiopia vista cansada antirreflexo.
                           lentes normais finas finíssimas visão simples miopia hipermetropia astigmatismo lentes multifocais presbiopia
                           tratamento maior nitidez reduz efeito ofuscamento sensação vidro película microscópica banhando material antirreflexo.
                           refletida espectro cores olho nitidez espessura índice refração película cancelamento de ondas espectros cores visíveis dirigir computador
                           </span>

                           </a>

                           <p><b>Quais são os tipos de lentes disponibilizadas pela Hatsu?</b><br />
                           A Hatsu disponibiliza a você lentes para correção de todas as ametropias: miopia, hipermetropia e astigmatismo, que são as lentes de Visão Simples, e ainda lentes Multifocais para a correção da presbiopia, ou vista cansada. Todas as lentes vêm com antirreflexo.<br /><br />
                           Possuímos ainda a escolha entre lentes normais, finas ou finíssimas para lentes de visão simples (para miopia, hipermetropia e astigmatismo) e entre lentes multifocais (para quem possui miopia e presbiopia).
                           <br /><br />


                           <b>O que é antirreflexo?</b><br />
                           O antirreflexo é um tratamento que a lente recebe que permite maior nitidez e reduz o efeito de ofuscamento, ou seja, retira a sensação de que estamos vendo através de um vidro. Esse tratamento é uma película microscópica que se forma na lente banhando-a no material antirreflexo.<br /><br />
                            Quando uma imagem é refletida na lente, todo o espectro de cores está refletindo e deixando de chegar ao seu olho, portanto diminuindo a nitidez do que enxergamos através da lente. O tratamento define a espessura e o índice de refração da película antirreflexo para que se tenha o maior cancelamento de ondas de luz refletidas. A luz, que possui basicamente sete espectros de cores visíveis, passa a ter seis desses sete espectros não mais refletidos com o antirreflexo, fazendo com que tais espectros cheguem ao seu olho, aumentando a qualidade da imagem.<br /><br />
                           Com lentes antirreflexo, dirigir a noite torna-se mais seguro uma vez que não temos mais o reflexo dos faróis nas lentes. O uso extensivo de computador é menos danoso à saúde dos olhos com tais lente já que temos também o aumento de contraste (diferença luminosa entre um ponto claro e outro escuro).
                           <br /><br />

                           <b>As lentes tem garantia? O que está incluso nela?</b><br />
                           Todas as lentes vêm com antirreflexo, e todos os antirreflexos vêm com garantia de 1 ano contra defeitos de fabricação. Todas as lentes vêm com garantia de 3 meses de adaptação, o que significa que se seu médico errou na receita você tem até três meses para que ele faça uma nova receita e possamos refazer suas lentes sem custo algum. Ainda, as lentes feitas na resina Trivex vêm com 1 ano de garantia contra quebras e trincas.<br /><br />

                           <b>É possível adquirir somente a armação, sem as lentes?</b><br />
                           Sim, é possível. Basta na seção “Comprar” selecionar “Lentes de Grau: Sem Grau” e depois escolher em “Tipo de lente” entre “Demonstrativas” (para substituir por outra lente) ou “Planas” (Se você quer usar os óculos apenas como acessório).<br /><br />

                           <b>A Hatsu oferece lentes para armações de outras marcas?</b><br />
                           Não. Oferecemos as lentes para completar nossas armações, não as vendemos de forma separada.<br /><br />

                        </li>

                        <li style="margin-bottom:10px;">

                           <a href="#" style="text-transform:uppercase;">Trocas e Devoluções

                           <span style="display:none;">

                           armações lentes detectou problema fabricação médico prescrição troca gratuitas devolução reembolso total
                           trocas devoluções envio pago produto rápido possível produto disponível termos garantia
                           solicitação reembolso pagamento cartão crédito boleto bancário conta corrente
                           
                           </a>

                           </a>

                           <p>
                           Caso você não se adapte as nossas armações ou lentes, ou detectou algum problema de fabricação, ou seu médico errou em sua prescrição, oferecemos troca gratuitas ou devolução com reembolso total de armações e/ou lentes.<br /><br />
                           As trocas e devoluções são realizadas através do envio, pago por nós, do produto de volta para nós. As trocas são realizadas o mais rápido possível e enviaremos para o endereço desejado assim que o novo produto estiver disponível.<br /><br />
                           Em caso de solicitação de devolução, o reembolso será feito da mesma maneira que o pagamento foi realizado, ou seja, caso o pagamento tenha sido feito através do cartão de créditos, nós estornaremos o valor em até duas fátuas. Por sua vez, se o pagamento foi realizado via boleto bancário, o reembolso será realizado em até 5 dias uteis em sua conta corrente.<br /><br />
                           Trocas e devoluções são efetuadas dentro dos <a href="{{ URL::to('garantia')}}" style="display:inline;text-decoration:none;cursor:pointer;" onclick="location.href='{{ URL::to('garantia')}}';">“Termos de Garantia”</a>
                           </p>

                        </li>


                        <li style="margin-bottom:10px;">
                           <a href="#" style="text-transform:uppercase;">Cuidados e manutenção 

                           <span style="display:none;">

                           Quais cuidados devo tomar com os óculos? Como devo limpá-los?
                           Tanto a armação quanto as lentes merecem cuidados especiais. Lave seus óculos com água fria e detergente neutro ou utilize um pano umedecido e sabão neutro. Seque bem com pano macio ou toalha de papel. Não utilize produtos ácidos ou alcalinos, detergentes cosméticos, produtos para cabelo ou óleos para a limpeza dos óculos. Tais produtos podem alterar a composição ou coloração da armações e remover as camadas de revestimento.
                           Estou notando alguma alteração na armação ou dobradiça de meus óculos. O que devo fazer?
                           Sempre solicite a um consultor para fazer a manutenção dos seus óculos. Ele tem o conhecimento e as ferramentas adequadas para fazer os ajustes necessários.

                           </a>

                           <p><b>Quais cuidados devo tomar com os óculos? Como devo limpá-los?</b><br />
                           Tanto a armação quanto as lentes merecem cuidados especiais. Lave seus óculos com água fria e detergente neutro ou utilize um pano umedecido e sabão neutro. Seque bem com pano macio ou toalha de papel. Não utilize produtos ácidos ou alcalinos, detergentes cosméticos, produtos para cabelo ou óleos para a limpeza dos óculos. Tais produtos podem alterar a composição ou coloração da armações e remover as camadas de revestimento.<br /><br />

                           <b>Estou notando alguma alteração na armação ou dobradiça de meus óculos. O que devo fazer?</b><br />
                           Sempre solicite a um consultor para fazer a manutenção dos seus óculos. Ele tem o conhecimento e as ferramentas adequadas para fazer os ajustes necessários.<br /><br />

                        </li>

                   </ul>

               </div>
               

               <div class="col-md-12" style="text-align:center;border-top:1px solid #000;border-bottom:1px solid #000;padding-top:20px;padding-bottom:20px;">

               <div class="col-md-4" style="border-right:1px solid #000;min-height:185px;">
               
               <br /><br /><img src="{{ URL::asset('img/icons/duvida.png') }}" alt="Dúvidas" /><br />

               <h3>DÚVIDAS?</h3>

               </div>

               <div class="col-md-4" style="border-right:1px solid #000;min-height:185px;">

               <img src="{{ URL::asset('img/icons/telephone.png') }}" alt="Ligue pra Gente" /><br /><br />

               <h3>LIGUE PRA GENTE</h3>

               <p>(19) 3252-3760</p>

               <p>Segunda a sexta, das 8h as 18h</p>

               </div>

               <div class="col-md-4" style="min-height:185px;">
               
               <img src="{{ URL::asset('img/icons/carta.png') }}" alt="E-mail" /><br /><br />

               <h3>MANDE UM E-MAIL</h3>

               <p><a href="mailto:atendimento@hatsu.com.br">atendimento@hatsu.com.br</a></p>

               <p>Respondemos em menos de 24 horas</p>

         </div>

            </div>
      </div>

      </div>
   </div>

@stop