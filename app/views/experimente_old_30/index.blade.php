@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Experimente Hatsu | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">experimente hatsu</span>

</div>

</div>

<!-- Page content starts -->

<div class="content contact-two">

  <div class="container resume">

    <div class="row"> 
      
      <div class="col-md-12" style="text-align:right;">

      <h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;">EXPERIMENTE HATSU</h1>

      </div>


    <div class="content resume">
      <div class="container">
      <div class="row">
        <div class="col-md-12">

          <!-- <div class="bor"></div> -->
          <br style="clear:both;" />
          <div class="clearfix"></div>

          <div class="rblock">
            <div class="row">
             <div class="col-md-2" style="text-align:center;">
              <!-- title -->
              <h4 style="text-transform:uppercase;text-align:center;">O que é "Experimente Hatsu"?</h4>                           
             </div>
             <div class="col-md-10">
              <div class="rinfo">
                 <br /><p>Experimente Hatsu é um serviço gratuito oferecido pela Hatsu onde um consultor leva nossos óculos até você e te ajuda a escolher o modelo de óculos que mais combina com seu rosto e necessidades diárias. O consultor realiza também medidas do seu rosto para confeccionar as lentes de forma mais adequada à seu rosto e armação.</p>
              </div>
             </div>
            </div>
           </div>

<div class="clearfix"></div>

<br style="clear:both;" />

<h2 style="text-transform:uppercase;">Como funciona o serviço?</h2>

<br style="clear:both;" />

           <div class="row" style="text-align:center;">

          <div class="col-md-about">
            <div class="feat-inner">
              <img src="{{ URL::asset('img/experimente/1-requisite.png') }}" alt="1- Requisite o serviço"  />
              <h4>1. Requisite o Serviço </h4>
              <p>Preencha os campos encontrados no final da página e entraremos em contato</p>
            </div>
          </div>

          <div class="col-md-about">
            <div class="feat-inner">
              <img src="{{ URL::asset('img/experimente/2-agende.png') }}" alt="2- Agende uma visita" />
              <h4>2. Agende uma visita</h4>
              <p>Ao ligarmos para você, agendamos horário e local de sua preferência</p>
            </div>
          </div>

          <div class="col-md-about">
            <div class="feat-inner">
              <img src="{{ URL::asset('img/experimente/3-escolha.png') }}" alt="3- Escolha os óculos" />
              <h4>3. Escolha os Óculos</h4>
              <p>Levaremos todos os nossos óculos para que você decida qual mais adequado as suas necessidades</p>
            </div>
          </div>  
 
          <div class="col-md-about">
            <div class="feat-inner">
              <img src="{{ URL::asset('img/experimente/4-compre.png') }}" alt="4- Compre" />
              <h4>4. Compre na Hora</h4>
              <p>Nosso consultor pode realizar a venda na hora, com o método de pagamento de sua preferência.</p>
            </div>
          </div>         

          <div class="col-md-about">
            <div class="feat-inner">
              <img src="{{ URL::asset('img/experimente/5-receba.png') }}" alt="5- Receba seus óculos" />
              <h4>5. Receba seus Óculos</h4>
              <p>Confeccionaremos suas lentes e em poucos dias enviaremos seus óculos</p>
            </div>
          </div>

          </div>         

          <div class="clearfix"></div>
          <br style='clear:both' />

          <div class="rblock">
            <div class="row">
             <div class="col-md-2" style="text-align:center;">
              <h4 style="text-transform:uppercase;">É realmente de graça?</h4>                           
             </div>
             <div class="col-md-10">
              <div class="rinfo">
                 <p>Sem compromisso nenhum, oferecemos este serviço para melhor lhe atender. Caso você requisite o serviço, o consultor for até você mas nenhum óculos lhe agradar você não paga nada. Assim como se a compra for realizada através do serviço Experimente Hatsu, não haverá acrescimo no preço apresentado no site.</p>
              </div>
             </div>
            </div>
           </div>

            <div class="rblock">
            <div class="row">
             <div class="col-md-2" style="text-align:center;">
              <h4 style="text-transform:uppercase;">Quais cidades são atendidas?</h4>                           
             </div>
             <div class="col-md-10">
              <div class="rinfo">
                 <p>Atualmente atendemos a Grande São Paulo e a Região Metropolitana de Campinas. Em breve expandiremos o serviço para atender a maior quantidade possível de cidades. Caso sua cidade não for atendida atualmente, lembre-se que a compra diretamente do nosso site possui frete grátis, e caso você queira devolver os óculos, devolvemos seu dinheiro com a devolução do produto.</p>
              </div>
             </div>
            </div>
           </div>

      </div>
    </div>

      <br style="clear:both;" />

      @if (Session::has('message'))
        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2>
      @endif

      <p style="text-align:center;">

      <div class="col-md-12">
     <!-- Contact form -->
        <h4 class="title" style="text-align:center;">SOLICITE UMA VISITA</h4>

        <div class="form" style="text-align:center;">
          <!-- Contact form (not working)-->
          <form class="form-horizontal" action="{{ URL::to('experimente/store') }}" method="post">
              
              @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif

              <!-- Name -->
              <div class="form-group">
                <label class="control-label col-md-2" for="name1">Nome</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('nome')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" value="{{ Input::old('nome') }}" placeholder="Informe seu nome completo">
                </div>
              </div>

              @if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
              <!-- Email -->
              <div class="form-group">
                <label class="control-label col-md-2" for="email1">E-mail</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" value="{{ Input::old('email') }}" placeholder="Informe seu e-mail">
                </div>
              </div>

              @if($errors->has('telefone')>0) <p class='error'> {{ $errors->first('telefone') }} </p> @endif
              <!-- Email -->
              <div class="form-group">
                <label class="control-label col-md-2" for="telefone">Telefone</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('telefone')>0) style='border-color:#ff0000;' @endif id="telefone" name="telefone" value="{{ Input::old('telefone') }}" placeholder="Telefone ou Celular">
                </div>
              </div>

              @if($errors->has('estado')>0) <p class='error'> {{ $errors->first('estado') }} </p> @endif

                <div class="form-group">
                <label class="control-label col-md-2">Estado</label>
                <div class="col-md-9">                               
                  <select class="form-control" id="estado" name="estado" @if($errors->has('estado')>0) style='border-color:#ff0000;' @endif>
                  <option value="">- Selecione -</option>
                    @foreach(Estado::all() as $estado)
                        <option @if(Input::old('estado') == $estado->id) {{ 'selected="selected"' }} @endif value="{{ $estado->id }}">{{ $estado->nome }}</option>
                    @endforeach
                  </select>
                </div>
                </div>  

                @if($errors->has('cidade')>0) <p class='error'> {{ $errors->first('cidade') }} </p> @endif    

                <div class="form-group">
                <label class="control-label col-md-2">Cidade</label>
                <div class="col-md-9">                               
                  <select class="form-control" id="cidade" name="cidade" @if($errors->has('cidade')>0) style='border-color:#ff0000;' @endif>
                    @if (Input::old('cidade'))
                        @foreach(Cidade::where('uf','=',Cidade::find(Input::old('cidade'))->uf)->get() as $cidade)
                          <option value='{{ $cidade->id }}' @if($cidade->id == Input::old('cidade')) selected='selected' @endif >{{ $cidade->nome }}</option>
                        @endforeach
                      @else
                        <option value="">- Selecione -</option>
                      @endif
                    </select>
                  </div>
                </div>  

              @if($errors->has('obs')>0) <p class='error'> {{ $errors->first('obs') }} </p> @endif

              <!-- Comment -->
              <div class="form-group">
                <label class="control-label col-md-2" for="obs">Observações</label>
                <div class="col-md-9">
                  <textarea class="form-control" id="obs" name="obs" rows="3" placeholder="Informe o melhor horário para agendarmos a visita! Nosso atendimento entrará em contato o mais breve possível." @if($errors->has('obs')>0) style='border-color:#ff0000;' @endif>{{ Input::old('obs') }}</textarea>
                </div>
              </div>
              <!-- Buttons -->
              <div class="form-group">
                 <!-- Buttons -->
         <div class="col-md-9 col-md-offset-4">
          <input type="submit" class="botao-comprar" value="ENVIAR">
         </div>
              </div>
          </form>
        </div>

</div>

</p
>

</div>

</div>

</div>

</div>

</div>

</div>
<!-- Page content ends -->


@stop