@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Newsletter | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">newsletter</span>

</div>

</div>

<!-- Page content starts -->

<div class="content contact-two">

  <div class="container resume">

    <div class="row"> 
      
      <div class="col-md-12" style="text-align:right;">

      <h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;">NEWSLETTER</h1>

      </div>

      <br style="clear:both;" />

      @if (Session::has('message'))
        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2>
      @endif

      <p style="text-align:center;">

      <div class="col-md-12">

        <div class="form" style="text-align:center;">
          <!-- Contact form (not working)-->
          <form class="form-horizontal" action="{{ URL::to('newsletter/store') }}" method="post">
              
              @if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
              <!-- Email -->
              <div class="form-group">
                <label class="control-label col-md-2" for="email1">E-mail</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" value="{{ Input::old('email') }}" placeholder="Informe seu e-mail">
                </div>
              </div>


              <!-- Buttons -->
              <div class="form-group">
                 <!-- Buttons -->
         <div class="col-md-9 col-md-offset-2">
          <button type="submit" class="btn btn-success" style="background-color:#77A302;text-transform:uppercase;width:200px;">CADASTRAR</button>
         </div>
              </div>
          </form>
        </div>

</div>

</p>


  </div>
</div>

</div>

</div>

<!-- Page content ends -->


@stop