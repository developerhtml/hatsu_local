@extends('template')

@section('seo')

<!-- Title here -->
<title>Linha Projection | Óculos de grau, armações e lentes | Hatsu | Óculos de Alta Tecnologia</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil. Produtos de primeira linha com pagamento em até 10 vezes sem juros.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importados, óculos miopia, óculos de alta tecnologia, óculos femininos, óculos masculinos, óculos inquebrável, lentes bifocais, lentes,raid, projection, titanium, óculos titânio">
<meta name="author" content="Hatsu - 2014 - Todos os direitos reservados">
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="2 days" />
<link rel="canonical" href="http://www.hatsu.com.br" />
<link rel="index" title="Óculos de grau, armações e lentes | Hatsu" href="http://www.hatsu.com.br" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<!-- Page content starts -->

<div class="content">

	<div class="container">


		<div class="row">

			<div class="col-md-12 responsive" id="linha-projection">

			<h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;">PROJECTION</h1>

			</div>

		<div class="row">

			<div class="col-md-12">

              A linha Projection concentra materiais de alto desempenho, utilizados em processos que unem o cuidado da montagem manual, com avançados processos industriais, tudo para tornar o uso dos óculos uma experiência muito mais agradável. Com projeto inovador, a linha Projection não utiliza parafuso em suas dobradiças. Baseada em lemes aeronáuticos, as dobradiças trazem movimentos suaves nas articulações, aumentando também resistência e durabilidade. 
				 
			</div>

		</div>

		<br />

			<h2 style="border-bottom:1px solid #000;">MODELOS</h2>

			{{ Helper::LinhaProdutos(1) }}

			<p>&nbsp;</p>

			<h2 style="border-bottom:1px solid #000;">COMO É FEITO?</h2>

			<div class="content features-two">
			  <div class="container">
			    <div class="row">
					<div class="col-md-4">
						<img src="{{ URL::asset('img/linha-projection/como-e-feito-selecao-de-materiais.jpg') }}" alt="Seleção de materiais" class="img-responsive" style="max-width:310px" />
						<br />
						<h4>SELEÇÃO DE MATERIAIS</h4>
						<p>Acetato de primeira qualidade e materiais de alta tecnologia são empregados em nossos óculos, como Resina Termicamente Reversível - também usada em componentes de ônibus espaciais, titânio, beta titânio, policarbonato e plaquetas de Nucrel® - material antialérgico, proporcionando assim a perfeita combinação entre acabamento de alta qualidade e resistência.
						</p>
						</div>  

			  			<div class="col-md-4">
						<img src="{{ URL::asset('img/linha-projection/como-e-feito-processo.jpg') }}" alt="Processo" class="img-responsive" style="max-width:310px;" />
						<br />
						<h4>PROCESSO</h4>
						<p>Para adquirir seu aspecto requintado, a armação passa por diversos processos, incluindo prensagem, blindagem especial e jateamento.
						Nas hastes, que tem núcleo de titânio, folhas de acetato de diferentes cores são combinadas para criar padrões únicos. Elas recebem polimento especial para um acabamento acetinado na medida certa.

						</p>
						</div>  

						<div class="col-md-4">
						<img src="{{ URL::asset('img/linha-projection/como-e-feito-precisao.jpg') }}"  alt="Processo" class="img-responsive" style="max-width:310px;" />
						<br />
						<h4>PRECISÃO</h4>
						<p>Desde o molde da armação, desenhado com extrema precisão, até os processos manuais de checagem de espessura, curvatura e montagem feita por um artesão, fica evidente o cuidado japonês em oferecer um produto de qualidade máxima. O resultado são óculos extremamente confortáveis e com perfeito caimento no rosto.
						</p>
						</div>  

			    </div>
			  </div>
			</div>

			</div>

			</div>

		</div>

	</div>


<!-- Page content ends -->


@stop