@extends('template')

@section('seo')

<!-- Title here -->
<title>Linha Projection | Hatsu | Óculos de Alta Tecnologia</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil. Produtos de primeira linha com pagamento em até 10 vezes sem juros.">
<meta name="keywords" content="titânio, hatsu, óculos de grau, óculos de graus, comprar óculos, óculos importados, óculos miopia, óculos de alta tecnologia, óculos femininos, óculos masculinos, óculos inquebrável, lentes bifocais, lentes,raid, projection, titanium, óculos titânio">
<meta name="author" content="Hatsu - 2014 - Todos os direitos reservados">
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="2 days" />
<link rel="canonical" href="http://www.hatsu.com.br" />
<link rel="index" title="Óculos de grau, armações e lentes | Hatsu" href="http://www.hatsu.com.br" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

	<div class="container-fluid" style="background-color:#EDEDED;">

		<div class="row">

			<div class="col-md-12" id="linha-projection">

			<div class="col-md-7">

			&nbsp;

			</div>

			<div class="col-md-3" id="info-projection">

			<p class='titulo-projection'>JAPONISM PROJECTION</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

	</div>

	</div>	

		<div class="container-fluid" id="tres_textos">

			<div class="col-md-1">

			&nbsp;

			</div>

			  <div class="col-md-3" style="text-align:center;margin-top:-10px">

			  <p class="projection-titulo-box"><span>LENTES CR-39</span><br />GRATUITAS</p>

			</div>

			  <div class="col-md-4" style="text-align:center;">

			  <p class="projection-titulo-box">DOBRADIÇAS<br /><span>SEM PARAFUSOS</span></p>

			</div>

			  <div class="col-md-3" style="text-align:center;margin-top:-10px">

			  <p class="projection-titulo-box"><span>FINALIZADO POR</span><br />ARTESÕES</p>

			</div>

			<div class="col-md-1">

			&nbsp;

			</div>

		</div>

		<br style="clear:both;">

		<div class="container-fluid" id="titulo_h1">

			<div class="col-md-1">

			&nbsp;

			</div>

			  <div class="col-md-9">

			  <p>TECNOLOGIA</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="aviao">

			<div class="col-md-2">

			&nbsp;

			</div>

			<div class="col-md-8" style="text-align:center;">

			<p class="texto_aviao">

			<span>MELHOR QUE TUDO QUE VOCÊ JÁ VIU</span><br style="line-height:35px;font-size:35px;" />

			Baseada em lemes aeronáuticos as dobradiças da linha Projection não possuem nenhum parafuso e contam com o mesmo material usado em ônibus espaciais

			</p>

			</div>			


			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<br />

		<div class="container" id="blocos" style="text-align:center;">


			  <div class="col-md-4" style="text-align:center;vertical-align:middle;">

			  <img src="{{ URL::asset('img/linha-projection/inovacao.jpg') }}" alt="Inovação" class="img-responsive"   />


			  <p style="margin-top:10px;">

			  <span>Inovação</span><br style="line-height:25px;font-size:25px;" />

				Projeto inovador que aumentar a resistência das dobradiças e a suavidade do movimento

			  </p>

			  </div>

			  <div class="col-md-4" style="text-align:center;vertical-align:middle;">

			  <img src="{{ URL::asset('img/linha-projection/projetado.jpg') }}" alt="Projetado milimetricamente" class="img-responsive" />


			    <p style="margin-top:10px;">
			  <span>Projetado milimetricamente</span><br style="line-height:25px;font-size:25px;" />

				Todas peças possuem curvatura de 1/100 de milímetro para garantir movimento suaves

			  </p>
			  </div>

			  <div class="col-md-4" style="text-align:center;vertical-align:middle;">

			  <img src="{{ URL::asset('img/linha-projection/testado.jpg') }}" alt="Testada 20 mil vezes "  class="img-responsive"  />

	
				<p style="margin-top:10px;">

			  <span>Testada 20 mil vezes </span><br style="line-height:25px;font-size:25px;" />

				Mecanicamente testada, as dobradiças possuem funcionamento perfeito

			  </p>
			  </div>


		</div>

		<br style="clear:both;">


		<div class="container-fluid" id="titulo_h1">

			<div class="col-md-1">

			&nbsp;

			</div>

			  <div class="col-md-9">

			  <p>MATERIAIS</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="projection-materiais">

			<div class="col-md-2">

			&nbsp;

			</div>


			<div class="col-md-8" style="text-align:center;">

			<p class="texto_materiais">

			<span>DESEMPENHO SUBLIME</span><br style="line-height:35px;font-size:35px;" />

			Resistência e durabilidade sem deixar para trás conforto e ergonomia

			</p>

			</div>			

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>


		<div class="container-fluid" id="projection-materiais">

			<div class="row">

				<div class="col-md-2">

				&nbsp;

				</div>


				<div class="col-md-3" style="text-align:right;">

				<img src="{{ URL::asset('img/linha-projection/materiais-titanio.jpg') }}" alt="Titânio" class="img-responsive"    />

				</div>			

				<div class="col-md-5" style="text-align:left;vertical-align:middle;">

				<p class="texto_img_materiais"><b>β –Titânio</b>

				<br />

				<span style="font-family:roboto-light">Aro feito de β –Titânio permite resistência e leveza ao mesmo tempo</span>

				</p>

				</div>

				<div class="col-md-2">

				&nbsp;

				</div>

			</div>

			<div class="row">

				<div class="col-md-2">

				&nbsp;

				</div>


				<div class="col-md-3" style="text-align:right;">

				<img src="{{ URL::asset('img/linha-projection/materiais-resina.jpg') }}" alt="Resina" class="img-responsive" />

				</div>			

				<div class="col-md-5" style="text-align:left;">

				<p class="texto_img_materiais"><b>Resistência térmica reversível</b>

				<br />

				<span style="font-family:roboto-light">Desenvolvida pela industrial aeroespacial, o material das dobradiças é utilizado em ônibus espaciais</span>

				</p>

				</div>
				
				<div class="col-md-2">

				&nbsp;

				</div>

			</div>

			<div class="row">

				<div class="col-md-2">

				&nbsp;

				</div>


				<div class="col-md-3" style="text-align:right;">

				<img src="{{ URL::asset('img/linha-projection/materiais-nucrel.jpg') }}" alt="Nucrel" class="img-responsive"    />

				</div>			

				<div class="col-md-5" style="text-align:left;">

				<p class="texto_img_materiais"><b>Nucrel</b>

				<br />

				<span style="font-family:roboto-light">Antialérgico e antibactericida, o material da plaqueta foi desenvolvido pela America’s DuPont Company.</span>

				</p>

				</div>
				
				<div class="col-md-2">

				&nbsp;

				</div>

			</div>

		</div>

			<!-- Experimente -->
			<div class="container-fluid" style="background-color:#F8F8F8;">
			<div class="col-md-2" style="text-align:center;"></div>
			<div class="col-md-5" style="text-align:center;">

			<p class="texto_materiais">

			<br style="line-height:35px;font-size:35px;" />

			<span><b>Experimente Hatsu:</b></span> Um consultor especializado vai gratuitamente até você, em data e local de sua preferência.

			</p><br /><br />

			</div>	

			<div class="col-md-3" style="text-align:center;">	
	        <div class="quero-experiment">
	        	<br style="line-height:35px;font-size:35px;" />
        		<button><a href="experimente" style="color:#fff;">Experimente</a></button><br /><br />

    		</div></div>
    		</div><BR />
			<!-- fim do Experimente -->

		<div class="container-fluid" id="titulo_h1">

			<div class="col-md-1">

			&nbsp;

			</div>

			  <div class="col-md-9">

			  <p>DESIGN</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="design">

			<div class="col-md-12">

			<img src="{{ URL::asset('img/linha-projection/design_new.jpg') }}" style="float:left;">

			<p class="texto_design" style="float:left">

				<span>DESENVOLVIMENTO EXCLUSIVO</span><br style="line-height:35px;font-size:35px;" />

				A fabricação dos óculos Projection une os avançados processos industriais de montagem com o cuidado da montagem e inspeção manual

			</p>

			</div>

		</div>

		<br />

		<div class="container-fluid" id="titulo_h1">

			<div class="col-md-1">

			&nbsp;

			</div>

			  <div class="col-md-9">

			  <p>PROCESSO</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container-fluid" id="processo-projection">

			<div class="row">

				<div class="col-md-2">

				&nbsp;

				</div>

				<div class="col-md-8" style="text-align:center;">

				<p class="texto_sofisticacao">

					<span>SOFISTICAÇÃO</span><br style="line-height:35px;font-size:35px;" />

					Unindo avançados processos industriais com o cuidado da montagem manual, artesãos aplicam sob a armação um jateamento de areia que combina as superfícies lustrosas e foscas.</span>

				</p>

				</div>

				<div class="col-md-2">

				&nbsp;

				</div>

			</div>	

		<br style="clear:both;" />

		<br style="clear:both;" />

			<div class="row">

				<div class="col-md-2">

				&nbsp;

				</div>

				<div class="col-md-4" style="text-align:center;">


					<img src="{{ URL::asset('img/linha-projection/sofisticacao1.jpg') }}" alt="Sofisticação" class="img-responsive"    />

				</div>

				<div class="col-md-4" style="text-align:center;">

					<img src="{{ URL::asset('img/linha-projection/sofisticacao2.jpg') }}" alt="Sofisticação" class="img-responsive"    />

				</div>

				<div class="col-md-2">

				&nbsp;

				</div>

			</div>				

		</div>	

		<br style="clear:both;" />

		<br style="clear:both;" />		

		<div class="container-fluid" id="titulo_h1">

			<div class="col-md-1">

			&nbsp;

			</div>

			  <div class="col-md-9">

			  <p>ESCOLHA O SEU</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>

		</div>

		<div class="container" id="escolhaoseu">

			{{ Helper::LinhaProdutos(1) }}

		</div>

		<br style="clear:both;">


		<div class="container-fluid" id="embalagem">

		<br />

		<div id="titulo_h1" style="background:none;">

			<div class="col-md-1">

			&nbsp;
			
			</div>

			  <div class="col-md-9">

			  <p>EMBALAGEM</p>

			</div>

			<div class="col-md-2">

			&nbsp;

			</div>


		<div class="col-md-4">

			&nbsp;

			</div>

			<div class="col-md-5" style="text-align:center;">

			<p class="texto_embalagem">

			<span>DETALHES DO INÍCIO AO FIM</span><br /><br />

			Cuidadosamente desenvolvidas, a embalagem da linha Projection garante proteção, portabilidade e beleza

			</p>

			</div>			


			<div class="col-md-3">

			&nbsp;

			</div>

		</div>		

</div>

@stop