@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Lentes | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">lentes</span>

</div>

</div>

<!-- Page content starts -->

<div class="content contact-two">

  <div class="container resume">

      <div class="col-md-12" style="text-align:right;">

      <h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;">LENTES</h1>

      </div>


      <div class="col-md-12">

      <p>As lentes utilizadas nas armações de óculos, chamadas lentes oftálmicas, são lentes com o intuito de compensar alguns problemas de visão, como miopia, hipermetropia, presbiopia e astigmatismo.
Elas, diferentemente de lentes para microscópios, câmeras fotográficas ou telescópios, são projetadas para interagirem com o olho humano, corrigindo os problemas de visão.
Seu formato pode ser variado, porém o mais comum é o chamado côncavo-convexo, onde os lados externo e interno possuem curvatura diferente.
</p>

      </div>

      <br style="clear:both" />
      <br style="clear:both" />

<div class="col-md-12">

      <div class="rblock">

          <div class="col-md-3" style="text-align:center;">

            <h4 id="sobre-info" style="font-size:19px;">TIPOS</h4>  

          </div>

          <table id="sobre">

            <tr>

              <td class="col-md-1">

                <div class="rinfo">

                <img src="{{ URL::asset('img/lentes/simples.png') }}" alt="Lentes - Visão Simples" class="img-responsive" style="max-width:150px" />

                </div>

              </td>

              <td class="col-md-8">

                  <p>

                  <b>Visão Simples</b><br />
          As lentes de visão simples são lentes que possuem o mesmo grau de dioptria em toda superfície, ou seja, são específicas para determinados distúrbios visuais, como miopia, hipermetropia, astigmatismo e presbiopia (vista cansada).
Vale lembrar que lentes de visão simples são capazes de corrigir miopia e astigmatismo, ou hipermetropia e astigmatismo ao mesmo tempo.

                  </p>

              </td>

            </tr>

            <tr>

              <td class="col-md-1">

                <div class="rinfo">

                <img src="{{ URL::asset('img/lentes/bifocal.png') }}" alt="Lentes - Bifocal" class="img-responsive" style="max-width:150px" />

                </div>

              </td>

              <td class="col-md-8">

                  <p>

                  <b>Bifocal</b><br />
                  Para correção simultânea de miopia e presbiopia, as lentes bifocais trazem duas lentes, sendo a parte superior da lente para correção de miopia e a parte inferior para correção de presbiopia, facilitando assim a leitura.

                  </p>

              </td>

            </tr>

            <tr>

              <td class="col-md-1">

                <div class="rinfo">

                <img src="{{ URL::asset('img/lentes/multifocal.png') }}" alt="Lentes - Multifocal" class="img-responsive small-mobile" style="max-width:150px" />

                </div>

              </td>

              <td class="col-md-8">

                  <p>

                  <b>Multifocal</b><br />
            Lentes multifocais também são ideais para correção simultânea de miopia e presbiopia, porém diferentemente das lentes bifocais, não possuem duas lentes, e sim uma alteração progressiva de grau dióptrico.<br />
Como a alteração de grau é progressiva, tais lentes permitem a visualização correta de objetos que estão à meia distância, ou seja, não entram nem no grau de miopia nem no grau hipermetrope.

                  </p>

              </td>

            </tr>

            <tr>

              <td class="col-md-1">

                <div class="rinfo">

                <img src="{{ URL::asset('img/lentes/simples.png') }}" alt="Lentes - Plana" class="img-responsive small-mobile" style="max-width:150px" />

                </div>

              </td>

              <td class="col-md-8">

                  <p>

                  <b>Planas</b><br />
                  Para quem deseja usar óculos como um acessório e não possui problemas na visão, as lentes planas não alteram a imagem.

                  </p>

              </td>

            </tr>

          </table>

      </div>      

      <div class="clear-fix"></div>
        <div class="rblock">
          
            <div class="col-md-3" style="text-align:center;">
              <h4 id="sobre-info">MATERIAIS</h4>                           
            </div>

            <div class="col-md-9">
            <div class="rinfo">
               
               <p>Os materiais mais comuns são:<br />

<b>CR-39</b><br />
Feitas de plástico, as lentes de CR-39 são as mais comuns no mercado, como as lentes Orma, uma vez que são baratas e possuem boa qualidade óptica.
Sua limitação é, principalmente, não ser tão fina para graus dióptricos tão elevados. 
<br /><br />
<b>Policarbonato</b>
<br />
Mais leve que o CR-39, o policarbonato é resistente a rachaduras, porém possui pouca resistência a riscos e possui qualidade óptica baixa, gerando distorções na imagem.
<br /><br />
<b>Trivex</b>
<br />
De altíssima qualidade óptica, o material Trivex possui ótima resistência contra torções e tensões, além de ser extremamente fino até para alto grau dióptrico.
<br /><br />
<b>Cristal</b><br />
Com umas das melhores qualidades ópticas, as lentes de cristal proporcionam maior transparência, contraste na imagem e dificilmente riscam. Porém são pesadas e não são resistentes contra queda.
<br /><br />
</p>

            </div>

          </div>

      </div>

      <div class="clearfix"></div>

      <div class="rblock">
      
          <div class="col-md-3" style="text-align:center;">

            <h4 id="sobre-info">ANTIRREFLEXO</h4>  

          </div>

          <div class="col-md-9">
          
            <div class="rinfo">

            <p>O antirreflexo é um tratamento que a lente recebe que permite maior nitidez e reduz o efeito de ofuscamento, ou seja, retira a sensação de que estamos vendo através de um vidro.<br /><br />

Esse tratamento é uma película microscópica que se forma na lente banhando-a no material antirreflexo.<br /><br />

Quando uma imagem é refletida na lente, todo o espectro de cores está refletindo e deixando de chegar ao seu olho, portanto diminuindo a nitidez do que enxergamos através da lente. O tratamento define a espessura e o índice de refração da película antirreflexo para que se tenha o maior cancelamento de ondas de luz refletidas. A luz, que possui basicamente sete espectros de cores visíveis, passa a ter seis desses sete espectros não mais refletidos com o antirreflexo, fazendo com que tais espectros cheguem ao seu olho, aumentando a qualidade da imagem. <br /><br />

Com lentes antirreflexo, dirigir a noite torna-se mais seguro uma vez que não temos mais o reflexo dos faróis nas lentes. O uso extensivo de computador é menos danoso à saúde dos olhos com tais lente já que temos também o aumento de contraste (diferença luminosa entre um ponto claro e outro escuro).
</p>

          </div>

        </div>

      </div>

      <div class="clearfix"></div>

</div> <!-- fecha div col 12 --> 

</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div><!-- fecha div col 12 -->


@stop