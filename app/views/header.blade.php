<!-- Header starts -->
<header style="padding:0;">
	<div class="container">
		<div class="row">
			<div class="col-md-3" style="padding-left:0;padding:0;">
				<!-- Logo. Use class "color" to add color to the text. -->
				<div class="logo">
					<h1><a href="{{ URL::to('/') }}"><img src="{{ URL::asset('img/logo-hatsu.png') }}" class="img-responsive" /></a></h1>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-4" style="text-align:right;padding:0;width:390px;padding-top:15px;">

				<div class="hlinks" class="col-md-4" style="float:right;color:#000;width:380px;">
		
				@if(Auth::check())

				OLÁ <a href="{{ URL::to('cliente')}}" style="color:#000;text-decoration:underline;" title="Clique para acessar sua conta">{{ substr(strtoupper(Auth::user()->nome),0,15) }}</a>

				&nbsp;&nbsp;<a href="{{ URL::to('logout') }}" style="color:#000;font-size:11px;">(Sair)</a>

				@else 

				<a href="#login" role="button" data-toggle="modal" style="color:#000;">LOGIN</a> | 

				<a href="{{ URL::to('/cliente') }}" style="color:#000;">REGISTRAR</a> 

				@endif


				<!-- ADICIONAR VERIFICAÇÃO DE SESSÃO DO CARRINHO DE COMPRAS -->

				{{ Helper::topoCarrinho() }}


				</div>

			</div>
		</div>
	</div>
</header>

{{ Helper::menuLinha() }} 