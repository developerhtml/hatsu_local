@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Sobre a Hatsu | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')

<div id="breadcrumbs" class="container-fluid">

<div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">Home</a>&nbsp;|&nbsp;<span class="ativo">Sobre a Hatsu</span>

</div>

</div>

<div class="content contact-two">

  <div class="container resume">

      <div class="col-md-12" style="text-align:right;">

      <h1 style="font-size:62px;font-weight:normal;color:#2z2z2z;">SOBRE A HATSU</h1>

      </div>


      <div class="col-md-12">

      <h3 id="sobre">Óculos produzidos no Japão, com design inovador e materiais de alta tecnologia, inéditos no Brasil.</h3>

        <br />
      </div>

      <div class="col-md-12">

        <div class="rblock">
          
            <div class="col-md-3" style="text-align:center;">
              <h4 id="sobre-info">QUEM SOMOS</h4>                           
            </div>

            <div class="col-md-9">
            <div class="rinfo">
               
               <p>Criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil, a Hatsu proporciona produtos de primeira linha. Não desejando mais que você compre óculos por valor da marca, queremos trazer a você produtos que se justifiquem por seus materiais e tecnologias. Eliminamos ainda intermediários, tudo para oferecer produtos exclusivos e diferenciados.</p>

            </div>

          </div>

      </div>

      <div class="clearfix"></div>

      <div class="rblock">
      
          <div class="col-md-3" style="text-align:center;">

            <h4 id="sobre-info">O QUE FAZEMOS</h4>  

          </div>

          <div class="col-md-9">
          
            <div class="rinfo">

            <ul>

             <li>Produzimos armações no Japão para trazer a mais avançada tecnologia do mercado</li>
              <li>Utilizamos lentes adequadas para cada tipo de armação e para cada cliente, facilitando a adaptação</li>
              <li>Fazemos vendas diretamente pelo site, trazendo comodidade a sua compra</li>
              <li>Temos o exclusivo serviço de experimentação em casa através de um consultor treinado para realizar medições que adequam armação e lente a seu rosto e hábitos</li>
              <li>Oferecemos óculos com materiais tecnológicos, design inovador e serviço inteligente.</li>

            </ul>

          </div>

        </div>

      </div>

      <div class="clearfix"></div>

      <div class="rblock">

          <div class="col-md-3" style="text-align:center;">

            <h4 id="sobre-info" style="font-size:19px;">NOSSOS ÓCULOS</h4>  

          </div>

          <table id="sobre">

            <tr>

              <td class="col-md-1">

                <div class="rinfo">

                <img src="{{ URL::asset('img/sobre/MateriaisEspeciais.jpg') }}" alt="Materiais Especiais" />

                </div>

              </td>

              <td class="col-md-8">

                  <p>

                  <b>Materiais especiais</b><br />
                  Utilizamos em nossos óculos materiais de alto desempenho, como resina termicamente reversível, titânio e polifenilsulfona (PPSU), oferecendo assim resistência, acabamento de qualidade máxima e exclusividade.

                  </p>

              </td>

            </tr>

            <tr>

              <td class="col-md-1">

                <div class="rinfo">

                <img src="{{ URL::asset('img/sobre/ProjetoInovador.jpg') }}" alt="Projeto Inovador" />

                </div>

              </td>

              <td class="col-md-8">

                  <p>

                  <b>Projeto Inovador</b><br />
                  Projetado para aproveitar o máximo de desempenho das matérias primas, nossos óculos contam com design arrojado e ergonômico, além de oferecerem durabilidade excepcional.

                  </p>

              </td>

            </tr>

            <tr>

              <td class="col-md-1">

                <div class="rinfo">

                <img src="{{ URL::asset('img/sobre/FabricacaoIndustrial.jpg') }}" alt="Fabricação Industrial" />

                </div>

              </td>

              <td class="col-md-8">

                  <p>

                  <b>Fabricação Industrial</b><br />
                  Recortes mecanicamente perfeitos, feitos a partir de moldes precisos, dão ao aro e à ponte estruturas resistentes e ergonômicas, aderindo perfeitamente ao seu rosto.

                  </p>

              </td>

            </tr>

            <tr>

              <td class="col-md-1">

                <div class="rinfo">

                <img src="{{ URL::asset('img/sobre/MontagemManual.jpg') }}" alt="Montagem Manual" />

                </div>

              </td>

              <td class="col-md-8">

                  <p>

                  <b>Montagem Manual</b><br />
                  Processos manuais de checagem de espessura, comprimento e curvatura de cada peça, além da montagem por um artesão, garantem precisão e beleza nos mínimos detalhes.

                  </p>

              </td>

            </tr>

            <tr>

              <td class="col-md-1">

                <div class="rinfo">

                <img src="{{ URL::asset('img/sobre/Lentes.jpg') }}" alt="Lentes" class="img-responsive" />

                </div>

              </td>

              <td class="col-md-8">

                  <p>

                  <b>Lentes</b><br />
                  Lentes produzidas de maneira a aumentar o campo de visão e diminuir distorções na imagem, com antirreflexo que possui proteção contra raios UVA e UVB, facilitando adaptação e oferecendo melhor qualidade de imagem.

                  </p>

              </td>

            </tr>

          </table>

      </div>      

      </div> <!-- fecha div col 12 --> 

</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div><!-- fecha div col 12 -->


@stop