@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Sobre a Hatsu | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')
	
    <!----------- TOP PAGE NAV START --------------->
	<section class="top-page-nav">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
					<p><a href="{{ URL::to('/') }}" class="inativo">INÍCIO</a>&nbsp;|&nbsp;<span class="ativo">Sobre a Hatsu</span></p>
				</div>
            </div>
        </div>
    </section>
    <!----------- TOP PAGE NAV END --------------->
	
    <!-----------BANNER START--------------->
    <section class="banner-section">
    	<div class="sobre-banner">
        	<div class="container">
            	<div class="row">
                    <div class="col-sm-12">
                    	<div class="sobre-bnner-content">
                    		<h3>sobre a hatsu</h3>
                            <p>Óculos produzidos no Japão e na Coréia, com design inovador e materiais de alta tecnologia, inéditos no Brasil.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-----------BANNER END--------------->
    <!-----------CONTENT AREA START--------------->
    <div class="container">
    	<div class="row">
        	<div class="col-sm-12">
            	<div class="quem">
                	<h4>QUEM SOMOS</h4>
                    <p>Criada para suprir a falta de óculos com material de alto desempenho e alta tecnologia no Brasil, a Hatsu proporciona produtos de primeira linha. Não desejando mais que você compre óculos por valor da marca, queremos trazer a você produtos que se justifiquem por seus materiais e tecnologias. Eliminamos ainda intermediários, tudo para oferecer produtos exclusivos e diferenciados.</p>
                    <a href="http://www.brainfacture.com.br/public/produtos"><button>conheça nossos óculos</button></a>
                </div>
            </div>
        </div>
    </div>
    <!-----------CONTENT AREA END--------------->
    <!-----------WORLD SECTION START--------------->
    <section class="world-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="world-content">
                        <h3>o que fazemos</h3>
                        <ul>
                        	<li>Importamos armações do Japão para trazer a mais avançada tecnologia do mercado</li>
							<li>Utilizamos lentes adequadas para cada tipo de armação e para cada cliente, facilitando a adaptação</li>
                            <li>Fazemos vendas diretamente pelo site, trazendo comodidade a sua compra</li>
                            <li>Temos o exclusivo serviço de experimentação em casa através de um consultor treinado para realizar medições que adequam armação e lente a seu rosto e hábitos</li>
                            <li>Oferecemos óculos com materiais tecnológicos, design inovador e serviço inteligente.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>	
    </section>
    <!-----------WORLD SECTION END--------------->
    <!-----------GOGGLE SECTION START------------------->
    <section class="goggle-section">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-12">
                	<h3>Nossos óculos</h3>
                </div>
                <div class="goggle-container">
                    <div class="goggle-point goggle-point1">
                        <div class="goggle-dot"></div>
                        <div class="goggle-circle"></div>
                        <div class="goggle-info">
                            <h3>Materiais especiais</h3>
                            <p>Utilizamos em nossos óculos materiais de alto desempenho, como resina oferecendo assim resistência, acabamento de qualidade máxima e exclusividade.</p>
                        </div>
                        
                        <div class="goggle-info-line"></div>
                    </div>
                    <div class="goggle-point goggle-point2">
                        <div class="goggle-dot"></div>
                        <div class="goggle-circle"></div>
                        <div class="goggle-info goggle-info2">
                            <h3>Fabricação industrial</h3>
                            <p>Recortes mecanicamente perfeitos, feitos a partir de moldes precisos, dão ao aro e à ponte estruturas resistentes e ergonômicas, aderindo perfeitamente ao seu rosto.</p>
                        </div>
                        
                        <div class="goggle-info-line goggle-info-line2"></div>
                    </div>
                    <div class="goggle-point goggle-point3">
                        <div class="goggle-dot"></div>
                        <div class="goggle-circle"></div>
                        <div class="goggle-info goggle-info3">
                            <h3>Lentes</h3>
                            <p>Nossas lentes japonesas contam com antirrisco e antirreflexo que possui proteção contra raios UVA e UVB, facilitando adaptação e oferecendo melhor qualidade de imagem.</p>
                        </div>
                        
                        <div class="goggle-info-line goggle-info-line3"></div>
                    </div>
                    <div class="goggle-point goggle-point4">
                        <div class="goggle-dot"></div>
                        <div class="goggle-circle"></div>
                        <div class="goggle-info goggle-info4">
                            <h3>Montagem manual</h3>
                            <p>Processos manuais de checagem de espessura, comprimento e curvatura de cada peça, garantem precisão e beleza nos mínimos detalhes.</p>
                        </div>
                        
                        <div class="goggle-info-line goggle-info-line4"></div>
                    </div>
                    <div class="goggle-point goggle-point5">
                        <div class="goggle-dot"></div>
                        <div class="goggle-circle"></div>
                        <div class="goggle-info goggle-info5">
                            <h3>Projeto inovador</h3>
                            <p>Projetado para aproveitar o máximo de desempenho das matérias primas, nossos óculos contam com design arrojado e ergonômico.</p>
                        </div>
                        
                        <div class="goggle-info-line goggle-info-line5"></div>
                    </div>
                        
                	<img class="sobre-goggle" src="{{ URL::asset('img/sobre/sobre_goggle.png') }}" alt="">
                    <img class="goggle-info" src="{{ URL::asset('img/sobre/goggle-info.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>	
    <!-----------GOGGLE SECTION END--------------------->
    


</div> <!-- fecha div col 12 -->

</div> <!-- fecha div col 12 -->

</div><!-- fecha div col 12 -->


@stop