@extends('template')

@section('seo')

<!-- Tags de SEO personalizadas -->
<title>Experimente Hatsu | Hatsu | Óculos de Alta Tecnologia, Óculos de receituário</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="A Hatsu é uma marca de óculos de receituário únicos no Brasil. Nossos produtos incorporam tecnologia e materiais desenvolvidos para aplicações de alto desempenho, com design e engenharia resultam na perfeita combinação entre performance e beleza.">
<meta name="keywords" content="hatsu, óculos de grau, óculos de graus, óculos online, óculos importado, óculos importado do japão, óculos de alta tecnologia, óculos high tech, óculos showroom">
<meta name="author" content="´Hatsu - 2014 - Todos os direitos reservados">
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

@stop

@section('body')
<!----------- TOP PAGE NAV START --------------->
<section class="top-page-nav">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p style="font-family: roboto-light"><a href="{{ URL::to('/') }}" class="inativo" style="color:#77a101;"">INÍCIO</a> | Experimente Hatsu</p>
                </div>
            </div>
        </div>
    </section>
<!----------- TOP PAGE NAV END --------------->

<!-- i-comment -- <div class="container" style="padding:0;">

<a href="{{ URL::to('/') }}" class="inativo">INÍCIO</a>&nbsp;|&nbsp;<span class="ativo">experimente em casa</span>

</div>-->



<!-- Page content starts -->

<!-----------BANNER START--------------->
    <section class="banner-section">
    	<div class="banner-image">
        	<div class="container">
            	<div class="row">
                    <div class="col-sm-12">
                    	<h3 class="experiment-banner">EXPERIMENTE HATSU</h3>
                    </div>
                </div>
            </div>
        	<img src="img/banner.jpg" alt="" title=""/>
        </div>
    </section>
    <!-----------BANNER END--------------->
    <!-----------CONTENT AREA START--------------->
    <div class="container">
    	<div class="row">
        	<div class="col-sm-12">
            	<div class="quero-experiment">
                	<h3>o que é experimente hatsu?</h3>
                    <p>Experimente Hatsu  é um serviço gratuito oferecido pela Hatsu onde um consultor leva nossos óculos até você e te ajuda a escolher o modelo de óculos que mais combina com seu rosto e necessidades diárias.</p>
                    <button><a href="#ancora" style="color:#fff;">eu quero!</a></button>
                </div>
            </div>
        </div>
    </div>
    
    <section class="como-experiment-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="quero-experiment qomo">
                        <h3>como funciona o serviço?</h3>
                    </div>
                    <div class="como-boxes">
                        <div class="como-experiment">
                            <div class="requ-heading">
                                <h4>requisite<br/> o serviço</h4>
                                <div class="como-experiment-circle"></div>
                            </div>
                            <div class="requ-para">
                            <p>Preencha os campos encontrados no final da página e entraremos em contato</p>
                            </div>
                        </div>
                        <div class="como-experiment">
                            <div class="requ-heading">
                                <h4>agende<br/>uma visita</h4>
                                <div class="como-experiment-circle"></div>
                            </div>
                            <div class="requ-para">
                            <p>Ao ligarmos para você, agendamos horário e local de sua preferência</p>
                            </div>
                        </div>
                        <div class="como-experiment">
                            <div class="requ-heading">
                                <h4>Escolha<br/>os óculos </h4>
                                <div class="como-experiment-circle"></div>
                            </div>
                            <div class="requ-para">
                            <p>varemos todos os nossos óculos para que você decida qual mais adequado as suas necessidades</p>
                            </div>
                        </div>
                        <div class="como-experiment">
                            <div class="requ-heading">
                                <h4>compre<br/>na hora</h4>
                                <div class="como-experiment-circle"></div>
                            </div>
                            <div class="requ-para">
                            <p>Nosso consultor  a venda na hora, com o método de pagamento de sua preferência.</p>
                            </div>
                        </div>
                        <div class="como-experiment last-repu">
                            <div class="requ-heading">
                                <h4>receba<br/>em casa</h4>
                                <div class="como-experiment-circle"></div>
                            </div>
                            <div class="requ-para">
                            <p>Confeccionaremos suas lentes e em poucos dias enviaremos seus óculos</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-----------CONTENT AREA END--------------->
    <!-----------REALMENTE AREA START--------------->
    <section class="realmente-green bg-none">
    	<div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="quero-experiment">
                        <h3>é realmente grátis?</h3>
                        <p>Experimente Hatsu  é um serviço gratuito oferecido pela Hatsu onde um consultor leva nossos óculos até você e te ajuda a escolher o modelo de óculos que mais combina com seu rosto e necessidades diárias.</p>
                    </div>
                </div>
            </div>
    	</div>	
    </section>
    <!-----------REALMENTE AREA END--------------->
    <!-----------MAP AREA START------------------->
    <div class="container">
    	<div class="row">
         	<div class="col-sm-12">
                <div class="quero-experiment realment">
                	<h3>Quais cidades são atendidas?</h3>
                </div>
            </div>
            <div class="map-box">
            	<div class="col-sm-6">
                	<div class="map">
                    	<img src="img/map.jpg" alt="" title="map"/>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="quero map-para experiment-map-contant1">
                    	<p>Atualmente atendemos a Grande São Paulo, Campinas e sua região metropolitana. Em breve expandiremos o serviço para atender a maior quantidade possível de cidades.</p>
						<div class="requ-para caso experiment-map-contant2">
                        	<p>Caso sua cidade não for atendida atualmente, lembre-se que a compra diretamente do nosso site possui frete grátis, e caso você queira devolver os óculos, devolvemos seu dinheiro com a devolução do produto.</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>	
    <!-----------MAP AREA END-->
    

      @if (Session::has('message'))
        <h2 style="background:#72a504;color:#fff;text-transform:uppercase;text-align:center;font-size:16px;font-weight-normal;">{{ Session::get('message') }}</h2>
      @endif

      <section class="form-area">
    	<div class="container">
           <a name="ancora"></a>
        	<div class="row">
            	<div class="col-sm-12">
                	<div class="form-box">
                    	<h3>solicite uma visita</h3>
          <!-- Contact form (gambs para juntar do thales e tanveer)-->
          <form class="form-horizontal" action="{{ URL::to('experimente/store') }}" method="post">
              
              @if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif

              <!-- Name -->
              <div class="form-group">
                <label class="control-label col-md-2" for="name1">Nome</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('nome')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" value="{{ Input::old('nome') }}" placeholder="Informe seu nome completo">
                </div>
              </div>

              @if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif
              <!-- Email -->
              <div class="form-group">
                <label class="control-label col-md-2" for="email1">E-mail</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" value="{{ Input::old('email') }}" placeholder="Informe seu e-mail">
                </div>
              </div>

              @if($errors->has('telefone')>0) <p class='error'> {{ $errors->first('telefone') }} </p> @endif
              <!-- Email -->
              <div class="form-group">
                <label class="control-label col-md-2" for="telefone">Telefone</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" @if($errors->has('telefone')>0) style='border-color:#ff0000;' @endif id="telefone" name="telefone" value="{{ Input::old('telefone') }}" placeholder="Telefone ou Celular">
                </div>
              </div>

              @if($errors->has('estado')>0) <p class='error'> {{ $errors->first('estado') }} </p> @endif

                <div class="form-group">
                <label class="control-label col-md-2">Estado</label>
                <div class="col-md-9">                               
                  <select class="form-control" id="estado" name="estado" @if($errors->has('estado')>0) style='border-color:#ff0000;' @endif>
                  <option value="">- Selecione -</option>
                    @foreach(Estado::all() as $estado)
                        <option @if(Input::old('estado') == $estado->id) {{ 'selected="selected"' }} @endif value="{{ $estado->id }}">{{ $estado->nome }}</option>
                    @endforeach
                  </select>
                </div>
                </div>  

                @if($errors->has('cidade')>0) <p class='error'> {{ $errors->first('cidade') }} </p> @endif    

                <div class="form-group">
                <label class="control-label col-md-2">Cidade</label>
                <div class="col-md-9">                               
                  <select class="form-control" id="cidade" name="cidade" @if($errors->has('cidade')>0) style='border-color:#ff0000;' @endif>
                    @if (Input::old('cidade'))
                        @foreach(Cidade::where('uf','=',Cidade::find(Input::old('cidade'))->uf)->get() as $cidade)
                          <option value='{{ $cidade->id }}' @if($cidade->id == Input::old('cidade')) selected='selected' @endif >{{ $cidade->nome }}</option>
                        @endforeach
                      @else
                        <option value="">- Selecione -</option>
                      @endif
                    </select>
                  </div>
                </div>  

              @if($errors->has('obs')>0) <p class='error'> {{ $errors->first('obs') }} </p> @endif

              <!-- Comment -->
              <div class="form-group">
                <label class="control-label col-md-2" for="obs">Observações</label>
                <div class="col-md-9">
                  <textarea class="form-control" id="obs" name="obs" rows="3" placeholder="Informe o melhor horário para agendarmos a visita! Nosso atendimento entrará em contato o mais breve possível." @if($errors->has('obs')>0) style='border-color:#ff0000;' @endif>{{ Input::old('obs') }}</textarea>
                </div>
              </div>
              <!-- Buttons -->
              <div class="form-group">
                 <!-- Buttons -->
         <div class="col-md-9 col-md-offset-4">
          <input type="submit" class="botao-comprar" value="ENVIAR">
         </div>
              </div>
          </form>
        

<!--
          <form action="{{ URL::to('experimente/store') }}" method="post">
              
@if($errors->has('nome')>0) <p class='error'> {{ $errors->first('nome') }} </p> @endif

							<div class="input-box">
                            	<input type="text" name="txt" placeholder="NOME" @if($errors->has('nome')>0) style='border-color:#ff0000;' @endif id="nome" name="nome" value="{{ Input::old('nome') }}" placeholder="Informe seu nome completo"/>
                            </div>
                            
@if($errors->has('email')>0) <p class='error'> {{ $errors->first('email') }} </p> @endif                            
                            <div class="input-box">
                            	<input class="input1 wd-43 mrb-20" type="text" name="txt" placeholder="EMAIL" @if($errors->has('email')>0) style='border-color:#ff0000;' @endif id="email" name="email" value="{{ Input::old('email') }}" placeholder="Informe seu e-mail"/>
                                
@if($errors->has('telefone')>0) <p class='error'> {{ $errors->first('telefone') }} </p> @endif                                
                                
                                <input class="wd-43" type="text" name="txt" placeholder="TELEFONE DE CONTATO" @if($errors->has('telefone')>0) style='border-color:#ff0000;' @endif id="telefone" name="telefone" value="{{ Input::old('telefone') }}" placeholder="Telefone ou Celular"/>
                            </div>
                            
@if($errors->has('estado')>0) <p class='error'> {{ $errors->first('estado') }} </p> @endif                        
                            
                            <div class="input-box">
                            	<input class="input1 wd-22 mrb-20" type="text" name="txt" placeholder="ESTADO"/>

@if($errors->has('cidade')>0) <p class='error'> {{ $errors->first('cidade') }} </p> @endif    
                                
                                <input class="wd-75" type="text" name="txt" placeholder="CIDADE"/>
                            </div>
                            <div class="input-box">
                            	<input class="" type="text" name="txt" placeholder="ENDEREÇO"/>
                            </div>
@if($errors->has('obs')>0) <p class='error'> {{ $errors->first('obs') }} </p> @endif                            
                            <div class="input-box">
                            	<textarea placeholder="OBSERVACOES" @if($errors->has('obs')>0) style='border-color:#ff0000;' @endif>{{ Input::old('obs') }}  </textarea>
                            </div>
                            <input type="submit" name="sbmt" value="solicitar" class="submit sbmit-right" />

          </form>
      -->
		</div>
	   </div>
	  </div>
	 </div>
    </section>
<!-----------FORM AREA END--------------------->
       
<!-- Page content ends -->


@stop