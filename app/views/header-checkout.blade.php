<!-- Header starts -->
<header>
	<div class="container">
		<div class="row" style="padding-bottom:5px;border-bottom:1px solid #000;">
			<div class="col-md-4">
				<!-- Logo. Use class "color" to add color to the text. -->
				<div class="logo">
					<h1><a href="{{ URL::to('/') }}"><img src="{{ URL::asset('img/logo-hatsu.png') }}" /></a></h1>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-4">

				<div class="hlinks" style="min-width:350px;color:#000;">
		
				@if(Auth::check())

					<?php $old_uri = Request::path(); ?>

					@if ($old_uri == "checkout/store")

						<a href="#" style="color:#000;">1 CARRINHO</a>

						<a href="#" style="color:#000;">2 PAGAMENTO</a>

						<span>3</span> <a href="#" style="color:#000;">CONFIRMAÇÃO</a>

					@elseif($old_uri == "carrinho")

						<span>1</span> <a href="{{ URL::to('carrinho') }}" style="color:#000;">CARRINHO</a>

						<a href="{{ URL::to('checkout') }}" style="color:#000;">2 PAGAMENTO</a>

						<a href="#" style="color:#000;">3 CONFIRMAÇÃO</a>

					@elseif($old_uri == "checkout")
		
						<a href="{{ URL::to('carrinho') }}" style="color:#000;">1 CARRINHO</a>

						<span>2</span> <a href="{{ URL::to('checkout') }}" style="color:#000;">PAGAMENTO</a>

						<a href="#" style="color:#000;">3 CONFIRMAÇÃO</a>

					@endif

				@endif

				</div>

			</div>
		</div>
	</div>
</header>