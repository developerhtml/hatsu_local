<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">

    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/hatsu.css') }}" rel="stylesheet"> 
    <link rel="shortcut icon" href="{{ URL::asset('img/favicon/favicon.png') }}">

    <style type="text/css">

    p { font-size:14px; }

    </style>

</head>

<body>

<div class="container" style="padding:10px;width:600px;">

  <div class="row">

  <header style="padding:0;">
    <div class="container">
      <div class="row">
        <div class="col-md-3" style="padding-left:0;padding:0;">
          <!-- Logo. Use class "color" to add color to the text. -->
          <div class="logo">
            <h1><a href="http://www.hatsu.com.br"><img src="http://hatsu.com.br/public/img/logo-hatsu.png" class="img-responsive" /></a></h1>
          </div>
        </div>
      </div>
    </div>
  </header>

  <?php 

    $cliente =  Cliente::find($id_cliente);

    $pedido =  Pedido::find($id_pedido);

    $itens_pedido = PedidoItem::where('id_pedido', '=', $id_pedido)->get();

    $pagamento =  Pagamento::find($id_pagamento);

  ?>

  <h1>Olá {{ $cliente->nome }}</h1>

    <p>Obrigado por comprar seus óculos conosco! Seu pedido foi criado com sucesso!</p>

  @if(Helper::Receita($pedido->id)==1) 

  <p>Lembre-se que para confeccionarmos corretamente suas lentes precisamos de sua prescrição médica. Por esse motivo solicitamos que tire uma foto ou escaneie sua receita e nos envie por email ou faça o envio da imagem na página de “Minha Conta” em nosso site.</p>

  @endif

  @if(isset($boleto))

  <p>Caso ainda não tenha impresso seu boleto, <a href="{{ $boleto }}" target="_blank">CLIQUE AQUI</a></p>

  @endif

  <p> Acesse sua conta Hatsu a qualquer momento clicando no botão de <a href="http://hatsu.com.br/login">LOGIN</a>. Utilize seu e-mail e senha para alterar seus dados, gerenciar seus pedidos e enviar suas prescrições médicas.</p>


      <h5 class="title" style="font-size:20px;">PEDIDO {{ $pedido->cod_pedido }}</h5>

            <table class="table table-striped tcart" style='width:600px;border-collapse:collapse;'>
              <thead>
                <tr style="background:#f1f1f1;">
                  <th style="border:1px solid #ccc;">DATA DO PEDIDO</th>
                  <th style="border:1px solid #ccc;">CÓDIGO</th>
                  <th style="border:1px solid #ccc;">TOTAL</th>
                  <th style="border:1px solid #ccc;">STATUS</th>
                </tr>
              </thead>

              <tbody style="text-align:center;">

              <tr>
              <td style="border:1px solid #ccc;">{{ Helper::formataData($pedido->data_pedido,1) }}</td>
              <td style="border:1px solid #ccc;">{{ $pedido->cod_pedido }}</td>
              <td style="border:1px solid #ccc;">R$ {{ number_format($pedido->valor_total,2,",",".") }}</td>
              <td style="border:1px solid #ccc;">{{ $pedido->status }}</td>

              </tr>
              </tbody>

              </table>


              <h5 class="title" style="font-size:20px;">DETALHAMENTO</h5>

              {{ Helper::ConfirmacaoPedido($pedido->id)  }}

              <tr style='background:#f1f1f1;'><td style="border: 1px solid #ccc;"><b>TOTAL</b></td>

              <td style="border:1px solid #ccc;"><b>R$  {{ number_format($pedido->valor_total,2,",",".") }} </b></td></tr>

              </table>

              <br />

  <p>Dúvidas podem ser previamente retiradas na página de <a href="http://hatsu.com.br/duvidas">Dúvidas</a>. 

  <br />

  <p>Caso necessite entrar em contato conosco envie um email para <b><a href="atendimento@hatsu.com.br">atendimento@hatsu.com.br</a></b> ou fique à vontade para nos ligar no telefone <b>(19) 3252-3760</b></p>

  <br />

  <p>

  Curta nossa página no Facebook: <a href="http://www.facebook.com/HatsuBrasil">FB.com/HatsuBrasil</a>
  <br />

  Siga nosso twitter: <a href="http://www.twitter.com/hatsubr">@HatsuBR</a>
  </p>

  </div>

</div>

</body>

</html>
