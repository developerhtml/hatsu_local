<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "The :attribute must be accepted.",
	"active_url"           => "O endereço :attribute não é uma URL válida.",
	"after"                => "A data :attribute tem que ser após a data :date.",
	"alpha"                => "O campo :attribute só pode conter letras.",
	"alpha_dash"           => "The :attribute may only contain letters, numbers, and dashes.",
	"alpha_num"            => "The :attribute may only contain letters and numbers.",
	"array"                => "O atributo :attribute deve ser um array.",
	"before"               => "The :attribute must be a date before :date.",
	"between"              => array(
		"numeric" => "The :attribute must be between :min and :max.",
		"file"    => "The :attribute must be between :min and :max kilobytes.",
		"string"  => "The :attribute must be between :min and :max characters.",
		"array"   => "The :attribute must have between :min and :max items.",
	),
	"confirmed"            => "The :attribute confirmation does not match.",
	"date"                 => "The :attribute is not a valid date.",
	"date_format"          => "The :attribute does not match the format :format.",
	"different"            => "The :attribute and :other must be different.",
	"digits"               => "The :attribute must be :digits digits.",
	"digits_between"       => "The :attribute must be between :min and :max digits.",
	"email"                => "The :attribute must be a valid email address.",
	"exists"               => "The selected :attribute is invalid.",
	"image"                => "The :attribute must be an image.",
	"in"                   => "The selected :attribute is invalid.",
	"integer"              => "The :attribute must be an integer.",
	"ip"                   => "The :attribute must be a valid IP address.",
	"max"                  => array(
		"numeric" => "The :attribute may not be greater than :max.",
		"file"    => "The :attribute may not be greater than :max kilobytes.",
		"string"  => "The :attribute may not be greater than :max characters.",
		"array"   => "The :attribute may not have more than :max items.",
	),
	"mimes"                => "The :attribute must be a file of type: :values.",
	"min"                  => array(
		"numeric" => "The :attribute must be at least :min.",
		"file"    => "The :attribute must be at least :min kilobytes.",
		"string"  => "The :attribute must be at least :min characters.",
		"array"   => "The :attribute must have at least :min items.",
	),
	"not_in"               => "The selected :attribute is invalid.",
	"numeric"              => "The :attribute must be a number.",
	"regex"                => "The :attribute format is invalid.",
	"required"             => "O campo :attribute é obrigatório.",
	"required_if"          => "The :attribute field is required when :other is :value.",
	"required_with"        => "The :attribute field is required when :values is present.",
	"required_with_all"    => "The :attribute field is required when :values is present.",
	"required_without"     => "The :attribute field is required when :values is not present.",
	"required_without_all" => "The :attribute field is required when none of :values are present.",
	"same"                 => "O campo :attribute deve ser igual ao campo :other ",
	"size"                 => array(
		"numeric" => "The :attribute must be :size.",
		"file"    => "The :attribute must be :size kilobytes.",
		"string"  => "The :attribute must be :size characters.",
		"array"   => "The :attribute must contain :size items.",
	),
	"unique"               => "The :attribute has already been taken.",
	"url"                  => "The :attribute format is invalid.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(

		"password"=>"Senha",
		"username"=> "Usuário",
		"name"=> "Nome",
		"nome"=> "Nome",
		"razao_social"=> "Razão Social",
		"email"=> "Email",
		"cpf" => "CPF",
		"cnpj" => "CNPJ",
		"tipo" => "Tipo",
		"endereco" => "Endereço",
		"numero" => "Número",
		"bairro" => "Bairro",
		"cidade" => "Cidade",
		"estado" => "Estado",
		"cep" => "CEP",
		"cep_cobranca" => "CEP para Cobrança",
		"endereco_cobranca" => "Endereço para Cobrança",
		"numero_cobranca" => "Número",
		"bairro_cobranca" => "Bairro para Cobrança",
		"cidade_cobranca" => "Cidade para Cobrança",
		"estado_cobranca" => "Estado para Cobrança",
		"telefone" => "Telefone",
		"celular" => "Celular",
		"rg" => "RG",
		"cargo" => "Cargo",
		"salario" => "Salário",
		"transporte" => "Vale Transporte",
		"alimentacao" => "Vale Alimentação",
		"saude" => "Saúde",
		"data_admissao" => "Data de Admissão",
		"data_rescisao" => "Data de Rescisão",
		"banco" => "Código do Banco",
		"nome_banco" => "Nome do Banco",
		"agencia" => "Agência",
		"conta" => "Conta Corrente",
		"gerente" => "Gerente",
		"saldo" => "Saldo",
		"data_atualizacao" => "Data de Atualização",
		"apelido" => "Apelido",
		"contrato" => "Contrato",
		"registro" => "Registro",
		"escritura" => "Escritura",
		"valor_compra" => "Valor da Compra",
		"data_compra" => "Data da Compra",
		"disponivel_venda" => "Disponível p/ Venda",
		"valor_venda" => "Valor da Venda",
		"data_venda" => "Data da Venda",
		"disponivel_locacao" => "Disponível p/ Locação",
		"valor_aluguel" => "Valor do Aluguel",
		"locado" => "Locado",
		"contrato_locacao" => "Código do Contrato de Locação",
		"data_vencimento" => "Data de Vencimento",
		"forma_pagamento" => "Forma de Pagamento",
		"valor_recebido" => "Valor Recebido",
		"data_recebimento" => "Data de Recebimento",
		"valor_conta" => "Valor da Conta",
		"data_contrato_locacao" => "Data do Contrato de Locação",
		"data_vigencia_contrato_locacao_de" => "Data de Vencimento do Contrato de Locação (de)",
		"data_vigencia_contrato_locacao_ate" => "Data de Vencimento do Contrato de Locação (até)",
		"data_reajuste_aluguel" => "Data de Reajuste do Aluguel",
		"valor_locacao" => "Valor da Locação",
		"despesa" => "Despesa",
		"receita" => "Receita",
		"descricao" => "Descrição",
		"id_cliente" => "Cliente",
		"id_imovel" => "Imóvel",
		"id_funcionario" => "Funcionário",
		"id_fornecedor" => "Fornecedor",
		"id_descricao" => "Identificação",
		"id_grupo" => "Grupo de Despesa/Receita",
		"id_conta_corrente" => "Conta Corrente",
		"c_password" => "Confirmação de Senha",
		"validation.cpf" => "O CPF informado não é válido!",
		"validation.cnpj" => "O CNPJ informado não é válido!",
		"tempo_expiracao" => "Tempo de Expiração",
		"card_number" => "Número do Cartão",
		"card_holder_name" => "Titular do cartão",
		"card_expiration_month" => "Mês de validades",
		"card_expiration_year" => "Ano de validade",
		"card_cvv" => "código de segurança"
		
	),

);
