<?php

class NewsletterController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex()
	{

	    return View::make('newsletter.index');
	}
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'email'      	=> 'required|email',

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('newsletter')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$newsletter = new Newsletter;

			$newsletter->email = Input::get('email');
			
			$newsletter->save();

			// redirect
			Session::flash('message', 'E-mail cadastrado com sucesso!');
			return Redirect::to('newsletter');

		}

	}

}