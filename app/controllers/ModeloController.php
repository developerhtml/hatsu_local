<?php

class ModeloController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex($sortby=NULL,$order=NULL)
	{

	    if ($sortby && $order) {

	    	if ($sortby == "cidade") {

				$clientes = DB::table('clientes')
				    ->select(array(
				        'id_cliente',
				        'clientes.nome AS nome',
				        'telefone',
				        'celular',
				        'tipo',
				        'bairro',
				        'clientes.uf AS uf',
				        'clientes.cidade AS cidade',
				        'cidades.nome AS nome_cidade',

				    ))
				    ->join('cidades', 'cidades.id', '=', 'clientes.cidade')
				    ->orderBy('cidades.nome', $order)
				   	->paginate(100);

	    	}else {

	    		$clientes = Cliente::orderBy($sortby, $order)->paginate(100);	
	    	}
	        
	    } else {
	        $clientes = Cliente::paginate(100);
	    }

	    return View::make('clientes.index', compact('clientes', 'sortby', 'order'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function postSearch($sortby=NULL,$order=NULL)
	{

		$busca = utf8_encode(Input::get('busca'));
		
		// TRATANDO O TERMO DE PESQUISA
		if(Input::has('busca')){
			$busca = trim(strip_tags(strtolower(Input::get('busca'))));
		}

		$busca = Helper::remover_caracter(Input::get('busca'));

		### ORDENAÇÃO ###
		if (Input::has('orderby')) {
			$orderby = Input::get('orderby');
		}else {
			$orderby = "nome";
		}

		### PAGINATE ###
		if (Input::has('paginate')) {
			$paginate = Input::get('paginate');
		}else {
			$paginate = "100";
		}

		// FAZENDO A CONSULTA NO MODEL
		$clientes = Cliente::where('nome','LIKE','%'.$busca.'%')
					->orderBy($orderby, 'asc')
					->paginate($paginate);

		// RETORNANDO A VIEW COM OS VALORES ENCONTRADOS
		return View::make('modelos.index', compact('clientes', 'sortby', 'order'));
	}	


		/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getSearch($sortby=NULL,$order=NULL)
	{

		$busca = utf8_encode(Input::get('busca'));
		
		// TRATANDO O TERMO DE PESQUISA
		if(Input::has('busca')){
			$busca = trim(strip_tags(strtolower(Input::get('busca'))));
		}

		$busca = Helper::remover_caracter(Input::get('busca'));

		### ORDENAÇÃO ###
		if (Input::has('orderby')) {
			$orderby = Input::get('orderby');
		}else {
			$orderby = "nome";
		}

		### PAGINATE ###
		if (Input::has('paginate')) {
			$paginate = Input::get('paginate');
		}else {
			$paginate = "100";
		}

		// FAZENDO A CONSULTA NO MODEL
		$clientes = Cliente::where('nome','LIKE','%'.$busca.'%')
					->orderBy($orderby, 'asc')
					->paginate($paginate);

		// RETORNANDO A VIEW COM OS VALORES ENCONTRADOS
		return View::make('clientes.index', compact('clientes', 'sortby', 'order'));
	}	

	/**
	 * form pra criar novo cliente
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return View::make('modelos.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

		// DADOS PESSOAIS

			'numero'   => 'required',

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('modelo/create')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$cliente = new Cliente;

			// DADOS PESSOAIS

			$cliente->tipo	   		= Input::get('tipo');

			if ($cliente->tipo == "P") {
				
				$cliente->cpf      		= Input::get('cpf');
				$cliente->nome     		= Input::get('nome');

			}elseif ($cliente->tipo == "J") {

				$cliente->cnpj    	= Input::get('cnpj');
				$cliente->nome  	= Input::get('razao_social');
			}
			
			$cliente->telefone  	= Input::get('telefone');
			$cliente->celular  		= Input::get('celular');
			$cliente->email    		= Input::get('email');

			// ENDEREÇO

			$cliente->cep 	    	= Input::get('cep');
			$cliente->endereco    	= Input::get('endereco');
			$cliente->numero   		= Input::get('numero');
			$cliente->complemento   = Input::get('complemento');
			$cliente->bairro    	= Input::get('bairro');
			$cliente->cidade    	= Input::get('cidade');
			$cliente->uf    		= Input::get('estado');

			$cliente->save();

			// SALVANDO LOG
			$insertedId = $cliente->id_cliente;
			$logger = Helper::salvaLog("clientes",$insertedId,"cadastro");

			// redirect

			Session::flash('message', 'Cliente cadastrado com sucesso!');
			return Redirect::to('cliente');

		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{

		// busca id do cliente
		$cliente = Cliente::find($id);

		// show the edit form and pass the nerd
		return View::make('clientes.edit')
			->with('cliente', $cliente);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

		// DADOS PESSOAIS

			'tipo'		 =>	'required',
			'telefone'   => 'required',
			'email'      => 'required|email',

		// ENDEREÇO

			'cep'	   => 'required',
			'estado'   => 'required',
			'cidade'   => 'required',
			'bairro'   => 'required',
			'endereco' => 'required',
			'numero'   => 'required',

		);

		// PESSOA FISICA E JURIDICA PF / CNPJ

		if(Input::get('tipo') == "P"){
			$rules['cpf'] = 'required|cpf';
			$rules['nome'] = 'required';
		}elseif (Input::get('tipo') == "J") {
			$rules['cnpj'] = 'required|cnpj';
			$rules['razao_social'] = 'required';
		} 

		$validator = Validator::make(Input::all(), $rules);

		// executa a validação, retorna a pagina se der errO
		if ($validator->fails()) {

			return Redirect::to('cliente/edit/' . $id )
				->withErrors($validator)
				->withInput();

		} else {

			// ARMAZENANDO NO BANCO DE DADOS

			$cliente = Cliente::find($id);

			// DADOS PESSOAIS

			$cliente->tipo	   		= Input::get('tipo');

			if ($cliente->tipo == "P") {
				
				$cliente->cpf      	= Input::get('cpf');
				$cliente->nome     	= Input::get('nome');

			}elseif ($cliente->tipo == "J") {

				$cliente->cnpj    	= Input::get('cnpj');
				$cliente->nome  	= Input::get('razao_social');
			}

			$cliente->telefone  	= Input::get('telefone');
			$cliente->celular  		= Input::get('celular');
			$cliente->email    		= Input::get('email');

			// ENDEREÇO

			$cliente->cep  	    	= Input::get('cep');
			$cliente->endereco    	= Input::get('endereco');
			$cliente->numero   		= Input::get('numero');
			$cliente->complemento   = Input::get('complemento');
			$cliente->bairro    	= Input::get('bairro');
			$cliente->cidade    	= Input::get('cidade');
			$cliente->uf    		= Input::get('estado');

			$cliente->save();

			// SALVANDO LOG
			$logger = Helper::salvaLog("clientes",$id,"alteracao");

			// REDIRECIONANDO PARA PAGINA DE SUCESSO

			Session::flash('message', 'Cliente atualizado com sucesso!');

			return Redirect::to('cliente');
		}
	}

	/**
	 * EXCLUIR CLIENTE DO DATABASE
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDestroy($id)
	{

		// delete
		$cliente = Cliente::find($id);
		$cliente->delete();

		// SALVANDO LOG
		$logger = Helper::salvaLog("clientes",$id,"exclusao");

		// redirect
		Session::flash('message', 'Cliente excluído com sucesso!');
		return Redirect::to('cliente');
	}

}