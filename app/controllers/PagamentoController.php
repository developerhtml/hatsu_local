<?php

class PagamentoController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/


	public function getIndex($id)
	{

	    $dados = Pedido::where('id_cliente','=',Auth::id())->where('id','=',$id)->get();
		$itens = PedidoItem::where('id_pedido', '=', $id)->get();
		$pagamento = Pagamento::where('id_pedido', '=', $id)->get();
    	return View::make('minhaconta.pagamento')
    	->with('dados',$dados)
    	->with('itens',$itens)
    	->with('pagamento',$pagamento);

	}



	public function postStore()
	{

		// die(print_r($_POST));

		########################################################
		##############	ESTRUTURA DO CHECKOUT    ###############
		########################################################
		##  1- VALIDAÇÃO DOS DADOS DO CLIENTE SERVER-SIDE   ####
		##  2- VALIDAÇÃO DOS DADOS DE PAGAMENTO             ####
		##  3- INSERIR CLIENTE NO BANCO DE DADOS            ####
		##  4- VALIDAR DADOS DO PEDIDO E SALVAR NO BANCO    ####
		##  5- ENVIO DE EMAIL DE CONFIRMAÇÃO DE CADASTRO    ####
		##  6- PAGAMENTO OK, REDEIRECIONA PARA CONFIRMAÇÃO  ####
		##  7- ERRO PAGAMENTO, VOLTA PRO CHECKOUT JA LOGADO ####
		########################################################
		########################################################

		// VALIDAÇÃO DOS CAMPOS CLIENTE / PEDIDO / PAGAMENTO

		$rules = array(

		// PAGAMENTO

			'forma_pagamento' 	=> 'required',
			'amount'		 	=> 'required',

		);

		if (Input::has('forma_pagamento') && Input::get('forma_pagamento') == "cartao") {

			$rules['bandeira'] 					= 'required';
			$rules['card_number'] 				= 'required';
			$rules['card_holder_name'] 			= 'required';
			$rules['card_expiration_month'] 	= 'required';
			$rules['card_expiration_year'] 		= 'required';
			$rules['card_cvv'] 					= 'required';
			$rules['instalments'] 				= 'required';
		}

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('pagamento/index/'.Input::get('id_pedido'))
				->withErrors($validator)
				->withInput(); 

		}else {

			$id_cliente = Auth::id();

			$cliente = Cliente::find($id_cliente);

			$id_pedido = Input::get('id_pedido');

			if (Input::has('forma_pagamento')) {

				$forma_pagamento = Input::get('forma_pagamento');

			}

			PagarMe::setApiKey("ak_live_FYNqxclCedwXDzu1kVAlYbfa1jNgZo"); // Insira sua chave de API 

			##### DEFININDO AMOUNT #####
			$amount = str_replace(".", "", Input::get('amount'));

			if ($forma_pagamento == "cartao"){

				########################################################
				########## DADOS DO CARTAO DO CLIENTE ##################
				########################################################

				$numero_cartao 		= Input::get('card_number');
				$nome_cartao 		= Input::get('card_holder_name');
				$mes_expiracao 		= Input::get('card_expiration_month');
				$ano_expiracao 		= Input::get('card_expiration_month');
				$mes_expiracao 		= Input::get('card_expiration_year');
				$codigo_validade 	= Input::get('card_cvv');
				$instalments 		= Input::get('instalments');
				$postback_url	 	= "http://hatsu.com.br/retorno_cartao/";

				########################################################
				###### INSERINDO PAGAMENTO NO BANCO DE DADOS  ##########
				########################################################

				if (Input::has('id_pagamento')) {

					$id_pagamento = Input::get('id_pagamento');
					$pagamento = Pagamento::find($id_pagamento);
					
				}

				$pagamento->id_cliente 		= $id_cliente;
				$pagamento->id_pedido 		= $id_pedido;
				$pagamento->valor_total 	= $amount;
				$pagamento->forma_pagamento = $forma_pagamento;
				$pagamento->parcelas	 	= $instalments;

				$pagamento->save();

				if (Input::has('id_pagamento')) {

					$id_pagamento = Input::get('id_pagamento');
					
				}else {

					$lastInsertedId = $pagamento->id;
					$id_pagamento = $lastInsertedId;

				}
				
				########################################################
				###### GERAÇÃO TRANSAÇÃO COM O PAGAR.ME ################
				########################################################

				// die(print_r($_POST)); // pra testar o card hash tem que ser aqui

				$transaction = new PagarMe_Transaction(

				array(
				'card_hash' => 
				Input::get('card_hash')));

				$transaction->setAmount($amount); // Valor da transação em centavos 3000 - R$ 30,00
				$transaction->setInstallments($instalments); // Número de parcelas - OPCIONAL
				
				try {
					
					$transaction->charge();
					
					if($transaction->getStatus() == 'paid') {
						
						$pagamento = Pagamento::find($id_pagamento);
						$pagamento->status = 'PAGO';
						$pagamento->cod_pagamento = $transaction->getId();
						$pagamento->save();

						$pedido = Pedido::find($id_pedido);
						$pedido->cod_pedido = $transaction->getId();
						$pedido->status = 'PAGO';
						$pedido->save();

						return Redirect::to('pagamento/index/'.Input::get('id_pedido'))
							->with('cod',$transaction->getId())
							->with('sucesso',1);

						// return View::make('pagamento.concluido');

					}elseif($transaction->getStatus() == 'refused') {
							//Transação foi recusada
						$transaction->getRefuseReason(); // - mostra por que a transação foi recusada

						$pagamento = Pagamento::find($id_pagamento);
						$pagamento->status = 'PENDENTE';
						$pagamento->cod_pagamento = $transaction->getId();
						$pagamento->save();

						$pedido = Pedido::find($id_pedido);
						$pedido->cod_pedido = $transaction->getId();
						$pedido->status = 'PENDENTE';
						$pedido->save();


						Session::flash('message', 'Sua transação foi recusada. Tente novamente ou utilize outro cartão!');

						return Redirect::to('pagamento/index/'.Input::get('id_pedido'))
							->with('erro_pagamento',$transaction->getRefuseReason())
							->with('cod',$transaction->getId())
							->with('sucesso',0);

					}

				}catch(PagarMe_Exception $e) {

					$pagamento = Pagamento::find($id_pagamento);
					$pagamento->status = 'PENDENTE';
					$pagamento->cod_pagamento = $transaction->getId();
					$pagamento->save();

					$pedido = Pedido::find($id_pedido);
					$pedido->cod_pedido = $transaction->getId();
					$pedido->status = 'PENDENTE';
					$pedido->save();

					Session::flash('message', 'Sua transação foi recusada. Tente novamente ou utilize outro cartão!');

					return Redirect::to('pagamento/index/'.Input::get('id_pedido'))
						->with('erro_pagamento',$e->getMessage())
						->with('cod',$transaction->getId())
						->with('sucesso',0);

						// echo ; // Retorna todos os erros concatendaos.
				}

				}elseif ($forma_pagamento == "boleto") {

					if (Input::has('id_pagamento')) {

						$id_pagamento = Input::get('id_pagamento');
						$pagamento = Pagamento::find($id_pagamento);
						
					}

					$transaction = new PagarMe_Transaction(array(
						'payment_method' => 'boleto',
						'amount' => $amount, // 1000 = R$ 10,00
					    'postback_url' => 'http://hatsu.com.br/retorno_boleto/',
					    	"customer" => array(
				        'name' => $cliente->nome,  
				        'document_number' => $cliente->cpf, 
				        'email' => $cliente->email, 
				        'address' => array(
				            'street' => $cliente->endereco, 
				            'neighborhood' => $cliente->bairro,
				            'zipcode' => $cliente->cep, 
				            'street_number' => $cliente->numero
				        	)
						)
					));

					$transaction->charge();

					$transaction->getBoletoUrl(); // Retorna a URL do boleto gerado. PS: Em modo TESTE retorna SEMPRE `null`



					$pagamento = Pagamento::find($id_pagamento);
					$pagamento->status = 'PENDENTE';
					$pagamento->forma_pagamento = "boleto";
					$pagamento->cod_pagamento = $transaction->getId();
					$pagamento->save();

					$pedido = Pedido::find($id_pedido);
					$pedido->cod_pedido = $transaction->getId();
					$pedido->forma_pagamento = "boleto";
					$pedido->link_boleto = $transaction->getBoletoUrl();
					$pedido->save();

					// $data = array("id_cliente"=>$id_cliente,"id_pedido"=>$id_pedido,"id_pagamento"=>$id_pagamento,'boleto'=>$transaction->getBoletoUrl());

					// Mail::send('emails/confirmacao', $data, function($message) use ($cliente){
					//     $message->to($cliente->email)->subject('Obrigado por comprar na Hatsu');
					//     $message->to('karen@hatsu.com.br')->subject('Novo Pedido Hatsu')->cc("thales@hatsu.com.br");
					// });

					Session::flash('boleto', $transaction->getBoletoUrl());

					return Redirect::to('pagamento/index/'.Input::get('id_pedido'))
						->with('cod',$transaction->getId())
						->with('sucesso',1);

			}

		}

	}

}