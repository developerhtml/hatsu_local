<?php

class LoggerController extends \BaseController {

	public function getIndex($sortby=NULL,$order=NULL)
	{

	    if ($sortby && $order) {

    		$loggers = Logger::orderBy($sortby, $order)->get();	
	        
	    } else {

	        $loggers = Logger::get();
	    }

	    return View::make('loggers.index', compact('loggers', 'sortby', 'order'));
	}


	/**
	 * form pra criar novo log
	 *
	 * @return Response
	 */
	public function getCreate()
	{

		// busca id do imovel
		$logger = DB::table('logs_config')->find(1);
		return View::make('loggers.create')
			->with('logger', $logger);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'tempo_expiracao'		 => 'required'

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('logger/create')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$logger = DB::table('logs_config')->find(1);

			DB::table('logs_config')
            ->where('id', 1)
            ->update(array('tempo_expiracao' => Input::get('tempo_expiracao')));

			// SALVANDO LOG
			$salva_log = Helper::salvaLog("logs",1,"alteracao");	

			// redirect

			Session::flash('message', 'configuração salva com sucesso!');
			return Redirect::to('logger');

		}

	}

}