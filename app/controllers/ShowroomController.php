<?php

class ShowroomController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex()
	{

	    return View::make('showroom.index');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'nome'   		=> 'required',
			'email'      	=> 'required|email',
			'sugestao'		=> 'required'

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('showroom')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$showroom = new Showroom;

			// DADOS PESSOAIS

			$showroom->nome  		= Input::get('nome');
			$showroom->email   		= Input::get('email');
			$showroom->sugestao		= Input::get('sugestao');
			$showroom->data	 		= date("Y-m-d");

			$showroom->save();

			// $data = array("nome"=>$showroom->nome,"email"=>"contato@hatsu.com.br");

			// Mail::send('showroom/padrao', $data, function($message) {
			//     $message->to('thales@hatsu.com.br', "Thales - Hatsu")->subject('Hatsu - Showroom');
			// });

			// redirect
			Session::flash('message', 'Sugestão enviada com sucesso! A Hatsu agradece seu contato, retornaremos em breve!');
			return Redirect::to('showroom');

		}

	}

}