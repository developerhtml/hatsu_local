<?php

class ContatoController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex()
	{

	    return View::make('contato.index');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'nome'   		=> 'required',
			'email'      	=> 'required|email',
			'telefone'      => 'required',
			'cidade'		=> 'required',
			'estado'		=> 'required',
			'mensagem'		=> 'required'

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('contato')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$contato = new Contato;
 
			// DADOS PESSOAIS

			$contato->nome  		= Input::get('nome');
			$contato->email    		= Input::get('email');
			$contato->telefone    	= Input::get('telefone');
			$contato->cidade    	= Input::get('cidade');
			$contato->uf    		= Input::get('estado');
			$contato->mensagem 		= Input::get('mensagem');

			$contato->save();

			$lastInsertedId = $contato->id;
			$id_contato = $lastInsertedId; //get last inserted record's user id value

			$data = array("id_contato"=>$id_contato);

			Mail::send('contato/padrao', $data, function($message){
			    $message->to('contato@hatsu.com.br')->subject('Hatsu - Contato');
			});


			// redirect
			Session::flash('message', 'Formulário enviado com sucesso! A Hatsu agradece seu contato, retornaremos em breve!');
			return Redirect::to('contato');

		}

	}

}