<?php

class ReceitaController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex()
	{

		$pedidos = Pedido::where('id_cliente','=',Auth::id())->where('status',"=","PAGO")->get();
		$total_pedidos = count($pedidos);

	    $dados = Receita::where('id_cliente','=',Auth::id())->get();
		$count = count($dados);
    	return View::make('minhaconta.receitas')
    	->with('dados',$dados)
    	->with('total',$count)
    	->with('pedidos',$pedidos)
    	->with('total_pedidos',$total_pedidos);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'nome'   		=> 'required',
			// 'receita'      	=> 'required|mimes:jpeg,jpg,gif,png,bmp,pdf'

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('receitas')
				->withErrors($validator)
				->withInput(); 

		} else {

			$file = Input::file('receita'); // your file upload input field in the form should be named 'file'

			$destinationPath = 'uploads/'.str_random(8);
			$filename = $file->getClientOriginalName();
			$uploadSuccess = Input::file('receita')->move($destinationPath, $filename);
			 
			$receita = new Receita;

			$receita->id_cliente	= Auth::id();
			$receita->nome  		= Input::get('nome');
			$receita->file    		= $destinationPath."/".$filename;
			$receita->data    		= date("Y-m-d");

			if (Input::has('id_pedido')) {

				$find_cod_pedido = Pedido::where("id","=",Input::get('id_pedido'))->first();

				$receita->cod_pedido 	= $find_cod_pedido->cod_pedido;
				$receita->id_pedido		= Input::get('id_pedido');
			}

			$receita->save();

			if (Input::has('id_pedido')) {

				$find_cod_pedido = Pedido::where("id","=",Input::get('id_pedido'))->first();
				$atualiza_pedido = Pedido::find(Input::get('id_pedido'));
				$atualiza_pedido->receita = 1;
				$atualiza_pedido->save();

			}

			// redirect
			Session::flash('message', 'Receita cadastrada com sucesso');
			return Redirect::to('receitas');

		}

	}

}

