<?php

class CheckoutController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex()
	{

	    return View::make('checkout.index');
	}

	public function postIndex()
	{

		// echo '<pre>';
		// var_dump($_POST);
		// exit();

		// echo '<pre>';
		// var_dump(Session::all());
		// exit();

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

		// DADOS PESSOAIS

			'lente'		=>	'required',
			'tipo'      => 	'required',
			'linha'		=>	'required',
			'sku'		=>	'required',
			'modelo'	=>	'required',
			'cor'		=>	'required'

		);

		### SETANDO VARIAVEIS ####
		$linha  	= Input::get('linha');
		$modelo 	= Input::get('modelo');
		$cor 		= Input::get('cor');
		$lente  	= Input::get('lente');
		$tipo 		= Input::get('tipo');
		$sku 		= Input::get('sku');
		$preco 		= Input::get('preco');
		$url 		= Input::get('url');
		$thumb  	= Input::get('thumb');
		$quantidade	= 1;

		$validator = Validator::make(Input::all(), $rules); // executando validação

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to($linha.'/'.$url)
				->withErrors($validator)
				->withInput(); 

		} else {

			// - SEM GRAU - Demonstrativas ou planas;
			// - VISAO SIMPLES - Carrega o que já tem no layout;
			// - MULTIFOCAL - Disable no botão comprar e exiibe texto abaixo do botão Experimente em casa

			// VERIFICAR LENTE SELECIONA E SETAR VALOR DA LENTE
			if ($lente == "semgrau") { // sem grau não tem valor
				$valor_lente = "0.00";
			}elseif ($lente == "simples") { // visao simples
				if ($tipo == "normais" || $tipo == "naosei") {
					$valor_lente = "0.00";
				}elseif ($tipo == "finas") {
					if ($linha == "projection" || $linha == "kizo" || $linha == "raid") {
						$valor_lente = "0.00";
					}else {
						$valor_lente = "129.00";
					}
				}elseif ($tipo == "finissimas") {
					$valor_lente = "393.00";
				}
			}

			$subtotal = (($quantidade*$preco)+($quantidade*$valor_lente));

			// SETAR A SESSÃO INICIAL DO CARRINHO 

			

			// VERIFICA SE O SKU JA EXISTE NA SESSÃO

			// Session::flush();

			for ($value = 1; $value <= 15; $value++) {

				if (!empty($sku)) {
	
					$item = $sku."-".$value;

					// print $sku;
					// print "<br /><br />";
					// print $value;
					// print "<br /><br/>";

					if (Session::has($item)) {

						// die("teste");
						$maisum = $value+1;

						$novo_item = $sku."-".$maisum;

						$produto = array("indice"=>$novo_item,"sku"=>$sku,"preco"=>$preco,"quantidade"=>$quantidade,"cor"=>$cor,"lente"=>$lente,"tipo"=>$tipo,"valor_lente"=>$valor_lente,"thumb"=>$thumb,"subtotal"=>$subtotal);

						Session::put($novo_item,$produto);

						// echo '<pre>';
						// var_dump(Session::all());
						// exit();

						break;

					}else {

						// die($item);

						$produto = array("indice"=>$item,"sku"=>$sku,"preco"=>$preco,"quantidade"=>$quantidade,"cor"=>$cor,"lente"=>$lente,"tipo"=>$tipo,"valor_lente"=>$valor_lente,"thumb"=>$thumb,"subtotal"=>$subtotal);

						Session::put($item,$produto);	

						break;
					}

				} // se o sku nao estiver vazio

			}

			// REDIRECT
			// Session::flash('message', 'produto adicionado ao carrinho com sucesso!');
			return Redirect::secure('checkout');

		}

	}

	public function postStore()
	{

		// die(print_r($_POST));

		########################################################
		##############	ESTRUTURA DO CHECKOUT    ###############
		########################################################
		##  1- VALIDAÇÃO DOS DADOS DO CLIENTE SERVER-SIDE   ####
		##  2- VALIDAÇÃO DOS DADOS DE PAGAMENTO             ####
		##  3- INSERIR CLIENTE NO BANCO DE DADOS            ####
		##  4- VALIDAR DADOS DO PEDIDO E SALVAR NO BANCO    ####
		##  5- ENVIO DE EMAIL DE CONFIRMAÇÃO DE CADASTRO    ####
		##  6- PAGAMENTO OK, REDEIRECIONA PARA CONFIRMAÇÃO  ####
		##  7- ERRO PAGAMENTO, VOLTA PRO CHECKOUT JA LOGADO ####
		########################################################
		########################################################

		// VALIDAÇÃO DOS CAMPOS CLIENTE / PEDIDO / PAGAMENTO

		$rules = array(

		// DADOS PESSOAIS

			'nome'		 => 'required',
			'cpf'		 => 'required',
			'nome'		 => 'required',
			'telefone'   => 'required',
			'email'      => 'required|email',


		// ENDEREÇO

			'cep'	   => 'required',
			'estado'   => 'required',
			'cidade'   => 'required',
			'bairro'   => 'required',
			'endereco' => 'required',
			'numero'   => 'required',

		// PAGAMENTO

			'forma_pagamento' 	=> 'required',
			'amount'		 	=> 'required',

		);

		if (!Auth::check()) {

			$rules['password'] = 'required|alphaNum|min:3';
			$rules['c_password'] = 'required|alphaNum|min:3|same:password';

		}

		if (Input::has('forma_pagamento') && Input::get('forma_pagamento') == "cartao") {

			$rules['bandeira'] 					= 'required';
			$rules['card_number'] 				= 'required';
			$rules['card_holder_name'] 			= 'required';
			$rules['card_expiration_month'] 	= 'required';
			$rules['card_expiration_year'] 		= 'required';
			$rules['card_cvv'] 					= 'required';
			$rules['instalments'] 				= 'required';
		}

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::secure('checkout')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			if (Auth::check()) {

				$cliente = Cliente::find(Auth::id());

			}else {

				$cliente = new Cliente;

			}

			// DADOS PESSOAIS

			$cliente->cpf      		= Input::get('cpf');
			$cliente->nome     		= Input::get('nome');
			$cliente->telefone  	= Input::get('telefone');
			$cliente->celular  		= Input::get('celular');
			$cliente->email    		= Input::get('email');
			$cliente->sexo    		= Input::get('sexo');


			// ENDEREÇO

			$cliente->cep 	    	= Input::get('cep');
			$cliente->endereco    	= Input::get('endereco');
			$cliente->numero   		= Input::get('numero');
			$cliente->complemento   = Input::get('complemento');
			$cliente->bairro    	= Input::get('bairro');
			$cliente->cidade    	= Input::get('cidade');
			$cliente->uf    		= Input::get('estado');

			// PASSWORD

			if (!Auth::check()) {

				$cliente->password 		= Hash::make(Input::get('password'));
			}

			$cliente->save();

			if (!Auth::check()) {

				$lastInsertedId = $cliente->id_cliente;
				$id_cliente = $lastInsertedId; //get last inserted record's user id value

				#### ENVIA EMAIL DE CADASTRO ####

				$data = array("id_cliente"=>$id_cliente);

			    Mail::send('emails/cadastro', $data, function($message) use ($cliente){
				    $message->to($cliente->email)->subject('Seja bem vindo a Hatsu');
				    $message->to('karen@hatsu.com.br')->subject('Novo Cadastro Hatsu')->cc("thales@hatsu.com.br");
				});


			}else {

				$id_cliente = Auth::id();

			}
			##############################################################################
			################### INTEGRAÇÃO TINY ##########################################
			##############################################################################

			// $cidade = Cidade::where('id','=',$cliente->cidade)->first();
			// // echo '<pre>';
			// // var_dump($cidade);
			// // exit();
			// $nome_cidade = $cidade->nome;

			// // die($nome_cidade);

			// $uf_cidade = $cidade->uf;

			/* $array_contato = '

			 <?xml version="1.0" encoding="UTF-8"?>
			// <contatos>
			//   <contato>
			//     <sequencia>'.$id_cliente.'</sequencia>
			//     <nome>'.$cliente->nome.'</nome>
			//     <tipo_pessoa>F</tipo_pessoa>
			//     <cpf_cnpj>'.$cliente->cpf.'</cpf_cnpj>
			//     <endereco>'.$cliente->endereco.'</endereco>
			//     <numero>'.$cliente->numero.'</numero>
			//     <complemento>'.$cliente->complemento.'</complemento>
			//     <bairro>'.$cliente->bairro.'</bairro>
			//     <cep>'.$cliente->cep.'</cep>
			//     <cidade>'.utf8_decode($nome_cidade).'</cidade>
			//     <uf>'.$uf_cidade.'</uf>
			//     <pais/>
			//     <fone>'.$cliente->telefone.'</fone>
			//     <celular>'.$cliente->celular.'</celular>
			//     <email>'.$cliente->email.'</email>
			//     <email_nfe>'.$cliente->email.'</email_nfe>
			//     <sexo>'.strtolower($cliente->sexo).'</sexo>
			//     <situacao>A</situacao>
			//   </contato>
			// </contatos>

			 '; */

			// // die($array_contato);

			// $url = 'https://www.tiny.com.br/api2/contato.incluir.php';
			// $token = '96205339ceedc780c6c9b39b211bbd2146f451c8';
			// $contato = $array_contato;
			// $data = "token=$token&contato=$contato&formato=XML";

			// function enviarREST($url, $data, $optional_headers = null) {
			// 	$params = array('http' => array(
			// 		'method' => 'POST',
			// 		'header' => "".
	  //               "Content-type: "."application/x-www-form-urlencoded"."\r\n",
			// 	    'content' => $data
			// 	));
				
			// 	if ($optional_headers !== null) {
			// 		$params['http']['header'] = $optional_headers;
			// 	}
				
			// 	$ctx = stream_context_create($params);
			// 	$fp = @fopen($url, 'rb', false, $ctx);
			// 	// if (!$fp) {
			// 	// 	throw new Exception("Problema com $url, $php_errormsg");
			// 	// }
			// 	$response = @stream_get_contents($fp);
			// 	// if ($response === false) {
			// 	// 	throw new Exception("Problema obtendo retorno de $url, $php_errormsg");
			// 	// }
				
			// 	return $response;
			// }

			// @enviarREST($url, $data);    

			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);

			if(Auth::attempt($userdata)) {

				$id_cliente = Auth::id();
			}

			##############################################################
			#######   REGISTRANDO PEDIDO NO BANCO DE DADOS   #############
			##############################################################

			if (Input::has('id_pedido')) {

				$pedido = Pedido::find(Input::get('id_pedido'));
			}else {

				$pedido = new Pedido();	
			}

			$pedido->id_cliente			= $id_cliente;
			$pedido->data_pedido		= date("Y-m-d");
			$pedido->valor_total		= Helper::CalculaTotal();
			$pedido->forma_pagamento 	= Input::get('forma_pagamento');

			// echo '<pre>';
			// var_dump($pedido);
			// exit();

			$pedido->save();

			if (Input::has('id_pedido')) {

				$id_pedido = Input::get('id_pedido');
				
			}else { 

				$lastInsertedId = $pedido->id;
				$id_pedido = $lastInsertedId;
			}

			##############################################################
			####   REGISTRANDO OS ITENS DO PEDIDO NO BANCO DE DADOS   ####
			##############################################################

			// SE JA EXISTIR O PEDIDO, DELETAR TUDO E INSERIR DE NOVO

			$count = PedidoItem::where('id_pedido', '=', $id_pedido)->count();

			if ($count>0) {

				$cleaning = PedidoItem::where('id_pedido', '=', $id_pedido)->forceDelete();
			}

			// SE NÃO EXISTIR, CADASTRAR

	      	$all_skus = Modelo::where("ativo","=",1)->get();

			foreach ($all_skus as $modelos) {

	      		$verificar_beta = $modelos->sku;

	      		$array_verificador = range(1,15);

				foreach ($array_verificador as $value) {

					$beta = $verificar_beta."-".$value;

					if (Session::has($beta)) {

						$item = Session::get($beta);

						$itemPedido = new PedidoItem();

						$itemPedido->id_pedido 		= $id_pedido;
						$itemPedido->indice			= $item["indice"];
						$itemPedido->sku 			= $modelos->sku;
						$itemPedido->quantidade 	= $item["quantidade"];
						$itemPedido->preco 			= $item["preco"];
						$itemPedido->lente 			= $item["lente"];
						$itemPedido->tipo_lente		= $item["tipo"];
						$itemPedido->valor_lente	= $item["valor_lente"];
						$itemPedido->subtotal		= $item["subtotal"];
						$itemPedido->thumb			= $item["thumb"];

						$itemPedido->save();

					}

	      		}
	      		
	      	}

	      	$atualiza_estoque = Helper::AtualizaEstoque($id_pedido);

			##############################################################
			###################   PAGAMENTO   ############################
			##############################################################

			if (Input::has('forma_pagamento')) {

				$forma_pagamento = Input::get('forma_pagamento');

			}

			PagarMe::setApiKey("ak_live_FYNqxclCedwXDzu1kVAlYbfa1jNgZo"); // Insira sua chave de API 

			##### DEFININDO AMOUNT #####
			$pre_amount		= Helper::CalculaTotal();
			if (substr_count($pre_amount, ".")>=1) {
				$pre_amount = str_replace(".", "", $pre_amount);
			}
			$amount = str_replace(",", "", $pre_amount);
			$amount = $amount."00";

			if ($forma_pagamento == "cartao") {

				########################################################
				########## DADOS DO CARTAO DO CLIENTE ##################
				########################################################

				$numero_cartao 		= Input::get('card_number');
				$nome_cartao 		= Input::get('card_holder_name');
				$mes_expiracao 		= Input::get('card_expiration_month');
				$ano_expiracao 		= Input::get('card_expiration_month');
				$mes_expiracao 		= Input::get('card_expiration_year');
				$codigo_validade 	= Input::get('card_cvv');
				$instalments 		= Input::get('instalments');
				$postback_url	 	= "http://hatsu.com.br/retorno_cartao/";

				########################################################
				###### INSERINDO PAGAMENTO NO BANCO DE DADOS  ##########
				########################################################

				if (Input::has('id_pagamento')) {

					$id_pagamento = Input::get('id_pagamento');
					$pagamento = Pagamento::find($id_pagamento);
					
				}else {

					$pagamento = new Pagamento();
				}

				$pagamento->id_cliente 		= $id_cliente;
				$pagamento->id_pedido 		= $id_pedido;
				$pagamento->valor_total 	= $amount;
				$pagamento->forma_pagamento = $forma_pagamento;
				$pagamento->parcelas	 	= $instalments;

				$pagamento->save();

				if (Input::has('id_pagamento')) {

					$id_pagamento = Input::get('id_pagamento');
					
				}else {

					$lastInsertedId = $pagamento->id;
					$id_pagamento = $lastInsertedId;

				}
				
				########################################################
				###### GERAÇÃO TRANSAÇÃO COM O PAGAR.ME ################
				########################################################

				// die(print_r($_POST)); // pra testar o card hash tem que ser aqui

					$transaction = new PagarMe_Transaction(

					array(
					'card_hash' => 
					Input::get('card_hash')));

					$transaction->setAmount($amount); // Valor da transação em centavos 3000 - R$ 30,00
					$transaction->setInstallments($instalments); // Número de parcelas - OPCIONAL
					
					try {
						$transaction->charge();
						if($transaction->getStatus() == 'paid') {
							
							$pagamento = Pagamento::find($id_pagamento);
							$pagamento->status = 'PAGO';
							$pagamento->cod_pagamento = $transaction->getId();
							$pagamento->save();

							$pedido = Pedido::find($id_pedido);
							$pedido->cod_pedido = $transaction->getId();
							$pedido->status = 'PAGO';
							$pedido->save();

							$data = array("id_cliente"=>$id_cliente,"id_pedido"=>$id_pedido,"id_pagamento"=>$id_pagamento);

							Mail::send('emails/confirmacao', $data, function($message) use ($cliente){
							    $message->to($cliente->email)->subject('Obrigado por comprar na Hatsu');
							    $message->to('karen@hatsu.com.br')->subject('Novo Pedido Hatsu')->cc("thales@hatsu.com.br");
							});

							return View::make('checkout.concluido')->with('cod',$transaction->getId());

						} else if($transaction->getStatus() == 'refused') {
							//Transação foi recusada
							$transaction->getRefuseReason(); // - mostra por que a transação foi recusada

							$pagamento = Pagamento::find($id_pagamento);
							$pagamento->status = 'PENDENTE';
							$pagamento->cod_pagamento = $transaction->getId();
							$pagamento->save();

							$pedido = Pedido::find($id_pedido);
							$pedido->cod_pedido = $transaction->getId();
							$pedido->status = 'PENDENTE';
							$pedido->save();

							$data = array("id_cliente"=>$id_cliente,"id_pedido"=>$id_pedido,"id_pagamento"=>$id_pagamento);

							Mail::send('emails/confirmacao', $data, function($message) use ($cliente){
							    $message->to('thales@hatsu.com.br')->subject('Erro Pagamento Pedido Hatsu');
							});

							return View::make('checkout.index')
							->with('erro_pagamento',$transaction->getRefuseReason())
							->with('id_pedido',$id_pedido)
							->with('id_pagamento',$id_pagamento)
							->withInput(Input::all());

						}

					} catch(PagarMe_Exception $e) {

							$pagamento = Pagamento::find($id_pagamento);
							$pagamento->status = 'PENDENTE';
							$pagamento->cod_pagamento = $transaction->getId();
							$pagamento->save();

							$pedido = Pedido::find($id_pedido);
							$pedido->cod_pedido = $transaction->getId();
							$pedido->status = 'PENDENTE';
							$pedido->save();

							// $data = array("id_cliente"=>$id_cliente,"id_pedido"=>$id_pedido,"id_pagamento"=>$id_pagamento,'boleto'=>$transaction->getBoletoUrl());

							// Mail::send('emails/erro', $data, function($message) {
							//     $message->to('thales@hatsu.com.br', "Erro em Novo Pedido - Hatsu")->subject('Erro no Pagamento');
							// });

							### ENVIAR PARA NOS O MOTIVO DA REFUSED DA COMPRA ###
							
							return View::make('checkout.index')
							->with('erro_pagamento',$e->getMessage())
							->with('id_pedido',$id_pedido)
							->with('id_pagamento',$id_pagamento)
							->withInput(Input::all());

						// echo ; // Retorna todos os erros concatendaos.
					}

			}elseif ($forma_pagamento == "boleto") {

				########################################################
				###### INSERINDO PAGAMENTO NO BANCO DE DADOS  ##########
				########################################################

				if (Input::has('id_pagamento')) {

					$id_pagamento = Input::get('id_pagamento');
					$pagamento = Pagamento::find($id_pagamento);
					
				}else {

					$pagamento = new Pagamento();
				}

				$pagamento->id_cliente 		= $id_cliente;
				$pagamento->id_pedido 		= $id_pedido;
				$pagamento->valor_total 	= $amount;
				$pagamento->forma_pagamento = $forma_pagamento;

				$pagamento->save();

				if (Input::has('id_pagamento')) {

					$id_pagamento = Input::get('id_pagamento');
					
				}else {

					$lastInsertedId = $pagamento->id;
					$id_pagamento = $lastInsertedId;

				}

				#### CLUBE 9 ####
				if($amount <= 0 && Session::has('voucherclube')){ 

					$gera_codigo = rand(10000,99999);
					$pedido = Pedido::find($id_pedido);
					$pedido->cod_pedido = $gera_codigo;
					$pedido->save();

					return View::make('checkout.concluido')
					->with('cod',$gera_codigo);

				}else {

					$transaction = new PagarMe_Transaction(array(
						'payment_method' => 'boleto',
						'amount' => $amount, // 1000 = R$ 10,00
					    'postback_url' => 'http://hatsu.com.br/retorno_boleto/',
					    	"customer" => array(
				        'name' => $cliente->nome,  
				        'document_number' => $cliente->cpf, 
				        'email' => $cliente->email, 
				        'address' => array(
				            'street' => $cliente->endereco, 
				            'neighborhood' => $cliente->bairro,
				            'zipcode' => $cliente->cep, 
				            'street_number' => $cliente->numero
				        	)
						)
					));

					$transaction->charge();

					$transaction->getBoletoUrl(); // Retorna a URL do boleto gerado. PS: Em modo TESTE retorna SEMPRE `null`

					$pedido = Pedido::find($id_pedido);
					$pedido->cod_pedido = $transaction->getId();
					$pedido->save();

					$data = array("id_cliente"=>$id_cliente,"id_pedido"=>$id_pedido,"id_pagamento"=>$id_pagamento,'boleto'=>$transaction->getBoletoUrl());

					Mail::send('emails/confirmacao', $data, function($message) use ($cliente){
					    $message->to($cliente->email)->subject('Obrigado por comprar na Hatsu');
					    $message->to('karen@hatsu.com.br')->subject('Novo Pedido Hatsu')->cc("thales@hatsu.com.br");
					});

					return View::make('checkout.concluido')
					->with('boleto',$transaction->getBoletoUrl())
					->with('cod',$transaction->getId());

				}


			}

		}

	}

}