<?php

class CarrinhoController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex()
	{

		$all_skus = Modelo::where("ativo","=",1)->get();

			foreach ($all_skus as $modelos) {

	      		$verificar_beta = $modelos->sku;

	      		$array_verificador = range(1,15);

				foreach ($array_verificador as $value) {

					$beta = $verificar_beta."-".$value;

					if (Session::has($beta)) {

						$item = Session::get($beta);

		      			$retornar = 1;

		      			$array_carrinho[$beta] = $item;

		      			// echo '<pre>';
		      			// var_dump($array_carrinho);
		      			// exit();
		      		}

		      	}
      		}
      		
      	if ($retornar == 1) {

			return View::make('carrinho.index')
			->with('produtos',$array_carrinho);

      	}
	    
	}

	public function postStore()
	{

		// foreach ($_POST as $key => $value) {
			
		// 	echo '<pre>';
		// 	var_dump($key);
		// 	echo '<br /><Br />';
		// 	var_dump($value);

		// }

		// exit();

		// VALIDAÇÃO DOS CAMPOS

		$produto = array();
		$quantidade = 1;

		foreach ($_POST as $key => $value) { 

			// KEY = PRECO, SKU, MODELO, LINHA, COR, LENTE E TIPO
			// VALUE = ARRAY COM SKU ON INDICE  E VALOR DO KEY NO VALUE

			foreach ($value as $chave => $valor) {

				Session::forget($chave);

				$produto["indice"] = $chave;
				$produto["quantidade"] = $quantidade;

				// CHAVE = SKU
				// VALOR = VALOR DO KEY NO VALUE

				// SKU

				if ($key == "sku") {
					$produto["sku"] = $valor;
				} // FECHA KEY MODELO

				if ($key == "linha") {
					$produto["linha"] = $key;
				}
				// MODELO

				if ($key == "modelo") {
					$produto["modelo"] = $valor;
				} // FECHA KEY MODELO

				// THUMB
				if ($key == "thumb") {
					$produto["thumb"] = $valor;
				} // FECHA KEY THUMBNAIL

				// PREÇO
				if ($key == "preco") {
					$produto["preco"] = $valor;
				} // FECHA KEY PREÇO

				// COR
				if ($key == "cor") {
					$produto["cor"] = $valor;
				} // FECHA KEY COR

				// LENTE

				if ($key == "lente") {

					// VERIFICAR LENTE SELECIONA E SETAR VALOR DA LENTE
					if ($valor == "semgrau") { // sem grau não tem valor
						
						$produto['lente'] = $valor;

					}elseif ($valor == "simples") { // visao simples
						
						$produto['lente'] = $valor;

 					}

				} // FECHA KEY LENTE

				// TIPO

				if ($key == "tipo") {

					if ($produto["lente"] == "semgrau") {

						$valor_lente = '0.00';
						$produto["tipo"] = $valor;
						$produto["valor_lente"] = $valor_lente;
						$produto["subtotal"] = (($quantidade*$produto["preco"])+($produto['valor_lente']));
					
					}elseif($produto["lente"] == "simples") {

						if ($valor == "normais") {

							$valor_lente = "0.00";
							$produto["tipo"] = $valor;
							$produto['valor_lente'] = $valor_lente;
							$produto["subtotal"] = (($quantidade*$produto["preco"])+($produto['valor_lente']));

						}elseif ($valor == "finas") {

							if ($produto["linha"] == "kizo" || $produto["linha"] == "ultra") {
								$valor_lente = "100.00";
							}else {
								$valor_lente = "0.00";
							}

							$produto["tipo"] = $valor;
							$produto['valor_lente'] = $valor_lente;
							$produto["subtotal"] = (($quantidade*$produto["preco"])+($produto['valor_lente']));

						}elseif ($valor == "finissimas") {
							
							$valor_lente = "200.00";
							$produto["tipo"] = $valor;
							$produto['valor_lente'] = $valor_lente;
							$produto["subtotal"] = (($quantidade*$produto["preco"])+($produto['valor_lente']));
						}

					}else {

						$produto["valor_lente"] = "0.00";
					}

				} // FECHA KEY TIPO

				Session::put($chave,$produto);

			} // FECHA FOREACH DO VALUE


		}

		// REDIRECT
		Session::flash('message', 'carrinho atualizado com sucesso!');
		return Redirect::secure('checkout');

	}

		public function getRemover($sku)
	{

	
		Session::forget($sku);

		// REDIRECT
		Session::flash('message', 'produto removido com sucesso!');
		return Redirect::to('produtos');

	}
	
}