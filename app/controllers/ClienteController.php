<?php

class ClienteController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/



	public function getIndex(){
		if( Auth::check() ){ 

			return View::make('minhaconta.index');

		}else{  // return 
			return View::make('cliente.cadastro');

		}
	}

	public function postLogin() {

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'email'      	=> 'required|email',
			'password' 	 => 'required|alphaNum|min:3', // senha pode ser alfanumerica com no minimo de 3 caracteres

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {

			return Redirect::to('login')
				->withErrors($validator)
				->withInput(); 

		} else {


			// create our user data for the authentication
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);

			// echo '<pre>';
			// var_dump($userdata);
			// exit();

			// se estiver tudo certo, efetua o login
			if (Auth::attempt($userdata)) {

				if (Input::has('url_redirect') && Input::get('url_redirect') <> "http://hatsu.com.br/logout") {

					return Redirect::to(Input::get('url_redirect'));
			
				}else {

					return Redirect::intended('minhaconta');
				}

			}else {

				Session::flash('erro', 'Email e/ou senha não encontrados!');
				return Redirect::to('login');

			}
			
		}

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'nome'   		=> 'required',
			'email'      	=> 'required|email',
			'telefone'      => 'required',
			'cidade'		=> 'required',
			'estado'		=> 'required',
			'password' 	 => 'required|alphaNum|min:3', // senha pode ser alfanumerica com no minimo de 3 caracteres
			'c_password' => 'required|alphaNum|min:3|same:password' // senha pode ser alfanumerica com no minimo de 3 caracteres

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('cliente')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$cliente = new Cliente;

			// DADOS PESSOAIS

			$cliente->nome     		= Input::get('nome');
			$cliente->telefone  	= Input::get('telefone');
			$cliente->celular  		= Input::get('celular');
			$cliente->email    		= Input::get('email');
			$cliente->cidade    	= Input::get('cidade');
			$cliente->uf    		= Input::get('estado');
			$cliente->password 		= Hash::make(Input::get('password'));

			$cliente->save();

			$lastInsertedId = $cliente->id_cliente;
			$id_cliente = $lastInsertedId; //get last inserted record's user id value

			// create our user data for the authentication
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);

			// echo '<pre>';
			// var_dump($userdata);
			// exit();

			// se estiver tudo certo, efetua o login
			if (Auth::attempt($userdata)) {

				#### ENVIA EMAIL DE CADASTRO ####

				$data = array('id_cliente'=>$id_cliente);

				Mail::send('emails/cadastro', $data, function($message) use ($cliente){
				    $message->from('atendimento@hatsu.com.br')->to($cliente->email)->subject('Seja bem vindo a Hatsu');
				    $message->from('atendimento@hatsu.com.br')->bcc('karen@hatsu.com.br')->subject('Novo Cadastro Hatsu')->bcc("makoto@hatsu.com.br");
				});

				// validacao efetuada com sucesso
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				// return View::make('index');

				Session::flash('message', 'Conta criada com sucesso!');
				return Redirect::intended('produtos');

			}

		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{

		// busca id do cliente
		$cliente = Cliente::find($id);

		// show the edit form and pass the nerd
		return View::make('clientes.edit')
			->with('cliente', $cliente);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

		// DADOS PESSOAIS

			'nome'		 => 'required',
			'cpf'		 => 'required',
			'nome'		 => 'required',
			'telefone'   => 'required',
			'email'      => 'required|email',


		// ENDEREÇO

			'cep'	   => 'required',
			'estado'   => 'required',
			'cidade'   => 'required',
			'bairro'   => 'required',
			'endereco' => 'required',
			'numero'   => 'required',

		);

		$validator = Validator::make(Input::all(), $rules);

		// executa a validação, retorna a pagina se der errO
		if ($validator->fails()) {

			return Redirect::to('/meusdados')
				->withErrors($validator)
				->withInput();

		} else {

			// ARMAZENANDO NO BANCO DE DADOS

			$cliente = Cliente::find(Auth::id());

			// DADOS PESSOAIS


			$cliente->cpf      		= Input::get('cpf');
			$cliente->nome     		= Input::get('nome');
			$cliente->telefone  	= Input::get('telefone');
			$cliente->celular  		= Input::get('celular');
			$cliente->email    		= Input::get('email');
			$cliente->sexo    		= Input::get('sexo');


			// ENDEREÇO

			$cliente->cep 	    	= Input::get('cep');
			$cliente->endereco    	= Input::get('endereco');
			$cliente->numero   		= Input::get('numero');
			$cliente->complemento   = Input::get('complemento');
			$cliente->bairro    	= Input::get('bairro');
			$cliente->cidade    	= Input::get('cidade');
			$cliente->uf    		= Input::get('estado');

			// PASSWORD

			$cliente->save();

			// REDIRECIONANDO PARA PAGINA DE SUCESSO

			Session::flash('message', 'Dados atualizados com sucesso!');

			return Redirect::to('/meusdados');
		}
	}

}