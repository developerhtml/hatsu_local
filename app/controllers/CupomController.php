<?php

class CupomController extends \BaseController {


	public function getIndex(){
		return View::make('landing/diadospais.index');
	}


	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'nome'   		=> 'required',
			'email'      	=> 'required|email'

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('diadospais')
				->withErrors($validator)
				->withInput(); 

		} else {

			$array_cupom = array(1=>"PRL453",2=>"874RAI",3=>"193HAT",4=>"KAR235",5=>"LEP498",6=>"MKT732");
			
			$sorteio = array_rand($array_cupom,1);

			$cupom_premiado = $array_cupom[$sorteio];

			$cupom = new Cupom;

			$cupom->nome    = Input::get('nome');
			$cupom->email   = Input::get('email');
			$cupom->cupom 	= $cupom_premiado;

			$cupom->save();

			$lastInsertedId = $cupom->id;
			$id_cupom = $lastInsertedId; //get last inserted record's user id value

			$data = array('id_cupom'=>$id_cupom);
			// $data = array("nome"=>$cupom->nome,"email"=>$cupom->email,"cupom"=>$cupom->cupom);

			Mail::send('cupom/padrao', $data, function($message) use ($cupom) {
				$message->to($cupom->email)->subject('Hatsu - Dia dos Pais');
			    $message->to("thaz17@gmail.com")->subject('Hatsu - Dia dos Pais');
			    $message->to("karen@hatsu.com.br")->subject('Hatsu - Dia dos Pais');
			});

			// redirect
			Session::flash('message', 'Cadastro efetuado com sucesso! Acesse seu e-mail para visualizar seu VOUCHER!');
			return Redirect::to('diadospais');

		}

	}

}