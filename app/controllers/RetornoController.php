<?php

class RetornoController extends \BaseController {

	public function cartao()
	{

		$id = $_GET['id'];
		$current_status = $_GET['current_status'];
		$old_status = $_GET['old_status'];
		$fingerprint = $_GET['fingerprint'];

		if(PagarMe::validateFingerprint($id, $fingerprint)) {
			if($current_status == 'paid') {
				
				$find_pagamento = Pagamento::where('cod_pagamento','=',$id);

				$pagamento = Pagamento::find($find_pagamento->id);
				$pagamento->status = 'PAGO';
				$pagamento->cod_pagamento = $id;
				$pagamento->save();

				$pedido = Pedido::find($find_pagamento->id_pedido);
				$pedido->cod_pedido = $id;
				$pedido->status = 'PAGO';
				$pedido->save();

			} else {
				
				$find_pagamento = Pagamento::where('cod_pagamento','=',$id);

				$pagamento = Pagamento::find($find_pagamento->id);
				$pagamento->status = 'PENDENTE';
				$pagamento->cod_pagamento = $id;
				$pagamento->save();

				$pedido = Pedido::find($find_pagamento->id_pedido);
				$pedido->cod_pedido = $id;
				$pedido->status = 'PENDENTE';
				$pedido->save();
				
			}
		}

	}

	public function boleto()
	{

		$id = $_GET['id'];
		$current_status = $_GET['current_status'];
		$old_status = $_GET['old_status'];
		$fingerprint = $_GET['fingerprint'];

		if(PagarMe::validateFingerprint($id, $fingerprint)) {
			if($current_status == 'paid') {
				
				$find_pagamento = Pagamento::where('cod_pagamento','=',$id);

				$$pagamento = Pagamento::find($find_pagamento->id);
				$pagamento->status = 'PAGO';
				$pagamento->cod_pagamento = $id;
				$pagamento->save();

				$pedido = Pedido::find($find_pagamento->id_pedido);
				$pedido->cod_pedido = $id;
				$pedido->status = 'PAGO';
				$pedido->save();

			} else {
								
				$find_pagamento = Pagamento::where('cod_pagamento','=',$id);

				$$pagamento = Pagamento::find($find_pagamento->id);
				$pagamento->status = 'PENDENTE';
				$pagamento->cod_pagamento = $id;
				$pagamento->save();

				$pedido = Pedido::find($find_pagamento->id_pedido);
				$pedido->cod_pedido = $id;
				$pedido->status = 'PENDENTE';
				$pedido->save();
			}
		}

	}

}