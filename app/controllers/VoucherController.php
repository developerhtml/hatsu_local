<?php

class VoucherController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function postStore() {

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(
			'codigo'  => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			
			return Redirect::to('checkout')
				->withErrors($validator)
				->withInput(); 

		} else {

			$voucher = Input::get('codigo');

			if ($voucher == "DTN2014" || $voucher == "dtn2014") {

				Session::put('voucher',$voucher);
				Session::flash('message', 'Desconto aplicado com sucesso!');
				return Redirect::to('checkout');

			}elseif ($voucher == "CLUBE9" || $voucher == "clube9") {

				Session::put('voucherclube',$voucher);
				Session::flash('message', 'Desconto aplicado com sucesso!');
				return Redirect::to('checkout');

			}elseif ($voucher == "PD2014" || $voucher == "pd2014") {

				Session::put('voucher-pd',$voucher);
				Session::flash('message', 'Desconto aplicado com sucesso!');
				return Redirect::to('checkout');

			}elseif ($voucher == "FABIO" || $voucher == "fabio") {

				Session::put('voucher-fabio',$voucher);
				Session::flash('message', 'Desconto aplicado com sucesso!');
				return Redirect::to('checkout');

			}elseif ($voucher == "PRL453" || $voucher == "prl453" ||
				$voucher == "874RAI" || $voucher == "874rai" ||
				$voucher == "193HAT" || $voucher == "193hat" ||
				$voucher == "KAR235" || $voucher == "kar235" ||
				$voucher == "LEP498" || $voucher == "lep498" ||
				$voucher == "MKT732" || $voucher == "mkt732") {

				Session::put('voucher-diadospais',$voucher);
				Session::flash('message', 'Desconto aplicado com sucesso!');
				return Redirect::to('checkout');

			}else {

				Session::flash('naodeu', 'O código informado não é válido!');
				return Redirect::to('checkout');
			}
			
		}

	}

}