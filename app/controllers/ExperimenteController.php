<?php

class ExperimenteController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex()
	{

	    return View::make('experimente.index');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'nome'   		=> 'required',
			'email'      	=> 'required|email',
			'cidade'		=> 'required',
			'estado'		=> 'required',
			'obs'		=> 'required'

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('experimente')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$experimente = new Experimente;

			// DADOS PESSOAIS

			$experimente->nome  		= Input::get('nome');
			$experimente->email    		= Input::get('email');
			$experimente->telefone 		= Input::get('telefone');
			$experimente->cidade    	= Input::get('cidade');
			$experimente->uf    		= Input::get('estado');
			$experimente->obs 			= Input::get('obs');

			$experimente->save();

			$data = array("nome"=>$experimente->nome,"email"=>$experimente->email);

			Mail::send('experimente/padrao', $data, function($message) {
			    $message->to('karen@hatsu.com.br', "Experimente em Casa - Hatsu")->subject('Hatsu - Experimente');
			});

			// VERIFICAR SE É CIDADE ATENDIDA
			// Mail::send('experimente/cliente', $data, function($message) {
			//     $message->to(Input::get('email'), "Experimente em Casa - Hatsu")->subject('Hatsu - Experimente');
			// });


			// Mail::send('experimente/cliente', $data, function($message) {
			//     $message->to(Input::get('email'), "Experimente em Casa - Hatsu")->subject('Hatsu - Experimente');
			// });

			// redirect
			Session::flash('message', 'Solicitação enviada com sucesso! A Hatsu agradece seu contato, retornaremos em breve!');
			return Redirect::to('experimente');

		}

	}

}