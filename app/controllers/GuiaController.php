<?php

class GuiaController extends \BaseController {

	public function getIndex(){
		return View::make('guiadecompra.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		$guiadecompra = new Guia;

		// DADOS PESSOAIS

		$guiadecompra->nome  		= Input::get('nome');
		$guiadecompra->email    	= Input::get('email');
		$guiadecompra->social		= Input::get('social');

		$guiadecompra->save();

		$data = array("nome"=>$guiadecompra->nome,"email"=>$guiadecompra->email);

		Mail::send('guiadecompra/padrao', $data, function($message) {
		    // $message->to('karen@hatsu.com.br', "Guia de Compra - Hatsu")->subject('Hatsu - Guia de Compra');
		    $message->to('thales@hatsu.com.br', "Guia de Compra - Hatsu")->subject('Hatsu - Guia de Compra');
		});

		// redirect
		Session::flash('message', 'Personal Stylist solicitado com sucesso! Aguarde nosso contato!');
		return Redirect::to('guiadecompra');

	}



}