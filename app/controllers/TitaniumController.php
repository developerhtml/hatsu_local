<?php

class TitaniumController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex()
	{

		$dados = Modelo::where('id','=',5)->get();
	    return View::make('titanium.index')
	    ->with('linha',$dados);

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'nome'   		=> 'required',
			'email'      	=> 'required|email',
			'telefone'      => 'required',
			'cidade'		=> 'required',
			'estado'		=> 'required',
			'mensagem'		=> 'required'

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('contato')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$contato = new Contato;

			// DADOS PESSOAIS

			$contato->nome  		= Input::get('nome');
			$contato->email    		= Input::get('email');
			$contato->telefone    	= Input::get('telefone');
			$contato->cidade    	= Input::get('cidade');
			$contato->uf    		= Input::get('estado');
			$contato->mensagem 		= Input::get('mensagem');

			$contato->save();

			$data = array("nome"=>$contato->nome,"email"=>$contato->email);

			Mail::send('contato/padrao', $data, function($message) {
			    $message->to('thales@hatsu.com.br', "Thales - Hatsu")->subject('Hatsu - Contato');
			});



			// redirect
			Session::flash('message', 'Formulário enviado com sucesso! A Hatsu agradece seu contato, retornaremos em breve!');
			return Redirect::to('contato');

		}

	}

}