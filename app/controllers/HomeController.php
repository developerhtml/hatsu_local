<?php

class HomeController extends BaseController {

	public function getLogin()
	{
		// show the form
		return View::make('login');
	}

	public function getLogout()
	{
		Auth::logout();
		return View::make('login');
	}	

	public function postLogin()
		{
			// validate the info, create rules for the inputs
			$rules = array(
				'username'    => 'required|alphaNum|min:4', // validacao de username
				'password' => 'required|alphaNum|min:3' // senha pode ser alfanumerica com no minimo de 3 caracteres
			);

			// executa as validacoes utilizando o Validator (verificar lang)
			$validator = Validator::make(Input::all(), $rules);

			// se naovalidar,retorna pro formulario
			if ($validator->fails()) {
				return Redirect::to('login')
					->withErrors($validator) // envia os errros gerados pelo validator
					->withInput(Input::except('password')); // devolve os valoes informacos, com exceção da senha
			} else {


				// $password = Hash::make(Input::get('password'));

				// create our user data for the authentication
				$userdata = array(
					'username' 	=> Input::get('username'),
					'password' 	=> Input::get('password')
				);

				// se estiver tudo certo, efetua o login
				if (Auth::attempt($userdata)) {

					// validacao efetuada com sucesso
					// redirect them to the secure section or whatever
					// return Redirect::to('secure');
					// for now we'll just echo success (even though echoing in a controller is bad)
					// return View::make('index');
					return Redirect::to('index');

				} else {	 	

					return Redirect::to('login')
					->withErrors("Usuário não encontrado") // envia os errros gerados pelo validator
					->withInput(Input::except('password')); // devolve os valoes informacos, com exceção da senha

				}

			}
		}

	public function getRedirectHome()
	{
		// send to view index
		return View::make('index');
	}
	
	public function getIndex(){
		if( !Auth::check() ) return Redirect::to('login');
		else // return 
		return View::make('index');
	}

}