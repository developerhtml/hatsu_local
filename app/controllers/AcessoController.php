<?php

class AcessoController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex($sortby=NULL,$order=NULL)
	{

	    if ($sortby && $order) {

    		$acessos = Acesso::orderBy($sortby, $order)->get();	
	        
	    } else {
	        $acessos = Acesso::get();
	    }

	    return View::make('acessos.index', compact('acessos', 'sortby', 'order'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function postSearch($sortby=NULL,$order=NULL)
	{

		// TRATANDO O TERMO DE PESQUISA
		if(Input::get('busca')){
			$busca = trim(strip_tags(strtolower(Input::get('busca'))));
		}else {
			Session::flash('erro_busca', 'Preenchimento obrigatório!');
			return Redirect::to('usuario');
		}

		// FAZENDO A CONSULTA NO MODEL
		$acessos = Acesso::where('nome','LIKE','%'.$busca.'%')
					->orderBy('nome', 'asc')
					->get();

		// RETORNANDO A VIEW COM OS VALORES ENCONTRADOS
		return View::make('acessos.index', compact('acessos', 'sortby', 'order'));
	}	

	/**
	 * form pra criar novo usuario
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return View::make('acessos.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

		// 

			'tipo'		 =>	'required',
			'username'   => 'required',
			'nome'		 => 'required',
			'password' 	 => 'required|alphaNum|min:3', // senha pode ser alfanumerica com no minimo de 3 caracteres
			'c_password' => 'required|alphaNum|min:3|same:password', // senha pode ser alfanumerica com no minimo de 3 caracteres
			'email'      => 'required|email',

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('acesso/create')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$acesso = new Acesso;

			// DADOS PESSOAIS

			$acesso->tipo	   		= Input::get('tipo');
			$acesso->nome      	= Input::get('nome');

			$acesso->save();

			// redirect

			Session::flash('message', 'grupo de acesso cadastrado com sucesso!');
			return Redirect::to('acesso');

		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{

		// busca id do cliente
		$acesso = Acesso::find($id);

		// show the edit form and pass the nerd
		return View::make('acessos.edit')
			->with('acessos', $acesso);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

		// DADOS PESSOAIS

			'tipo'		 =>	'required',
			'username'   => 'required',
			'nome'		 => 'required',
			'email'      => 'required|email',
		);

		$validator = Validator::make(Input::all(), $rules);

		// executa a validação, retorna a pagina se der errO
		if ($validator->fails()) {

			return Redirect::to('acesso/edit/' . $id )
				->withErrors($validator)
				->withInput();

		} else {

			// ARMAZENANDO NO BANCO DE DADOS

			$usuario = User::find($id);

			// DADOS PESSOAIS

			$usuario->tipo	   		= Input::get('tipo');
			$usuario->nome      	= Input::get('nome');

			$usuario->save();

			// REDIRECIONANDO PARA PAGINA DE SUCESSO

			Session::flash('message', 'usuário atualizado com sucesso!');

			return Redirect::to('usuario');
		}
	}

	/**
	 * EXCLUIR CLIENTE DO DATABASE
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDestroy($id)
	{

		// delete
		$usuario = User::find($id);
		$usuario->delete();

		// redirect
		Session::flash('message', 'usuário excluído com sucesso!');
		return Redirect::to('usuario');
	}

}