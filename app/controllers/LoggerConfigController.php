<?php

class LoggerConfigController extends \BaseController {

	public function getCreate()
	{

		// busca id do imovel
		$logger = LoggerConfig::find(1);

		return View::make('loggers.create')
			->with('logger', $logger);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

			'tempo_expiracao'		 => 'required'

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('loggerconfig/create')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$logger = LoggerConfig::find(1);
			$logger->tempo_expiracao = Input::get('tempo_expiracao');
			$logger->save();

			// SALVANDO LOG
			$salva_log = Helper::salvaLog("logs",1,"alteracao");	

			// redirect

			Session::flash('message', 'configuração salva com sucesso!');
			return Redirect::to('logger');

		}

	}

}