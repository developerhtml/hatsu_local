<?php

class UserController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex($sortby=NULL,$order=NULL)
	{

	    if ($sortby && $order) {

    		$usuarios = User::orderBy($sortby, $order)->get();	
	        
	    } else {
	        $usuarios = User::get();
	    }

	    return View::make('usuarios.index', compact('usuarios', 'sortby', 'order'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function postSearch($sortby=NULL,$order=NULL)
	{

		// TRATANDO O TERMO DE PESQUISA
		$busca = Input::get('busca');
		if(!empty($busca)){
			$busca = trim(strip_tags(strtolower(Input::get('busca'))));
		}else {
			Session::flash('erro_busca', 'Preenchimento obrigatório!');
			return Redirect::to('usuario');
		}

		$busca = Helper::remover_caracter(Input::get('busca'));
		
		// FAZENDO A CONSULTA NO MODEL
		$usuarios = User::where('nome','LIKE','%'.$busca.'%')
					->orderBy('nome', 'asc')
					->get();

		// RETORNANDO A VIEW COM OS VALORES ENCONTRADOS
		return View::make('usuarios.index', compact('usuarios', 'sortby', 'order'));
	}	

	/**
	 * form pra criar novo usuario
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return View::make('usuarios.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

		// DADOS PESSOAIS

			'tipo'		 =>	'required',
			'username'   => 'required',
			'nome'		 => 'required',
			'password' 	 => 'required|alphaNum|min:3', // senha pode ser alfanumerica com no minimo de 3 caracteres
			'c_password' => 'required|alphaNum|min:3|same:password', // senha pode ser alfanumerica com no minimo de 3 caracteres
			'email'      => 'required|email',

		);

		$validator = Validator::make(Input::all(), $rules);

		// processando a validação das informacoes, redirecionando em caso de erro

		if ($validator->fails()) {
			
			return Redirect::to('usuario/create')
				->withErrors($validator)
				->withInput(); 

		} else {

			// armazenando no banco de dados

			$usuario = new User;

			// DADOS PESSOAIS

			$usuario->tipo	   		= Input::get('tipo');
			$usuario->nome      	= Input::get('nome');
			$usuario->email      	= Input::get('email');
			$usuario->username    	= Input::get('username');
			$usuario->telefone     	= Input::get('telefone');
			$usuario->celular      	= Input::get('celular');
			$usuario->password 		= Hash::make(Input::get('password'));

			$usuario->save();

			// SALVANDO LOG
			$insertedId = $usuario->id;
			$logger = Helper::salvaLog("users",$insertedId,"cadastro");			

			// REDIRECT
			Session::flash('message', 'usuário cadastrado com sucesso!');
			return Redirect::to('usuario');

		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{

		// busca id do cliente
		$usuario = User::find($id);

		// show the edit form and pass the nerd
		return View::make('usuarios.edit')
			->with('usuario', $usuario);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{

		// VALIDAÇÃO DOS CAMPOS

		$rules = array(

		// DADOS PESSOAIS

			'tipo'		 =>	'required',
			'username'   => 'required',
			'nome'		 => 'required',
			'email'      => 'required|email',
		);

		$new_password = 0;

		$pass = Input::get('password');
		if (!empty($pass)) {
			$rules['password'] = 'required|alphaNum|min:3'; // senha pode ser alfanumerica com no minimo de 3 caracteres
			$rules['c_password'] = 'required|alphaNum|min:3|same:password'; // senha pode ser alfanumerica com no minimo de 3 caracteres
			$new_password = 1;
		}

		$validator = Validator::make(Input::all(), $rules);

		// executa a validação, retorna a pagina se der errO
		if ($validator->fails()) {

			return Redirect::to('usuario/edit/' . $id )
				->withErrors($validator)
				->withInput();

		} else {

			// ARMAZENANDO NO BANCO DE DADOS

			$usuario = User::find($id);

			// DADOS PESSOAIS

			$usuario->tipo	   		= Input::get('tipo');
			$usuario->nome      	= Input::get('nome');
			$usuario->email      	= Input::get('email');
			$usuario->username    	= Input::get('username');
			$usuario->telefone     	= Input::get('telefone');
			$usuario->celular      	= Input::get('celular');

			if ($new_password == 1) {
				$usuario->password 		= Hash::make(Input::get('password'));
			}

			$usuario->save();

			// SALVANDO LOG
			$logger = Helper::salvaLog("users",$id,"alteracao");

			// REDIRECIONANDO PARA PAGINA DE SUCESSO

			Session::flash('message', 'usuário atualizado com sucesso!');

			return Redirect::to('usuario');
		}
	}

	/**
	 * EXCLUIR CLIENTE DO DATABASE
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDestroy($id)
	{

		// delete
		$usuario = User::find($id);
		$usuario->delete();

		// SALVANDO LOG
		$logger = Helper::salvaLog("users",$id,"exclusao");		

		// redirect
		Session::flash('message', 'usuário excluído com sucesso!');
		return Redirect::to('usuario');
	}

}