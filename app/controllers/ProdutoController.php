<?php

class ProdutoController extends \BaseController {

	/*
	/ EXIBE A VIEW INDEX, COM O FORM DE BUSCA EO LINK DE ORDENAÇÃO NA TABELA
	*/

	public function getIndex($sortby=NULL,$order=NULL)
	{

   		$produtos = Produto::where('ativo','=',1)->get();	
	    return View::make('produtos.index')->with('produtos',$produtos);
	}


	public function postStore()
	{

		if (Input::has('email') || Input::has('nome')) {

			$esgotado = new Esgotado;

			$esgotado->nome    = Input::get('nome');
			$esgotado->email   = Input::get('email');
			$esgotado->modelo  = Input::get('url_redirect');

			$esgotado->save();

			Session::flash('sucesso', 'seu cadastro foi efetuado com sucesso');
			return Redirect::secure(Input::get('url_redirect'));

		}else {

			Session::flash('erro', 'preencha os campos obrigatórios');
			return Redirect::secure(Input::get('url_redirect'));

		}

	}

}