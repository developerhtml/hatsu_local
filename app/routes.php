<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
*/

// GUIA DE COMPRA AJAX
Route::post('guia-resultado', function(){

	extract($_REQUEST);

	// echo '<pre>';
	// var_dump($_REQUEST);
	// exit();

	$projection = "<h2>PROJECTION</h2><br /><br />".Helper::LinhaProdutos(1);
	$raid = "<h2>RAID</h2><br /><br />".Helper::LinhaProdutos(2);
	$ultra = "<h2>ULTRA</h2><br /><br />".Helper::LinhaProdutos(3);
	$kizo = "<h2>KIZO</h2><br /><br />".Helper::LinhaProdutos(4);
	$titanium = "<h2>TITANIUM</h2><br /><br />".Helper::LinhaProdutos(5);

// ESPORTISTA = Raid / Ultra
// COSMOPOLITA = Projection / Ultra / Titanium
// SOFISTICADO = Projection / Raid / Titanium
// INFANTO JUVENIL = Kizo

// - IF FLEXIVEL
// Remover titanioum e projection;
// - IF LEVEZA
// Remover titanium;

	if (isset($_REQUEST["qualidades"]) && is_array($_REQUEST["qualidades"])) {

		foreach($_REQUEST["qualidades"] as $ind=>$value) {

			if ($value == "flexibilidade") {
				$titanium = "";
				$projection = "";
			}

			if ($value == "leveza") {
				$titanium = "";
			}
					
		}

	}


	$return = "<H1>Encontramos os óculos perfeitos para você!</h1><br />
<h2>Navegue por todas características e fotos dos produtos para decidir qual mais lhe agrada.</h2>
<br />";

	if (isset($_REQUEST["perfil-masculino"])) {

		if ($_REQUEST["perfil-masculino"] == "esportista") {
			$return .= $raid."<br />".$ultra;
		}elseif ($_REQUEST["perfil-masculino"] == "sofisticado") {
			$return .= $projection."<br />".$raid."<br />".$titanium;		
		}elseif ($_REQUEST["perfil-masculino"] == "cosmopolita") {
			$return .= $projection."<br />".$ultra."<br />".$titanium;		
		}elseif ($_REQUEST["perfil-masculino"] == "kids") {
			$return .= $kizo;
		}

	}

	if (isset($_REQUEST["perfil-feminino"])) {

		if ($_REQUEST["perfil-feminino"] == "esportista") {
			$return .= $ultra;
		}elseif ($_REQUEST["perfil-feminino"] == "sofisticado") {
			$return .= $projection."<br />".$raid."<br />".$ultra."<br />".$titanium;		
		}elseif ($_REQUEST["perfil-feminino"] == "cosmopolita") {
			$return .= $projection."<br />".$ultra."<br />".$titanium;		
		}elseif ($_REQUEST["perfil-feminino"] == "kids") {
			$return .= $kizo;
		}

	}

	$return .= "<br /><a href='guiadecompra' class='botao-encontre' id='doover' style='color:#fff;'>REFAZER MEU PERFIL</a><br />";

	$return .= "<br /><br /><h1>Personal Stylist</h1><br />
	<p class='descricao'>Caso ainda esteja em dúvida em qual óculos escolher, oferecemos o serviço gratuito de análise através de um Personal Stylist, onde te informará qual armação mais combina com seu rosto e estilo.</p><br /><br />";

	$return .= "<a href='#stylist' role='button' data-toggle='modal' class='botao-encontre' style='color:#FFF;'>ACIONAR PERSONAL STYLIST</a>";

	return $return;

});

// LISTAR CIDADES VIA CEP
Route::post('getcidade', function(){
	$post_estado = $_POST["estado"];
	$post_cidade = $_POST["cidade"];
	$estado = Estado::where("uf","=",$post_estado);
	$cidades = Cidade::where("nome","=",$post_cidade);
	$array = array(
		'sucesso' => '1',
		'cidade' => $cidades->get(array('id'))->toArray(),
		'estado' => $estado->get(array('id'))->toArray()
	);
	return $array;
});

	// LISTAR ESTADOS E CIDADES
	Route::get('api/dropdown', function(){
	  	$input = Input::get('option');
		$estado = Estado::find($input);
		$cidades = $estado->cidades();
		return $estado->cidades()->get(array('id','nome'));
	});

// ROTA PARA CLIENTE JÁ LOGADO 

Route::group(array('before'=>'auth'), function() {

// MINHA CONTA	
	Route::get('/minhaconta', function()
	{
	    return View::make('minhaconta.index');
	});

	// MEUS PEDIDOS

	Route::get('/pedidos', function()
	{
		$dados = Pedido::where('id_cliente','=',Auth::id())->get();
		$count = count($dados);
    	return View::make('minhaconta.pedidos')
    	->with('dados',$dados)
    	->with('total',$count);
	    
	});

	// MEUS PEDIDOS - VISUALIZAR PEDIDO
	Route::get('/visualizar/{id}', function($id)
	{
		$dados = Pedido::where('id_cliente','=',Auth::id())->where('id','=',$id)->get();
		$itens = PedidoItem::where('id_pedido', '=', $id)->get();
    	return View::make('minhaconta.visualizar')
    	->with('dados',$dados)
    	->with('itens',$itens);
	    
	});

	// // MEUS PEDIDOS - EFETUAR PAGAMENTO
	// Route::get('/pagamento/{id}', function($id)
	// {
	// 	$dados = Pedido::where('id_cliente','=',Auth::id())->where('id','=',$id)->get();
	// 	$itens = PedidoItem::where('id_pedido', '=', $id)->get();
 //    	return View::make('minhaconta.pagamento')
 //    	->with('dados',$dados)
 //    	->with('itens',$itens);
	    
	// });	


	// ALTERAR DADOS

	Route::get('/meusdados', function()
	{
	    return View::make('minhaconta.meusdados')
	    ->with("id",Auth::id());
	});

	// UPLOAD DE RECEITA


	Route::controller('receitas', 'ReceitaController');

	// Route::get('/receitas', function()
	// {
	// 	$dados = Receita::where('id_cliente','=',Auth::id())->get();
	// 	$count = count($dados);
 //    	return View::make('minhaconta.receitas')
 //    	->with('dados',$dados)
 //    	->with('total',$count);
	// });

	// LOGOUT	
	Route::get('/logout', function()
	{
		Auth::logout();
	    return View::make('index');
	});

	}

);	

##############################################
############## LANDING PAGES #################
##############################################

## DIA DOS PAIS  ##
// Route::get('/diadospais', function()
// {
//     return View::make('landing/diadospais.index');
// });

## ICARO TECHNOLOGIES ##
Route::get('/icaro', function()
{
    return View::make('landing/icaro.index');
});

## CI & T ##
Route::get('/ciandt', function()
{
    return View::make('landing/ciandt.index');
});

## DAITAN ##
Route::get('/daitan', function()
{
    return View::make('landing/daitan.index');
});

##############################################
##############################################
##############################################

Route::controller('guiadecompra', 'GuiaController');

Route::controller('cliente', 'ClienteController');

Route::controller('diadospais', 'CupomController');

Route::controller('voucher', 'VoucherController');

Route::controller('newsletter', 'NewsletterController');

Route::get('/hatsu', function()
{
    return View::make('hatsu.index');
});


Route::get('/login', function()
{
    return View::make('login');
});

// GARANTIA	
Route::get('/garantia', function()
{
    return View::make('garantia.index');
});

// SOBRE	
Route::get('/sobre', function()
{
    return View::make('sobre.sobre');
});

// DUVIDAS	
Route::get('/duvidas', function()
{
    return View::make('duvidas.duvidas');
});
// NAMIDIA	
Route::get('/namidia', function()
{
    return View::make('namidia.namidia');
});
// 404	
Route::get('/404', function()
{
    return View::make('404.404');
});
//PONTOS DE VENDAS
Route::get('/pontosdevenda', function()
{
    return View::make('pontosdevenda.pontosdevenda');
});
//PONTOS DE VENDAS
Route::get('/viracopos', function()
{
    return View::make('pontosdevenda.pontosdevenda');
});

// CART	
Route::get('/cart', function()
{
    return View::make('cart.cart');
});
// OBRIGADO	
Route::get('/obrigado', function()
{
    return View::make('obrigado.obrigado');
});
// LENTES	
Route::get('/lentes', function()
{
    return View::make('lentes.lentes');
});
// CHECKOUT PAGE HTML LINKE	
Route::get('/checkout', function()
{
    return View::make('checkout.checkout');
});


// CONTATO
Route::controller('contato', 'ContatoController');

// EXPERIMENTE
Route::controller('experimente', 'ExperimenteController');

// SHOW ROOM
Route::controller('showroom', 'ShowroomController');

// PRODUTO
Route::controller('produtos', 'ProdutoController');

// CARRINHO
Route::controller('carrinho', 'CarrinhoController');

// PAGAMENTO
Route::controller('pagamento', 'PagamentoController');

// CHECKOUT
Route::controller('checkout', 'CheckoutController');

// RETORNO CARTAO
Route::any('retorno_cartao', 'RetornoController@cartao');

// RETORNNO BOLETO
Route::any('retorno_boleto', 'RetornoController@boleto');

// PROJECTION - MODELO E COR 
Route::get('/projection/{modelo}/{cor}/', function($modelo,$cor)
{
	$dados = Modelo::where('url','=',$modelo.'/'.$cor)->get();
    return View::make('projection.'.$modelo)
    ->with('dados',$dados);
});
// PROJECTION
Route::controller('projection', 'ProjectionController');

// RAID - MODELO E COR
Route::get('/raid/{modelo}/{cor}', function($modelo,$cor)
{
	$dados = Modelo::where('url','=',$modelo.'/'.$cor)->get();
    return View::make('raid.'.$modelo)
    ->with('dados',$dados);
});

// RAID
Route::controller('raid', 'RaidController');

// TITANIUM - MODELO E COR 
Route::get('/titanium/{modelo}/{cor}/', function($modelo,$cor)
{
	$dados = Modelo::where('url','=',$modelo.'/'.$cor)->get();
    return View::make('titanium.'.$modelo)
    ->with('dados',$dados);
});

// TITANIUM - MODELO E COR 
Route::get('/titanium/teste/', function()
{
    return View::make('titanium.test');
});

// TITANIUM
Route::controller('titanium', 'TitaniumController');

// ULTRA - MODELO E COR 
Route::get('/ultra/{modelo}/{cor}/', function($modelo,$cor)
{
	$dados = Modelo::where('url','=',$modelo.'/'.$cor)->get();
    return View::make('ultra.'.$modelo)
    ->with('dados',$dados);
});

// ULTRA
Route::controller('ultra', 'UltraController');

// KIZO - MODELO E COR 
Route::get('/kizo/{modelo}/{cor}/', function($modelo,$cor)
{
	$dados = Modelo::where('url','=',$modelo.'/'.$cor)->get();
    return View::make('kizo.'.$modelo)
    ->with('dados',$dados);
});

// kizo
Route::controller('kizo', 'KizoController');

// INDEX
Route::get('/', function()
{
    return View::make('index');
});

// INDEX
Route::get('/emailconfirmacao', function()
{
    return View::make('emails.confirmacao');
});

// INDEX
Route::get('/emailcadastro', function()
{
    return View::make('emails.cadastro');
});

// INDEX
Route::get('/emailexperimente', function()
{
    return View::make('emails.experimente');
});

